//
//  CalendarEventModel.h
//  AIAChina
//
//  Created by Burri on 2015-07-29.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMSServiceManager.h"
#import "CachingService.h"
#import "InterviewEventsMTLModel.h"
#import "HolidayEventsMTLModel.h"
#import "EOPEventsMTLModel.h"
#import "ContactDataProvider.h"
typedef void (^CMSCalendarEventsSuccessCallback)();
@interface CalendarEventModel : NSObject
{
    dispatch_group_t _dispatchGroup;
}
@property (nonatomic,retain) CMSServiceManager *serviceManager;
@property (nonatomic,retain) CachingService *cachingService;
@property (nonatomic, retain) ContactDataProvider *contactDataProvider;
@property (nonatomic,retain) InterviewEventsMTLModel *interviewMTLModel;
@property (nonatomic,retain) HolidayEventsMTLModel *holidayEventsMTLModel;
@property (nonatomic, retain) NSMutableDictionary *allEvnetsDict;
- (void)getAllCalendarEvents:(CMSCalendarEventsSuccessCallback)calendarEvent;
+ (instancetype)sharedInstance;
- (void)deleteAllCalendarEvents:(CMSCalendarEventsSuccessCallback)calendarEvent;

- (void)getQueriesForAllEvents:(NSDictionary *)prameters WithJson:(NSString *)jsonstring andEncodedString:(NSString *)MD5encoded completion:(void(^)(NSDictionary *dict,NSError *err))block;

- (void)insertEnhancementEventsIntoDatabase:(NSArray *)res;

-(void)deletePreviousYearData:(int)CurrentYear;
-(void)deleteICalendarEvent:(NSString*)ref_id AgentCode:(NSString*)agentCode;
@end
