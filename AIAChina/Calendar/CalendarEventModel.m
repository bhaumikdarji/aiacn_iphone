//
//  CalendarEventModel.m
//  AIAChina
//
//  Created by Burri on 2015-07-29.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CalendarEventModel.h"
#import "CMSNetworkingConstants.h"
#import "CachingService.h"
#import "MTLJSONAdapter.h"
#import "InterviewEventsMTLModel.h"
#import "CaledndarEventMTLModel.h"
#import "TblCalendarEvent.h"
#import "TblInterview.h"
#import "EOPEventsMTLModel.h"
#import "HolidayEventsMTLModel.h"
#import "TblTrainingMTLModel.h"
#import "AFHTTPRequestOperationManager.h"

@implementation CalendarEventModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.serviceManager = [[CMSServiceManager alloc]init];
        self.cachingService = [CachingService sharedInstance];
        self.interviewMTLModel = [[InterviewEventsMTLModel alloc]init];
        self.holidayEventsMTLModel = [[HolidayEventsMTLModel alloc]init];
        self.allEvnetsDict = [[NSMutableDictionary alloc]init];
        self.contactDataProvider = [ContactDataProvider sharedContactDataProvider];
    }
    return self;
}
+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}
- (void)deleteAllCalendarEvents:(CMSCalendarEventsSuccessCallback)calendarEvent
{
    _dispatchGroup = dispatch_group_create();
    dispatch_group_enter(_dispatchGroup);
    [self deleteEopCall];
    dispatch_group_enter(_dispatchGroup);
    [self deleteInterviewCall];
        // Notify main queue request had been finished
    dispatch_group_notify(_dispatchGroup, dispatch_get_main_queue(), ^{
        calendarEvent();
        
    });
}
- (void)getAllCalendarEvents:(CMSCalendarEventsSuccessCallback)calendarEvent
{
    _dispatchGroup = dispatch_group_create();
    dispatch_group_enter(_dispatchGroup);
    [self getInterviewEvents];
    dispatch_group_enter(_dispatchGroup);
    [self getEOPEvents];
    dispatch_group_enter(_dispatchGroup);
    [self getHolidayEvents];
    dispatch_group_enter(_dispatchGroup);
    [self getTrainingEvents];
    // Notify main queue request had been finished
    dispatch_group_notify(_dispatchGroup, dispatch_get_main_queue(), ^{
       [self insertEventsIntoDatabase];
        calendarEvent();
        
    });
}

- (void)getTrainingEvents
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSTraningURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject){
        if (responseObject != nil) {
            NSSet *interviewEventsSet = [self getTrainingEventsMTLObjects:responseObject];
            [self.allEvnetsDict setObject:interviewEventsSet forKey:@"trainingEvents"];
        }
        dispatch_group_leave (_dispatchGroup);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        dispatch_group_leave (_dispatchGroup);
    }]resume];
}

- (void)getInterviewEvents
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSInterviewURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject){
          if (responseObject != nil) {
              NSSet *interviewEventsSet = [self getInterviewEventsMTLObjects:responseObject];
              [self.allEvnetsDict setObject:interviewEventsSet forKey:@"interviewEvents"];
          }
          dispatch_group_leave (_dispatchGroup);
      } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
          dispatch_group_leave (_dispatchGroup);
      }]resume];
}
- (void)deleteEopCall
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSDELETEDEOPURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSArray *deletedArray = responseObject;
        [self deleteEOPEvents:deletedArray];
        dispatch_group_leave (_dispatchGroup);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error)
    {
       dispatch_group_leave (_dispatchGroup);
    }]resume];
}
- (void)deleteInterviewCall
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
    [[self.serviceManager getDataFromServiceCall:kCMSDELETEINTERVEWURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSArray *deletedArray = responseObject;//@[@17,@15];
        [self deleteInterviewEvents:deletedArray];
        dispatch_group_leave (_dispatchGroup);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error)
      {
          dispatch_group_leave (_dispatchGroup);
      }]resume];
}
- (void) deleteEOPEvents:(NSArray *)eopEventArray
{
    TblCalendarEvent *calendarEvent = (TblCalendarEvent *)[DbUtils fetchObject:@"TblCalendarEvent"
                                                                    andPredict:[NSPredicate predicateWithFormat:@"agentCode == %@",[CachingService sharedInstance].currentUserId]
                                                             andSortDescriptor:nil
                                                          managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    NSArray *allEOPEventArray;
    
    
    if (calendarEvent.eopEvents.count > 0) {
        allEOPEventArray = [calendarEvent.eopEvents allObjects];
    }
    if (eopEventArray.count > 0)
    {
        for (int count = 0; count < eopEventArray.count; count++) {
            for (TblEOP *eOPObj in allEOPEventArray)
            {
                if ([eOPObj.eventCode intValue] == [[eopEventArray objectAtIndex:count] intValue])
                {
                    NSLog(@"deleted:%@",eOPObj.eventCode);
                    [self.contactDataProvider deleteEOPEvent:eOPObj];
                }
            }
        }
        
    }
}
- (void) deleteInterviewEvents:(NSArray *)interviewEvents
{
    TblCalendarEvent *calendarEvent = (TblCalendarEvent *)[DbUtils fetchObject:@"TblCalendarEvent"
                                                                    andPredict:[NSPredicate predicateWithFormat:@"agentCode == %@",[CachingService sharedInstance].currentUserId]
                                                             andSortDescriptor:nil
                                                          managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    NSArray *allInterviewEventArray;
    
    
    if (calendarEvent.interviewEvents.count > 0) {
        allInterviewEventArray = [calendarEvent.interviewEvents allObjects];
    }
    if (interviewEvents.count > 0)
    {
        for (int count = 0; count < interviewEvents.count; count++) {
            for (TblInterview *interviewObj in allInterviewEventArray)
            {
                if ([interviewObj.interviewCode intValue] == [[interviewEvents objectAtIndex:count] intValue])
                {
                    NSLog(@"deleted:%@",interviewObj.interviewCode);
                    [self.contactDataProvider deleteInterviewEvent:interviewObj];
                }
            }
        }
        
    }
}
- (void)getHolidayEvents
{
    [[self.serviceManager getDataFromServiceCall:kCMSHolidayURL parametersDictionary:nil success:^(NSURLSessionDataTask *task, id responseObject){
          if (responseObject != nil) {
              NSSet *holidayEventsSet = [self getHolidayEventsMTLObjects:responseObject];
              [self.allEvnetsDict setObject:holidayEventsSet forKey:@"holidayEvents"];
          }
          dispatch_group_leave (_dispatchGroup);
      } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
          dispatch_group_leave (_dispatchGroup);
      }]resume];
}

- (void)getEOPEvents
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    
    [[self.serviceManager getDataFromServiceCall:kCMSEOPURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject)
      {
          if (responseObject != nil) {
              NSSet *eopEventsSet = [self getEOPEventsMTLObjects:responseObject];
              [self.allEvnetsDict setObject:eopEventsSet forKey:@"eopEvents"];
          }
          dispatch_group_leave (_dispatchGroup);
      } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
          dispatch_group_leave (_dispatchGroup);
      }]resume];
}

- (void)insertEventsIntoDatabase
{
    if (self.allEvnetsDict.allKeys.count > 0) {
        
        
        NSMutableDictionary *eventsData = [NSMutableDictionary dictionary];
        [eventsData setObject:[CachingService sharedInstance].currentUserId forKey:@"agentCode"];
        [eventsData setObject:@0 forKey:@"allDay"];
        if ([self.allEvnetsDict objectForKey:@"interviewEvents"])
            [eventsData setObject:[self.allEvnetsDict objectForKey:@"interviewEvents"] forKey:@"interviewEvents"];
        if ([self.allEvnetsDict objectForKey:@"eopEvents"])
            [eventsData setObject:[self.allEvnetsDict objectForKey:@"eopEvents"] forKey:@"eopEvents"];
        if ([self.allEvnetsDict objectForKey:@"trainingEvents"])
            [eventsData setObject:[self.allEvnetsDict objectForKey:@"trainingEvents"] forKey:@"trainingEvents"];

        
        NSMutableArray *mutableEventsJSONArray = [[NSMutableArray alloc] init];
        [mutableEventsJSONArray addObject:[NSDictionary dictionaryWithDictionary:eventsData]];
        NSArray *calendarEventsMTLModelArray = [MTLJSONAdapter modelsOfClass:[CaledndarEventMTLModel class] fromJSONArray:mutableEventsJSONArray error:nil];
        
        
        for (CaledndarEventMTLModel *calendarEventModel in calendarEventsMTLModelArray)
        {
            TblCalendarEvent *event = [MTLManagedObjectAdapter managedObjectFromModel:calendarEventModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
            if (event) {
                [self.cachingService saveContext];
            }
        }
    }
}

- (void)getQueriesForAllEvents:(NSDictionary *)prameters WithJson:(NSString *)jsonstring andEncodedString:(NSString *)MD5encoded completion:(void(^)(NSDictionary *dict,NSError *err))block
{
    
    AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString *sessionId = delegate.sessionid;
    // web service call if the session id exits
    if (sessionId== nil || [sessionId isEqualToString:@""]){
    }
    else {
        AFHTTPRequestOperationManager *ASAPIManager=[[AFHTTPRequestOperationManager alloc]init];
        [ASAPIManager.securityPolicy setAllowInvalidCertificates:YES];
        [ASAPIManager.requestSerializer setValue:@"multipart/form-data;charset=UTF-8;boundary=--AIACalendar--" forHTTPHeaderField:@"Content-Type"];
        [ASAPIManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html" ,nil]];
        [ASAPIManager POST:KErecruitmentEnhancementURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFormData:[sessionId dataUsingEncoding:NSUTF8StringEncoding] name:@"SessionId"];
            [formData appendPartWithFormData:[@"UTF-8" dataUsingEncoding:NSUTF8StringEncoding] name:@"Encoding"];
            [formData appendPartWithFormData:[MD5encoded dataUsingEncoding:NSUTF8StringEncoding] name:@"Signature"];
            [formData appendPartWithFormData:[@"OneCalendarEventService" dataUsingEncoding:NSUTF8StringEncoding] name:@"Service"];
            [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",jsonstring] dataUsingEncoding:NSUTF8StringEncoding] name:@"Data"];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *Resposedict=responseObject;
            
            block(Resposedict,nil);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@",error.description);
        }];
    }
}

- (void)insertEnhancementEventsIntoDatabase:(NSArray *)res{
    
    NSManagedObject *managedObj;
    NSManagedObjectContext *context;
    NSError *error;
    context = [self.cachingService managedObjectContext];
    NSArray *dictResponse =res;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"ICalendarEvents" inManagedObjectContext:context]];
    NSArray *arr = [context executeFetchRequest:fetchRequest error:nil];
    for (int i=0; i<[arr count]; i++) {
        NSDictionary *dict=[arr objectAtIndex:i];
        NSString *eventid=[dict valueForKey:@"eventId"];
        for (int j=0; j<[dictResponse count]; j++) {
            NSDictionary *dict=[dictResponse objectAtIndex:j];
            NSString *evid=[dict valueForKey:@"id"];
            if ([eventid isEqualToString:evid]) {
                NSManagedObject *obj=[arr objectAtIndex:i];
                [context deleteObject:obj];
            }
        }
    }
    [context save:&error];
    
    for (id data in dictResponse) {
        if ([data isKindOfClass:[NSDictionary class]]){
            managedObj = [NSEntityDescription insertNewObjectForEntityForName:@"ICalendarEvents" inManagedObjectContext:context];
            [managedObj setValue:[data valueForKey:@"address"] forKey:@"address"];
            [managedObj setValue:[data valueForKey:@"agentCode"] forKey:@"agentCode"];
            [managedObj setValue:[data valueForKey:@"co"] forKey:@"co"];
            [managedObj setValue:[data valueForKey:@"description"] forKey:@"eventDescription"];
            [managedObj setValue:[data valueForKey:@"id"] forKey:@"eventId"];
            [managedObj setValue:[data valueForKey:@"endTime"] forKey:@"endTime"];
            [managedObj setValue:[data valueForKey:@"openFlag"] forKey:@"openFlag"];
            [managedObj setValue:[data valueForKey:@"origin"] forKey:@"origin"];
            [managedObj setValue:[data valueForKey:@"referenceId"] forKey:@"referenceId"];
            [managedObj setValue:[data valueForKey:@"startTime"] forKey:@"startTime"];
            NSString *startdate=[data valueForKey:@"startTime"];
            startdate=[startdate substringToIndex:10];
            NSString *stdt = [NSString stringWithFormat:@"%@ 00:00:00",[startdate substringToIndex:10]];
            [managedObj setValue:stdt forKey:@"st_dt"];
            [managedObj setValue:[data valueForKey:@"subject"] forKey:@"subject"];
            [managedObj setValue:[data valueForKey:@"typeId"] forKey:@"typeId"];
            [managedObj setValue:[data valueForKey:@"typeName"] forKey:@"typeName"];
            [managedObj setValue:[data valueForKey:@"updateTime"] forKey:@"updateTime"];
            
            [context save:&error];
        }
    }
}

-(void)deletePreviousYearData:(int)CurrentYear {
    NSManagedObjectContext *context;
    NSError *error;
    context = [self.cachingService managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"ICalendarEvents" inManagedObjectContext:context]];
    NSArray *arr = [context executeFetchRequest:fetchRequest error:nil];
    if (arr && [arr count]>0) {
        for (int i=0; i<[arr count]; i++) {
            NSDictionary *dict=[arr objectAtIndex:i];
            NSString *starttime=[dict valueForKey:@"startTime"];
            starttime=[starttime substringToIndex:4];
            int yr=[starttime intValue];
            if (yr<CurrentYear) {
                NSManagedObject *obj=[arr objectAtIndex:i];
                [context deleteObject:obj];
            }
        }
        [context save:&error];
    }
    
}

-(void)deleteICalendarEvent:(NSString*)ref_id AgentCode:(NSString*)agentCode {
    NSManagedObjectContext *context;
    NSError *error;
    context = [self.cachingService managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"ICalendarEvents" inManagedObjectContext:context]];
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"referenceId == %@ AND agentCode==%@",ref_id,agentCode];
    [fetchRequest setPredicate:pred];
    NSArray *arr = [context executeFetchRequest:fetchRequest error:nil];
    if (arr && [arr count]>0) {
        for (int i=0; i<[arr count]; i++) {
            NSManagedObject *obj=[arr objectAtIndex:i];
            [context deleteObject:obj];
        }
        [context save:&error];
    }
}

- (NSSet *)getTrainingEventsMTLObjects:(id)resDict
{
    
    NSArray *traningsEventsArray = resDict;
    NSError *mtlError;
    NSMutableArray *trainingListArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [traningsEventsArray count]; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:traningsEventsArray[i]];
//        NSString *dateString =  [dict objectForKey:@"creationDate"];
//        NSDate *date = [NSDate stringToDate:dateString dateFormat:kServerDateFormatte];
//        [dict setObject:date forKey:@"creationDate"];
        NSString *startDateString =  [dict objectForKey:@"startDate"];
        NSDate *startDate = [NSDate stringToDate:startDateString dateFormat:kServerDateFormatte];
        [dict setObject:startDate forKey:@"startDate"];
        TblTrainingMTLModel *interviewEventModel = [MTLJSONAdapter modelOfClass:[TblTrainingMTLModel class] fromJSONDictionary:dict  error:&mtlError];
        [interviewEventModel updateTrainingDetails];
        [trainingListArray addObject:interviewEventModel];
    }
    return [NSSet setWithArray:trainingListArray];
}

- (NSSet *)getInterviewEventsMTLObjects:(id)resDict
{
    
    NSArray *interviewEventsArray = resDict;
    NSError *mtlError;
    NSMutableArray *interviewListArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [interviewEventsArray count]; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:interviewEventsArray[i]];
        NSString *dateString =  [dict objectForKey:@"interviewDate"];
        NSDate *date = [NSDate stringToDate:dateString dateFormat:kServerDateFormatte];
        [dict setObject:date forKey:@"interviewDate"];
        InterviewEventsMTLModel *interviewEventModel = [MTLJSONAdapter modelOfClass:[InterviewEventsMTLModel class] fromJSONDictionary:dict  error:&mtlError];
        [interviewListArray addObject:interviewEventModel];
    }
    return [NSSet setWithArray:interviewListArray];
}
- (NSSet *)getHolidayEventsMTLObjects:(id)resDict
{
    NSArray *holidayEventsArray = resDict;
    NSError *mtlError;
    NSMutableArray *holidayListArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [holidayEventsArray count]; i++)
    {
        HolidayEventsMTLModel *holidatModel = [MTLJSONAdapter modelOfClass:[HolidayEventsMTLModel class] fromJSONDictionary:holidayEventsArray[i]  error:&mtlError];
        [holidayListArray addObject:holidatModel];
    }
    return [NSSet setWithArray:holidayListArray];
}
- (NSSet *)getEOPEventsMTLObjects:(id)resDict
{
    NSArray *eopEventsArray = resDict;
    NSError *mtlError;
    NSMutableArray *eopListArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [eopEventsArray count]; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:eopEventsArray[i]];
        NSString *dateString =  [dict objectForKey:@"eventDate"];
        NSDate *date = [NSDate stringToDate:dateString dateFormat:kServerDateFormatte];
        [dict setObject:date forKey:@"eventDate"];
        EOPEventsMTLModel *eopEventModel = [MTLJSONAdapter modelOfClass:[EOPEventsMTLModel class] fromJSONDictionary:dict  error:&mtlError];
        [eopListArray addObject:eopEventModel];
    }
    return [NSSet setWithArray:eopListArray];
}

//+ (void)insertServiceProvidersFromMTLArray:(NSArray *)serviceProviderMTLArray intoCache:(CachingService *)cachingService
//{
//    for (ServiceProviderMTLModel *serviceProviderMTLModel in serviceProviderMTLArray) {
//        [ServiceProvider insertServiceProviderMTLModel:serviceProviderMTLModel intoCache:cachingService];
//    }
//    [cachingService saveContext];
//}
@end
