//
//  InterviewEventsMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-07-29.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblCalendarEvent.h"
#import "MTLValueTransformer.h"
@interface InterviewEventsMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSNumber *interviewCode;
@property (nonatomic, strong) NSString *interviewType;
@property (nonatomic, strong) NSString *interviewSessionName;
@property (nonatomic, strong) NSDate *interviewDate;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *endTime;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *interviewMaterial;
@property (nonatomic, strong) NSNumber *estimatedCondidates;
@property (nonatomic, strong) NSNumber *buCode;
@property (nonatomic, strong) NSString *cityCode;
@property (nonatomic, strong) NSString *sscCode;
@property (nonatomic, strong) NSString * createdBy;
@property (nonatomic, strong) NSString *creationDate;
@property (nonatomic, strong) NSString * modifiedBy;
@property (nonatomic, assign) BOOL status;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSNumber *district;
@property (nonatomic, strong) NSString *buName;
@property (nonatomic, strong) NSString *organizer;
@property (nonatomic, strong) NSNumber *registeredCount;
@property (nonatomic, strong) NSNumber *attendeeCount;
@property (nonatomic, strong) NSString *attachmentName;
@property (nonatomic, strong) NSString *attachmentPath;
@property (nonatomic,strong) NSString *modificationDate;
@property (nonatomic, assign) BOOL isRegistered;
@property (nonatomic, strong) TblCalendarEvent *calendarEvent;
@end
