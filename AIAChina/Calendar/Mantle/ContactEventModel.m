//
//  ContactEventModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "ContactEventModel.h"
#import "NSDate+FFDaysCount.h"
#import "CMSServiceManager.h"
#import "CachingService.h"
#import "CMSNetworkingConstants.h"
#import "ContactMTLModel.h"
#import "TblContact.h"
#import "EOPEventsMTLModel.h"
#import "GroupMTLModel.h"
#import "CandidateFamilytInfoMTLModel.h"
#import "PersonalCertificationMTLModel.h"
#import "CandidateESignatureMTLModel.h"
#import "CandidateEducationDetailsMTLModel.h"
#import "CandidateExperienceMTLModel.h"
#import "NotesMTLModel.h"
#import "InterviewEventsMTLModel.h"
#import "TblInterviewCandidateMTLModel.h"
#import "TblInterview.h"
#import "InterviewEventsMTLModel.h"
#import "TblEOPCandidateMTLModel.h"
#import "TblEOPCandidate.h"
#import "TblInterviewCandidate.h"
#import "AgentMTLModel.h"
#import "TblAgentDetails.h"
#import "DateUtils.h"
#import "ContactDataProvider.h"
#import "GreetingsMTLModel.h"
#import "TblGreetings.h"
#import "CaledndarEventMTLModel.h"
#import "TblCalendarEvent.h"
#import "DropDownManager.h"
#import "NSString+isNumeric.h"

#define kFamilyInfoConstant @"candidateFamilyInfos"
#define kPersonalCertificationInfoConstant @"candidateProfessionalCertifications"
#define kEducationInfoConstant @"candidateEducations"
#define kEsignatureInfoConstant @"candidateESignatures"
#define kGroupsInfoConstant @"candidateGroups"

#define kExperienceInfoConstant @"candidateWorkExperiences"
#define kNotesInfoConstant @"candidateNotes"
#define kCMSEOPURL1 @"rest/event/getAllEop"
#define kCMSDELETEDEOPURL @"rest/event/getAllDeletedEop"
#define kCMSPastEOPURL @"rest/event/getAllEopPast"
#define kCMSEOPResigteredCountURL @"rest/event/getEOPRegisteredCandidateCount"
#define kCMSInterviewResigteredCountURL @"rest/interview/getInterviewRegisteredCandidateCount"
#define kCMSCCTestResultURL @"rest/addressBook/getCCTestResults"
#define kCMSPostCCTestResultURL @"rest/addressBook/insertCCTestResults"
#define kCMSSyncURL @"rest/addressBook/sync"
#define kCMSSyncNotesURL @"rest/addressBook/syncNotes"
#define kCMSEOPCandidateRegister @"rest/event/candidateRegister?agentId="
#define kCMSGetEOPRegisteredCandidate @"rest/event/getEOPRegisteredCandidate"
#define kCMSGetCandidateRegisteredEOP @"rest/event/getCandidateRegisteredEOP"
#define kCMAPostFirstInterview @"rest/firstInterview/pushFirstInterview"
#define kCMSGetFirstInterview @"rest/firstInterview/getFirstInterview"
#define kCMSGetALEExamResults @"rest/training/getALEExamResults"
#define kCMSPushContractedDetails @"rest/addressBook/pushContractedDetails"
#define kCMSDeleteInterviewRegistration @"rest/interview/deleteInterviewRegistration"
#define kCMSDeleteEOPRegistration @"rest/event/deleteEOPRegistration"
#define kCMSGetAllInterviewWithCandidateCode @"rest/interview/getAllInterview"
#define kCMSDeleteInterview @"rest/interview/getAllDeletedInterview"
#define kCMSGetAllPastInterviewWithCandidateCode @"rest/interview/getAllInterviewPast"
#define kCMSGetInterviewRegisteredCandidate @"rest/interview/getInterviewRegisteredCandidate"
#define kCMSInterViewCandidateRegister @"rest/interview/candidateRegister?agentId="
#define kCMSABCPushResults @"rest/training/pushResults"
#define kCMSgetABCResults @"rest/training/getABCTrainingResult"
#define kCMSGetContractDetail @"rest/addressBook/getContractDetail"
#define kCMSGetEGreetingsDetail @"rest/egreeting/getAllegreeting"
#define kCMSCandidateInteviewStatus @"rest/interview/getCandidateInterviewStatus"
#define kGetCandidateDetails @"rest/interview/getcandidatDetails"



@implementation ContactEventModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.serviceManager = [[CMSServiceManager alloc]init];
        self.cachingService = [CachingService sharedInstance];
        self.contactDataProvider = [ContactDataProvider sharedContactDataProvider];
        
    }
    return self;
}
+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}
#pragma mark - AddressBook service call
/** AddressBook service calls */
- (void)syncAddressBook:(NSString *)agentCode contactsArray:(NSArray *)contactArray success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    [[self.serviceManager createPostDataTaskForRequestWithVersionPath:@"" relativePath:kCMSSyncURL forBodyDictionary:contactArray success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject, error);
    }] resume];
}

- (void)syncNotes:(NSString *)agentCode contactsArray:(NSArray *)notesArray success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    [[self.serviceManager createPostDataTaskForRequestWithVersionPath:@"" relativePath:kCMSSyncNotesURL forBodyDictionary:notesArray success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject, error);
    }] resume];
}

- (void)getAddressBook:(NSString *)agentId dateTime:(NSString *)dateTime pageNumber:(int)page success:(CMSContactSuccessCallbackWithCount)success failure:(CMSContactFailureCallback)failure
{
    //?agentId=17061991&pageNo=2
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[NSNumber numberWithInt:page] forKey:@"pageNo"];
    [dict setValue:agentId forKey:@"agentId"];
    [dict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    if (![dateTime isEqualToString:@""])
    {
        NSString *lastBackUpDateTime = dateTime;
        NSTimeInterval seconds = [lastBackUpDateTime doubleValue];
        NSDate * backUpDate = [NSDate dateWithTimeIntervalSince1970:seconds];
        [dict setValue:[NSDate dateToString:backUpDate andFormate:kServerDateFormatte] forKey:@"dateTime"];
    }
    [[self.serviceManager getDataFromServiceCall:kCMSGetAddressBook parametersDictionary:dict success:^(NSURLSessionDataTask *task, id responseObject)
      {
          //NSLog(@"%@",responseObject);
          if (responseObject != nil) {
              NSArray *addressBookListArray = responseObject;
              int count = 0;
              if (addressBookListArray.count > 0) {
                   count= (int)[addressBookListArray count];
              }
              [self contactMTLModelArrayByUsingRespnoseObject:responseObject];
              success(count);
          }
      } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error)
    {
        failure(responseObject,error);
      }]resume];
}

- (void)contactMTLModelArrayByUsingRespnoseObject:(id)resDict
{
    NSArray *addressBookListArray = resDict;
    NSError *mtlError;
    NSMutableArray *tempAddressBookListArray = [[NSMutableArray alloc]init];
    for (NSDictionary *dict in addressBookListArray)
    {
        NSSet *familySet,*personalCertificationSet,*educationInforSet,*eSignatureSet,*groupSet,*experienceSet,*notesSet;
        NSArray *familyArray =  [dict objectForKey:kFamilyInfoConstant];
        if ([familyArray count] > 0) {
            NSArray *familyMTLArray = [MTLJSONAdapter modelsOfClass:[CandidateFamilytInfoMTLModel class] fromJSONArray:familyArray error:nil];
            familySet = [NSSet setWithArray:familyMTLArray];
        }
        NSArray *personalCertificationArray = [dict objectForKey:kPersonalCertificationInfoConstant];
        if ([personalCertificationArray count] > 0) {
            NSArray *personalCertificationMTLArray = [MTLJSONAdapter modelsOfClass:[PersonalCertificationMTLModel class] fromJSONArray:personalCertificationArray error:nil];
            personalCertificationSet = [NSSet setWithArray:personalCertificationMTLArray];
        }
        NSArray *educationArray = [dict objectForKey:kEducationInfoConstant];
        if ([educationArray count] > 0) {
            NSArray *educationMTLArray = [MTLJSONAdapter modelsOfClass:[CandidateEducationDetailsMTLModel class] fromJSONArray:educationArray error:nil];
            educationInforSet = [NSSet setWithArray:educationMTLArray];
        }
        NSArray *eSignatureArray = [dict objectForKey:kEsignatureInfoConstant];
        if ([eSignatureArray count] > 0) {
            NSMutableArray *eSignatureMTLArray = [[NSMutableArray alloc]init];
            for (int eSignaure = 0; eSignaure < [eSignatureArray count]; eSignaure++)
            {
                NSDictionary *dict = eSignatureArray[eSignaure];
                NSString *esignatureString = [dict objectForKey:@"eSignaturePhoto"];
                NSData *eSignatureData;
                if (esignatureString) {
                   eSignatureData = [[NSData alloc] initWithBase64EncodedString:esignatureString options:0];
                }
                
                NSMutableDictionary * contactDict = [NSMutableDictionary dictionaryWithDictionary:eSignatureArray[eSignaure]];
                [contactDict setValue:eSignatureData forKey:@"eSignaturePhoto"];
                CandidateESignatureMTLModel *eSignatureMTLModel = [MTLJSONAdapter modelOfClass:[CandidateESignatureMTLModel class] fromJSONDictionary:contactDict  error:&mtlError];
                [eSignatureMTLArray addObject:eSignatureMTLModel];
            }
            
            eSignatureSet = [NSSet setWithArray:eSignatureMTLArray];
        }
        
        NSArray *groupsArray = [dict objectForKey:kGroupsInfoConstant];
        if ([groupsArray count] > 0) {
            NSArray *groupsMTLArray = [MTLJSONAdapter modelsOfClass:[GroupMTLModel class] fromJSONArray:groupsArray error:nil];
            groupSet = [NSSet setWithArray:groupsMTLArray];
        }

        NSArray *experienceArray = [dict objectForKey:kExperienceInfoConstant];
        if ([experienceArray count] > 0) {
            NSArray *experienceMTLArray = [MTLJSONAdapter modelsOfClass:[CandidateExperienceMTLModel class] fromJSONArray:experienceArray error:nil];
            experienceSet = [NSSet setWithArray:experienceMTLArray];
        }
        
        NSArray *notesArray = [dict objectForKey:kNotesInfoConstant];
        if(notesArray.count > 0) {
//            notesArray =  @[@{@"iosAddressCode": @"37",
//                                @"activityType": @"Appointment",
//                                @"activityDate": @"2014-11-06 05:00:00 +0000",
//                                        @"desc": @"description",
//                              @"activityStatus": @1
//                            }
//                            ];
        
            NSMutableArray *tmpNoteArray = [[NSMutableArray alloc] init];
            for (NSDictionary *noteDic in notesArray) {
                NSString *activityDateString =  [noteDic objectForKey:@"activityDate"];
                NSDate *activityDate;
                if (!isEmptyString(activityDateString)) {
                    activityDate = [NSDate stringToDate:activityDateString dateFormat:kServerDateFormatte];
                }

                NSMutableDictionary * tmpNoteDict = [NSMutableDictionary dictionaryWithDictionary:noteDic];
                [tmpNoteDict setValue:activityDate forKey:@"activityDate"];
                [tmpNoteDict setValue:[tmpNoteDict valueForKey:@"iosNoteCode"] forKey:@"iosAddressCode"];
                [tmpNoteDict removeObjectForKey:@"iosNoteCode"];
                [tmpNoteArray addObject:tmpNoteDict];
            }
            
            if ([tmpNoteArray count] > 0) {
                NSArray *notesMTLArray = [MTLJSONAdapter modelsOfClass:[NotesMTLModel class] fromJSONArray:tmpNoteArray error:nil];
                notesSet = [NSSet setWithArray:notesMTLArray];
            }
        }
        
        NSString *dobString =  [dict objectForKey:@"birthDate"];
        NSDate *dobDate;
        if (![dobString isEqualToString:@""]) {
             dobDate = [DateUtils stringToDate:dobString dateFormat:kServerDateFormatte];
        }
        
        NSString *newComerSource;
        if ([dict objectForKey:@"newComerSource"]) {
            newComerSource = [dict objectForKey:@"newComerSource"];
            //newComerSource = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_NEWCOMER_SOURCE code:newComerSource];
            
        }
        
        NSString *gender;
        if ([dict objectForKey:@"gender"]) {
            gender = [dict objectForKey:@"gender"];
            gender = [gender getTrimmedValue];
            
        }
        
        NSString *lastContactedString = @"";
        if ([dict objectForKey:@"lastContactedDate"]) {
            lastContactedString = [dict objectForKey:@"lastContactedDate"];
        }
        
        NSDate *lastContactedDate;
        if(![lastContactedString isEqualToString:@""])
            lastContactedDate = [NSDate stringToDate:lastContactedString dateFormat:kServerDateFormatte];
         NSString *modificationDateString = @"";
        if ([dict objectForKey:@"modificationDate"]) {
            modificationDateString = [dict objectForKey:@"modificationDate"];
        }
        NSDate *modificationDate;
        if (![modificationDateString isEqualToString:@""]) {
            modificationDate = [NSDate stringToDate:modificationDateString dateFormat:kServerDateFormatte];
        }
        
        
       
        NSDate *presenterReportDate;
        NSString *presenterReportDateString =  [dict objectForKey:@"presenterDate"];
        if (!isEmptyString(presenterReportDateString)) {
            presenterReportDate = [DateUtils stringToDate:presenterReportDateString dateFormat:kServerDateFormatte];
        }
        
        NSString *base64Encoded = [dict objectForKey:@"presenterImg"];
        NSData *nsdataPresenterFromBase64String = nil;
        if(!isEmptyString(base64Encoded)) {
            nsdataPresenterFromBase64String = [[NSData alloc] initWithBase64EncodedString:base64Encoded options:0];
        }
        
        
        base64Encoded = [dict objectForKey:@"candidatePhoto"];
        if(isEmptyString(base64Encoded))
            base64Encoded = @"";
        NSData *nsdataFromBase64String = [[NSData alloc] initWithBase64EncodedString:base64Encoded options:0];

        
        NSMutableDictionary * contactDict = [NSMutableDictionary dictionaryWithDictionary:dict];
        [contactDict setValue:nsdataFromBase64String forKey:@"candidatePhoto"];
        [contactDict setValue:newComerSource forKey:@"newComerSource"];
        [contactDict setValue:dobDate forKey:@"birthDate"];
        [contactDict setValue:[DateUtils dateToString:dobDate andFormate:@"dd-MMM"] forKey:@"birthMonthDay"];
        [contactDict setValue:[NSNumber numberWithInteger:dobDate.year] forKey:@"birthYear"];
        [contactDict setValue:lastContactedDate forKey:@"lastContactedDate"];
        [contactDict setValue:[NSNumber numberWithInteger:[DateUtils ageFromBirthday:dobDate]] forKey:@"age"];
        if (modificationDate) {
             [contactDict setObject:modificationDate forKey:@"modificationDate"];
        }
    
        [contactDict setValue:gender forKey:@"gender"];
        [contactDict setValue:familySet forKey:kFamilyInfoConstant];
        [contactDict setValue:personalCertificationSet forKey:kPersonalCertificationInfoConstant];
        [contactDict setValue:educationInforSet forKey:kEducationInfoConstant];
        [contactDict setValue:eSignatureSet forKey:kEsignatureInfoConstant];
        [contactDict setValue:groupSet forKey:kGroupsInfoConstant];
        [contactDict setValue:experienceSet forKey:kExperienceInfoConstant];
        [contactDict setValue:notesSet forKey:kNotesInfoConstant];
        [contactDict setValue:presenterReportDate forKey:@"presenterDate"];
        [contactDict setValue:nsdataPresenterFromBase64String forKey:@"presenterImg"];
        ContactMTLModel *contactMTLModel = [MTLJSONAdapter modelOfClass:[ContactMTLModel class] fromJSONDictionary:contactDict  error:&mtlError];
        if (contactMTLModel)
            [tempAddressBookListArray addObject:contactMTLModel];
        
    }
    
    for (ContactMTLModel *contactModel in tempAddressBookListArray)
    {
        TblContact *event = [MTLManagedObjectAdapter managedObjectFromModel:contactModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
        if (event) {
            if([event.group count] == 0)
                [ApplicationFunction joinUngroup:event];
            [self.cachingService saveContext];
        }
    }
}

#pragma mark - EOP Service Calls
/** EOP Service Calls */

- (void)getEOPRegistrationCall:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSEOPURL1 parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        NSSet *eopEventsSet =[self getEOPEventsMTLObjects:responseObject isPastEvents:NO];
        success(eopEventsSet);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

- (void)getEOPDeleteCall:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSDELETEDEOPURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSArray *deleteArray = responseObject;
        [self deleteEOPEvents:deleteArray];
        
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}
- (void)deleteInterviewCall:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSDELETEINTERVEWURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSArray *deleteArray = responseObject;//@[@17,@15];
        [self deleteInterviewEvents:deleteArray];
        
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}
- (void)getPastEOP:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSPastEOPURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        NSSet *eopEventsSet =[self getEOPEventsMTLObjects:responseObject isPastEvents:YES];
        success(eopEventsSet);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}
- (void) deleteEOPEvents:(NSArray *)eopEventArray
{
    TblCalendarEvent *calendarEvent = (TblCalendarEvent *)[DbUtils fetchObject:@"TblCalendarEvent"
                                                                    andPredict:[NSPredicate predicateWithFormat:@"agentCode == %@",[CachingService sharedInstance].currentUserId]
                                                             andSortDescriptor:nil
                                                          managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    NSArray *allEOPEventArray;
   
    
    if (calendarEvent.eopEvents.count > 0) {
        allEOPEventArray = [calendarEvent.eopEvents allObjects];
    }
    if (eopEventArray.count > 0)
    {
        for (int count = 0; count < eopEventArray.count; count++) {
            for (TblEOP *eOPObj in allEOPEventArray)
            {
                if ([eOPObj.eventCode intValue] == [[eopEventArray objectAtIndex:count] intValue])
                {
                    NSLog(@"deleted:%@",eOPObj.eventCode);
                    [self.contactDataProvider deleteEOPEvent:eOPObj];
                }
            }
        }
        
    }
}
- (void) deleteInterviewEvents:(NSArray *)interviewEvents
{
    TblCalendarEvent *calendarEvent = (TblCalendarEvent *)[DbUtils fetchObject:@"TblCalendarEvent"
                                                                    andPredict:[NSPredicate predicateWithFormat:@"agentCode == %@",[CachingService sharedInstance].currentUserId]
                                                             andSortDescriptor:nil
                                                          managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    NSArray *allInterviewEventArray;
    
    
    if (calendarEvent.interviewEvents.count > 0) {
        allInterviewEventArray = [calendarEvent.interviewEvents allObjects];
    }
    if (interviewEvents.count > 0)
    {
        for (int count = 0; count < interviewEvents.count; count++) {
            for (TblInterview *interviewObj in allInterviewEventArray)
            {
                if ([interviewObj.interviewCode intValue] == [[interviewEvents objectAtIndex:count] intValue])
                {
                    NSLog(@"deleted:%@",interviewObj.interviewCode);
                    [self.contactDataProvider deleteInterviewEvent:interviewObj];
                }
            }
        }
        
    }
}
- (void)saveEopEvents:(NSSet *)eopEventsSet
{
    NSDictionary *dict = @{ @"agentCode" : [CachingService sharedInstance].currentUserId, @"allDay" : @0,@"eopEvents" : eopEventsSet};
    NSMutableArray *mutableEventsJSONArray = [[NSMutableArray alloc] init];
    [mutableEventsJSONArray addObject:dict];
    NSArray *calendarEventsMTLModelArray = [MTLJSONAdapter modelsOfClass:[CaledndarEventMTLModel class] fromJSONArray:mutableEventsJSONArray error:nil];
    
    
    for (CaledndarEventMTLModel *calendarEventModel in calendarEventsMTLModelArray)
    {
        TblCalendarEvent *event = [MTLManagedObjectAdapter managedObjectFromModel:calendarEventModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
        if (event) {
            [self.cachingService saveContext];
        }
    }
}

- (void)getEOPRegisteredCandidateCount:(NSString *)eventCode agentID:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:eventCode forKey:@"eventCode"];
    [parametersDict setValue:agentId forKey:@"agentId"];
//    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSEOPResigteredCountURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}


- (NSSet *)getEOPEventsMTLObjects:(id)resDict isPastEvents:(BOOL)pastEvent
{
    NSArray *eopEventsArray = resDict;
    NSError *mtlError;
    NSMutableArray *eopListArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [eopEventsArray count]; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:eopEventsArray[i]];
        NSString *dateString =  [dict objectForKey:@"eventDate"];
        NSDate *date = [NSDate stringToDate:dateString dateFormat:kServerDateFormatte];
        [dict setObject:date forKey:@"eventDate"];
        
        EOPEventsMTLModel *eopEventModel = [MTLJSONAdapter modelOfClass:[EOPEventsMTLModel class] fromJSONDictionary:dict  error:&mtlError];
        if (eopEventModel.publicUrl.length>0) {
            eopEventModel.publicUrl = [NSString stringWithFormat:@"%@%@&co=%@",eopEventModel.publicUrl,[[TblAgentDetails getCurrentAgentDetails] userID],[CachingService sharedInstance].branchCode];
        }
        if (pastEvent) {
            eopEventModel.isRegistered = YES;
        }
        [eopListArray addObject:eopEventModel];
    }
    return [NSSet setWithArray:eopListArray];
}
/*- (void)getEOPEventsMTLObjects:(id)resDict
{
    NSArray *eopEventsArray = resDict;
    NSError *mtlError;
    NSMutableArray *eopListArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [eopEventsArray count]; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:eopEventsArray[i]];

        NSString *dateString =  [dict objectForKey:@"eventDate"];

        NSDate *date = [DateUtils stringToDate:dateString dateFormat:kServerDateFormatte];
        [dict setObject:date forKey:@"eventDate"];
        EOPEventsMTLModel *eopEventModel = [MTLJSONAdapter modelOfClass:[EOPEventsMTLModel class] fromJSONDictionary:dict  error:&mtlError];
        [eopListArray addObject:eopEventModel];
    }
    for (EOPEventsMTLModel *eopEventModel in eopListArray)
    {
        EOPEventsMTLModel *event = [MTLManagedObjectAdapter managedObjectFromModel:eopEventModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
        if (event) {
            [self.cachingService saveContext];
        }
    }
    
    
    /*NSDictionary *dict = @{ @"agentCode" : [CachingService sharedInstance].currentUserId, @"allDay" : @0,@"interviewEvents" : [self.allEvnetsDict objectForKey:@"interviewEvents"],@"eopEvents" : [self.allEvnetsDict objectForKey:@"eopEvents"],@"holidayEvents" : [self.allEvnetsDict objectForKey:@"holidayEvents"],@"trainingEvents":[self.allEvnetsDict objectForKey:@"trainingEvents" ]};
    NSMutableArray *mutableEventsJSONArray = [[NSMutableArray alloc] init];
    [mutableEventsJSONArray addObject:dict];
    NSArray *calendarEventsMTLModelArray = [MTLJSONAdapter modelsOfClass:[CaledndarEventMTLModel class] fromJSONArray:mutableEventsJSONArray error:nil];
    
    
    for (CaledndarEventMTLModel *calendarEventModel in calendarEventsMTLModelArray)
    {
        TblCalendarEvent *event = [MTLManagedObjectAdapter managedObjectFromModel:calendarEventModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
        if (event) {
            [self.cachingService saveContext];
        }
    }
}*/
- (void)postCandidateRegistrationServiceCall:(NSDictionary *)eopDict agentId:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSString *relativeURL = [NSString stringWithFormat:@"%@%@&co=%@",kCMSEOPCandidateRegister,agentId,[CachingService sharedInstance].branchCode];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObject:eopDict];
    [[self.serviceManager createPostDataTaskForRequestWithVersionPath:@"" relativePath:relativeURL forBodyDictionary:array success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }] resume];
}

- (void)getEOPRegisteredCandidate:(NSString *)eventCode agentId:(NSString *)agentID success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:eventCode forKey:@"eventCode"];
    [dict setValue:agentID forKey:@"agentId"];
    [dict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSGetEOPRegisteredCandidate parametersDictionary:dict success:^(NSURLSessionDataTask *task, id responseObject){
          [self getEOPRegisteredCandidateMTLObjects:responseObject eventCode:eventCode];
          success(responseObject);
      } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
          failure(responseObject,error);
      }] resume];
}
- (void)getEOPRegisteredCandidateMTLObjects:(id)resDict eventCode:(NSString *)eventCode
{
//    NSArray *contactArray = [[DbUtils fetchAllObject:@"TblEOP"
//                                          andPredict:[NSPredicate predicateWithFormat:@"eventCode == %d",[eventCode intValue]]
//                                   andSortDescriptor:nil
//                                managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
//    NSSet *eopSet = [[NSSet alloc]initWithArray:contactArray];
    
//    [DbUtils deleteAllObjectsWithEntityName:@"TblEOPCandidate" inContext:[CachingService sharedInstance].managedObjectContext];
    
    [DbUtils deleteAllObjectsWithEntityName:@"TblEOPCandidate"
                                    predict:[NSPredicate predicateWithFormat:@"eventCode == %@",[NSNumber numberWithInteger:[eventCode integerValue]]]
                                  inContext:[CachingService sharedInstance].managedObjectContext];
    
    NSArray *registeredCandidatesArray = resDict;
    NSError *mtlError;
    NSMutableArray *registeredCandidatesListArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [registeredCandidatesArray count]; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:registeredCandidatesArray[i]];
        NSString *dateString =  [dict objectForKey:@"dob"];
        if(!isEmptyString(dateString)){
            NSDate *date = [NSDate stringToDate:dateString dateFormat:kServerDateFormatte];
            [dict setObject:date forKey:@"dob"];
        }
        
        TblEOPCandidateMTLModel *eopCandidateModel = [MTLJSONAdapter modelOfClass:[TblEOPCandidateMTLModel class] fromJSONDictionary:dict  error:&mtlError];
        [registeredCandidatesListArray addObject:eopCandidateModel];
    }
    
    for (TblEOPCandidateMTLModel *eopCandidateModel in registeredCandidatesListArray)
    {
        TblEOPCandidateMTLModel *event = [MTLManagedObjectAdapter managedObjectFromModel:eopCandidateModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
        if (event) {
            [self.cachingService saveContext];
        }
    }
    
}

- (void)getCandidateRegisteredEOP:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSGetCandidateRegisteredEOP parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}
- (void) deleteEOPRegistration:(NSString *)interviewCode candidateCode:(NSString*)candidateCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:interviewCode forKey:@"eventCode"];
    [[self.serviceManager getDataFromServiceCall:kCMSDeleteEOPRegistration parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}


#pragma mark - Interview Service Calls
/** Interview service Calls */


- (void)getAllInterviewWithCandidateCode:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSGetAllInterviewWithCandidateCode parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        NSSet *interviewSet = [self getInterviewEventsMTLObjects:responseObject isPastEvents:NO];
        success(interviewSet);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}


- (void)getAllPastInterviewWithCandidateCode:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSGetAllPastInterviewWithCandidateCode parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        NSSet *interviewSet = [self getInterviewEventsMTLObjects:responseObject isPastEvents:YES];
        success(interviewSet);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

- (void)saveInterviewEvents:(NSSet *)interviewSet
{
    NSDictionary *dict = @{ @"agentCode" : [CachingService sharedInstance].currentUserId, @"allDay" : @0,@"interviewEvents" : interviewSet};
    NSMutableArray *mutableEventsJSONArray = [[NSMutableArray alloc] init];
    [mutableEventsJSONArray addObject:dict];
    NSArray *calendarEventsMTLModelArray = [MTLJSONAdapter modelsOfClass:[CaledndarEventMTLModel class] fromJSONArray:mutableEventsJSONArray error:nil];
    
    
    for (CaledndarEventMTLModel *calendarEventModel in calendarEventsMTLModelArray)
    {
        TblCalendarEvent *event = [MTLManagedObjectAdapter managedObjectFromModel:calendarEventModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
        if (event) {
            [self.cachingService saveContext];
        }
    }
}

- (void)getInterviewRegisteredCandidateCount:(NSString *)interviewCode agentID:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:interviewCode forKey:@"interviewCode"];
    [parametersDict setValue:agentId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSInterviewResigteredCountURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

- (NSSet *)getInterviewEventsMTLObjects:(id)resDict isPastEvents:(BOOL)pastEvent
{
    
    NSArray *interviewEventsArray = resDict;
    NSError *mtlError;
    NSMutableArray *interviewListArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [interviewEventsArray count]; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:interviewEventsArray[i]];
        NSString *dateString =  [dict objectForKey:@"interviewDate"];
        NSDate *date = [NSDate stringToDate:dateString dateFormat:kServerDateFormatte];
        [dict setObject:date forKey:@"interviewDate"];
        InterviewEventsMTLModel *interviewEventModel = [MTLJSONAdapter modelOfClass:[InterviewEventsMTLModel class] fromJSONDictionary:dict  error:&mtlError];
        if (pastEvent) {
            interviewEventModel.isRegistered = YES;
        }
        [interviewListArray addObject:interviewEventModel];
    }
    return [NSSet setWithArray:interviewListArray];
}
- (void) deleteInterviewRegistration:(NSString *)interviewCode candidateCode:(NSString*)candidateCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:interviewCode forKey:@"interviewCode"];
    [[self.serviceManager getDataFromServiceCall:kCMSDeleteInterviewRegistration parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

- (void)getInterviewRegisteredCandidate:(NSString *)interviewCode agentID:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:interviewCode forKey:@"interviewCode"];
    [dict setValue:agentId forKey:@"agentId"];
    [dict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSGetInterviewRegisteredCandidate parametersDictionary:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [self getInterviewRegisteredCandidateMTLObjects:responseObject interviewCode:@"6"];
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }] resume];
}
- (void)getInterviewRegisteredCandidateMTLObjects:(id)resDict interviewCode:(NSString *)interviewCode
{
    //    NSArray *contactArray = [[DbUtils fetchAllObject:@"TblInterview"
    //                                          andPredict:[NSPredicate predicateWithFormat:@"interviewCode == %d",[interviewCode intValue]]
    //                                   andSortDescriptor:nil
    //                                managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    //    NSSet *inteviewSet = [[NSSet alloc]initWithArray:contactArray];
    
//    [DbUtils deleteAllObjectsWithEntityName:@"TblInterviewCandidate"
//                                  inContext:[CachingService sharedInstance].managedObjectContext];
    [DbUtils deleteAllObjectsWithEntityName:@"TblInterviewCandidate"
                                    predict:[NSPredicate predicateWithFormat:@"interviewCode == %d",[interviewCode intValue]]
                                  inContext:[CachingService sharedInstance].managedObjectContext];
    
    NSArray *registeredCandidatesArray = resDict;
    NSError *mtlError;
    NSMutableArray *registeredCandidatesListArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [registeredCandidatesArray count]; i++)
    {
        NSDictionary *dict1 = registeredCandidatesArray[i];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:dict1];
        NSString *dateString =  [dict objectForKey:@"dob"];
        if(!isEmptyString(dateString)){
            NSDate *date = [NSDate stringToDate:dateString dateFormat:kServerDateFormatte];
            [dict setObject:date forKey:@"dob"];
        }
        
        TblInterviewCandidateMTLModel *interviewCandidateModel = [MTLJSONAdapter modelOfClass:[TblInterviewCandidateMTLModel class] fromJSONDictionary:dict  error:&mtlError];
        [registeredCandidatesListArray addObject:interviewCandidateModel];
    }
   
    for (TblInterviewCandidateMTLModel *interviewEventModel in registeredCandidatesListArray)
    {
        TblInterviewCandidateMTLModel *event = [MTLManagedObjectAdapter managedObjectFromModel:interviewEventModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
        if (event) {
            [self.cachingService saveContext];
        }
    }
}

- (void)postInterviewCandidateRegister:(NSDictionary *)dict agentId:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSString *relativeURL = [NSString stringWithFormat:@"%@%@&co=%@",kCMSInterViewCandidateRegister,agentId,[CachingService sharedInstance].branchCode];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObject:dict];
    [[self.serviceManager createPostDataTaskForRequestWithVersionPath:@"" relativePath:relativeURL forBodyDictionary:array success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        NSLog(@"%@",error);
    }] resume];
}

#pragma mark - Training Service Calls
- (void)getTrainings:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSCCTestResultURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

/** CCTest Service Calls */
#pragma mark - CCTest Service Calls

- (void)getCCTestResult:(NSString *)agentCode candidateCode:(NSString *)candidateCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSCCTestResultURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

- (void)postCCTestResult:(NSString *)agentCode candidateCode:(NSString *)candidateCode testResult:(NSString *)testResult resultDate:(NSString *)resultDate success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:testResult forKey:@"ccTestResult"];
    [parametersDict setValue:resultDate forKey:@"ccTestResultDate"];
    [[self.serviceManager getDataFromServiceCall:kCMSPostCCTestResultURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
    
}

/**First interview */
#pragma mark - First interview Service Calls

- (void)postFirstInterview:(NSDictionary *)bodyDict success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObject:bodyDict];
    
    [[self.serviceManager createPostDataTaskForRequestWithVersionPath:@"" relativePath:kCMAPostFirstInterview forBodyDictionary:array success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }] resume];
}

- (void)getFirstInterview:(NSString *)candidateCode agentID:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSGetFirstInterview parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}



/** ALE Exam */
#pragma mark - ALE Exam Service Calls

- (void) getALEExamResults:(NSString *)candidateCode agentID:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSGetALEExamResults parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

- (void) getABCTraningResults:(NSString *)candidateCode agentID:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"recruiterAgentCode"];
    [parametersDict setValue:agentCode forKey:@"perAgentId"];
    [[self.serviceManager getDataFromServiceCall:kCMSgetABCResults parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

/** Contract Service Call */
#pragma mark - Contract  Service Calls

- (void) postContractedDetails:(NSDictionary *)dict success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObject:dict];
    
    [[self.serviceManager createPostDataTaskForRequestWithVersionPath:@"" relativePath:kCMSPushContractedDetails forBodyDictionary:array success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }] resume];
}

- (void) getContractDetail:(NSString *)candidateAgentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateAgentCode forKey:@"candidateAgentCode"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSGetContractDetail parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

/** ABC push results */
#pragma mark - ABC push Service Calls
- (void) pushABCTraining:(NSDictionary *)body success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObject:body];
    
    [[self.serviceManager createPostDataTaskForRequestWithVersionPath:@"" relativePath:kCMSABCPushResults forBodyDictionary:array success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }] resume];
}

- (void) saveAgentLoginDetails:(NSDictionary *)dict
{
    TblAgentDetails *agentDetails = [TblAgentDetails getCurrentAgentDetails];
    if (!agentDetails)
        {
        AgentMTLModel *agentDetailsMTLModel = [MTLJSONAdapter modelOfClass:[AgentMTLModel class] fromJSONDictionary:dict  error:nil];
        
        AgentMTLModel *event = [MTLManagedObjectAdapter managedObjectFromModel:agentDetailsMTLModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
        if (event) {
            [self.cachingService saveContext];
        }
    }
}

#pragma mark - EGreetings
- (void)getEGreetingsForAgentCode: (NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:agentCode forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [[self.serviceManager getDataFromServiceCall:kCMSGetEGreetingsDetail parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject) {
            NSArray *eGreetingMTLArray = [self getEGreetingsMTLObjects:responseObject];
            for (GreetingsMTLModel *greetingsMTLModel in eGreetingMTLArray)
            {
                TblGreetings *event = [MTLManagedObjectAdapter managedObjectFromModel:greetingsMTLModel insertingIntoContext:[self.cachingService managedObjectContext] error:nil];
                if (event) {
                    [self.cachingService saveContext];
                }
            }
            success([self getEGreetingsMTLObjects:responseObject]);
        }
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];

}

- (NSArray *)getEGreetingsMTLObjects:(id)resDict
{
    NSArray *greetingArray = resDict;
    NSError *mtlError;
    NSMutableArray *greetingMTLArray = [[NSMutableArray alloc]init];
    for (int i= 0; i < [greetingArray count]; i++)
    {
        NSMutableDictionary *greetingDict = [[NSMutableDictionary alloc]initWithDictionary:greetingArray[i]];
        [greetingDict setObject:[[CachingService sharedInstance] currentUserId] forKey:@"agentId"];
        
        GreetingsMTLModel *greetingMTLModel = [MTLJSONAdapter modelOfClass:[GreetingsMTLModel class] fromJSONDictionary:greetingDict  error:&mtlError];
        [greetingMTLArray addObject:greetingMTLModel];
    }
    return greetingMTLArray;
}

- (void)getCandidateInterviewStatus:(NSString *)interviewType forCandidateCode:(NSString *)candidateCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:interviewType forKey:@"interviewType"];
    [[self.serviceManager getDataFromServiceCall:kCMSCandidateInteviewStatus parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];
}

- (void)getCandidateDetailsForCandidateCode:(NSString *)candidateCode
                                    agentID:(NSString *)agentId
                                  eventCode:(NSString *)eventCode
                                    success:(CMSContactSuccessCallbackWithResponseObject)success
                                    failure:(CMSContactFailureCallback)failure {
   
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:candidateCode forKey:@"candidateCode"];
    [parametersDict setValue:agentId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [parametersDict setValue:eventCode forKey:@"interviewCode"];
    [[self.serviceManager getDataFromServiceCall:kGetCandidateDetails parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(responseObject,error);
    }]resume];

}

@end
