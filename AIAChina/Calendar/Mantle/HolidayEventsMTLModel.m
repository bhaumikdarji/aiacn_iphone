//
//  HolidayEventsMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-07-31.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "HolidayEventsMTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblCalendarEvent.h"
#import "MTLValueTransformer.h"
#import "TblHoliday.h"
@implementation HolidayEventsMTLModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblHolidayAttributes.buCode : @"buCode",
             TblHolidayAttributes.buName : @"buName",
             TblHolidayAttributes.cityCode : @"cityCode",
             TblHolidayAttributes.createdBy : @"createdBy",
             TblHolidayAttributes.creationDate : @"creationDate",
             TblHolidayAttributes.district : @"district",
             TblHolidayAttributes.endDate : @"endDate",
             TblHolidayAttributes.holidayCode : @"holidayCode",
             TblHolidayAttributes.holidayName : @"holidayName",
             TblHolidayAttributes.iconSelection : @"iconSelection",
             TblHolidayAttributes.modificationDate : @"modificationDate",
             TblHolidayAttributes.modifiedBy : @"modifiedBy",
             TblHolidayAttributes.postedBy : @"postedBy",
             TblHolidayAttributes.sscCode : @"sscCode",
             TblHolidayAttributes.startDate : @"startDate",
             TblHolidayAttributes.status : @"status",
             TblHolidayAttributes.token : @"token"
             };
}
+ (NSString *)managedObjectEntityName
{
    return [TblHoliday entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblHolidayAttributes.holidayCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{TblHolidayAttributes.buCode : TblHolidayAttributes.buCode,
             TblHolidayAttributes.buName : TblHolidayAttributes.buName,
             TblHolidayAttributes.cityCode : TblHolidayAttributes.cityCode,
             TblHolidayAttributes.createdBy : TblHolidayAttributes.createdBy,
             TblHolidayAttributes.creationDate : TblHolidayAttributes.creationDate,
             TblHolidayAttributes.district : TblHolidayAttributes.district,
             TblHolidayAttributes.endDate : TblHolidayAttributes.endDate,
             TblHolidayAttributes.holidayCode : TblHolidayAttributes.holidayCode,
             TblHolidayAttributes.holidayName : TblHolidayAttributes.holidayName,
             TblHolidayAttributes.iconSelection : TblHolidayAttributes.iconSelection,
             TblHolidayAttributes.modificationDate : TblHolidayAttributes.modificationDate,
             TblHolidayAttributes.modifiedBy : TblHolidayAttributes.modifiedBy,
             TblHolidayAttributes.postedBy : TblHolidayAttributes.postedBy,
             TblHolidayAttributes.sscCode :  TblHolidayAttributes.sscCode,
             TblHolidayAttributes.startDate : TblHolidayAttributes.startDate,
             TblHolidayAttributes.status : TblHolidayAttributes.status,
             TblHolidayAttributes.token : TblHolidayAttributes.token
             };
}

+ (NSDictionary *)relationshipModelClassesByPropertyKey
{
    return @{TblHolidayRelationships.calendarEvent : TblHolidayRelationships.calendarEvent};
}

@end
