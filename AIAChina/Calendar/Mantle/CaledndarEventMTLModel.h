//
//  CaledndarEventMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-07-29.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblInterview.h"
@interface CaledndarEventMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic,retain) NSString *agentCode;
@property (nonatomic,assign) BOOL allDay;
@property (nonatomic, strong) NSSet *interviewEvents;
@property (nonatomic, strong) NSSet *eopEvents;
@property (nonatomic, strong) NSSet *holidayEvents;
@property (nonatomic, strong) NSSet *trainingEvents;

@end
