//
//  InterviewEventsMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-07-29.
//  Copyright (c) 2015 AIA. All rights reserved.
//
/*{
 EndTime = "Jan 1, 1970 5:00:00 PM";
 StartTime = "Jan 1, 1970 9:00:00 AM";
 attendeeCount = 0;
 buCode = 1;
 buName = BU1;
 cityCode = 0;
 createdBy = 1;
 creationDate = "May 13, 2015 12:36:39 PM";
 district = 0;
 estimatedCondidates = 2;
 interviewDate = "May 23, 2015 12:00:00 AM";
 interviewMaterial = Google;
 interviewSessionName = Java;
 interviewType = 2nd;
 "interview_code" = 3;
 location = Baroda;
 modifiedBy = 0;
 organizer = H;
 registeredCount = 0;
 sscCode = 0;
 status = 1;
 token = "";
 },
 {
 EndTime = "Jan 1, 1970 5:00:00 PM";
 StartTime = "Jan 1, 1970 9:00:00 AM";
 attachmentName = "JAGANARTHA.jpg";
 attachmentPath = "INT_5dd25d76/JAGANARTHA.jpg";
 attendeeCount = 0;
 buCode = 1;
 buName = BU1;
 cityCode = 0;
 createdBy = 3;
 creationDate = "May 28, 2015 5:33:53 PM";
 district = 0;
 estimatedCondidates = 0;
 interviewDate = "May 31, 2015 12:00:00 AM";
 interviewMaterial = mat2;
 interviewSessionName = tt;
 interviewType = 2nd;
 "interview_code" = 23;
 location = India;
 modificationDate = "May 28, 2015 6:42:14 PM";
 modifiedBy = 3;
 organizer = "Jay Gagnani";
 registeredCount = 0;
 sscCode = 0;
 status = 1;
 token = 5dd25d76;
 },*/
#import "InterviewEventsMTLModel.h"
#import "TblInterview.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblCalendarEvent.h"
#import "MTLValueTransformer.h"
@implementation InterviewEventsMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{TblInterviewAttributes.interviewCode : @"interview_code",
             TblInterviewAttributes.interviewType : @"interviewType",
             TblInterviewAttributes.interviewSessionName : @"interviewSessionName",
             TblInterviewAttributes.interviewDate : @"interviewDate",
             TblInterviewAttributes.startTime: @"StartTime",
             TblInterviewAttributes.endTime : @"EndTime",
             TblInterviewAttributes.location : @"location",
             TblInterviewAttributes.interviewMaterial : @"interviewMaterial",
             TblInterviewAttributes.estimatedCondidates : @"estimatedCondidates",
             TblInterviewAttributes.buCode : @"buCode",
             TblInterviewAttributes.cityCode : @"cityCode",
             TblInterviewAttributes.sscCode : @"sscCode",
             TblInterviewAttributes.createdBy : @"createdBy",
             TblInterviewAttributes.creationDate: @"creationDate",
             TblInterviewAttributes.modifiedBy : @"modifiedBy",
             TblInterviewAttributes.status : @"status",
             TblInterviewAttributes.token : @"token",
             TblInterviewAttributes.district : @"district",
             TblInterviewAttributes.buName : @"buName",
             TblInterviewAttributes.organizer : @"organizer",
             TblInterviewAttributes.registeredCount : @"registeredCount",
             TblInterviewAttributes.attendeeCount : @"attendeeCount",
              TblInterviewAttributes.attachmentName : @"attachmentName",
              TblInterviewAttributes.attachmentPath : @"attachmentPath",
              TblInterviewAttributes.modificationDate : @"modificationDate",
             TblInterviewAttributes.isRegistered : @"isRegistered"
             
             };
}
+ (NSString *)managedObjectEntityName
{
    return [TblInterview entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblInterviewAttributes.interviewCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{TblInterviewAttributes.interviewCode : TblInterviewAttributes.interviewCode,
             TblInterviewAttributes.interviewType : TblInterviewAttributes.interviewType,
             TblInterviewAttributes.interviewSessionName : TblInterviewAttributes.interviewSessionName,
             TblInterviewAttributes.interviewDate : TblInterviewAttributes.interviewDate,
             TblInterviewAttributes.startTime: TblInterviewAttributes.startTime,
             TblInterviewAttributes.endTime : TblInterviewAttributes.endTime,
             TblInterviewAttributes.location : TblInterviewAttributes.location,
             TblInterviewAttributes.interviewMaterial :  TblInterviewAttributes.interviewMaterial,
             TblInterviewAttributes.estimatedCondidates : TblInterviewAttributes.estimatedCondidates,
             TblInterviewAttributes.buCode : TblInterviewAttributes.buCode,
             TblInterviewAttributes.cityCode : TblInterviewAttributes.cityCode,
             TblInterviewAttributes.sscCode : TblInterviewAttributes.sscCode,
             TblInterviewAttributes.createdBy : TblInterviewAttributes.createdBy,
             TblInterviewAttributes.creationDate: TblInterviewAttributes.creationDate,
             TblInterviewAttributes.modifiedBy : TblInterviewAttributes.modifiedBy,
             TblInterviewAttributes.status : TblInterviewAttributes.status,
             TblInterviewAttributes.token : TblInterviewAttributes.token,
             TblInterviewAttributes.district : TblInterviewAttributes.district,
             TblInterviewAttributes.buName : TblInterviewAttributes.buName,
             TblInterviewAttributes.organizer : TblInterviewAttributes.organizer,
             TblInterviewAttributes.registeredCount : TblInterviewAttributes.registeredCount,
             TblInterviewAttributes.attachmentName : TblInterviewAttributes.attachmentName,
             TblInterviewAttributes.attachmentPath : TblInterviewAttributes.attachmentPath,
             TblInterviewAttributes.modificationDate : TblInterviewAttributes.modificationDate,
             TblInterviewAttributes.attendeeCount :TblInterviewAttributes.attendeeCount,
             TblInterviewAttributes.isRegistered : TblInterviewAttributes.isRegistered
             };
}

+ (NSDictionary *)relationshipModelClassesByPropertyKey
{
    return @{TblInterviewRelationships.calendarEvent : TblInterviewRelationships.calendarEvent};
}
@end
