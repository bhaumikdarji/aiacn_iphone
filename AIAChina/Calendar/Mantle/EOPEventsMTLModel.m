//
//  EOPEventsMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-07-31.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EOPEventsMTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblCalendarEvent.h"
#import "MTLValueTransformer.h"
#import "TblEOP.h"
@implementation EOPEventsMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{TblEOPAttributes.attendeeCount : @"attendeeCount",
             TblEOPAttributes.agentTeam : @"agentTeam",
             TblEOPAttributes.buCode : @"buCode",
             TblEOPAttributes.buName : @"buName",
             TblEOPAttributes.cityCode : @"cityCode",
             TblEOPAttributes.completedCount : @"completedCount",
             TblEOPAttributes.createdBy : @"createdBy",
             TblEOPAttributes.creationDate : @"creationDate",
             TblEOPAttributes.district : @"district",
             TblEOPAttributes.endTime : @"endTime",
             TblEOPAttributes.eopDescription : @"eopDescription",
             TblEOPAttributes.estimatedCandidates : @"estimatedCandidates",
             TblEOPAttributes.eventCode : @"event_code",
             TblEOPAttributes.eventDate : @"eventDate",
             TblEOPAttributes.eventDateStr : @"eventDateStr",
             TblEOPAttributes.eventName : @"eventName",
             TblEOPAttributes.eventType : @"eventType",
             TblEOPAttributes.location : @"location",
             TblEOPAttributes.modifiedBy : @"modifiedBy",
             TblEOPAttributes.openTo : @"openTo",
             TblEOPAttributes.openToRegistration : @"openToRegistration",
             TblEOPAttributes.openToSpGrp : @"openToSpGrp",
             TblEOPAttributes.oraganiserStr : @"oraganiserStr",
             TblEOPAttributes.organizer : @"organizer",
             TblEOPAttributes.profilePath : @"profilePath",
             TblEOPAttributes.publicUrl : @"publicUrl",
             TblEOPAttributes.registeredCount : @"registeredCount",
             TblEOPAttributes.speaker : @"speaker",
             TblEOPAttributes.sscCode : @"sscCode",
             TblEOPAttributes.startTime : @"startTime",
             TblEOPAttributes.status : @"status",
             TblEOPAttributes.timeEnd : @"timeEnd",
             TblEOPAttributes.timeStart : @"timeStart",
             TblEOPAttributes.token : @"token",
             TblEOPAttributes.topic : @"topic",
             TblEOPAttributes.topicPath : @"topicPath",
             TblEOPAttributes.userName : @"userName",
             TblEOPAttributes.isRegistered : @"isRegistered"
             };
}
+ (NSString *)managedObjectEntityName
{
    return [TblEOP entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblEOPAttributes.eventCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{TblEOPAttributes.attendeeCount : TblEOPAttributes.attendeeCount,
             TblEOPAttributes.agentTeam : TblEOPAttributes.agentTeam,
             TblEOPAttributes.buCode : TblEOPAttributes.buCode,
             TblEOPAttributes.buName : TblEOPAttributes.buName,
             TblEOPAttributes.cityCode : TblEOPAttributes.cityCode,
             TblEOPAttributes.completedCount : TblEOPAttributes.completedCount,
             TblEOPAttributes.createdBy : TblEOPAttributes.createdBy,
             TblEOPAttributes.creationDate : TblEOPAttributes.creationDate,
             TblEOPAttributes.district : TblEOPAttributes.district,
             TblEOPAttributes.endTime : TblEOPAttributes.endTime,
             TblEOPAttributes.eopDescription : TblEOPAttributes.eopDescription,
             TblEOPAttributes.estimatedCandidates : TblEOPAttributes.estimatedCandidates,
             TblEOPAttributes.eventCode : TblEOPAttributes.eventCode,
             TblEOPAttributes.eventDate : TblEOPAttributes.eventDate,
             TblEOPAttributes.eventDateStr : TblEOPAttributes.eventDateStr,
             TblEOPAttributes.eventName : TblEOPAttributes.eventName,
             TblEOPAttributes.eventType : TblEOPAttributes.eventType,
             TblEOPAttributes.location : TblEOPAttributes.location,
             TblEOPAttributes.modifiedBy : TblEOPAttributes.modifiedBy,
             TblEOPAttributes.openTo : TblEOPAttributes.openTo,
             TblEOPAttributes.openToRegistration : TblEOPAttributes.openToRegistration,
             TblEOPAttributes.openToSpGrp : TblEOPAttributes.openToSpGrp,
             TblEOPAttributes.oraganiserStr :TblEOPAttributes.oraganiserStr,
             TblEOPAttributes.organizer :TblEOPAttributes.organizer,
             TblEOPAttributes.profilePath : TblEOPAttributes.profilePath,
             TblEOPAttributes.publicUrl : TblEOPAttributes.publicUrl,
             TblEOPAttributes.registeredCount : TblEOPAttributes.registeredCount,
             TblEOPAttributes.speaker : TblEOPAttributes.speaker,
             TblEOPAttributes.sscCode : TblEOPAttributes.sscCode,
             TblEOPAttributes.startTime : TblEOPAttributes.startTime,
             TblEOPAttributes.status : TblEOPAttributes.status,
             TblEOPAttributes.timeEnd : TblEOPAttributes.timeEnd,
             TblEOPAttributes.timeStart : TblEOPAttributes.timeStart,
             TblEOPAttributes.token : TblEOPAttributes.token,
             TblEOPAttributes.topic : TblEOPAttributes.topic,
             TblEOPAttributes.userName : TblEOPAttributes.userName,
             TblEOPAttributes.topicPath : TblEOPAttributes.topicPath,
             TblEOPAttributes.isRegistered : TblEOPAttributes.isRegistered
             };
}

+ (NSDictionary *)relationshipModelClassesByPropertyKey
{
    return @{TblEOPRelationships.calendarEvent : TblEOPRelationships.calendarEvent};
}
@end
