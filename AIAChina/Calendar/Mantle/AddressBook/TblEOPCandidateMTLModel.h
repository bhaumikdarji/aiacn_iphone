//
//  TblEOPCandidateMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-08.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblEOPCandidate.h"
#import "MTLValueTransformer.h"
@interface TblEOPCandidateMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, strong) NSNumber *buCode;
@property (nonatomic, strong) NSNumber *candidateCode;
@property (nonatomic, strong) NSString *cityCode;
@property (nonatomic, strong) NSNumber *districtCode;
@property (nonatomic, strong) NSString *eventCandidateCode;
@property (nonatomic, strong) NSNumber *eventCode;
@property (nonatomic, strong) NSString *sscCode;
@property (nonatomic, strong) NSNumber *attendedEventCount;
@property (nonatomic, strong) NSDate *dob;

@property (nonatomic, assign) BOOL status;
@property (nonatomic, strong) NSString *agencyLeaderCode;
@property (nonatomic, strong) NSString *agencyLeaderName;
@property (nonatomic, strong) NSString *agentName;
@property (nonatomic, strong) NSString *buName;
@property (nonatomic, strong) NSString *candidateName;
@property (nonatomic, strong) NSString *completeStatus;
@property (nonatomic, strong) NSString *contactNumber;
@property (nonatomic, strong) NSString *distName;
@property (nonatomic, strong) NSString *dobStr;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *servicingAgent;
@property (nonatomic, strong) NSString *sourceOfReferal;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *attendedStatus;

@end
