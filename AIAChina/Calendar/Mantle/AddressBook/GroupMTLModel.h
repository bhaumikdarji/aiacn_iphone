//
//  GroupMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblGroup.h"
#import "MTLValueTransformer.h"
@interface GroupMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic , retain) NSString *agentId;
@property (nonatomic , retain) NSString *groupDescription;
@property (nonatomic , retain) NSString *groupName;
@property (nonatomic , retain) NSString *iosAddressCode;
@property (nonatomic , retain) NSData *image;
@end
