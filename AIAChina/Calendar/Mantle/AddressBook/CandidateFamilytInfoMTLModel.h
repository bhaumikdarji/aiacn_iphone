//
//  CandidateFamilytInfoMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblFamily.h"
#import "MTLValueTransformer.h"
@interface CandidateFamilytInfoMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSString *iosAddressCode;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *occupation;
@property (nonatomic, strong) NSString *phoneNo;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSString *relationship;
@property (nonatomic, strong) NSString *unit;
@end
