//
//  TblEOPCandidateMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-08.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "MTLValueTransformer.h"

@interface TblTrainingMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>

@property (nonatomic, strong) NSNumber *detailCode;
@property (nonatomic, strong) NSString *courseCode;
@property (nonatomic, strong) NSString *curriculamCode;
@property (nonatomic, strong) NSString *courseName;
@property (nonatomic, strong) NSString *courseType;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSString *creationDate;

- (void)updateTrainingDetails;
@end
