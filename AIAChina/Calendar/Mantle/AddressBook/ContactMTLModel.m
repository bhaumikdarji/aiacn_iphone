//
//  ContactMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "ContactMTLModel.h"
#import "TblContact.h"
#import "TblEducation.h"
@implementation ContactMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblContactAttributes.addressCode : @"addressCode",
             TblContactAttributes.age : @"age",
             TblContactAttributes.candidatePhoto : @"candidatePhoto",
             TblContactAttributes.presenterReport : @"presenterImg",
            TblContactAttributes.presenterReportDate : @"presenterDate",
             TblContactAttributes.agentId : @"agentId",
             TblContactAttributes.birthDate : @"birthDate",
             TblContactAttributes.birthPlace : @"birthPlace",
             TblContactAttributes.birthMonthDay : @"birthMonthDay",
             TblContactAttributes.birthYear : @"birthYear",
             TblContactAttributes.createdBy : @"createdBy",
             TblContactAttributes.creationDate : @"creationDate",
             TblContactAttributes.eMailId : @"eMailId",
             TblContactAttributes.education : @"education",
             TblContactAttributes.fixedLineNO : @"fixedLineNO",
             TblContactAttributes.gender : @"gender",
             TblContactAttributes.groupName : @"groupName",
             TblContactAttributes.identityType : @"idType",
             TblContactAttributes.iosAddressCode : @"iosAddressCode",
             TblContactAttributes.isDelete : @"deleteStatus",
             TblContactAttributes.isSync : @"isSync",
            TblContactAttributes.lastContactedDate : @"lastContactedDate",
             TblContactAttributes.marritalStatus : @"marritalStatus",
             TblContactAttributes.mobilePhoneNo : @"mobilePhoneNo",
             TblContactAttributes.modificationDate : @"modificationDate",
             TblContactAttributes.modifiedBy : @"modifiedBy",
             TblContactAttributes.name : @"name",
             TblContactAttributes.natureOfBusiness : @"natureOfBusiness",
             TblContactAttributes.nric : @"nric",
             TblContactAttributes.officePhoneNo : @"officePhoneNo",
             TblContactAttributes.addressCode : @"addressCode",
             TblContactAttributes.outlookApperance : @"outlookApperance",
             TblContactAttributes.registeredPostalCode : @"registeredPostalCode",
             TblContactAttributes.presentWorkCondition : @"presentWorkCondition",
             TblContactAttributes.purchasedAnyInsurance : @"purchasedAnyInsurance",
             TblContactAttributes.qq : @"qq",
             TblContactAttributes.race : @"race",
             TblContactAttributes.reJoin : @"reJoin",
             TblContactAttributes.recommendedRecruitmentScheme : @"recommendedRecruitmentScheme",
             TblContactAttributes.recruitedBy : @"recruitedBy",
             TblContactAttributes.recruitmentProgressStatus : @"recruitmentProgressStatus",
             TblContactAttributes.referalSource : @"referalSource",
             TblContactAttributes.remarks : @"remarks",
             TblContactAttributes.salesExperience : @"salesExperience",
             TblContactAttributes.talentProfile : @"talentProfile",
             TblContactAttributes.weChat : @"weChat",
             TblContactAttributes.weibo : @"weibo",
             TblContactAttributes.workingYearExperience : @"workingYearExperience",
             TblContactAttributes.yearlyIncome : @"yearlyIncome",
             TblContactRelationships.group : @"candidateGroups",
             TblContactAttributes.registeredAddress : @"registeredAddress1",
             TblContactAttributes.registeredAddress1 : @"registeredAddress2",
             TblContactAttributes.registeredAddress2 : @"registeredAddress3",
             TblContactAttributes.newcomerSource : @"newComerSource",
             TblContactAttributes.residentialPostalCode : @"residentialPostalCode",
             TblContactAttributes.residentialAddress2 : @"residentialAddress3",
             TblContactAttributes.residentialAddress : @"residentialAddress1",
             TblContactAttributes.residentialAddress1 : @"residentialAddress2",
             TblContactRelationships.candidateFamilyInfos : @"candidateFamilyInfos",
             TblContactRelationships.candidateEducations : @"candidateEducations",
             TblContactRelationships.candidateESignatures :@"candidateESignatures",
             TblContactRelationships.candidateProfessionalCertifications : @"candidateProfessionalCertifications",
             TblContactRelationships.candidateWorkExperiences : @"candidateWorkExperiences",
             TblContactRelationships.candidateNotes : @"candidateNotes"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblContact entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblContactAttributes.iosAddressCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
            TblContactAttributes.registeredAddress : TblContactAttributes.registeredAddress,
            TblContactAttributes.registeredAddress1 : TblContactAttributes.registeredAddress1,
            TblContactAttributes.registeredAddress2 : TblContactAttributes.registeredAddress2,
            TblContactAttributes.identityType : TblContactAttributes.identityType,
            TblContactAttributes.residentialPostalCode : TblContactAttributes.residentialPostalCode,
            TblContactAttributes.residentialAddress : TblContactAttributes.residentialAddress,
            TblContactAttributes.residentialAddress2 : TblContactAttributes.residentialAddress2,
            TblContactAttributes.residentialAddress1 : TblContactAttributes.residentialAddress1,
            TblContactAttributes.presenterReport : TblContactAttributes.presenterReport,
            TblContactAttributes.presenterReportDate : TblContactAttributes.presenterReportDate,
             TblContactAttributes.addressCode : TblContactAttributes.addressCode,
            TblContactAttributes.newcomerSource : TblContactAttributes.newcomerSource,
             TblContactAttributes.age : TblContactAttributes.age,
             TblContactAttributes.agentId : TblContactAttributes.agentId,
             TblContactAttributes.birthDate : TblContactAttributes.birthDate,
             TblContactAttributes.birthPlace : TblContactAttributes.birthPlace,
             TblContactAttributes.birthMonthDay : TblContactAttributes.birthMonthDay,
             TblContactAttributes.birthYear : TblContactAttributes.birthYear,
            TblContactAttributes.createdBy : TblContactAttributes.createdBy,
             TblContactAttributes.creationDate : TblContactAttributes.creationDate,
             TblContactAttributes.eMailId : TblContactAttributes.eMailId,
             TblContactAttributes.education : TblContactAttributes.education,
             TblContactAttributes.fixedLineNO :TblContactAttributes.fixedLineNO,
             TblContactAttributes.gender : TblContactAttributes.gender,
             TblContactAttributes.groupName : TblContactAttributes.groupName,
             TblContactAttributes.idType : TblContactAttributes.idType,
             TblContactAttributes.iosAddressCode : TblContactAttributes.iosAddressCode,
             TblContactAttributes.isDelete :TblContactAttributes.isDelete ,
             TblContactAttributes.isSync : TblContactAttributes.isSync,
             TblContactAttributes.lastContactedDate : TblContactAttributes.lastContactedDate,
             TblContactAttributes.marritalStatus : TblContactAttributes.marritalStatus,
             TblContactAttributes.mobilePhoneNo : TblContactAttributes.mobilePhoneNo,
             TblContactAttributes.modificationDate : TblContactAttributes.modificationDate,
             TblContactAttributes.modifiedBy : TblContactAttributes.modifiedBy,
             TblContactAttributes.name : TblContactAttributes.name ,
             TblContactAttributes.natureOfBusiness : TblContactAttributes.natureOfBusiness,
             TblContactAttributes.nric : TblContactAttributes.nric,
             TblContactAttributes.officePhoneNo : TblContactAttributes.officePhoneNo,
             TblContactAttributes.addressCode : TblContactAttributes.addressCode,
             TblContactAttributes.outlookApperance : TblContactAttributes.outlookApperance,
             TblContactAttributes.registeredPostalCode : TblContactAttributes.registeredPostalCode,
             TblContactAttributes.presentWorkCondition : TblContactAttributes.presentWorkCondition,
             TblContactAttributes.purchasedAnyInsurance : TblContactAttributes.purchasedAnyInsurance,
             TblContactAttributes.qq : TblContactAttributes.qq,
             TblContactAttributes.race : TblContactAttributes.race,
             TblContactAttributes.reJoin :TblContactAttributes.reJoin,
             TblContactAttributes.recommendedRecruitmentScheme :TblContactAttributes.recommendedRecruitmentScheme,
             TblContactAttributes.recruitedBy : TblContactAttributes.recruitedBy,
             TblContactAttributes.recruitmentProgressStatus : TblContactAttributes.recruitmentProgressStatus,
             TblContactAttributes.referalSource : TblContactAttributes.referalSource,
             TblContactAttributes.remarks :TblContactAttributes.remarks,
             TblContactAttributes.salesExperience : TblContactAttributes.salesExperience,
             TblContactAttributes.talentProfile : TblContactAttributes.talentProfile,
             TblContactAttributes.weChat : TblContactAttributes.weChat,
             TblContactAttributes.weibo : TblContactAttributes.weibo,
             TblContactAttributes.workingYearExperience :  TblContactAttributes.workingYearExperience,
             TblContactAttributes.yearlyIncome : TblContactAttributes.yearlyIncome,
            TblContactAttributes.candidatePhoto : TblContactAttributes.candidatePhoto
             };
}

+ (NSDictionary *)relationshipModelClassesByPropertyKey
{
    return @{
             TblContactRelationships.group : TblContactRelationships.group,
             TblContactRelationships.candidateFamilyInfos : TblContactRelationships.candidateFamilyInfos,
             TblContactRelationships.candidateWorkExperiences : TblContactRelationships.candidateWorkExperiences,
             TblContactRelationships.candidateProfessionalCertifications : TblContactRelationships.candidateProfessionalCertifications,
             TblContactRelationships.candidateEducations : TblContactRelationships.candidateEducations,
             TblContactRelationships.candidateESignatures : TblContactRelationships.candidateESignatures,
             TblContactRelationships.candidateNotes : TblContactRelationships.candidateNotes
             };
}
@end
