//
//  TblInterviewCandidateMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-08.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblInterviewCandidate.h"
#import "MTLValueTransformer.h"
#import "TblInterview.h"
@interface TblInterviewCandidateMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSNumber *candidateCode;
@property (nonatomic, strong) NSNumber *interviewCode;
@property (nonatomic, strong) NSString *candidateName;
@property (nonatomic, strong) NSString *servicingAgent;
@property (nonatomic, strong) NSString *agentName;

@property (nonatomic, strong) NSNumber *buCode;
@property (nonatomic, strong) NSString *cityCode;
@property (nonatomic, strong) NSString *sscCode;
@property (nonatomic, strong) NSNumber *districtCode;
@property (nonatomic, strong) NSString *nric;
//
@property (nonatomic, strong) NSString *buName;
@property (nonatomic, strong) NSString *distName;
@property (nonatomic, strong) NSString *agencyLeaderCode;
@property (nonatomic, strong) NSString *agencyLeaderName;
@property (nonatomic, strong) NSString *sourceOfReferal;
//
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, strong) NSNumber *p100;
@property (nonatomic, strong) NSDate *dob;
@property (nonatomic, strong) NSString *dobStr;
@property (nonatomic, strong) NSString *gender;
//
@property (nonatomic, strong) NSString *contactNumber;
@property (nonatomic, strong) NSString *ccTestResult;
@property (nonatomic, strong) NSString *recruitmentScheme;
@property (nonatomic, strong) NSString *interviewResult;
@property (nonatomic, strong) NSString *remarks;
@property (nonatomic, strong) NSString *passedStatus;

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *fileNameList;
@property (nonatomic, assign) BOOL status;
//@property (nonatomic, strong) NSSet *interview;
@end
