//
//  CandidateFamilytInfoMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CandidateFamilytInfoMTLModel.h"

@implementation CandidateFamilytInfoMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblFamilyAttributes.iosAddressCode : @"iosAddressCode",
             TblFamilyAttributes.name : @"name",
             TblFamilyAttributes.occupation : @"occupation",
             TblFamilyAttributes.phoneNo : @"phoneNo",
             TblFamilyAttributes.relationship : @"relationship",
             TblFamilyAttributes.unit : @"unit",
             TblFamilyAttributes.position : @"position"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblFamily entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblFamilyAttributes.iosAddressCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblFamilyAttributes.iosAddressCode : TblFamilyAttributes.iosAddressCode,
             TblFamilyAttributes.name : TblFamilyAttributes.name,
             TblFamilyAttributes.occupation : TblFamilyAttributes.occupation,
             TblFamilyAttributes.position : TblFamilyAttributes.position,
             TblFamilyAttributes.relationship : TblFamilyAttributes.relationship,
             TblFamilyAttributes.unit : TblFamilyAttributes.unit,
             TblFamilyAttributes.phoneNo : TblFamilyAttributes.phoneNo
             };
}
@end
