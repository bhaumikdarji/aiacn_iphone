//
//  PersonalCertificationMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblPersonalCertification.h"
#import "MTLValueTransformer.h"
@interface PersonalCertificationMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSString *certificateName;
@property (nonatomic, strong) NSString *charterAgency;
@property (nonatomic, strong) NSString *charterDate;
@property (nonatomic, strong) NSString *iosAddressCode;

@end
