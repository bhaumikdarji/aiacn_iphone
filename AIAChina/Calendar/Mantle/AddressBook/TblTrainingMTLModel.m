//
//  TblTrainingMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-08.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "TblTrainingMTLModel.h"
#import "TblTraninigEvent.h"

@implementation TblTrainingMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblTraninigEventAttributes.courseCode:@"courseCode",
             TblTraninigEventAttributes.courseName:@"courseName",
             TblTraninigEventAttributes.detailCode:@"detailCode",
             TblTraninigEventAttributes.curriculamCode:@"curriculamCode",
             TblTraninigEventAttributes.courseType:@"courseType",
             TblTraninigEventAttributes.startDate:@"startDate",
             TblTraninigEventAttributes.creationDate:@"creationDate"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblTraninigEvent entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblTraninigEventAttributes.courseCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblTraninigEventAttributes.detailCode:TblTraninigEventAttributes.detailCode,
             TblTraninigEventAttributes.courseCode:TblTraninigEventAttributes.courseCode,
             TblTraninigEventAttributes.courseName:TblTraninigEventAttributes.courseName,
             TblTraninigEventAttributes.curriculamCode:TblTraninigEventAttributes.curriculamCode,
             TblTraninigEventAttributes.courseType:TblTraninigEventAttributes.courseType,
             TblTraninigEventAttributes.startDate:TblTraninigEventAttributes.startDate,
             TblTraninigEventAttributes.creationDate:TblTraninigEventAttributes.creationDate

             };
}

- (void)updateTrainingDetails
{
    NSMutableString *courseType = [NSMutableString string];
    if ([self.courseType rangeOfString:@"1"].location != NSNotFound) {
        if (courseType.length > 0) {
            [courseType appendString:@","];
        }
        [courseType appendString:@"SA"];
    }
    
    if ([self.courseType rangeOfString:@"2"].location != NSNotFound)
    {
        if (courseType.length > 0) {
            [courseType appendString:@","];
        }
        [courseType appendString:@"HA"];
    }
    if(courseType.length == 0) {
        courseType  = self.courseType;
    }
    self.courseName = [NSString stringWithFormat:@"%@ %@",courseType,@"岗前培训班"];
}

@end
