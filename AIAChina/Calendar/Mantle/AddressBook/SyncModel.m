//
//  SyncModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-13.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "SyncModel.h"
#import "Utility.h"
#import "TblFamily.h"
#import "TblPersonalCertification.h"
#import "TblExperience.h"
#import "TblEducation.h"
#import "TblESignature.h"
#import "TblNotes.h"
#import "NSString+isNumeric.h"
/**
 Contact constants
 
 "residentialAddress1": "address",
 "residentialAddress2": "address1",
 "residentialAddress3": "address2",
 "residentialPostalCode": "391110",
 "registeredAddress1": "address",
 "registeredAddress2": "address1",
 "registeredAddress3": "address2",
 "registeredPostalCode": "391110",
*/
#define kAddressCode @"addressCode"
#define kIosAddressCode @"iosAddressCode"
#define kAgentId @"agentId"
#define kBranchCode @"co"
#define kName @"name"
#define kGender @"gender"
#define kBirthDate @"birthDate"
#define kCreatedBy @"createdBy"
#define kCreationDate @"creationDate"
#define kModifiedBy @"modifiedBy"
#define kModificationDate @"modificationDate"
#define kRecruitmentProgressStatus @"recruitmentProgressStatus"
#define kReferalSource @"referalSource"
#define kGroup @"group"
#define kAddress @"registeredAddress1"
#define kAddress1 @"registeredAddress2"
#define kAddress2 @"registeredAddress3"
#define kPostalCode @"registeredPostalCode"

#define kResidentialAddress @"residentialAddress1"
#define kResidentialAddress1 @"residentialAddress2"
#define kResidentialAddress2 @"residentialAddress3"
#define kResidentialPostalCode @"residentialPostalCode"


#define kFixedLineNO @"fixedLineNO"
#define kMobilePhoneNo @"mobilePhoneNo"

#define kOfficePhoneNo @"officePhoneNo"
#define kMarritalStatus @"marritalStatus"
#define kEducation @"education"

#define kWorkingYearExperience @"workingYearExperience"
#define kYearlyIncome @"yearlyIncome"
#define kPresentWorkCondition @"presentWorkCondition"

#define kPurchasedAnyInsurance @"purchasedAnyInsurance"
#define kOutlookApperance @"outlookApperance"
#define kRecommendedRecruitmentScheme @"recommendedRecruitmentScheme"

#define kSalesExperience @"salesExperience"
#define kRemarks @"remarks"
#define kWeibo @"weibo"

#define kWeChat @"weChat"
#define kIdType @"idType"
#define kAge @"age"
#define kQRCode @"qrCode"

#define kBirthPlace @"birthPlace"
#define kNewComerSource @"newComerSource"
#define kEMailId @"eMailId"
#define kRace @"race"

#define kReJoin @"reJoin"
#define kQQ @"qq"
#define kLastContactedDate @"lastContactedDate"

#define kRecruitedBy @"recruitedBy"
#define kNric @"nric"
#define kTalentProfile @"talentProfile"

#define kNatureOfBusiness @"natureOfBusiness"
#define kDeleteStatus @"deleteStatus"
#define kBranchCode @"co"

#define kCandidatePhoto @"candidatePhoto"
#define kCandidateAgentCode @"candidateAgentCode"

#define kRecruitmentType @"recruitmentType"
#define kContractDate @"contractDate"

/**
 candidateFamilyInfos constants
 */
#define kFamilyMemberName @"name"
#define kFamilyMemberUnit @"unit"
#define kFamilyMemberPosition @"position"
#define kFamilyMemberRelationship @"relationship"
#define kFamilyMemberOccupation @"occupation"
#define kFamilyMemberPhoneNo @"phoneNo"

/**
 candidateWorkExperiences constants
 */
#define kWorkExperienceStartDate @"startDate"
#define kWorkExperienceendDate @"endDate"
#define kWorkExperiencePosition @"position"
#define kWorkExperienceOccupation @"occupation"
#define kWorkExperienceUnit @"unit"
#define kWorkExperienceWitness @"witness"
#define kWorkExperienceWitnessContactNo @"witnessContactNo"
#define kWorkExperienceIncome @"income"

/**
 candidateEducations constants
 */

#define kEducationStartDate @"startDate"
#define kEducationEndDate @"endDate"
#define kEducationEducation @"education"
#define kEducationSchool @"school"
#define kEducationEducationLevel @"educationLevel"
#define kEducationWitness @"witness"
#define kEducationWitnessContactNo @"witnessContactNo"

/**
 candidateProfessionalCertifications constants
 */
#define kProfessionalCertificationsCertificateName @"certificateName"
#define kProfessionalCertificationsCharterAgency @"charterAgency"
#define kProfessionalCertificationsCharterDate @"charterDate"

/**
 ESignature Constants
 */

#define kESignatureBranch @"branch"
#define kESignatureserviceDepartment @"serviceDepartment"
#define kESignaturecity @"city"
#define kESignatureagentId @"agentId"
#define kESignatureatachedWithInsuranceCo @"atachedWithInsuranceCo"
#define kESignaturecontactWithAia @"contactWithAia"
#define kESignaturetakenPspTest @"takenPspTest"
#define kESignaturecandidateName @"candidateName"
#define kESignatureapplicationDate @"applicationDate"
#define kESignatureeSignaturePhoto @"eSignaturePhoto"
#define kInteractivePresenterReport @"presenterImg"
#define kInteractivePresenterDate @"presenterDate"



@implementation SyncModel
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.serviceManager = [[CMSServiceManager alloc]init];
        self.cachingService = [CachingService sharedInstance];
        self.contactDataModel = [ContactEventModel sharedInstance];
        
    }
    return self;
}
- (void) syncObjectsIntoDatabase:(NSString *)agentCode success:(CMSContactSyncSuccessCallback)success failure:(CMSContactSyncSuccessCallback)failure
{
    NSArray *contactArray = [[DbUtils fetchAllObject:@"TblContact"
                                          andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isSync == 1",[CachingService sharedInstance].currentUserId]
                                   andSortDescriptor:nil
                                managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    NSMutableArray *newContactArray = [[NSMutableArray alloc]init];
    NSMutableArray *newnotesArray = [NSMutableArray array];
    for (TblContact *contact in contactArray)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        if (contact.addressCode != nil) {
            [dict setValue:contact.addressCode forKey:kAddressCode];
        }
        else
        {
            [dict setValue:[NSNumber numberWithInt:0] forKey:@"addressCode"];
        }
        if (contact.iosAddressCode != nil) {
            [dict setValue:contact.iosAddressCode forKey:kIosAddressCode];
        }
        if (contact.agentId != nil) {
            [dict setValue:contact.agentId forKey:kAgentId];
        }
        
        if (contact.agentId != nil) {
            [dict setValue:[NSString stringWithFormat:@
            "%@",[CachingService sharedInstance].branchCode] forKey:kBranchCode];
        }
        
        if (contact.name != nil) {
            [dict setValue:contact.name forKey:kName];
        }
        if (contact.modificationDate != nil) {
            NSString *modificationDateString = [NSDate dateToString:contact.modificationDate andFormate:kServerDateFormatte];

            [dict setValue:modificationDateString forKey:kModificationDate];
        }
        if (contact.gender != nil)
        {
        
            [dict setValue:[contact.gender getTrimmedValue] forKey:kGender];
        }

        if (contact.birthDate != nil) {
            NSString *dob = [DateUtils dateToString:contact.birthDate andFormate:kServerDateFormatte];
            [dict setValue:dob forKey:kBirthDate];
        }
        if (contact.createdBy != nil) {
            [dict setValue:contact.createdBy forKey:kCreatedBy];
        }
        if (contact.creationDate != nil) {
             NSString *creationDateString = [NSDate dateToString:contact.creationDate andFormate:kServerDateFormatte];
            [dict setValue:creationDateString forKey:kCreationDate];
        }
        if (contact.modifiedBy != nil) {
            [dict setValue:contact.modifiedBy forKey:kModifiedBy];
        }
        if (contact.recruitmentProgressStatus != nil) {
            [dict setValue:contact.recruitmentProgressStatus forKey:kRecruitmentProgressStatus];
        }
        if (contact.referalSource != nil) {
            [dict setValue:contact.referalSource forKey:kReferalSource];
        }
        if (contact.groupName != nil) {
            [dict setValue:contact.groupName forKey:kGroup];
        }
        if (contact.registeredAddress != nil) {
            [dict setValue:contact.registeredAddress forKey:kAddress];
        }
        if (contact.registeredAddress1 != nil) {
            [dict setValue:contact.registeredAddress1 forKey:kAddress1];
        }
        if (contact.registeredAddress2 != nil) {
            [dict setValue:contact.registeredAddress2 forKey:kAddress2];
        }
        if (contact.residentialAddress != nil) {
            [dict setValue:contact.residentialAddress forKey:kResidentialAddress];
        }
        if (contact.residentialAddress1 != nil) {
            [dict setValue:contact.residentialAddress1 forKey:kResidentialAddress1];
        }
        if (contact.residentialAddress2 != nil) {
            [dict setValue:contact.residentialAddress2 forKey:kResidentialAddress2];
        }
//        if (contact.registeredAddress1 != nil) {
//            [dict setValue:contact.registeredAddress forKey:kAddress1];
//        }
//        if (contact.registeredAddress2 != nil) {
//            [dict setValue:contact.registeredAddress2 forKey:kAddress2];
//        }
        if (contact.registeredPostalCode != nil) {
            [dict setValue:contact.registeredPostalCode forKey:kPostalCode];
        }
        if (contact.residentialPostalCode != nil) {
            [dict setValue:contact.residentialPostalCode forKey:kResidentialPostalCode];
        }
        if (contact.fixedLineNO != nil) {
            [dict setValue:contact.fixedLineNO forKey:kFixedLineNO];
        }
        if (contact.mobilePhoneNo != nil) {
            [dict setValue:contact.mobilePhoneNo forKey:kMobilePhoneNo];
        }
        if (contact.officePhoneNo != nil) {
            [dict setValue:contact.officePhoneNo forKey:kOfficePhoneNo];
        }
        if (contact.marritalStatus != nil) {
            [dict setValue:contact.marritalStatus forKey:kMarritalStatus];
        }
        if (contact.education != nil) {
            [dict setValue:contact.education forKey:kEducation];
        }
        if (contact.workingYearExperience != nil) {
            [dict setValue:contact.workingYearExperience forKey:kWorkingYearExperience];
        }
        if (contact.yearlyIncome != nil) {
            [dict setValue:contact.yearlyIncome forKey:kYearlyIncome];
        }
        if (contact.presentWorkCondition != nil) {
            [dict setValue:contact.presentWorkCondition forKey:kPresentWorkCondition];
        }
        
        if (contact.purchasedAnyInsurance != nil) {
            [dict setValue:contact.purchasedAnyInsurance forKey:kPurchasedAnyInsurance];
        }
        if (contact.outlookApperance != nil) {
            [dict setValue:contact.outlookApperance forKey:kOutlookApperance];
        }
        if (contact.recommendedRecruitmentScheme != nil) {
            [dict setValue:contact.recommendedRecruitmentScheme forKey:kRecommendedRecruitmentScheme];
        }
        
        if (contact.salesExperience != nil) {
            [dict setValue:contact.salesExperience forKey:kSalesExperience];
        }
        if (contact.remarks != nil) {
            [dict setValue:contact.remarks forKey:kRemarks];
        }
        if (contact.weibo != nil) {
            [dict setValue:contact.weibo forKey:kWeibo];
        }
        
        if (contact.weChat != nil) {
            [dict setValue:contact.weChat forKey:kWeChat];
        }
        if (contact.identityType != nil) {
            [dict setValue:contact.identityType forKey:kIdType];
        }
//        if (contact.age != nil) {
//            [dict setValue:contact.age forKey:kAge];
//        }
        
        
        if (contact.birthPlace != nil) {
            [dict setValue:contact.birthPlace forKey:kBirthPlace];
        }
        if (contact.newcomerSource != nil) {
            [dict setValue:contact.newcomerSource forKey:kNewComerSource];
        }
        if (contact.eMailId != nil) {
            [dict setValue:contact.eMailId forKey:kEMailId];
        }
        if (contact.race != nil) {
            [dict setValue:contact.race forKey:kRace];
        }
        
        if (contact.reJoin != nil) {
            [dict setValue:contact.reJoin forKey:kReJoin];
        }
        if (contact.qq != nil) {
            [dict setValue:contact.qq forKey:kQQ];
        }
        if (contact.lastContactedDate != nil) {

            NSString *lastContactedDateString = [NSDate dateToString:contact.lastContactedDate andFormate:kServerDateFormatte];
            [dict setValue:lastContactedDateString forKey:kLastContactedDate];
        }
        if (contact.recruitedBy != nil) {
            [dict setValue:contact.recruitedBy forKey:kRecruitedBy];
        }
        if (contact.talentProfile != nil) {
            [dict setValue:contact.talentProfile forKey:kTalentProfile];
        }
        if (contact.nric != nil) {
            [dict setValue:contact.nric forKey:kNric];
        }
        if (contact.natureOfBusiness != nil) {
            [dict setValue:contact.natureOfBusiness forKey:kNatureOfBusiness];
        }
        if (contact.candidatePhoto != nil) {
            [dict setValue:[self imageToBase64EncodedString:contact.candidatePhoto] forKey:kCandidatePhoto];
        }
        
        if (contact.presenterReport != nil) {
            [dict setValue:[self imageToBase64EncodedString:contact.presenterReport] forKey:kInteractivePresenterReport];
        }
        
        if (contact.presenterReportDate != nil) {
            
            NSString *lastContactedDateString = [NSDate dateToString:contact.presenterReportDate andFormate:kServerDateFormatte];
            [dict setValue:lastContactedDateString forKey:kInteractivePresenterDate];
        }
            
        if (contact != nil) {
            
            UIImage *image = [ApplicationFunction generateBarcodeImageWithQRCode:[contact getLocalizedQRCodeString]];
            NSData *qrData = UIImagePNGRepresentation(image);
            if (qrData != nil)
            {
                [dict setValue:[self imageToBase64EncodedString:qrData] forKey:kQRCode];
            }
        }
        if (contact.isDelete != nil) {
                [dict setObject:[NSNumber numberWithBool:contact.isDeleteValue] forKey:kDeleteStatus];
        }
        NSArray *tempFamilyArray = [contact.candidateFamilyInfos allObjects];
        NSMutableArray *familyArr = [[NSMutableArray alloc]init];
        for (TblFamily *family in tempFamilyArray)
        {
            NSMutableDictionary *familyDict = [[NSMutableDictionary alloc]init];
            if (family.iosAddressCode != nil) {
                [familyDict setValue:family.iosAddressCode forKey:kIosAddressCode];
            }
            if (family.name != nil) {
                [familyDict setValue:family.name forKey:kFamilyMemberName];
            }
            if (family.occupation != nil) {
                [familyDict setValue:family.occupation forKey:kFamilyMemberOccupation];
            }
            if (family.phoneNo != nil) {
                [familyDict setValue:family.phoneNo forKey:kFamilyMemberPhoneNo];
            }
            if (family.position != nil) {
                [familyDict setValue:family.position forKey:kFamilyMemberPosition];
            }
            if (family.relationship != nil) {
                [familyDict setValue:family.relationship forKey:kFamilyMemberRelationship];
            }
            
            
            [familyArr addObject:familyDict];
            
        }
        NSArray *tempPersonalCertificationArray = [contact.candidateProfessionalCertifications allObjects];
        NSMutableArray *certificatesArray = [[NSMutableArray alloc]init];
        for (TblPersonalCertification *certificate in tempPersonalCertificationArray)
        {
            NSMutableDictionary *certificateDict = [[NSMutableDictionary alloc]init];
            if (certificate.iosAddressCode != nil) {
                [certificateDict setValue:certificate.iosAddressCode forKey:kIosAddressCode];

            }
            if (certificate.charterAgency != nil) {
                [certificateDict setValue:certificate.charterAgency forKey:kProfessionalCertificationsCharterAgency];

            }
            if (certificate.charterDate != nil) {
                [certificateDict setValue:certificate.charterDate forKey:kProfessionalCertificationsCharterDate];

            }
            if (certificate.certificateName != nil) {
                [certificateDict setValue:certificate.certificateName forKey:kProfessionalCertificationsCertificateName];
  
            }

            
            [certificatesArray addObject:certificateDict];
            
        }
        NSArray *tempWorkExpArray = [contact.candidateWorkExperiences allObjects];
        NSMutableArray *expArray = [[NSMutableArray alloc]init];
        for (TblExperience *experience in tempWorkExpArray)
        {
            NSMutableDictionary *expDict = [[NSMutableDictionary alloc]init];
            if (experience.iosAddressCode != nil)
            {
                [expDict setValue:experience.iosAddressCode forKey:kIosAddressCode];

            }
            if (experience.startDate != nil)
            {
                [expDict setValue:experience.startDate forKey:kWorkExperienceStartDate];
 
            }
            if (experience.endDate != nil)
            {
                [expDict setValue:experience.endDate forKey:kWorkExperienceendDate];

            }
            if (experience.position != nil)
            {
                [expDict setValue:experience.position forKey:kWorkExperiencePosition];

            }
            if (experience.occupation != nil)
            {
                [expDict setValue:experience.occupation forKey:kWorkExperienceOccupation];

            }
            if (experience.unit != nil)
            {
                [expDict setValue:experience.unit forKey:kWorkExperienceUnit];

            }
            if (experience.witness != nil)
            {
                [expDict setValue:experience.witness forKey:kWorkExperienceWitness];

            }
            if (experience.witnessContactNo != nil)
            {
                [expDict setValue:experience.witnessContactNo forKey:kWorkExperienceWitnessContactNo];

            }
            if (experience.income != nil)
            {
                [expDict setValue:experience.income forKey:kWorkExperienceIncome];

            }
            [expArray addObject:expDict];
            
        }
        NSArray *tempEducationArray = [contact.candidateEducations allObjects];
        NSMutableArray *educationArray = [[NSMutableArray alloc]init];
        for (TblEducation *education in tempEducationArray)
        {
            NSMutableDictionary *educationDict = [[NSMutableDictionary alloc]init];
            if (education.iosAddressCode != nil)
            {
                [educationDict setValue:education.iosAddressCode forKey:kIosAddressCode];

            }
            if (education.startDate != nil)
            {
                [educationDict setValue:education.startDate forKey:kEducationStartDate];

            }
            if (education.endDate != nil)
            {
               // NSDate *endDate = [DbUtils string
                [educationDict setValue:education.endDate forKey:kEducationEndDate];

            }
            if (education.school != nil)
            {
                [educationDict setValue:education.school forKey:kEducationSchool];

            }
            if (education.educationLevel != nil)
            {
                [educationDict setValue:education.educationLevel forKey:kEducationEducationLevel];

            }
            if (education.witness != nil)
            {
                [educationDict setValue:education.witness forKey:kEducationWitness];

            }
            if (education.education != nil)
            {
                [educationDict setValue:education.education forKey:kEducationEducation];

            }
            if (education.witnessContactNo != nil)
            {
                [educationDict setValue:education.witnessContactNo forKey:kEducationWitnessContactNo];

            }
            [educationArray addObject:educationDict];
            
        }
        NSArray *tempESignatureArray = [contact.candidateESignatures allObjects];
        NSMutableArray *eSignatureArray = [[NSMutableArray alloc]init];
        for (TblESignature *eSignature in tempESignatureArray)
        {
            NSMutableDictionary *eSignatureDict = [[NSMutableDictionary alloc]init];
            if (eSignature.iosAddressCode != nil)
            {
                [eSignatureDict setValue:eSignature.iosAddressCode forKey:kIosAddressCode];

            }
            if (eSignature.branch != nil)
            {
                [eSignatureDict setValue:eSignature.branch forKey:kESignatureBranch];

            }
            if (eSignature.serviceDepartment != nil)
            {
                [eSignatureDict setValue:eSignature.serviceDepartment forKey:kESignatureserviceDepartment];

            }
            if (eSignature.agentId != nil)
            {
                [eSignatureDict setValue:eSignature.agentId forKey:kESignatureagentId];

            }
            if (eSignature.candidateName != nil)
            {
                [eSignatureDict setValue:eSignature.candidateName forKey:kESignaturecandidateName];

            }
            if (eSignature.city != nil)
            {
                [eSignatureDict setValue:eSignature.city forKey:kESignaturecity];
            }
            if (eSignature.atachedWithInsuranceCo != nil)
            {
                [eSignatureDict setValue:[NSNumber numberWithBool:eSignature.atachedWithInsuranceCoValue] forKey:kESignatureatachedWithInsuranceCo];
            }
            if (eSignature.contactWithAia != nil)
            {
                [eSignatureDict setValue:[NSNumber numberWithBool:eSignature.contactWithAiaValue] forKey:kESignaturecontactWithAia];
            }
            if (eSignature.takenPspTest != nil)
            {
                [eSignatureDict setValue:[NSNumber numberWithBool:eSignature.takenPspTestValue] forKey:kESignaturetakenPspTest];
            }
            if (eSignature.applicationDate != nil)
            {
                [eSignatureDict setValue:eSignature.applicationDate forKey:kESignatureapplicationDate];
            }
            if (eSignature.eSignaturePhoto != nil)
            {
                [eSignatureDict setValue:[self imageToBase64EncodedString:eSignature.eSignaturePhoto] forKey:kESignatureeSignaturePhoto];
            }
            [eSignatureArray addObject:eSignatureDict];
            
        }
        
                               
                               
        [dict setValue:familyArr forKey:@"candidateFamilyInfos"];
        [dict setValue:certificatesArray forKey:@"candidateProfessionalCertifications"];
        [dict setValue:expArray forKey:@"candidateWorkExperiences"];
        [dict setValue:educationArray forKey:@"candidateEducations"];
        [dict setValue:eSignatureArray forKey:@"candidateESignatures"];
        
        
        [dict setValue:@"" forKey:@"token"];
        [newContactArray addObject:dict];
        
        
        NSArray *notesArray = [contact.candidateNotes allObjects];
        NSMutableArray *notesMutableArray = [[NSMutableArray alloc]init];
        for (TblNotes *notes in notesArray)
        {
//            if ([notes.isSync boolValue] == NO) {
//                continue;
//            }
            NSMutableDictionary *notesDict = [[NSMutableDictionary alloc]init];
            if (contact.addressCode) {
                [notesDict setValue:contact.addressCode forKey:@"addressCode"];
            }
            if (notes.iosAddressCode != nil)
            {
                [notesDict setValue:notes.iosAddressCode forKey:@"iosNoteCode"];
                
            }
            if (notes.activityType != nil)
            {
                [notesDict setValue:notes.activityType forKey:@"activityType"];
                
            }
            if (notes.activityDate != nil)
            {
                NSString *dateString = [DateUtils dateToString:notes.activityDate andFormate:kServerDateFormatte];
                [notesDict setValue:dateString forKey:@"activityDate"];
                
            }
            if (notes.desc != nil)
            {
                [notesDict setValue:notes.desc forKey:@"description"];
                
            }
            if (notes.activityStatus != nil)
            {

                if ([notes.activityStatus boolValue] == YES) {
                    [notesDict setValue:@"true"forKey:@"activityStatus"];
                } else {
                    [notesDict setValue:@"false" forKey:@"activityStatus"];
                }
            }
            if (notes.isDelete != nil) {
                if ([notes.isDelete boolValue] == YES) {
                    [notesDict setValue:@"true"forKey:@"isDelete"];
                } else {
                    [notesDict setValue:@"false" forKey:@"isDelete"];
                }
            } else {
                    [notesDict setValue:@"false" forKey:@"isDelete"];
            }
            
            if (notes.isSystem != nil) {
                if ([notes.isSystem boolValue] == YES) {
                    [notesDict setValue:@"true"forKey:@"isSystem"];
                } else {
                    [notesDict setValue:@"false" forKey:@"isSystem"];
                }
            } else {
                [notesDict setValue:@"false" forKey:@"isSystem"];
            }

            
            [notesMutableArray addObject:notesDict];
            
        }
        
        newnotesArray = notesMutableArray;
//        if (notesMutableArray.count > 0) {
//            NSMutableDictionary *newNotesdict = [[NSMutableDictionary alloc]init];
//            [newNotesdict setValue:notesMutableArray forKey:@"candidateNotes"];
//            [newnotesArray addObject:newNotesdict];
//        }
        
    }
    
    if (newContactArray.count > 0) {
        [self.contactDataModel syncAddressBook:[CachingService sharedInstance].currentUserId contactsArray:newContactArray success:^(id responseObject) {
            
            [self updateDatabase:responseObject success:^{
                if (newnotesArray.count> 0) {
                    [self syncNotes:newnotesArray success:success failure:failure];
                }
                success();
            }failure:^{
                failure();
            }];
            
        } failure:^(id responseObject, NSError *error) {
            failure();
        }];
    } else {
        success();
    }
    
//    else {
//    
//        if (newnotesArray.count> 0) {
//            [self syncNotes:newnotesArray success:success failure:failure];
//        }
//
//    }
}
- (NSString *)imageToBase64EncodedString:(NSData *)imageData
{
    NSString *base64EncodedString = [imageData base64EncodedStringWithOptions:0];
    return base64EncodedString;
}

- (void)syncNotes:(NSArray*)notesArray success:(CMSContactSyncSuccessCallback)success failure:(CMSContactSyncSuccessCallback)failure
{
    [self.contactDataModel syncNotes:[CachingService sharedInstance].currentUserId contactsArray:notesArray success:^(id responseObject) {
        [self updateNotesDatabase:responseObject success:^{
            success();
        }failure:^{
            failure();
        }];
    } failure:^(id responseObject, NSError *error) {
        failure();
    }];
    
}

- (void)updateNotesDatabase:(id)response success:(CMSContactSyncSuccessCallback)success failure:(CMSContactSyncSuccessCallback)failure
{
    
}
- (void)updateDatabase:(id)response success:(CMSContactSyncSuccessCallback)success failure:(CMSContactSyncSuccessCallback)failure
{
    if (response != nil)
    {
        NSArray *syncedContactsArray = response;
        if ([syncedContactsArray count] > 0)
        {
            for (NSDictionary *dict in syncedContactsArray)
            {
                if ([dict valueForKey:@"iosAddressCode"] != nil)
                {
                    NSArray *contactArray = [[DbUtils fetchAllObject:@"TblContact"
                                                          andPredict:[NSPredicate predicateWithFormat:@"iosAddressCode == %@",[dict valueForKey:@"iosAddressCode"]]
                                                   andSortDescriptor:nil
                                                managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
                    if ([contactArray count] > 0) {
                        TblContact *contact = contactArray[0];
                        contact.isSync = @0;
                        contact.addressCode = [dict valueForKey:@"addressCode"];
                        if ([dict valueForKey:@"deleteStatus"] != nil)
                        {
                            contact.isDelete = [dict valueForKey:@"deleteStatus"];
                        }
                        [self.cachingService saveContext];
                    }
                }
                
            }
        }
        success();
    }else{
        failure();
    }
}

@end
