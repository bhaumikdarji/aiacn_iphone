//
//  SyncModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-13.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CachingService.h"
#import "CMSServiceManager.h"
#import "ContactEventModel.h"
typedef void (^CMSContactSyncSuccessCallback)();
@interface SyncModel : NSObject
@property (nonatomic, retain) CachingService *cachingService;
@property (nonatomic, retain) CMSServiceManager *serviceManager;
@property (nonatomic, retain) ContactEventModel *contactDataModel;
- (void) syncObjectsIntoDatabase:(NSString *)agentCode success:(CMSContactSyncSuccessCallback)success failure:(CMSContactSyncSuccessCallback)failure;
@end
