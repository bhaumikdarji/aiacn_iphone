//
//  TblEOPCandidateMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-08.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "TblEOPCandidateMTLModel.h"

@implementation TblEOPCandidateMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblEOPCandidateAttributes.dob : @"dob",
             TblEOPCandidateAttributes.status : @"status",
             TblEOPCandidateAttributes.agencyLeaderCode : @"agencyLeaderCode",
             TblEOPCandidateAttributes.agencyLeaderName : @"agencyLeaderName",
             TblEOPCandidateAttributes.agentName : @"agentName",
             TblEOPCandidateAttributes.buName : @"buName",
             TblEOPCandidateAttributes.candidateName : @"candidateName",
             TblEOPCandidateAttributes.completeStatus : @"completeStatus",
             TblEOPCandidateAttributes.contactNumber : @"contactNumber",
             TblEOPCandidateAttributes.distName : @"distName",
             TblEOPCandidateAttributes.dob : @"dob",
             TblEOPCandidateAttributes.dobStr : @"dobStr",
             TblEOPCandidateAttributes.gender : @"gender",
             TblEOPCandidateAttributes.servicingAgent : @"servicingAgent",
             TblEOPCandidateAttributes.sourceOfReferal : @"sourceOfReferal",
            TblEOPCandidateAttributes.token : @"token",
             TblEOPCandidateAttributes.age : @"age",
             TblEOPCandidateAttributes.buCode : @"buCode",
             TblEOPCandidateAttributes.candidateCode : @"candidateCode",
             TblEOPCandidateAttributes.cityCode : @"cityCode",
             TblEOPCandidateAttributes.districtCode : @"districtCode",
             TblEOPCandidateAttributes.eventCandidateCode : @"eventCandidateCode",
             TblEOPCandidateAttributes.eventCode : @"eventCode",
             TblEOPCandidateAttributes.sscCode : @"sscCode",
             TblEOPCandidateAttributes.attendedStatus : @"attendanceStatus",
             TblEOPCandidateAttributes.attendedEventCount : @"noOfRegisteredEvents"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblEOPCandidate entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblEOPCandidateAttributes.candidateCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblEOPCandidateAttributes.dob : TblEOPCandidateAttributes.dob,
             TblEOPCandidateAttributes.status : TblEOPCandidateAttributes.status,
             TblEOPCandidateAttributes.agencyLeaderCode : TblEOPCandidateAttributes.agencyLeaderCode,
             TblEOPCandidateAttributes.agencyLeaderName : TblEOPCandidateAttributes.agencyLeaderName,
             TblEOPCandidateAttributes.agentName : TblEOPCandidateAttributes.agentName,
             TblEOPCandidateAttributes.buName : TblEOPCandidateAttributes.buName,
             TblEOPCandidateAttributes.candidateName : TblEOPCandidateAttributes.candidateName,
             TblEOPCandidateAttributes.completeStatus : TblEOPCandidateAttributes.completeStatus,
             TblEOPCandidateAttributes.contactNumber : TblEOPCandidateAttributes.contactNumber,
             TblEOPCandidateAttributes.distName : TblEOPCandidateAttributes.distName,
             TblEOPCandidateAttributes.dob : TblEOPCandidateAttributes.dob,
             TblEOPCandidateAttributes.dobStr : TblEOPCandidateAttributes.dobStr,
             TblEOPCandidateAttributes.gender : TblEOPCandidateAttributes.gender,
             TblEOPCandidateAttributes.servicingAgent : TblEOPCandidateAttributes.servicingAgent,
             TblEOPCandidateAttributes.sourceOfReferal : TblEOPCandidateAttributes.sourceOfReferal,
             TblEOPCandidateAttributes.token : TblEOPCandidateAttributes.token,
             TblEOPCandidateAttributes.age : TblEOPCandidateAttributes.age,
             TblEOPCandidateAttributes.buCode : TblEOPCandidateAttributes.buCode,
             TblEOPCandidateAttributes.candidateCode : TblEOPCandidateAttributes.candidateCode,
             TblEOPCandidateAttributes.cityCode : TblEOPCandidateAttributes.cityCode,
             TblEOPCandidateAttributes.districtCode : TblEOPCandidateAttributes.districtCode,
             TblEOPCandidateAttributes.eventCandidateCode : TblEOPCandidateAttributes.eventCandidateCode,
             TblEOPCandidateAttributes.eventCode :  TblEOPCandidateAttributes.eventCode,
             TblEOPCandidateAttributes.sscCode : TblEOPCandidateAttributes.sscCode,
             TblEOPCandidateAttributes.attendedStatus : TblEOPCandidateAttributes.attendedStatus,
            TblEOPCandidateAttributes.attendedEventCount : TblEOPCandidateAttributes.attendedEventCount
             };
}

@end
