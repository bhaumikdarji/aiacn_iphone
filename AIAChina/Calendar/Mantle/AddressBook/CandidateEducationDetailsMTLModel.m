//
//  CandidateEducationDetailsMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CandidateEducationDetailsMTLModel.h"
#import "TblEducation.h"
@implementation CandidateEducationDetailsMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblEducationAttributes.iosAddressCode : @"iosAddressCode",
             TblEducationAttributes.education : @"education",
             TblEducationAttributes.educationLevel : @"educationLevel",
             TblEducationAttributes.endDate : @"endDate",
             TblEducationAttributes.school : @"school",
             TblEducationAttributes.startDate : @"startDate",
             TblEducationAttributes.witness : @"witness",
             TblEducationAttributes.witnessContactNo : @"witnessContactNo"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblEducation entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblEducationAttributes.iosAddressCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblEducationAttributes.iosAddressCode : TblEducationAttributes.iosAddressCode,
             TblEducationAttributes.education : TblEducationAttributes.education,
             TblEducationAttributes.educationLevel : TblEducationAttributes.educationLevel,
             TblEducationAttributes.endDate : TblEducationAttributes.endDate,
             TblEducationAttributes.school : TblEducationAttributes.school,
             TblEducationAttributes.startDate : TblEducationAttributes.startDate,
             TblEducationAttributes.witness : TblEducationAttributes.witness,
             TblEducationAttributes.witnessContactNo : TblEducationAttributes.witnessContactNo
             };
}

@end
