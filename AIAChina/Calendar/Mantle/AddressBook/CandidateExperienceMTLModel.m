//
//  CandidateExperienceMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CandidateExperienceMTLModel.h"

@implementation CandidateExperienceMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblExperienceAttributes.endDate : @"endDate",
             TblExperienceAttributes.income : @"income",
             TblExperienceAttributes.iosAddressCode : @"iosAddressCode",
             TblExperienceAttributes.occupation : @"occupation",
             TblExperienceAttributes.position : @"position",
             TblExperienceAttributes.startDate : @"startDate",
             TblExperienceAttributes.unit : @"unit",
             TblExperienceAttributes.witness : @"witness",
             TblExperienceAttributes.witnessContactNo : @"witnessContactNo"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblExperience entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblExperienceAttributes.iosAddressCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblExperienceAttributes.endDate : TblExperienceAttributes.endDate,
             TblExperienceAttributes.iosAddressCode : TblExperienceAttributes.iosAddressCode,
             TblExperienceAttributes.income : TblExperienceAttributes.income,
             TblExperienceAttributes.occupation : TblExperienceAttributes.occupation,
             TblExperienceAttributes.position : TblExperienceAttributes.position,
             TblExperienceAttributes.startDate : TblExperienceAttributes.startDate,
             TblExperienceAttributes.unit : TblExperienceAttributes.unit,
             TblExperienceAttributes.witness : TblExperienceAttributes.witness,
             TblExperienceAttributes.witnessContactNo : TblExperienceAttributes.witnessContactNo
             
             };
}
@end
