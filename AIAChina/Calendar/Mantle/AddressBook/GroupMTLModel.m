//
//  GroupMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "GroupMTLModel.h"

@implementation GroupMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblGroupAttributes.iosAddressCode : @"iosAddressCode",
             TblGroupAttributes.agentId : @"agentId",
             TblGroupAttributes.groupName : @"groupName"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblGroup entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblGroupAttributes.iosAddressCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblGroupAttributes.iosAddressCode : TblGroupAttributes.iosAddressCode,
             TblGroupAttributes.agentId : TblGroupAttributes.agentId,
             TblGroupAttributes.groupName : TblGroupAttributes.groupName
             };
}
@end
