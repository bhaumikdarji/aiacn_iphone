//
//  CandidateESignatureMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CandidateESignatureMTLModel.h"

@implementation CandidateESignatureMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblESignatureAttributes.iosAddressCode : @"iosAddressCode",
             TblESignatureAttributes.atachedWithInsuranceCo : @"atachedWithInsuranceCo",
             TblESignatureAttributes.contactWithAia : @"contactWithAia",
             TblESignatureAttributes.takenPspTest : @"takenPspTest",
             TblESignatureAttributes.agentId : @"agentId",
             TblESignatureAttributes.applicationDate : @"applicationDate",
             TblESignatureAttributes.branch : @"branch",
             TblESignatureAttributes.candidateName : @"candidateName",
             TblESignatureAttributes.city : @"city",
             TblESignatureAttributes.eSignaturePhoto : @"eSignaturePhoto",
             TblESignatureAttributes.serviceDepartment : @"serviceDepartment"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblESignature entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblESignatureAttributes.iosAddressCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblESignatureAttributes.iosAddressCode : TblESignatureAttributes.iosAddressCode,
             TblESignatureAttributes.atachedWithInsuranceCo : TblESignatureAttributes.atachedWithInsuranceCo,
             TblESignatureAttributes.contactWithAia : TblESignatureAttributes.contactWithAia,
             TblESignatureAttributes.takenPspTest : TblESignatureAttributes.takenPspTest,
             TblESignatureAttributes.agentId : TblESignatureAttributes.agentId,
             TblESignatureAttributes.applicationDate : TblESignatureAttributes.applicationDate,
             TblESignatureAttributes.branch : TblESignatureAttributes.branch,
             TblESignatureAttributes.candidateName : TblESignatureAttributes.candidateName,
             TblESignatureAttributes.city : TblESignatureAttributes.city,
             TblESignatureAttributes.eSignaturePhoto : TblESignatureAttributes.eSignaturePhoto,
             TblESignatureAttributes.serviceDepartment : TblESignatureAttributes.serviceDepartment
             
             };
}
@end
