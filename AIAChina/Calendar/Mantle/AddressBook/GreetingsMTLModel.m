//
//  TblTrainingMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-08.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "GreetingsMTLModel.h"
#import "TblGreetings.h"

@implementation GreetingsMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblGreetingsAttributes.eGreetingCode:@"EGreetingCode",
             TblGreetingsAttributes.eGreetingName:@"EGreetingName",
             TblGreetingsAttributes.festiveCategory:@"festive_category",
             TblGreetingsAttributes.fileName:@"fileName",
             TblGreetingsAttributes.fileLocation:@"fileLocation",
             TblGreetingsAttributes.createdby:@"created_by",
             TblGreetingsAttributes.createddate:@"created_date",
             TblGreetingsAttributes.modifiedby:@"modified_by",
             TblGreetingsAttributes.modifieddate:@"modified_date",
             TblGreetingsAttributes.token:@"token",
             TblGreetingsAttributes.status:@"status",
             TblGreetingsAttributes.catName:@"catName",
             TblGreetingsAttributes.agentId : @"agentId"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblGreetings entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblGreetingsAttributes.eGreetingCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblGreetingsAttributes.eGreetingCode:TblGreetingsAttributes.eGreetingCode,
             TblGreetingsAttributes.eGreetingName:TblGreetingsAttributes.eGreetingName,
             TblGreetingsAttributes.festiveCategory:TblGreetingsAttributes.festiveCategory,
             TblGreetingsAttributes.fileName:TblGreetingsAttributes.fileName,
             TblGreetingsAttributes.fileLocation:TblGreetingsAttributes.fileLocation,
             TblGreetingsAttributes.createdby :TblGreetingsAttributes.createdby,
             TblGreetingsAttributes.createddate:TblGreetingsAttributes.createddate,
             TblGreetingsAttributes.modifiedby:TblGreetingsAttributes.modifiedby,
             TblGreetingsAttributes.modifieddate:TblGreetingsAttributes.modifieddate,
             TblGreetingsAttributes.token:TblGreetingsAttributes.token,
             TblGreetingsAttributes.status :TblGreetingsAttributes.status,
             TblGreetingsAttributes.catName:TblGreetingsAttributes.catName,
             TblGreetingsAttributes.agentId : TblGreetingsAttributes.agentId

             };
}

@end
