//
//  CandidateESignatureMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblESignature.h"
#import "MTLValueTransformer.h"
@interface CandidateESignatureMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSString *agentId;
@property (nonatomic, strong) NSString *applicationDate;
@property (nonatomic, strong) NSString *branch;
@property (nonatomic, strong) NSString *candidateName;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSData *eSignaturePhoto;
@property (nonatomic, strong) NSString *iosAddressCode;
@property (nonatomic, strong) NSString *serviceDepartment;
@property (nonatomic, assign) BOOL atachedWithInsuranceCo;
@property (nonatomic, assign) BOOL contactWithAia;
@property (nonatomic, assign) BOOL takenPspTest;
@end
