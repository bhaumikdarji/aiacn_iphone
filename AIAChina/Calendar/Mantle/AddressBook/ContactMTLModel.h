//
//  ContactMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblContact.h"
#import "MTLValueTransformer.h"

@interface ContactMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSString *registeredAddress;
@property (nonatomic, strong) NSString *registeredAddress1;
@property (nonatomic, strong) NSString *registeredAddress2;
@property (nonatomic, strong) NSString *residentialAddress;
@property (nonatomic, strong) NSString *residentialAddress2;
@property (nonatomic, strong) NSString *residentialPostalCode;
@property (nonatomic, strong) NSString *residentialAddress1;
@property (nonatomic, strong) NSString *agentId;
@property (nonatomic, strong) NSString *birthMonthDay;
@property (nonatomic, strong) NSString *birthPlace;
@property (nonatomic, strong) NSString *createdBy;
@property (nonatomic, strong) NSString *education;
@property (nonatomic, strong) NSString *eMailId;
@property (nonatomic, strong) NSString *fixedLineNO;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *groupName;
@property (nonatomic, strong) NSString *identityType;
@property (nonatomic, strong) NSString *iosAddressCode;
@property (nonatomic, strong) NSString *marritalStatus;
@property (nonatomic, strong) NSString *mobilePhoneNo;
@property (nonatomic, strong) NSString *modifiedBy;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *natureOfBusiness;
@property (nonatomic, strong) NSString *nric;
@property (nonatomic, strong) NSString *officePhoneNo;
@property (nonatomic, strong) NSString *outlookApperance;
@property (nonatomic, strong) NSString *registeredPostalCode;
@property (nonatomic, strong) NSString *presentWorkCondition;
@property (nonatomic, strong) NSString *purchasedAnyInsurance;
@property (nonatomic, strong) NSString *qq;
@property (nonatomic, strong) NSString *race;
@property (nonatomic, strong) NSString *recommendedRecruitmentScheme;
@property (nonatomic, strong) NSString *recruitedBy;
@property (nonatomic, strong) NSString *newcomerSource;
@property (nonatomic, strong) NSString *recruitmentProgressStatus;
@property (nonatomic, strong) NSString *referalSource;
@property (nonatomic, strong) NSString *reJoin;
@property (nonatomic, strong) NSString *remarks;
@property (nonatomic, strong) NSString *salesExperience;
@property (nonatomic, strong) NSString *talentProfile;
@property (nonatomic, strong) NSString *weChat;
@property (nonatomic, strong) NSString *weibo;
@property (nonatomic, strong) NSString *workingYearExperience;
@property (nonatomic, strong) NSString *yearlyIncome;
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, strong) NSNumber *addressCode;
@property (nonatomic, strong) NSNumber *birthYear;
@property (nonatomic, assign) BOOL isDelete;
@property (nonatomic, assign) BOOL isSync;
@property (nonatomic, strong) NSDate *birthDate;
@property (nonatomic, strong) NSString *creationDate;
@property (nonatomic, strong) NSDate *lastContactedDate;
@property (nonatomic, strong) NSDate *presenterReportDate;
@property (nonatomic, strong) NSDate *modificationDate;
@property (nonatomic, strong) NSData *candidatePhoto;
@property (nonatomic, strong) NSData *presenterReport;


@property (nonatomic, strong) NSSet *candidateEducations;
@property (nonatomic, strong) NSSet *candidateESignatures;
@property (nonatomic, strong) NSSet *candidateWorkExperiences;
@property (nonatomic, strong) NSSet *candidateFamilyInfos;
@property (nonatomic, strong) NSSet *group;
@property (nonatomic, strong) NSSet *candidateNotes;
@property (nonatomic, strong) NSSet *candidateProfessionalCertifications;

@end
