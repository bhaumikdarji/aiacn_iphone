//
//  NotesMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-04.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "NotesMTLModel.h"

@implementation NotesMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblNotesAttributes.iosAddressCode : @"iosAddressCode",
             TblNotesAttributes.activityDate : @"activityDate",
             TblNotesAttributes.activityStatus : @"activityStatus",
             TblNotesAttributes.activityType : @"activityType",
             TblNotesAttributes.desc : @"description",
             TblNotesAttributes.isSystem : @"isSystem",
             
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblNotes entityName];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblNotesAttributes.iosAddressCode : TblNotesAttributes.iosAddressCode,
             TblNotesAttributes.activityDate : TblNotesAttributes.activityDate,
             TblNotesAttributes.activityStatus : TblNotesAttributes.activityStatus,
             TblNotesAttributes.activityType : TblNotesAttributes.activityType,
             TblNotesAttributes.desc : TblNotesAttributes.desc,
             TblNotesAttributes.isSystem : TblNotesAttributes.isSystem
             };
}
@end
