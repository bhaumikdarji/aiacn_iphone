//
//  AgentMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-27.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AgentMTLModel.h"

@implementation AgentMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblAgentDetailsAttributes.userName : @"USERNAME",
             TblAgentDetailsAttributes.title : @"TITLE",
             TblAgentDetailsAttributes.officeCode : @"OFFICECODE",
             TblAgentDetailsAttributes.userType : @"USERTYPE",
             TblAgentDetailsAttributes.teamName : @"TEAMNAME",
             TblAgentDetailsAttributes.teamCode : @"TEAMCODE",
             TblAgentDetailsAttributes.branch : @"BRANCH",
             TblAgentDetailsAttributes.city : @"CITY",
             TblAgentDetailsAttributes.officeName : @"OFFICENAME",
             TblAgentDetailsAttributes.gender : @"GENDER",
             TblAgentDetailsAttributes.ssc : @"SSC",
             TblAgentDetailsAttributes.userID : @"USERID",
             TblAgentDetailsAttributes.userStatus : @"USERSTATUS",
             TblAgentDetailsAttributes.certiID : @"CERTIID",
             TblAgentDetailsAttributes.dateofBirth : @"DATEOFBIRTH",
             TblAgentDetailsAttributes.contractedDate : @"CONTRACTEDDATE",
             TblAgentDetailsAttributes.dtleader : @"DTLEADER",
             TblAgentDetailsAttributes.agentImage : @"agentImage",
             TblAgentDetailsAttributes.serviceDepartment : @"serviceDepartment",
             TblAgentDetailsAttributes.rank : @"TITLEDESCRIBE",
             TblAgentDetailsAttributes.contactNumber : @"MOBILE",
             TblAgentDetailsAttributes.emailId : @"EMAIL",
             TblAgentDetailsAttributes.agentType : @"AGENTTYPE",
             TblAgentDetailsAttributes.agentDetails : @"AGENTDETAIL",
             TblAgentDetailsAttributes.titleDescribe : @"TITLEDESCRIBE"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblAgentDetails entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblAgentDetailsAttributes.userID];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblAgentDetailsAttributes.userName : TblAgentDetailsAttributes.userName,
             TblAgentDetailsAttributes.title : TblAgentDetailsAttributes.title,
             TblAgentDetailsAttributes.officeCode : TblAgentDetailsAttributes.officeCode,
             TblAgentDetailsAttributes.userType : TblAgentDetailsAttributes.userType,
             TblAgentDetailsAttributes.teamName : TblAgentDetailsAttributes.teamName,
             TblAgentDetailsAttributes.teamCode : TblAgentDetailsAttributes.teamCode,
             TblAgentDetailsAttributes.branch : TblAgentDetailsAttributes.branch,
             TblAgentDetailsAttributes.city : TblAgentDetailsAttributes.city,
             TblAgentDetailsAttributes.officeName : TblAgentDetailsAttributes.officeName,
             TblAgentDetailsAttributes.gender : TblAgentDetailsAttributes.gender,
             TblAgentDetailsAttributes.ssc : TblAgentDetailsAttributes.ssc,
             TblAgentDetailsAttributes.userID : TblAgentDetailsAttributes.userID,
             TblAgentDetailsAttributes.userStatus : TblAgentDetailsAttributes.userStatus,
             TblAgentDetailsAttributes.certiID : TblAgentDetailsAttributes.certiID,
             TblAgentDetailsAttributes.dateofBirth : TblAgentDetailsAttributes.dateofBirth,
             TblAgentDetailsAttributes.contractedDate : TblAgentDetailsAttributes.contractedDate,
             TblAgentDetailsAttributes.dtleader : TblAgentDetailsAttributes.dtleader,
             TblAgentDetailsAttributes.agentImage : TblAgentDetailsAttributes.agentImage,
             TblAgentDetailsAttributes.serviceDepartment : TblAgentDetailsAttributes.serviceDepartment,
             TblAgentDetailsAttributes.rank : TblAgentDetailsAttributes.rank,
             TblAgentDetailsAttributes.contactNumber : TblAgentDetailsAttributes.contactNumber,
             TblAgentDetailsAttributes.emailId : TblAgentDetailsAttributes.emailId,
             TblAgentDetailsAttributes.titleDescribe : TblAgentDetailsAttributes.titleDescribe,
             TblAgentDetailsAttributes.agentType : TblAgentDetailsAttributes.agentType,
             TblAgentDetailsAttributes.agentDetails : TblAgentDetailsAttributes.agentDetails
             };
}
@end
