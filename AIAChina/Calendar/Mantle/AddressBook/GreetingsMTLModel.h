//
//  GreetingsMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-08.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "MTLValueTransformer.h"

@interface GreetingsMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>

@property (nonatomic, strong) NSNumber *eGreetingCode;
@property (nonatomic, strong) NSString *eGreetingName;
@property (nonatomic, strong) NSNumber *festiveCategory;
@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, strong) NSString *fileLocation;
@property (nonatomic, strong) NSString *createdby;
@property (nonatomic, strong) NSString *createddate;
@property (nonatomic, strong) NSString *modifiedby;
@property (nonatomic, strong) NSString *modifieddate;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *catName;
@property (nonatomic, strong) NSString *agentId;
@property (nonatomic, assign) BOOL status;

@end
