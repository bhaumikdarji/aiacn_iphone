//
//  PersonalCertificationMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "PersonalCertificationMTLModel.h"

@implementation PersonalCertificationMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblPersonalCertificationAttributes.iosAddressCode : @"iosAddressCode",
             TblPersonalCertificationAttributes.certificateName : @"certificateName",
             TblPersonalCertificationAttributes.charterAgency : @"charterAgency",
             TblPersonalCertificationAttributes.charterDate : @"charterDate"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblPersonalCertification entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblPersonalCertificationAttributes.iosAddressCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblPersonalCertificationAttributes.iosAddressCode : TblPersonalCertificationAttributes.iosAddressCode,
             TblPersonalCertificationAttributes.certificateName : TblPersonalCertificationAttributes.certificateName,
             TblPersonalCertificationAttributes.charterAgency : TblPersonalCertificationAttributes.charterAgency,
             TblPersonalCertificationAttributes.charterDate : TblPersonalCertificationAttributes.charterDate
             };
}
@end
