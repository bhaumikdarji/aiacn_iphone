//
//  AgentMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-27.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "MTLValueTransformer.h"
#import "TblAgentDetails.h"
@interface AgentMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *officeCode;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, strong) NSString *teamName;
@property (nonatomic, strong) NSString *teamCode;
@property (nonatomic, strong) NSString *branch;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *officeName;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *ssc;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *certiID;
@property (nonatomic, strong) NSString *dateofBirth;
@property (nonatomic, strong) NSString *contractedDate;
@property (nonatomic, strong) NSString *dtleader;
@property (nonatomic, strong) NSString *userStatus;
@property (nonatomic, strong) NSData *agentImage;
@property (nonatomic, strong) NSString *serviceDepartment;
@property (nonatomic, strong) NSString *rank;
@property (nonatomic, strong) NSString *contactNumber;
@property (nonatomic,strong) NSString *emailId;
@property (nonatomic, strong) NSString *agentType;
@property (nonatomic, strong) NSString *agentDetails;
@property (nonatomic, strong) NSString *titleDescribe;
@end
