//
//  TblInterviewCandidateMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-08-08.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "TblInterviewCandidateMTLModel.h"
#import "TblInterviewCandidate.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblCalendarEvent.h"
#import "MTLValueTransformer.h"

@implementation TblInterviewCandidateMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            TblInterviewCandidateAttributes.candidateCode : @"candidateCode",
             TblInterviewCandidateAttributes.interviewCode : @"interviewCode",
             TblInterviewCandidateAttributes.candidateName : @"candidateName",
             TblInterviewCandidateAttributes.servicingAgent : @"servicingAgent",
             TblInterviewCandidateAttributes.agentName : @"agentName",
             TblInterviewCandidateAttributes.nric : @"nric",
           TblInterviewCandidateAttributes.buCode : @"buCode",
             TblInterviewCandidateAttributes.cityCode : @"cityCode",
             TblInterviewCandidateAttributes.sscCode : @"sscCode",
             TblInterviewCandidateAttributes.districtCode : @"districtCode",
             TblInterviewCandidateAttributes.buName : @"buName",
             TblInterviewCandidateAttributes.distName : @"distName",
             TblInterviewCandidateAttributes.agencyLeaderCode : @"agencyLeaderCode",
             TblInterviewCandidateAttributes.agencyLeaderName : @"agencyLeaderName",
             TblInterviewCandidateAttributes.sourceOfReferal : @"sourceOfReferal",
             TblInterviewCandidateAttributes.age : @"age",
             TblInterviewCandidateAttributes.dob : @"dob",
             TblInterviewCandidateAttributes.dobStr : @"dobStr",
             TblInterviewCandidateAttributes.gender : @"gender",
             TblInterviewCandidateAttributes.contactNumber : @"contactNumber",
             TblInterviewCandidateAttributes.ccTestResult : @"ccTestResult",
             TblInterviewCandidateAttributes.recruitmentScheme : @"recruitmentScheme",
             TblInterviewCandidateAttributes.p100 : @"p100",
             TblInterviewCandidateAttributes.interviewResult : @"interviewResult",
             TblInterviewCandidateAttributes.remarks : @"remarks",
             TblInterviewCandidateAttributes.status : @"status",
             TblInterviewCandidateAttributes.token : @"token",
             TblInterviewCandidateAttributes.fileNameList : @"fileNameList",
            TblInterviewCandidateAttributes.passedStatus :@"passedStatus"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblInterviewCandidate entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblInterviewCandidateAttributes.candidateCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
            TblInterviewCandidateAttributes.candidateCode : TblInterviewCandidateAttributes.candidateCode,
             TblInterviewCandidateAttributes.interviewCode : TblInterviewCandidateAttributes.interviewCode,
             TblInterviewCandidateAttributes.candidateName : TblInterviewCandidateAttributes.candidateName,
             TblInterviewCandidateAttributes.servicingAgent : TblInterviewCandidateAttributes.servicingAgent,
             TblInterviewCandidateAttributes.agentName : TblInterviewCandidateAttributes.agentName,
             TblInterviewCandidateAttributes.nric : TblInterviewCandidateAttributes.nric,
             TblInterviewCandidateAttributes.buCode : TblInterviewCandidateAttributes.buCode,
             TblInterviewCandidateAttributes.cityCode : TblInterviewCandidateAttributes.cityCode,
             TblInterviewCandidateAttributes.sscCode : TblInterviewCandidateAttributes.sscCode,
             TblInterviewCandidateAttributes.districtCode : TblInterviewCandidateAttributes.districtCode,
             TblInterviewCandidateAttributes.buName : TblInterviewCandidateAttributes.buName,
             TblInterviewCandidateAttributes.distName :TblInterviewCandidateAttributes.distName,
             TblInterviewCandidateAttributes.agencyLeaderCode : TblInterviewCandidateAttributes.agencyLeaderCode,
             TblInterviewCandidateAttributes.agencyLeaderName : TblInterviewCandidateAttributes.agencyLeaderName,
             TblInterviewCandidateAttributes.sourceOfReferal : TblInterviewCandidateAttributes.sourceOfReferal,
             TblInterviewCandidateAttributes.age : TblInterviewCandidateAttributes.age,
             TblInterviewCandidateAttributes.dob : TblInterviewCandidateAttributes.dob,
             TblInterviewCandidateAttributes.dobStr : TblInterviewCandidateAttributes.dobStr,
             TblInterviewCandidateAttributes.gender : TblInterviewCandidateAttributes.gender,
             TblInterviewCandidateAttributes.contactNumber : TblInterviewCandidateAttributes.contactNumber,
             TblInterviewCandidateAttributes.ccTestResult : TblInterviewCandidateAttributes.ccTestResult,
             TblInterviewCandidateAttributes.recruitmentScheme : TblInterviewCandidateAttributes.recruitmentScheme,
             TblInterviewCandidateAttributes.p100 : TblInterviewCandidateAttributes.p100,
             TblInterviewCandidateAttributes.interviewResult : TblInterviewCandidateAttributes.interviewResult,
             TblInterviewCandidateAttributes.remarks : TblInterviewCandidateAttributes.remarks,
             TblInterviewCandidateAttributes.status : TblInterviewCandidateAttributes.status,
             TblInterviewCandidateAttributes.token :TblInterviewCandidateAttributes.token,
             TblInterviewCandidateAttributes.fileNameList : TblInterviewCandidateAttributes.fileNameList,
            TblInterviewCandidateAttributes.passedStatus : TblInterviewCandidateAttributes.passedStatus
            //TblInterviewCandidateRelationships.interview : TblInterviewCandidateRelationships.interview
             };
}
@end
