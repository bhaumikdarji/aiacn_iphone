//
//  CandidateEducationDetailsMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblEducation.h"
#import "MTLValueTransformer.h"
@interface CandidateEducationDetailsMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSString *education;
@property (nonatomic, strong) NSString *educationLevel;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *iosAddressCode;
@property (nonatomic, strong) NSString *school;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic,strong) NSString *witness;
@property (nonatomic, strong) NSString *witnessContactNo;
@end
