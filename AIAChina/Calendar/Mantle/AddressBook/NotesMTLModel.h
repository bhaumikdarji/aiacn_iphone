//
//  NotesMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-04.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblNotes.h"
#import "MTLValueTransformer.h"
@interface NotesMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSDate *activityDate;
@property (nonatomic, assign) NSInteger activityStatus;
@property (nonatomic, strong) NSString *activityType;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, assign) BOOL isSystem;
@property (nonatomic, strong) NSString *iosAddressCode;

@end
