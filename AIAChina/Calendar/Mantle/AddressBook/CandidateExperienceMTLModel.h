//
//  CandidateExperienceMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblExperience.h"
#import "MTLValueTransformer.h"
@interface CandidateExperienceMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *income;
@property (nonatomic, strong) NSString *iosAddressCode;
@property (nonatomic, strong) NSString *occupation;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *unit;
@property (nonatomic, strong) NSString *witness;
@property (nonatomic, strong) NSString *witnessContactNo;

@end
