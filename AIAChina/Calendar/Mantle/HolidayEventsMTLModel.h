//
//  HolidayEventsMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-07-31.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblCalendarEvent.h"
#import "MTLValueTransformer.h"
@interface HolidayEventsMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>
@property (nonatomic, retain) NSString *buName;
@property (nonatomic, retain) NSString *creationDate;
@property (nonatomic, retain) NSString *endDate;
@property (nonatomic, retain) NSString *holidayName;
@property (nonatomic, retain) NSString *iconSelection;
@property (nonatomic, retain) NSString *modificationDate;
@property (nonatomic, retain) NSString *modifiedBy;
@property (nonatomic, retain) NSString *postedBy;
@property (nonatomic, retain) NSString *startDate;
@property (nonatomic, assign) Boolean status;
@property (nonatomic, retain) NSString *token;
@property (nonatomic,retain) NSNumber *buCode;
@property (nonatomic,retain) NSString *cityCode;
@property (nonatomic,retain) NSNumber *createdBy;
@property (nonatomic,retain) NSNumber *holidayCode;
@property (nonatomic,retain) NSString *sscCode;
@property (nonatomic,retain) NSNumber *district;
@property (nonatomic, strong) TblCalendarEvent *calendarEvent;
@end
