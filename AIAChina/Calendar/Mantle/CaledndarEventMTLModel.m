//
//  CaledndarEventMTLModel.m
//  AIAChina
//
//  Created by Burri on 2015-07-29.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CaledndarEventMTLModel.h"
#import "TblCalendarEvent.h"
#import "TblInterview.h"
@implementation CaledndarEventMTLModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             TblCalendarEventAttributes.agentCode : @"agentCode",
             TblCalendarEventAttributes.allDay : @"allDay",
             TblCalendarEventRelationships.interviewEvents : @"interviewEvents",
             TblCalendarEventRelationships.eopEvents : @"eopEvents",
             TblCalendarEventRelationships.holidayEvents : @"holidayEvents",
             TblCalendarEventRelationships.trainingEvents : @"trainingEvents"
             };
}

+ (NSString *)managedObjectEntityName
{
    return [TblCalendarEvent entityName];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing
{
    return [NSSet setWithObject:TblCalendarEventAttributes.agentCode];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey
{
    return @{
             TblCalendarEventAttributes.agentCode : TblCalendarEventAttributes.agentCode,
             TblCalendarEventAttributes.allDay : TblCalendarEventAttributes.allDay
             };
}

+ (NSDictionary *)relationshipModelClassesByPropertyKey
{
    return @{TblCalendarEventRelationships.interviewEvents : TblCalendarEventRelationships.interviewEvents,
             TblCalendarEventRelationships.eopEvents:TblCalendarEventRelationships.eopEvents,
             TblCalendarEventRelationships.holidayEvents:TblCalendarEventRelationships.holidayEvents,
             TblCalendarEventRelationships.trainingEvents:TblCalendarEventRelationships.trainingEvents
             };
}
@end
