//
//  EOPEventsMTLModel.h
//  AIAChina
//
//  Created by Burri on 2015-07-31.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLManagedObjectAdapter.h"
#import "TblCalendarEvent.h"
#import "MTLValueTransformer.h"
@interface EOPEventsMTLModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>

@property (nonatomic, strong) TblCalendarEvent *calendarEvent;
@property (nonatomic, strong) NSString *agentTeam;
@property (nonatomic, strong) NSString *buName;
@property (nonatomic, strong) NSString *creationDate;
@property (nonatomic, strong) NSString *endTime;
@property (nonatomic, strong) NSString *eopDescription;
@property (nonatomic, strong) NSDate *eventDate;
@property (nonatomic, strong) NSString *eventDateStr;
@property (nonatomic, strong) NSString *eventName;
@property (nonatomic, strong) NSString *eventType;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *openTo;
@property (nonatomic, strong) NSString *openToRegistration;
@property (nonatomic, strong) NSString *openToSpGrp;
@property (nonatomic, strong) NSString *oraganiserStr;
@property (nonatomic, strong) NSString *profilePath;
@property (nonatomic, strong) NSString *publicUrl;
@property (nonatomic, strong) NSString *speaker;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *topicPath;
@property (nonatomic, assign) Boolean status;
@property (nonatomic, assign) Boolean isRegistered;
@property (nonatomic, strong) NSString *timeEnd;
@property (nonatomic, strong) NSString *timeStart;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *topic;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSNumber *buCode;
@property (nonatomic, strong) NSNumber *attendeeCount;
@property (nonatomic, strong) NSString *cityCode;
@property (nonatomic, strong) NSNumber *completedCount;
@property (nonatomic, strong) NSString *createdBy;
@property (nonatomic, strong) NSNumber *district;
@property (nonatomic, strong) NSNumber *estimatedCandidates;
@property (nonatomic, strong) NSNumber *eventCode;
@property (nonatomic, strong) NSString *modifiedBy;
@property (nonatomic, strong) NSNumber *organizer;
@property (nonatomic, strong) NSNumber *registeredCount;
@property (nonatomic, strong) NSString *sscCode;



@end
