//
//  ContactEventModel.h
//  AIAChina
//
//  Created by Burri on 2015-08-03.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMSServiceManager.h"
#import "CachingService.h"
#import "ContactDataProvider.h"

typedef void (^CMSContactSuccessCallback)();
typedef void (^CMSContactSuccessCallbackWithCount)(int count);
typedef void (^CMSContactSuccessCallbackWithResponseObject)(id responseObject);
typedef void (^CMSContactFailureCallback)(id responseObject, NSError *error);
@interface ContactEventModel : NSObject
@property (nonatomic,retain) CMSServiceManager *serviceManager;
@property (nonatomic,retain) CachingService *cachingService;
@property (nonatomic,retain) ContactDataProvider *contactDataProvider;
@property (nonatomic, retain) NSMutableDictionary *allEvnetsDict;

+ (instancetype)sharedInstance;
- (void)syncAddressBook:(NSString *)agentCode contactsArray:(NSArray *)contactArray success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;

- (void)syncNotes:(NSString *)agentCode contactsArray:(NSArray *)notesArray success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getAddressBook:(NSString *)agentId dateTime:(NSString *)dateTime pageNumber:(int)page success:(CMSContactSuccessCallbackWithCount)success failure:(CMSContactFailureCallback)failure;
- (void)getEOPRegistrationCall:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getPastEOP:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getEOPRegisteredCandidateCount:(NSString *)eventCode agentID:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;

//CC Test Result
- (void)getCCTestResult:(NSString *)agentCode candidateCode:(NSString *)candidateCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)postCCTestResult:(NSString *)agentCode candidateCode:(NSString *)candidateCode testResult:(NSString *)testResult resultDate:(NSString *)resultDate success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;

- (void)postCandidateRegistrationServiceCall:(NSDictionary *)eopDict agentId:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getEOPRegisteredCandidate:(NSString *)eventCode agentId:(NSString *)agentID success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getCandidateRegisteredEOP:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getFirstInterview:(NSString *)candidateCode agentID:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)postFirstInterview:(NSDictionary *)bodyDict success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void) getALEExamResults:(NSString *)candidateCode agentID:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void) postContractedDetails:(NSDictionary *)dict success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void) deleteInterviewRegistration:(NSString *)interviewCode candidateCode:(NSString*)candidateCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void) deleteEOPRegistration:(NSString *)interviewCode candidateCode:(NSString*)candidateCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getAllInterviewWithCandidateCode:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getInterviewRegisteredCandidateCount:(NSString *)interviewCode agentID:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getAllPastInterviewWithCandidateCode:(NSString *)candidateCode agentCode:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)postInterviewCandidateRegister:(NSDictionary *)dict agentId:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void) pushABCTraining:(NSDictionary *)body success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;

- (void) getABCTraningResults:(NSString *)candidateCode agentID:(NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void) getContractDetail:(NSString *)candidateAgentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)getInterviewRegisteredCandidate:(NSString *)interviewCode agentID:(NSString *)agentId success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void) saveAgentLoginDetails:(NSDictionary *)dict;
- (void)getEGreetingsForAgentCode: (NSString *)agentCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)saveEopEvents:(NSSet *)eopEventsSet;
- (void)saveInterviewEvents:(NSSet *)interViewEventsSet;
- (void)getEOPDeleteCall:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
- (void)deleteInterviewCall:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;

- (void)getCandidateInterviewStatus:(NSString *)interviewType forCandidateCode:(NSString *)candidateCode success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;

- (void)getCandidateDetailsForCandidateCode:(NSString *)candidateCode
                                    agentID:(NSString *)agentId
                                  eventCode:(NSString *)eventCode
                                    success:(CMSContactSuccessCallbackWithResponseObject)success failure:(CMSContactFailureCallback)failure;
@end
