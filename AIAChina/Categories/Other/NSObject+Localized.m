//
//  NSObject+Localized.m
//  AccountManager
//
//  Created by Smitesh Patel on 2014-07-29.
//  Copyright (c) 2014 TD Canada Mobile. All rights reserved.
//

#import "NSObject+Localized.h"

@implementation NSObject (Localized)
///
/// This method is used to translate strings in .xib files.
/// Using the "User Defined Runtime Attributes" set an entry like:
/// Key Path: textLocalized
/// Type: String
/// Value: {THE TRANSLATION KEY}
///
-(void) setTextLocalized:(NSString *)key
{
    if ([self isKindOfClass:[UILabel class]])
    {
        UILabel *label = (UILabel *)self;
        [label setText:NSLocalizedString(key, nil)];
    }
    else if ([self isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)self;
        [button setTitle:NSLocalizedString(key, nil) forState:UIControlStateNormal];
    }
    else if ([self isKindOfClass:[UIBarButtonItem class]])
    {
        UIBarButtonItem *button = (UIBarButtonItem *)self;
        [button setTitle:NSLocalizedString(key, nil)];
    }
}

@end
