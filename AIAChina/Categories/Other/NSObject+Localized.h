//
//  NSObject+Localized.h
//  AccountManager
//
//  Created by Smitesh Patel on 2014-07-29.
//  Copyright (c) 2014 TD Canada Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Localized)
-(void) setTextLocalized:(NSString *)key;

@end
