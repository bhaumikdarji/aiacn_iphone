

#import "UINavigationBar+setFontFace.h"

@implementation UINavigationBar (setFontFace)

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                            [UIColor whiteColor], NSForegroundColorAttributeName,
                                            [UIColor clearColor], NSShadowAttributeName,
                                            [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)], NSShadowAttributeName,
                                            nil]];
}

@end
