//
//  
//  AIAChina
//
//  Created by Burri on 2015-07-22.
//  Copyright (c) 2015 AIA. All rights reserved.
//
#import "NSData+HexEncoding.h"

@implementation NSData (HexEncoding)

+ (NSData *)dataForHexString:(NSString *)hexString
{
    NSMutableData *mutableData = [[NSMutableData alloc] init];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    for (int byteIndex = 0; byteIndex < ([hexString length] / 2); byteIndex++) {
        byte_chars[0] = [hexString characterAtIndex:(byteIndex * 2)];
        byte_chars[1] = [hexString characterAtIndex:((byteIndex * 2) + 1)];
        whole_byte = strtol(byte_chars, NULL, 16);
        [mutableData appendBytes:&whole_byte length:1];
    }
   
    return [NSData dataWithData:mutableData];
}

- (NSString *)encodeHexString
{
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    NSUInteger dataLength  = [self length];
    NSMutableString  *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i) {
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    }
    
    return [NSString stringWithString:hexString];
}

@end
