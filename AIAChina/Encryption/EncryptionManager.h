//
//  EncryptionManager.h
//  AIAChina
//
//  Created by Burri on 2015-07-22.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kEncryptionKeySize       kCCKeySizeAES256
@interface EncryptionManager : NSObject
{
    
}

@property (nonatomic, strong) NSString *hexEncodedMasterKey;

+ (instancetype)sharedEncrptionManager;
- (NSData *)DWUK;
@end
