//
//  CMSEncryptedStringTransformer.m
//  AIAChina
//
//  Created by Burri on 2015-07-22.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CMSEncryptedStringTransformer.h"
#import "NSData+AES256.h"
#import "EncryptionManager.h"
#define kStringEncoding      NSUTF8StringEncoding
@interface CMSEncryptedStringTransformer ()
@property (nonatomic, strong) EncryptionManager *encryptionKeyManager;
@end
@implementation CMSEncryptedStringTransformer
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.encryptionKeyManager = [EncryptionManager sharedEncrptionManager];
    }
    return self;
}
+ (Class)transformedValueClass
{
    return [NSData class];
}
+ (BOOL)allowsReverseTransformation
{
    return YES;
}
- (id)transformedValue:(id)value
{
    if (value && [value isKindOfClass:[NSString class]]) {
        NSString * stringValue = (NSString*)value;
        NSData * stringData = [stringValue dataUsingEncoding:kStringEncoding];
        NSData * encryptionKey = [self.encryptionKeyManager DWUK];
        NSData * encryptedData = [stringData AES256EncryptWithKeyData:encryptionKey];;
        return encryptedData;
    }
    return nil;
}

- (id)reverseTransformedValue:(id)value
{
    if (value && [value isKindOfClass:[NSData class]]) {
        NSData * encryptedData = (NSData*)value;
        NSData * decryptionKey = [self.encryptionKeyManager DWUK];
        NSData * decryptedData = [encryptedData AES256DecryptWithKeyData:decryptionKey];
        NSString * decryptedString = [[NSString alloc] initWithData:decryptedData encoding:kStringEncoding];
        return decryptedString;
    }
    return nil;
}
@end
