//
//  CMSEncryptedDataTransformer.m
//  AIAChina
//
//  Created by Burri on 2015-07-22.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CMSEncryptedDataTransformer.h"
#import "NSData+AES256.h"
#import "EncryptionManager.h"
#define kStringEncoding      NSUTF8StringEncoding
@interface CMSEncryptedDataTransformer ()
@property (nonatomic, strong) EncryptionManager *encryptionKeyManager;
@end
@implementation CMSEncryptedDataTransformer
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.encryptionKeyManager = [EncryptionManager sharedEncrptionManager];
    }
    return self;
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

+ (Class)transformedValueClass
{
    return [NSData class];
}

- (id)transformedValue:(id)value
{
    if (value && [value isKindOfClass:[NSData class]]) {
        NSData *data = (NSData *)value;
        //changed data
        NSData * encryptionKey = [self.encryptionKeyManager DWUK];
        NSData * encryptedData = [data AES256EncryptWithKeyData:encryptionKey];
        return encryptedData;
    }
    return nil;
}

- (id)reverseTransformedValue:(id)value
{
    if (value && [value isKindOfClass:[NSData class]]) {
        NSData *encryptedData = (NSData*)value;
        NSData * decryptionKey = [self.encryptionKeyManager DWUK];
        NSData * decryptedData = [encryptedData AES256DecryptWithKeyData:decryptionKey];
        return decryptedData;
    }
    return nil;
}
@end
