//
//  EncryptionManager.m
//  AIAChina
//
//  Created by Burri on 2015-07-22.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EncryptionManager.h"
#import <CommonCrypto/CommonCrypto.h>
#import "NSData+HexEncoding.h"
#import "JNKeychain.h"

#define kMasterKeyInjectorKey       @"kMasterKeyInjectorKey"


#define kUserKeyGenerationAlgorithm              kCCPBKDF2
#define kUserKeyGenerationPseudoRandomFunction   kCCPRFHmacAlgSHA1
#define kUserKeyGenerationRounds                 (10000)
@interface EncryptionManager ()

@property (nonatomic, strong) NSData * generatedDWUK;

@end
@implementation EncryptionManager

+ (instancetype)sharedEncrptionManager
{
    static id _encryptionManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _encryptionManager = [[self alloc] init];
    });
    
    return _encryptionManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (NSData *)DWUK
{
    if (self.generatedDWUK)
        return self.generatedDWUK;
    
    self.generatedDWUK = [JNKeychain loadValueForKey:kMasterKeyInjectorKey];
    
    if (!self.generatedDWUK) {
        
        NSData * userIDData = [[self getRandomKey] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableData *derivedUserKey = [NSMutableData dataWithLength:kEncryptionKeySize];
        
        
        NSData * masterKeyData = [self.hexEncodedMasterKey dataUsingEncoding:NSUTF8StringEncoding];
        //[NSData dataForHexString:self.hexEncodedMasterKey];
        
        const char * password = masterKeyData.bytes;
        const size_t passwordLength = masterKeyData.length;
        
        const uint8_t *salt = userIDData.bytes;
        const size_t saltLength = userIDData.length;
        
        int result = CCKeyDerivationPBKDF(kUserKeyGenerationAlgorithm, password, passwordLength,
                                          salt, saltLength, kUserKeyGenerationPseudoRandomFunction,
                                          kUserKeyGenerationRounds, derivedUserKey.mutableBytes, derivedUserKey.length);
        
        self.generatedDWUK = ((result == 0) ? derivedUserKey : nil);
        [JNKeychain saveValue:self.generatedDWUK forKey:kMasterKeyInjectorKey];
    }
    return self.generatedDWUK;
}


- (NSString *)getRandomKey
{
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789/=+";
    NSMutableString *s = [NSMutableString stringWithCapacity:16];
    for (NSUInteger i = 0; i < 16; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    NSLog(@"%@", s);
    NSString *key = s;
    return key;
}
@end
