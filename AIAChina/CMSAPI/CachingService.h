//
//  ResourceTagPopoverViewController.m
//  AIAChina
//
//  Created by Burri on 2015-07-19.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CachingService : NSObject
@property (nonatomic,strong)  NSString *currentUserIdWithZero;
@property (nonatomic,strong)  NSString *currentUserId;
@property (nonatomic,strong)  NSString *branchCode;
@property (nonatomic,assign)  BOOL searchForContactCompleted;
@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (instancetype)sharedInstance;

- (void)saveContext;
- (void)deleteObjects:(NSArray *)objectsArray;

// Use this method on logout to flush the managed objects from memory
- (void)releasePersistantStoreAndManagedObjectContext;

@end
