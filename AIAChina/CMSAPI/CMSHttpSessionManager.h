//
//  CMSHttpSessionManager.h
//  AIAChina
//
//  Created by Smitesh Patel on 2015-07-20.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AFHTTPSessionManager.h"

typedef void (^CMSServerRequestSuccessCallback)(NSURLSessionDataTask *task, id responseObject);
typedef void (^CMSServerRequestFailureCallback)(NSURLSessionDataTask *task, id responseObject, NSError *error);

@interface CMSHttpSessionManager : AFHTTPSessionManager

- (NSURLSessionDataTask *)GETRequestWithRelativeURLString:(NSString*)relativeRequestURLString
                              requestHTTPHeaderDictionary:(NSDictionary*)requestHTTPHeaderDictionary
                              requestParametersDictionary:(NSDictionary*)requestParametersDictionary
                                                  success:(CMSServerRequestSuccessCallback)success
                                                  failure:(CMSServerRequestFailureCallback)failure;
- (NSURLSessionDataTask *)POSTRequestWithRelativeURLString:(NSString *)relativeRequestURLString
                                                  HTTPBody:(NSData *)HTTPBody
                                                   success:(CMSServerRequestSuccessCallback)success
                                                   failure:(CMSServerRequestFailureCallback)failure;
@end
