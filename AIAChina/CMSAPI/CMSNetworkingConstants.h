//
//  CMSNetworkingConstants.h
//  AIAChina
//
//  Created by Smitesh Patel on 2015-07-20.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#ifndef AIAChina_CMSNetworkingConstants_h
#define AIAChina_CMSNetworkingConstants_h
//#define kAgentId @"000000071"

//Client

#define kCMSBaseURLString            @"http://202.150.215.194/IMOCN3/"
//#define kCMSBaseURLString            @"http://192.168.0.64:8086/IMOCN/"
//UAT

#define kLoginServiceURL            @"http://211.144.219.243/isp/rest/index.do"
//#define kCMSBaseURLString            @"http://211.144.219.243/IMOCN/"
//#define kCMSBaseURLString            @"http://192.168.0.68:8080/IMOCN_MAVEN/" //jay's URL
//#define kCMSBaseURLString            @"http://192.168.0.64:8080/IMOCN/" //maunish's URL

#define KErecruitmentEnhancementURL @"https://211.144.219.243/AIATouchBE/IService.do"
#define kCMSAnnouncementURL @"rest/announcement/getAllannouncement"
#define kAgentLoginDetails  @"rest/user/agentLoginDetails"
#define kCMSDeleteAnnouncementURL @"rest/announcement/getAllDeletedAnnouncement"
#define kCMSInterviewURL @"rest/interview/getAllInterview"
#define kCMSDELETEDEOPURL @"rest/event/getAllDeletedEop"
#define kCMSDELETEINTERVEWURL @"rest/interview/getAllDeletedInterview"
#define kCMSHolidayURL @"rest/holiday/getAllHoliday"
#define kCMSEOPURL @"rest/event/getAllEop"
#define kCMSGetAddressBook @"rest/addressBook/getAgentAddressBook"
#define kCMSDecryptUrl @"rest/encryptDecrypt/decrypt"
#define kCMSTraningURL @"rest/training/getAllTrainingDetails"
#define kCMSGetSessionURL @"http://211.144.219.243/SSO/loginAction.do"
#define kCMSValidateURL @"http://211.144.219.243/SSO/ssoValidateLoginAction.do"
//PRDO
//#define kCMSBaseURLString            @""

#if defined(DEBUG) || defined(ADHOC)
#define kAllowInvalidSSLCertificates YES
#else
#define kAllowInvalidSSLCertificates NO
#endif


#define kAcceptHeaderKey            @"Accept"
#define kAcceptHeaderValue          @"application/json;charset=UTF-8"

#define kAcceptEncodingHeaderKey    @"Accept-Encoding"
#define kAcceptEncodingHeaderValue  @"gzip,deflate"

#define kContentTypeHeaderKey       @"Content-Type"
#define kContentTypeHeaderValue     @"application/json"
#define kContentTypePlainHeaderValue @"text/plain"
#define kCMSVersionConstant         @"2.0.1"

#endif
