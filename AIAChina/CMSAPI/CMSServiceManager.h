//
//  CMSServiceManager.h
//  AIAChina
//
//  Created by Smitesh Patel on 2015-07-20.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMSHttpSessionManager.h"
@interface CMSServiceManager : NSObject

+ (instancetype)sharedCMSServiceManager;
- (NSURLSessionDataTask*)doLogin:(NSDictionary*)parameters success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure;

- (NSURLSessionDataTask*)decrypt:(NSString*)relativeURL toDecrypt:(NSString *)base64 success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure;

- (NSURLSessionDataTask*)getDataFromServiceCall:(NSString*)relativeURL parametersDictionary:(NSDictionary *)parametersDict success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure;
- (NSURLSessionDataTask *)createDataTaskForRequestWithVersionPath:(NSString *)versionPath
                                                     relativePath:(NSString *)relativePath
                                                forBodyDictionary:(NSDictionary *)bodyDictionary
                                                          success:(CMSServerRequestSuccessCallback)success
                                                          failure:(CMSServerRequestFailureCallback)failure;
- (NSURLSessionDataTask *)createPostDataTaskForRequestWithVersionPath:(NSString *)versionPath
                                                         relativePath:(NSString *)relativePath
                                                    forBodyDictionary:(NSArray *)bodyArray
                                                              success:(CMSServerRequestSuccessCallback)success
                                                              failure:(CMSServerRequestFailureCallback)failure;
- (NSURLSessionDataTask*)getSessionData:(NSString*)relativeURL withParameter:(NSDictionary*)param success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure;
- (NSURLSessionDataTask*) validateSessionData:(NSString*)relativeURL withParameter:(NSDictionary*)param success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure;
- (NSURLSessionDataTask*) setLoginValue:(NSString*)relativeURL withParameter:(NSDictionary*)param success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure;

- (BOOL) canReachServer;
@end
