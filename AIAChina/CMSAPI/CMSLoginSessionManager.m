//
//  CMSHttpSessionManager.m
//  AIAChina
//
//  Created by Smitesh Patel on 2015-07-20.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CMSLoginSessionManager.h"
#import "CMSNetworkingConstants.h"

@implementation CMSLoginSessionManager

- (instancetype)init
{
    self = [super initWithBaseURL:[NSURL URLWithString:kLoginServiceURL]
             sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    if (self) {
        [self.securityPolicy setAllowInvalidCertificates:kAllowInvalidSSLCertificates];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    return self;
}


#pragma mark - Send a request
- (NSURLSessionDataTask *)GETRequestWithRelativeURLString:(NSString*)relativeRequestURLString
                              requestHTTPHeaderDictionary:(NSDictionary*)requestHTTPHeaderDictionary
                              requestParametersDictionary:(NSDictionary*)requestParametersDictionary
                                                  success:(CMSLoginRequestSuccessCallback)success
                                                  failure:(CMSLoginRequestFailureCallback)failure
{
    NSError *serializationError = nil;
    
    NSString * absoluteURLString = [NSString stringWithFormat:@"%@%@", kLoginServiceURL, relativeRequestURLString];
    NSLog(@"URL : %@",absoluteURLString);
    NSLog(@"PARAM : %@",requestParametersDictionary);
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"GET"
                                                                   URLString:absoluteURLString
                                                                  parameters:requestParametersDictionary
                                                                       error:&serializationError];
    
    if (serializationError) {
        if (failure) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
            dispatch_async(self.completionQueue ?: dispatch_get_main_queue(), ^
            {
                failure(nil, nil, serializationError);
            });
#pragma clang diagnostic pop
        }
        return nil;
    }
    
    [requestHTTPHeaderDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [request addValue:(NSString*)obj forHTTPHeaderField:(NSString*)key];
    }];
    
    __block NSURLSessionDataTask *dataTask = nil;
    dataTask = [self dataTaskWithRequest:request completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
        if (error)
        {
            if (failure)
            {
                failure(dataTask, responseObject, error);
            }
        } else {
            if (success) {
                NSLog(@"RES : %@",responseObject);
                success(dataTask, responseObject);
            }
        }
    }];
    return dataTask;
}


- (NSURLSessionDataTask *)POSTRequestWithRelativeURLString:(NSString *)relativeRequestURLString
                                                  HTTPBody:(NSData *)HTTPBody
                                                   success:(CMSLoginRequestSuccessCallback)success
                                                   failure:(CMSLoginRequestFailureCallback)failure
{
    NSError *serializationError = nil;
    NSString * absoluteURLString = [NSString stringWithFormat:@"%@%@", kCMSBaseURLString, relativeRequestURLString];
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"POST"
                                                                   URLString:absoluteURLString
                                                                  parameters:nil
                                                                       error:&serializationError];
    
    if (serializationError) {
        if (failure) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
            dispatch_async(self.completionQueue ?: dispatch_get_main_queue(), ^{
                failure(nil, nil, serializationError);
            });
#pragma clang diagnostic pop
        }
        return nil;
    }
    [request addValue:kAcceptEncodingHeaderValue forHTTPHeaderField:kAcceptEncodingHeaderKey];
    [request addValue:@"text/plain" forHTTPHeaderField:kContentTypeHeaderKey];
    
    [request setHTTPBody:HTTPBody];
    
    __block NSURLSessionDataTask *dataTask = nil;
    dataTask = [self dataTaskWithRequest:request completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
        if (error) {
            if (failure) {
                failure(dataTask, responseObject, error);
            }
        } else {
            if (success) {
                success(dataTask, responseObject);
            }
        }
    }];
    return dataTask;
}


@end
