//
//  CMSGetSessionManager.h
//  AIAChina
//
//  Created by imac on 18/12/15.
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AFHTTPSessionManager.h"

typedef void (^CMSGetSessionSuccessCallback)(NSURLSessionDataTask *task, id responseObject);
typedef void (^CMSGetSessionFailureCallback)(NSURLSessionDataTask *task, id responseObject, NSError *error);


@interface CMSGetSessionManager : AFHTTPSessionManager

- (NSURLSessionDataTask *)GETRequestWithRelativeURLString:(NSString*)relativeRequestURLString
                              requestHTTPHeaderDictionary:(NSDictionary*)requestHTTPHeaderDictionary
                              requestParametersDictionary:(NSDictionary*)requestParametersDictionary
                                                  success:(CMSGetSessionSuccessCallback)success
                                                  failure:(CMSGetSessionFailureCallback)failure;
- (NSURLSessionDataTask *)POSTRequestWithRelativeURLString:(NSString *)relativeRequestURLString
                                                  HTTPBody:(NSData *)HTTPBody
                                                   success:(CMSGetSessionSuccessCallback)success
                                                   failure:(CMSGetSessionFailureCallback)failure;

@end
