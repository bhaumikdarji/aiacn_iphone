//
//  CMSHttpSessionManager.h
//  AIAChina
//
//  Created by Smitesh Patel on 2015-07-20.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AFHTTPSessionManager.h"

typedef void (^CMSLoginRequestSuccessCallback)(NSURLSessionDataTask *task, id responseObject);
typedef void (^CMSLoginRequestFailureCallback)(NSURLSessionDataTask *task, id responseObject, NSError *error);

@interface CMSLoginSessionManager : AFHTTPSessionManager

- (NSURLSessionDataTask *)GETRequestWithRelativeURLString:(NSString*)relativeRequestURLString
                              requestHTTPHeaderDictionary:(NSDictionary*)requestHTTPHeaderDictionary
                              requestParametersDictionary:(NSDictionary*)requestParametersDictionary
                                                  success:(CMSLoginRequestSuccessCallback)success
                                                  failure:(CMSLoginRequestFailureCallback)failure;
- (NSURLSessionDataTask *)POSTRequestWithRelativeURLString:(NSString *)relativeRequestURLString
                                                  HTTPBody:(NSData *)HTTPBody
                                                   success:(CMSLoginRequestSuccessCallback)success
                                                   failure:(CMSLoginRequestFailureCallback)failure;
@end
