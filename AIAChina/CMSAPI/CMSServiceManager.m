//
//  CMSServiceManager.m
//  AIAChina
//
//  Created by Smitesh Patel on 2015-07-20.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CMSServiceManager.h"
#import "CMSLoginSessionManager.h"
#import "CMSNetworkingConstants.h"
#import "CMSGetSessionManager.h"

@interface CMSServiceManager ()
@property (nonatomic,strong) CMSHttpSessionManager *sessionManager;
@property (nonatomic,strong) CMSLoginSessionManager *loginSessionManager;
@property (nonatomic,strong) CMSGetSessionManager *getSessionManager;

@end
@implementation CMSServiceManager

+ (instancetype)sharedCMSServiceManager {
    
    static id _sharedCMSServiceManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedCMSServiceManager = [[self alloc] init];
       
    });
    
    return _sharedCMSServiceManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.sessionManager = [[CMSHttpSessionManager alloc] init];
        self.loginSessionManager = [[CMSLoginSessionManager alloc] init];
        _getSessionManager=[[CMSGetSessionManager alloc]init];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return self;
}

- (BOOL)canReachServer
{

    return [AFNetworkReachabilityManager sharedManager].reachable;

}

- (NSURLSessionDataTask*)doLogin:(NSDictionary*)parameters success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure {
    NSMutableDictionary * requestHTTPHeaderDictionary = [self httpHeaders];
    return [self.loginSessionManager GETRequestWithRelativeURLString:@"" requestHTTPHeaderDictionary:requestHTTPHeaderDictionary requestParametersDictionary:parameters success:^(NSURLSessionDataTask *task, id responseObject)
            {
                
                
                success(task,responseObject);
            } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
                
                
                
                failure(task,responseObject,error);
            }];
}

- (NSURLSessionDataTask*)decrypt:(NSString*)relativeURL toDecrypt:(NSString *)base64 success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure {

    return [self.sessionManager POSTRequestWithRelativeURLString:relativeURL HTTPBody:[base64 dataUsingEncoding:NSUTF8StringEncoding] success:^(NSURLSessionDataTask *task, id responseObject)
            {

                success(task,responseObject);
            } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
                
                
                
                failure(task,responseObject,error);
            }];
}

- (NSURLSessionDataTask*) getSessionData:(NSString*)relativeURL withParameter:(NSDictionary*)param success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure {
    NSMutableDictionary * requestHTTPHeaderDictionary = [self httpHeaders];
    
    return [self.getSessionManager GETRequestWithRelativeURLString:relativeURL requestHTTPHeaderDictionary:requestHTTPHeaderDictionary requestParametersDictionary:param success:^(NSURLSessionDataTask *task, id responseObject) {
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(task,responseObject,error);
    }];
}

- (NSURLSessionDataTask*) validateSessionData:(NSString*)relativeURL withParameter:(NSDictionary*)param success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure {
    NSMutableDictionary * requestHTTPHeaderDictionary = [self httpHeaders];
    
    return [self.getSessionManager GETRequestWithRelativeURLString:relativeURL requestHTTPHeaderDictionary:requestHTTPHeaderDictionary requestParametersDictionary:param success:^(NSURLSessionDataTask *task, id responseObject) {
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(task,responseObject,error);
    }];
}

- (NSURLSessionDataTask*) setLoginValue:(NSString*)relativeURL withParameter:(NSDictionary*)param success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure {
//    NSMutableDictionary * requestHTTPHeaderDictionary = [self httpHeaders];
    
    NSData *data=[NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
    return [self.sessionManager POSTRequestWithRelativeURLString:relativeURL HTTPBody:data success:^(NSURLSessionDataTask *task, id responseObject) {
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        failure(task,responseObject,error);
    }];
}


- (NSURLSessionDataTask *)createDataTaskForRequestWithVersionPath:(NSString *)versionPath
                                                     relativePath:(NSString *)relativePath
                                                forBodyDictionary:(NSDictionary *)bodyDictionary
                                                          success:(CMSServerRequestSuccessCallback)success
                                                          failure:(CMSServerRequestFailureCallback)failure
{
    NSString *relativeRequestURLString = [versionPath stringByAppendingString:relativePath];
    return [self.sessionManager POSTRequestWithRelativeURLString:relativeRequestURLString HTTPBody:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        //TODO: Handle Server error.
       
            success(task, responseObject);
    } failure:failure];
}

- (NSURLSessionDataTask *)createPostDataTaskForRequestWithVersionPath:(NSString *)versionPath
                                                     relativePath:(NSString *)relativePath
                                                forBodyDictionary:(NSArray *)bodyArray
                                                          success:(CMSServerRequestSuccessCallback)success
                                                          failure:(CMSServerRequestFailureCallback)failure
{
    NSLog(@"PARAM : %@",bodyArray);
    NSData *bodyJSONData = [NSJSONSerialization dataWithJSONObject:bodyArray options:0 error:nil];
    
    NSString *relativeRequestURLString = [versionPath stringByAppendingString:relativePath];
    return [self.sessionManager POSTRequestWithRelativeURLString:relativeRequestURLString HTTPBody:bodyJSONData success:^(NSURLSessionDataTask *task, id responseObject) {
        
        //TODO: Handle Server error.
        
        success(task, responseObject);
    } failure:failure];
}

- (NSURLSessionDataTask*)getDataFromServiceCall:(NSString*)relativeURL parametersDictionary:(NSDictionary *)parametersDict success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure{
    NSMutableDictionary * requestHTTPHeaderDictionary = [self httpHeaders];
    return [self.sessionManager GETRequestWithRelativeURLString:relativeURL requestHTTPHeaderDictionary:requestHTTPHeaderDictionary requestParametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject)
    {        
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        
        
        
        failure(task,responseObject,error);
    }];
}
- (NSMutableDictionary *) httpHeaders
{
    NSMutableDictionary * requestHTTPHeaderDictionary = [[NSMutableDictionary alloc]init];
    [requestHTTPHeaderDictionary setObject:kAcceptHeaderValue forKey:kAcceptHeaderKey];
    [requestHTTPHeaderDictionary setObject:kAcceptEncodingHeaderValue forKey:kAcceptEncodingHeaderKey];
    [requestHTTPHeaderDictionary setObject:kContentTypeHeaderValue forKey:kContentTypeHeaderKey];
    
    
    return requestHTTPHeaderDictionary;
}
- (NSURLSessionDataTask*)getAllHolidays:(NSString*)relativeURL success:(CMSServerRequestSuccessCallback)success failure:(CMSServerRequestFailureCallback)failure{
    NSMutableDictionary * requestHTTPHeaderDictionary = [self httpHeaders];
    return [self.sessionManager GETRequestWithRelativeURLString:relativeURL requestHTTPHeaderDictionary:requestHTTPHeaderDictionary requestParametersDictionary:nil success:^(NSURLSessionDataTask *task, id responseObject)
            {
                
                
                success(task,responseObject);
            } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
                
                
                
                failure(task,responseObject,error);
            }];
}


@end
