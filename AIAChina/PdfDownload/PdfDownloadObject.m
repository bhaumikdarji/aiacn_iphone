//
//  PdfDownloadObject.m
//  AIAChina
//
//  Created by Burri on 2015-07-23.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "PdfDownloadObject.h"
#import "CMSNetworkingConstants.h"
#import "AFDownloadRequestOperation.h"

@implementation PdfDownloadObject
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (void)downloadFileWithFilePath:(NSString *)filePath
                                 fileName:(NSString *)fileName
                                 finished:(PdfDownloadFinished)completionBlock
{
    NSString *baseURL = [kCMSBaseURLString stringByAppendingString:kBaseURL];
    NSString *pdfURLPath = [baseURL stringByAppendingString:filePath];
    NSString *pdfPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:fileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:pdfPath];
    
    NSError *attributesError = nil;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:pdfPath error:&attributesError];
    
    NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
    long long fileSize = [fileSizeNumber longLongValue];
    if (fileExists && fileSize > 0) {
        completionBlock(pdfPath,nil);
    } else {
        NSString *urlString = [NSString stringWithFormat:pdfURLPath,
                               filePath];
        NSString *encodedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSURL *url = [NSURL URLWithString:encodedUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:pdfPath append:NO]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"RES: %@", [[[operation response] allHeaderFields] description]);
            completionBlock(pdfPath, nil);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            completionBlock(nil, error);
        }];
        
        [operation start];
    }
}
- (void)downloadFile:(NSString *)eventCode afterMaterial:(BOOL)usingMaterial finished:(FileDownloadFinished)completion failure:(FileDownloadNotFinished)errorBlock
{
    NSString *baseURL = [kCMSBaseURLString stringByAppendingString:kEventMaterialDownloadURL];
    NSString *urlString = [baseURL stringByAppendingString:eventCode];
    if (usingMaterial) {
        urlString = [urlString stringByAppendingString:kAfterEventMaterialDownloadURL];
    }
    NSString *encodedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [NSURL URLWithString:encodedUrl];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    requestOperation.responseSerializer = [AFHTTPResponseSerializer serializer];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        completion(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Image error: %@", error);
        errorBlock(error);
    }];
    [requestOperation start];
}

- (void)downloadFile:(NSString *)urlString finished:(FileDownloadFinished)completion failure:(FileDownloadNotFinished)errorBlock
{
//    NSString *baseURL = [kCMSBaseURLString stringByAppendingString:kEventMaterialDownloadURL];
//    NSString *urlString = [baseURL stringByAppendingString:eventCode];
//    if (usingMaterial) {
//        urlString = [urlString stringByAppendingString:kAfterEventMaterialDownloadURL];
//    }
    NSString *encodedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [NSURL URLWithString:encodedUrl];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    requestOperation.responseSerializer = [AFHTTPResponseSerializer serializer];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         completion(responseObject);
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Image error: %@", error);
         errorBlock(error);
     }];
    [requestOperation start];
}
@end
