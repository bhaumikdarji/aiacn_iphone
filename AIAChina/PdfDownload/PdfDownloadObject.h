//
//  PdfDownloadObject.h
//  AIAChina
//
//  Created by Burri on 2015-07-23.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kBaseURL    @"rest/announcement/downloadFile?announcementCode="
#define kEventMaterialDownloadURL @"rest/event/downloadFile?eventCode="
#define kAttachmentDownloadURL @"rest/interview/downloadFile?interviewCode="
#define kAfterEventMaterialDownloadURL @"&material=Profile"

typedef void (^PdfDownloadFinished)(NSString *filePath,NSError *error);
typedef void (^FileDownloadFinished)(id response);
typedef void (^FileDownloadNotFinished)(NSError *error);
@interface PdfDownloadObject : NSObject
- (void)downloadFileWithFilePath:(NSString *)filePath
                                 fileName:(NSString *)fileName
                                 finished:(PdfDownloadFinished)completionBlock;
- (void)downloadFile:(NSString *)eventCode afterMaterial:(BOOL)usingMaterial finished:(FileDownloadFinished)completion failure:(FileDownloadNotFinished)errorBlock;

- (void)downloadFile:(NSString *)urlString finished:(FileDownloadFinished)completion failure:(FileDownloadNotFinished)errorBlock;
@end
