//
//  LoginViewController.h
//  AIAChina
//
//  Created by Burri on 2015-07-27.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : AIAChinaViewController<UITextFieldDelegate> 
@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *masterKeyButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end
