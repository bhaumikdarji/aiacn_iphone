 //
//
//  LoginViewController.m
//  AIAChina
//
//  Created by Burri on 2015-07-27.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "LoginViewController.h"
#import "PickerPopoverViewController.h"
#import "HomeViewController.h"
#import "Utils.h"
#import "NSData+AES256.h"
#import "MBProgressHUDUpd.h"
#import "CMSServiceManager.h"

#import "TblAgentDetails.h"
#import "AgentMTLModel.h"
#import "ContactEventModel.h"
#import "JNKeychain.h"
#import "NSString+isNumeric.h"



@interface LoginViewController ()
@property (nonatomic, strong) NSArray *masterKeyArray;
@property (nonatomic, strong) NSDictionary *branchCodeDict;
@property (nonatomic, retain) NSString *selectedBranchCode;
@property (nonatomic, strong) ContactEventModel *contactDataModel;
@property (weak, nonatomic) IBOutlet UILabel *lblVersionString;
@property (nonatomic, strong) UIPopoverController *popOVer;
@end

const int USER_ID_LENGTH = 9;

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Login", @"");
    self.contactDataModel = [ContactEventModel sharedInstance];
    self.loginView.layer.borderWidth = 1.0f;
    self.loginView.layer.cornerRadius = 8.0f;
    self.userNameTextField.placeholder = NSLocalizedString(@"Enter User Name Here", @" Enter UserName Here");
    self.passwordTextField.placeholder = NSLocalizedString(@"Enter Password Here", @"");
    [self.masterKeyButton setTitle:NSLocalizedString(@"branch", @"") forState:UIControlStateNormal];
    [self.loginButton setTitle:NSLocalizedString(@"Login", @"") forState:UIControlStateNormal];
    self.navigationItem.title = NSLocalizedString(@"Login", @"");
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = YES;
    self.loginView.layer.borderColor = [[UIColor colorWithRed:200.0/255.0f green:23.0f/255.0f blue:67.0/255.0f alpha:1.0]CGColor];
    
    self.branchCodeDict = [Utility loadJsonDataFromPath:@"brachInfo.json"];

    NSDate *date = [DateUtils stringToDate:[NSString stringWithUTF8String:__DATE__] dateFormat:kBuildDateFormat];
    
    NSString *buildDate = [DateUtils dateToString:date andFormate:kLoginDateFormat];
    self.lblVersionString.text = [NSString stringWithFormat:@"%@:%@ %@:%@",NSLocalizedString(@"Version",@""),[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"],NSLocalizedString(@"Build", @""),buildDate];
    //self.masterbranchCodeArray = [branchCodeDict allKeys];
    self.masterKeyArray = [self.branchCodeDict allValues];
    self.masterKeyButton.layer.borderWidth = 1.0f;
    self.masterKeyButton.layer.borderColor = [[UIColor blackColor] CGColor];
    [self.masterKeyButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 2.0, 0.0, 0.0)];
    
#if DEBUG
//    self.userNameTextField.text = @"000015710";
//    self.passwordTextField.text = @"11111111";
    
    self.userNameTextField.text = @"000309086";
    self.passwordTextField.text = @"11111111";
#endif
    // Do any additional setup after loading the view.
    self.userNameTextField.delegate = self;
}
- (IBAction)onMasterKeyButtonClick:(id)sender {
    [self setPopoverView:self.masterKeyButton pickerArray:self.masterKeyArray];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setPopoverView:(UIButton *)btn pickerArray:(NSArray *)pickerArray{
    NSMutableArray *pickerDataArray = [[NSMutableArray alloc] init];
    [pickerDataArray addObjectsFromArray:[pickerArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    
    PickerPopoverViewController *popoverViewCtrl = [[PickerPopoverViewController alloc] initWithNibName:@"PickerPopoverViewController" bundle:nil];
    
    if(isEmptyString(btn.titleLabel.text))
        [btn setTitle:pickerDataArray[0] forState:UIControlStateNormal];
    else
        popoverViewCtrl.oldValue = btn.titleLabel.text;
    
    [popoverViewCtrl setPickerArray:pickerDataArray];
    
    __weak typeof(self) weakSelf = self;
    [popoverViewCtrl setSelectedIndex:^(int selectedRowIndexPath) {
        NSString *value = pickerDataArray[selectedRowIndexPath];
        [btn setTitle:value forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        weakSelf.selectedBranchCode = [weakSelf.branchCodeDict allKeysForObject:value][0];
        [weakSelf.popOVer dismissPopoverAnimated:TRUE];
    }];
    
    self.popOVer = [[UIPopoverController alloc] initWithContentViewController:popoverViewCtrl];
    
        self.popOVer.popoverContentSize = CGSizeMake(150, 200);
   
    [self.popOVer presentPopoverFromRect:btn.frame
                                            inView:self.loginView
     
                          permittedArrowDirections:UIPopoverArrowDirectionLeft
                                          animated:TRUE];
}
- (void)keyboardWillShow:(NSNotification *)notification{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.view.frame = CGRectMake(self.view.frame.origin.x,-100,self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
    
   
}
- (IBAction)onLoginButtonClick:(id)sender
{
    self.userNameTextField.text = [self getProcessedUserID:self.userNameTextField.text];
    [self byPassLoginRequest];
}

- (void)byPassLoginRequest
{
    
    NSDictionary *dict = [Utility loadJsonDataFromPath:@"Login.json"];
    
    int responseStatus = [[dict objectForKey:@"success"] intValue];
    
    if (responseStatus == 1)
    {
        NSDictionary *contentDict = [dict objectForKey:@"content"];
        [self executeActualLoginWithData:contentDict];

    }

    
}

- (void)loginRequest
{
    if (self.userNameTextField.text.length > 0 &&
        self.passwordTextField.text.length > 0 &&
        self.selectedBranchCode.length > 0 ) {
        if ([[CMSServiceManager sharedCMSServiceManager] canReachServer] == YES)
        {
            NSDictionary *parameters = @{
                                         @"isAjax": @"true",
                                         @"account": self.userNameTextField.text,
                                         @"co": self.selectedBranchCode,
                                         @"password": self.passwordTextField.text
                                         };
            [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
            [[[CMSServiceManager sharedCMSServiceManager] doLogin:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
                
                int responseStatus = [[responseObject objectForKey:@"success"] intValue];
                
                if (responseStatus == 1) {
                    
                    NSString *content = [responseObject objectForKey:@"content"];
                    [[[CMSServiceManager sharedCMSServiceManager] decrypt:kCMSDecryptUrl toDecrypt:content success:^(NSURLSessionDataTask *task, id responseObject) {
                        NSDictionary *dict = responseObject;
                        // new web service
                        NSDictionary *parameters = @{
                                                     @"requestFrom": @"ios",
                                                     @"userId": self.userNameTextField.text,
                                                     @"companyId": self.selectedBranchCode,
                                                     @"password": self.passwordTextField.text,
                                                     @"resultFlag":@"json"
                                                     };
                        
                        
                        [[[CMSServiceManager sharedCMSServiceManager]getSessionData:kCMSGetSessionURL withParameter:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
                            
                            NSDictionary *dict12=responseObject;
                            
                            NSDictionary *parameters = @{
                                                         @"key":[dict12 valueForKey:@"sessionID"],
                                                         @"resultFlag":@"json"
                                                         };
                            
                            [[[CMSServiceManager sharedCMSServiceManager]validateSessionData:kCMSValidateURL withParameter:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
                                NSDictionary *result=responseObject;
                                
                                if ([[result valueForKey:@"loginResult"] isEqualToString:@"success"]) {
                                    NSDate *dt=[NSDate date];
                                    NSDateFormatter *format=[[NSDateFormatter alloc]init];
                                    [format setDateFormat:kServerDateFormatte];
                                    NSString *dt1=[format stringFromDate:dt];
                                    AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
                                    NSString *agent=[delegate removeZeroFromAgentCode:self.userNameTextField.text];
                                    NSDictionary *loginDict=@{
                                                              @"logedInId":agent,
                                                              @"co":[NSString stringWithFormat:@"%@",self.selectedBranchCode],
                                                              @"logedInDate":dt1
                                                              };
                                    
                                    [[[CMSServiceManager sharedCMSServiceManager] setLoginValue:kAgentLoginDetails withParameter:loginDict success:^(NSURLSessionDataTask *task, id responseObject) {
                                        NSDictionary *responseLoginDict=responseObject;
                                        if ([[responseLoginDict valueForKey:@"status"]boolValue]) {
                                            AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
                                            [delegate setSessionid:[dict12 valueForKey:@"sessionID"]];
                                            [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                                            NSString *contentString = [dict objectForKey:@"output"];
                                            contentString = [contentString stringByReplacingOccurrencesOfString:@"NULL" withString:@"\"\""];
                                            NSData *data = [contentString dataUsingEncoding:NSUTF16StringEncoding];
                                            NSError *error;
                                            NSDictionary *contentDict =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                            [self executeActualLoginWithData:contentDict];
                                        } else {
                                            NSLog(@"failed");
                                            [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                                            [[[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"Please check UserName or Password or branch",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil] show];
                                        }
                                    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
                                        NSLog(@"failed:%@",error.description);
                                        [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                                        [[[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"Please check UserName or Password or branch",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil] show];
                                    }] resume];
                                } else {
                                    [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                                    [[[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"Please check UserName or Password or branch",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil] show];
                                }
                            } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
                                [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                            }] resume];
                        } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
                            [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                        }]resume];
                    } failure:^(NSURLSessionDataTask *task, id responseObject,NSError *error) {
                        [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                        
                    }] resume];
                } else {
                    [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                    [[[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"Please check UserName or Password or branch",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil] show];
                }
                
            } failure:^(NSURLSessionDataTask *task, id responseObject,NSError *error) {
                [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                //[[[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil] show];
            }] resume];
        }
        else
        {
            [self tryOffLineMode];
        }
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"Please check UserName or Password or branch",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil] show];
    }
}

- (void)showLoginView
{
    UIStoryboard *storyBoard = [self storyboard];
    HomeViewController *homeViewController  = [storyBoard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:homeViewController animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)notifi{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.view.frame = CGRectMake(self.view.frame.origin.x,0,self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    // Issue #75 - prefix userid with "0" so that the login id is at least 10 characters.
    if (textField == self.userNameTextField) {
        NSString *userID = self.userNameTextField.text;
        self.userNameTextField.text = [self getProcessedUserID:userID];;
        
    }
    return YES;
}

- (void)executeActualLoginWithData:(NSDictionary *)contentDict
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString  *agentCode;
    if ([[contentDict objectForKey:@"USERTYPE"] isEqualToString:@"AGENT"]) {
        agentCode= [self processCurrentUserId:contentDict];
        agentCode = [NSString stringWithFormat:@"%d", [agentCode intValue]];
        NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionaryWithDictionary:contentDict];
        [mutableDictionary setObject:agentCode forKey:@"USERID"];
        contentDict = mutableDictionary;
        
    } else {
        agentCode =  [[contentDict objectForKey:@"USERID"] stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    
    NSString *branchCode = [contentDict objectForKey:@"BRANCH"];
   
    [[NSUserDefaults standardUserDefaults] setObject:agentCode forKey:@"userID"];
    [CachingService sharedInstance].currentUserId = agentCode;
    [CachingService sharedInstance].currentUserIdWithZero = [appDelegate prepareAgentCodeWithZero:agentCode];
    [CachingService sharedInstance].branchCode = branchCode;
    [self secureStoreCurrentData:contentDict];
    [self.contactDataModel saveAgentLoginDetails:contentDict];
    [self showLoginView];
}



-(void) tryOffLineMode
{
    NSString *userID = [NSString stringWithFormat:@"%d", [self.userNameTextField.text intValue]];
    if (!userID) {
        userID = self.userNameTextField.text;
    }
    NSString *password = self.passwordTextField.text;
    
    userID = [NSString stringWithFormat:@"%d", [userID intValue]];
    
    // See if something has been stored.
    NSString *jsonString = [JNKeychain loadValueForKey:userID];
    if (jsonString != nil)
    {
        NSError * err;
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *fullContentDict = [NSJSONSerialization
                              JSONObjectWithData: jsonData
                              options: NSJSONReadingMutableContainers
                              error: &err];
        if (fullContentDict != nil)
        {
            NSString *pwd = [fullContentDict objectForKey:@"password"];
            NSString *dateString = [fullContentDict objectForKey:@"current"];
            
            // debugging
            //dateString = @"2015-08-09 09:38:45";
            
            NSDate *lastRead =[DateUtils stringToDate:dateString dateFormat:kServerDateFormatte];
            
            if ([DateUtils isDate:lastRead isLessThan:30])
            {
                // If more than 30 days, delete it.
                if ([pwd compare:password] == NSOrderedSame)
                {
                    // Update the timestamp of last use.
                    NSDictionary *contentDict = [fullContentDict objectForKey:@"data"];
                    [self secureStoreCurrentData:contentDict];
                    
                    // Do all the normal login stuff.
                    [self executeActualLoginWithData:contentDict];
                    return;
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"Password does not match the one used in last successful login ", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil] show];
                    return;
                }
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"Could not log in with the specified credentials", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil] show];
                
                // Delete old data.
                [JNKeychain deleteValueForKey:userID];
                return;
            }
        }
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"An internet connection is required for first time access", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil] show];
}

-(void) secureStoreCurrentData:(NSDictionary *)contentDict
{
    NSString *key = [contentDict valueForKey:@"USERID"];
    
    [JNKeychain deleteValueForKey:key];
    NSString *now = [DateUtils dateToString:[NSDate date] andFormate:kServerDateFormatte];
    NSError * err;
    NSDictionary *fullData = @{@"userid":key, @"password":self.passwordTextField.text, @"data":contentDict, @"current":now};
    NSData *jsonData = [NSJSONSerialization  dataWithJSONObject:fullData options:0 error:&err];
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [JNKeychain saveValue:str forKey:key];
}


- (NSString *)processCurrentUserId:(NSDictionary *)contentDict
{
    NSString *stringTeamCode = [[contentDict objectForKey:@"USERID"] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSInteger noofZeroes = USER_ID_LENGTH - stringTeamCode.length;
    NSMutableString *agentCode = [NSMutableString string];
    for (int i = 0;i<noofZeroes;i++) {
        [agentCode appendString:@"0"];
    }
    [agentCode appendString:stringTeamCode];
    
    return agentCode;
}

- (NSString *)getProcessedUserID:(NSString *)userID
{
    NSMutableString *newName = [NSMutableString stringWithString:userID];
    userID = [userID stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([userID isNumeric] && userID.length < USER_ID_LENGTH) {

        for (int i=userID.length; i<USER_ID_LENGTH; ++i) {
            [newName insertString:@"0" atIndex:0];
        }
    }
    
    return newName;
}


@end
