//
//  DropDownViewController.m
//  AIAChina
//
//  Created by AIA on 30/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "DropDownViewController.h"
#import "DropDownCell.h"

@interface DropDownViewController ()

@end

@implementation DropDownViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dropDownArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DropDownCell *cell = (DropDownCell *)[tableView dequeueReusableCellWithIdentifier:@"DropDownCell"];
    [cell loadDropDownData:self.dropDownArray[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.selectedValue)
        self.selectedValue(indexPath.row);
}

@end
