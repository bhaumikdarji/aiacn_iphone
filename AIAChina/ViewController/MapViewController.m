//
//  MapViewController.m
//  AIAChina
//
//  Created by MacMini on 16/09/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Map", @"");
    self.locService = [[BMKLocationService alloc]init];
    self.mapView.delegate = self;
    [self.locService startUserLocationService];
    self.mapView.showsUserLocation = NO;
    self.mapView.userTrackingMode = BMKUserTrackingModeNone;

    if (self.latitude != 0 && self.longitude != 0) {
        [self addPointAnnotation];
        self.region = BMKCoordinateRegionMakeWithDistance(self.coor, 0, 0);
        [self.mapView setRegion:self.region animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.locService.delegate = self;
    [self.mapView setMapType:BMKMapTypeStandard];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.mapView viewWillDisappear];
    self.mapView.delegate = nil;
    self.locService.delegate = nil;
}

- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    if (self.latitude == 0.000000 && self.longitude == 0.000000) {
        CLLocationCoordinate2D coor;
        self.latitude = userLocation.location.coordinate.latitude; //27.693510;
        self.longitude = userLocation.location.coordinate.longitude; //75.178514;
//        NSLog(@"latitude %f longitude %f ",self.latitude, self.longitude);
        coor.latitude = self.latitude;
        coor.longitude = self.longitude;
        self.region = BMKCoordinateRegionMakeWithDistance(coor, 0, 0);
        [self.mapView setRegion:self.region animated:YES];
        [self.mapView updateLocationData:userLocation];
        if (self.pointAnnotation == nil) {
            [self addPointAnnotation];
        }
    }
//    [self.mapView updateLocationData:userLocation];
}

- (void)addPointAnnotation{
    self.pointAnnotation = [[BMKPointAnnotation alloc]init];
    CLLocationCoordinate2D coor;
    coor.latitude = self.latitude;
    coor.longitude = self.longitude;
    self.coor = coor;
    self.pointAnnotation.coordinate = coor;
    [self.mapView addAnnotation:self.pointAnnotation];
    [self getLocationName];
}

- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation{
    NSString *AnnotationViewID = @"MARK";
    if (self.annotationNew == nil) {
        self.annotationNew = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
        ((BMKPinAnnotationView*)self.annotationNew).pinColor = BMKPinAnnotationColorRed;
        ((BMKPinAnnotationView*)self.annotationNew).animatesDrop = YES;
        ((BMKPinAnnotationView*)self.annotationNew).draggable = YES;
    }
    return self.annotationNew;
}

- (void)mapView:(BMKMapView *)mapView annotationView:(BMKAnnotationView *)view didChangeDragState:(BMKAnnotationViewDragState)newState
   fromOldState:(BMKAnnotationViewDragState)oldState{
    CLLocationCoordinate2D droppedAt = view.annotation.coordinate;
    self.latitude = droppedAt.latitude;
    self.longitude = droppedAt.longitude;
//    NSLog(@"dragged latitude %f longitude %f ",self.latitude, self.longitude);
    [self getLocationName];
}

- (void)getLocationName{
    self.locManager.delegate = self;
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:self.latitude longitude:self.longitude];
    [ceo reverseGeocodeLocation: loc
              completionHandler:^(NSArray *placemarks, NSError *error){
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                  self.locationTitle = locatedAt;
//                  NSLog(@"locationTitle :%@",self.locationTitle);
              }];
}

- (IBAction)onBtnDoneClick:(id)sender {
    if(self.locationBlock)
        self.locationBlock(self.latitude, self.longitude, self.locationTitle);
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
