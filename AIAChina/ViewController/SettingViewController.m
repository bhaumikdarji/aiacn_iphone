//
//  SettingViewController.m
//  AIAChina
//
//  Created by Tankar on 2015-07-18.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "SettingViewController.h"
#import "SettingCell.h"
#import "ContactDataProvider.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.cachingService = [CachingService sharedInstance];
    [self loadImages];
}

- (void)loadImages{
    
    self.agentProfile = (TblAgentDetails *)[DbUtils fetchObject:@"TblAgentDetails"
                                  andPredict:[NSPredicate predicateWithFormat:@"userID == %@", [CachingService sharedInstance].currentUserId]
                           andSortDescriptor:nil
                        managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    self.arrImages = [[NSArray alloc]init];
    if ([self.agentProfile.agentGallayImages count] > 0){
        self.arrImages = self.agentProfile.agentGallayImages;
    }
    [self.tblImages reloadData];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = [self prepareRightBarButton:YES];
    self.navigationItem.leftBarButtonItem = [self prepareLeftBarButton:YES withLogo:YES];
    self.navigationItem.title=NSLocalizedString(@"Setup", nil);
    [MyAppDelegate showAnnouncements:self.view]; 
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1; 
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int countImages = [self.arrImages count];
    if (countImages == 0) {
        return 1;
    }
    return [self.arrImages count] + 1;
}

- (SettingCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"settingCell";
    
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[ SettingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.layer.borderColor = [[UIColor grayColor] CGColor];
    cell.layer.borderWidth = 1.5;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < [self.arrImages count]) {
         cell.imgShowImage.image = [self.arrImages objectAtIndex:indexPath.row];
        [cell.btnAddImage setHidden:YES];
    } else {
        cell.imgShowImage.image = [[UIImage alloc] init];
        [cell.btnAddImage setHidden:NO];
    }
    [cell.btnAddImage addTarget:self action:@selector(btnAddImage:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *arr = self.agentProfile.agentGallayImages;
    if (indexPath.row == arr.count) {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *arr = self.agentProfile.agentGallayImages;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [arr removeObjectAtIndex:indexPath.row];
        self.agentProfile.agentGallayImages = arr;
        [tableView reloadData];
        arr = nil;
    }
}

- (void)handleImagePicker:(UIImagePickerController *)picker withMediaInfo:(NSDictionary *)info{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    if(info!=nil) {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        [self insertAgentImagesIntoDatabase:image];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

//    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
}

- (void)btnAddImage:(id)sender {
    UIButton *btn = (UIButton *)sender;
    [self showPhotoActionSheet:btn withCircularCrop1:YES withFullScreen:NO];
}

- (void)insertAgentImagesIntoDatabase:(UIImage *)img{
    
    NSMutableArray *arrImages = [NSMutableArray new];
    NSArray *arr = self.agentProfile.agentGallayImages;
    [arrImages addObjectsFromArray:arr];
    [arrImages addObject:img];
    self.agentProfile.agentGallayImages = arrImages;
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    arrImages = nil;
    [self loadImages];
}

@end
