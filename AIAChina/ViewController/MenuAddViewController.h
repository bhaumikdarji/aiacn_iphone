//
//  MenuAddViewController.h
//  AIAChina
//
//  Created by AIA on 30/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface MenuAddViewController : AIAChinaViewController

@property (copy, nonatomic) void(^showIndex)(int selectedIndex);
- (IBAction)onBtnAddContactClick:(id)sender;
- (IBAction)onBtnAddGroupClick:(id)sender;

@end
