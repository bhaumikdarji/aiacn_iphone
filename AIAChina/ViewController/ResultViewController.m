
//
//  ResultViewController.m
//  AIAChina
//
//  Created by MacMini on 09/10/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "ResultViewController.h"
#import "ContactEventModel.h"

@interface ResultViewController ()

@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getInterviewResultApi];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getInterviewResultApi{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:TRUE];
    [[ContactEventModel sharedInstance] getCandidateDetailsForCandidateCode:[self.contact.addressCode stringValue]
                                                 agentID:[CachingService sharedInstance].currentUserId
                                               eventCode:[self.interview.interviewCode stringValue]
                                                 success:^(id responseObject) {
                                                     [self loadData:responseObject];
                                                     [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                 } failure:^(id responseObject, NSError *error) {
                                                     [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                 }];
}

- (void)loadData:(NSDictionary *)dic{
    self.lblDate.text = [NSDate stringDateToString:[dic objectForKey:@"interviewDate"]
                                         currentDateForamte:kServerDateFormatte
                                         displayDateFormate:kDisplayDateFormatte];
    self.lblSessionName.text = [ValueParser parseString:[dic objectForKey:@"interviewSessionName"]];
    NSString *resultString = [ValueParser parseString:[dic objectForKey:@"interviewResult"]];
    self.lblResult.text = [resultString isEqualToString:@"P"] ?  NSLocalizedString(@"Pass",@"") : NSLocalizedString(@"Fail", @"");
    self.lblViewer.text = [ValueParser parseString:[dic objectForKey:@"interviwer_name"]];
//    self.lblCC.text = [ValueParser parseString:[dic objectForKey:@"ccResult"]];
    //for localization
    NSString  *str = [ValueParser parseString:[dic objectForKey:@"ccResult"]];
    if ([str isEqualToString:@"Urgent"]) {
        self.lblCC.text=NSLocalizedString(@"Urgent Recruit", nil);
    } else if ([str isEqualToString:@"Normal"]){
        self.lblCC.text=NSLocalizedString(@"Normal Recruit", nil);
    } else {
        self.lblCC.text=NSLocalizedString(@"Caution Recruit ", nil);
    }
    self.lblP100.text = [NSString stringWithFormat:@"%d",[ValueParser parseInt:[dic objectForKey:@"p100"]]];
    self.txtRemarks.text = [ValueParser parseString:[dic objectForKey:@"remarks"]];
    //self.txtRemarks.textColor = [UIColor colorWithRed:99.0/255.0 green:99.0/255.0 blue:99.0/255.0 alpha:1.0];
}

@end
