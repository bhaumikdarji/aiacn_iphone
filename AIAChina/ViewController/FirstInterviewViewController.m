//
//  FirstInterviewViewController.m
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "FirstInterviewViewController.h"
#import "ContactEventModel.h"
#import "ContactDataProvider.h"

@interface FirstInterviewViewController ()
@property (nonatomic, retain) ContactEventModel *eventModel;
@end

@implementation FirstInterviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title =  NSLocalizedString(@"First Interview", @"");
    
    self.eventModel = [ContactEventModel sharedInstance];
    
    [Utility setBorderAndRoundCorner:self.txtRemark
                         borderColor:[UIColor colorWithRed:221.0/255.0
                                                     green:221.0/255.0
                                                      blue:221.0/255.0
                                                     alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    
    self.oldValue = @"";
    if(self.contact.candidateProcess){
        if([self.contact.candidateProcess.firstInterviewResult isEqualToString:FIRST_INTERVIEW_PASS]){
            [self.btnPassFail[0] setSelected:TRUE];
            self.oldValue = FIRST_INTERVIEW_PASS;
        }else if([self.contact.candidateProcess.firstInterviewResult isEqualToString:FIRST_INTERVIEW_FAIL])
            [self.btnPassFail[1] setSelected:TRUE];
    
        if([self.contact.candidateProcess.firstInterviewRecruitment isEqualToString:FIRST_INTERVIEW_GA])
            [self.btnRecruitmentPlan[0] setSelected:TRUE];
        else if([self.contact.candidateProcess.firstInterviewRecruitment isEqualToString:FIRST_INTERVIEW_HA])
            [self.btnRecruitmentPlan[1] setSelected:TRUE];
        else if([self.contact.candidateProcess.firstInterviewRecruitment isEqualToString:FIRST_INTERVIEW_SA])
            [self.btnRecruitmentPlan[2] setSelected:TRUE];
        
        self.txtRemark.text = self.contact.candidateProcess.firstInterviewRemark;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnPassFailClick:(id)sender {
    for (UIButton *btnTmp in self.btnPassFail)
        [btnTmp setSelected:FALSE];
    
    [((UIButton *)sender) setSelected:TRUE];
}

- (IBAction)onBtnRecruitmentPlanClick:(id)sender {
    for (UIButton *btnTmp in self.btnRecruitmentPlan)
        [btnTmp setSelected:FALSE];
        
    [((UIButton *)sender) setSelected:TRUE];
}

- (IBAction)onBtnCancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onBtnSaveClick:(id)sender{

    if(self.contact.addressCode == 0){
        [DisplayAlert showAlertInControllerInView:self
                                            title:NSLocalizedString(@"Error", @"")
                                          message:NSLocalizedString(@"Please sync contact", @"")
                                cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }
    
    if ([self.btnPassFail[0] isSelected] &&
        (![self.btnRecruitmentPlan[1] isSelected] &&
         ![self.btnRecruitmentPlan[2] isSelected])) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                        message:NSLocalizedString(@"please select the recruitment scheme", @"")
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                              otherButtonTitles:nil] show];
            return;
        }
    
    NSString *result = @"";
    NSString *recruitmentPlan = @"";
    NSDate *passDate = [NSDate date];
    
    
    
    if([self.btnPassFail[0] isSelected]){
        result = FIRST_INTERVIEW_PASS;
    }else if([self.btnPassFail[1] isSelected])
        result = FIRST_INTERVIEW_FAIL;
    
    if([self.btnRecruitmentPlan[0] isSelected])
        recruitmentPlan = FIRST_INTERVIEW_GA;
    else if([self.btnRecruitmentPlan[1] isSelected])
        recruitmentPlan = FIRST_INTERVIEW_HA;
    else if([self.btnRecruitmentPlan[2] isSelected])
        recruitmentPlan = FIRST_INTERVIEW_SA;

    NSDictionary *dict = @{@"agentId": [CachingService sharedInstance].currentUserId,
                           @"candidateCode": [NSString stringWithFormat:@"%d",[self.contact.addressCode integerValue]],
                           @"interviewResult": result,
                           @"recruitmentPlan": recruitmentPlan,
                           @"remarks": isEmptyString(self.txtRemark.text) ? @"" : self.txtRemark.text,
                           @"passTime": [NSDate dateToString:passDate andFormate:kNewDisplayDateFormatte]};
    
    TblCandidateProcess *candidateProcess = self.contact.candidateProcess;
    if(!candidateProcess){
        candidateProcess = [[ContactDataProvider sharedContactDataProvider] createCandidateProcess];
        candidateProcess.contact = self.contact;
    }
    
    if(![self.oldValue isEqualToString:result] || ![recruitmentPlan isEqualToString:candidateProcess.firstInterviewRecruitment]){
        candidateProcess.firstInterviewDate = passDate;
        candidateProcess.completeProgressDate = [NSDate date];
    }
    
    candidateProcess.firstInterviewStatus = ([result isEqualToString:FIRST_INTERVIEW_PASS]) ? RECURITMENT_STATUS_TRUE : RECURITMENT_STATUS_FALSE;
    candidateProcess.firstInterviewResult = result;
    candidateProcess.firstInterviewRecruitment = recruitmentPlan;
    candidateProcess.firstInterviewRemark = self.txtRemark.text;
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:FALSE];
    [self.eventModel postFirstInterview:dict success:^(id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:FALSE];
        [self dismissViewControllerAnimated:YES completion:nil];
        if(self.callBackBlock)
            self.callBackBlock();
    } failure:^(id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:FALSE];
        [self dismissViewControllerAnimated:YES completion:nil];
        if(self.callBackBlock)
            self.callBackBlock();
    }];
}


@end
