//
//  BirthDayViewController.h
//  AIAChina
//
//  Created by SCS2 on 09/09/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface BirthDayViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UITableView *tblBirthDay;
@property (strong, nonatomic) NSArray *birthDayArray;
@property (nonatomic, strong) NSMutableArray *selectedContactArray;
@property (nonatomic, strong) NSDate *birthDate;
@property (copy, nonatomic) void(^cancelBirthDayView)();
@property (copy, nonatomic) void(^sendEGreeting)(NSMutableArray *selectedEmailId);

- (IBAction)onBtnCancelClick:(id)sender;
- (IBAction)onBtnSendEGreetingClick:(id)sender;

@end
