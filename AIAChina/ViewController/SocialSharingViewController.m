//
//  SocialSharingViewController.m
//  AIAChina
//
//  Created by MacMini on 24/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "SocialSharingViewController.h"
#import "CachingService.h"

@interface SocialSharingViewController ()

@end

@implementation SocialSharingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onBtnQQClick:(id)sender {
    if(self.isGreeting){
        if(isEmptyString(self.contact.qq)){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Please add QQ id", @"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:@""
                                          cancelBlock:nil
                                              okBlock:nil];
            return;
        }
    }
    
    if (!self.shareMsg) {
        self.shareMsg = [self EopBody];
    }
    [self shareQQ:self.shareImage
          subject:self.shareSub
              msg:self.shareMsg
    callBackBlock:^(NSString *errorMsg) {
        self.shareMsg = nil;
        if(!isEmptyString(errorMsg))
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:errorMsg
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:@""
                                          cancelBlock:nil
                                              okBlock:nil];
    }];
}

- (IBAction)onBtnWeChatClick:(id)sender {
    
    UIImage *wechatImage = self.shareImage;
    NSString *wechatMessage = self.shareMsg;
    
    if (!self.shareMsg) {
        wechatMessage = self.shareMsg = [self EopBody];
    }
    
    if (self.eop) {
        wechatImage = nil;
    }
    if (self.isQRCode || self.isGreeting) {
        wechatMessage = nil;
    }
    
    
    
    [self shareWeChat:sender image:wechatImage subject:self.shareSub msg:wechatMessage callBackBlock:^(BOOL isInstalled, BOOL isSuccess) {
        self.shareMsg = nil;
        if(!isInstalled){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"WeChat dont installed", @"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:@""
                                          cancelBlock:nil
                                              okBlock:nil];
        }else if (!isSuccess){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Share unsuccessful", @"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:@""
                                          cancelBlock:nil
                                              okBlock:nil];
        }else if(isSuccess){
            [self dismissViewControllerAnimated:TRUE completion:nil];
        }
    }];
//    if(self.isGreeting){
//        if(isEmptyString(self.contact.weChat)){
//            [DisplayAlert showAlertInControllerInView:self
//                                                title:@""
//                                              message:NSLocalizedString(@"Please add wechat id", @"")
//                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
//                                        okButtonTitle:@""
//                                          cancelBlock:nil
//                                              okBlock:nil];
//
//            return;
//        }
//    }
//
//
//    
//    if(self.isEOP){
//        [self displayActionSheet:sender];
//    }else{
//        if (!self.shareMsg) {
//            self.shareMsg = [self EopBody];
//        }
//        [self shareMsgMoment:FALSE image:self.shareImage sub:self.shareSub msg:self.shareMsg];
//    }
}

- (void)displayActionSheet:(id)sender{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    alert.view.tintColor = [UIColor colorWithRed:45.0/255.0
                                           green:45.0/255.0
                                            blue:45.0/255.0
                                           alpha:1.0];
    
    UIAlertAction *shareMsg = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Share message", @"")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action){
                                   if (!self.shareMsg) {
                                       self.shareMsg = [self EopBody];
                                   }
                                   [self shareMsgMoment:FALSE image:self.shareImage sub:self.shareSub msg:self.shareMsg];
                               }];
    
    UIAlertAction *shareMoment = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Share Moment", @"")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action){
                                        if (!self.shareMsg) {
                                            self.shareMsg = [self EopBody];
                                        }
                                        [self shareMsgMoment:TRUE image:self.shareImage sub:self.shareSub msg:self.shareMsg];
                                    }];
    
    [alert addAction:shareMsg];
    [alert addAction:shareMoment];
    
    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    popPresenter.sourceView = self.view;
    [popPresenter setPermittedArrowDirections:UIPopoverArrowDirectionAny];
    popPresenter.sourceRect = self.view.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)shareMsgMoment:(BOOL)isMoment image:(UIImage *)image sub:(NSString *)sub msg:(NSString *)msg{
//    [self shareWeChat:image
//              subject:sub
//                  msg:msg
//             isMoment:isMoment
//        callBackBlock:^(BOOL isInstalled, BOOL isSuccess) {
//            self.shareMsg = nil;
//            if(!isInstalled){
//                [DisplayAlert showAlertInControllerInView:self
//                                                    title:@""
//                                                  message:NSLocalizedString(@"WeChat dont installed", @"")
//                                        cancelButtonTitle:NSLocalizedString(@"OK", @"")
//                                            okButtonTitle:@""
//                                              cancelBlock:nil
//                                                  okBlock:nil];
//            }else if (!isSuccess){
//                [DisplayAlert showAlertInControllerInView:self
//                                                    title:@""
//                                                  message:NSLocalizedString(@"Share unsuccessful", @"")
//                                        cancelButtonTitle:NSLocalizedString(@"OK", @"")
//                                            okButtonTitle:@""
//                                              cancelBlock:nil
//                                                  okBlock:nil];
//            }else if(isSuccess){
//                [self dismissViewControllerAnimated:TRUE completion:nil];
//            }
//        }];
}

- (IBAction)onBtnEmailClick:(id)sender {
    if(self.isGreeting){
        if(isEmptyString(self.contact.eMailId)){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Plese add email address", @"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:@""
                                          cancelBlock:nil
                                              okBlock:nil];
            return;
        }
        
        NSString *fileName = [self.shareImageFile stringByDeletingPathExtension];
        NSURL *imgPath = [[NSBundle mainBundle] URLForResource:fileName withExtension:@"jpg"];
        NSString*stringPath = [imgPath absoluteString];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:stringPath]];
//        unsigned long long fileSize = data.length;
//        if (fileSize < (unsigned long long)15728640){
//            return;
//        }
    }else{
        self.shareImageFile = [NSString stringWithFormat: @"%@_QR_code.jpg",APP_NAME];
        if(self.contact && self.isQRCode){
            self.shareImage = [ApplicationFunction generateBarcodeImageWithQRCode:[self.contact getLocalizedQRCodeString]];
           
        }
    }
    if (self.isQRCode) {
        self.shareMsg = @"";
    }
    if (self.isEOP && self.eop)
     self.shareMsg = [self EopBody];
    

    [self shareEmail:self.shareImage
           recipient:(self.contact.eMailId) ? @[self.contact.eMailId] : nil
            fileName:self.shareImageFile
             subject:self.shareSub
                 msg:self.shareMsg
       callBackBlock:^(BOOL isInstalled) {
           self.shareMsg = nil;
           if(!isInstalled)
               [DisplayAlert showAlertInControllerInView:self
                                                   title:NSLocalizedString(@"can not send email,please configure email account!", @"")
                                                 message:@""
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                           okButtonTitle:@""
                                             cancelBlock:nil
                                                 okBlock:nil];
           
       }];
}

- (NSMutableString *)EopBody{
    NSMutableString *content = [NSMutableString stringWithString:@""];
//    [content appendFormat:@"尊敬的 %@",self.contact.name];
//    [content appendFormat:@"\r\n您报名的活动进行了更新，请重新查看活动信息："];
    [content appendFormat:@"活动名称： %@",[(NSString *)self.eop.eventName length] > 0 ? self.eop.eventName : @""];
    NSString *eventDateString =  [[NSDate dateToString:self.eop.eventDate
                                         andFormate:kDisplayDateFormatte] lowercaseString];
    [content appendFormat:@"\r\n活动日期： %@",eventDateString.length > 0 ? eventDateString: @""];
    NSString *dateString = [[NSDate stringDateToString:self.eop.startTime
                                    currentDateForamte:kServerDateFormatte
                                    displayDateFormate:@"HH:mma"] lowercaseString];
    [content appendFormat:@"\r\n活动开始时间： %@",dateString.length > 0 ? dateString: @""];
    [content appendFormat:@"\r\n主讲人：%@",[(NSString *)self.eop.speaker length] > 0 ? self.eop.speaker : @""];
    [content appendFormat:@"\r\n地点：%@",[(NSString *)self.eop.location length] > 0 ? self.eop.location : @""];
    [content appendFormat:@"\r\n描述：%@",[(NSString *)self.eop.eopDescription length] > 0 ? self.eop.eopDescription : @""];
    [content appendFormat:@"\r\n注册网址：%@", (isEmptyString(self.eop.publicUrl)) ? @"" : self.eop.publicUrl];
    [content appendFormat:@"\r\n祝您：身体健康 万事如意"];
    [content appendFormat:@"\r\n您的伙伴：%@",[[TblAgentDetails getCurrentAgentDetails] userName]];
    return content;
}

@end
