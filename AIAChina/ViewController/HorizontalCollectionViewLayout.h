//
//  HorizontalCollectionViewLayout.h
//  AIAChina
//
//  Created by imac on 18/01/16.
//  Copyright © 2016 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol pagenumberDelegate <NSObject>
@optional
-(void)setTotalPage:(int)pagenumber;
-(void)setCurrentPage:(int)currentPage;
@end

@interface HorizontalCollectionViewLayout : UICollectionViewLayout
@property (nonatomic, assign) CGSize itemSize;
@property (nonatomic,weak) id<pagenumberDelegate> delegate;
@end
