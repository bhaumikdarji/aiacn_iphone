//
//  CandidateRankingViewController.m
//  AIAChina
//
//  Created by MacMini on 22/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CandidateRankingViewController.h"
#import "RegisterSuccessfulCell.h"
#import "DropDownManager.h"

@interface CandidateRankingViewController ()

@end

@implementation CandidateRankingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utility setRoundCornerRedius:self.mainView
                     cornerRedius:5.0];
    [Utility setRoundCornerRedius:self.btnSearch
                     cornerRedius:5.0];
    [self setGAMAData];
    
    
    
    NSString *str = [NSString stringWithFormat:NSLocalizedString(@"Agent Code:%@", @""), self.candidateAgentCode];
    NSRange range = [str rangeOfString:[NSString stringWithFormat:@"%@",[CachingService sharedInstance].currentUserId]];
    [self.lblRanking setAttributedText:[Utility setBoldTextOnSingleLable:str fontSize:12.0 range:range setBoldFontColor:[UIColor whiteColor]]];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadData{
//    int maxRange = self.candidateProgressRank + 2;
//    int minRange = self.candidateProgressRank - 2;
    
    self.candidateArray = [DbUtils fetchAllObject:@"TblContact"
                                       andPredict:[NSPredicate predicateWithFormat:@"candidateProcess.candidateRank >= 1 AND agentId == %@ AND isDelete == 0", [CachingService sharedInstance].currentUserId]
                                andSortDescriptor:[NSSortDescriptor sortDescriptorWithKey:@"candidateProcess.candidateRank" ascending:TRUE]
                             managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    [self.tblCandidateRanking reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.candidateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RegisterSuccessfulCell *cell = (RegisterSuccessfulCell *)[tableView dequeueReusableCellWithIdentifier:@"CandidateRankingCell"];
    TblContact *contact = self.candidateArray[indexPath.row];
    [cell loadData:contact];
    
    if (contact.candidateProcess.candidateRankValue == self.candidateProgressRank) {
        cell.lblRank.backgroundColor = [UIColor colorWithRed:250.0/255.0
                                                         green:190.0/255.0
                                                          blue:83.0/255.0
                                                         alpha:1.0];
        cell.lblRank.textColor = [UIColor whiteColor];
    }else{
        cell.lblRank.backgroundColor = [UIColor clearColor];
        cell.lblRank.textColor = [UIColor blackColor];
    }
    if(indexPath.row % 2 == 0)
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0
                                               green:239.0/255.0
                                                blue:241.0/255.0
                                               alpha:1.0];
    else
        cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

- (void)setGAMAData{
    NSArray *gamaArray = [DropDownManager sharedManager].gamaContentEnglish;
    NSDictionary *gamaDic = [gamaArray objectAtIndex:arc4random() % [gamaArray count]];
    
    self.lblGAMAQuoteEnglish.text = [gamaDic objectForKey:@"Description_en"];
    self.lblGAMAAuthorEnglish.text = [gamaDic objectForKey:@"Author_en"];
    self.lblGAMAQuoteChinese.text = [gamaDic objectForKey:@"Description_cn"];
}

- (IBAction)onBtnCancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onBtnSearch:(id)sender {
    if(self.searchBlock){
        [self dismissViewControllerAnimated:YES completion:nil];
        self.searchBlock();
    }
}

@end
