//
//  AddressPickerViewController.m
//  AIAChina
//
//  Created by MacMini on 15/10/15.
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AddressPickerViewController.h"
#import "DropDownManager.h"
#import "ContactDataProvider.h"
#import "CachingService.h"

#define FIRST_PICKER 111
#define SECOND_PICKER 222
#define THIRD_PICKER 333

@interface AddressPickerViewController ()

@end

@implementation AddressPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectedIndexA = 0;
    self.selectedIndexB = 0;
    self.selectedIndexC = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadFirstPickerData];
}

- (void)loadJSONInDb{
    for (NSDictionary *addressDic in [[DropDownManager sharedManager] getDropDownArray:DROP_DOWN_ADDRESS]) {
        self.address = [[ContactDataProvider sharedContactDataProvider] createAddress];
        self.address.field = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"FIELD1"]]];
        self.address.addressId = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"ID"]]];
        self.address.rgCodeA = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGCODEA"]]];
        self.address.rgCodeB = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGCODEB"]]];
        self.address.rgCodeC = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGCODEC"]]];
        self.address.rgFiller1 = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGFILLER1"]]];
        self.address.rgFiller2 = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGFILLER2"]]];
        self.address.rgLevel = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGLEVEL"]]];
        self.address.rgName = [ValueParser parseString:addressDic[@"RGNAME"]];
        self.address.rgsName = [ValueParser parseString:addressDic[@"RGSNAME"]];
        [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    }
}

- (IBAction)onBtnCancelClick:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)onBtnDoneClick:(id)sender {
    if(self.callBackBlock)
        self.callBackBlock(self.rgCodeCArray[self.selectedIndexC]);
        
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (void)loadFirstPickerData{
    self.rgCodeAArray = [DbUtils fetchAllObject:@"TblAddress"
                                     andPredict:[NSPredicate predicateWithFormat:@"rgLevel = 1"]
                              andSortDescriptor:nil
                           managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    [self.addressPicker1 reloadAllComponents];
    [self loadSecondPickerData];
}

- (void)loadSecondPickerData{
    self.address = self.rgCodeAArray[self.selectedIndexA];
    self.rgCodeBArray = [DbUtils fetchAllObject:@"TblAddress"
                                     andPredict:[NSPredicate predicateWithFormat:@"rgLevel = 2 AND rgCodeA = %d", [self.address.rgCodeA integerValue]]
                              andSortDescriptor:nil
                           managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    [self.addressPicker2 reloadAllComponents];
    [self performSelector:@selector(loadThirdPickerData) withObject:nil afterDelay:0.5];
}

- (void)loadThirdPickerData{
    self.address = self.rgCodeBArray[self.selectedIndexB];
    self.rgCodeCArray = [DbUtils fetchAllObject:@"TblAddress"
                                     andPredict:[NSPredicate predicateWithFormat:@"rgLevel = 3 AND rgCodeB = %d", [self.address.rgCodeB integerValue]]
                              andSortDescriptor:nil
                           managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    [self.addressPicker3 reloadAllComponents];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == FIRST_PICKER)
        return self.rgCodeAArray.count;
    else if(pickerView.tag == SECOND_PICKER)
        return self.rgCodeBArray.count;
    else if(pickerView.tag == THIRD_PICKER)
        return self.rgCodeCArray.count;
    else
        return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == FIRST_PICKER){
        self.address = self.rgCodeAArray[row];
        return [NSString stringWithFormat:@"%@",self.address.rgName];
    }else if(pickerView.tag == SECOND_PICKER){
        self.address = self.rgCodeBArray[row];
        return [NSString stringWithFormat:@"%@",self.address.rgName];
    }else if(pickerView.tag == THIRD_PICKER){
        self.address = self.rgCodeCArray[row];
        return [NSString stringWithFormat:@"%@",self.address.rgName];
    }else
        return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag == FIRST_PICKER){
        self.selectedIndexA = row;
        [self loadSecondPickerData];
    }else if(pickerView.tag == SECOND_PICKER){
        self.selectedIndexB = row;
        [self loadThirdPickerData];
    }else if(pickerView.tag == THIRD_PICKER){
        self.selectedIndexC = row;
    }
}

@end
