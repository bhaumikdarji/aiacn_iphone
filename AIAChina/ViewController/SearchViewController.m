//
//  SearchViewController.m
//  AIAChina
//
//  Created by AIA on 21/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchCell.h"
#import "PickerPopoverViewController.h"
#import "DropDownManager.h"
#import "NSDate+Escort.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([CachingService sharedInstance].searchForContactCompleted == YES) {
        self.autoSearch = YES;
        [CachingService sharedInstance].searchForContactCompleted = NO;
    }
    self.navigationItem.title = NSLocalizedString(@"Search", @"");
    self.maritalStatusArray = [[NSMutableArray alloc] init];
    self.recuritmentArray = [[NSMutableArray alloc] init];
    self.contactArray = [[NSMutableArray alloc] init];
    self.lastContactedArray = [[NSArray alloc]initWithObjects:NSLocalizedString(@"01 month", @""),
                                                              NSLocalizedString(@"02 month", @""),
                                                              NSLocalizedString(@"03 month", @""),
                                                              NSLocalizedString(@"04 month", @""),
                                                              NSLocalizedString(@"05 month", @""),
                                                              NSLocalizedString(@"06 month", @""),
                                                              NSLocalizedString(@"07 month", @""),
                                                              NSLocalizedString(@"08 month", @""),
                                                              NSLocalizedString(@"09 month", @""),
                                                              NSLocalizedString(@"10 month", @""),
                                                              NSLocalizedString(@"11 month", @""),
                                                              NSLocalizedString(@"12 month", @""), nil];
    
    if(self.autoSearch){
        [self autoRecruitmentSearch];
    }
    [self setGUI];
}

- (void)viewDidAppear:(BOOL)animated{
    self.scrollView.contentOffset = CGPointZero;
}

- (void)viewWillAppear:(BOOL)animated{
     self.scrollView.contentOffset = CGPointZero;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setGUI{
    [Utility setBorderAndRoundCorner:self.btnLastContacted
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    [Utility setBorderAndRoundCorner:self.btnGender
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    [Utility setBorderAndRoundCorner:self.btnEducation
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    [Utility setBorderAndRoundCorner:self.btnRecommendScheme
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    [Utility setBorderAndRoundCorner:self.btnSourceOfReferral
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    
    [Utility setBorderAndRoundCorner:self.lblNotesDateToBG
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    [Utility setBorderAndRoundCorner:self.lblNotesDateFromBG
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    [Utility setBorderAndRoundCorner:self.lblDOBFromBG
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    [Utility setBorderAndRoundCorner:self.lblDOBToBG
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    [self.btnNotesDateFrom setTitle:@"YYYY-MM-DD" forState:UIControlStateNormal];
    [self.btnNotesDateTo setTitle:@"YYYY-MM-DD" forState:UIControlStateNormal];
    [self.btnDOBFrom setTitle:@"YYYY-MM-DD" forState:UIControlStateNormal];
    [self.btnDOBTo setTitle:@"YYYY-MM-DD" forState:UIControlStateNormal];
    [self.txtName setPlaceholder:NSLocalizedString(@"Type here", @"")];
    [self.txtAgeFrom setPlaceholder:NSLocalizedString(@"Type here", @"")];
    [self.txtAgeTo setPlaceholder:NSLocalizedString(@"Type here", @"")];
}

- (IBAction)onBtnCloseClick:(id)sender {
    [self dismissViewControllerAnimated:TRUE
                             completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *str = NSLocalizedString(@"Search Results", @"");
    self.lblSearchResult.text = [NSString stringWithFormat:str,self.contactArray.count];
    return self.contactArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isNote){
        TblContact *contact = [self.contactArray objectAtIndex:indexPath.row];
        if([self getNoteArray:contact].count >= 4){
            return 371;
        }else{
            return 195 + (44 * [self getNoteArray:contact].count);
        }
    }else
        return 155;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchCell *cell = nil;
    TblContact *contact = [self.contactArray objectAtIndex:indexPath.row];
    if(self.isNote){
        cell = (SearchCell *)[tableView dequeueReusableCellWithIdentifier:@"SearchNoteCell"];
        [cell loadSeachNoteData:[self getNoteArray:contact]];
    }else{
        cell = (SearchCell *)[tableView dequeueReusableCellWithIdentifier:@"SearchCell"];
    }
    [cell loadSeachData:contact];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSArray *)getNoteArray:(TblContact *)contact{
    return [[contact.candidateNotes allObjects] filteredArrayUsingPredicate:[self displayNotePredicate]];
}

- (NSPredicate *)displayNotePredicate{
    NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    NSPredicate *predicate = nil;

    if(!isEmptyString(self.btnNotesDateFrom.currentTitle) && ![self.btnNotesDateFrom.currentTitle isEqualToString:@"YYYY-MM-DD"]){
        predicate = [NSPredicate predicateWithFormat:@"activityDate >= %@",[DateUtils stringToDate:self.btnNotesDateFrom.currentTitle dateFormat:kDisplayDateFormatte]];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.btnNotesDateTo.currentTitle) && ![self.btnNotesDateTo.currentTitle isEqualToString:@"YYYY-MM-DD"]){
        predicate = [NSPredicate predicateWithFormat:@"activityDate <= %@",[DateUtils stringToDate:self.btnNotesDateTo.currentTitle dateFormat:kDisplayDateFormatte]];
        [predicateArray addObject:predicate];
    }
    return [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
}

- (void)setPopoverView:(UIButton *)btn pickerArray:(NSArray *)pickerArray{

    NSMutableArray *pickerDataArray = [[NSMutableArray alloc] initWithObjects:@"", nil];
    if (![btn isEqual:self.btnLastContacted])
        [pickerDataArray addObjectsFromArray:[pickerArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    else
        [pickerDataArray addObjectsFromArray:pickerArray];
    
    PickerPopoverViewController *popoverViewCtrl = [[PickerPopoverViewController alloc] initWithNibName:@"PickerPopoverViewController" bundle:nil];
    if(isEmptyString(btn.titleLabel.text))
        [btn setTitle:pickerDataArray[0] forState:UIControlStateNormal];
    else
        popoverViewCtrl.oldValue = btn.titleLabel.text;
    [popoverViewCtrl setPickerArray:pickerDataArray];
    [popoverViewCtrl setSelectedIndex:^(int selectedRowIndexPath) {
        if(btn.tag == 110 && selectedRowIndexPath == 0){
        }else{
            [btn setTitle:pickerDataArray[selectedRowIndexPath] forState:UIControlStateNormal];
            self.selectedMonthIndex = selectedRowIndexPath;
        }
        [self.searchPopover dismissPopoverAnimated:TRUE];
    }];
    self.searchPopover = [[UIPopoverController alloc] initWithContentViewController:popoverViewCtrl];
    self.searchPopover.popoverContentSize = popoverViewCtrl.view.frame.size;
    [self.searchPopover presentPopoverFromRect:btn.frame
                                            inView:self.scrollView
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:TRUE];
}

- (IBAction)onBtnGenderClick:(id)sender {
    [self.view endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_GENDER]];
}

- (IBAction)onBtnLastContactedClick:(id)sender {
    [self setPopoverView:sender pickerArray:self.lastContactedArray];
}

- (IBAction)onBtnEducationClick:(id)sender {
    [self.view endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_EDUCATION]];
}

- (IBAction)onBtnMaritalStatusClick:(id)sender {
    
    [self.view endEditing:true];
    
    UIButton *btn = (UIButton *)sender;
    if([btn isSelected]){
        [btn setSelected:FALSE];
    }else{
        [btn setSelected:TRUE];
    }
    
    if([[self.btnMaritalStatus objectAtIndex:0] isSelected]){
        if (![self.maritalStatusArray containsObject:ENUM_MARITAL_SINGLE])
            [self.maritalStatusArray addObject:ENUM_MARITAL_SINGLE];
    }else
        [self.maritalStatusArray removeObject:ENUM_MARITAL_SINGLE];
    
    if([[self.btnMaritalStatus objectAtIndex:1] isSelected]){
        if (![self.maritalStatusArray containsObject:ENUM_MARITAL_MARRIED])
            [self.maritalStatusArray addObject:ENUM_MARITAL_MARRIED];
    }else
        [self.maritalStatusArray removeObject:ENUM_MARITAL_MARRIED];
    
    if([[self.btnMaritalStatus objectAtIndex:2] isSelected]){
        if (![self.maritalStatusArray containsObject:ENUM_MARITAL_DIVORCED])
            [self.maritalStatusArray addObject:ENUM_MARITAL_DIVORCED];
    }else
        [self.maritalStatusArray removeObject:ENUM_MARITAL_DIVORCED];
    
    if([[self.btnMaritalStatus objectAtIndex:3] isSelected]){
        if (![self.maritalStatusArray containsObject:ENUM_MARITAL_WIDOWED])
            [self.maritalStatusArray addObject:ENUM_MARITAL_WIDOWED];
    }else
        [self.maritalStatusArray removeObject:ENUM_MARITAL_WIDOWED];
}

- (IBAction)onBtnRecommendSchemeClick:(id)sender {
    [self.view endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_RECOMMEND_SCHEME]];
}

- (IBAction)onBtnSourceofReferral:(id)sender {
    [self.view endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_SOURCE_OF_REFERRAL]];
}

- (IBAction)onBtnRecuritmentStepsClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if([btn isSelected]){
        [btn setSelected:FALSE];
    }else{
        [btn setSelected:TRUE];
    }
    
    if([self.btnRecuritmentSteps[0] isSelected]){
        if(![self.recuritmentArray containsObject:RECURITMENT_INTERACTIVE_PRESENER])
            [self.recuritmentArray addObject:RECURITMENT_INTERACTIVE_PRESENER];
    }else
        [self.recuritmentArray removeObject:RECURITMENT_INTERACTIVE_PRESENER];
    
    if([self.btnRecuritmentSteps[1] isSelected]){
      if(![self.recuritmentArray containsObject:RECURITMENT_COMPANY_INTERVIEW])
          [self.recuritmentArray addObject:RECURITMENT_COMPANY_INTERVIEW];
    }else
        [self.recuritmentArray removeObject:RECURITMENT_COMPANY_INTERVIEW];
    
    if([self.btnRecuritmentSteps[2] isSelected]){
        if(![self.recuritmentArray containsObject:RECURITMENT_EOP_REGISTRATION])
            [self.recuritmentArray addObject:RECURITMENT_EOP_REGISTRATION];
    }else
        [self.recuritmentArray removeObject:RECURITMENT_EOP_REGISTRATION];
    
    if([self.btnRecuritmentSteps[3] isSelected]){
        if(![self.recuritmentArray containsObject:RECURITMENT_ALE_EXAM])
            [self.recuritmentArray addObject:RECURITMENT_ALE_EXAM];
    }else
        [self.recuritmentArray removeObject:RECURITMENT_ALE_EXAM];
    
    if([self.btnRecuritmentSteps[4] isSelected]){
        if(![self.recuritmentArray containsObject:RECURITMENT_FIRST_INTERVIEW])
            [self.recuritmentArray addObject:RECURITMENT_FIRST_INTERVIEW];
    }else
        [self.recuritmentArray removeObject:RECURITMENT_FIRST_INTERVIEW];
    
    if([self.btnRecuritmentSteps[5] isSelected]){
        if(![self.recuritmentArray containsObject:RECURITMENT_ABC_TRAINING])
            [self.recuritmentArray addObject:RECURITMENT_ABC_TRAINING];
    }else
        [self.recuritmentArray removeObject:RECURITMENT_ABC_TRAINING];
    
    if([self.btnRecuritmentSteps[6] isSelected]){
        if(![self.recuritmentArray containsObject:RECURITMENT_ATTEND_EOP])
            [self.recuritmentArray addObject:RECURITMENT_ATTEND_EOP];
    }else
        [self.recuritmentArray removeObject:RECURITMENT_ATTEND_EOP];
    
    if([self.btnRecuritmentSteps[7] isSelected]){
        if(![self.recuritmentArray containsObject:RECURITMENT_CONTRACT])
            [self.recuritmentArray addObject:RECURITMENT_CONTRACT];
    }else
        [self.recuritmentArray removeObject:RECURITMENT_CONTRACT];
    
    if([self.btnRecuritmentSteps[8] isSelected]){
        if(![self.recuritmentArray containsObject:RECURITMENT_CC_TEST])
            [self.recuritmentArray addObject:RECURITMENT_CC_TEST];
    }else
        [self.recuritmentArray removeObject:RECURITMENT_CC_TEST];
}

- (IBAction)onBtnNotesDateToClick:(id)sender {
    [self setDatePopover:sender];
}

- (IBAction)onBtnNotesDateFromClick:(id)sender {
    [self setDatePopover:sender];
}

- (IBAction)onBtnDOBFromClick:(id)sender {
    [self setDatePopover:sender];
}

- (IBAction)onBtnDOBToClick:(id)sender {
    [self setDatePopover:sender];
}

- (void)setDatePopover:(UIButton *)sender{
    [self showDatePopoverCtrl:UIDatePickerModeDate
                  minimumDate:nil
                  maximumDate:[NSDate date]
                         rect:sender.frame
                       inView:self.scrollView
                   pickerDate:^(NSDate *pickerDate) {
                       [sender setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:kDisplayDateFormatte]] forState:UIControlStateNormal];
                   }];
}

- (IBAction)onBtnResetClick:(id)sender {
    self.txtName.text = @"";
    [self.btnGender setTitle:NSLocalizedString(@"Please select", @"") forState:UIControlStateNormal];
    self.txtAgeFrom.text = @"";
    self.txtAgeTo.text = @"";
    [self.btnLastContacted setTitle:NSLocalizedString(@"Please select", @"") forState:UIControlStateNormal];
    [self.btnEducation setTitle:NSLocalizedString(@"Please select", @"") forState:UIControlStateNormal];
    [self.btnRecommendScheme setTitle:NSLocalizedString(@"Please select", @"") forState:UIControlStateNormal];
    [self.btnNotesDateFrom setTitle:@"YYYY-MM-DD" forState:UIControlStateNormal];
    [self.btnNotesDateTo setTitle:@"YYYY-MM-DD" forState:UIControlStateNormal];
    [self.btnDOBFrom setTitle:@"YYYY-MM-DD" forState:UIControlStateNormal];
    [self.btnDOBTo setTitle:@"YYYY-MM-DD" forState:UIControlStateNormal];
    [self.btnSourceOfReferral setTitle:NSLocalizedString(@"Please select", @"") forState:UIControlStateNormal];
    
    for (UIButton *btnMarital in self.btnMaritalStatus)
        [btnMarital setSelected:FALSE];
    
    for (UIButton *btnRecuritment in self.btnRecuritmentSteps)
        [btnRecuritment setSelected:FALSE];
    
    [self.maritalStatusArray removeAllObjects];
    [self.recuritmentArray removeAllObjects];
    [self.contactArray removeAllObjects];
    [self.tblSearch reloadData];
}

- (IBAction)onBtnSearchClick:(id)sender {
    self.isNote = FALSE;
    NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    NSPredicate *predicate = nil;
    
    if(!isEmptyString(self.txtName.text)){
        predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", self.txtName.text];
        [predicateArray addObject:predicate];
    }
    if((!isEmptyString(self.btnGender.currentTitle)) && ![self.btnGender.currentTitle isEqualToString:NSLocalizedString(@"Please select", @"")]){
        predicate = [NSPredicate predicateWithFormat:@"gender contains[cd] %@", [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_GENDER
                                                                                                          description:self.btnGender.currentTitle]];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.txtAgeFrom.text)){
        predicate = [NSPredicate predicateWithFormat:@"age >= %d", [self.txtAgeFrom.text integerValue]];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.txtAgeTo.text)){
        predicate = [NSPredicate predicateWithFormat:@"age <= %d", [self.txtAgeTo.text integerValue]];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.btnLastContacted.currentTitle) && ![self.btnLastContacted.currentTitle isEqualToString:NSLocalizedString(@"Please select", @"")]){
//        NSDate *startDate = [DateUtils monthAgo:self.selectedMonthIndex];
//        if(self.selectedMonthIndex == 12)
//            predicate = [NSPredicate predicateWithFormat:@"lastContactedDate <= %@", startDate];
//        else
//            predicate = [NSPredicate predicateWithFormat:@"lastContactedDate >= %@ AND lastContactedDate <= %@", startDate,[DateUtils addOneMonth:startDate]];
//        [predicateArray addObject:predicate];
        NSDate *endDate = [NSDate date];
        NSDate *startDate = [DateUtils monthAgo:self.selectedMonthIndex];
//        if(self.selectedMonthIndex == 12)
//            predicate = [NSPredicate predicateWithFormat:@"lastContactedDate <= %@", startDate];
//        else
        predicate = [NSPredicate predicateWithFormat:@"lastContactedDate <= %@", startDate];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.btnEducation.currentTitle) && ![self.btnEducation.currentTitle isEqualToString:NSLocalizedString(@"Please select", @"")]){
        predicate = [NSPredicate predicateWithFormat:@"education = %@", [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_EDUCATION
                                                                                                             description:self.btnEducation.currentTitle]];
        [predicateArray addObject:predicate];
    }
    
    if(self.maritalStatusArray.count > 0){
        NSMutableArray *maritalStatusPredicArray = [[NSMutableArray alloc] init];
        for (NSString *maritalStatus in self.maritalStatusArray) {
            predicate= [NSPredicate predicateWithFormat:@"marritalStatus = %@",[[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_MARITAL_STATUS description:maritalStatus]];
            [maritalStatusPredicArray addObject:predicate];
        }
        [predicateArray addObject:[NSCompoundPredicate orPredicateWithSubpredicates:maritalStatusPredicArray]];
    }
    
    if(!isEmptyString(self.btnRecommendScheme.currentTitle) && ![self.btnRecommendScheme.currentTitle isEqualToString:NSLocalizedString(@"Please select", @"")]){
        predicate = [NSPredicate predicateWithFormat:@"recommendedRecruitmentScheme = %@",[[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_RECOMMEND_SCHEME description:self.btnRecommendScheme.currentTitle]];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.btnNotesDateFrom.currentTitle) && ![self.btnNotesDateFrom.currentTitle isEqualToString:@"YYYY-MM-DD"]){
        self.isNote = TRUE;
        predicate = [NSPredicate predicateWithFormat:@"ANY self.candidateNotes.activityDate >= %@",[DateUtils stringToDate:self.btnNotesDateFrom.currentTitle dateFormat:kDisplayDateFormatte]];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.btnNotesDateTo.currentTitle) && ![self.btnNotesDateTo.currentTitle isEqualToString:@"YYYY-MM-DD"]){
        self.isNote = TRUE;
        predicate = [NSPredicate predicateWithFormat:@"ANY self.candidateNotes.activityDate <= %@",[DateUtils stringToDate:self.btnNotesDateTo.currentTitle dateFormat:kDisplayDateFormatte]];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.btnDOBFrom.currentTitle) && ![self.btnDOBFrom.currentTitle isEqualToString:@"YYYY-MM-DD"]){
        predicate = [NSPredicate predicateWithFormat:@"birthDate >= %@",[DateUtils stringToDate:self.btnDOBFrom.currentTitle dateFormat:kDisplayDateFormatte]];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.btnDOBTo.currentTitle) && ![self.btnDOBTo.currentTitle isEqualToString:@"YYYY-MM-DD"]){
        predicate = [NSPredicate predicateWithFormat:@"birthDate <= %@",[DateUtils stringToDate:self.btnDOBTo.currentTitle dateFormat:kDisplayDateFormatte]];
        [predicateArray addObject:predicate];
    }
    
    if(!isEmptyString(self.btnSourceOfReferral.currentTitle) && ![self.btnSourceOfReferral.currentTitle isEqualToString:NSLocalizedString(@"Please select", @"")]){
        predicate = [NSPredicate predicateWithFormat:@"referalSource = %@",[[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_SOURCE_OF_REFERRAL description:self.btnSourceOfReferral.currentTitle]];
        [predicateArray addObject:predicate];
    }
    
    if(self.recuritmentArray.count > 0){
        NSMutableArray *recuritmentPredicArray = [[NSMutableArray alloc] init];
        for (NSString *recuritment in self.recuritmentArray) {
            if([recuritment isEqualToString:RECURITMENT_INTERACTIVE_PRESENER]){
                predicate= [NSPredicate predicateWithFormat:@"self.candidateProcess.interactivePresentStatus = %@",RECURITMENT_STATUS_TRUE];
            }else if([recuritment isEqualToString:RECURITMENT_EOP_REGISTRATION]){
                predicate= [NSPredicate predicateWithFormat:@"self.candidateProcess.eopRegistrationStatus = %@",RECURITMENT_STATUS_TRUE];
            }else if([recuritment isEqualToString:RECURITMENT_FIRST_INTERVIEW]){
                predicate= [NSPredicate predicateWithFormat:@"self.candidateProcess.firstInterviewStatus = %@",RECURITMENT_STATUS_TRUE];
            }else if([recuritment isEqualToString:RECURITMENT_ATTEND_EOP]){
                predicate= [NSPredicate predicateWithFormat:@"self.candidateProcess.attendEopStatus = %@",RECURITMENT_STATUS_TRUE];
            }else if([recuritment isEqualToString:RECURITMENT_CC_TEST]){
                predicate= [NSPredicate predicateWithFormat:@"self.candidateProcess.ccTestStatus = %@",RECURITMENT_STATUS_TRUE];
            }else if([recuritment isEqualToString:RECURITMENT_COMPANY_INTERVIEW]){
                predicate= [NSPredicate predicateWithFormat:@"self.candidateProcess.companyInterviewStatus = %@",RECURITMENT_STATUS_TRUE];
            }else if([recuritment isEqualToString:RECURITMENT_ALE_EXAM]){
                predicate= [NSPredicate predicateWithFormat:@"self.candidateProcess.aleExamStatus = %@",RECURITMENT_STATUS_TRUE];
            }else if([recuritment isEqualToString:RECURITMENT_ABC_TRAINING]){
                predicate= [NSPredicate predicateWithFormat:@"self.candidateProcess.abcTrainingStatus = %@",RECURITMENT_STATUS_TRUE];
            }else if([recuritment isEqualToString:RECURITMENT_CONTRACT]){
                predicate= [NSPredicate predicateWithFormat:@"self.candidateProcess.contractStatus = %@",RECURITMENT_STATUS_TRUE];
            }
            [recuritmentPredicArray addObject:predicate];
        }
        [predicateArray addObject:[NSCompoundPredicate andPredicateWithSubpredicates:recuritmentPredicArray]];
    }
    
    if(predicateArray.count > 0){
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0",[CachingService sharedInstance].currentUserId]];
        NSPredicate *searchPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        
        self.contactArray = [[DbUtils fetchAllObject:@"TblContact"
                                          andPredict:searchPredicate
                                   andSortDescriptor:nil
                                managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
        
        [self.tblSearch reloadData];
    }else{
        [DisplayAlert showAlertInControllerInView:self
                                            title:NSLocalizedString(@"Error",@"")
                                          message:NSLocalizedString(@"Please select search option",@"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.txtAgeFrom && [Utility isNumeric:string])
        return YES;
    else if (textField == self.txtAgeTo && [Utility isNumeric:string])
        return YES;
    else
        return NO;
}

- (void)autoRecruitmentSearch{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"attenededEOPCount >= 3 AND self.candidateProcess.candidateProgressValue>=5 AND (self.candidateProcess.ccTestStatus.length>0 AND self.candidateProcess.ccTestStatus != %@)",CAUTION_RECRUIT];
    
    self.contactArray = [[DbUtils fetchAllObject:@"TblContact"
                                      andPredict:predicate
                               andSortDescriptor:nil
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    [self.tblSearch reloadData];
}

@end
