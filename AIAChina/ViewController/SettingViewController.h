//
//  SettingViewController.h
//  AIAChina
//
//  Created by Tankar on 2015-07-18.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : AIAChinaViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,retain) CachingService *cachingService;
@property (strong, nonatomic) IBOutlet UITableView *tblImages;
@property (strong, nonatomic) NSArray *arrImages;
@property (nonatomic, strong) TblAgentDetails *agentProfile;

@end
