//
//  CandidateRankingViewController.h
//  AIAChina
//
//  Created by MacMini on 22/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CandidateRankingViewController : AIAChinaViewController


@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UITableView *tblCandidateRanking;
@property (weak, nonatomic) IBOutlet UILabel *lblGAMAQuoteEnglish;
@property (weak, nonatomic) IBOutlet UILabel *lblGAMAQuoteChinese;
@property (weak, nonatomic) IBOutlet UILabel *lblGAMAAuthorEnglish;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UILabel *lblRanking;

@property (copy, nonatomic) void(^searchBlock)();

@property (nonatomic) int candidateProgressRank;
@property (strong, nonatomic) NSArray *candidateArray;

@property (nonatomic, strong) NSString* candidateAgentCode;

- (IBAction)onBtnCancelClick:(id)sender;

@end
