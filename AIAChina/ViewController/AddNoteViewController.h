//
//  AddNoteViewController.h
//  AIAChina
//
//  Created by MacMini on 12/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface AddNoteViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnActivityType;
@property (weak, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet UITextField *txtDescription;

@property (strong, nonatomic) TblContact *contact;
@property (strong, nonatomic) TblNotes *note;
@property (strong, nonatomic) UIPopoverController *notePopoverCntrlr;
@property (copy, nonatomic) void(^reloadNoteBlock)();

- (IBAction)onBtnCancelClick:(id)sender;
- (IBAction)onBtnDoneClick:(id)sender;
- (IBAction)onBtnDateClick:(id)sender;

@end
