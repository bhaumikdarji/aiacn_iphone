//
//  CollectionDetailViewController.h
//  AIAChina
//
//  Created by Purva on 29/01/16.
//  Copyright © 2016 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionDetailViewController : UIViewController
- (IBAction)btnCancelTapped:(UIButton *)sender;
@property (nonatomic, strong) NSArray *arrData;
@property (nonatomic, assign) int selectedIndex;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionV;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@end
