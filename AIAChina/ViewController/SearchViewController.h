//
//  SearchViewController.h
//  AIAChina
//
//  Created by AIA on 21/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "ContactListViewController.h"

@interface SearchViewController : AIAChinaViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblCriteria;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UIButton *btnGender;
@property (weak, nonatomic) IBOutlet UILabel *lblAgeFrom;
@property (weak, nonatomic) IBOutlet UITextField *txtAgeFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblAgeTo;
@property (weak, nonatomic) IBOutlet UITextField *txtAgeTo;
@property (weak, nonatomic) IBOutlet UILabel *lblLastContacted;
@property (weak, nonatomic) IBOutlet UIButton *btnLastContacted;
@property (weak, nonatomic) IBOutlet UILabel *lblEducation;
@property (weak, nonatomic) IBOutlet UIButton *btnEducation;
@property (weak, nonatomic) IBOutlet UILabel *lblMaritalStatus;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnMaritalStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblRecommendScheme;
@property (weak, nonatomic) IBOutlet UIButton *btnRecommendScheme;
@property (weak, nonatomic) IBOutlet UILabel *lblNotesDateFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblNotesDateFromBG;
@property (weak, nonatomic) IBOutlet UIButton *btnNotesDateFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblNotesDateTo;
@property (weak, nonatomic) IBOutlet UILabel *lblNotesDateToBG;
@property (weak, nonatomic) IBOutlet UIButton *btnNotesDateTo;
@property (weak, nonatomic) IBOutlet UILabel *lblDOBFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblDOBFromBG;
@property (weak, nonatomic) IBOutlet UIButton *btnDOBFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblDOBTo;
@property (weak, nonatomic) IBOutlet UILabel *lblDOBToBG;
@property (weak, nonatomic) IBOutlet UIButton *btnDOBTo;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceofReferral;
@property (weak, nonatomic) IBOutlet UIButton *btnSourceOfReferral;
@property (weak, nonatomic) IBOutlet UILabel *lblRecuritmentStatus;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnRecuritmentSteps;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;

@property (weak, nonatomic) IBOutlet UILabel *lblSearchResult;
@property (weak, nonatomic) IBOutlet UITableView *tblSearch;

@property (strong, nonatomic) NSMutableArray *maritalStatusArray;
@property (strong, nonatomic) NSMutableArray *recuritmentArray;
@property (strong, nonatomic) NSMutableArray *contactArray;
@property (strong, nonatomic) NSArray *lastContactedArray;
@property (nonatomic) BOOL isNote;
@property (nonatomic) int selectedMonthIndex;
@property (nonatomic) BOOL autoSearch;

@property (strong, nonatomic) UIPopoverController *searchPopover;

- (IBAction)onBtnGenderClick:(id)sender;
- (IBAction)onBtnLastContactedClick:(id)sender;
- (IBAction)onBtnEducationClick:(id)sender;
- (IBAction)onBtnMaritalStatusClick:(id)sender;
- (IBAction)onBtnRecommendSchemeClick:(id)sender;
- (IBAction)onBtnSourceofReferral:(id)sender;
- (IBAction)onBtnResetClick:(id)sender;
- (IBAction)onBtnSearchClick:(id)sender;
- (IBAction)onBtnRecuritmentStepsClick:(id)sender;
- (IBAction)onBtnNotesDateToClick:(id)sender;
- (IBAction)onBtnNotesDateFromClick:(id)sender;
- (IBAction)onBtnDOBFromClick:(id)sender;
- (IBAction)onBtnDOBToClick:(id)sender;

@end
