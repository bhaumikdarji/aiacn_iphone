//
//  NoteListViewController.m
//  AIAChina
//
//  Created by AIA on 20/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "NoteListViewController.h"
#import "NoteCell.h"
#import "AddNoteViewController.m"
#import "CHCSVParser.h"
#import "ExcelExport.h"
@interface NoteListViewController ()
@property (nonatomic,strong) NSString *notesFileName;
@end

@implementation NoteListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Notes", @"");
    [Utility setBorderAndRoundCorner:self.lblTotalNotes
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:15.0];
    
    [Utility setBorderAndRoundCorner:self.borderView
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:6.0];
    self.selectNoteArray = [[NSMutableArray alloc] init];
    self.sortBy = @"activityDate";
    [self loadNoteData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadNoteData{
    
    if([self.sortBy isEqualToString:@"activityDate"]){
        self.noteArray = [[[self.contact.candidateNotes allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isDelete != 1"]]
                          sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:self.sortBy ascending:FALSE]]];
    }else{
        self.noteArray = [[[self.contact.candidateNotes allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isDelete != 1"]]
                          sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:self.sortBy ascending:TRUE]]];
    }
    
    self.lblTotalNotes.text = [NSString stringWithFormat:@"%d",self.noteArray.count];
    [self.tblNote reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.noteArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier1 = @"TableViewCell";
    
    NoteCell *cell = (NoteCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
    TblNotes *note = self.noteArray[indexPath.row];
    [cell loadNoteData:note];
    
    if(note.isSystemValue){
        cell.btnEdit.hidden = TRUE;
        cell.btnDelete.hidden = TRUE;
    }else{
        cell.btnEdit.hidden = FALSE;
        cell.btnDelete.hidden = FALSE;
    }
    
    [cell.btnSelect setSelected:[self isSelectedNote:note]];
    cell.btnSelect.tag = cell.btnDelete.tag = cell.btnEdit.tag = indexPath.row;
    [cell.btnSelect addTarget:self action:@selector(onBtnSelectClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDelete addTarget:self action:@selector(onBtnDeleteClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEdit addTarget:self action:@selector(onBtnEditNoteClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (BOOL)isSelectedNote:(TblNotes *)note{
    for (TblNotes *selectedNote in self.selectNoteArray) {
        if([note.objectID isEqual:selectedNote.objectID])
            return TRUE;
    }
    return FALSE;
}

- (IBAction)onBtnSelectClick:(UIButton *)sender{
    TblNotes *note = self.noteArray[[sender tag]];
    if([sender isSelected]){
        [sender setSelected:FALSE];
        [self.selectNoteArray removeObject:note];
    }else{
        [sender setSelected:TRUE];
        [self.selectNoteArray addObject:note];
    }
}

- (IBAction)onBtnEditNoteClick:(UIButton *)sender{
    TblNotes *note = self.noteArray[[sender tag]];
    if(!note.isSystemValue)
        [self performSegueWithIdentifier:@"NoteToAddNotePopup" sender:note];
}

- (IBAction)onBtnDeleteClick:(UIButton *)sender{
    TblNotes *note = self.noteArray[[sender tag]];
    if(!note.isSystemValue){
        note.isDelete = [NSNumber numberWithBool:TRUE];
        self.contact.isSync = [NSNumber numberWithInt:1];
        [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
        [self loadNoteData];
    }
}

- (IBAction)onBtnDoneClick:(id)sender {
    if(self.updateLastContactedDateBlock)
        self.updateLastContactedDateBlock();
    
    [self dismissViewControllerAnimated:TRUE
                             completion:nil];
}

- (IBAction)onBtnActivityClick:(id)sender{
    self.sortBy = @"activityType";
    [self loadNoteData];
}

- (IBAction)onBtnDateClick:(id)sender{
    self.sortBy = @"activityDate";
    [self loadNoteData];
}

- (IBAction)onBtnDescriptionClick:(id)sender {
    self.sortBy = @"desc";
    [self loadNoteData];
}

- (IBAction)onBtnSelectAllClick:(id)sender {
    if([sender isSelected]){
        [sender setSelected:FALSE];
        [self.selectNoteArray removeAllObjects];
    }else{
        [sender setSelected:TRUE];
        [self.selectNoteArray addObjectsFromArray:self.noteArray];
    }
    [self.tblNote reloadData];
}

- (IBAction)onBtnExportNotesActivityClick:(id)sender
{
   // [self createCSVWithNotes:self.selectNoteArray];
    [self exportNotes];
    [self sendContactEmail];
}

- (void)createCSVWithNotes:(NSArray *)notesArray
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString* documentsDir = [paths objectAtIndex:0];
    NSString* myFilePath = [NSString stringWithFormat:@"%@/%@", documentsDir, @"ExportNotes.csv"];
    CHCSVWriter *csvWriter = [[CHCSVWriter alloc]initForWritingToCSVFile:myFilePath];
    for (TblNotes *notes in notesArray)
    {    NSString *notesDes = notes.desc;
         NSString *notesActivity = notes.activityType;
        [csvWriter writeLineOfFields:@[notesDes.length == 0? @"":notes.desc,
                                       notesActivity.length == 0? @"":notes.activityType,
                                       notes.activityDate ? @"" : notes.activityDate]];
        [csvWriter finishLine];
    }
}

- (void)exportNotes
{
     self.notesFileName = [ExcelExport ExportNotesToFile:self.selectNoteArray];
}

- (void)sendContactEmail
{
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *csvPath = [docsPath stringByAppendingPathComponent:@"ExportNotes.xls"];
    NSData *data = [NSData dataWithContentsOfFile:csvPath];
    NSString *mimeType;
    
    if ([MFMailComposeViewController canSendMail] == NO) return;
    unsigned long long fileSize = data.length;
    
    if (fileSize < (unsigned long long)15728640)
    {
        if (data != nil){
            
            MFMailComposeViewController *mailComposer = [MFMailComposeViewController new];
            mimeType = @"application/vnd.ms-excel";
            [mailComposer addAttachmentData:data mimeType:mimeType fileName:@"ExportNotes.xls"];
            NSString *message = [NSString stringWithFormat:NSLocalizedString(@"Selected Notes Data", @"")];
            [mailComposer setMessageBody:message isHTML:NO];
            [mailComposer setSubject:NSLocalizedString(@"Export Notes", @"")];
            mailComposer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            mailComposer.modalPresentationStyle = UIModalPresentationFormSheet;
            mailComposer.mailComposeDelegate = self;
            [self presentViewController:mailComposer animated:YES completion:NULL];
        }
    }
    
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result == MFMailComposeResultSent){
        [self dismissViewControllerAnimated:YES completion:^{
            [self dismissViewControllerAnimated:NO completion:^{
            }];
        }];
    }else if(result == MFMailComposeResultCancelled){
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
    
    [[NSFileManager defaultManager]removeItemAtPath:self.notesFileName error:nil];
}

- (IBAction)onBtnAddNoteClcik:(id)sender {
    [self performSegueWithIdentifier:@"NoteToAddNotePopup" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"NoteToAddNotePopup"]){
        AddNoteViewController *addNoteViewCtrl = segue.destinationViewController;
        addNoteViewCtrl.contact = self.contact;
        [addNoteViewCtrl setReloadNoteBlock:^{
            [self loadNoteData];
        }];
        if([sender isKindOfClass:[TblNotes class]])
            addNoteViewCtrl.note = (TblNotes *)sender;
    }
}

@end
