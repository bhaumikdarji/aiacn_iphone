//
//  RegisterSuccessfulController.m
//  AIAChina
//
//  Created by MacMini on 24/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "RegisterSuccessfulController.h"
#import "RegisterSuccessfulCell.h"
#import "DropDownManager.h"

@interface RegisterSuccessfulController ()

@end

@implementation RegisterSuccessfulController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utility setRoundCornerRedius:self.imgHeader
                     cornerRedius:60.0];
    
    [Utility setBorderAndRoundCorner:self.mainView
                         borderColor:[UIColor clearColor]
                               width:1.0
                        cornerRedius:5.0];

    self.lblCong.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Congratulations", @""), self.candidateName];
    self.lblDesc.text = self.desc;
    
    NSString *str = [NSString stringWithFormat:@"%@ %@.", NSLocalizedString(@"Currently Candidate's rank is", @""), [Utility ordinalNumberFormat:self.candidateProgressRank]];
    NSRange rang = [str rangeOfString:[NSString stringWithFormat:@"%d",self.candidateProgressRank]];
    [self.lblRanking setAttributedText:[Utility setBoldTextOnSingleLable:str fontSize:12.0 range:rang setBoldFontColor:[UIColor redColor]]];
 	[self loadData];
    [self setGAMAData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData{
    int count = [DbUtils getCountForEntity:@"TblContact"
                                andPredict:nil
                      managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    int minRange = self.candidateProgressRank<=3? 1 : self.candidateProgressRank - 2;
    int maxRange = self.candidateProgressRank<=3? 5 : self.candidateProgressRank + 2;
    // Handle last 3 rankings.
    if (self.candidateProgressRank >= count - 2) {
        minRange = count - 4;
        maxRange = count;
    }
    
    self.contactArray = [DbUtils fetchAllObject:@"TblContact"
                                     andPredict:[NSPredicate predicateWithFormat:@"candidateProcess.candidateRank >= %d AND candidateProcess.candidateRank <= %d AND agentId == %@ AND isDelete == 0",minRange, maxRange,[CachingService sharedInstance].currentUserId]
                              andSortDescriptor:[NSSortDescriptor sortDescriptorWithKey:@"candidateProcess.candidateRank" ascending:TRUE]
                           managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    [self.tblRegisterUser reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.contactArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RegisterSuccessfulCell *cell = (RegisterSuccessfulCell *)[tableView dequeueReusableCellWithIdentifier:@"RegisterSuccessfulCell"];
    TblContact *contact = self.contactArray[indexPath.row];
    [cell loadData:contact];
   
    if (self.candidateProgressRank == [contact.candidateProcess.candidateRank integerValue]) {
        cell.lblRank.backgroundColor = [UIColor colorWithRed:250.0/255.0
                                                             green:190.0/255.0
                                                              blue:83.0/255.0
                                                             alpha:1.0];
        cell.lblRank.textColor = [UIColor whiteColor];
    }else{
        cell.lblRank.textColor = [UIColor blackColor];
    }
    
    if(indexPath.row % 2 == 0)
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0
                                               green:239.0/255.0
                                                blue:241.0/255.0
                                               alpha:1.0];
    else
        cell.backgroundColor = [UIColor whiteColor];
     return cell;
}

- (void)setGAMAData{
    
    NSArray *gamaArray = [DropDownManager sharedManager].gamaContentEnglish;
    NSDictionary *gamaDic = [gamaArray objectAtIndex:arc4random() % [gamaArray count]];
    
    self.lblGAMAQuoteEnglish.text = [gamaDic objectForKey:@"Description_en"];
    self.lblGAMAAuthorEnglish.text = [gamaDic objectForKey:@"Author_en"];
    self.lblGAMAQuoteChinese.text = [gamaDic objectForKey:@"Description_cn"];
    
}

- (IBAction)onBtnCancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
