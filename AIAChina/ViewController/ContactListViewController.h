//
//  ContactListViewController.h
//  AIAChina
//
//  Created by AIA on 17/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "GDIIndexBar.h"

@interface ContactListViewController : AIAChinaViewController <GDIIndexBarDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tblContactList;
@property (weak, nonatomic) IBOutlet UIView *transView;

@property (nonatomic, strong) NSMutableArray *contactArray;
@property (strong, nonatomic) NSMutableArray *contactAlphabetArray;
@property (strong, nonatomic) NSMutableArray *alphabetArray;
@property (strong, nonatomic) GDIIndexBar *indexBar;
@property (nonatomic) BOOL isGroupList;
@property (nonatomic) BOOL isAddContactGroup;

@property (strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) TblContact *selectedContact;
@property (nonatomic, strong) TblGroup *selectedGroup;
@property (copy, nonatomic) void(^showContact)(NSArray *);
@property (nonatomic, strong) UIPopoverController *menuListPopOver;

- (IBAction)onBtnAddContactClick:(id)sender;
- (IBAction)onBtnSearchClick:(id)sender;
- (IBAction)onBtnSyncClick:(id)sender;
- (IBAction)onBtnDeleteClick:(id)sender;
- (IBAction)onBtnExportClick:(id)sender;

@end
