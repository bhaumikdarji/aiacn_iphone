//
//  AssignContactViewController.h
//  AIAChina
//
//  Created by AIA on 31/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignContactViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblContact;
@property (weak, nonatomic) IBOutlet UITableView *tblContact;

@property (nonatomic) BOOL isContact;
@property (nonatomic) BOOL isDelete;
@property (nonatomic) BOOL isExport;
@property (nonatomic) BOOL isAssigneeContact;

@property (strong, nonatomic) NSMutableArray *contactArray;
@property (strong, nonatomic) NSMutableArray *selectedArray;
@property (strong, nonatomic) NSArray *ignoreArray;

@property (copy, nonatomic) void(^selectedContactBlock)(NSArray *);

- (IBAction)onBtnCancelClick:(id)sender;
- (IBAction)onBtnDoneClick:(id)sender;

@end
