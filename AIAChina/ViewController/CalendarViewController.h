//
//  CalendarViewController.h
//  AIAChina
//
//  Created by AIA on 27/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "FFCalendar.h"

@protocol FFCalendarViewControllerProtocol <NSObject>
@required
- (void)arrayUpdatedWithAllEvents:(NSMutableArray *)arrayUpdated;
@end

@interface CalendarViewController : AIAChinaViewController<FFYearCalendarViewProtocol, FFMonthCalendarViewProtocol, FFWeekCalendarViewProtocol, FFDayCalendarViewProtocol, UIPopoverControllerDelegate>
@property (nonatomic, strong) id <FFCalendarViewControllerProtocol> protocol;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthYear;
@property (weak, nonatomic) IBOutlet UIView *calendarView;
@property (weak, nonatomic) IBOutlet UIButton *btnToday;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sgmCalType;
@property (weak, nonatomic) IBOutlet UIButton *btnCreateEvent;

@property (nonatomic, strong) FFYearCalendarView *viewCalendarYear;
@property (nonatomic, strong) FFMonthCalendarView *viewCalendarMonth;
@property (nonatomic, strong) FFWeekCalendarView *viewCalendarWeek;
@property (nonatomic, strong) FFDayCalendarView *viewCalendarDay;

@property (nonatomic, strong) NSMutableDictionary *dictEvents;
@property (nonatomic, strong) NSMutableArray *eventArray;
@property (nonatomic, strong) NSMutableDictionary *eventDic;
@property (nonatomic, strong) NSArray *arrayCalendars;
@property (nonatomic, strong) NSString *eventType;

@property (nonatomic) NSInteger currentYear;
@property (nonatomic, strong) UIPopoverController *eventPopoverController;

@property (nonatomic, strong) NSMutableDictionary *yearlyEventDict;
@property (nonatomic, strong) NSMutableDictionary *yearlyICalendarEventDict;


- (void)setArrayWithEvents:(BOOL)isReload;
- (IBAction)onBtnTodayClick:(id)sender;
- (IBAction)onBtnCreateEventClick:(id)sender;

@end
