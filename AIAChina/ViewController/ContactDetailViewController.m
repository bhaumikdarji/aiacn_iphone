//
//  ContactDetailViewController.m
//  AIAChina
//
//  Created by AIA on 17/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "ContactDetailViewController.h"
#import "HomeViewController.h"
#import "AddContactViewController.h"
#import "GroupViewController.h"
#import "EOPEventsMTLModel.h"
#import "ContactEventModel.h"
#import "EOPRegistrationViewController.h"
#import "DropDownManager.h"
#import "CandidateApplicationFormViewController.h"
#import "NoteListViewController.h"
#import "EOPAttendanceViewController.h"
#import "ZXingObjC.h"
#import "FTDHomeViewController.h"
#import "GreetingsViewController.h"
#import "TblAgentDetails.h"
#import "JNKeychain.h"
#import "NSString+isNumeric.h"

@interface ContactDetailViewController ()
@property (nonatomic, retain) ContactEventModel *eventModel;
@property (weak, nonatomic) IBOutlet UIButton *barCodeButton;
@end

@implementation ContactDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Address Book", @"");
    self.eventModel = [ContactEventModel sharedInstance];
    if(!self.contact){
        self.contact = (TblContact *)[DbUtils fetchObject:@"TblContact"
                                               andPredict:nil
                                        andSortDescriptor:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:TRUE]
                                     managedObjectContext:[CachingService sharedInstance].managedObjectContext];
        [self loadData];
    }
    self.isSyncRecruitmentProgress = TRUE;
    [self setBorder];
    if(self.contact.presenterReport && self.contact.presenterReportDate){
        [self setRecurimentProcess:TRUE];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = [self prepareRightBarButton:YES];
    self.navigationItem.leftBarButtonItem = [self prepareLeftBarButton:YES withLogo:NO];
    [MyAppDelegate showAnnouncements:self.view];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showProfile:)
                                                 name:SPLIT_MASTER_DETAIL_TO_PROFILE_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addNewContact:)
                                                 name:SPLIT_MASTER_DETAIL_TO_ADD_CONTACT_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoSearch:)
                                                 name:SPLIT_MASTER_DETAIL_TO_SEARCH_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addNewGroup:)
                                                 name:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                               object:nil];
    [self loadData];
    if(self.contact.presenterReport && self.contact.presenterReportDate){
        [self setRecurimentProcess:TRUE];
    }
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnOverview];
    [self.btnOverview addSubview:bottomBorder];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SPLIT_MASTER_DETAIL_TO_PROFILE_NOTIFICATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SPLIT_MASTER_DETAIL_TO_ADD_CONTACT_NOTIFICATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SPLIT_MASTER_DETAIL_TO_SEARCH_NOTIFICATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                                  object:nil];
}

- (void)showProfile:(NSNotification *)notifi{
    if([[notifi object] isKindOfClass:[NSDictionary class]]){

        self.contact = [[notifi object] valueForKey:@"object"];
        if(![AFNetworkReachabilityManager sharedManager].isReachable){
            [self loadData];
        } else {
            if([self.contact.addressCode integerValue] == 0){
                [self setRecurimentProcess:NO];
                [DisplayAlert showAlertInControllerInView:self
                                                    title:@""
                                                  message:NSLocalizedString(@"Please sync candidate", @"")
                                        cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                            okButtonTitle:nil
                                              cancelBlock:nil
                                                  okBlock:nil];
                
            }
            else {
                [self isSyncProgress];
            }
        }
    }else {
        self.contact = [notifi object];
        if([self.contact.addressCode integerValue] == 0){
            [self setRecurimentProcess:NO];
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Please sync candidate", @"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:nil
                                          cancelBlock:nil
                                              okBlock:nil];
        }
        else if(self.isSyncRecruitmentProgress)
            [self isSyncProgress];
    }
    

    if(!self.isSyncRecruitmentProgress) {
        [self loadData];
    
    }
}

- (void)isSyncProgress{

    if([AFNetworkReachabilityManager sharedManager].isReachable){
        self.isSyncRecruitmentProgress = TRUE;
        [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
        [self EOPApiCall:FALSE];
    } else {
        [self setRecurimentProcess:NO];
        [self loadData];
    }

}

- (void)loadData{
    if(self.contact){
        if(((int)[self.contact.candidatePhoto length]!=0)){
            self.imgProfilePicture.image = [UIImage imageWithData:self.contact.candidatePhoto];
        }else{
            if ([self.contact.gender isEqualToString:@"F"]) {
                self.imgProfilePicture.image = [UIImage imageNamed:@"female.png"];
            } else {
                self.imgProfilePicture.image = [UIImage imageNamed:@"male.png"];
            }
        }
        [self lastContactUpdated];
        self.lblStatus.text=[NSString stringWithFormat:@"Completion of the selection step %@", [ApplicationFunction recruitmentProgress:self.contact]];
        self.TalentResult.text=self.contact.talentProfile;
        self.LatestDate.text=[DateUtils dateToString:self.contact.lastContactedDate andFormate:kDisplayDetailFormat];
//        self.imgBackground.image = [Utility blurWithCoreImage:self.imgProfilePicture.image setView:self.imgBackground];
        self.lblCandidateName.text = self.contact.name;
        self.lblGender.text = [self getDropdownValue:[DropDownManager sharedManager].gender code:self.contact.gender];
        self.lblDOB.text = [DateUtils dateToString:self.contact.birthDate andFormate:kDisplayDetailFormat];
        self.imgTalent.layer.cornerRadius=38;
        self.imgTalent.clipsToBounds=YES;
        self.lblMobileNumber.text = self.contact.mobilePhoneNo;
        self.lblAge.text=[NSString stringWithFormat:@"%@",self.contact.age];
        self.lblSOR.text = [self getDropdownValue:[DropDownManager sharedManager].sourceOfReferral code:self.contact.referalSource];
        NSString *qrString = [self.contact getLocalizedQRCodeString];
        if (qrString.length > 0) {
            [self.barCodeButton setBackgroundImage:[ApplicationFunction generateBarcodeImageWithQRCode:qrString] forState:UIControlStateNormal];
        } else {
            [self.barCodeButton setBackgroundImage:nil forState:UIControlStateNormal];
        }
        
        if(self.contact.presenterReport && self.contact.presenterReportDate){
            [ApplicationFunction setUpdateUserProcess:RECURITMENT_INTERACTIVE_PRESENER
                                              contact:self.contact
                                        processStatus:RECURITMENT_STATUS_TRUE
                                          processDate:self.contact.presenterReportDate];
            [self setRecurimentProcess:FALSE];
        }
        [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
    }
}

- (void)lastContactUpdated{
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"activityDate" ascending:FALSE];
    NSArray *noteArray = [[[self.contact.candidateNotes allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isDelete != 1"]] sortedArrayUsingDescriptors:@[sort]];
    if(noteArray.count > 0){
        TblNotes *notes = noteArray[0];
        NSString *activityDate = [DateUtils dateToString:notes.activityDate andFormate:kDisplayDateFormatte];
        self.lblGroup.text = activityDate;
        self.contact.lastContactedDate =  notes.activityDate;
    } else if (self.contact.lastContactedDate) {
        self.lblGroup.text =  [DateUtils dateToString:self.contact.lastContactedDate andFormate:kDisplayDateFormatte];

    } else  {
        self.lblGroup.text = @"";
    }
    
//    NSMutableArray *notesDateArray = [[NSMutableArray alloc]init];
//    if (noteArray.count > 0){
//        for (int i = 0; i < noteArray.count; i++){
//            TblNotes *notes = noteArray[i];
//            NSDate *date = notes.activityDate;
//            [notesDateArray addObject:date];
//        }
//    }
//    
//    if (noteArray.count == 0) {
//        self.lblGroup.text = @"";
//    }else if (noteArray.count == 1) {
//        
//    }else{
//        NSArray *reverseOrderUsingComparator = [notesDateArray sortedArrayUsingComparator:
//                                                ^(id obj1, id obj2) {
//                                                    return [obj2 compare:obj1];
//                                                }];
//        NSDate *date = reverseOrderUsingComparator[0];
//        NSString *activityDate = [DateUtils dateToString:date andFormate:kDisplayDateFormatte];
//        self.lblGroup.text = activityDate;
//    }
}

- (NSString *)getDropdownValue:(NSArray *)dropdownArray code:(NSString *)code{
    code =  [code getTrimmedValue];
    NSArray *array = [dropdownArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Code=%@",code]];
    if(array.count > 0)
        return [[array objectAtIndex:0] valueForKey:@"Description"];
    else
        return @"";
}

- (void)resetRecruitmentProcess
{
    [self.btnInteractivePres setBackgroundImage:[UIImage imageNamed:@"iPresenter.png"] forState:UIControlStateNormal];
    [self.btnEopReg setBackgroundImage:[UIImage imageNamed:@"EOPRegistration.png"] forState:UIControlStateNormal];
    [self.btnFirstInterView setBackgroundImage:[UIImage imageNamed:@"FirstInterview.png"] forState:UIControlStateNormal];
    [self.btnAttendEOP setBackgroundImage:[UIImage imageNamed:@"AttendEOP.png"] forState:UIControlStateNormal];
    [self.btnCCTest setBackgroundImage:[UIImage imageNamed:@"CCTest.png"] forState:UIControlStateNormal];
    [self.btnCompanyInteview setBackgroundImage:[UIImage imageNamed:@"CompanyInterview.png"] forState:UIControlStateNormal];
    [self.btnALEExam setBackgroundImage:[UIImage imageNamed:@"ALEExam.png"] forState:UIControlStateNormal];

    [self.btnABCTraining setBackgroundImage:[UIImage imageNamed:@"ABCTraining.png"] forState:UIControlStateNormal];
    [self.btnContract setBackgroundImage:[UIImage imageNamed:@"Contract.png"] forState:UIControlStateNormal];

    self.lblInteractivePresenterDate.text = @"";
    self.lblEOPRegistrationDate.text = @"";
    self.lblFirstInterviewDate.text = @"";
    self.lblAttendEOPDate.text = @"";
    self.lblCCTestDate.text = @"";
    self.lblCompanyInterviewDate.text = @"";
    self.lblALEExamDate.text = @"";
    self.lblABCTrainingDate.text = @"";
    self.lblContractDate.text = @"";
}

- (void)setRecurimentProcess:(BOOL)isReloadContactList{
    
    TblCandidateProcess *candidateProcess = self.contact.candidateProcess;

    if([candidateProcess.interactivePresentStatus isEqualToString:RECURITMENT_STATUS_TRUE] || (self.contact.presenterReport && self.contact.presenterReportDate)){
        [self.btnInteractivePres setBackgroundImage:[UIImage imageNamed:@"iPresenter.png"] forState:UIControlStateNormal];
        
       
    }else{
        [self.btnInteractivePres setBackgroundImage:[UIImage imageNamed:@"iPresenterBlue.png"] forState:UIControlStateNormal];
    }
    
    if([candidateProcess.eopRegistrationStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
        [self.btnEopReg setBackgroundImage:[UIImage imageNamed:@"EOPRegistrationBlue.png"] forState:UIControlStateNormal];
    }else{
        [self.btnEopReg setBackgroundImage:[UIImage imageNamed:@"EOPRegistration.png"] forState:UIControlStateNormal];
    }
    
    if(!isEmptyString(candidateProcess.firstInterviewStatus)){
        if([candidateProcess.firstInterviewStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
            [self.btnFirstInterView setBackgroundImage:[UIImage imageNamed:@"FirstInterviewBlue.png"] forState:UIControlStateNormal];
            [self addRecruitmentNote:0
                     recruitmentStep:RS_FIRST_INTERVIEW
                        activityDate:candidateProcess.firstInterviewDate
                             contact:self.contact
                         description:FIRST_INTERVIEW_COMPLETED_DESCRIPTION];
        }else{
            [self.btnFirstInterView setBackgroundImage:[UIImage imageNamed:@"FirstInterviewBlue.png"] forState:UIControlStateNormal];
        }
    }else
        [self.btnFirstInterView setBackgroundImage:[UIImage imageNamed:@"FirstInterview.png"] forState:UIControlStateNormal];
    
    if([candidateProcess.attendEopStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
        [self.btnAttendEOP setBackgroundImage:[UIImage imageNamed:@"AttendEOPBlue.png"] forState:UIControlStateNormal];
    }else{
        [self.btnAttendEOP setBackgroundImage:[UIImage imageNamed:@"AttendEOP.png"] forState:UIControlStateNormal];
    }
    
    if(!isEmptyString(candidateProcess.ccTestStatus)){
        if([candidateProcess.ccTestStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
            [self.btnCCTest setBackgroundImage:[UIImage imageNamed:@"CCTestBlue.png"]
                                      forState:UIControlStateNormal];
            
            [self addRecruitmentNote:0
                     recruitmentStep:RS_CC_TEST
                        activityDate:candidateProcess.ccTestDate
                             contact:self.contact
                         description:CC_TEST_COMPLETED_DESCRIPTION];
        }else{
            [self.btnCCTest setBackgroundImage:[UIImage imageNamed:@"CCTestBlue.png"] forState:UIControlStateNormal];
        }
    }else
        [self.btnCCTest setBackgroundImage:[UIImage imageNamed:@"CCTest.png"] forState:UIControlStateNormal];
    
    if([candidateProcess.companyInterviewStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
        [self.btnCompanyInteview setBackgroundImage:[UIImage imageNamed:@"CompanyInterviewBlue.png"] forState:UIControlStateNormal];
        
        [self addRecruitmentNote:0
                 recruitmentStep:RS_COMPANY_INTERVIEW
                    activityDate:candidateProcess.companyInterviewDate
                         contact:self.contact
                     description:COMPANY_INTERVIEW_COMPLETED_DESCRIPTION];
    }else{
        [self.btnCompanyInteview setBackgroundImage:[UIImage imageNamed:@"CompanyInterview.png"] forState:UIControlStateNormal];
    }
    
    if([candidateProcess.aleExamStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
        [self.btnALEExam setBackgroundImage:[UIImage imageNamed:@"ALEExamBlue.png"] forState:UIControlStateNormal];
        
        [self addRecruitmentNote:0
                 recruitmentStep:RS_ALE_EXAM
                    activityDate:candidateProcess.aleExamDate
                         contact:self.contact
                     description:ALE_EXAM_COMPLETED_DESCRIPTION];
    }else{
        [self.btnALEExam setBackgroundImage:[UIImage imageNamed:@"ALEExam.png"] forState:UIControlStateNormal];
    }
    
    if(!isEmptyString(candidateProcess.abcTrainingStatus)){
        if([candidateProcess.abcTrainingStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
            [self.btnABCTraining setBackgroundImage:[UIImage imageNamed:@"ABCTrainingBlue.png"] forState:UIControlStateNormal];
            
            [self addRecruitmentNote:0
                     recruitmentStep:RS_ABC_TRANING
                        activityDate:candidateProcess.abcTrainingDate
                             contact:self.contact
                         description:ABC_TRAINING_COMPLETED_DESCRIPTION];
        }else{
            [self.btnABCTraining setBackgroundImage:[UIImage imageNamed:@"ABCTrainingBlue.png"] forState:UIControlStateNormal];
        }
    }else{
        [self.btnABCTraining setBackgroundImage:[UIImage imageNamed:@"ABCTraining.png"] forState:UIControlStateNormal];
    }
    
    if([candidateProcess.contractStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
        [self.btnContract setBackgroundImage:[UIImage imageNamed:@"ContractBlue.png"] forState:UIControlStateNormal];
        
        [self addRecruitmentNote:0
                 recruitmentStep:RS_CONTRACT
                    activityDate:candidateProcess.contractDate
                         contact:self.contact
                     description:CONTRACT_COMPLETED_DESCRIPTION];
        
        [self addRecruitmentNote:0
                 recruitmentStep:RS_ALE_EXAM
                    activityDate:candidateProcess.aleExamDate
                         contact:self.contact
                     description:ALE_EXAM_COMPLETED_DESCRIPTION];
    }else{
        [self.btnContract setBackgroundImage:[UIImage imageNamed:@"Contract.png"] forState:UIControlStateNormal];
    }
    
    
    
    NSString *dateFormat = kDisplayDateFormatte;
    if(candidateProcess.interactivePresentDate)
        self.lblInteractivePresenterDate.text = [NSDate dateToString:candidateProcess.interactivePresentDate
                                                          andFormate:dateFormat];
    else
        self.lblInteractivePresenterDate.text = @"";
    
    if(candidateProcess.eopRegistrationDate)
        self.lblEOPRegistrationDate.text = [NSDate dateToString:candidateProcess.eopRegistrationDate
                                                     andFormate:dateFormat];
    else
        self.lblEOPRegistrationDate.text = @"";
    
    if(candidateProcess.firstInterviewDate)
        self.lblFirstInterviewDate.text = [NSDate dateToString:candidateProcess.firstInterviewDate
                                                    andFormate:dateFormat];
    else
        self.lblFirstInterviewDate.text = @"";
    
    if(candidateProcess.attendEopDate)
        self.lblAttendEOPDate.text = [NSDate dateToString:candidateProcess.attendEopDate
                                               andFormate:dateFormat];
    else
        self.lblAttendEOPDate.text = @"";
    
    if(candidateProcess.ccTestDate)
        self.lblCCTestDate.text = [NSDate dateToString:candidateProcess.ccTestDate
                                            andFormate:dateFormat];
    else
        self.lblCCTestDate.text = @"";
    
    if(candidateProcess.companyInterviewDate && [candidateProcess.companyInterviewStatus isEqualToString:RECURITMENT_STATUS_TRUE])
        self.lblCompanyInterviewDate.text = [NSDate dateToString:candidateProcess.companyInterviewDate
                                                      andFormate:dateFormat];
    else
        self.lblCompanyInterviewDate.text = @"";
    
    if(candidateProcess.aleExamDate)
        self.lblALEExamDate.text = [NSDate dateToString:candidateProcess.aleExamDate
                                                 andFormate:dateFormat];
    else
        self.lblALEExamDate.text = @"";
    
    if(candidateProcess.abcTrainingDate)
        self.lblABCTrainingDate.text = [NSDate dateToString:candidateProcess.abcTrainingDate
                                                 andFormate:dateFormat];
    else
        self.lblABCTrainingDate.text = @"";
    
    if(candidateProcess.contractDate)
        self.lblContractDate.text = [NSDate dateToString:candidateProcess.contractDate
                                              andFormate:dateFormat];
    else
        self.lblContractDate.text = @"";
    
    if(isReloadContactList){
        candidateProcess.candidateProgressValue = [self getTotalProgressCompleteValue];
        [self setRank];
        [[NSNotificationCenter defaultCenter] postNotificationName:CONTACT_LIST_RELOAD_NOTIFICATION
                                                            object:self.contact];
    }
}

- (int)getTotalProgressCompleteValue{
    NSArray *keys = [[[self.contact.candidateProcess entity] attributesByName] allKeys];
    NSArray *value = [[[self.contact.candidateProcess dictionaryWithValuesForKeys:keys] allValues] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF = %@",RECURITMENT_STATUS_TRUE]];
    return (int)value.count;
}

- (void)setRank{
    NSSortDescriptor *sortProgress = [NSSortDescriptor sortDescriptorWithKey:@"candidateProcess.candidateProgress" ascending:NO];
    NSSortDescriptor *sortDate = [NSSortDescriptor sortDescriptorWithKey:@"candidateProcess.completeProgressDate" ascending:YES];
    
    NSArray *totalCandidate = [DbUtils fetchAllObject:@"TblContact"
                                           andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0", [CachingService sharedInstance].currentUserId]
                               andSortDescriptorArray:[NSArray arrayWithObjects:sortProgress, sortDate, nil]
                                 managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    int i = 1;
    for (TblContact *contact in totalCandidate) {
        contact.candidateProcess.candidateRankValue = i++;
//        NSLog(@"Candidate Rank is %d completed date %@ and progress %@",i,contact.candidateProcess.completeProgressDate, contact.candidateProcess.candidateProgress);
    }
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"ContactDetailToAddContact"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:EDIT_CONTACT_GROUP_UNHIDE_TRANS_VIEW_NOTIFICATION
                                                            object:nil];
        AddContactViewController *addContactVC = segue.destinationViewController;
        if(self.contact)
            addContactVC.contact = self.contact;
        [addContactVC setAddGroupBlock:^{
            [self addNewGroup:nil];
        }];
    }else if([segue.identifier isEqualToString:@"ContactDetailToAddGroup"]){
        GroupViewController *groupVC = segue.destinationViewController;
        [groupVC addGroup:nil];
        [groupVC setAddContactBlock:^(BOOL isAddContact) {
            if(isAddContact)
                [self addNewContact:nil];
            else
                [self gotoSearch:nil];
        }];
    }else if([segue.identifier isEqualToString:@"ContactDetailToNote"]){
        UINavigationController *navCtrl = segue.destinationViewController;
        NoteListViewController *noteListViewCtrl = navCtrl.viewControllers[0];
        noteListViewCtrl.contact = self.contact;
        [noteListViewCtrl setUpdateLastContactedDateBlock:^{
            [self lastContactUpdated];
        }];
    }else if([segue.identifier isEqualToString:@"candidateApplicationForm"]){
        CandidateApplicationFormViewController *applicationForm = (CandidateApplicationFormViewController*)[[segue destinationViewController] topViewController];
        applicationForm.addressCode = self.contact.iosAddressCode;
        [applicationForm setCompletionBlock:^{
            [self loadData];
        }];
    }else if([segue.identifier isEqualToString:@"AddressBookToGreeting"]){
        UINavigationController *navCtrl = segue.destinationViewController;
        GreetingsViewController *greetingViewCtrl = navCtrl.viewControllers[0];
        greetingViewCtrl.contact = self.contact;
    }
}

- (IBAction)onBtnBackClick:(id)sender {
    [ApplicationFunction showToAgentProfile:NO];
}

- (void)setBorder{
    [Utility setRoundCornerRedius:self.profileView cornerRedius:100.0];
    [Utility setRoundCornerRedius:self.imgProfilePicture cornerRedius:100.0];
//    [Utility setBorderWithColor:self.profileView borderColor:[UIColor colorWithRed:146.0/255.0 green:145.0/255.0 blue:147.0/255.0 alpha:1.0] borderWidth:5.0];    
    self.imgBackground.image = [Utility blurWithCoreImage:self.imgProfilePicture.image setView:self.view];
}

- (void)addNewContact:(NSNotification *)notifi{
    self.contact = nil;
    [self resetRecruitmentProcess];
    [self onBtnShowDetailClick:nil];
}

- (void)addNewGroup:(NSNotification *)notifi{
    [self performSegueWithIdentifier:@"ContactDetailToAddGroup" sender:self];
}

- (void)gotoSearch:(NSNotification *)notifi{
    [self performSegueWithIdentifier:@"ContactDetailToSearch" sender:self];
}

- (IBAction)onBtnShowDetailClick:(id)sender {
    [self performSegueWithIdentifier:@"ContactDetailToAddContact" sender:self];
}

- (IBAction)onBtnQQShareClick:(id)sender {
    
    if (!self.contact.qq) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"The Contact’s QQ ID is not specified", @"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }
    
    [self shareQQ:self.barCodeButton.currentImage
          subject:@""
              msg:@""
    callBackBlock:^(NSString *errorMsg) {
        if(!isEmptyString(errorMsg))
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:errorMsg
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:@""
                                          cancelBlock:nil
                                              okBlock:nil];
    }];
}

- (IBAction)onBtnWeChatShareClick:(id)sender {
    
    if (!self.contact.weChat) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"The Contact’s WeChat ID is not specified", @"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }

    
    [self shareWeChat:self.barCodeButton
                image:self.barCodeButton.currentImage
              subject:@""
                  msg:@""
        callBackBlock:^(BOOL isInstalled, BOOL isSuccess) {
            if(!isInstalled){
                [DisplayAlert showAlertInControllerInView:self
                                                    title:@""
                                                  message:NSLocalizedString(@"Please install WeChat before you are allowed to use this function", @"")
                                        cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                            okButtonTitle:@""
                                              cancelBlock:nil
                                                  okBlock:nil];
            }else if (!isSuccess){
                [DisplayAlert showAlertInControllerInView:self
                                                    title:@""
                                                  message:NSLocalizedString(@"Share unsuccessful", @"")
                                        cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                            okButtonTitle:@""
                                              cancelBlock:nil
                                                  okBlock:nil];
            }else{
                [self dismissViewControllerAnimated:TRUE completion:nil];
            }
        }];
}

- (IBAction)onBtnEmailShareClick:(id)sender {
    
    if (!self.contact.eMailId) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"The Contact’s Email Address is not specified", @"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }

    [self shareEmail:self.barCodeButton.currentImage
           recipient:@[self.contact.eMailId]
            fileName:[NSString stringWithFormat: @"%@_QR_code.jpg",APP_NAME]
             subject:@""
                 msg:@""
       callBackBlock:^(BOOL isInstalled) {
           if(!isInstalled)
               [DisplayAlert showAlertInControllerInView:self
                                                   title:NSLocalizedString(@"can not send email,please configure email account!", @"")
                                                 message:@""
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                           okButtonTitle:@""
                                             cancelBlock:nil
                                                 okBlock:nil];
           
       }];
}

- (IBAction)btnInteractivePressClick:(id)sender {
    [self onBtnInteractivePresClick:nil];
}

- (IBAction)onBtnInteractivePresClick:(id)sender {
    if(self.contact.presenterReport && self.contact.presenterReportDate) {
        NSLog(@"Show Webview with loadview");
        [self showWebView];
        UIWindow * window = [UIApplication sharedApplication].windows.lastObject;
        [self.webView loadData:self.contact.presenterReport MIMEType:@"application/png" textEncodingName:@"utf-8" baseURL:[NSURL URLWithString:@""]];
        [window addSubview:self.webView];
        [window addSubview:self.closeButton];
    } else {
        if(isEmptyString([self.contact getLocalizedQRCodeString])){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"QR Code must be generated for Contact",@"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:nil
                                          cancelBlock:nil
                                              okBlock:nil];
        } else {
            [self showInteractivePresenter];
        }
    }
}

- (IBAction)onBtnEopRegClick:(id)sender{
    if([self isInternetConnect]){
        if([self.contact.addressCode integerValue] == 0){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Please sync candidate", @"")
                                    cancelButtonTitle:NSLocalizedString(@"", @"")
                                        okButtonTitle:nil
                                          cancelBlock:nil
                                              okBlock:nil];
            return;
        }
        [self EOPApiCall:TRUE];
    }
}

- (void)EOPApiCall:(BOOL)isShowPopup{
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
    [self.eventModel getEOPRegistrationCall:[self.contact.addressCode stringValue] agentCode:[CachingService sharedInstance].currentUserId success:^(NSSet * eventSet){
        [self.eventModel getPastEOP:[self.contact.addressCode stringValue]  agentCode:[CachingService sharedInstance].currentUserId success:^(NSSet *pastEventSet){
            NSSet *mergeEventSet = [eventSet setByAddingObjectsFromSet:pastEventSet];
            [self.eventModel saveEopEvents:mergeEventSet];
            [self.eventModel getEOPDeleteCall:^(id responseObject) {
                [self eopEventRegAction:isShowPopup];
            } failure:^(id responseObject, NSError *error) {
                [self eopEventRegAction:isShowPopup];
            }];
         } failure:^(id responseObject, NSError *error) {
             [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
         }];
        if(isShowPopup)
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
    } failure:^(id responseObject, NSError *error) {
        if(isShowPopup)
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        else
            [self eopEventRegAction:isShowPopup];
    }];
}

- (void)eopEventRegAction:(BOOL)isShowPopup{
    TblEOP *eop = (TblEOP *)[DbUtils fetchObject:@"TblEOP"
                                      andPredict:[NSPredicate predicateWithFormat:@"calendarEvent.agentCode == %@ AND isRegistered = 1",[CachingService sharedInstance].currentUserId]
                               andSortDescriptor:[NSSortDescriptor sortDescriptorWithKey:@"eventDate" ascending:FALSE]
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    
    if (eop.isRegisteredValue) {
        [ApplicationFunction setUpdateUserProcess:RECURITMENT_EOP_REGISTRATION
                                          contact:self.contact
                                    processStatus:(eop) ? RECURITMENT_STATUS_TRUE : RECURITMENT_STATUS_FALSE
                                      processDate:[NSDate date]];
    } else {
        [ApplicationFunction setUpdateUserProcess:RECURITMENT_EOP_REGISTRATION
                                          contact:self.contact
                                    processStatus:nil
                                      processDate:nil];
        
    }

    if(isShowPopup)
        [self showEOPRegistrationPopoverCtrl:self.contact
                                        rect:[MyAppDelegate window].rootViewController.view.bounds
                                      inView:[MyAppDelegate window].rootViewController.view
                               callBackBlock:^{
                                   [self setRecurimentProcess:TRUE];
                               }];
    else
        
        [self getFirstInterViewApiCall:isShowPopup];
}

- (IBAction)onBtnFirstInterviewClick:(id)sender {
    [self showFirstInterviewPopoverCtrl:self.contact
                                   rect:[MyAppDelegate window].rootViewController.view.bounds
                                 inView:[MyAppDelegate window].rootViewController.view
                          callBackBlock:^{
                              [self setRecurimentProcess:TRUE];
                              if(self.contact.candidateProcess.firstInterviewResult != FIRST_INTERVIEW_FAIL)
                                  [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                                                            candidateName:self.contact.name
                                                                     desc:NSLocalizedString(@"candidate has passed first interview, do not forget CC Test!", @"")
                                                                     rect:[MyAppDelegate window].rootViewController.view.bounds
                                                                   inView:[MyAppDelegate window].rootViewController.view];
                          }];
}


- (void)getFirstInterViewApiCall:(BOOL)isShowAttePopup
{
    if([self.contact.addressCode integerValue] == 0){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"Please sync candidate",@"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }
    
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
    [self.eventModel getFirstInterview:[self.contact.addressCode stringValue] agentID:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        TblCandidateProcess *candidateProcess = self.contact.candidateProcess;
        if(!candidateProcess){
            candidateProcess = [[ContactDataProvider sharedContactDataProvider] createCandidateProcess];
            candidateProcess.contact = self.contact;
        }
        NSString *result = [responseObject objectForKey:@"interviewResult"];
        NSDate *passDate = [DateUtils stringToDate:[responseObject objectForKey:@"passTime"] dateFormat:kServerDateFormatte];
        NSString *recruitmentPlan = ([responseObject objectForKey:@"recruitmentPlan"]) ? [responseObject objectForKey:@"recruitmentPlan"] : @"";
        
        NSString *recruitmentRemarks = ([responseObject objectForKey:@"remarks"]) ? [responseObject objectForKey:@"remarks"] : @"";
        
        if(!isEmptyString(result)){
            candidateProcess.firstInterviewDate = passDate;
            candidateProcess.firstInterviewStatus = ([result isEqualToString:FIRST_INTERVIEW_PASS]) ? RECURITMENT_STATUS_TRUE : ([result isEqualToString:FIRST_INTERVIEW_FAIL]) ?RECURITMENT_STATUS_FALSE : result;
            candidateProcess.firstInterviewResult = result;
            candidateProcess.firstInterviewRecruitment = recruitmentPlan;
            candidateProcess.firstInterviewRemark = recruitmentRemarks;
            candidateProcess.completeProgressDate = [NSDate date];
            [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
        }
       
        if (isShowAttePopup) {
             [self setRecurimentProcess:TRUE];
            if(self.contact.candidateProcess.firstInterviewResult != FIRST_INTERVIEW_FAIL)
                [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                                          candidateName:self.contact.name
                                                   desc:NSLocalizedString(@"candidate has passed first interview, do not forget CC Test!", @"")
                                                   rect:[MyAppDelegate window].rootViewController.view.bounds
                                                 inView:[MyAppDelegate window].rootViewController.view];
        } else {
            [self setRecurimentProcess:FALSE];
            [self atteFirstInterApiCall:isShowAttePopup];
        }
        

    } failure:^(id responseObject, NSError *error) {
        if(isShowAttePopup)
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        else
            [self atteFirstInterApiCall:isShowAttePopup];

    }];
}



- (IBAction)onBtnAttendEOPClick:(id)sender{
    if([self isInternetConnect])
        [self atteFirstInterApiCall:TRUE];
}

- (void)atteFirstInterApiCall:(BOOL)isShowAttePopup{
    if([self.contact.addressCode integerValue] == 0){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"Please sync candidate",@"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
    [self.eventModel getCandidateRegisteredEOP:[self.contact.addressCode stringValue] agentCode:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
        if(isShowAttePopup)
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        NSArray *response = (NSArray *)responseObject;
        if (response.count > 0) {
            NSArray *attandEopArray = [responseObject sortedArrayUsingComparator:^(NSMutableDictionary *obj1,NSMutableDictionary *obj2) {
                NSString *num1 = [obj1 objectForKey:@"eventDate"];
                NSString *num2 = [obj2 objectForKey:@"eventDate"];
                return (NSComparisonResult) [num1 compare:num2 options:(NSNumericSearch)];
            }];

            NSInteger attenededEOPCount = 0;
            for (NSDictionary *attandEopDic in attandEopArray) {
                if([[attandEopDic valueForKey:@"attendedStatus"] isEqualToString:@"Y"]){
                    [self addRecruitmentNote:0
                             recruitmentStep:RS_ATTEND_EOP
                                activityDate:[NSDate stringToDate:[attandEopDic valueForKey:@"eventDate"] dateFormat:kServerDateFormatte]
                                     contact:self.contact
                                 description:ATTENDED_EOP_DESCRIPTION];
                }
            }
            
            self.contact.attenededEOPCount = [NSNumber numberWithInteger:attenededEOPCount];
            [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
            
            for (NSDictionary *attandEopDic in attandEopArray) {
                if([[attandEopDic valueForKey:@"attendedStatus"] isEqualToString:@"Y"]){
                    [ApplicationFunction setUpdateUserProcess:RECURITMENT_ATTEND_EOP
                                                      contact:self.contact
                                                processStatus:RECURITMENT_STATUS_TRUE
                                                  processDate:[NSDate stringToDate:[attandEopDic valueForKey:@"eventDate"] dateFormat:kServerDateFormatte]];
                    [self setRecurimentProcess:isShowAttePopup];
                    break;
                }
            }
            
            if(isShowAttePopup)
                [self showEOPAttendancePopoverCtrl:response
                                              rect:[MyAppDelegate window].rootViewController.view.bounds
                                            inView:[MyAppDelegate window].rootViewController.view
                                     callBackBlock:^(BOOL isAttended){
                                         if (isAttended) {
                                         [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                                                                   candidateName:self.contact.name
                                                                            desc:NSLocalizedString(@"has attended company event, keep it up!", @"")
                                                                            rect:[MyAppDelegate window].rootViewController.view.bounds
                                                                          inView:[MyAppDelegate window].rootViewController.view];
                                         }
                                     }];
            else
                [self CCTestApi:isShowAttePopup];
        }else{
            [ApplicationFunction setUpdateUserProcess:RECURITMENT_ATTEND_EOP
                                              contact:self.contact
                                        processStatus:RECURITMENT_STATUS_FALSE
                                          processDate:nil];
            [self setRecurimentProcess:isShowAttePopup];
            if(isShowAttePopup)
                [DisplayAlert showAlertInControllerInView:self
                                                    title:NSLocalizedString(@"Not found register user", @"")
                                                  message:@""
                                        cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                            okButtonTitle:@""
                                              cancelBlock:nil
                                                  okBlock:nil];
            else
                [self CCTestApi:isShowAttePopup];
        }
    } failure:^(id responseObject, NSError *error) {
        if(isShowAttePopup)
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        else
            [self CCTestApi:isShowAttePopup];
    }];
}

- (IBAction)onBtnCCTestClick:(id)sender {
    if([self.contact.addressCode integerValue] == 0){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"Please sync candidate", @"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }
    [self CCTestApi:TRUE];
}

- (void)CCTestApi:(BOOL)isShowPopup{
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
    [self.eventModel getCCTestResult:[CachingService sharedInstance].currentUserId candidateCode:[self.contact.addressCode stringValue] success:^(id responseObject) {
        if(isShowPopup)
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        for (NSArray *responseArray in responseObject) {
            NSString *status = [responseArray valueForKey:@"CCTestResult"];
            NSString *statusDate = [responseArray valueForKey:@"Date"];
            NSDate *processDate = [DateUtils stringToDate:statusDate dateFormat:kServerDateFormatte];
            if(isEmptyString(status) || [status isEqualToString:@"null"]){
                if(isShowPopup)
                    [self showCCTestPopover];
                else
                    [self showCompanyInterviewStatus:isShowPopup];
            }else{
                self.contact.candidateProcess.ccTestRecruitment = status;
                [ApplicationFunction setUpdateUserProcess:RECURITMENT_CC_TEST
                                                  contact:self.contact
                                            processStatus:([status isEqualToString:CAUTION_RECRUIT]) ? RECURITMENT_STATUS_FALSE : RECURITMENT_STATUS_TRUE
                                              processDate:processDate];
                
                if(isShowPopup) {
                    [self showCCTestPopover];
                    [self setRecurimentProcess:TRUE];
                }
                else{
                    [self showCompanyInterviewStatus:isShowPopup];
                }
            }
        }
    }failure:^(id responseObject, NSError *error) {
        if(isShowPopup){
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
            [self showCCTestPopover];
        }else
            [self showCompanyInterviewStatus:isShowPopup];
    }];
}

- (void)showCCTestPopover{
    [self showCCTestPopoverCtrl:self.contact
                           rect:[MyAppDelegate window].rootViewController.view.bounds
                         inView:[MyAppDelegate window].rootViewController.view
                  callBackBlock:^{
                      [self setRecurimentProcess:TRUE];
                      [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
                      [self.eventModel postCCTestResult:[CachingService sharedInstance].currentUserId
                                          candidateCode:[self.contact.addressCode stringValue]
                                             testResult:self.contact.candidateProcess.ccTestRecruitment
                                             resultDate:[NSDate dateToString:self.contact.candidateProcess.ccTestDate
                                                                  andFormate:kNewDisplayDateFormatte]
                                                success:^(id responseObject) {
                          [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
                          [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                                                    candidateName:self.contact.name
                                                             desc:NSLocalizedString(@"has completed CC Test.", @"")
                                                             rect:[MyAppDelegate window].rootViewController.view.bounds
                                                           inView:[MyAppDelegate window].rootViewController.view];
                      } failure:^(id responseObject, NSError *error) {
                          [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
                      }];
                  }];
}

- (IBAction)onBtnCompanyInteviewClick:(id)sender {
    if([self isInternetConnect]){
        if([self.contact.addressCode integerValue] == 0){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Please sync candidate", @"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:nil
                                          cancelBlock:nil
                                              okBlock:nil];
            return;
        }
        [self showCompanyInterviewStatus:TRUE];
    }
}

- (void)showCompanyInterviewStatus:(BOOL)isShowRegPopup{
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
    [self.eventModel getCandidateInterviewStatus:@"3rd"
                                forCandidateCode:[self.contact.addressCode stringValue]
                                         success:^(id responseObject) {
                                             [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
                                             for (NSArray *resArray in responseObject) {
                                                 NSString *response = [resArray valueForKey:@"interviewStatus"];
                                                 [ApplicationFunction setUpdateUserProcess:RECURITMENT_COMPANY_INTERVIEW
                                                                                   contact:self.contact
                                                                             processStatus:([response isEqualToString:@"PASS"]) ? RECURITMENT_STATUS_TRUE : RECURITMENT_STATUS_FALSE
                                                                               processDate:([response isEqualToString:@"PASS"]) ? [NSDate date] : nil];
                                                 
                                             }
                                             [self companyInterviewRegApiCall:isShowRegPopup];
                                         } failure:^(id responseObject, NSError *error) {
                                             [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
                                             [self companyInterviewRegApiCall:isShowRegPopup];
                                         }];
}

- (void)companyInterviewRegApiCall:(BOOL)isShowRegPopup{
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
    [self.eventModel getAllInterviewWithCandidateCode:[self.contact.addressCode stringValue] agentCode:[CachingService sharedInstance].currentUserId success:^(NSSet *interviewEvents){
        [self.eventModel getAllPastInterviewWithCandidateCode:[self.contact.addressCode stringValue] agentCode:[CachingService sharedInstance].currentUserId success:^ (NSSet *pastInterviewEvents)
        {
            if(isShowRegPopup)
                [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
            NSSet *mergeInterviewEvents = [interviewEvents setByAddingObjectsFromSet:pastInterviewEvents];
            [self.eventModel saveInterviewEvents:mergeInterviewEvents];
            [self.eventModel deleteInterviewCall:^(id responseObject) {
                [self showInterviewEvents:isShowRegPopup];
            } failure:^(id responseObject, NSError *error) {
                [self showInterviewEvents:isShowRegPopup];
            }];
        } failure:^(id responseObject, NSError *error) {
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        }];
    } failure:^(id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        if(!isShowRegPopup)
            [self showInterviewEvents:isShowRegPopup];
    }];
}

- (void)showInterviewEvents:(BOOL)isShowRegPopup{
    if(isShowRegPopup)
        [self showCompanyInterviewPopoverCtrl:self.contact
                                         rect:[MyAppDelegate window].rootViewController.view.bounds
                                       inView:[MyAppDelegate window].rootViewController.view
                                callBackBlock:^ (BOOL isPassed) {
                                    if (isPassed) {
                                        [self setRecurimentProcess:TRUE];
                                        [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                                                                  candidateName:self.contact.name
                                                                           desc:NSLocalizedString(@"has registered for interview! Please remember to bring all interview materials, go for it!", @"")
                                                                           rect:[MyAppDelegate window].rootViewController.view.bounds
                                                                         inView:[MyAppDelegate window].rootViewController.view];
                                    }else
                                        [self setRecurimentProcess:FALSE];
                                }
                                candidateForm:^{
                                    [self.pickerPopover dismissPopoverAnimated:TRUE];
                                    [self applicationForm:nil];
                                }];
    else
        [self abcTrainingApiCall:isShowRegPopup];
}

- (IBAction)onBtnALEExamClick:(id)sender {
    if([self isInternetConnect]){
    if([self.contact.addressCode integerValue] == 0){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"Please sync candidate", @"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }
    [self showALEExamPopoverCtrl:NSLocalizedString(@"ALE Exam", @"")
                        descr:@"完成签约后会自动更新，请进行岗前培训报名"
                         rect:[MyAppDelegate window].rootViewController.view.bounds
                       inView:[MyAppDelegate window].rootViewController.view
                callBackBlock:^{
                }];
    }
    //Fix for issue 89 remove ALE call
//    [self displayAlert:NSLocalizedString(@"ALE Exam", @"")
//              alertMsg:[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"Congratulations", @""), self.contact.name, ]];
    
//    [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
//                              candidateName:self.contact.name
//                                       desc:NSLocalizedString(@"has successfully passed a round of interview!", @"")
//                                       rect:[MyAppDelegate window].rootViewController.view.bounds
//                                     inView:[MyAppDelegate window].rootViewController.view];
    //[self setRecurimentProcess:TRUE];
    
    return;
//    if(self.contact.candidateProcess.candidateProgressValue >= 6){
//        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
//        [self.eventModel getALEExamResults:[self.contact.addressCode stringValue] agentID:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
//            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
//            for (NSArray *responseArray in responseObject) {
//                NSDate *date = [NSDate stringToDate:[responseArray valueForKey:@"Date"] dateFormat:@"dd/MM/yyyy hh:mm:ss"];
//                NSString *status = [responseArray valueForKey:@"Status"];
//                NSString *strTitle;
//                NSString *strDescription;
//                if(isEmptyString(status)){
//                    strTitle = @"ALE Exam";
//                    strDescription = @"ALE Exam";
//                }else if([status isEqualToString:@"Passed"]){
//                    strTitle = @"Passed ALE";
//                    strDescription = @"Pass";
//                }else{
//                    [ApplicationFunction openSafariBrowser:@"http://http://uat.aia.com.cn//souAtms/login.do?username=nsnp123&password=A000000A&branchCode=0986&lname=ss&id=sss&sex=&degree=&tele_num&Source=&address=&city="];
//                }
//                if(!isEmptyString(strTitle))
//                    [self showALEExamPopoverCtrl:strTitle
//                                           descr:strDescription
//                                            rect:[MyAppDelegate window].rootViewController.view.bounds
//                                          inView:[MyAppDelegate window].rootViewController.view
//                                   callBackBlock:^{
//                                   }];
//                [ApplicationFunction setUpdateUserProcess:RECURITMENT_ALE_EXAM
//                                                  contact:self.contact
//                                            processStatus:([status isEqualToString:@"Passed"]) ? @"TRUE" : @"FALSE"
//                                              processDate:date];
//                [self setRecurimentProcess:TRUE];
//            }
//        } failure:^(id responseObject, NSError *error) {
//            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
//        }];
//    }
}

- (IBAction)onBtnABCTrainingClick:(id)sender {
    if([self isInternetConnect]){       
        if(![self.contact.candidateProcess.companyInterviewStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Company interview don't registered.", @"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:@""
                                          cancelBlock:nil
                                              okBlock:nil];
            return;
        }
        [self abcTrainingApiCall:TRUE];
    }
}

- (void)abcTrainingApiCall:(BOOL)isShowABCPassPopup{
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
    NSString *strURL = [self getTrainingSSOURL];
    [self.eventModel getABCTraningResults:[[CachingService sharedInstance] currentUserId] agentID:self.contact.nric success:^(id responseObject) {
        NSString *response = [responseObject valueForKey:@"trainingResult"];
        if(!isEmptyString(response) && response){
            if([response isEqualToString:@"PASS"]){
//                [self createNoteWithTblContact:self.contact
//                                      noteDate:[NSDate date]
//                                      noteDesc:NSLocalizedString(@"Passed ABC Training", @"")];
                
                [self updateABCRecruitment:RECURITMENT_STATUS_TRUE
                              isSetProcess:isShowABCPassPopup];
                
                if(isShowABCPassPopup)
                    [self showABCTrainingPassPopoverCtrl:[MyAppDelegate window].rootViewController.view.bounds
                                                  inView:[MyAppDelegate window].rootViewController.view
                                           callBackBlock:^{
                                               [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                                                                         candidateName:self.contact.name
                                                                                  desc:NSLocalizedString(@"has passed the training! Bring your partner to sign the contract!", @"")
                                                                                  rect:[MyAppDelegate window].rootViewController.view.bounds
                                                                                inView:[MyAppDelegate window].rootViewController.view];
                                               
                                           }];
            }else
                [self updateABCRecruitment:RECURITMENT_STATUS_FALSE isSetProcess:isShowABCPassPopup];
        }else{
            [self updateABCRecruitment:@"" isSetProcess:isShowABCPassPopup];
            if(isShowABCPassPopup)
                [ApplicationFunction openSafariBrowser:strURL];
        }
        if(isShowABCPassPopup)
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        else
            [self contractApiCall:isShowABCPassPopup];
    } failure:^(id responseObject, NSError *error) {
        if(isShowABCPassPopup){
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
            [ApplicationFunction openSafariBrowser:strURL];
        }else
            [self contractApiCall:isShowABCPassPopup];
    }];
}

- (void)updateABCRecruitment:(NSString *)status isSetProcess:(BOOL)isSetProcess{
    [ApplicationFunction setUpdateUserProcess:RECURITMENT_ABC_TRAINING
                                      contact:self.contact
                                processStatus:status
                                  processDate:isEmptyString(status) ? nil : [NSDate date]];
    if(isSetProcess)
        [self setRecurimentProcess:TRUE];
}

- (IBAction)onBtnContractClick:(id)sender{
    if([self isInternetConnect]){
        if([self.contact.addressCode integerValue] == 0){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Please sync candidate",@"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:nil
                                          cancelBlock:nil
                                              okBlock:nil];
            return;
        }
        [self contractApiCall:TRUE];
    }
}

- (void)contractApiCall:(BOOL)isShowContractPopup{
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
    [self.eventModel getContractDetail:[self.contact.addressCode stringValue] success:^(id responseObject) {
         [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        NSDate *contractDate = [DateUtils stringToDate:[responseObject valueForKey:@"contractDate"] dateFormat:kServerDateFormatte];
        if(contractDate){
            TblCandidateProcess *candidateProcess = self.contact.candidateProcess;
            candidateProcess.recruitmentType = [responseObject valueForKey:@"recruitmentType"];
            candidateProcess.contractStatus = candidateProcess.aleExamStatus = RECURITMENT_STATUS_TRUE;
            candidateProcess.contractDate = candidateProcess.aleExamDate = contractDate;
            candidateProcess.candidateAgentCode = [responseObject valueForKey:@"candidateAgentCode"];
            [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
            
            NSArray *noteArray = [[self.contact.candidateNotes allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"activityDate = %@ AND desc = %@",contractDate,NSLocalizedString(@"Completed Contract", @"")]];
            if(noteArray.count == 0){
//                [self createNoteWithTblContact:self.contact
//                                      noteDate:contractDate
//                                      noteDesc:NSLocalizedString(@"Completed Contract", @"")];
//                [self createNoteWithTblContact:self.contact
//                                      noteDate:contractDate
//                                      noteDesc:NSLocalizedString(@"Completed ALE Exam", @"")];
            }
            
            [self setRecurimentProcess:TRUE];
            if(isShowContractPopup){
               
                [self showCandidateRankingPassPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                                                     rect:self.view.frame
                                                   inView:self.view
                                                         candidateAgentCode:self.contact.candidateProcess.candidateAgentCode];
                
                
                [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                                          candidateName:@""
                                                   desc:[NSString stringWithFormat:@"%@ %@, %@",NSLocalizedString(@"you have a new partner", @""), self.contact.name, NSLocalizedString(@"Agent Code is: xxxxxxxxx.", @"")]
                                                   rect:[MyAppDelegate window].rootViewController.view.bounds
                                                 inView:[MyAppDelegate window].rootViewController.view];
            }
        }else{
            [self setRecurimentProcess:TRUE];
        }
        self.isSyncRecruitmentProgress = FALSE;
    } failure:^(id responseObject, NSError *error) {
        if(isShowContractPopup)
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        else
            self.isSyncRecruitmentProgress = FALSE;
        [self setRecurimentProcess:TRUE];
    }];
}

- (IBAction)onBtnSendEgreetingClick:(id)sender {
    [self performSegueWithIdentifier:@"AddressBookToGreeting" sender:self];
}

- (IBAction)onBtnNotesClick:(id)sender {
    [self performSegueWithIdentifier:@"ContactDetailToNote" sender:self];
}

- (IBAction)applicationForm:(id)sender {
    [self performSegueWithIdentifier:@"candidateApplicationForm" sender:self];
}

- (IBAction)barCodePressed:(id)sender {
    [self showSocialSharingPopoverCtrl:self.contact
                                EOPObj:nil
                            isGreeting:FALSE
                                 isEOP:TRUE
                            isQRCode:YES
                            shareImage:self.barCodeButton.currentImage
                        shareImageFile:@""
                              shareMsg:[self.contact getLocalizedQRCodeString]
                              shareSub:NSLocalizedString(@"Share Contact",@"")
                                  rect:self.barCodeButton.frame
                                inView:self.view];
}

- (IBAction)intractivePresenter:(id)sender {
    if(isEmptyString([self.contact getLocalizedQRCodeString])){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"QR Code must be generated for Contact",@"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
    } else {
        [self showInteractivePresenter];
    }
}

- (void)showInteractivePresenter
{
    FTDHomeViewController *FTDHomeViewCol=[[FTDHomeViewController alloc]init];
    FTDHomeViewCol.contact = self.contact;
    TFDNavViewController *navFTDHomeViewCol=[[TFDNavViewController alloc]initWithRootViewController:FTDHomeViewCol];
    navFTDHomeViewCol.navigationBar.hidden=YES;
    [navFTDHomeViewCol setPresentercallBackBlock:^(TblContact *contact){
        //TODO check if contact has presenterReport
        contact.isSync = [NSNumber numberWithBool:TRUE];
        if(self.contact.presenterReport && self.contact.presenterReportDate){
            [ApplicationFunction setUpdateUserProcess:RECURITMENT_INTERACTIVE_PRESENER
                                              contact:self.contact
                                        processStatus:RECURITMENT_STATUS_TRUE
                                          processDate:self.contact.presenterReportDate];
            [self addRecruitmentNote:0
                     recruitmentStep:RS_INTERACTIVE_PRESENTER
                        activityDate:self.contact.candidateProcess.interactivePresentDate
                             contact:self.contact
                         description:INTERACTIVE_PRESENTER_COMPLETED_DESCRIPTION];
            [self setRecurimentProcess:TRUE];
            [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                                      candidateName:self.contact.name
                                               desc:NSLocalizedString(@"completed interactive presenter, bring your partner into the next step!", @"")
                                               rect:[MyAppDelegate window].rootViewController.view.bounds
                                             inView:[MyAppDelegate window].rootViewController.view];
        } else {
            [ApplicationFunction setUpdateUserProcess:RECURITMENT_INTERACTIVE_PRESENER
                                              contact:self.contact
                                        processStatus:RECURITMENT_STATUS_FALSE
                                          processDate:self.contact.presenterReportDate];
            [self setRecurimentProcess:TRUE];

        }
    }];

    
    [navFTDHomeViewCol setEopCallBackBlock:^{
        [ApplicationFunction showToAgentProfile:YES];
    }];
    [self presentViewController:navFTDHomeViewCol animated:YES completion:nil];

}

- (NSString *)getTrainingSSOURL
{
    TblAgentDetails *agentDetails = [TblAgentDetails getCurrentAgentDetails];
    NSMutableString *strURL;
    if ([agentDetails.branch isEqualToString:@"1086"] ||
        [agentDetails.branch isEqualToString:@"2586"] ||
        [agentDetails.branch isEqualToString:@"2686"] ||
        [agentDetails.branch isEqualToString:@"2786"] ||
        [agentDetails.branch isEqualToString:@"2886"]) {
        strURL = [NSMutableString stringWithString:@"http://uat.aia.com.cn/souAtms/login.do?username=%@&password=%@&branchCode=%@&loginType=1&name=%@&id=%@&sex=%@&degree=%@&tele_num=%@&Source=%@&address=%@" ];
    } else if ([agentDetails.branch isEqualToString:@"0986"] ||
                   [agentDetails.branch isEqualToString:@"1186"] ||
                   [agentDetails.branch isEqualToString:@"1286"]) {
        strURL = [NSMutableString stringWithString:@"http://uat.aia.com.cn/ATMS/login.do?username=%@&password=%@&branchCode=%@&loginType=1&name=%@&id=%@&sex=%@&degree=%@&tele_num=%@&source=%@&address=%@,%@"];
    }
    
    if ([@[@"A1", @"A2",@"A3",@"A4",@"M1",@"M2",@"M3"] containsObject:agentDetails.title]) {
        [strURL appendFormat:@"&recru=%@",agentDetails.userID];
    }
        
        
    
    NSString *pwd;
    NSString *jsonString = [JNKeychain loadValueForKey:agentDetails.userID];
    if (jsonString != nil)
    {
        NSError * err;
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *fullContentDict = [NSJSONSerialization
                                         JSONObjectWithData: jsonData
                                         options: NSJSONReadingMutableContainers
                                         error: &err];
        if (fullContentDict != nil)
        {
            pwd = [fullContentDict objectForKey:@"password"];

        }
    }
    


    strURL = [NSString stringWithFormat:strURL,agentDetails.userID,pwd,agentDetails.branch,self.contact.name,self.contact.nric,self.contact.gender? [self getTrimmedValue:self.contact.gender]:@"",self.contact.education, ((NSString *)self.contact.mobilePhoneNo).length>0 ? [self getTrimmedValue:self.contact.mobilePhoneNo]:@"" ,[self.contact.newcomerSource lowercaseString],self.contact.residentialAddress ? self.contact.residentialAddress:@"",self.contact.residentialAddress1 ? self.contact.residentialAddress1:@""];
    
    NSLog(@"Training URL:%@",strURL);
    
    NSString* encodedUrl = [[strURL stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return encodedUrl;
}

- (NSString *)getTrimmedValue:(NSString *)string
{
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

//-----------------------------------------------------------------------------------------------

-(void)buttonFram :(UIButton *)btn
{
    bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(8, btn.frame.size.height - 1.0f, btn.frame.size.width-16, 2)];
    bottomBorder.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
    
}
-(IBAction)btnOverview:(id)sender
{
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnOverview];
    [self.btnOverview addSubview:bottomBorder];
    
}
-(IBAction)btnDetails:(id)sender
{
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnDetails];
    [self.btnDetails addSubview:bottomBorder];
    
    
}
-(IBAction)btnApplicationForms:(id)sender
{
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnApplicationForm];
    [self.btnApplicationForm addSubview:bottomBorder];
    CandidateApplicationFormViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"CandidateApplicationFormViewController"];
    vc.addressCode = self.contact.iosAddressCode;
    [vc setCompletionBlock:^{
        [self loadData];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}
-(IBAction)btnNotes:(id)sender
{
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnNote];
    [self.btnNote addSubview:bottomBorder];
    NoteListViewController *noteListViewCtrl = [self.storyboard instantiateViewControllerWithIdentifier:@"noteVC"];
    noteListViewCtrl.contact = self.contact;
    [noteListViewCtrl setUpdateLastContactedDateBlock:^{
        [self lastContactUpdated];
    }];
    [self.navigationController pushViewController:noteListViewCtrl animated:YES];

}

@end
