//
//  MenuAddViewController.m
//  AIAChina
//
//  Created by AIA on 30/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "MenuAddViewController.h"

@interface MenuAddViewController ()

@end

@implementation MenuAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBtnAddContactClick:(id)sender {
    if(self.showIndex)
        self.showIndex(0);
}

- (IBAction)onBtnAddGroupClick:(id)sender {
//    [self performSegueWithIdentifier:@"MenuAddToGroup" sender:self];
    if(self.showIndex)
        self.showIndex(1);
}
@end
