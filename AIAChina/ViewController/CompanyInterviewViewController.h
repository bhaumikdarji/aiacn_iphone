//
//  CompanyInterviewViewController.h
//  AIAChina
//
//  Created by AIA on 07/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface CompanyInterviewViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UIButton *btn2ndInterview;
@property (weak, nonatomic) IBOutlet UIButton *btn3rdInterview;
@property (weak, nonatomic) IBOutlet UITableView *tblCompanyInterview;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgInterviewCompleteStep;
@property (weak, nonatomic) IBOutlet UILabel *lblCandidateForm;
@property (weak, nonatomic) IBOutlet UIButton *btnCandidateForm;
@property (weak, nonatomic) IBOutlet UILabel *lbl1stInterview;

@property (weak, nonatomic) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet UILabel *lblInterviewName;
@property (weak, nonatomic) IBOutlet UILabel *lblInterviewType;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;
@property (weak, nonatomic) IBOutlet UITextView *txtLocation;
@property (weak, nonatomic) IBOutlet UITextView *txtInterviewMaterial;
@property (weak, nonatomic) IBOutlet UIButton *btnAttachment;
@property (weak, nonatomic) IBOutlet UIButton *btnTotalCandidates;
@property (weak, nonatomic) IBOutlet UILabel *lblQuotaBalance;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnViewResult;

@property (nonatomic) int quata;
@property (strong, nonatomic) NSArray *companyInterviewArray;
@property (strong, nonatomic) TblInterview *detailInterview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *interviewCompStepHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attachHeightConstraint;

@property (strong, nonatomic) TblContact *contact;
@property (copy, nonatomic) void(^callBackBlock)(BOOL isPassed);
@property (copy, nonatomic) void(^candidateApplicationBlock)();

- (IBAction)onBtnTotalCandidatesClick:(id)sender;
- (IBAction)onBtn2ndInterviewClick:(id)sender;
- (IBAction)onBtn3rdInterview:(id)sender;
- (IBAction)onBtnRegisterClick:(id)sender;
- (IBAction)onBtnViewResultClick:(id)sender;
- (IBAction)onBtnCandidateFormClick:(id)sender;
- (IBAction)onBtnAttachmentClick:(id)sender;

@end
