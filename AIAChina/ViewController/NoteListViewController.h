//
//  NoteListViewController.h
//  AIAChina
//
//  Created by AIA on 20/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface NoteListViewController : AIAChinaViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTotalNotes;
@property (weak, nonatomic) IBOutlet UITableView *tblNote;
@property (weak, nonatomic) IBOutlet UIView *borderView;

@property (strong, nonatomic) TblContact *contact;
@property (strong, nonatomic) NSArray *noteArray;
@property (strong, nonatomic) NSMutableArray *selectNoteArray;
@property (strong, nonatomic) NSString *sortBy;

@property (strong, nonatomic) UIPopoverController *notePopoverCntrlr;
@property (copy, nonatomic) void(^updateLastContactedDateBlock)();

- (IBAction)onBtnExportNotesActivityClick:(id)sender;
- (IBAction)onBtnAddNoteClcik:(id)sender;
- (IBAction)onBtnActivityClick:(id)sender;
- (IBAction)onBtnDateClick:(id)sender;
- (IBAction)onBtnDescriptionClick:(id)sender;
- (IBAction)onBtnSelectAllClick:(id)sender;

@end
