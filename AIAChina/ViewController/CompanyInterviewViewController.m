//
//  CompanyInterviewViewController.m
//  AIAChina
//
//  Created by AIA on 07/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CompanyInterviewViewController.h"
#import "CompanyInterviewCell.h"
#import "ContactDataProvider.h"
#import "ContactEventModel.h"
#import "PdfDownloadObject.h"
#import "AFHTTPRequestOperationManager.h"
#import "CalendarEventModel.h"
@interface CompanyInterviewViewController ()
@property (nonatomic, retain) ContactEventModel *eventModel;
@property (nonatomic, assign) NSInteger lastSelectedIndex;
@property (nonatomic, assign) NSInteger selectedIndex;

@end

@implementation CompanyInterviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Company Interview", @"");
    
    self.eventModel = [ContactEventModel sharedInstance];
    self.selectedIndex = 0;
    [self onBtn2ndInterviewClick:nil];
    [self getInteviewStatus];
    
    if(![ApplicationFunction isCompletePersonalInfo:self.contact]){
        ((UIImageView *)self.imgInterviewCompleteStep[0]).image = [UIImage imageNamed:@"ic_small_checkbox.png"];
        self.btnCandidateForm.hidden = FALSE;
        self.lblCandidateForm.textColor = [UIColor colorWithRed:200.0/255.0 green:23.0/255.0 blue:67.0/255.0 alpha:1.0];
    }else if(![ApplicationFunction isCompleteEducation:self.contact]){
        ((UIImageView *)self.imgInterviewCompleteStep[0]).image = [UIImage imageNamed:@"ic_small_checkbox.png"];
        self.btnCandidateForm.hidden = FALSE;
        self.lblCandidateForm.textColor = [UIColor colorWithRed:200.0/255.0 green:23.0/255.0 blue:67.0/255.0 alpha:1.0];
    }else if(![ApplicationFunction isCompleteSignature:self.contact]){
        ((UIImageView *)self.imgInterviewCompleteStep[0]).image = [UIImage imageNamed:@"ic_small_checkbox.png"];
        self.btnCandidateForm.hidden = FALSE;
        self.lblCandidateForm.textColor = [UIColor colorWithRed:200.0/255.0 green:23.0/255.0 blue:67.0/255.0 alpha:1.0];
    }
    
    if(isEmptyString(self.contact.referalSource))
        ((UIImageView *)self.imgInterviewCompleteStep[1]).image = [UIImage imageNamed:@"ic_small_checkbox.png"];
    
    if(![self.contact.candidateProcess.firstInterviewResult isEqualToString:FIRST_INTERVIEW_PASS]){
        ((UIImageView *)self.imgInterviewCompleteStep[2]).image = [UIImage imageNamed:@"ic_small_checkbox.png"];
        self.lbl1stInterview.textColor = [UIColor colorWithRed:200.0/255.0 green:23.0/255.0 blue:67.0/255.0 alpha:1.0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.companyInterviewArray.count > 0)
        self.rightView.hidden = FALSE;
    else
        self.rightView.hidden = TRUE;
    return self.companyInterviewArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CompanyInterviewCell *cell = (CompanyInterviewCell *)[tableView dequeueReusableCellWithIdentifier:@"CompanyInterviewCell"];
    TblInterview *interview = self.companyInterviewArray[indexPath.row];
    [cell loadCompanyInterviewData:interview];
    
    cell.btnInterviewDate.tag = cell.btnInterviewStatus.tag = indexPath.row;
    [cell.btnInterviewDate addTarget:self action:@selector(onBtnInterviewDateClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnInterviewStatus addTarget:self action:@selector(onBtnInterviewDateClick:) forControlEvents:UIControlEventTouchUpInside];
    if([interview.isRegistered boolValue])
        cell.registeredImageView.hidden = NO;
    else
        cell.registeredImageView.hidden = YES;
    if (indexPath.row == self.selectedIndex) {
        cell.backgroundColor = REG_BG_COLOR;
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    if([interview.objectID isEqual:self.detailInterview.objectID])
        [self loadInterviewDetail:interview];
    
    return  cell;
}

- (IBAction)onBtnInterviewDateClick:(UIButton *)sender{
    
    if (self.lastSelectedIndex>=0) {
        CompanyInterviewCell *cell = [self.tblCompanyInterview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.lastSelectedIndex inSection:0]];
        cell.backgroundColor = [UIColor whiteColor];
    }
    CompanyInterviewCell *cell = [self.tblCompanyInterview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    cell.backgroundColor = REG_BG_COLOR;
    self.lastSelectedIndex = sender.tag;
    [self loadInterviewDetail:self.companyInterviewArray[[sender tag]]];
}

- (IBAction)onBtnInterviewStatusClick:(UIButton *)sender{
    if (self.lastSelectedIndex>=0) {
        CompanyInterviewCell *cell = [self.tblCompanyInterview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.lastSelectedIndex inSection:0]];
        cell.backgroundColor = [UIColor whiteColor];
    }
    CompanyInterviewCell *cell = [self.tblCompanyInterview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    cell.backgroundColor = REG_BG_COLOR;
    self.lastSelectedIndex = sender.tag;
    [self loadInterviewDetail:self.companyInterviewArray[[sender tag]]];
}

- (IBAction)onBtnTotalCandidatesClick:(id)sender {
    if([self.btnTotalCandidates.titleLabel.text integerValue] > 0){
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [self.eventModel getInterviewRegisteredCandidate:[self.detailInterview.interviewCode stringValue]
                                                 agentID:[CachingService sharedInstance].currentUserId
                                                 success:^(id responseObject) {
                                                     [self showInterviewRegCandAndAttandPopoverCtrl:self.view.bounds
                                                                                             inView:self.view
                                                                                        interviewId:self.detailInterview.objectID
                                                                                      callBackBlock:nil];
                                                     [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                 } failure:^(id responseObject, NSError *error) {
                                                     [self showInterviewRegCandAndAttandPopoverCtrl:self.view.bounds
                                                                                             inView:self.view
                                                                                        interviewId:self.detailInterview.objectID
                                                                                      callBackBlock:nil];
                                                     [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                 }];
    }
}

- (IBAction)onBtn2ndInterviewClick:(id)sender {
    if(![self.btn2ndInterview isSelected])
        [self clearAllData];
    self.interviewCompStepHeightConstraint.constant = 140;
    [self.btn2ndInterview setSelected:TRUE];
    [self.btn3rdInterview setSelected:FALSE];
    
    [self loadInterviewData:@"2nd"];
}

- (IBAction)onBtn3rdInterview:(id)sender {
    if(![self.btn3rdInterview isSelected])
        [self clearAllData];
    self.interviewCompStepHeightConstraint.constant = 0;
    [self.btn2ndInterview setSelected:FALSE];
    [self.btn3rdInterview setSelected:TRUE];
    
    [self loadInterviewData:@"3rd"];
}

- (IBAction)onBtnRegisterClick:(id)sender {
    [self apiCall:self.detailInterview];
}

- (IBAction)onBtnViewResultClick:(id)sender {
    [self showResultViewPopoverCtrl:self.contact
                          interview:self.detailInterview
                               rect:self.view.frame
                             inView:self.view];
}

- (void)loadInterviewData:(NSString *)interviewType{
    
    self.companyInterviewArray = [DbUtils fetchAllObject:@"TblInterview"
                                              andPredict:[NSPredicate predicateWithFormat:@"calendarEvent.agentCode == %@ AND interviewType = %@",[CachingService sharedInstance].currentUserId, interviewType]
                                       andSortDescriptor:[NSSortDescriptor sortDescriptorWithKey:@"interviewDate" ascending:YES]
                                    managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    [self.tblCompanyInterview reloadData];
    if(self.companyInterviewArray.count > 0)
        [self loadInterviewDetail:self.companyInterviewArray[0]];
}

- (void)loadInterviewDetail:(TblInterview *)interview{
    self.detailInterview = interview;
    [self getTotalRegCandidateCountApi:interview];
    self.lblInterviewName.text = interview.interviewSessionName;
    self.lblInterviewType.text = interview.interviewType;
    self.lblDate.text = [NSDate dateToString:interview.interviewDate andFormate:kDisplayDateFormatte];
    self.lblStartTime.text = [[NSDate stringDateToString:interview.startTime
                                      currentDateForamte:kServerDateFormatte
                                      displayDateFormate:@"HH:mma"] lowercaseString];
    self.lblEndTime.text = [[NSDate stringDateToString:interview.endTime
                                    currentDateForamte:kServerDateFormatte
                                    displayDateFormate:@"HH:mma"] lowercaseString];
    self.txtLocation.text = interview.location;
    self.txtInterviewMaterial.text = interview.interviewMaterial;
    self.txtLocation.textColor = self.txtInterviewMaterial.textColor = [UIColor colorWithRed:99.0/255.0 green:99.0/255.0 blue:99.0/255.0 alpha:1.0];
    self.txtLocation.font = self.txtInterviewMaterial.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
    
    self.quata = [interview.estimatedCondidates integerValue] - [interview.registeredCount integerValue];
    self.lblQuotaBalance.text = [NSString stringWithFormat:@"%d",self.quata];
    
    
    if(isEmptyString(interview.attachmentPath)){
        self.attachHeightConstraint.constant = 0;
    }else{
        [self.btnAttachment setAttributedTitle:[Utility setUnderline:[[interview.attachmentPath componentsSeparatedByString:@"/"] lastObject]
                                                       withTextColor:self.btnAttachment.currentTitleColor]
                                      forState:UIControlStateNormal];
        self.attachHeightConstraint.constant = 36;
    }
    
    NSDate *startDate = [NSDate stringToDate:interview.startTime dateFormat:kServerDateFormatte];
    NSDate *eventDate = [NSDate dateWithYear:interview.interviewDate.year month:interview.interviewDate.month day:interview.interviewDate.day hour:startDate.hour min:startDate.minute];
    
    if([eventDate isEarlierThanDate:[NSDate date]]){
        self.btnRegister.hidden = TRUE;
        self.btnViewResult.hidden = FALSE;
    }else{
        self.btnRegister.hidden = FALSE;
        self.btnViewResult.hidden = TRUE;
        if(interview.isRegisteredValue)
            [self.btnRegister setTitle:NSLocalizedString(@"Cancel",@"") forState:UIControlStateNormal];
        else
            [self.btnRegister setTitle:NSLocalizedString(@"Registration",@"") forState:UIControlStateNormal];
    }
}

- (void)getTotalRegCandidateCountApi:(TblInterview *)interview{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:TRUE];
    [self.eventModel getInterviewRegisteredCandidateCount:[interview.interviewCode stringValue] agentID:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        for (NSArray *response in responseObject) {
            interview.registeredCount = [NSNumber numberWithInt:[ValueParser parseInt:[response valueForKey:@"registeredCount"]]];
            [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
            [self.btnTotalCandidates setAttributedTitle:[Utility setUnderline:[interview.registeredCount stringValue]
                                                                withTextColor:self.btnTotalCandidates.currentTitleColor]
                                               forState:UIControlStateNormal];
        }
    } failure:^(id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        [self.btnTotalCandidates setAttributedTitle:[Utility setUnderline:[interview.registeredCount stringValue]
                                                            withTextColor:self.btnTotalCandidates.currentTitleColor]
                                           forState:UIControlStateNormal];
    }];
}

- (void)clearAllData{
    self.lblDate.text = self.lblStartTime.text = self.lblEndTime.text = self.lblQuotaBalance.text = @"";
    self.txtLocation.text = self.txtInterviewMaterial.text = @"";
    [self.btnTotalCandidates setTitle:@"" forState:UIControlStateNormal];
    self.selectedIndex = 0;
    
    
}

- (void)apiCall:(TblInterview *)interview{
    if([self.contact.addressCode isEqual:@0]){
        [DisplayAlert showAlertInControllerInView:self
                                            title:NSLocalizedString(@"Error", @"")
                                          message:NSLocalizedString(@"Please sync contact", @"")
                                cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }
    __block NSString *msg = nil;
    if(self.quata == 0)
        msg = NSLocalizedString(@"Don't available quota", @"");
    else if([interview.interviewType isEqualToString:@"2nd"]){
        if(![self.contact.candidateProcess.firstInterviewResult isEqualToString:FIRST_INTERVIEW_PASS]){
            msg = NSLocalizedString(@"First interview don't completed.", @"");
        }else if(![ApplicationFunction isCompletePersonalInfo:self.contact]){
            msg = NSLocalizedString(@"Personal info don't completed.", @"");
        }else if(![ApplicationFunction isCompleteEducation:self.contact]){
            msg = NSLocalizedString(@"Education info don't completed.", @"");
        }else if(![ApplicationFunction isCompleteSignature:self.contact]){
            msg = NSLocalizedString(@"ESignature don't completed.", @"");
        }
    }else if([interview.interviewType isEqualToString:@"3rd"]){
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [[ContactEventModel sharedInstance] getCandidateInterviewStatus:@"2nd"
                                                       forCandidateCode:[self.contact.addressCode stringValue]
                                                                success:^(id responseObject) {
                                                                    [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                                    for (NSArray *resArray in responseObject) {
                                                                        NSString *response = [resArray valueForKey:@"interviewStatus"];
                                                                        NSLog(@"interviewStatus:%@",response);
                                                                        if([response isEqualToString:@"No Status"] || [response isEqualToString:@"FAIL"]){
                                                                            [DisplayAlert showAlertInControllerInView:self
                                                                                                                title:@""
                                                                                                              message:NSLocalizedString(@"2nd interview don't completed.", @"")
                                                                                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                                                                        okButtonTitle:nil
                                                                                                          cancelBlock:nil
                                                                                                              okBlock:nil];
                                                                            return;
                                                                        }else
                                                                            [self regCanApiCall:interview];
                                                                    }
                                                                } failure:^(id responseObject, NSError *error) {
                                                                    [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                                }];
    }
    
    if(!isEmptyString(msg)){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:msg
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }else if([interview.interviewType isEqualToString:@"2nd"])
        [self regCanApiCall:interview];
}

- (void)regCanApiCall:(TblInterview *)interview{
    if(interview.isRegisteredValue){
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [[ContactEventModel sharedInstance] deleteInterviewRegistration:[interview.interviewCode stringValue]
                                                          candidateCode:[self.contact.addressCode stringValue]
                                                                success:^(id responseObject) {
                                                                    for (NSArray *response in responseObject) {
                                                                        if([[response valueForKey:@"status"] boolValue])
                                                                            [self updateStatus:interview isRegister:FALSE];
                                                                        
                                                                        
                                                                        AFHTTPRequestOperationManager *ASAPIManager=[[AFHTTPRequestOperationManager alloc]init];
                                                                        AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
                                                                        NSString *sessionId=delegate.sessionid;
                                                                        if (interview.registeredCount ==0){
                                                                            // web service call if the session id exits
                                                                            if (sessionId== nil || [sessionId isEqualToString:@""]) {
                                                                            } else {
                                                                                // creation of json dictionary with parameters
                                                                                NSDictionary *ob1 = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@-%@",interview.interviewCode,[CachingService sharedInstance].currentUserId],@"referenceId",
                                                                                                     [CachingService sharedInstance].branchCode,@"co",
                                                                                                     [CachingService sharedInstance].currentUserIdWithZero,@"agentcode",
                                                                                                     nil];
                                                                                NSArray *arr = [[NSArray alloc]initWithObjects:ob1,nil];
                                                                                
                                                                                //from 03 is for erecruitment , version is static 2.0.1 & option D is for delete and arr is the dictionary of events.
                                                                                NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:kCMSVersionConstant,@"version",@"D",@"option",@"03",@"from",arr,@"events",nil];
                                                                                
                                                                                NSDictionary *postDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:dict,@"RequestMessage",nil];
                                                                                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDictionary options:NSJSONWritingPrettyPrinted error:nil];
                                                                                NSString *jsonString;
                                                                                //convert the dictionary to json
                                                                                if (! jsonData) {
                                                                                } else {
                                                                                    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                                                                }
                                                                                jsonString=[jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                                                                                jsonString=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                                                                                NSString *encodedStr=[delegate md5StringForString:jsonString];
                                                                                [ASAPIManager.securityPolicy setAllowInvalidCertificates:YES];
                                                                                [ASAPIManager.requestSerializer setValue:@"multipart/form-data;charset=UTF-8;boundary=--AIACalendar--" forHTTPHeaderField:@"Content-Type"];
                                                                                [ASAPIManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html" ,nil]];
                                                                                
                                                                                [ASAPIManager POST:KErecruitmentEnhancementURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                                                    [formData appendPartWithFormData:[sessionId dataUsingEncoding:NSUTF8StringEncoding] name:@"SessionId"];
                                                                                    [formData appendPartWithFormData:[@"UTF-8" dataUsingEncoding:NSUTF8StringEncoding] name:@"Encoding"];
                                                                                    [formData appendPartWithFormData:[encodedStr dataUsingEncoding:NSUTF8StringEncoding] name:@"Signature"];
                                                                                    [formData appendPartWithFormData:[@"OneCalendarEventService" dataUsingEncoding:NSUTF8StringEncoding] name:@"Service"];
                                                                                    [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding] name:@"Data"];
                                                                                    
                                                                                } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                                    NSDictionary *Resposedict=[responseObject valueForKey:@"ResponseMessage"];
                                                                                    BOOL exists = [Resposedict objectForKey:@"returnMessage"] != nil;
                                                                                    if (!exists) {
                                                                                        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                                                                                        [alert show];
                                                                                    } else {
                                                                                        //delete
                                                                                        NSString *str=[Resposedict valueForKey:@"returnMessage"];
                                                                                        if ([str isEqualToString:@"Successful!"]) {
                                                                                            NSString *strRefid = [NSString stringWithFormat:@"%d-%@",[interview.interviewCode intValue],[CachingService sharedInstance].currentUserId];
                                                                                            //deleting frfom local db
                                                                                            [[CalendarEventModel sharedInstance] deleteICalendarEvent:strRefid AgentCode:[CachingService sharedInstance].currentUserIdWithZero];
                                                                                        }
                                                                                    }
                                                                                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                                                                                    [alert show];
                                                                                }];
                                                                            }
                                                                        }
                                                                    }
                                                                    [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                                } failure:^(id responseObject, NSError *error) {
                                                                    [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                                }];
    }else{
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        if (!self.contact.gender) {
            self.contact.gender = @"";
        }
        
        //        NSLog(@"candidateCode %@", self.contact.addressCode);
        //        NSLog(@"interviewCode %@", interview.interviewCode);
        //        NSLog(@"candidateName %@",self.contact.name);
        //        NSLog(@"servicingAgent %@", [CachingService sharedInstance].currentUserId);
        //        NSLog(@"sourceOfReferal %@", self.contact.referalSource);
        //        NSLog(@"age %@", self.contact.age);
        //        NSLog(@"dob %@",[DateUtils dateToString:self.contact.birthDate andFormate:kDisplayDateFormatte]);
        //        NSLog(@"gende r%@", self.contact.gender);
        //        NSLog(@"contactNumber %@", self.contact.mobilePhoneNo);
        //        NSLog(@"ccTestResult %@", self.contact.candidateProcess.ccTestStatus ? self.contact.candidateProcess.ccTestStatus : @"");
        //        NSLog(@"remarks %@", );
        
        NSDictionary *bodyDictionary = @{@"candidateCode" : self.contact.addressCode,
                                         @"interviewCode" : interview.interviewCode,
                                         @"candidateName" : self.contact.name,
                                         @"servicingAgent" : [CachingService sharedInstance].currentUserId,
                                         @"sourceOfReferal" : self.contact.referalSource,
                                         @"age" : self.contact.age,
                                         @"dob" : [DateUtils dateToString:self.contact.birthDate andFormate:kDisplayDateFormatte],
                                         @"dobStr" : @"",
                                         @"gender" : self.contact.gender,
                                         @"contactNumber" : self.contact.mobilePhoneNo,
                                         @"ccTestResult" : self.contact.candidateProcess.ccTestStatus ? self.contact.candidateProcess.ccTestStatus : @"",
                                         @"recruitmentScheme" : @"Y",
                                         @"interviewResult" : @"",
                                         @"remarks" : [ValueParser parseString:self.contact.remarks],
                                         @"statusStr" : @"true",
                                         @"token" : @""
                                         };
        
        [[ContactEventModel sharedInstance] postInterviewCandidateRegister:bodyDictionary
                                                                   agentId:[CachingService sharedInstance].currentUserId
                                                                   success:^(id responseObject) {
                                                                       for (NSArray *response in responseObject) {
                                                                           BOOL status = [[response valueForKey:@"status"] boolValue];
                                                                           BOOL duplicate = [[response valueForKey:@"isDuplicate"] boolValue];
                                                                           int registeredCount = [[response valueForKey:@"registeredCount"]intValue];
                                                                           if (registeredCount==1 && status && !duplicate) {
                                                                               //call Add web service
                                                                               NSString *strMainTime = [DateUtils dateToString:interview.interviewDate andFormate:kServerDateFormatte];
                                                                               NSString *subMainTime = [strMainTime substringToIndex:10];
                                                                               
                                                                               NSString *strStartTime = interview.startTime;
                                                                               NSString *subStartTime = [strStartTime substringFromIndex:11];
                                                                               subStartTime = [subStartTime stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                                                               
                                                                               NSString *strEndTime = interview.endTime;
                                                                               NSString *subEndTime =  [strEndTime substringFromIndex:11];
                                                                               subEndTime = [subEndTime stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                                                               
                                                                               strStartTime = [NSString stringWithFormat:@"%@##%@",subMainTime,subStartTime];
                                                                               strEndTime = [NSString stringWithFormat:@"%@##%@",subMainTime,subEndTime];
                                                                               
                                                                               NSDictionary *ob1 = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@-%@",interview.interviewCode,[CachingService sharedInstance].currentUserId],@"referenceId",
                                                                                                    [CachingService sharedInstance].branchCode,@"co",
                                                                                                    [CachingService sharedInstance].currentUserIdWithZero,@"agentcode",
                                                                                                    @"09",@"typeId",
                                                                                                    NSLocalizedString(@"EOP", @""),@"typeName",
                                                                                                    interview.interviewSessionName,@"subject",
                                                                                                    @"",@"description" ,
                                                                                                    interview.location,@"address",
                                                                                                    strStartTime,@"startTime",
                                                                                                    strEndTime,@"endTime",
                                                                                                    @"N",@"openFlag",nil];
                                                                               NSArray *arr = [[NSArray alloc]initWithObjects:ob1,nil];
                                                                               NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:kCMSVersionConstant,@"version",@"A",@"option",@"03",@"from",arr,@"events",nil];
                                                                               NSDictionary *postDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:dict,@"RequestMessage",nil];
                                                                               NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDictionary options:NSJSONWritingPrettyPrinted error:nil];
                                                                               NSString *jsonString;
                                                                               if (! jsonData) {
                                                                               } else {
                                                                                   jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                                                               }
                                                                               jsonString=[jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                                                                               jsonString=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                                                                               jsonString=[jsonString stringByReplacingOccurrencesOfString:@"##" withString:@" "];
                                                                               
                                                                               AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
                                                                               NSString *encodedStr=[delegate md5StringForString:jsonString];
                                                                               
                                                                               NSString *sessionId=delegate.sessionid;
                                                                               if (sessionId == nil || [sessionId isEqualToString:@""]) {
                                                                               } else {
                                                                                   AFHTTPRequestOperationManager *ASAPIManager=[[AFHTTPRequestOperationManager alloc]init];
                                                                                   [ASAPIManager.securityPolicy setAllowInvalidCertificates:YES];
                                                                                   [ASAPIManager.requestSerializer setValue:@"multipart/form-data;charset=UTF-8;boundary=--AIACalendar--" forHTTPHeaderField:@"Content-Type"];
                                                                                   [ASAPIManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html" ,nil]];
                                                                                   [ASAPIManager POST:@"https://211.144.219.243/AIATouchBE/IService.do" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                                                       [formData appendPartWithFormData:[sessionId dataUsingEncoding:NSUTF8StringEncoding] name:@"SessionId"];
                                                                                       [formData appendPartWithFormData:[@"UTF-8" dataUsingEncoding:NSUTF8StringEncoding] name:@"Encoding"];
                                                                                       [formData appendPartWithFormData:[encodedStr dataUsingEncoding:NSUTF8StringEncoding] name:@"Signature"];
                                                                                       [formData appendPartWithFormData:[@"OneCalendarEventService" dataUsingEncoding:NSUTF8StringEncoding] name:@"Service"];
                                                                                       [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding] name:@"Data"];
                                                                                       
                                                                                   } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                                       NSDictionary *Resposedict=[responseObject valueForKey:@"ResponseMessage"];
                                                                                       BOOL exists = [Resposedict objectForKey:@"returnMessage"] != nil;
                                                                                       if (!exists) {
                                                                                           UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                                                                                           [alert show];
                                                                                       }
                                                                                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                       
                                                                                       UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                                                                                       [alert show];
                                                                                       
                                                                                   }];
                                                                               }
                                                                               
                                                                           }
                                                                           if (registeredCount==0 && status) {
                                                                               //call Delete Web Service
                                                                           }
                                                                           
                                                                           if(!status && !duplicate){
                                                                               NSLog(@"Error");
                                                                           }else{
                                                                               if(duplicate){
                                                                                   [DisplayAlert showAlertInControllerInView:self
                                                                                                                       title:NSLocalizedString(@"Registration",@"")
                                                                                                                     message:NSLocalizedString(@"Already registration",@"")
                                                                                                           cancelButtonTitle:NSLocalizedString(@"OK",@"")
                                                                                                               okButtonTitle:nil
                                                                                                                 cancelBlock:^(UIAlertAction *action) {
                                                                                                                     [self showGAMAPopup];
                                                                                                                 } okBlock:nil];
                                                                               }else{
                                                                                   [self createNoteWithTblContact:self.contact
                                                                                                         noteDate:[NSDate date]
                                                                                                         noteDesc:NSLocalizedString(@"Registered for Interview successfully", @"")];
                                                                                   [self updateStatus:interview isRegister:TRUE];
                                                                                   [self showGAMAPopup];
                                                                               }
                                                                           }
                                                                       }
                                                                       [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                                   } failure:^(id responseObject, NSError *error) {
                                                                       [self updateStatus:interview isRegister:TRUE];
                                                                       [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                                   }];
    }
}
- (void)showGAMAPopup{
    //                                                                                 [self showCompanyIntSuccessPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
    //                                                                                                                 rect:self.view.bounds
    //                                                                                                               inView:self.view];
    
    [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                              candidateName:self.contact.name
                                       desc:NSLocalizedString(@"has registered for interview! Please remember to bring all interview materials, go for it!", @"")
                                       rect:self.view.bounds
                                     inView:self.view];
    
}

- (void)updateStatus:(TblInterview *)interview isRegister:(BOOL)isRegister{
    interview.isRegisteredValue = isRegister;
    if(isRegister)
        interview.registeredCount = [NSNumber numberWithInteger:[interview.registeredCount integerValue] + 1];
    else
        interview.registeredCount = [NSNumber numberWithInteger:[interview.registeredCount integerValue] - 1];
    
    [[ContactDataProvider sharedContactDataProvider]saveLocalDBData];
    [self.tblCompanyInterview reloadData];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self updateCandidateProcess];
}

- (void)updateCandidateProcess{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    [self.eventModel getCandidateInterviewStatus:@"3rd"
                                forCandidateCode:[self.contact.addressCode stringValue]
                                         success:^(id responseObject) {
                                             [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                             for (NSArray *resArray in responseObject) {
                                                 NSString *response = [resArray valueForKey:@"interviewStatus"];
                                                 [ApplicationFunction setUpdateUserProcess:RECURITMENT_COMPANY_INTERVIEW
                                                                                   contact:self.contact
                                                                             processStatus:([response isEqualToString:@"PASS"]) ? RECURITMENT_STATUS_TRUE : RECURITMENT_STATUS_FALSE
                                                                               processDate:[NSDate date]];
                                                 if(self.callBackBlock){
                                                     if ([response isEqualToString:@"PASS"])
                                                         self.callBackBlock(YES);
                                                     else
                                                         self.callBackBlock(NO);
                                                 }
                                             }
                                         } failure:^(id responseObject, NSError *error) {
                                             [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                             if(self.callBackBlock)
                                                 self.callBackBlock(NO);
                                             
                                         }];
}

- (IBAction)onBtnCandidateFormClick:(id)sender{
    if(self.candidateApplicationBlock)
        self.candidateApplicationBlock();
}

- (IBAction)onBtnAttachmentClick:(id)sender {
    if(isEmptyString(self.detailInterview.attachmentPath))
        return;
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    
    NSString *baseURL = [kCMSBaseURLString stringByAppendingString:kAttachmentDownloadURL];
    NSString *urlString = [baseURL stringByAppendingString:[self.detailInterview.interviewCode stringValue]];
    
    PdfDownloadObject *pdfDownloadObj = [[PdfDownloadObject alloc]init];
    
    [pdfDownloadObj downloadFile:urlString finished:^(id response) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        [self showWebView];
        if([response isKindOfClass:[NSData class]])
        {
            UIWindow * window = [UIApplication sharedApplication].windows.lastObject;
            NSString *ext = [self.detailInterview.attachmentPath pathExtension];
            if (ext && [ext caseInsensitiveCompare:@"jpg"] == NSOrderedSame)
            {
                [self.webView loadData:response MIMEType:@"application/jpg" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if (ext && [ext caseInsensitiveCompare:@"png"] == NSOrderedSame)
            {
                [self.webView loadData:response MIMEType:@"application/png" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if (ext && [ext caseInsensitiveCompare:@"pdf"] == NSOrderedSame)
            {
                [self.webView loadData:response MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([self.detailInterview.attachmentPath hasSuffix:@"xls"])
            {
                [self.webView loadData:response MIMEType:@"application/xls" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
        }
    } failure:^(NSError *error) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:@"暂时无法下载文档，请稍后再尝试"
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
    }];
}


- (void)getInteviewStatus
{
    [self.eventModel getCandidateInterviewStatus:@"2nd" forCandidateCode:@"24" success:^(id response) {
        
    }  failure:^(id response,NSError *error) {
        
    }];
}
- (IBAction)btnLocationClick:(id)sender {
    
    NSString *address = self.txtLocation.text;
    [self getSearchLocation:address];
}


@end
