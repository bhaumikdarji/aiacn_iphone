//
//  CCTestViewController.m
//  AIAChina
//
//  Created by AIA on 06/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CCTestViewController.h"
#import "ContactDataProvider.h"

@interface CCTestViewController ()

@end

@implementation CCTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"CC Test Result", @"");
    self.oldValue = @"";
    if(self.contact.candidateProcess){
        if([self.contact.candidateProcess.ccTestRecruitment isEqualToString:URGENT_RECRUIT]){
            [self.btnRecruit[0] setSelected:TRUE];
            self.oldValue = URGENT_RECRUIT;
        }else if([self.contact.candidateProcess.ccTestRecruitment isEqualToString:NORMAL_RECRUIT]){
            [self.btnRecruit[1] setSelected:TRUE];
            self.oldValue = NORMAL_RECRUIT;
        }else if([self.contact.candidateProcess.ccTestRecruitment isEqualToString:CAUTION_RECRUIT]){
            [self.btnRecruit[2] setSelected:TRUE];
            self.oldValue = NORMAL_RECRUIT;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnAssessmentResultClick:(id)sender {
    [ApplicationFunction openSafariBrowser:@"https://www.exselhub.com/"];
}

- (IBAction)onBtnRecruitClick:(id)sender {
    for (UIButton *btnrecruitchoice in self.btnRecruit)
        [btnrecruitchoice setSelected:FALSE];
    
    [(UIButton *)sender setSelected:TRUE];
}

- (IBAction)onBtnSubmitClick:(id)sender {
    TblCandidateProcess *candidateProcess = self.contact.candidateProcess;
    if(!candidateProcess){
        candidateProcess = [[ContactDataProvider sharedContactDataProvider] createCandidateProcess];
        candidateProcess.contact = self.contact;
    }
    
    NSString *selectedRecruit = @"";
    if([self.btnRecruit[0] isSelected])
        candidateProcess.ccTestRecruitment = selectedRecruit = URGENT_RECRUIT;
    else if([self.btnRecruit[1] isSelected])
        candidateProcess.ccTestRecruitment = selectedRecruit = NORMAL_RECRUIT;
    else if([self.btnRecruit[2] isSelected])
        candidateProcess.ccTestRecruitment = selectedRecruit = CAUTION_RECRUIT;
    
    if(![self.oldValue isEqualToString:selectedRecruit]){
        candidateProcess.ccTestDate = [NSDate date];
    }

//    if(![self.oldValue isEqualToString:selectedRecruit])
//        [self createNoteWithTblContact:self.contact
//                              noteDate:candidateProcess.ccTestDate
//                              noteDesc:NSLocalizedString(@"Completed CC Test", @"")];
    
    candidateProcess.ccTestStatus = ([selectedRecruit isEqualToString:CAUTION_RECRUIT]) ? RECURITMENT_STATUS_FALSE : RECURITMENT_STATUS_TRUE;
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];

    [self dismissViewControllerAnimated:YES completion:nil];
    if(self.callBackBlock)
        self.callBackBlock();

}

- (IBAction)onBtnCancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
