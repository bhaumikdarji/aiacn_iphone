//
//  AddressPickerViewController.h
//  AIAChina
//
//  Created by MacMini on 15/10/15.
//  Copyright © 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressPickerViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UIPickerView *addressPicker1;
@property (weak, nonatomic) IBOutlet UIPickerView *addressPicker2;
@property (weak, nonatomic) IBOutlet UIPickerView *addressPicker3;

@property (strong, nonatomic) TblAddress *address;
@property (strong, nonatomic) NSArray *rgCodeAArray;
@property (strong, nonatomic) NSArray *rgCodeBArray;
@property (strong, nonatomic) NSArray *rgCodeCArray;
@property (nonatomic) int selectedIndexA;
@property (nonatomic) int selectedIndexB;
@property (nonatomic) int selectedIndexC;
@property (copy, nonatomic) void(^callBackBlock)(TblAddress *);

- (IBAction)onBtnCancelClick:(id)sender;
- (IBAction)onBtnDoneClick:(id)sender;

@end
