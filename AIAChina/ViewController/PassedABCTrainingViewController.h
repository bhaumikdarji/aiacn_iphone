//
//  PassedABCTrainingViewController.h
//  AIAChina
//
//  Created by MacMini on 07/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PassedABCTrainingViewController : AIAChinaViewController

@property (copy, nonatomic) void(^callBackBlock)();

@end
