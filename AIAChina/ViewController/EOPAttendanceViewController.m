//
//  EOPAttendanceViewController.m
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EOPAttendanceViewController.h"
#import "EOPAttendanceCell.h"
#import "ContactEventModel.h"
#import "PdfDownloadObject.h"

@interface EOPAttendanceViewController ()
@property (nonatomic, retain) ContactEventModel *eventModel;
@property (nonatomic, assign) BOOL isAttended;
@end

@implementation EOPAttendanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"EOP Attendance", @"");
    self.eventModel = [ContactEventModel sharedInstance];
    [self loadData:self.eopRegisteredCandidateArray[0]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.eopRegisteredCandidateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EOPAttendanceCell *cell = (EOPAttendanceCell *)[tableView dequeueReusableCellWithIdentifier:@"EOPAttendanceCell"];
    NSDictionary *dic = self.eopRegisteredCandidateArray[indexPath.row];
    [cell loadAttendanceData:dic];
    NSString *isAttended = [dic objectForKey:@"attendedStatus"];
    NSString *attendedStatus;
    if ([isAttended isEqualToString: @"Y"]) {
        self.isAttended = YES;
    }
    cell.btnEventdate.tag = indexPath.row;
    [cell.btnEventdate addTarget:self action:@selector(onBtnEventDateClick:) forControlEvents:UIControlEventTouchUpInside];
    return  cell;
}

- (IBAction)onBtnEventDateClick:(UIButton *)sender{
    [self loadData:self.eopRegisteredCandidateArray[[sender tag]]];
}

- (IBAction)onBtnTopicClick:(id)sender {
}

- (IBAction)onBtnEventMaterialsClick:(id)sender {
    
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    PdfDownloadObject *pdfDownloadObj = [[PdfDownloadObject alloc]init];
    [pdfDownloadObj downloadFile:self.eventCode afterMaterial:NO finished:^(id response) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        [self showWebView];
        if([response isKindOfClass:[NSData class]])
        {
            UIWindow * window = [UIApplication sharedApplication].windows.lastObject;
            if ([[self.currentEOPDic valueForKey:@"topicPath"] hasSuffix:@"jpg"])
            {
                [self.webView loadData:response MIMEType:@"application/jpg" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([[self.currentEOPDic valueForKey:@"topicPath"] hasSuffix:@"png"])
            {
                [self.webView loadData:response MIMEType:@"application/png" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([[self.currentEOPDic valueForKey:@"topicPath"] hasSuffix:@"pdf"])
            {
                [self.webView loadData:response MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([[self.currentEOPDic valueForKey:@"topicPath"] hasSuffix:@"xls"])
            {
                [self.webView loadData:response MIMEType:@"application/xls" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
        }
    } failure:^(NSError *error) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:@"暂时无法下载文档，请稍后再尝试"
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
    }];
}

- (IBAction)onBtnAfterEventMaterialsClick:(id)sender {
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    PdfDownloadObject *pdfDownloadObj = [[PdfDownloadObject alloc]init];
    [pdfDownloadObj downloadFile:self.eventCode afterMaterial:YES finished:^(id response) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        [self showWebView];
        if([response isKindOfClass:[NSData class]])
        {
            UIWindow * window = [UIApplication sharedApplication].windows.lastObject;
            
            if ([[self.currentEOPDic valueForKey:@"profilePath"] hasSuffix:@"jpg"])
            {
                [self.webView loadData:response MIMEType:@"application/jpg" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([[self.currentEOPDic valueForKey:@"profilePath"] hasSuffix:@"png"])
            {
                [self.webView loadData:response MIMEType:@"application/png" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([[self.currentEOPDic valueForKey:@"profilePath"] hasSuffix:@"pdf"])
            {
                [self.webView loadData:response MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([[self.currentEOPDic valueForKey:@"profilePath"] hasSuffix:@"xls"])
            {
                [self.webView loadData:response MIMEType:@"application/xls" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
        }
    } failure:^(NSError *error) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:@"暂时无法下载文档，请稍后再尝试"
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
    }];
}

- (IBAction)onBtnTotalCandidateClick:(id)sender {
    if([self.btnTotalCandidate.titleLabel.text integerValue] > 0){
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [self.eventModel getEOPRegisteredCandidate:self.eventCode agentId:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
            [self showEOPRegCandAndAttandPopoverCtrl:self.view.bounds
                                              inView:self.view
                                           eventCode:[self.eventCode integerValue]
                                       callBackBlock:NULL];
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        } failure:^(id responseObject, NSError *error) {
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
            [self showEOPRegCandAndAttandPopoverCtrl:self.view.bounds
                                              inView:self.view
                                           eventCode:[self.eventCode integerValue]
                                       callBackBlock:NULL];
        }];
    }
}

- (void)loadData:(NSDictionary *)candidateDic{
    self.currentEOPDic = candidateDic;
    self.eventCode = [NSString stringWithFormat:@"%d",[ValueParser parseInt:[candidateDic valueForKey:@"event_code"]]];
    self.lblDate.text = [NSDate stringDateToString:[candidateDic valueForKey:@"eventDate"]
                                currentDateForamte:kServerDateFormatte
                                displayDateFormate:kDisplayDateFormatte];
    self.lblStartTime.text = [NSDate stringDateToString:[candidateDic valueForKey:@"startTime"]
                                     currentDateForamte:kServerDateFormatte
                                     displayDateFormate:kTimeDateFormatte];
    self.lblEndTime.text = [NSDate stringDateToString:[candidateDic valueForKey:@"endTime"]
                                   currentDateForamte:kServerDateFormatte
                                   displayDateFormate:kTimeDateFormatte];
    self.txtLocation.text = [candidateDic valueForKey:@"location"];
    self.lblSpeaker.text = [candidateDic valueForKey:@"speaker"];
    self.lblOrganizerValue.text = [candidateDic valueForKey:@"oraganiserStr"];
    self.txtDescription.text = [candidateDic valueForKey:@"eopDescription"];
    self.txtLocation.textColor = self.txtDescription.textColor = [UIColor colorWithRed:99.0/255.0 green:99.0/255.0 blue:99.0/255.0 alpha:1.0];
    self.txtLocation.font = self.txtDescription.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
    [self.btnTopic setAttributedTitle:[Utility setUnderline:[candidateDic valueForKey:@"topic"]
                                              withTextColor:self.btnTopic.currentTitleColor]
                             forState:UIControlStateNormal];
    
    [self getTotalRegCandidateCountApi];
    
    

    
    
    if(isEmptyString([ValueParser parseString:[candidateDic valueForKey:@"profilePath"]]))
        self.EMHeightConstraint.constant = 0;
    else{
        [self.btnEventMaterials setAttributedTitle:[Utility setUnderline:[[[candidateDic valueForKey:@"profilePath"] componentsSeparatedByString:@"/"] lastObject]
                                                           withTextColor:self.btnEventMaterials.currentTitleColor]
                                          forState:UIControlStateNormal];
        self.EMHeightConstraint.constant = 40;
    }
    
    if(isEmptyString([ValueParser parseString:[candidateDic valueForKey:@"topicPath"]]))
        self.AEMHeightConstraint.constant = 0;
    else{
        [self.btnAfterEventMaterials setAttributedTitle:[Utility setUnderline:[[[candidateDic valueForKey:@"topicPath"] componentsSeparatedByString:@"/"] lastObject]
                                                                withTextColor:self.btnAfterEventMaterials.currentTitleColor]
                                               forState:UIControlStateNormal];
        self.AEMHeightConstraint.constant = 40;
    }
}

- (void)getTotalRegCandidateCountApi{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:TRUE];
    [self.eventModel getEOPRegisteredCandidateCount:self.eventCode agentID:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        for (NSArray *response in responseObject) {
            [self.btnTotalCandidate setAttributedTitle:[Utility setUnderline:[NSString stringWithFormat:@"%d",[ValueParser parseInt:[response valueForKey:@"registeredCount"]]]
                                                               withTextColor:self.btnTotalCandidate.currentTitleColor]
                                              forState:UIControlStateNormal];
        }
    } failure:^(id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        [self.btnTotalCandidate setAttributedTitle:[Utility setUnderline:[NSString stringWithFormat:@"%d",[ValueParser parseInt:[self.currentEOPDic valueForKey:@"registeredCount"]]]
                                                           withTextColor:self.btnTotalCandidate.currentTitleColor]
                                          forState:UIControlStateNormal];
    }];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if(self.callBackBlock)
        self.callBackBlock(self.isAttended);
}

- (IBAction)btnLocationClick:(id)sender {
    
    NSString *address = self.txtLocation.text;
    [self getSearchLocation:address];

}


@end
