//
//  DropDownViewController.h
//  AIAChina
//
//  Created by AIA on 30/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface DropDownViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UITableView *tblDropDown;

@property (strong, nonatomic) NSArray *dropDownArray;
@property (copy, nonatomic) void(^selectedValue)(int selectedRow);

@end
