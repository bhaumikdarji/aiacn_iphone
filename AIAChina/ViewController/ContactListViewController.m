//
//  ContactListViewController.m
//  AIAChina
//
//  Created by AIA on 17/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "ContactListViewController.h"
#import "ContactDetailViewController.h"
#import "ContactListCell.h"
#import "MenuListViewController.h"
#import "MenuAddViewController.h"
#import "ContactDataProvider.h"
#import "ContactEventModel.h"
#import "SyncModel.h"
#import "SearchViewController.h"
#import "CHCSVParser.h"
#import <MessageUI/MessageUI.h>
#import <LibXL/LibXL.h>
#import "ExcelExport.h"

@interface ContactListViewController ()<MFMailComposeViewControllerDelegate>
@property (nonatomic, retain) ContactEventModel *contactDataModel;
@property (nonatomic, retain) UIButton *loadMoreButton;
@property (nonatomic, assign) BOOL moreContactsAvailableOrNot;
@property (nonatomic, readwrite) int pageCount;
@property (nonatomic, retain) SyncModel *syncObj;
@property (nonatomic, strong) NSString *exportFilePath;
@end

@implementation ContactListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageCount = 0;
    self.moreContactsAvailableOrNot = YES;
    self.contactDataModel = [ContactEventModel sharedInstance];
        self.alphabetArray = [[NSMutableArray alloc] init];
    [self.alphabetArray addObject:@"#"];
    for (int i = 0; i<26; i++)
        [self.alphabetArray addObject:[NSString stringWithFormat:@"%c", i+65]];
    
    [self setTableIndexBar];
    [self reloadContactList:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadContactList:)
                                                 name:CONTACT_LIST_RELOAD_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadGroupList:)
                                                 name:GROUP_LIST_RELOAD_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(unHideTransView)
                                                 name:EDIT_CONTACT_GROUP_UNHIDE_TRANS_VIEW_NOTIFICATION
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CONTACT_LIST_RELOAD_NOTIFICATION
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:GROUP_LIST_RELOAD_NOTIFICATION
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EDIT_CONTACT_GROUP_UNHIDE_TRANS_VIEW_NOTIFICATION
                                                  object:nil];
}

- (void)reloadContactList:(NSNotification *)notifi{
    self.navigationItem.title = NSLocalizedString(@"Contact", @"");
    if(notifi)
        self.selectedContact = [notifi object];
    self.transView.hidden = TRUE;
    self.isGroupList = FALSE;
    [self loadList];
}

- (void)reloadGroupList:(NSNotification *)notifi{
    self.navigationItem.title = NSLocalizedString(@"Group", @"");
    if(notifi)
        self.selectedGroup = [notifi object];
    self.transView.hidden = TRUE;
    self.isGroupList = TRUE;
    [self loadList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadList{
    NSString *agentId = [CachingService sharedInstance].currentUserId;

    [self.contactArray removeAllObjects];
    self.contactArray = [[DbUtils fetchAllObject:self.isGroupList ? @"TblGroup" : @"TblContact"
                                      andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0", agentId]
                               andSortDescriptor:nil
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    
    [self getAlphabetArray];
    [self.tblContactList reloadData];
    [self setTableIndexBar];
    
    if(self.isAddContactGroup){
        self.isAddContactGroup = FALSE;
    }else if(self.contactArray.count > 0){
        [self performSelector:@selector(defaultSelectedCell) withObject:nil afterDelay:0.1];
    }else if(!self.isAddContactGroup){
        [self unHideTransView];
        if(self.isGroupList)
            [self performSelector:@selector(addContactGroup:) withObject:[NSNumber numberWithInteger:1] afterDelay:0.1];
        else
            [self performSelector:@selector(addContactGroup:) withObject:[NSNumber numberWithInteger:0] afterDelay:0.1];
    }
}

- (void)defaultSelectedCell{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    if(self.selectedIndexPath)
        indexPath = self.selectedIndexPath;
    [self.tblContactList selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section] animated:NO scrollPosition:UITableViewScrollPositionNone];
            [[NSNotificationCenter defaultCenter] postNotificationName: self.isGroupList ? SPLIT_MASTER_DETAIL_TO_GROUP_NOTIFICATION : SPLIT_MASTER_DETAIL_TO_PROFILE_NOTIFICATION
                                                        object:[[self getNumberOfRowsInSection:indexPath.section isOrder:TRUE] objectAtIndex:indexPath.row]];
    
}

- (void)setTableIndexBar{
    if(self.indexBar){
        [self.indexBar removeFromSuperview];
    }
    self.indexBar = [[GDIIndexBar alloc] initWithTableView:self.tblContactList];
    self.indexBar.delegate = self;
    [self.tblContactList addSubview:self.indexBar];
    [[GDIIndexBar appearance] setTextColor:[UIColor colorWithRed:212.0/255.0 green:16.0/255.0 blue:60.0/255.0 alpha:1.0]];
    [[GDIIndexBar appearance] setTextShadowColor:[UIColor clearColor]];
    [[GDIIndexBar appearance] setTextFont:[UIFont systemFontOfSize:16]];
    [[GDIIndexBar appearance] setBarBackgroundColor:[UIColor whiteColor]];
}

- (void)getAlphabetArray{
    self.contactAlphabetArray = [[NSMutableArray alloc]init];
    if(self.isGroupList){
        for (TblGroup *group in self.contactArray) {
            NSString *strFirstChar = [[group.groupName substringToIndex:1] uppercaseString];
            if([Utility isNumeric:strFirstChar]){
                if(![self.contactAlphabetArray containsObject:@"#"]){
                    [self.contactAlphabetArray addObject:@"#"];
                }
            }else if(![self.contactAlphabetArray containsObject:strFirstChar]){
                [self.contactAlphabetArray addObject:strFirstChar];
            }
        }
    }else{
        for (TblContact *contact in self.contactArray) {
            if(!isEmptyString(contact.name)){
                NSString *strFirstChar = [[contact.name substringToIndex:1] uppercaseString];
                if([Utility isNumeric:strFirstChar]){
                    if(![self.contactAlphabetArray containsObject:@"#"]){
                        [self.contactAlphabetArray addObject:@"#"];
                    }
                }else if(![self.contactAlphabetArray containsObject:strFirstChar]){
                    [self.contactAlphabetArray addObject:strFirstChar];
                }
            }
        }
    }
    [self.contactAlphabetArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (NSMutableArray *)getNumberOfRowsInSection:(NSInteger)section isOrder:(BOOL)setOrdering{
    NSMutableArray *numberOfRowArray=[[NSMutableArray alloc] init];
    if(self.isGroupList){
        if([[self.contactAlphabetArray objectAtIndex:section] isEqualToString:@"#"]){
            for (TblGroup *group in self.contactArray) {
                NSString *strFirstNameFirstChar = [[group.groupName substringToIndex:1] uppercaseString];
                if([Utility isNumeric:strFirstNameFirstChar]){
                    [numberOfRowArray addObject:group];
                }
            }
        }else{
            for (TblGroup *group in self.contactArray) {
                NSString *strFirstNameFirstChar = [[group.groupName substringToIndex:1] uppercaseString];
                if([strFirstNameFirstChar isEqualToString:[self.contactAlphabetArray objectAtIndex:section]]){
                    [numberOfRowArray addObject:group];
                }
            }
        }
    }else{
        if([[self.contactAlphabetArray objectAtIndex:section] isEqualToString:@"#"]){
            for (TblContact *contact in self.contactArray) {
                if(!isEmptyString(contact.name)){
                    NSString *strFirstNameFirstChar = [[contact.name substringToIndex:1] uppercaseString];
                    if([Utility isNumeric:strFirstNameFirstChar]){
                        [numberOfRowArray addObject:contact];
                    }
                }
            }
        }else{
            for (TblContact *contact in self.contactArray) {
                if(!isEmptyString(contact.name)){
                    NSString *strFirstNameFirstChar = [[contact.name substringToIndex:1] uppercaseString];
                    if([strFirstNameFirstChar isEqualToString:[self.contactAlphabetArray objectAtIndex:section]]){
                        [numberOfRowArray addObject:contact];
                    }
                }
            }
        }
    }
    
    if(setOrdering){
        [numberOfRowArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            if(self.isGroupList){
                TblGroup *group1 = obj1, *group2 = obj2;
                return [group1.groupName caseInsensitiveCompare:group2.groupName];
            }else{
                TblContact *contact1 = obj1, *contact2 = obj2;
                return [contact1.name caseInsensitiveCompare:contact2.name];
            }
        }];
    }
    return numberOfRowArray;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    self.selectedIndexPath = nil;
    return self.contactAlphabetArray.count;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.contactAlphabetArray objectAtIndex:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tblContactList.frame.size.width, 30)];
    headerView.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, headerView.frame.origin.y, headerView.frame.size.width, 30)];
    lblHeader.backgroundColor = [UIColor clearColor];
    lblHeader.text = [NSString stringWithFormat:@"   %@",[self.contactAlphabetArray objectAtIndex:section]];
    lblHeader.font = [UIFont boldSystemFontOfSize:12.0];
    
    lblHeader.textColor = [UIColor colorWithRed:48.0/255.0
                                          green:48.0/255.0
                                           blue:48.0/255.0
                                          alpha:1.0];
    [headerView addSubview:lblHeader];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self getNumberOfRowsInSection:section isOrder:TRUE].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactListCell *cell = nil;
    
    NSArray *tmpArray = [self getNumberOfRowsInSection:indexPath.section isOrder:TRUE];
    
    if(self.isGroupList){
        cell = (ContactListCell *)[tableView dequeueReusableCellWithIdentifier:@"GroupListCell"];
        TblGroup *group = [tmpArray objectAtIndex:indexPath.row];
        [cell loadGroupData:group];
        if([group.objectID isEqual:self.selectedGroup.objectID])
            self.selectedIndexPath = indexPath;
    }else{
        cell = (ContactListCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactListCell"];
        TblContact *contact = [tmpArray objectAtIndex:indexPath.row];
        [cell loadContactData:contact];
        if([contact.objectID isEqual:self.selectedContact.objectID])
            self.selectedIndexPath = indexPath;
    }
    
    if([self getNumberOfRowsInSection:indexPath.section isOrder:NO].count-1 == indexPath.row){
        cell.lblSeprator.hidden = TRUE;
    }else{
        cell.lblSeprator.hidden = FALSE;
    }
    
    cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedContact = [[self getNumberOfRowsInSection:indexPath.section isOrder:TRUE] objectAtIndex:indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:SPLIT_MASTER_DETAIL_TO_PROFILE_NOTIFICATION object:@{@"IsSync":RECURITMENT_STATUS_TRUE, @"object":self.selectedContact}];
}

- (void)loadMoreContacts
{
    self.pageCount += 1;
    if (self.moreContactsAvailableOrNot)
    {
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        
    }
}

- (NSUInteger)numberOfIndexesForIndexBar:(GDIIndexBar *)indexBar{
    return self.alphabetArray.count;
}

- (NSString *)stringForIndex:(NSUInteger)index{
    return [self.alphabetArray objectAtIndex:index];
}

- (void)indexBar:(GDIIndexBar *)indexBar didSelectIndex:(NSUInteger)index{
    int section = -1;
    for (NSString *contactAlphabet in self.contactAlphabetArray) {
        section++;
        if([[self.alphabetArray objectAtIndex:index] isEqualToString:contactAlphabet])
            [self.tblContactList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]
                                       atScrollPosition:UITableViewScrollPositionTop
                                               animated:NO];
    }
}

- (IBAction)onBtnMenuListClick:(id)sender {
    [self performSegueWithIdentifier:@"MenuListPopover" sender:self];
}

- (IBAction)onBtnAddContactClick:(id)sender {
    [self performSegueWithIdentifier:@"MenuAddPopover" sender:self];
}

- (IBAction)onBtnSearchClick:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:SPLIT_MASTER_DETAIL_TO_SEARCH_NOTIFICATION
                                                        object:nil];
}

- (IBAction)onBtnSyncClick:(id)sender{
    if([self isInternetConnect]){
        self.syncObj = [[SyncModel alloc]init];
        [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
        [self.syncObj syncObjectsIntoDatabase:[CachingService sharedInstance].currentUserId success:^{
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Successful", @"")
                                                            message:NSLocalizedString(@"Contacts are synced with server successfully.", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                  otherButtonTitles:nil, nil];
            [alert show];
        }failure:^{
            [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        }];
    }
}

- (IBAction)onBtnDeleteClick:(id)sender {
    [self showAssignPopoverCtrl:nil
              withSelectedArray:nil
                      isContact:!self.isGroupList
                       isDelete:TRUE
                       isExport:FALSE
              isAssigneeContact:FALSE
                           rect:[MyAppDelegate window].rootViewController.view.bounds
                         inView:[MyAppDelegate window].rootViewController.view
                selectedContact:^(NSArray *deleteArray) {
                    if(!self.isGroupList){
                        for (TblContact *contact in deleteArray) {
                            contact.isDelete = [NSNumber numberWithBool:TRUE];
                            contact.isSync = [NSNumber numberWithBool:TRUE];
                            contact.modificationDate=[NSDate date];
                        }
                    }else{
                        for (TblGroup *group in deleteArray) {
                            group.isDelete = [NSNumber numberWithBool:TRUE];
                            group.isSync = [NSNumber numberWithBool:TRUE];
                            
                            NSArray *assContact = [group.contact allObjects];
                            for (TblContact *contact in assContact) {
                                [group removeContactObject:contact];
                                if([contact.group count] == 0)
                                    [ApplicationFunction joinUngroup:contact];
                            }
                        }
                    }
                    [[ContactDataProvider sharedContactDataProvider]saveLocalDBData];
                    [self loadList];
                }];
}

- (IBAction)onBtnExportClick:(id)sender {
    [self showAssignPopoverCtrl:nil
              withSelectedArray:nil
                      isContact:!self.isGroupList
                       isDelete:FALSE
                       isExport:TRUE
              isAssigneeContact:FALSE
                           rect:[MyAppDelegate window].rootViewController.view.bounds
                         inView:[MyAppDelegate window].rootViewController.view
                selectedContact:^(NSArray *deleteArray)
     {
         if(self.isGroupList)
         {
             
         }
         else
         {
             [self exportContactsToExcel:deleteArray];
//             [self createCSVWithContacts:deleteArray];
         }
         [self dismissViewControllerAnimated:YES completion:nil];
         [self sendContactEmail];
     }];
}
- (void)createCSVWithContacts:(NSArray *)contactsArray
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString* documentsDir = [paths objectAtIndex:0];
    NSString* myFilePath = [NSString stringWithFormat:@"%@/%@", documentsDir, @"ExportContacts.csv"];
    CHCSVWriter *csvWriter = [[CHCSVWriter alloc]initForWritingToCSVFile:myFilePath];
    for (TblContact *contact in contactsArray)
    {
        NSString *birthDate,*age;
        if (contact.birthDate != nil)
        {
             birthDate = [NSString stringWithFormat:@"%@",[DateUtils dateToString:[NSDate date] andFormate:@"YYYY-MM-dd"]];
        }
        else
        {
            birthDate = @"";
        }
        if (contact.age != nil) {
            age = [contact.age stringValue];
        }
        else
        {
            age = @"";
        }
       
        [csvWriter writeLineOfFields:@[((NSString *)contact.name).length == 0? @"":contact.name,age,birthDate,((NSString *)contact.gender).length == 0 ? @"":contact.gender,((NSString *)contact.referalSource).length == 0 ? @"":contact.referalSource,((NSString *)contact.groupName).length == 0 ? @"":contact.groupName,((NSString *)contact.registeredAddress).length == 0 ? @"":contact.registeredAddress,((NSString *)contact.nric).length == 0 ? @"":contact.nric,((NSString *)contact.eMailId).length == 0 ? @"":contact.eMailId,((NSString *)contact.mobilePhoneNo).length == 0 ? @"":contact.mobilePhoneNo,((NSString *)contact.fixedLineNO).length == 0 ? @"":contact.fixedLineNO,((NSString *)contact.qq).length == 0 ? @"":contact.qq,((NSString *)contact.weChat).length == 0 ? @"":contact.weChat,((NSString *)contact.officePhoneNo).length == 0 ? @"":contact.officePhoneNo,((NSString *)contact.marritalStatus).length == 0 ? @"":contact.marritalStatus,((NSString *)contact.workingYearExperience).length == 0 ? @"":contact.workingYearExperience,((NSString *)contact.race).length == 0 ? @"":contact.race,((NSString *)contact.education).length == 0 ? @"":contact.education,((NSString *)contact.talentProfile).length == 0 ? @"":contact.talentProfile,((NSString *)contact.reJoin).length == 0 ? @"":contact.reJoin,((NSString *)contact.recommendedRecruitmentScheme).length == 0 ? @"":contact.recommendedRecruitmentScheme,((NSString *)contact.yearlyIncome).length == 0 ? @"":contact.yearlyIncome,((NSString *)contact.presentWorkCondition).length == 0 ? @"":contact.presentWorkCondition,((NSString *)contact.purchasedAnyInsurance).length == 0 ? @"":contact.purchasedAnyInsurance,((NSString *)contact.salesExperience).length == 0 ? @"":contact.salesExperience,((NSString *)contact.remarks).length == 0? @"":contact.remarks]];
        [csvWriter finishLine];
    }
}
- (void)sendContactEmail
{
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *csvPath = [docsPath stringByAppendingPathComponent:@"ExportContacts.xls"];
    NSData *data = [NSData dataWithContentsOfFile:csvPath];
    NSString *mimeType;
    
    if ([MFMailComposeViewController canSendMail] == NO) return;
    unsigned long long fileSize = data.length;
    
    if (fileSize < (unsigned long long)15728640)
    {
        if (data != nil){
            MFMailComposeViewController *mailComposer = [MFMailComposeViewController new];
            mimeType = @"application/vnd.ms-excel";
            [mailComposer addAttachmentData:data mimeType:mimeType fileName:@"ExportContacts.xls"];
            NSString *message = [NSString stringWithFormat:@"Selected Contacts Data"];
            [mailComposer setMessageBody:message isHTML:NO];
            [mailComposer setSubject:@"ExportContacts"];
            mailComposer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            mailComposer.modalPresentationStyle = UIModalPresentationFormSheet;
            mailComposer.mailComposeDelegate = self;
            [self presentViewController:mailComposer animated:YES completion:NULL];
        }
    }
}


+ (NSString*)templateExcelFileName
{
    static NSString *defaultFile = nil;
    static dispatch_once_t token2 = 0;
    dispatch_once(&token2, ^ {
        defaultFile = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Contacts_Template.xls"];
        NSLog(@"defaultFile :%@",defaultFile);
    });
    return defaultFile;
}

- (void)exportContactsToExcel:(NSArray *)contactsArray
{
    self.exportFilePath = [ExcelExport ExportContactsToFile:contactsArray];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result == MFMailComposeResultSent){
        [self dismissViewControllerAnimated:YES completion:^{
            [self dismissViewControllerAnimated:NO completion:^{
            }];
        }];
    }else if(result == MFMailComposeResultCancelled){
        [self dismissViewControllerAnimated:YES completion:^{
           
        }];
    }
    
    [[NSFileManager defaultManager]removeItemAtPath:self.exportFilePath error:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"MenuListPopover"]){
        MenuListViewController *menuList = segue.destinationViewController;
        [menuList setShowAllContact:^(int selectedList) {
            self.transView.hidden = TRUE;
            if(selectedList == 0){
                [[NSNotificationCenter defaultCenter] postNotificationName:GROUP_TO_PROFILE_NOTIFICATION
                                                                    object:nil];
                [self reloadContactList:nil];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                                                    object:nil];
                [self reloadGroupList:nil];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }else if([segue.identifier isEqualToString:@"MenuAddPopover"]){
        MenuAddViewController *menuList = segue.destinationViewController;
        [menuList setShowIndex:^(int selectedList) {
            self.isAddContactGroup = TRUE;
            if(selectedList == 0){
                [self reloadContactList:nil];
            }else{
                [self reloadGroupList:nil];
            }
            [self addContactGroup:[NSNumber numberWithInteger:selectedList]];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }
}

- (void)addContactGroup:(NSNumber *)selectedList{
    if([selectedList isEqual: @0]){
        [[NSNotificationCenter defaultCenter] postNotificationName:SPLIT_MASTER_DETAIL_TO_ADD_CONTACT_NOTIFICATION
                                                            object:nil];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                                            object:nil];
    }
    [self unHideTransView];
}

- (void)unHideTransView{
    self.transView.hidden = FALSE;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (maximumOffset - currentOffset <= -10) {
       // [self loadMoreContacts];
    }
}

@end
