//
//  GroupViewController.m
//  AIAChina
//
//  Created by AIA on 20/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "GroupViewController.h"
#import "GroupCell.h"
#import "ApplicationFunction.h"

@interface GroupViewController ()

@end

@implementation GroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Group", @"");
    self.groupDataProvider = [ContactDataProvider sharedContactDataProvider];
    self.grpContactArray = [[NSMutableArray alloc]init];
    
    [Utility setRoundCornerRedius:self.imgGroup
                     cornerRedius:self.imgGroup.frame.size.width/2];
    
    [Utility setBorderAndRoundCorner:self.profileImageView
                         borderColor:[UIColor colorWithRed:241.0/255.0
                                                     green:239.0/255.0
                                                      blue:240.0
                                                     alpha:1.0]
                               width:5.0
                        cornerRedius:72.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showGroup:)
                                                 name:SPLIT_MASTER_DETAIL_TO_GROUP_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addGroup:)
                                                 name:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addContact:)
                                                 name:SPLIT_MASTER_DETAIL_TO_ADD_CONTACT_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(groupToProfile)
                                                 name:GROUP_TO_PROFILE_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoSearch:)
                                                 name:SPLIT_MASTER_DETAIL_TO_SEARCH_NOTIFICATION
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SPLIT_MASTER_DETAIL_TO_GROUP_NOTIFICATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SPLIT_MASTER_DETAIL_TO_ADD_CONTACT_NOTIFICATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:GROUP_TO_PROFILE_NOTIFICATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SPLIT_MASTER_DETAIL_TO_SEARCH_NOTIFICATION
                                                  object:nil];
}

- (void)gotoSearch:(NSNotification *)notifi{
    [[NSNotificationCenter defaultCenter] postNotificationName:CONTACT_LIST_RELOAD_NOTIFICATION
                                                        object:nil];
    [self.navigationController popToRootViewControllerAnimated:TRUE];
    if(self.addContactBlock)
        self.addContactBlock(FALSE);
}

- (void)groupToProfile{
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)showGroup:(NSNotification *)notifi{
    if(notifi)
        self.group = [notifi object];
    [self loadData];
    self.isEditGroup = FALSE;
    self.btnCancelDelete.title = NSLocalizedString(@"Back",nil);
    if([self.group.groupName isEqualToString:UNGROUP]){
        self.btnSaveEdit.title = @"";
        self.btnSaveEdit.enabled = FALSE;
    }else{
        self.btnSaveEdit.title = NSLocalizedString(@"Edit",nil);
        self.btnSaveEdit.enabled = TRUE;
    }
    self.btnAddPhoto.hidden = TRUE;
    self.editProfileView.hidden = FALSE;
    [self setTableHeight];
}

- (void)addGroup:(NSNotification *)notifi{
    self.group = nil;
    self.imgGroup.image = nil;
    self.btnAddPhoto.hidden = FALSE;
    [self.btnAddPhoto setTitle:NSLocalizedString(@"Add Photo", @"") forState:UIControlStateNormal];
    self.txtGroupName.text = @"";
    self.lblTotalGroupMember.text = @"0";
    self.txtDescription.text = @"";
    [self.grpContactArray removeAllObjects];
    [self editGroup];
}

- (void)editGroup{
    if(!self.group.image){
        self.imgGroup.image = nil;
        [self.btnAddPhoto setTitle:NSLocalizedString(@"Add Photo", @"") forState:UIControlStateNormal];
    }else{
        [self.btnAddPhoto setTitle:NSLocalizedString(@"Edit Photo", @"") forState:UIControlStateNormal];
    }
    self.isEditGroup = TRUE;
    self.btnAddPhoto.hidden = FALSE;
    self.btnCancelDelete.title = NSLocalizedString(@"Cancel",@"");
    self.btnSaveEdit.title = NSLocalizedString(@"Save",@"");
    self.btnSaveEdit.enabled = TRUE;
    self.editProfileView.hidden = TRUE;
    [self.tblGroup reloadData];
    [self setTableHeight];
}

- (void)addContact:(NSNotification *)notifi{
    [self.navigationController popToRootViewControllerAnimated:TRUE];
    if(self.addContactBlock)
        self.addContactBlock(TRUE);
}

- (void)loadData{
    if(self.group){
        [self.btnAddPhoto setTitle:@"" forState:UIControlStateNormal];
        if(self.group.image)
            self.imgGroup.image = [UIImage imageWithData:self.group.image];
        else
            self.imgGroup.image = nil;
        self.txtGroupName.text = self.group.groupName;
        self.txtDescription.text = self.group.groupDescription;
        self.grpContactArray = [[[self.group.contact allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0",[CachingService sharedInstance].currentUserId]] mutableCopy];
        self.lblTotalGroupMember.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.grpContactArray.count];
        
        [self.tblGroup reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.grpContactArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupCell" forIndexPath:indexPath];
    TblContact *contact = self.grpContactArray[indexPath.row];
    cell.btnDelete.tag = indexPath.row;
    [cell loadGroupContact:contact];
    if (self.isEditGroup) {
        cell.btnDelete.hidden = FALSE;
        [cell.btnDelete addTarget:self action:@selector(onBtnDeleteClick:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        cell.btnDelete.hidden = TRUE;
    }
    return cell;
}

- (IBAction)onBtnAddPhotoClick:(id)sender {
    [self showPhotoActionSheet:sender withCircularCrop1:YES];
}

- (void)handleImagePicker:(UIImagePickerController *)thePicker withMediaInfo:(NSDictionary *)imageInfo
{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil) {
       UIImage *profileImage = imageInfo[UIImagePickerControllerEditedImage];
        if (!profileImage) profileImage = imageInfo[UIImagePickerControllerOriginalImage];
        profileImage = [self resizeImage:profileImage];
        self.imgGroup.image = profileImage;
        [self.btnAddPhoto setTitle:@"" forState:UIControlStateNormal];
    }
}
    
- (void) imagePickerController:(UIImagePickerController *)thePicker didFinishPickingMediaWithInfo:(NSDictionary *)imageInfo{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil) {
        UIImage *profileImage = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
        self.imgGroup.image = profileImage;
        [self.btnAddPhoto setTitle:@"" forState:UIControlStateNormal];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onBtnAddContactClick:(id)sender {
    [self loadContact];
}

- (IBAction)onBtnCancelDeleteClick:(id)sender {
    if([[sender title] isEqualToString:NSLocalizedString(@"Cancel",@"")]){
        [self showGroup:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:GROUP_LIST_RELOAD_NOTIFICATION
                                                            object:self.group];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:CONTACT_LIST_RELOAD_NOTIFICATION
                                                            object:nil];
        [self groupToProfile];
    }
}

- (IBAction)onBtnSaveEditClick:(id)sender {
    if([[sender title] isEqualToString:NSLocalizedString(@"Save", @"")]){
        if([self validate]){
            [self saveGroupLocalDatabase];
            [self showGroup:nil];
            [self.txtGroupName resignFirstResponder];
            [self.txtDescription resignFirstResponder];
            [[NSNotificationCenter defaultCenter] postNotificationName:GROUP_LIST_RELOAD_NOTIFICATION
                                                                object:self.group];
        }
    }else{
        [self editGroup];
        [[NSNotificationCenter defaultCenter] postNotificationName:EDIT_CONTACT_GROUP_UNHIDE_TRANS_VIEW_NOTIFICATION
                                                            object:nil];
    }
}

- (IBAction)onBtnDeleteClick:(id)sender{
    NSInteger index = ((UIButton *)sender).tag;
    [self.grpContactArray removeObjectAtIndex:index];
    [self.tblGroup reloadData];
}

- (void)setTableHeight{
    if (self.isEditGroup) {
        self.btnAddContact.hidden = FALSE;
        self.btnAddContactHeightConstraint.constant = 40;
    }else{
        self.btnAddContact.hidden = TRUE;
        self.btnAddContactHeightConstraint.constant = 0;
    }
}

- (void)saveGroupLocalDatabase{
    if(!self.group)
        self.group = [self.groupDataProvider createGroup];
    else{
        NSArray *assContact = [self.group.contact allObjects];
        for (TblContact *contact in assContact) {
            [self.group removeContactObject:contact];
            if([contact.group count] == 0)
                [ApplicationFunction joinUngroup:contact];
        }
    }
    
    self.group.agentId = [CachingService sharedInstance].currentUserId;
    self.group.isDelete = [NSNumber numberWithBool:FALSE];
    self.group.groupName = self.txtGroupName.text;
    self.group.groupDescription = self.txtDescription.text;
    if(self.imgGroup.image)
        self.group.image = UIImagePNGRepresentation(self.imgGroup.image);
    
    for (TblContact *contact in self.grpContactArray) {
        [self.group addContactObject:contact];
    }
    
    [ApplicationFunction removeUngroup:self.grpContactArray];
    [self.groupDataProvider saveLocalDBData];
}



- (BOOL)validate{
    NSString *msg = nil;
    if(isEmptyString(self.txtGroupName.text)){
        msg = NSLocalizedString(@"Plese enter group name", @"");
    }else if ([[self.txtGroupName.text lowercaseString] isEqualToString:[UNGROUP lowercaseString]]){
        msg = NSLocalizedString(@"Duplicate Group", @"");
    }
    
    if(!isEmptyString(msg)){
        [DisplayAlert showAlertInControllerWithTitle:@""
                                             message:msg
                                   cancelButtonTitle:nil
                                       okButtonTitle:NSLocalizedString(@"OK", @"")
                                         cancelBlock:^(UIAlertAction *action) {
                                         } okBlock:^(UIAlertAction *action) {
                                         }];
        return FALSE;
    }
    return TRUE;
}

- (void)loadContact{
    [self showAssignPopoverCtrl:nil
              withSelectedArray:self.grpContactArray
                      isContact:TRUE
                       isDelete:FALSE
                       isExport:FALSE
              isAssigneeContact:TRUE
                           rect:self.view.bounds
                         inView:self.view
                selectedContact:^(NSArray *selectedContactArray) {
                    self.grpContactArray = [selectedContactArray mutableCopy];
                    [self.tblGroup reloadData];
                }];
}

@end
