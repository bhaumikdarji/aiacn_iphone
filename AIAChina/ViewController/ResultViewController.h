//
//  ResultViewController.h
//  AIAChina
//
//  Created by MacMini on 09/10/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblSessionName;
@property (weak, nonatomic) IBOutlet UILabel *lblResult;
@property (weak, nonatomic) IBOutlet UILabel *lblViewer;
@property (weak, nonatomic) IBOutlet UILabel *lblCC;
@property (weak, nonatomic) IBOutlet UILabel *lblP100;
@property (weak, nonatomic) IBOutlet UITextView *txtRemarks;

@property (strong, nonatomic) TblInterview *interview;
@property (strong, nonatomic) TblContact *contact;

@end
