//
//  CompanyIntSuccessController.h
//  AIAChina
//
//  Created by MacMini on 28/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyIntSuccessController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgHeader;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblGAMAQuoteEnglish;
@property (weak, nonatomic) IBOutlet UILabel *lblGAMAQuoteChinese;
@property (weak, nonatomic) IBOutlet UILabel *lblGAMAAuthorEnglish;

@property (nonatomic) int candidateProgressRank;

- (IBAction)onBtnCancelClick:(id)sender;

@end
