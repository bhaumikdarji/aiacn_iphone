//
//  MenuListViewController.h
//  AIAChina
//
//  Created by AIA on 30/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface MenuListViewController : AIAChinaViewController

@property (copy, nonatomic) void(^showAllContact)(int selectedIndex);

- (IBAction)onBtnAllContactClick:(id)sender;
- (IBAction)onBtnAllGroupClick:(id)sender;

@end
