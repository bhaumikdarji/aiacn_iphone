    //
//  HomeViewController.m
//  AIAChina
//
//  Created by AIA on 16/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "HomeViewController.h"
#import "CMSServiceManager.h"
#import "CMSNetworkingConstants.h"
#import "AnnouncementServiceSynchronizer.h"
#import "Announcement.h"
#import "AnnouncementDataModel.h"
#import "AnnouncementPopOverViewController.h"
#import "AgentProfileViewController.h"
#import "FTDHomeViewController.h"
#import "ContactEventModel.h"
#import "MBProgressHUDUpd.h"
#import "TblAgentDetails.h"
#import "CachingService.h"
#import "iCarousel.h"
#import "ContactDataProvider.h"
#import "LoginViewController.h"
#import "SyncModel.h"
#import "AnnouncementViewController.h"
#import "CalendarViewController.h"
#import "DropDownManager.h"
#import "ContactNewListViewController.h"
#import "CollectionDetailViewController.h"
#import "GridCell.h"
#import "Header.h"
#import "SettingViewController.h"

#define kLastBackUpTime @"lastBackUpTime"
@interface HomeViewController ()<UIScrollViewDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;
@property (nonatomic,retain) UIButton *scrollButton;

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic,retain) CMSServiceManager *serviceManager;
@property (weak, nonatomic) IBOutlet UIButton *announcementButton;
@property (nonatomic,retain) AnnouncementDataModel *announcementDataModel;
@property (weak, nonatomic) IBOutlet iCarousel *iCarousel;
@property (weak, nonatomic) IBOutlet UIView *carouselParentView;
@property (nonatomic,retain) AnnouncementPopOverViewController *popover;
@property (nonatomic,retain) SyncModel *syncObj;
@property (nonatomic, retain) ContactDataProvider *contactDataProvider;
@property (nonatomic, retain) ContactEventModel *contactEventModel;
@property (nonatomic, readwrite) int pageNumber;
@property (nonatomic, strong) AppDelegate *appDelegate;
@end

@implementation HomeViewController
{
    
    IBOutlet NVBnbCollectionView *_collectionView;
    NSInteger _numberOfItems;
    CollectionDetailViewController *collectionDetailVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(importantAnnouncementClick:) name:@"importantAnnoucement" object:nil];
     self.contactDataProvider = [ContactDataProvider sharedContactDataProvider];
    [self setGUI];
    self.serviceManager = [[CMSServiceManager alloc]init];
    self.contactEventModel = [ContactEventModel sharedInstance];
    self.iCarousel.type = iCarouselTypeLinear;
    self.iCarousel.contentOffset = CGSizeMake(-350, 0);
    self.announcementButton.hidden = YES;
    self.scrollView.delegate = self;
    if (self.isInteractivePresenter) {
        [self onBrnCalendarClick:self];
//        [self performSegueWithIdentifier:@"HomeToCalendar" sender:self];
    } else {
        [self setUpAgentAddressBook];
    }
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _numberOfItems = self.carouselImageArray.count;
    
    [_collectionView registerNib:[UINib nibWithNibName:@"Header" bundle:nil] forSupplementaryViewOfKind:NVBnbCollectionElementKindHeader withReuseIdentifier:@"header"];
}
- (void)setupPage
{
    //[MBProgressHUDUpd hideHUDForView:self.view animated:YES];
    CGFloat cx = 10;
    int existImageArray = [self.carouselImageArray count];
    if (existImageArray >= 8)
    {
        existImageArray = existImageArray;
    }
    else
    {
        existImageArray = existImageArray+1;
    }
    for(int count=0;count < existImageArray; count++)
    {
        CGRect rect;
        //button.frame = CGRectMake(0.0f, 0.0f, 235, 150);
        rect.size.height = 150.0f;
        rect.size.width = 235.0f;
        
        rect.origin.x = cx;
        rect.origin.y = 12.0f;
        self.scrollButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.scrollButton.layer setBorderColor: [[UIColor grayColor] CGColor]];
        [self.scrollButton.layer setBorderWidth: 1.0];
        self.scrollButton.frame = rect;
        self.scrollButton.tag = count;
        self.scrollButton.backgroundColor = [UIColor clearColor];
        self.scrollButton.userInteractionEnabled =YES;
        self.scrollButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.scrollButton.layer.masksToBounds = NO;
        if (self.carouselImageArray.count > count)
        {
            UIImage *cImage = nil;
            if([self.carouselImageArray[count] isKindOfClass:[NSData class]])
                cImage = [UIImage imageWithData:self.carouselImageArray[count]];
            [self.scrollButton setImage:cImage forState:UIControlStateNormal];
        }
        else
        {
           [self.scrollButton setTitle:NSLocalizedString(@"Add Image", @"") forState:UIControlStateNormal];
            [self.scrollButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        [self.scrollButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:self.scrollButton];
        [self.scrollView setExclusiveTouch:YES];
        self.scrollView.showsHorizontalScrollIndicator = NO;

        [self.scrollView setUserInteractionEnabled:YES];
            cx += 258;
        [self.scrollView setContentSize:CGSizeMake(cx, [self.scrollView bounds].size.height)];
    }
    
    
    UIStoryboard *storyBoard = [self storyboard];
    collectionDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"CollectionDetailViewControler"];
    NSLog(@"Carousel image array is %@",self.carouselImageArray);
    collectionDetailVC.arrData = [NSArray arrayWithArray:self.carouselImageArray];
    _numberOfItems = self.carouselImageArray.count;
}
-(void)importantAnnouncementClick:(NSNotification*)notification
{
    /*
    if ([notification.name isEqualToString:@"importantAnnoucement"])
    {
        NSDictionary* userInfo = notification.userInfo;
        Announcement *announcement = userInfo[@"code"];
        [self.pickerPopover dismissPopoverAnimated:YES];
        UIStoryboard *storyBoard = [self storyboard];
        AnnouncementViewController *announcementViewController  = [storyBoard instantiateViewControllerWithIdentifier:@"AnnouncementViewController"];
        announcementViewController.ann = announcement;
        [self.navigationController pushViewController:announcementViewController animated:YES];
    }
     */
    if ([notification.name isEqualToString:@"importantAnnoucement"]){
        AnnouncementViewController *announcementVC;
        NSDictionary* userInfo = notification.userInfo;
        Announcement *announcement = userInfo[@"code"];
        
        NSMutableArray *arr;
        NSMutableArray *arrMain;
        NSString *str = @"";
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[AnnouncementViewController class]]) {
                [self.pickerPopover dismissPopoverAnimated:YES];
                announcementVC = (AnnouncementViewController *)controller;
                arr = [NSMutableArray new];
                arrMain = [NSMutableArray new];
                arr = (NSMutableArray *)self.navigationController.viewControllers;
                for (int i = 0; i < [arr count]; i++){
                    if (![arr[i] isEqual:announcementVC]) {
                        [arrMain addObject:arr[i]];
                    }
                }
                arr = nil;
                [arrMain addObject:announcementVC];
                str = @"";
                break;
            } else {
                str = @"Not Found";
            }
        }
        if ([str isEqualToString:@"Not Found"]) {
            announcementVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AnnouncementViewController"];
            announcementVC.ann = announcement;
            [self.navigationController pushViewController:announcementVC animated:YES];
        } else {
            announcementVC.ann = announcement;
            [self.navigationController setViewControllers:arrMain animated:YES];
            arrMain = nil;
        }
    }
}

- (void)setUpAgentAddressBook
{
//    self.agentProfile = (TblAgentDetails *)[DbUtils fetchObject:@"TblAgentDetails"
//                                                     andPredict:[NSPredicate predicateWithFormat:@"userID == %@", [CachingService sharedInstance].currentUserId]
//                                              andSortDescriptor:nil
//                                           managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    self.agentArray = [DbUtils fetchAllObject:@"TblAgentDetails" andPredict:[NSPredicate predicateWithFormat:@"userID == %@", [CachingService sharedInstance].currentUserId] andSortDescriptor:nil managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    self.agentProfile = (TblAgentDetails *)[self.agentArray objectAtIndex:0];
    NSLog(@"%@",self.agentProfile.isFirstTimeLogin);
    if (self.agentProfile.isFirstTimeLogin){
        [self synContactCall];
    }else{
        self.pageNumber = 0;
        [self recurressiveAddressBookCall:self.pageNumber dateTime:@""];
    }
   
}
- (void)recurressiveAddressBookCall:(int)pageNumber dateTime:(NSString *)dateTime
{       NSLog(@"%@",dateTime);

    if (![dateTime isEqualToString:@""])
    {
        [self enableRightBar:NO];
        [MBProgressHUDUpd showHUDAddedTo:self.view withText:@"" animated:YES];
 
    }
    else
    {
        [self enableRightBar:NO];
        [MBProgressHUDUpd showHUDAddedToFirst:self.view withText:NSLocalizedString(@"Please Wait… we are setting up your account",@"") animated:YES];


    }
    __weak typeof(self) weakSelf = self;
    [self.contactEventModel getAddressBook:[CachingService sharedInstance].currentUserId dateTime:dateTime pageNumber:pageNumber success:^(int count) {
        
        self.pageNumber+=1;
        if (count < 20)
        {
            double  lastTimeBackUP = (double) ([[NSDate date] timeIntervalSince1970]);
            NSString *lastTimeBackString = [[NSString alloc]initWithFormat:@"%f",lastTimeBackUP];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:lastTimeBackString forKey:kLastBackUpTime];
            [self enableRightBar:YES];
            [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
            if ([dateTime isEqualToString:@""]) {
                weakSelf.agentProfile = (TblAgentDetails *)[weakSelf.agentArray objectAtIndex:0];
                weakSelf.agentProfile.isFirstTimeLogin = @1;
                [weakSelf.contactDataProvider saveLocalDBData];
                [CachingService sharedInstance].currentUserId = nil;
                [self loadAddressFromJSON];
                [ApplicationFunction showLoginView];
            }
            else
            {
               
                [self.appDelegate getAnnouncementsUpdate:self.view];
            }
        }
        else
        {
            [self recurressiveAddressBookCall:self.pageNumber dateTime:dateTime];
           
        }
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"%@",@"================ webservice fail call=================");
        [self recurressiveAddressBookCall:self.pageNumber dateTime:@""];
        //[self loadAddressFromJSON];
        //[CachingService sharedInstance].currentUserId = nil;
        //[ApplicationFunction showLoginView];
    }];
}
- (IBAction)announcementViewNavigation:(id)sender {
   /* NSArray *arr = [self.navigationController childViewControllers];
    
    if (![[arr lastObject] isKindOfClass:[AnnouncementViewController class]]) {
        [self.pickerPopover dismissPopoverAnimated:YES];
        UIStoryboard *storyBoard = [self storyboard];
        AnnouncementViewController *announcementViewController  = [storyBoard instantiateViewControllerWithIdentifier:@"AnnouncementViewController"];
        [self.navigationController pushViewController:announcementViewController animated:YES];
    }
    */
    AnnouncementViewController *announcement;
    NSMutableArray *arr;
    NSMutableArray *arrMain;
    NSString *str = @"";
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[AnnouncementViewController class]]) {
            [self.pickerPopover dismissPopoverAnimated:YES];
            announcement = (AnnouncementViewController *)controller;
            arr = [NSMutableArray new];
            arrMain = [NSMutableArray new];
            arr = (NSMutableArray *)self.navigationController.viewControllers;
            for (int i = 0; i < [arr count]; i++){
                if (![arr[i] isEqual:announcement]) {
                    [arrMain addObject:arr[i]];
                }
            }
            arr = nil;
            [arrMain addObject:announcement];
            str = @"";
            break;
        } else {
            str = @"Not Found";
        }
    }
    if ([str isEqualToString:@"Not Found"]) {
        announcement = [self.storyboard instantiateViewControllerWithIdentifier:@"AnnouncementViewController"];
        [self.navigationController pushViewController:announcement animated:YES];
    } else {
        [self.navigationController setViewControllers:arrMain animated:YES];
        arrMain = nil;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    [super viewWillDisappear:animated];
}

- (void)buttonTapped:(UIButton *)sender{
  
    NSLog(@"button tapped sender tag %d ",sender.tag);
    
    CATransition* transition = [CATransition animation];
    transition.duration = 1;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromBottom;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    collectionDetailVC.selectedIndex = sender.tag;
    
    [self presentViewController:collectionDetailVC animated:NO completion:nil];
    
}

- (void)handleImagePicker:(UIImagePickerController *)thePicker withMediaInfo:(NSDictionary *)imageInfo
{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil) {
        UIImage *profileImage;
        if (!profileImage) profileImage = imageInfo[UIImagePickerControllerOriginalImage];
        if(self.currentIndex < self.carouselImageArray.count)
            [self.carouselImageArray replaceObjectAtIndex:self.currentIndex withObject:UIImagePNGRepresentation(profileImage)];
        else if(self.carouselImageArray.count <= 8)
            [self.carouselImageArray insertObject:UIImagePNGRepresentation(profileImage) atIndex:self.currentIndex];
        self.agentProfile.agentGallayImages = self.carouselImageArray;

        
        //NSMutableArray *array = (NSMutableArray*)self.agentProfile.agentGallayImages;
        //[MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        // [self performSelector:@selector(setupPage) withObject:self afterDelay:1.0f];
        for (UIView *v in self.scrollView.subviews) {
            [v removeFromSuperview];
        }
        [self setupPage];
        [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    }

}
- (void)scrollViewDidScroll:(UIScrollView *)_newScrollView
{
    if(self.scrollView == _newScrollView)
    {

    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView
{
    //pageChangingPage = NO;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)setProfileData{
    NSString *userID =[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    self.agentProfile = (TblAgentDetails *)[DbUtils fetchObject:@"TblAgentDetails"
                                                                  andPredict:[NSPredicate predicateWithFormat:@"userID == %@", userID]
                                                           andSortDescriptor:nil
                                                        managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    self.agentProfileImageButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.agentProfileImageButton setImage:(self.agentProfile.agentImage) ? [UIImage imageWithData:self.agentProfile.agentImage] : nil
                                  forState:UIControlStateNormal];
    self.lblAgentName.text = self.agentProfile.userName;
    self.lblAgentPosition.text = self.agentProfile.titleDescribe;
    
    //change by Purva
    
    self.carouselImageArray = (NSMutableArray*)self.agentProfile.agentGallayImages;
    //self.carouselImageArray = [[NSMutableArray alloc] initWithObjects:@"dySellxa",@"dySellxb",@"dySellga",@"dySellgb",@"dySellla",@"dySelllb", nil];
    if(self.carouselImageArray.count == 0)
        self.carouselImageArray = [[NSMutableArray alloc] init];
    [self setupPage];
     [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    [_collectionView reloadData];
}

- (void)setGUI{
    BOOL isDateCheck = [[NSUserDefaults standardUserDefaults] boolForKey:@"isDateCheck"];
    
    [Utility setBorderAndRoundCorner:self.imageBorderView
                         borderColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:.5]
                               width:8.0
                        cornerRedius:self.imageBorderView.frame.size.width/2];
    [Utility setOuterShadow:self.imageBorderView
                      color:[UIColor grayColor]
                    opacity:.8
                     radius:3];
    
    [Utility setRoundCornerRedius:self.agentProfileImageButton
                     cornerRedius:self.agentProfileImageButton.frame.size.width/2];
    
    self.btnInteractive.titleLabel.numberOfLines =
    self.btnResource.titleLabel.numberOfLines =
    self.btnAddressbook.titleLabel.numberOfLines =
    self.btnGoal.titleLabel.numberOfLines = 2;
    
    self.btnInteractive.titleLabel.textAlignment =
    self.btnResource.titleLabel.textAlignment =
    self.btnAddressbook.titleLabel.textAlignment =
    self.btnGoal.titleLabel.textAlignment = NSTextAlignmentCenter;
    
}
/*
- (void)getAnnouncementsUpdate
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    [[self.serviceManager getDataFromServiceCall:kCMSAnnouncementURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
         [self insertAnnouncementsIntoDatabase:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
         [self showAnnouncements];
    }]resume];

}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
- (void)insertAnnouncementsIntoDatabase:(NSArray *)responseObject
{
    self.announcementDataModel = [[AnnouncementDataModel alloc]init];
    [self.announcementDataModel syncAnnouncementsIntoDatabase:responseObject returnNewAnnouncements:^(NSArray *importantArray, NSInteger count)
    {
    
        
        NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
        [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [[self.serviceManager getDataFromServiceCall:kCMSDeleteAnnouncementURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
            NSLog(@"%@",responseObject);
            NSArray *deletedArray = responseObject;
            if (deletedArray.count > 0) {
                [self.announcementDataModel deleteAnnouncements:deletedArray];
            }
                [self showAnnouncements];
            
            
        } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
            
            [self showAnnouncements];
            
        }]resume];
        
        
        
        
    }];
}
-(void)showAnnouncements
{
    NSString *agentId = [CachingService sharedInstance].currentUserId;

    NSArray *announcement = [[DbUtils fetchAllObject:@"Announcement"
                                          andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isRead == 0 AND msgType == %@ AND publishedDate <= %@ AND expDate > %@",agentId, @"Important",[DateUtils dateToString:[NSDate date]andFormate:kServerDateFormatte],[DateUtils dateToString:[NSDate date]andFormate:kServerDateFormatte]]
                                   andSortDescriptor:nil
                                managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    self.announcementButton.hidden = NO;
    [self.announcementButton setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[announcement count]] forState:UIControlStateNormal];
    if (announcement.count > 0) {
        [self showPopUP:announcement];
    } else {
        NSArray *announcement = [[DbUtils fetchAllObject:@"Announcement"
                                              andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isRead == 0 AND publishedDate <= %@ AND expDate > %@",agentId,[DateUtils dateToString:[NSDate date]andFormate:kDisplayDateFormatte],[DateUtils dateToString:[NSDate date]andFormate:kDisplayDateFormatte]]
                                       andSortDescriptor:nil
                                    managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
        [self.announcementButton setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[announcement count]] forState:UIControlStateNormal];
    }
}
*/
- (void)showPopUP:(NSArray *)importantArray{
    self.popover = [[AnnouncementPopOverViewController alloc] initWithNibName:@"AnnouncementPopOverViewController" bundle:nil];
    self.popover.importantAnnouncementArray = importantArray;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:self.popover];
    self.pickerPopover.popoverContentSize = CGSizeMake(253, 300);
    [self.pickerPopover presentPopoverFromRect:self.announcementButton.frame
                                       inView:self.view
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setTitle:NSLocalizedString(@"navigationTitle",@"")];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"openAnnouncement" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(announcementViewNavigation:) name:@"openAnnouncement" object:nil];
    self.navigationItem.leftBarButtonItem = [self prepareLeftBarButton:YES withLogo:YES];
    self.navigationItem.rightBarButtonItem = [self prepareRightBarButton:YES];
    //    self.navigationController.navigationBarHidden = TRUE;
    //    if (!self.announcementButton.hidden) {
    [self.appDelegate showAnnouncements:self.view];
    //    }
    [self setProfileData];
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
}

- (IBAction)onBtnLeftArrowClick:(id)sender {
}

- (IBAction)onBtnRightArrowClick:(id)sender {
}

- (IBAction)onBtnInteractiveClick:(id)sender {
    [self openInteractivePresenter];
}

- (IBAction)onBtnResourceClick:(id)sender {
}

- (IBAction)onBtnAddressbookClick:(id)sender
{
    [self navigateToAddressBook];
}

- (void)synContactCall
{
    self.syncObj = [[SyncModel alloc]init];
    [self enableRightBar:NO];
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    [self.syncObj syncObjectsIntoDatabase:[CachingService sharedInstance].currentUserId success:^{
        [self enableRightBar:YES];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        self.pageNumber = 0;
        NSString *lastBackUpTimeString = [[NSUserDefaults standardUserDefaults] objectForKey:kLastBackUpTime];
        [self recurressiveAddressBookCall:self.pageNumber dateTime:lastBackUpTimeString];
    }failure:^{
        [self enableRightBar:YES];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
    }];
}

- (void)navigateToAddressBook{
//    UISplitViewController *split = [self.storyboard instantiateViewControllerWithIdentifier:@"Split"];
//    UIView *sepratorView = [[UIView alloc] initWithFrame:CGRectMake(320, 0, 1, 64)];
//    [sepratorView setBackgroundColor:[UIColor colorWithRed:209.0/255.0 green:24.0/255.0 blue:72.0/255.0 alpha:1.0]];
//    [split.view addSubview:sepratorView];
    
    ContactNewListViewController *contact;
    NSMutableArray *arr;
    NSMutableArray *arrMain;
    NSString *str = @"";
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[ContactNewListViewController class]]) {
            contact = (ContactNewListViewController *)controller;
            arr = [NSMutableArray new];
            arrMain = [NSMutableArray new];
            arr = (NSMutableArray *)self.navigationController.viewControllers;
            for (int i = 0; i < [arr count]; i++){
                if (![arr[i] isEqual:contact]) {
                    [arrMain addObject:arr[i]];
                }
            }
            arr = nil;
            [arrMain addObject:contact];
            str = @"";
            break;
        } else {
            str = @"Not Found";
        }
    }
    if ([str isEqualToString:@"Not Found"]) {
        contact = [self.storyboard instantiateViewControllerWithIdentifier:@"contactController"];
        [self.navigationController pushViewController:contact animated:YES];
    } else {
        [self.navigationController setViewControllers:arrMain animated:YES];
        arrMain = nil;
    }
}

- (IBAction)onBrnCalendarClick:(id)sender {
//    [self performSegueWithIdentifier:@"HomeToCalendar" sender:self];
    CalendarViewController *calendar;
    NSMutableArray *arr;
    NSMutableArray *arrMain;
    NSString *str = @"";
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[CalendarViewController class]]) {
            calendar = (CalendarViewController *)controller;
            arr = [NSMutableArray new];
            arrMain = [NSMutableArray new];
            arr = (NSMutableArray *)self.navigationController.viewControllers;
            for (int i = 0; i < [arr count]; i++){
                if (![arr[i] isEqual:calendar]) {
                    [arrMain addObject:arr[i]];
                }
            }
            arr = nil;
            [arrMain addObject:calendar];
            str = @"";
            break;
        } else {
            str = @"Not Found";
        }
    }
    if ([str isEqualToString:@"Not Found"]) {
        calendar = [self.storyboard instantiateViewControllerWithIdentifier:@"CalendarViewController"];
        [self.navigationController pushViewController:calendar animated:YES];
    } else {
        [self.navigationController setViewControllers:arrMain animated:YES];
        arrMain = nil;
    }
}

- (void)btnSettingClick{
    
    SettingViewController *setting;
    NSMutableArray *arr;
    NSMutableArray *arrMain;
    NSString *str = @"";
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[SettingViewController class]]) {
            setting = (SettingViewController *)controller;
            arr = [NSMutableArray new];
            arrMain = [NSMutableArray new];
            arr = (NSMutableArray *)self.navigationController.viewControllers;
            for (int i = 0; i < [arr count]; i++){
                if (![arr[i] isEqual:setting]) {
                    [arrMain addObject:arr[i]];
                }
            }
            arr = nil;
            [arrMain addObject:setting];
            str = @"";
            break;
        } else {
            str = @"Not Found";
        }
    }
    if ([str isEqualToString:@"Not Found"]) {
        setting = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
        [self.navigationController pushViewController:setting animated:YES];
    } else {
        [self.navigationController setViewControllers:arrMain animated:YES];
        arrMain = nil;
    }
}

- (IBAction)onBtnGoalClick:(id)sender {
}

- (IBAction)onBtnReportClick:(id)sender {
}

- (IBAction)onAgentProfileImageButtonClick:(id)sender {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AgentProfileStoryboard" bundle:nil];
    AgentProfileViewController *agentProfile;
    NSMutableArray *arr;
    NSMutableArray *arrMain;
    NSString *str = @"";
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[AgentProfileViewController class]]) {
            agentProfile = (AgentProfileViewController *)controller;
            arr = [NSMutableArray new];
            arrMain = [NSMutableArray new];
            arr = (NSMutableArray *)self.navigationController.viewControllers;
            for (int i = 0; i < [arr count]; i++){
                if (![arr[i] isEqual:agentProfile]) {
                    [arrMain addObject:arr[i]];
                }
            }
            arr = nil;
            [arrMain addObject:agentProfile];
            str = @"";
            break;
        } else {
            str = @"Not Found";
        }
    }
    if ([str isEqualToString:@"Not Found"]) {
        agentProfile = [storyboard instantiateViewControllerWithIdentifier:@"AgentProfileStoryboard"];
        [self.navigationController pushViewController:agentProfile animated:YES];
    } else {
        [self.navigationController setViewControllers:arrMain animated:YES];
        arrMain = nil;
    }
}

- (IBAction)btnLogoutAction:(id)sender {
 //   [CachingService sharedInstance].currentUserId = nil;
    [ApplicationFunction doLogOut];
}

- (void)openInteractivePresenter
{
    FTDHomeViewController *FTDHomeViewCol=[[FTDHomeViewController alloc]init];
    TFDNavViewController *navFTDHomeViewCol=[[TFDNavViewController alloc]initWithRootViewController:FTDHomeViewCol];
    navFTDHomeViewCol.navigationBar.hidden=YES;
    [navFTDHomeViewCol setEopCallBackBlock:^{
        self.isInteractivePresenter = YES;
        [self onBrnCalendarClick:self];
//        [self performSegueWithIdentifier:@"HomeToCalendar" sender:self];
    }];
    [self presentViewController:navFTDHomeViewCol animated:YES completion:nil];
}

/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"HomeToCalendar"]) {
        CalendarViewController *destViewController = segue.destinationViewController;
        if (self.isInteractivePresenter) {
            destViewController.eventType = @"EOP";
            self.isInteractivePresenter = NO;
        }
    }
}
*/

// Load address dropdown data from JSON.
- (void)loadAddressFromJSON
{
    int count = [DbUtils getCountForEntity:@"TblAddress"
                                andPredict:nil
                      managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    if(count == 0){
    
        [self enableRightBar:NO];
        [MBProgressHUDUpd showHUDAddedTo:self.view withText:NSLocalizedString(@"Please Wait… we are setting up your account",@"") animated:YES];
        [self loadJSONInDb];

        [self enableRightBar:YES];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:FALSE];
    }
}

- (void)loadJSONInDb{
    for (NSDictionary *addressDic in [[DropDownManager sharedManager] getDropDownArray:DROP_DOWN_ADDRESS]) {
        TblAddress *address = [[ContactDataProvider sharedContactDataProvider] createAddress];
        address.field = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"FIELD1"]]];
        address.addressId = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"ID"]]];
        address.rgCodeA = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGCODEA"]]];
        address.rgCodeB = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGCODEB"]]];
        address.rgCodeC = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGCODEC"]]];
        address.rgFiller1 = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGFILLER1"]]];
        address.rgFiller2 = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGFILLER2"]]];
        address.rgLevel = [NSNumber numberWithInteger:[ValueParser parseInt:addressDic[@"RGLEVEL"]]];
        address.rgName = [ValueParser parseString:addressDic[@"RGNAME"]];
        address.rgsName = [ValueParser parseString:addressDic[@"RGSNAME"]];
        [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    }
}

-(void)enableRightBar:(BOOL)isEnable{
    if (isEnable) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

#pragma mark Grid View methods
//For Gridview Layout
- (NSInteger)numberOfItemsInBnbCollectionView:(NVBnbCollectionView *)collectionView {
    return _numberOfItems;
}

- (UICollectionViewCell *)bnbCollectionView:(NVBnbCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GridCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.label.text = [NSString stringWithFormat:@"Item %ld", (long)indexPath.row];
    cell.btnFullScreen.imageView.image = [self.carouselImageArray objectAtIndex:indexPath.row];
    [cell.imageView setImage:[self.carouselImageArray objectAtIndex:indexPath.row]];
    cell.btnFullScreen.tag = indexPath.row;
    [cell.btnFullScreen addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (UICollectionReusableView *)bnbCollectionView:(NVBnbCollectionView *)collectionView headerAtIndexPath:(NSIndexPath *)indexPath {
    Header *header = [_collectionView dequeueReusableSupplementaryViewOfKind: NVBnbCollectionElementKindHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
 
    
    [header.btnimage setBackgroundImage:(self.agentProfile.agentImage) ? [UIImage imageWithData:self.agentProfile.agentImage] : [UIImage imageNamed:@"male.png"] forState:UIControlStateNormal];
    [header.btnimage.layer setCornerRadius:75.0];
    header.btnimage.layer.masksToBounds = YES;
    
    [header.btnimage addTarget:self action:@selector(onAgentProfileImageButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    header.lblUserAgentDesignation.text =self.agentProfile.titleDescribe;
    header.lblUserAgentName.text = self.agentProfile.userName;
    
    return header;
}

- (UIView *)bnbCollectionView:(NVBnbCollectionView *)collectionView moreLoaderAtIndexPath:(NSIndexPath *)indexPath {
    UIActivityIndicatorView *view = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    view.color = [UIColor darkGrayColor];
    [view startAnimating];
    
    return view;
}

#pragma mark - NVBnbCollectionViewDelegate

- (void)loadMoreInBnbCollectionView:(NVBnbCollectionView *)collectionView {
    NSLog(@"loadMoreInBnbCollectionView:");
    if (_numberOfItems > 3) {
        collectionView.enableLoadMore = false;
        
        return;
    }
    _numberOfItems += 3;
    collectionView.loadingMore = false;
    [collectionView reloadData];
}

@end
