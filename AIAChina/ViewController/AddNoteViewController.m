//
//  AddNoteViewController.m
//  AIAChina
//
//  Created by MacMini on 12/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AddNoteViewController.h"
#import "ContactDataProvider.h"
#import "PickerPopoverViewController.h"
#import "DropDownManager.h"

@interface AddNoteViewController ()

@end

@implementation AddNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Add Notes", @"");
    [self loadNoteData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onBtnCancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onBtnActivityTypeClick:(id)sender {
    [self.view endEditing:TRUE];
    [self setPopoverView:sender pickerArray:@[NSLocalizedString(@"Telephone", @""),
                                              NSLocalizedString(@"Appointment", @""),
                                              NSLocalizedString(@"Participation", @""),
                                              NSLocalizedString(@"Others", @"")]];
}

- (IBAction)onBtnDateClick:(id)sender {
    [self.view endEditing:TRUE];
    
    [self showDatePopoverCtrl:UIDatePickerModeDate
                  minimumDate:nil
                  maximumDate:[NSDate date]
                         rect:self.btnDate.frame
                       inView:self.view
                   pickerDate:^(NSDate *pickerDate) {
                       [self.btnDate setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:kDisplayDateFormatte]] forState:UIControlStateNormal];
                   }];

}

- (IBAction)onBtnDoneClick:(id)sender {
    [self.view endEditing:TRUE];
    if([self validation]){
        [self addLocalDatabase];
        self.contact.isSync = [NSNumber numberWithInt:1];
        if(self.reloadNoteBlock)
            self.reloadNoteBlock();
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (BOOL)validation{
    NSString *msg = nil;
    if(isEmptyString(self.btnActivityType.currentTitle))
        msg = NSLocalizedString(@"Please select activity type", @"");
    else if(isEmptyString(self.btnDate.currentTitle))
        msg = NSLocalizedString(@"Please add date", @"");
    else if(isEmptyString(self.txtDescription.text))
        msg = NSLocalizedString(@"Please add description", @"");
    
    if(!isEmptyString(msg)){
        [DisplayAlert showAlertInControllerInView:self
                                            title:NSLocalizedString(@"Error", @"")
                                          message:msg
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:^(UIAlertAction *action) {
                                      }
                                          okBlock:nil];
        return FALSE;
    }
    return TRUE;
}

- (void)setPopoverView:(UIButton *)btn pickerArray:(NSArray *)pickerArray{
    NSMutableArray *pickerDataArray = [[NSMutableArray alloc] initWithObjects:@"", nil];
    [pickerDataArray addObjectsFromArray:[pickerArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    
    PickerPopoverViewController *popoverViewCtrl = [[PickerPopoverViewController alloc] initWithNibName:@"PickerPopoverViewController" bundle:nil];
    if(isEmptyString(btn.titleLabel.text))
        [btn setTitle:pickerDataArray[0] forState:UIControlStateNormal];
    else
        popoverViewCtrl.oldValue = btn.titleLabel.text;
    
    [popoverViewCtrl setPickerArray:pickerDataArray];
    [popoverViewCtrl setSelectedIndex:^(int selectedRowIndexPath) {
        [btn setTitle:pickerDataArray[selectedRowIndexPath] forState:UIControlStateNormal];
            [self.notePopoverCntrlr dismissPopoverAnimated:TRUE];
    }];
    
    self.notePopoverCntrlr = [[UIPopoverController alloc] initWithContentViewController:popoverViewCtrl];
    self.notePopoverCntrlr.popoverContentSize = popoverViewCtrl.view.frame.size;
    [self.notePopoverCntrlr presentPopoverFromRect:btn.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:TRUE];
}

- (void)loadNoteData{
    if(self.note){
        self.lblTitle.text = NSLocalizedString(@"Edit Note", @"");
        [self.btnActivityType setTitle:self.note.activityType forState:UIControlStateNormal];
        [self.btnDate setTitle:[DateUtils dateToString:self.note.activityDate andFormate:kDisplayDateFormatte] forState:UIControlStateNormal];
        self.txtDescription.text = self.note.desc;
    }
}

- (void)addLocalDatabase
{

    if (!self.note) {
        self.note = [[ContactDataProvider sharedContactDataProvider] createNote];
        self.note.iosAddressCode = [NSString stringWithFormat:@"%@",[[self.note objectID] URIRepresentation]];
    }
    self.note.isDelete = [NSNumber numberWithBool:FALSE];
    self.note.activityDate = [DateUtils stringToDate:self.btnDate.currentTitle dateFormat:kDisplayDateFormatte];
    self.note.activityType = self.btnActivityType.currentTitle;
    self.note.desc = self.txtDescription.text;
    self.note.activityStatus = [NSNumber numberWithInt:1];
    self.note.isSystem = [NSNumber numberWithBool:FALSE];
    self.note.isSync = [NSNumber numberWithInt:1];
    self.contact.lastContactedDate = [NSDate date];
    [self.contact addCandidateNotesObject:self.note];
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
}

@end
