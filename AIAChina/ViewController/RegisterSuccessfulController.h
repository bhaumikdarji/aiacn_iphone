//
//  RegisterSuccessfulController.h
//  AIAChina
//
//  Created by MacMini on 24/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterSuccessfulController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgHeader;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *lblCong;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblRanking;
@property (weak, nonatomic) IBOutlet UITableView *tblRegisterUser;

@property (weak, nonatomic) IBOutlet UILabel *lblGAMAQuoteEnglish;
@property (weak, nonatomic) IBOutlet UILabel *lblGAMAQuoteChinese;
@property (weak, nonatomic) IBOutlet UILabel *lblGAMAAuthorEnglish;

@property (strong, nonatomic) NSArray *contactArray;
@property (nonatomic) int candidateProgressRank;
@property (nonatomic, strong) NSString *candidateName;
@property (nonatomic, strong) NSString *desc;

- (IBAction)onBtnCancelClick:(id)sender;

@end
