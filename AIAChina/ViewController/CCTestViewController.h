//
//  CCTestViewController.h
//  AIAChina
//
//  Created by AIA on 06/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCTestViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UIButton *btnAssessmentResult;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnRecruit;
@property (strong, nonatomic) NSString *oldValue;

@property (nonatomic) TblContact *contact;
@property (copy, nonatomic) void(^callBackBlock)();

- (IBAction)onBtnAssessmentResultClick:(id)sender;
- (IBAction)onBtnRecruitClick:(id)sender;
- (IBAction)onBtnSubmitClick:(id)sender;
- (IBAction)onBtnCancelClick:(id)sender;

@end
