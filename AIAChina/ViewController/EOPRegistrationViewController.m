//
//  EOPRegistrationViewController.m
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EOPRegistrationViewController.h"
#import "EOPRegistrationCell.h"
#import "TblEOP.h"
#import "ContactEventModel.h"
#import "ContactDataProvider.h"
#import "PdfDownloadObject.h"
#import "AFHTTPRequestOperationManager.h"
#import "CalendarEventModel.h"

@interface EOPRegistrationViewController ()<UIWebViewDelegate>
@property (nonatomic, retain)  NSArray *allInterviewEventArray;
@property (nonatomic, retain) ContactEventModel *eventModel;
@property (weak, nonatomic) IBOutlet UILabel *lblOrganizer;
@property (nonatomic, retain) TblEOP *currentEOPObj;
@property (nonatomic, assign) NSInteger currentEOPObjIndex;
@property (nonatomic, assign) NSInteger lastSelectedIndex;
@property (nonatomic, assign) NSInteger selectedIndex;

@end

@implementation EOPRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationItem.title = NSLocalizedString(@"EOP Registration", @"");
    self.eventModel = [ContactEventModel sharedInstance];
    self.lastSelectedIndex = 0;
    [self loadData];
    
    if (self.allInterviewEventArray.count > 0)
        [self loadDataBasedOnTableViewSelection:self.allInterviewEventArray[0]];
}
//- (void)showWebView
//{
//    self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(10.0f, 20.0f, 1010, 740)];
//    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.closeButton.frame = CGRectMake(986.0f, 23.0f, 32.0f, 32.0f);
//    [self.closeButton setBackgroundImage:[UIImage imageNamed:@"close_red.png"] forState:UIControlStateNormal];
//    [self.closeButton addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
//    [[self.webView layer] setCornerRadius:10];
//    [self.webView setClipsToBounds:YES];
//    self.webView.delegate = self;
//}
//- (void)closeWebView
//{
//    [self.webView removeFromSuperview];
//    [self.closeButton removeFromSuperview];
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData{
    self.allInterviewEventArray = [DbUtils fetchAllObject:@"TblEOP"
                                               andPredict:[NSPredicate predicateWithFormat:@"calendarEvent.agentCode == %@",[CachingService sharedInstance].currentUserId]
                                        andSortDescriptor:[NSSortDescriptor sortDescriptorWithKey:@"eventDate" ascending:YES]
                                     managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    [self.tblEopReg reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([self.allInterviewEventArray count] > 0)
        self.rightView.hidden = FALSE;
    else
        self.rightView.hidden = TRUE;
    return [self.allInterviewEventArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EOPRegistrationCell *cell = (EOPRegistrationCell *)[tableView dequeueReusableCellWithIdentifier:@"EOPRegistrationCell"];
    TblEOP *eopObj = self.allInterviewEventArray[indexPath.row];
    [cell loadRegData:eopObj];
    [cell.btnEventDate addTarget:self action:@selector(eopButtonDateTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnTopicName addTarget:self action:@selector(eopButtonDateTapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnTopicName.tag =  cell.btnEventDate.tag = cell.btnRegistration.tag = cell.btnShare.tag = indexPath.row;
    
    if([eopObj.isRegistered boolValue])
        cell.registeredImageView.hidden = NO;
    else
        cell.registeredImageView.hidden = YES;
    
    if (indexPath.row == self.lastSelectedIndex) {
        cell.backgroundColor = REG_BG_COLOR;
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if([eopObj.objectID isEqual:self.currentEOPObj.objectID])
        [self loadDataBasedOnTableViewSelection:self.currentEOPObj];
    return  cell;
}

- (void)eopButtonDateTapped:(UIButton *)sender{
    
    self.selectedIndex = self.currentEOPObjIndex = [sender tag];
    if (self.lastSelectedIndex>=0) {
        EOPRegistrationCell *cell = [self.tblEopReg cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.lastSelectedIndex inSection:0]];
        cell.backgroundColor = [UIColor whiteColor];
    }
    EOPRegistrationCell *cell = [self.tblEopReg cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    cell.backgroundColor = REG_BG_COLOR;
    self.lastSelectedIndex = sender.tag;
    [self loadDataBasedOnTableViewSelection:self.allInterviewEventArray [[sender tag]]];
}

- (IBAction)onBtnCellRegisterClick:(UIButton *)sender{
    [self callRegisterEopCall:self.allInterviewEventArray[[sender tag]]];
}

- (IBAction)onBtnShareClick:(UIButton *)sender{
    EOPRegistrationCell *cell = (EOPRegistrationCell *)[self.tblEopReg cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[sender tag] inSection:0]];
    TblEOP *eop = self.allInterviewEventArray[self.currentEOPObjIndex];
    [self showSocialSharingPopoverCtrl:self.contact
                                EOPObj:eop
                            isGreeting:FALSE
                                 isEOP:TRUE
                              isQRCode:FALSE
                            shareImage:[ApplicationFunction generateBarcodeImageWithQRCode:[eop getLocalizedQRCodeString]]
                        shareImageFile:nil
                              shareMsg:nil
                              shareSub:eop.eventName
                                  rect:((UIButton *)sender).frame
                                inView:cell];
}

- (void)loadDataBasedOnTableViewSelection:(TblEOP *)eopObj{
    self.currentEOPObj = eopObj;
    
    [self getTotalRegCandidateCountApi:eopObj];
    self.lblDate.text = [NSDate dateToString:eopObj.eventDate andFormate:kDisplayDateFormatte];
    
    self.lblStartTime.text = [[NSDate stringDateToString:eopObj.startTime
                                      currentDateForamte:kServerDateFormatte
                                      displayDateFormate:@"HH:mma"] lowercaseString];
    self.lblEndTime.text = [[NSDate stringDateToString:eopObj.endTime
                                    currentDateForamte:kServerDateFormatte
                                    displayDateFormate:@"HH:mma"] lowercaseString];
    self.txtLocation.text = eopObj.location;
    self.txtDescription.text = eopObj.eopDescription;
    self.txtLocation.textColor = self.txtDescription.textColor = [UIColor colorWithRed:99.0/255.0 green:99.0/255.0 blue:99.0/255.0 alpha:1.0];
    self.txtLocation.font = self.txtDescription.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
    self.lblSpeaker.text = eopObj.speaker;
    self.lblOrganizer.text = eopObj.oraganiserStr;
    [self.btnTopic setAttributedTitle:[Utility setUnderline:eopObj.topic
                                              withTextColor:self.btnTotalCandidate.currentTitleColor]
                             forState:UIControlStateNormal];
    [self.btnTotalCandidate setAttributedTitle:[Utility setUnderline:[eopObj.registeredCount stringValue]
                                                       withTextColor:self.btnTotalCandidate.currentTitleColor]
                                      forState:UIControlStateNormal];
    
    if(isEmptyString(eopObj.profilePath))
        self.EMHeightConstraint.constant = 0;
    else{
        [self.btnEventMaterials setAttributedTitle:[Utility setUnderline:[[eopObj.profilePath componentsSeparatedByString:@"/"] lastObject]
                                                           withTextColor:self.btnEventMaterials.currentTitleColor]
                                          forState:UIControlStateNormal];
        self.EMHeightConstraint.constant = 40;
    }
    
    if(isEmptyString(eopObj.topicPath))
        self.AEMHeightConstraint.constant = 0;
    else{
        [self.btnAfterEventMaterials setAttributedTitle:[Utility setUnderline:[[eopObj.topicPath componentsSeparatedByString:@"/"] lastObject]
                                                                withTextColor:self.btnAfterEventMaterials.currentTitleColor]
                                               forState:UIControlStateNormal];
        self.AEMHeightConstraint.constant = 40;
    }
    
    NSDate *startDate = [NSDate stringToDate:eopObj.startTime dateFormat:kServerDateFormatte];
    NSDate *eventDate = [NSDate dateWithYear:eopObj.eventDate.year month:eopObj.eventDate.month day:eopObj.eventDate.day hour:startDate.hour min:startDate.minute];
    
    if([eventDate isEarlierThanDate:[NSDate date]])
        self.btnRegister.hidden = TRUE;
    else{
        self.btnRegister.hidden = FALSE;
        if([eopObj.isRegistered boolValue])
            [self.btnRegister setTitle:NSLocalizedString(@"Cancel",@"") forState:UIControlStateNormal];
        else
            [self.btnRegister setTitle:NSLocalizedString(@"Registration",@"") forState:UIControlStateNormal];
    }
    if([eopObj.openToRegistration isEqualToString:@"Y"])
        self.btnShare.hidden = FALSE;
    else
        self.btnShare.hidden = TRUE;
}

- (void)getTotalRegCandidateCountApi:(TblEOP *)eopObj{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:TRUE];
    [self.eventModel getEOPRegisteredCandidateCount:[eopObj.eventCode stringValue] agentID:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        for (NSArray *response in responseObject) {
            eopObj.registeredCount = [NSNumber numberWithInteger:[ValueParser parseInt:[response valueForKey:@"registeredCount"]]];
            [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
            [self.btnTotalCandidate setAttributedTitle:[Utility setUnderline:[eopObj.registeredCount stringValue]
                                                               withTextColor:self.btnTotalCandidate.currentTitleColor]
                                              forState:UIControlStateNormal];
        }
    } failure:^(id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        [self.btnTotalCandidate setAttributedTitle:[Utility setUnderline:[eopObj.registeredCount stringValue]
                                                           withTextColor:self.btnTotalCandidate.currentTitleColor]
                                          forState:UIControlStateNormal];
    }];
}

- (IBAction)onBtnTopicClick:(id)sender
{
    
    /*PdfDownloadObject *pdfDownloadObj = [[PdfDownloadObject alloc]init];
     [pdfDownloadObj downloadFileWithFilePath:announcement.annoucement_code fileName:fileName finished:^(NSString *filePath, NSError *error)
     {
     NSData *pdfData = [NSData dataWithContentsOfFile:filePath];
     pdfPoolObj.pdfdata = pdfData;
     announcementObj.pdfDetails = pdfPoolObj;
     [self.announcementSynchronizer save];
     success(announcement);
     }];
     */
}
- (IBAction)btnLocationClick:(id)sender {
    NSString *address = self.txtLocation.text;
    [self getSearchLocation:address];
}

- (IBAction)onBtnEventMaterialsClick:(id)sender
{
    NSInteger index = [(UIButton *)sender tag];
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    TblEOP *eop = self.allInterviewEventArray[index];
    PdfDownloadObject *pdfDownloadObj = [[PdfDownloadObject alloc]init];
    [pdfDownloadObj downloadFile:[eop.eventCode stringValue] afterMaterial:NO finished:^(id response) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        [self showWebView];
        if([response isKindOfClass:[NSData class]])
        {
            UIWindow * window = [UIApplication sharedApplication].windows.lastObject;
            if ([eop.topicPath hasSuffix:@"jpg"])
            {
                [self.webView loadData:response MIMEType:@"application/jpg" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([eop.topicPath hasSuffix:@"png"])
            {
                [self.webView loadData:response MIMEType:@"application/png" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([eop.topicPath hasSuffix:@"pdf"])
            {
                [self.webView loadData:response MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([eop.topicPath hasSuffix:@"xls"])
            {
                [self.webView loadData:response MIMEType:@"application/xls" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            
        }
    } failure:^(NSError *error) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:@"暂时无法下载文档，请稍后再尝试"
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
    }];
}

- (IBAction)onBtnAfterEventMaterialsClick:(id)sender
{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    TblEOP *eop = self.allInterviewEventArray[[(UIButton *)sender tag]];
    PdfDownloadObject *pdfDownloadObj = [[PdfDownloadObject alloc]init];
    [pdfDownloadObj downloadFile:[eop.eventCode stringValue] afterMaterial:YES finished:^(id response) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        [self showWebView];
        if([response isKindOfClass:[NSData class]])
        {
            UIWindow * window = [UIApplication sharedApplication].windows.lastObject;
            
            if ([eop.profilePath hasSuffix:@"jpg"])
            {
                [self.webView loadData:response MIMEType:@"application/jpg" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([eop.profilePath hasSuffix:@"png"])
            {
                [self.webView loadData:response MIMEType:@"application/png" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([eop.profilePath hasSuffix:@"pdf"])
            {
                [self.webView loadData:response MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([eop.topicPath hasSuffix:@"xls"])
            {
                [self.webView loadData:response MIMEType:@"application/xls" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
        }
    } failure:^(NSError *error) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:@"暂时无法下载文档，请稍后再尝试"
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
    }];
    
}

- (IBAction)onBtnTotalCandidateClick:(id)sender{
    if([self.btnTotalCandidate.titleLabel.text integerValue] > 0){
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [self.eventModel getEOPRegisteredCandidate:[self.currentEOPObj.eventCode stringValue] agentId:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
            [self showEOPRegCandAndAttandPopoverCtrl:self.view.bounds
                                              inView:self.view
                                           eventCode:[self.currentEOPObj.eventCode integerValue]
                                       callBackBlock:NULL];
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        } failure:^(id responseObject, NSError *error) {
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
            [self showEOPRegCandAndAttandPopoverCtrl:self.view.bounds
                                              inView:self.view
                                           eventCode:[self.currentEOPObj.eventCode integerValue]
                                       callBackBlock:NULL];
        }];
    }
}

- (IBAction)onBtnRegisterClick:(id)sender{
    [self callRegisterEopCall:self.currentEOPObj];
}

- (void)callRegisterEopCall:(TblEOP *)eop{
    if([self.contact.addressCode integerValue] == 0){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"Please sync candidate",@"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }else if(isEmptyString([self.contact getLocalizedQRCodeString])){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"QR Code must be generated for Contact",@"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }
    if(eop.isRegisteredValue){
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        NSLog(@"eop event code %@",[eop.eventCode stringValue]);
        
        [self.eventModel deleteEOPRegistration:[eop.eventCode stringValue]
                                 candidateCode:[self.contact.addressCode stringValue]
                                       success:^(id responseObject) {
                                           for (NSArray *response in responseObject) {
                                               if([[response valueForKey:@"status"] boolValue])
                                                   [self updateStatus:eop isRegister:FALSE];
                                               AFHTTPRequestOperationManager *ASAPIManager=[[AFHTTPRequestOperationManager alloc]init];
                                               AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
                                               NSString *sessionId=delegate.sessionid;
                                               if (sessionId== nil || [sessionId isEqualToString:@""]){
                                               }
                                               else {
                                                   int value = [eop.registeredCount intValue];
                                                   if (value == 0){
                                                       // creation of json dictionary with parameters
                                                       NSDictionary *ob1 = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                                            [NSString stringWithFormat:@"%@-%@",eop.eventCode,
                                                                             [CachingService sharedInstance].currentUserId],@"referenceId",
                                                                            [CachingService sharedInstance].branchCode,@"co",
                                                                            [CachingService sharedInstance].currentUserIdWithZero,@"agentcode",nil];
                                                       NSArray *arr = [[NSArray alloc]initWithObjects:ob1,nil];
                                                       
                                                       //from 03 is for erecruitment , version is static 2.0.1 & option D is for delete and arr is the dictionary of events.
                                                       NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:kCMSVersionConstant,@"version",@"D",@"option",@"03",@"from",arr,@"events",nil];
                                                       
                                                       NSDictionary *postDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:dict,@"RequestMessage",nil];
                                                       NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDictionary options:NSJSONWritingPrettyPrinted error:nil];
                                                       NSString *jsonString;
                                                       //convert the dictionary to json
                                                       if (! jsonData) {
                                                       } else {
                                                           jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                                       }
                                                       jsonString=[jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                                                       jsonString=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                                                       NSString *encodedStr=[delegate md5StringForString:jsonString];
                                                       [ASAPIManager.securityPolicy setAllowInvalidCertificates:YES];
                                                       [ASAPIManager.requestSerializer setValue:@"multipart/form-data;charset=UTF-8;boundary=--AIACalendar--" forHTTPHeaderField:@"Content-Type"];
                                                       [ASAPIManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html" ,nil]];
                                                       
                                                       [ASAPIManager POST:KErecruitmentEnhancementURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                           [formData appendPartWithFormData:[sessionId dataUsingEncoding:NSUTF8StringEncoding] name:@"SessionId"];
                                                           [formData appendPartWithFormData:[@"UTF-8" dataUsingEncoding:NSUTF8StringEncoding] name:@"Encoding"];
                                                           [formData appendPartWithFormData:[encodedStr dataUsingEncoding:NSUTF8StringEncoding] name:@"Signature"];
                                                           [formData appendPartWithFormData:[@"OneCalendarEventService" dataUsingEncoding:NSUTF8StringEncoding] name:@"Service"];
                                                           [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding] name:@"Data"];
                                                           
                                                       } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                           NSDictionary *Resposedict=[responseObject valueForKey:@"ResponseMessage"];
                                                           BOOL exists = [Resposedict objectForKey:@"returnMessage"] != nil;
                                                           if (!exists) {
                                                               UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                                                               [alert show];
                                                           } else {
                                                               //delete
                                                               NSString *str=[Resposedict valueForKey:@"returnMessage"];
                                                               if ([str isEqualToString:@"Successful!"]) {
                                                                   NSString *strRefid = [NSString stringWithFormat:@"%@-%@",eop.eventCode,[CachingService sharedInstance].currentUserId];
                                                                   //deleting frfom local db
                                                                   [[CalendarEventModel sharedInstance] deleteICalendarEvent:strRefid AgentCode:[CachingService sharedInstance].currentUserIdWithZero];
                                                               }
                                                           }
                                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                           UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                                                           [alert show];
                                                           
                                                       }];
                                                   }
                                               }
                                               
                                           }
                                           [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                       } failure:^(id responseObject, NSError *error) {
                                           [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                       }];
    }else{
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        
        NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc]init];
        [bodyDictionary setValue:self.contact.addressCode forKey:@"candidateCode"];
        [bodyDictionary setValue:eop.eventCode forKey:@"eventCode"];
        [bodyDictionary setValue:self.contact.name forKey:@"candidateName"];
        [bodyDictionary setValue:[CachingService sharedInstance].currentUserId forKey:@"servicingAgent"];
        [bodyDictionary setValue:self.contact.referalSource forKey:@"sourceOfReferal"];
        [bodyDictionary setValue:self.contact.age forKey:@"age"];
        [bodyDictionary setValue:[DateUtils dateToString:self.contact.birthDate andFormate:kNewDisplayDateFormatte] forKey:@"dob"];
        [bodyDictionary setValue:@"" forKey:@"dobStr"];
        [bodyDictionary setValue:self.contact.gender forKey:@"gender"];
        [bodyDictionary setValue:self.contact.mobilePhoneNo forKey:@"contactNumber"];
        [bodyDictionary setValue:@"true" forKey:@"statusStr"];
        [bodyDictionary setValue:@"" forKey:@"token"];
        
        [self.eventModel postCandidateRegistrationServiceCall:bodyDictionary agentId:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
            for (NSArray *response in responseObject) {
                BOOL status = [[response valueForKey:@"status"] boolValue];
                BOOL duplicate = [[response valueForKey:@"isDuplicate"] boolValue];
                int registeredCount = [[response valueForKey:@"registeredCount"]intValue];
                // If register count is 1 means the first candidate is registered for the event
                
                AFHTTPRequestOperationManager *ASAPIManager=[[AFHTTPRequestOperationManager alloc]init];
                AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
                NSString *sessionId=delegate.sessionid;
                if (sessionId == nil || [sessionId isEqualToString:@""]) {
                } else {
                    if (registeredCount ==1 && status && !duplicate) {
                        
                        NSString *strMainTime = [DateUtils dateToString:self.currentEOPObj.eventDate andFormate:kServerDateFormatte];
                        NSString *subMainTime = [strMainTime substringToIndex:10];
                        
                        NSString *strStartTime = self.currentEOPObj.startTime;
                        NSString *subStartTime = [strStartTime substringFromIndex:11];
                        subStartTime = [subStartTime stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        NSString *strEndTime = self.currentEOPObj.endTime;
                        NSString *subEndTime =  [strEndTime substringFromIndex:11];
                        subEndTime = [subEndTime stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        strStartTime = [NSString stringWithFormat:@"%@##%@",subMainTime,subStartTime];
                        strEndTime = [NSString stringWithFormat:@"%@##%@",subMainTime,subEndTime];
                        
                        // creation of json dictionary with parameters
                        NSDictionary *ob1 = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@-%@",eop.eventCode,[CachingService sharedInstance].currentUserId],@"referenceId",
                                             [CachingService sharedInstance].branchCode,@"co",
                                             [CachingService sharedInstance].currentUserIdWithZero,@"agentcode",
                                             @"09",@"typeId",
                                             NSLocalizedString(@"EOP", @""),@"typeName",
                                             self.currentEOPObj.topic,@"subject",
                                             self.currentEOPObj.eopDescription,@"description" ,
                                             self.currentEOPObj.location,@"address",
                                             strStartTime,@"startTime",
                                             strEndTime,@"endTime",
                                             @"N",@"openFlag",nil];
                        
                        NSArray *arr = [[NSArray alloc]initWithObjects:ob1,nil];
                        //from 03 is for erecruitment , version is static 2.0.1 & option A is for Add and arr is the dictionary of events.
                        
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:kCMSVersionConstant,@"version",@"A",@"option",@"03",@"from",arr,@"events",nil];
                        NSDictionary *postDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:dict,@"RequestMessage",nil];
                        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDictionary options:NSJSONWritingPrettyPrinted error:nil];
                        NSString *jsonString;
                        //convert the dictionary to json
                        if (! jsonData) {
                        } else {
                            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                        }
                        
                        jsonString=[jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        jsonString=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                        jsonString=[jsonString stringByReplacingOccurrencesOfString:@"##" withString:@" "];
                        
                        NSString *encodedStr=[delegate md5StringForString:jsonString];
                        
                        [ASAPIManager.securityPolicy setAllowInvalidCertificates:YES];
                        [ASAPIManager.requestSerializer setValue:@"multipart/form-data;charset=UTF-8;boundary=--AIACalendar--" forHTTPHeaderField:@"Content-Type"];
                        [ASAPIManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html" ,nil]];
                        [ASAPIManager POST:KErecruitmentEnhancementURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                            [formData appendPartWithFormData:[sessionId dataUsingEncoding:NSUTF8StringEncoding] name:@"SessionId"];
                            [formData appendPartWithFormData:[@"UTF-8" dataUsingEncoding:NSUTF8StringEncoding] name:@"Encoding"];
                            [formData appendPartWithFormData:[encodedStr dataUsingEncoding:NSUTF8StringEncoding] name:@"Signature"];
                            [formData appendPartWithFormData:[@"OneCalendarEventService" dataUsingEncoding:NSUTF8StringEncoding] name:@"Service"];
                            [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding] name:@"Data"];
                            
                        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            NSDictionary *Resposedict=[responseObject valueForKey:@"ResponseMessage"];
                            BOOL exists = [Resposedict objectForKey:@"returnMessage"] != nil;
                            if (!exists) {
                                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                                [alert show];
                            }
                            
                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                            
                            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                            [alert show];
                        }];
                    }
                }
                if(!status && !duplicate){
                    NSLog(@"Error");
                }else{
                    [self updateCandidateProcess];
                    if(duplicate){
                        [DisplayAlert showAlertInControllerInView:self
                                                            title:NSLocalizedString(@"Registration",@"")
                                                          message:NSLocalizedString(@"Already registration",@"")
                                                cancelButtonTitle:NSLocalizedString(@"OK",@"")
                                                    okButtonTitle:nil
                                                      cancelBlock:^(UIAlertAction *action) {
                                                          [self showGAMAPopup];
                                                      } okBlock:nil];
                    }else{
                        [self createNoteWithTblContact:self.contact
                                              noteDate:[NSDate date]
                                              noteDesc:NSLocalizedString(@"Registered for EOP Event Successfully", @"")];
                        [self updateStatus:eop isRegister:TRUE];
                        [self showGAMAPopup];
                    }
                }
            }
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        } failure:^(id responseObject, NSError *error) {
            NSLog(@"%@",error.description);
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        }];
    }
}



- (void)showGAMAPopup{
    [self showRegisterSuccessfulPopoverCtrl:[self.contact.candidateProcess.candidateRank integerValue]
                              candidateName:self.contact.name
                                       desc:NSLocalizedString(@"completed registration for EOP! See you there!", @"")
                                       rect:self.view.bounds
                                     inView:self.view];
    
}

- (void)updateStatus:(TblEOP *)eop isRegister:(BOOL)isRegister{
    eop.isRegisteredValue = isRegister;
    if(isRegister)
        eop.registeredCount = [NSNumber numberWithInteger:[eop.registeredCount integerValue] + 1];
    else
        eop.registeredCount = [NSNumber numberWithInteger:[eop.registeredCount integerValue] - 1];
    
    [[ContactDataProvider sharedContactDataProvider]saveLocalDBData];
    [self.tblEopReg reloadData];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self updateCandidateProcess];
}

- (void)updateCandidateProcess{
    
    TblEOP *eop = (TblEOP *)[DbUtils fetchObject:@"TblEOP"
                                      andPredict:[NSPredicate predicateWithFormat:@"calendarEvent.agentCode == %@ AND isRegistered = 1",[CachingService sharedInstance].currentUserId]
                               andSortDescriptor:[NSSortDescriptor sortDescriptorWithKey:@"eventDate" ascending:FALSE]
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    if (eop.isRegisteredValue) {
        [ApplicationFunction setUpdateUserProcess:RECURITMENT_EOP_REGISTRATION
                                          contact:self.contact
                                    processStatus:(eop) ? RECURITMENT_STATUS_TRUE : RECURITMENT_STATUS_FALSE
                                      processDate:[NSDate date]];
    } else {
        [ApplicationFunction setUpdateUserProcess:RECURITMENT_EOP_REGISTRATION
                                          contact:self.contact
                                    processStatus:nil
                                      processDate:nil];
        
    }
    if(self.callBackBlock)
        self.callBackBlock();
}

@end
