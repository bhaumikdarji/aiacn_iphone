//
//  GreetingsViewController.m
//  imo
//
//  Created by Smitesh Patel on 2014-11-20.
//  Copyright (c) 2014 iMO. All rights reserved.
//

#import "GreetingsViewController.h"
#import "GreetingsCollectionCell.h"
#import "Utility.h"
#import "GreetingCell.h"
#import "ContactEventModel.h"
#import "GreetingsMTLModel.h"
#import "TblGreetings.h"
#import "UIImageView+AFNetworking.h"

@interface GreetingsViewController ()

@end

@implementation GreetingsViewController

static NSString *const reuseIdentifier = @"GreetingCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"E Greeting", @"");
    self.categoryListArray = [[NSArray alloc] init];
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:TRUE];
    [[ContactEventModel sharedInstance] getEGreetingsForAgentCode:[[CachingService sharedInstance] currentUserId] success:^(id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        if ([(NSArray *)responseObject count] > 0)
            [self loadGreetingData];
    } failure:^(id responseObject, NSError *error){
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
            [self loadGreetingData];
    }];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = [self prepareRightBarButton:YES];
    self.navigationItem.leftBarButtonItem = [self prepareLeftBarButton:YES withLogo:NO];
    [MyAppDelegate showAnnouncements:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadGreetingData{
    self.greetingListArray = [DbUtils fetchAllObject:@"TblGreetings"
                                          andPredict:[NSPredicate predicateWithFormat:@"agentId == %@",[[CachingService sharedInstance] currentUserId]]
                                   andSortDescriptor:nil
                                managedObjectContext:[CachingService sharedInstance].managedObjectContext];

    self.categoryListArray = [self.greetingListArray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.catName"]];
    
    if(self.categoryListArray.count>0){
        [self.tblGrettingList reloadData];
        [self.tblGrettingList selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
        [self loadGreetingsData:0];
    }
}

- (void)loadGreetingsData:(NSInteger)index{
    self.categoryDataListArray = [self.greetingListArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"catName = %@",self.categoryListArray[index]]];
    [self.greetingsCollectionView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.categoryListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GreetingCell *cell = (GreetingCell *)[tableView dequeueReusableCellWithIdentifier:@"GreetingCell"];
    cell.lblGreetingName.text = self.categoryListArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self loadGreetingsData:indexPath.row];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if(self.categoryDataListArray.count == 0){
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.text = NSLocalizedString(@"This category is empty.", @"");
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        
        self.greetingsCollectionView.backgroundView = messageLabel;
        return 0;
    }else{
        self.greetingsCollectionView.backgroundView = nil;
        return 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.categoryDataListArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GreetingsCollectionCell *cell = (GreetingsCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"GreetingCollectionCell" forIndexPath:indexPath];
    [cell loadData:self.categoryDataListArray[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GreetingsCollectionCell *cell = (GreetingsCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    TblGreetings *greeting = self.categoryDataListArray[indexPath.row];
    if(self.isBirthday){
        [self shareEmail:cell.imgGreeting.image
               recipient:self.receipt
                fileName:greeting.fileName
                 subject:[NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"E-Greeting Card", @""), greeting.catName]
                     msg:NSLocalizedString(@"You have just received an E-greeting Card", @"")
           callBackBlock:^(BOOL isInstalled){
               if(!isInstalled)
                   [DisplayAlert showAlertInControllerInView:self.parentViewController
                                                       title:NSLocalizedString(@"can not send email,please configure email account!", @"")
                                                     message:@""
                                           cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                               okButtonTitle:@""
                                                 cancelBlock:nil
                                                     okBlock:nil];
           }];
    }else{
        [self showSocialSharingPopoverCtrl:self.contact
                                    EOPObj:nil
                                isGreeting:YES
                                     isEOP:YES
                                    isQRCode:FALSE
                                shareImage:cell.imgGreeting.image
                            shareImageFile:greeting.fileName
                                  shareMsg:NSLocalizedString(@"You have just received an E-greeting Card", @"")
                                  shareSub:[NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"E-Greeting Card", @""), greeting.catName]
                                      rect:cell.bounds
                                    inView:cell];
    }
}

- (IBAction)onBtnDoneClick:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
