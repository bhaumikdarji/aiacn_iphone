//
//  EOPAttendanceViewController.h
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"


@interface EOPAttendanceViewController : AIAChinaViewController
@property (weak, nonatomic) IBOutlet UILabel *lblOrganizer;
@property (weak, nonatomic) IBOutlet UILabel *lblOrganizerValue;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnTopic;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;
@property (weak, nonatomic) IBOutlet UITextView *txtLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblSpeaker;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnEventMaterials;
@property (weak, nonatomic) IBOutlet UIButton *btnAfterEventMaterials;
@property (weak, nonatomic) IBOutlet UIButton *btnTotalCandidate;

@property (strong, nonatomic) NSArray *eopRegisteredCandidateArray;
@property (strong, nonatomic) NSDictionary *currentEOPDic;

@property (strong, nonatomic) NSString *eventCode;
@property (copy, nonatomic) void(^callBackBlock)(BOOL isAttended);
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *EMHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *AEMHeightConstraint;

- (IBAction)onBtnTopicClick:(id)sender;
- (IBAction)onBtnEventMaterialsClick:(id)sender;
- (IBAction)onBtnAfterEventMaterialsClick:(id)sender;
- (IBAction)onBtnTotalCandidateClick:(id)sender;

- (void)loadData:(NSArray *)candidateArray;

@end
