//
//  AddContactViewController.h
//  AIAChina
//
//  Created by AIA on 17/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "ContactDataProvider.h"

@interface AddContactViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UITableView *tblAddContact;

@property (nonatomic, strong) TblContact *contact;
@property (nonatomic, strong) ContactDataProvider *contactDataProvider;
@property (copy, nonatomic) void(^addGroupBlock)();
@property (nonatomic, strong) UIPopoverController *contactPopoverController;
@property (strong, nonatomic) NSArray *groupArray;

@end
