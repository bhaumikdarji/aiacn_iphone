//
//  AssignContactViewController.m
//  AIAChina
//
//  Created by AIA on 31/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AssignContactViewController.h"
#import "AssignContactCell.h"
#import "GroupCell.h"

@interface AssignContactViewController ()
@property (assign,readwrite) BOOL isSelectAll;
@property (weak, nonatomic) IBOutlet UIButton *btnselectAll;
@end

@implementation AssignContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.isContact && self.isDelete){
        self.lblTitle.text = NSLocalizedString(@"Delete Contacts", @"");
    }else if(self.isContact && self.isExport){
        self.lblTitle.text = NSLocalizedString(@"Export Contacts",@"");
    }else if (self.isContact && self.isAssigneeContact){
        self.lblTitle.text = NSLocalizedString(@"Assign Contacts",@"");
    }else if(!self.isContact && self.isDelete){
        self.lblTitle.text = NSLocalizedString(@"Delete Groups",@"");
    }else if(!self.isContact && self.isExport){
        self.lblTitle.text = NSLocalizedString(@"Export Groups",@"");
    }else if (!self.isContact && self.isAssigneeContact){
        self.lblTitle.text = NSLocalizedString(@"Assign Groups",@"");
    }

    

    if(!self.isContact)
       [self setHeader];
    
    if(!self.selectedArray)
        self.selectedArray = [[NSMutableArray alloc]init];
    
    NSPredicate *predict = nil;
    if(self.isContact)
        predict = [NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0", [CachingService sharedInstance].currentUserId];
    else
        predict = [NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0 AND groupName != %@", [CachingService sharedInstance].currentUserId, UNGROUP];
    
    self.contactArray = [[DbUtils fetchAllObject:self.isContact ? @"TblContact" : @"TblGroup"
                                      andPredict:predict
                               andSortDescriptor:[NSSortDescriptor sortDescriptorWithKey: self.isContact ? @"name" : @"groupName" ascending:TRUE]
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    
    for (TblContact *ignorContact in self.ignoreArray) {
        for (TblContact *currentContact in self.contactArray) {
            if([ignorContact.objectID isEqual:currentContact.objectID]){
                [self.contactArray removeObject:currentContact];
                break;
            }
        }
    }
    
    [self.tblContact reloadData];
}

- (void)setHeader{
    self.lblName.text = NSLocalizedString(@"Group Name", @"");
    self.lblGender.hidden = true;
    self.lblContact.hidden =true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.contactArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AssignContactCell *cell = nil;
    if (self.isContact) {
        cell = (AssignContactCell *)[tableView dequeueReusableCellWithIdentifier:@"AssignContactCell"];
        TblContact *contact = [self.contactArray objectAtIndex:indexPath.row];
        [cell loadContactData:contact];
        cell.btnSelect.selected = FALSE;
        
        if(self.isDelete){
            if([contact.candidateProcess.firstInterviewStatus isEqualToString:RECURITMENT_STATUS_TRUE]) {
                cell.btnSelect.hidden = TRUE;
                self.btnselectAll.hidden = YES;
            }
            else {
                cell.btnSelect.hidden = FALSE;
                self.btnselectAll.hidden = NO;
            }
        }
        
        for (TblContact *selectedContact in self.selectedArray) {
            if([contact.objectID isEqual:selectedContact.objectID])
                cell.btnSelect.selected = TRUE;
        }
    }else{
        cell = (AssignContactCell *)[tableView dequeueReusableCellWithIdentifier:@"GroupCell"];
        TblGroup *group = [self.contactArray objectAtIndex:indexPath.row];
        [cell loadGroupData:group];
        cell.btnSelect.selected = FALSE;
        for (TblGroup *selectedGroup in self.selectedArray) {
            if([group.objectID isEqual:selectedGroup.objectID])
                cell.btnSelect.selected = TRUE;
        }
    }    
    [cell.btnSelect addTarget:self action:@selector(onBtnSelectClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnSelect.tag = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!self.isDelete) {
        AssignContactCell *cell = (AssignContactCell *)[tableView cellForRowAtIndexPath:indexPath];
        UIButton *selected = cell.btnSelect;
        [self onBtnSelectClick:selected];
    }
}

- (IBAction)onBtnSelectClick:(UIButton *)sender{

    if (sender.isSelected) {
        [sender setSelected:FALSE];
        [self.selectedArray removeObject:self.contactArray[[sender tag]]];
    }else{
        [sender setSelected:TRUE];
        [self.selectedArray addObject:self.contactArray[[sender tag]]];
    }
}

- (IBAction)onBtnCancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onBtnDoneClick:(id)sender {
    if([self.lblTitle.text isEqualToString:@"Delete Contacts"] || [self.lblTitle.text isEqualToString:@"    "]){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"Are you sure you want to proceed?", @"")
                                cancelButtonTitle:@"Yes"
                                    okButtonTitle:@"No"
                                      cancelBlock:^(UIAlertAction *action) {
                                          if (self.selectedContactBlock) {
                                              self.selectedContactBlock(self.selectedArray);
                                          }
                                          [self dismissViewControllerAnimated:YES completion:nil];
                                      } okBlock:^(UIAlertAction *action) {
                                          
                                      }];
    }else{
        if (self.selectedContactBlock) {
            self.selectedContactBlock(self.selectedArray);
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)onBtnSelectAll:(UIButton *)sender {
    NSLog(@"select All clicked");
    
    if (sender.isSelected) {
        [sender setSelected:FALSE];
        [self.selectedArray removeAllObjects];
    }else{
        [sender setSelected:TRUE];
        [self.selectedArray removeAllObjects];
        //* Prevents deletion of contacts if First Interview has already passed
        if(self.isContact && self.isDelete) {
            for (TblContact *contact in self.contactArray) {
                if(![contact.candidateProcess.firstInterviewStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
                    [self.selectedArray addObject:contact];
                }
            }
        }
        else {
            [self.selectedArray addObjectsFromArray:self.contactArray];
        }
    }
    [self.tblContact reloadData];
}


@end