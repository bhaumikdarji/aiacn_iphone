//
//  SocialSharingViewController.h
//  AIAChina
//
//  Created by MacMini on 24/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AIAChinaViewController.h"

@interface SocialSharingViewController : AIAChinaViewController

@property (strong, nonatomic) NSString *shareImageFile;
@property (strong, nonatomic) UIImage *shareImage;
@property (strong, nonatomic) TblContact *contact;
@property (strong, nonatomic) TblEOP *eop;
@property (nonatomic) BOOL isGreeting;
@property (nonatomic) BOOL isEOP;
@property (nonatomic) BOOL isQRCode;
@property (strong, nonatomic) NSString *shareMsg;
@property (strong, nonatomic) NSString *shareSub;

- (IBAction)onBtnQQClick:(id)sender;
- (IBAction)onBtnWeChatClick:(id)sender;
- (IBAction)onBtnEmailClick:(id)sender;

@end
