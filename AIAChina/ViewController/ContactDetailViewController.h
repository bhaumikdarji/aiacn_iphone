//
//  ContactDetailViewController.h
//  AIAChina
//
//  Created by AIA on 17/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "TblContact.h"

enum STATUS{
    IS_COMPLETED,
    IS_IN_COMPLETE,
    IS_DEFAULT
};

@interface ContactDetailViewController : AIAChinaViewController<UIPopoverControllerDelegate>
{
    UIView *bottomBorder;
}

@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePicture;
@property (weak, nonatomic) IBOutlet UIImageView *imgTalent;

@property (weak, nonatomic) IBOutlet UIButton *btnInteractivePresenter;
@property (weak, nonatomic) IBOutlet UIButton *btnEgreeting;
@property (weak, nonatomic) IBOutlet UIButton *btnNotes;
@property (weak, nonatomic) IBOutlet UIButton *btnCandidateForm;
@property (weak, nonatomic) IBOutlet UIButton *btnResourceCenter;
@property (weak, nonatomic) IBOutlet UILabel *TalentResult;
@property (weak, nonatomic) IBOutlet UILabel *LatestDate;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblTalentScheme;
@property (weak, nonatomic) IBOutlet UILabel *lblLatestUpdate;

@property (weak, nonatomic) IBOutlet UILabel *lblCandidateName;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblDOB;
@property (weak, nonatomic) IBOutlet UILabel *lblSOR;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblGroup;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;

@property (weak, nonatomic) IBOutlet UILabel *lblInteractivePresenterDate;
@property (weak, nonatomic) IBOutlet UILabel *lblEOPRegistrationDate;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstInterviewDate;
@property (weak, nonatomic) IBOutlet UILabel *lblAttendEOPDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCCTestDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyInterviewDate;
@property (weak, nonatomic) IBOutlet UILabel *lblALEExamDate;
@property (weak, nonatomic) IBOutlet UILabel *lblABCTrainingDate;

@property (weak, nonatomic) IBOutlet UILabel *lblContractDate;

@property (weak, nonatomic) IBOutlet UIButton *btnInteractivePres;
@property (weak, nonatomic) IBOutlet UIButton *btnEopReg;
@property (weak, nonatomic) IBOutlet UIButton *btnFirstInterView;
@property (weak, nonatomic) IBOutlet UIButton *btnAttendEOP;
@property (weak, nonatomic) IBOutlet UIButton *btnCCTest;
@property (weak, nonatomic) IBOutlet UIButton *btnCompanyInteview;
@property (weak, nonatomic) IBOutlet UIButton *btnALEExam;
@property (weak, nonatomic) IBOutlet UIButton *btnABCTraining;
@property (weak, nonatomic) IBOutlet UIButton *btnContract;
@property (nonatomic) BOOL isSyncRecruitmentProgress;

@property (strong, nonatomic) NSString *test;
@property (strong, nonatomic) TblContact *contact;
@property (strong, nonatomic) UIPopoverController *contactDetailPopover;

@property (weak, nonatomic) IBOutlet UIButton *btnOverview;
@property (weak, nonatomic) IBOutlet UIButton *btnDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnApplicationForm;
@property (weak, nonatomic) IBOutlet UIButton *btnNote;

- (IBAction)onBtnQQShareClick:(id)sender;
- (IBAction)onBtnWeChatShareClick:(id)sender;
- (IBAction)onBtnEmailShareClick:(id)sender;
- (IBAction)btnInteractivePressClick:(id)sender;

- (IBAction)onBtnInteractivePresClick:(id)sender;
- (IBAction)onBtnEopRegClick:(id)sender;
- (IBAction)onBtnFirstInterviewClick:(id)sender;
- (IBAction)onBtnAttendEOPClick:(id)sender;
- (IBAction)onBtnCCTestClick:(id)sender;
- (IBAction)onBtnCompanyInteviewClick:(id)sender;
- (IBAction)onBtnALEExamClick:(id)sender;
- (IBAction)onBtnABCTrainingClick:(id)sender;
- (IBAction)onBtnContractClick:(id)sender;
- (IBAction)onBtnSendEgreetingClick:(id)sender;

- (IBAction)onBtnShowDetailClick:(id)sender;
- (IBAction)onBtnNotesClick:(id)sender;

-(IBAction)btnOverview:(id)sender;
-(IBAction)btnDetails:(id)sender;
-(IBAction)btnApplicationForms:(id)sender;
-(IBAction)btnNotes:(id)sender;


@end
