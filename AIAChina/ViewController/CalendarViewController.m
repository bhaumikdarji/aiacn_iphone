//
//  CalendarViewController.m
//  AIAChina
//
//  Created by AIA on 27/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CalendarViewController.h"
#import "CreateEventViewController.h"
#import "CalendarEventModel.h"
#import "PickerPopoverViewController.h"

@interface CalendarViewController ()
@property (nonatomic,strong) CalendarEventModel *eventModel;
@property (nonatomic,strong) CachingService *cachingService;

@property (nonatomic, strong) UIPopoverController *popOVer;
@property (weak, nonatomic) IBOutlet UIButton *btnEventTypeChange;
@property (nonatomic, retain) NSString *selectedActivityMode;
@property (nonatomic, retain) NSArray *activityTypeArray;
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic) int calendarIndex;
@property (nonatomic) BOOL isForSearch;


@end

@implementation CalendarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.cachingService = [CachingService sharedInstance];
    if (!self.calendarIndex) {
        self.calendarIndex = 2;
    }
    
    [self.sgmCalType setTitle:NSLocalizedString(@"Day", @"") forSegmentAtIndex:0];
    [self.sgmCalType setTitle:NSLocalizedString(@"Week", @"") forSegmentAtIndex:1];
    [self.sgmCalType setTitle:NSLocalizedString(@"Month", @"") forSegmentAtIndex:2];
    [self.sgmCalType setTitle:NSLocalizedString(@"Year", @"") forSegmentAtIndex:3];
    
    self.activityTypeArray = [[NSArray alloc]initWithObjects:NSLocalizedString(@"All Events", @""),
                              //                                                             NSLocalizedString(@"Holiday", @""),
                              NSLocalizedString(@"NAP Traning", @""),
                              NSLocalizedString(@"Birthday", @""),
                              
                              NSLocalizedString(@"EOP", @""),
                              NSLocalizedString(@"INT", @""),
                              NSLocalizedString(@"iCalendar", @""),
                              nil];
    if (self.eventType) {
        self.selectedActivityMode = NSLocalizedString(@"EOP", @"");
        [self.btnEventTypeChange setTitle:self.selectedActivityMode forState:UIControlStateNormal];
    }
    else {
        self.selectedActivityMode = NSLocalizedString(@"All Events", @"");
    }
    
    self.navigationItem.title = NSLocalizedString(@"Calendar", @"");
    [Utility setBorderWithColor:self.btnToday borderColor:[UIColor colorWithRed:211.0/255.0 green:17.0/255.0 blue:69.0/269.0 alpha:1.0] borderWidth:1.0];
    if(!self.eventArray)
        self.eventArray = [[NSMutableArray alloc] init];
    
    if ([[CMSServiceManager sharedCMSServiceManager] canReachServer]) {
        self.eventModel = [CalendarEventModel sharedInstance];
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [self.eventModel getAllCalendarEvents:^
         {
             [self.eventModel deleteAllCalendarEvents:^{
                 [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
                 if (self.eventType) {
                     self.eventArray = [self addEventArrayWithEventType:self.eventType];
                 }
                 else {
                     self.eventArray = [self addEventArray];
                 }
                 
                 [self setArrayWithEvents:TRUE];
                 [self updateLabelWithMonthAndYear];
             }];
             
         }];
    } else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if (self.eventType)
                self.eventArray = [self addEventArrayWithEventType:self.eventType];
            else
                self.eventArray = [self addEventArray];
            
            [self setArrayWithEvents:TRUE];
            [self updateLabelWithMonthAndYear];
        });
    }
}

- (NSArray *)addiCalendarData{
    
    NSManagedObjectContext *context;
    context = [self.cachingService managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"ICalendarEvents" inManagedObjectContext:context]];
    NSArray *arr = [context executeFetchRequest:fetchRequest error:nil];
    NSManagedObject *matches = nil;
    NSMutableDictionary *temp = [NSMutableDictionary new];
    FFEvent *event = [[FFEvent alloc] init];
    
    NSMutableArray *arrData = [NSMutableArray new];
    for (int i = 0; i < [arr count]; i++) {
        matches = [arr objectAtIndex:i];
        [temp setValue:[matches valueForKey:@"address"] forKey:@"address"];
        [temp setValue:[matches valueForKey:@"agentCode"] forKey:@"agentCode"];
        [temp setValue:[matches valueForKey:@"co"] forKey:@"co"];
        [temp setValue:[matches valueForKey:@"eventDescription"] forKey:@"eventDescription"];
        [temp setValue:[matches valueForKey:@"eventId"] forKey:@"eventId"];
        [temp setValue:[matches valueForKey:@"endTime"] forKey:@"endTime"];
        [temp setValue:[matches valueForKey:@"openFlag"] forKey:@"openFlag"];
        [temp setValue:[matches valueForKey:@"origin"] forKey:@"origin"];
        [temp setValue:[matches valueForKey:@"referenceId"] forKey:@"referenceId"];
        [temp setValue:[matches valueForKey:@"startTime"] forKey:@"startTime"];
        [temp setValue:[matches valueForKey:@"subject"] forKey:@"subject"];
        [temp setValue:[matches valueForKey:@"typeId"] forKey:@"typeId"];
        [temp setValue:[matches valueForKey:@"typeName"] forKey:@"typeName"];
        [temp setValue:[matches valueForKey:@"updateTime"] forKey:@"updateTime"];
        [temp setValue:@"ICalendarEvent" forKey:@"MyEvent"];
        [event setiCalendarEvent:temp];
        [arrData addObject:event];
    }
    return arrData;
}

- (NSMutableArray *)addEventArray{
    NSMutableArray *eventArray = [[NSMutableArray alloc] init];
    
    TblCalendarEvent *calendarEvent = (TblCalendarEvent *)[DbUtils fetchObject:@"TblCalendarEvent"
                                                                    andPredict:[NSPredicate predicateWithFormat:@"agentCode == %@",[CachingService sharedInstance].currentUserId]
                                                             andSortDescriptor:nil
                                                          managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    NSArray *allEOPEventArray;
    NSArray *allInterviewEventArray;
    NSArray *allHolidayEventArray;
    NSArray *trainingCalEventArray;
    
    if (calendarEvent.eopEvents.count > 0) {
         allEOPEventArray = [calendarEvent.eopEvents allObjects];
    }
    
    if (calendarEvent.interviewEvents.count > 0) {
        allInterviewEventArray = [calendarEvent.interviewEvents allObjects];
    }
    
    if (calendarEvent.holidayEvents.count > 0) {
        allHolidayEventArray = [calendarEvent.holidayEvents allObjects];
    }

    if (calendarEvent.trainingEvents.count > 0) {
        trainingCalEventArray = [calendarEvent.trainingEvents allObjects];
    }
    
    NSArray *allManualCalEventArray =  [DbUtils fetchAllObject:@"TblManualEntryCalendarEvents"
                                           andPredict:[NSPredicate predicateWithFormat:@"agentId == %@",[CachingService sharedInstance].currentUserId]
                                    andSortDescriptor:nil
                                 managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    for (TblEOP *EOPObj in allEOPEventArray) {
        FFEvent *event = [[FFEvent alloc] init];
        [event setEOPEvent:EOPObj];
        [eventArray addObject:event];
    }
    
    for (TblInterview *interviewObj in allInterviewEventArray) {
        FFEvent *event = [[FFEvent alloc] init];
        [event setInterviewEvent:interviewObj];
        [eventArray addObject:event];
    }
    
    for (TblHoliday *holidayObj in allHolidayEventArray) {
        FFEvent *event = [[FFEvent alloc] init];
        [event setHolidayEvent:holidayObj];
        [eventArray addObject:event];
    }

    for (TblManualEntryCalendarEvents *calEventObj in allManualCalEventArray) {
        FFEvent *event = [[FFEvent alloc] init];
        [event setCalendarEvent:calEventObj];
        [eventArray addObject:event];
    }
    
    for (TblTraninigEvent *calEventObj in trainingCalEventArray) {
        FFEvent *event = [[FFEvent alloc] init];
        [event setTrainingEvent:calEventObj];
        [eventArray addObject:event];
    }

    return eventArray;
}
- (NSMutableArray *)addEventArrayWithEventType:(NSString *)eventType
{
    NSMutableArray *eventArray = [[NSMutableArray alloc] init];
    
    TblCalendarEvent *calendarEvent = (TblCalendarEvent *)[DbUtils fetchObject:@"TblCalendarEvent"
                                                                    andPredict:[NSPredicate predicateWithFormat:@"agentCode == %@",[CachingService sharedInstance].currentUserId]
                                                             andSortDescriptor:nil
                                                          managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    NSArray *allEOPEventArray;
    NSArray *allInterviewEventArray;
    NSArray *allHolidayEventArray;
    NSArray *trainingCalEventArray;
    
    if (calendarEvent.eopEvents.count > 0) {
        allEOPEventArray = [calendarEvent.eopEvents allObjects];
    }
    
    if (calendarEvent.interviewEvents.count > 0) {
        allInterviewEventArray = [calendarEvent.interviewEvents allObjects];
    }
    
    if (calendarEvent.holidayEvents.count > 0) {
        allHolidayEventArray = [calendarEvent.holidayEvents allObjects];
    }
    
    if (calendarEvent.trainingEvents.count > 0) {
        trainingCalEventArray = [calendarEvent.trainingEvents allObjects];
    }
    
    NSArray *allManualCalEventArray =  [DbUtils fetchAllObject:@"TblManualEntryCalendarEvents"
                                                    andPredict:[NSPredicate predicateWithFormat:@"agentId == %@",[CachingService sharedInstance].currentUserId]
                                             andSortDescriptor:nil
                                          managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    if ([eventType isEqualToString:NSLocalizedString(@"All Events", @"")])
    {
        for (TblEOP *EOPObj in allEOPEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setEOPEvent:EOPObj];
            [eventArray addObject:event];
        }
        
        for (TblInterview *interviewObj in allInterviewEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setInterviewEvent:interviewObj];
            [eventArray addObject:event];
        }
        
        for (TblHoliday *holidayObj in allHolidayEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setHolidayEvent:holidayObj];
            [eventArray addObject:event];
        }
        
        for (TblManualEntryCalendarEvents *calEventObj in allManualCalEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setCalendarEvent:calEventObj];
            [eventArray addObject:event];
        }
        
        for (TblTraninigEvent *calEventObj in trainingCalEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setTrainingEvent:calEventObj];
            [eventArray addObject:event];
        }
    }
    if ([eventType isEqualToString:NSLocalizedString(@"EOP", @"")]) {
        for (TblEOP *EOPObj in allEOPEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setEOPEvent:EOPObj];
            [eventArray addObject:event];
            
        }
        
    }
    if ([eventType isEqualToString:NSLocalizedString(@"INT", @"")]) {
        for (TblInterview *interviewObj in allInterviewEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setInterviewEvent:interviewObj];
            [eventArray addObject:event];
        }
    }
    if ([eventType isEqualToString:NSLocalizedString(@"NAP Traning", @"")]) {
        for (TblTraninigEvent *calEventObj in trainingCalEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setTrainingEvent:calEventObj];
            [eventArray addObject:event];
        }
    }
    if ([eventType isEqualToString:NSLocalizedString(@"Holiday", @"")]) {
        for (TblHoliday *holidayObj in allHolidayEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setHolidayEvent:holidayObj];
            [eventArray addObject:event];
        }
    }
 
    if ([eventType isEqualToString:NSLocalizedString(@"Appointment", @"")] || [eventType isEqualToString:NSLocalizedString(@"Others", @"")] || [eventType isEqualToString:NSLocalizedString(@"Telephone", @"")] || [eventType isEqualToString:NSLocalizedString(@"Participation", @"")]) {
        for (TblManualEntryCalendarEvents *calEventObj in allManualCalEventArray) {
            FFEvent *event = [[FFEvent alloc] init];
            [event setCalendarEvent:calEventObj];
            if ([event.activityType isEqualToString:eventType]) {
                [eventArray addObject:event];
            }
            
            
        }
    }
    
    if (self.isForSearch == YES) {
        NSArray *arr = [DbUtils fetchAllObject:@"ICalendarEvents"
                                    andPredict:[NSPredicate predicateWithFormat:@"agentCode == %@",[CachingService sharedInstance].currentUserIdWithZero]
                             andSortDescriptor:nil
                          managedObjectContext:[CachingService sharedInstance].managedObjectContext];
        
        NSManagedObject *matches = nil;
        NSMutableDictionary *temp = [NSMutableDictionary new];
        FFEvent *event = [[FFEvent alloc] init];
        
        for (int i = 0; i < [arr count]; i++) {
            matches = [arr objectAtIndex:i];
            [temp setValue:[matches valueForKey:@"address"] forKey:@"address"];
            [temp setValue:[matches valueForKey:@"agentCode"] forKey:@"agentCode"];
            [temp setValue:[matches valueForKey:@"co"] forKey:@"co"];
            [temp setValue:[matches valueForKey:@"eventDescription"] forKey:@"eventDescription"];
            [temp setValue:[matches valueForKey:@"eventId"] forKey:@"eventId"];
            [temp setValue:[matches valueForKey:@"endTime"] forKey:@"endTime"];
            [temp setValue:[matches valueForKey:@"openFlag"] forKey:@"openFlag"];
            [temp setValue:[matches valueForKey:@"origin"] forKey:@"origin"];
            [temp setValue:[matches valueForKey:@"referenceId"] forKey:@"referenceId"];
            [temp setValue:[matches valueForKey:@"startTime"] forKey:@"startTime"];
            [temp setValue:[matches valueForKey:@"subject"] forKey:@"subject"];
            [temp setValue:[matches valueForKey:@"typeId"] forKey:@"typeId"];
            [temp setValue:[matches valueForKey:@"typeName"] forKey:@"typeName"];
            [temp setValue:[matches valueForKey:@"updateTime"] forKey:@"updateTime"];
            [temp setValue:@"ICalendarEvent" forKey:@"MyEvent"];
            [event setiCalendarEvent:temp];
            [eventArray addObject:event];
        }
    }
    return eventArray;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationController.navigationBarHidden = FALSE;
    self.navigationItem.leftBarButtonItem = [self prepareLeftBarButton:YES withLogo:NO];
    self.navigationItem.rightBarButtonItem = [self prepareRightBarButton:YES];
    [self.appDelegate showAnnouncements:self.view];
    [self setArrayWithEvents:FALSE];
}

- (void)dateChanged:(NSNotification *)notification {
    [self updateLabelWithMonthAndYear];
}

- (void)updateLabelWithMonthAndYear {
    NSDateComponents *comp = [NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]];
    NSString *month = NSLocalizedString([arrayMonthName objectAtIndex:comp.month -1], @"");
    NSInteger currentMonth = comp.month;
    [self.lblMonthYear setAttributedText:[self setBoldTextOnSingleLable:[NSString stringWithFormat:@"%@ %li", month, (long)comp.year]
                                                               boldFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:25.0]
                                                            regularFont:[UIFont fontWithName:@"HelveticaNeue" size:22.0]
                                                                  range:NSMakeRange(0, [month length])]];
    if(self.currentYear!=comp.year){
        self.currentYear = comp.year;
        [self setNewDictionary:self.dictEvents];
    }else{
        self.currentYear = comp.year;
    }
    if (!self.appDelegate.currentSelectedMonth) {
        [self.appDelegate setCurrentSelectedMonth:currentMonth - 1];
        
    }
    if (self.calendarIndex == 0 || self.calendarIndex == 1 || self.calendarIndex == 2) {
        if (self.appDelegate.currentSelectedMonth != currentMonth) {
            //Dynamic date passing---------------------------------
            NSMutableDictionary *dictTime = [NSMutableDictionary new];
            NSDate *today = [[FFDateManager sharedManager] currentDate];
            NSRange dayRange = [[NSCalendar currentCalendar]
                                rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:today];
            
            
            
            NSMutableDictionary *dictThreeMonthData = [NSMutableDictionary new];
            NSMutableDictionary *dictPastMonthData = [NSMutableDictionary new];
            NSMutableDictionary *dictCurrentMonthData = [NSMutableDictionary new];
            NSMutableDictionary *dictFutureMonthData = [NSMutableDictionary new];
            
            NSInteger dMonth = comp.month;
            NSInteger dYear = comp.year;
            
            if ((dMonth + 1) == 13) {
                dMonth = 1;
                dYear += 1;
                [dictFutureMonthData setValue:[NSString stringWithFormat:@"%ld",(long)dMonth] forKey:@"Month"];
                [dictFutureMonthData setValue:[NSString stringWithFormat:@"%ld",(long)dYear] forKey:@"Year"];
                [dictThreeMonthData setObject:dictFutureMonthData forKey:@"FutureMonthData"];
                dictFutureMonthData = nil;
                
                [dictCurrentMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.month] forKey:@"Month"];
                [dictCurrentMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.year] forKey:@"Year"];
                [dictThreeMonthData setObject:dictCurrentMonthData forKey:@"CurrentMonthData"];
                dictCurrentMonthData = nil;
                
                [dictPastMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.month - 1] forKey:@"Month"];
                [dictPastMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.year] forKey:@"Year"];
                [dictThreeMonthData setObject:dictPastMonthData forKey:@"PastMonthData"];
                dictPastMonthData = nil;
            } else if ((dMonth - 1) == 0) {
                dMonth= 1;
                dYear -= 1;
                [dictFutureMonthData setValue:[NSString stringWithFormat:@"%ld",(long)dMonth + 1] forKey:@"Month"];
                [dictFutureMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.year] forKey:@"Year"];
                [dictThreeMonthData setObject:dictFutureMonthData forKey:@"FutureMonthData"];
                dictFutureMonthData = nil;
                
                dMonth = comp.month -1;
                if (dMonth==0) {
                    dMonth=12;
                }
                [dictCurrentMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.month] forKey:@"Month"];
                [dictCurrentMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.year] forKey:@"Year"];
                [dictThreeMonthData setObject:dictCurrentMonthData forKey:@"CurrentMonthData"];
                dictCurrentMonthData = nil;
                
                [dictPastMonthData setValue:[NSString stringWithFormat:@"%ld",(long)dMonth] forKey:@"Month"];
                [dictPastMonthData setValue:[NSString stringWithFormat:@"%ld",(long)dYear] forKey:@"Year"];
                [dictThreeMonthData setObject:dictPastMonthData forKey:@"PastMonthData"];
                dictPastMonthData = nil;
            } else {
                [dictFutureMonthData setValue:[NSString stringWithFormat:@"%ld",(long)dMonth + 1] forKey:@"Month"];
                [dictFutureMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.year] forKey:@"Year"];
                [dictThreeMonthData setObject:dictFutureMonthData forKey:@"FutureMonthData"];
                dictFutureMonthData = nil;
                
                
                [dictCurrentMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.month] forKey:@"Month"];
                [dictCurrentMonthData setValue:[NSString stringWithFormat:@"%ld",(long)comp.year] forKey:@"Year"];
                [dictThreeMonthData setObject:dictCurrentMonthData forKey:@"CurrentMonthData"];
                dictCurrentMonthData = nil;
                
                [dictPastMonthData setValue:[NSString stringWithFormat:@"%ld",(long)dMonth-1] forKey:@"Month"];
                [dictPastMonthData setValue:[NSString stringWithFormat:@"%ld",(long)dYear] forKey:@"Year"];
                [dictThreeMonthData setObject:dictPastMonthData forKey:@"PastMonthData"];
                dictPastMonthData = nil;
            }
            NSDictionary *dictFuture=[dictThreeMonthData valueForKey:@"FutureMonthData"];
            NSDictionary *dictPast=[dictThreeMonthData valueForKey:@"PastMonthData"];
            
            NSRange dayRange1 = [[NSCalendar currentCalendar]
                                 rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:[NSDate dateWithYear:[[dictFuture valueForKey:@"Year"]intValue] month:[[dictFuture valueForKey:@"Month"] intValue] day:1]];
            NSLog(@"%lu",(unsigned long)dayRange1.length);
            NSString *startDate,*endDate;
            //            if(comp.month > 9){
            //                startDate = [NSString stringWithFormat:@"%d-%d-01 00:00:00",[[dictPast valueForKey:@"Year"] intValue],[[dictPast valueForKey:@"Month"] intValue]];
            //                endDate = [NSString stringWithFormat:@"%d-%d-%ld 23:59:59",[[dictFuture valueForKey:@"Year"] intValue],[[dictFuture valueForKey:@"Month"] intValue],(unsigned long)dayRange1.length];
            //            }else {
            //                startDate = [NSString stringWithFormat:@"%d-0%d-01 00:00:00",[[dictPast valueForKey:@"Year"] intValue],[[dictPast valueForKey:@"Month"] intValue]];
            //                endDate = [NSString stringWithFormat:@"%d-0%d-%ld 23:59:59",[[dictFuture valueForKey:@"Year"] intValue],[[dictFuture valueForKey:@"Month"] intValue],(unsigned long)dayRange1.length];
            //            }
            if ([[dictPast valueForKey:@"Month"]intValue]>9) {
                startDate = [NSString stringWithFormat:@"%d-%d-01 00:00:00",[[dictPast valueForKey:@"Year"] intValue],[[dictPast valueForKey:@"Month"] intValue]];
            } else {
                startDate = [NSString stringWithFormat:@"%d-0%d-01 00:00:00",[[dictPast valueForKey:@"Year"] intValue],[[dictPast valueForKey:@"Month"] intValue]];
            }
            if ([[dictFuture valueForKey:@"Month"]intValue]>9) {
                endDate = [NSString stringWithFormat:@"%d-%d-%ld 23:59:59",[[dictFuture valueForKey:@"Year"] intValue],[[dictFuture valueForKey:@"Month"] intValue],(unsigned long)dayRange1.length];
            } else{
                endDate = [NSString stringWithFormat:@"%d-0%d-%ld 23:59:59",[[dictFuture valueForKey:@"Year"] intValue],[[dictFuture valueForKey:@"Month"] intValue],(unsigned long)dayRange1.length];
            }
            
            NSString *currentYr=[[dictThreeMonthData valueForKey:@"CurrentMonthData"] valueForKey:@"Year"];
            [dictTime setValue:startDate forKey:@"startDate"];
            [dictTime setValue:endDate forKey:@"endDate"];
            [dictTime setValue:currentYr forKey:@"currentYr"];
            [self updateCalendarDataWhenMonthChange:dictTime];
            dictTime = nil;
            [self.appDelegate setCurrentSelectedMonth:currentMonth];
        }
    }
}
- (void)updateCalendarDataWhenMonthChange:(NSDictionary *)timeData{
    NSString *startTime = [timeData valueForKey:@"startDate"];
    NSString *endTime = [timeData valueForKey:@"endDate"];
    int currentYr=[[timeData valueForKey:@"currentYr"]intValue];
    
    if ([[CMSServiceManager sharedCMSServiceManager] canReachServer]) {
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [self.eventModel deletePreviousYearData:currentYr];
        NSDictionary *ob1 = [[NSDictionary alloc]initWithObjectsAndKeys:
                             [CachingService sharedInstance].branchCode,@"co",
                             [NSString stringWithFormat:@"%@",[CachingService sharedInstance].currentUserIdWithZero],@"agentcode",
                             @"-1",@"origin",
                             @"",@"typeId",
                             @"",@"typeName",
                             startTime,@"startTime",
                             endTime,@"endTime",
                             nil];
        
        NSArray *arr = [[NSArray alloc]initWithObjects:ob1,nil];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"2.0.1",@"version",@"Q",@"option",@"03",@"from",arr,@"events",nil];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        jsonString=[jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
        jsonString=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSString *encodedStr=[self.appDelegate md5StringForString:jsonString];
        __block NSMutableArray *dataArray = [NSMutableArray new];
        dataArray = self.eventArray;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self.eventModel getQueriesForAllEvents:nil WithJson:jsonString andEncodedString:encodedStr completion:^(NSDictionary *dict, NSError *err) {
                if (!err) {
                    NSMutableArray *mutarr=[NSMutableArray new];
                    if ([[[dict valueForKey:@"ResponseMessage"] valueForKey:@"events"] count]>0) {
                        NSArray *arrEvent=[[dict valueForKey:@"ResponseMessage"] valueForKey:@"events"];
                        for (int i=0; i<[arrEvent count]; i++) {
                            [mutarr addObject:[arrEvent objectAtIndex:i]];
                        }
                    }
                    
                    NSDictionary *ob2 = [[NSDictionary alloc]initWithObjectsAndKeys:
                                         [CachingService sharedInstance].branchCode,@"co",
                                         [NSString stringWithFormat:@"%@",[CachingService sharedInstance].currentUserIdWithZero],@"agentcode",
                                         @"",@"origin",
                                         @"",@"typeId",
                                         @"",@"typeName",
                                         startTime,@"startTime",
                                         endTime,@"endTime",
                                         nil];
                    
                    NSArray *arr2 = [[NSArray alloc]initWithObjects:ob2,nil];
                    NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"2.0.1",@"version",@"Q",@"option",@"03",@"from",arr2,@"events",nil];
                    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict2 options:NSJSONWritingPrettyPrinted error:nil];
                    NSString *jsonString2;
                    if (! jsonData2) {
                    } else {
                        jsonString2 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
                    }
                    jsonString2=[jsonString2 stringByReplacingOccurrencesOfString:@" " withString:@""];
                    jsonString2=[jsonString2 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    NSString *encodedStr2=[self.appDelegate md5StringForString:jsonString2];
                    [self.eventModel getQueriesForAllEvents:nil WithJson:jsonString2 andEncodedString:encodedStr2 completion:^(NSDictionary *dict1, NSError *err) {
                        if (!err) {
                            if ([[[dict1 valueForKey:@"ResponseMessage"] valueForKey:@"events"] count]>0) {
                                NSArray *arrEvent1=[[dict1 valueForKey:@"ResponseMessage"] valueForKey:@"events"];
                                for (int i=0; i<[arrEvent1 count]; i++) {
                                    [mutarr addObject:[arrEvent1 objectAtIndex:i]];
                                }
                            }
                            [self.eventModel insertEnhancementEventsIntoDatabase:[NSArray arrayWithArray:mutarr]];
                            NSArray *temp = [self addiCalendarData];
                            //                            //Dual entry solved-----------------------------------
                            if ([temp count] > 0) {
                                NSError *er1;
                                NSArray *allManualCalEventArray =  [DbUtils fetchAllObject:@"TblManualEntryCalendarEvents"
                                                                                andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND activityType IN %@",[CachingService sharedInstance].currentUserId, ACTIVITY_ICALENDAR]
                                                                         andSortDescriptor:nil
                                                                      managedObjectContext:[CachingService sharedInstance].managedObjectContext];
                                //
                                if (allManualCalEventArray != nil && allManualCalEventArray.count > 0)
                                {
                                    for (NSManagedObject *obj in allManualCalEventArray) {
                                        [[CachingService sharedInstance].managedObjectContext deleteObject:obj];
                                    }
                                    //
                                    if ([[CachingService sharedInstance].managedObjectContext save:&er1]) {
                                    }
                                }
                                //
                                for (int i=0; i<[self.eventArray count]; i++) {
                                    FFEvent *event=[self.eventArray objectAtIndex:i];
                                    if ([event.activityType isEqualToString:ACTIVITY_ICALENDAR]){
                                        [self.eventArray removeObjectAtIndex:i];
                                    }
                                }
                                [self.eventArray addObjectsFromArray:temp];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self setArrayWithEvents:YES];
                                    [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                });
                            }
                        }
                        else {
                            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                        }
                    }];
                } else {
                    [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                }
                
            }];
        });
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Not able to connect" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}

- (NSAttributedString *)setBoldTextOnSingleLable:(NSString *)text boldFont:(UIFont *)boldFont regularFont:(UIFont *)regularFont range:(NSRange)setRange{
    
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                           regularFont, NSFontAttributeName,
                           nil];
    NSDictionary *subAttrs = [NSDictionary dictionaryWithObjectsAndKeys:
                              boldFont, NSFontAttributeName,
                              nil];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text
                                                                                       attributes:attrs];
    [attributedText setAttributes:subAttrs range:setRange];
    return attributedText;
}

- (void)addCalendar{
    for (UIView *view in [self.calendarView subviews]) {
        [view removeFromSuperview];
    }
    
    [self updateLabelWithMonthAndYear];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dateChanged:) name:DATE_MANAGER_DATE_CHANGED object:nil];
    
    CGRect frame = CGRectMake(0., 0., self.calendarView.frame.size.width, self.calendarView.frame.size.height);
    
    self.viewCalendarMonth = [[FFMonthCalendarView alloc] initWithFrame:frame];
    [self.viewCalendarMonth setProtocol:self];
    [self.viewCalendarMonth setDictEvents:self.dictEvents];
    [self.calendarView addSubview:self.viewCalendarMonth];
    
    self.viewCalendarWeek = [[FFWeekCalendarView alloc] initWithFrame:frame];
    [self.viewCalendarWeek setProtocol:self];
    [self.viewCalendarWeek setDictEvents:self.dictEvents];
    [self.calendarView addSubview:self.viewCalendarWeek];
    
    self.viewCalendarDay = [[FFDayCalendarView alloc] initWithFrame:frame];
    [self.viewCalendarDay setProtocol:self];
    [self.viewCalendarDay setDictEvents:self.dictEvents];
    [self.calendarView addSubview:self.viewCalendarDay];
    
    self.viewCalendarYear = [[FFYearCalendarView alloc] initWithFrame:frame];
    [self.viewCalendarYear setProtocol:self];
    [self.calendarView addSubview:self.viewCalendarYear];
    
    self.arrayCalendars = @[self.viewCalendarDay, self.viewCalendarWeek , self.viewCalendarMonth, self.viewCalendarYear];
    [self onSgmValueChanged:nil];
    [self showCurrentCal];
}

- (void)showCurrentCal{
    NSDate *currentDate = [NSDate dateWithYear:[NSDate componentsOfCurrentDate].year
                                         month:[NSDate componentsOfCurrentDate].month
                                           day:[NSDate componentsOfCurrentDate].day];
    
    [[FFDateManager sharedManager] setCurrentDate:currentDate];
    self.currentYear = currentDate.year;
}
- (IBAction)btnEventTypeChangeAction:(id)sender
{
    [self setPopoverView:self.btnEventTypeChange pickerArray:self.activityTypeArray];
}

- (IBAction)onSgmValueChanged:(id)sender {
    if([self.sgmCalType selectedSegmentIndex] == 0){
        [self.calendarView bringSubviewToFront:[self.arrayCalendars objectAtIndex:0]];
        [self.viewCalendarDay setDictEvents:self.dictEvents];
    } else if([self.sgmCalType selectedSegmentIndex] == 1){
        [self.calendarView bringSubviewToFront:[self.arrayCalendars objectAtIndex:1]];
    } else if([self.sgmCalType selectedSegmentIndex] == 2){
        [self.calendarView bringSubviewToFront:[self.arrayCalendars objectAtIndex:2]];
    } else if([self.sgmCalType selectedSegmentIndex] == 3){
        [self.calendarView bringSubviewToFront:[self.arrayCalendars objectAtIndex:3]];
    }
    [self updateLabelWithMonthAndYear];
}

- (void)showMonthCalendar {
    [self.sgmCalType setSelectedSegmentIndex:2];
    [self onSgmValueChanged:nil];
}

- (IBAction)onBtnTodayClick:(id)sender {
    [self showCurrentCal];
    [[NSNotificationCenter defaultCenter] postNotificationName:DATE_MANAGER_TODAY_DATE
                                                        object:[[FFDateManager sharedManager] currentDate]];
    [[NSNotificationCenter defaultCenter] postNotificationName:DATE_MANAGER_MONTH_DATE_TAP
                                                        object:[[FFDateManager sharedManager] currentDate]];
}

- (void)setArrayWithEvents:(BOOL)isReload{
    self.dictEvents = [NSMutableDictionary new];
    for (FFEvent *event in self.eventArray) {
        self.dictEvents = [event editEvent:event dicEvent:self.dictEvents];
    }
    
    if ([self.selectedActivityMode isEqualToString:NSLocalizedString(@"Birthday", @"")] || [self.selectedActivityMode isEqualToString:NSLocalizedString(@"All Events", @"")] || [self.selectedActivityMode isEqualToString:NSLocalizedString(@"iCalendar", @"")])
    {
        if ([self.selectedActivityMode isEqualToString:NSLocalizedString(@"iCalendar", @"")] && self.isForSearch) {
          [self setYearlyICalendarEvent];
        } else {
            [self setYearlyEvents];
            [self setYearlyICalendarEvent];
        }
    }
    if(isReload){
        [self setNewDictionary:self.dictEvents];
    }else{
        [self addCalendar];
    }
}


- (void)setYearlyEvents{
    NSMutableArray *currentYearBirthdayArray = [self getYearlyEvent:self.currentYear];
    NSMutableDictionary *existingDateDic = [[NSMutableDictionary alloc] init];
    for (FFEvent *event in currentYearBirthdayArray) {
        NSMutableArray *events = [self.dictEvents objectForKey:event.dateDay];
        if(![existingDateDic objectForKey:event.dateDay]){
            [events removeObjectsInArray:[events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"activityType=%@",ACTIVITY_BIRTHDAY]]];
            [existingDateDic setObject:[NSNull null] forKey:event.dateDay];
        }
        self.dictEvents = [event editEvent:event dicEvent:self.dictEvents];
    }
    [existingDateDic removeAllObjects];
}
- (void)setYearlyICalendarEvent{
    NSLog(@"Current year %ld",(long)self.currentYear);
    NSMutableArray *currentYearBirthdayArray = [self getYearlyICalendarEvent:self.currentYear];
    NSLog(@"icalendar array %@",currentYearBirthdayArray);
    NSMutableDictionary *existingDateDic = [[NSMutableDictionary alloc] init];
    for (FFEvent *event in currentYearBirthdayArray) {
        NSMutableArray *events = [self.dictEvents objectForKey:event.dateDay];
        if(![existingDateDic objectForKey:event.dateDay]){
            [events removeObjectsInArray:[events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"activityType=%@",ACTIVITY_ICALENDAR]]];
            [existingDateDic setObject:[NSNull null] forKey:event.dateDay];
        }
        self.dictEvents = [event editEvent:event dicEvent:self.dictEvents];
    }
    [existingDateDic removeAllObjects];
}

- (NSMutableArray *)getYearlyEvent:(NSInteger)year{
    NSMutableArray *yearlyEventArray = [self.yearlyEventDict objectForKey:[NSString stringWithFormat:@"%ld",(long)year]];
    
    if(yearlyEventArray.count<=0){
        yearlyEventArray = [[NSMutableArray alloc] init];
        
        NSArray *birthdayYearlyArray = [ApplicationFunction fetchYearlyBirthdayEvents:year];
        
        for (NSDictionary *birthdayDict in birthdayYearlyArray) {
            NSDate *date = [DateUtils stringToDate:[NSString stringWithFormat:@"%@-%ld",[birthdayDict objectForKey:@"birthMonthDay"],(long)self.currentYear] dateFormat:@"dd-MMM-yyyy"];
            if(date){
                NSDictionary *eventDic = @{@"eventName" : [NSString stringWithFormat:@"%ld %@",(long)[[birthdayDict objectForKey:@"count"] integerValue],NSLocalizedString(@"Contact Birthday", nil)],
                                           @"activityType" : ACTIVITY_BIRTHDAY,
                                           @"startDate" : [NSDate dateToString:date andFormate:kDateFormatte],
                                           @"endDate" : [NSDate dateToString:date andFormate:kDateFormatte],
                                           @"allDay": @"yes",
                                           };
                FFEvent *event = [[FFEvent alloc] init];
                [event createEvent:eventDic];
                [yearlyEventArray addObject:event];
            }
        }
    
        if(yearlyEventArray.count>0){
            if(!self.yearlyEventDict)
                self.yearlyEventDict = [[NSMutableDictionary alloc] init];
            [self.yearlyEventDict setObject:yearlyEventArray forKey:[NSString stringWithFormat:@"%ld",(long)year]];
        }
    }
    return yearlyEventArray;
}
- (NSMutableArray *)getYearlyICalendarEvent:(NSInteger)year{
    NSMutableArray *yearlyEventArray = [self.yearlyICalendarEventDict objectForKey:[NSString stringWithFormat:@"%ld",(long)year]];
    
    if(yearlyEventArray.count<=0){
        yearlyEventArray = [[NSMutableArray alloc] init];
        
        NSArray *iCalendarYearlyArray = [ApplicationFunction fetchICalendarEvents:year];
        
        NSLog(@"iCalendarYearlyArray is %@",iCalendarYearlyArray);
        
        for (NSDictionary *testDict in iCalendarYearlyArray) {
            //
            
            
            NSDictionary *eventDic = @{@"MyEvent" :[NSString stringWithFormat:@"%ld %@",(long)[[testDict objectForKey:@"count"]
                                                                                               integerValue],@"icalendar"],
                                       @"startTime":[testDict valueForKey:@"st_dt"],
                                       @"endTime":[testDict valueForKey:@"st_dt"],
                                       };
            
            
            FFEvent *event = [[FFEvent alloc] init];
            [event setiCalendarEvent:eventDic];
            [yearlyEventArray addObject:event];
            // }
        }
        
        if(yearlyEventArray.count>0){
            if(!self.yearlyICalendarEventDict)
                self.yearlyICalendarEventDict = [[NSMutableDictionary alloc] init];
            [self.yearlyICalendarEventDict setObject:yearlyEventArray forKey:[NSString stringWithFormat:@"%ld",(long)year]];
        }
    }
    return yearlyEventArray;
}



- (IBAction)onBtnCreateEventClick:(id)sender {
    
    [self.viewCalendarDay removeEditEventView];
    
    CreateEventViewController *createEventPopoverView = [[CreateEventViewController alloc] initWithNibName:@"CreateEventViewController" bundle:nil];
    [createEventPopoverView setCreateEvent:^(NSDictionary *eventDic, MBProgressHUDUpd *hud) {
        FFEvent *event = [[FFEvent alloc] init];
        [event createEvent:eventDic];
        [self addNewEvent:event];
        [hud hide:TRUE];
        [self.eventPopoverController dismissPopoverAnimated:TRUE];
    }];
    
    [createEventPopoverView setCancelEvent:^(UIView *view) {
        [self.eventPopoverController dismissPopoverAnimated:TRUE];
    }];
    
    self.eventPopoverController = [[UIPopoverController alloc] initWithContentViewController:createEventPopoverView];
    self.eventPopoverController.popoverContentSize = createEventPopoverView.view.frame.size;
    self.eventPopoverController.backgroundColor = createEventPopoverView.view.backgroundColor;
    
    [self.eventPopoverController presentPopoverFromRect:self.btnCreateEvent.frame
                                                 inView:self.view
                               permittedArrowDirections:UIPopoverArrowDirectionUp
                                               animated:YES];
}
- (void)setPopoverView:(UIButton *)btn pickerArray:(NSArray *)pickerArray{
    NSMutableArray *pickerDataArray = [[NSMutableArray alloc] initWithArray:pickerArray];
    
    PickerPopoverViewController *popoverViewCtrl = [[PickerPopoverViewController alloc] initWithNibName:@"PickerPopoverViewController" bundle:nil];
    
    if(isEmptyString(btn.titleLabel.text))
        [btn setTitle:pickerDataArray[0] forState:UIControlStateNormal];
    else
        popoverViewCtrl.oldValue = btn.titleLabel.text;
    
    [popoverViewCtrl setPickerArray:pickerDataArray];
    
    __weak typeof(self) weakSelf = self;
    [popoverViewCtrl setSelectedIndex:^(int selectedRowIndexPath) {
        NSString *value = pickerDataArray[selectedRowIndexPath];
        [btn setTitle:value forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        weakSelf.selectedActivityMode = value;
        if ([value isEqualToString:NSLocalizedString(@"iCalendar",@"")]) {
            self.isForSearch = YES;
            self.eventArray = [self addEventArrayWithEventType:value];
            [self.appDelegate setCurrentSelectedMonth:self.appDelegate.currentSelectedMonth - 1];
            [self updateLabelWithMonthAndYear];
        } else {
            self.isForSearch = NO;
            self.eventArray = [self addEventArrayWithEventType:value];
        }
        [self setArrayWithEvents:TRUE];
        [weakSelf.popOVer dismissPopoverAnimated:TRUE];
    }];
    
    self.popOVer = [[UIPopoverController alloc] initWithContentViewController:popoverViewCtrl];
    
    self.popOVer.popoverContentSize = CGSizeMake(250, 200);
    
    [self.popOVer presentPopoverFromRect:btn.frame
                                  inView:self.calendarView
     
                permittedArrowDirections:UIPopoverArrowDirectionUp
                                animated:TRUE];
}
- (void)addNewEvent:(FFEvent *)eventNew {
    self.dictEvents = [eventNew editEvent:eventNew dicEvent:self.dictEvents];
    [self setNewDictionary:self.dictEvents];
}

- (void)setNewDictionary:(NSDictionary *)dict {
    
    self.dictEvents = (NSMutableDictionary *)dict;
    
    [self.viewCalendarMonth setDictEvents:self.dictEvents];
    [self.viewCalendarWeek setDictEvents:self.dictEvents];
    [self.viewCalendarDay setDictEvents:self.dictEvents];
    
    [self arrayUpdatedWithAllEvents];
}

- (void)arrayUpdatedWithAllEvents {
    
    NSMutableArray *arrayNew = [NSMutableArray new];
    
    NSArray *arrayKeys = self.dictEvents.allKeys;
    for (NSDate *date in arrayKeys) {
        NSArray *arrayOfDate = [self.dictEvents objectForKey:date];
        for (FFEvent *event in arrayOfDate) {
            [arrayNew addObject:event];
        }
    }
    if (self.protocol != nil && [self.protocol respondsToSelector:@selector(arrayUpdatedWithAllEvents:)]) {
        [self.protocol arrayUpdatedWithAllEvents:arrayNew];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
