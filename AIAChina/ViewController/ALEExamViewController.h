//
//  ALEExamViewController.h
//  AIAChina
//
//  Created by MacMini on 07/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALEExamViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@property (strong, nonatomic) NSString *popTitle;
@property (strong, nonatomic) NSString *popDescription;

@end
