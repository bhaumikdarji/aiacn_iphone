//
//  BirthDayViewController.m
//  AIAChina
//
//  Created by SCS2 on 09/09/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "BirthDayViewController.h"
#import "BirthDayCell.h"

@interface BirthDayViewController ()

@end

@implementation BirthDayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectedContactArray = [[NSMutableArray alloc] init];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadData{
    self.birthDayArray = [DbUtils fetchAllObject:@"TblContact"
                                      andPredict:[NSPredicate predicateWithFormat:@"birthYear <=%d AND birthMonthDay.length>0 AND birthMonthDay=%@ AND agentId == %@ AND isDelete == 0",self.birthDate.year,[DateUtils dateToString:self.birthDate andFormate:@"dd-MMM"],[CachingService sharedInstance].currentUserId]
                               andSortDescriptor:nil
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    [self.tblBirthDay reloadData];
}

- (IBAction)onBtnCancelClick:(id)sender {
    if(self.cancelBirthDayView)
        self.cancelBirthDayView();
}

- (IBAction)onBtnSendEGreetingClick:(id)sender {
    if(self.sendEGreeting){
        NSMutableArray *emailArray = [[NSMutableArray alloc] init];
        for (TblContact *contact in self.selectedContactArray) {
            if(!isEmptyString(contact.eMailId))
                [emailArray addObject:contact.eMailId];
        }
        
        if(emailArray.count>0){
            self.sendEGreeting(emailArray);
        }else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                                message:NSLocalizedString(@"No email address found", @"")
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                      otherButtonTitles: nil];
            [alertView show];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.birthDayArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BirthDayCell *cell = (BirthDayCell *)[tableView dequeueReusableCellWithIdentifier:@"BirthDayCell"];
    TblContact *contact = self.birthDayArray[indexPath.row];
    [cell loadBirthDayCell:contact];
    cell.btnCheckBox.tag = indexPath.row;
    [cell.btnCheckBox addTarget:self action:@selector(onBtnCheckBoxClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btnCheckBox setSelected:FALSE];
    
    for (TblContact *selectedContact in self.selectedContactArray) {
        if([contact.objectID isEqual:selectedContact.objectID]){
            [cell.btnCheckBox setSelected:TRUE];
            break;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BirthDayCell *cell = (BirthDayCell *)[tableView cellForRowAtIndexPath:indexPath];
    UIButton *selected = cell.btnCheckBox;
    [self onBtnCheckBoxClick:selected];
}

- (IBAction)onBtnCheckBoxClick:(UIButton *)sender{
    TblContact *contact = self.birthDayArray[[sender tag]];
    if(sender.isSelected){
        [sender setSelected:NO];
        [self.selectedContactArray removeObject:contact];
    }else{
        [sender setSelected:YES];
        [self.selectedContactArray addObject:contact];
    }
    [self.tblBirthDay reloadData];
}

@end
