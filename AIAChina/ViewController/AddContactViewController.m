//
//  AddContactViewController.m
//  AIAChina
//
//  Created by AIA on 17/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AddContactViewController.h"
#import "AddContactCell.h"
#import "CachingService.h"
#import "DropDownManager.h"
#import "SyncModel.h"
#import "AddressPickerViewController.h"

#define REG_ADD_TAG 111
#define RES_ADD_TAG 222

enum candidateSection{
    PERSONAL_DETAIL_SECTION,
    CONTACT_INFO_SECTION,
    OCCUPATION_INFO_SECTION,
    REMARK_SECTION
};

enum addContactCellTag{
    PERSONAL_DETAIL_TAG = 110,
    CONTACT_INFO_TAG = 120,
    OCCUPATION_INFO_TAG = 130,
    REMARK_TAG = 140
};

@interface AddContactViewController ()
@property (nonatomic, retain) SyncModel *syncObj;
@property (nonatomic, retain) AFNetworkReachabilityManager *reachabilityManager;
@end

@implementation AddContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    self.navigationItem.title = NSLocalizedString(@"Candidate's Detail", @"");
    self.contactDataProvider = [ContactDataProvider sharedContactDataProvider];
    if(self.contact)
        self.groupArray = [[self.contact.group allObjects] mutableCopy];
    else
        self.groupArray = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addGroup:)
                                                 name:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                                  object:nil];
    
}

- (void)addGroup:(NSNotification *)notifi{
    [self.navigationController popToRootViewControllerAnimated:TRUE];
    if(self.addGroupBlock)
        self.addGroupBlock();
}

- (void) hideKeyboard{
    [self.view endEditing:true];
}

- (void)keyboardWillShow:(NSNotification *)notification{
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, 450, 0);
    [self.tblAddContact setContentInset:edgeInsets];
    [self.tblAddContact setScrollIndicatorInsets:edgeInsets];
}

- (void)keyboardWillHide:(NSNotification *)notifi{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tblAddContact.contentInset = contentInsets;
    self.tblAddContact.scrollIndicatorInsets = contentInsets;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] initWithFrame:tableView.tableHeaderView.frame];
    headerView.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
    
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(22, 0, tableView.frame.size.width, 35)];
    lblHeader.backgroundColor = [UIColor clearColor];
    lblHeader.textColor = [UIColor blackColor];
    lblHeader.font = [UIFont fontWithName:@"Arial" size:15];
    [headerView addSubview:lblHeader];
    
    NSString *strTitle = @"";
    NSString *strDesc = @"";
    NSString *strHeader = @"";
    
    if(section == PERSONAL_DETAIL_SECTION){
        strTitle = NSLocalizedString(@"Personal Info",@"");
        strDesc = NSLocalizedString(@"Unique QR Code will be generated with name, gender and DOB", @"");
        strHeader = [NSString stringWithFormat:@"%@ %@",strTitle, strDesc];
    }else if(section == CONTACT_INFO_SECTION){
        strTitle = NSLocalizedString(@"Complete Details",@"");
        strDesc = NSLocalizedString(@"The fields below are not mandatory, but it will help in search and can be reused in other parts of the application", @"");
        strHeader = [NSString stringWithFormat:@"%@ %@",strTitle, strDesc];
    }
    [lblHeader setAttributedText:[self setHeaderText:strHeader
                                           smallFont:[UIFont fontWithName:@"Arial" size:11]
                                         defaultFont:[UIFont fontWithName:@"Arial" size:15]
                                               range:[strHeader rangeOfString:strDesc]]];
    return headerView;
}

- (NSAttributedString *)setHeaderText:(NSString *)text
                            smallFont:(UIFont *)smallFont
                          defaultFont:(UIFont *)regularFont
                                range:(NSRange)setRange{
    
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                           regularFont, NSFontAttributeName,
                           [UIColor colorWithRed:105.0/255.0 green:105.0/255.0 blue:105.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                           nil];
    
    NSDictionary *smallAttrs = [NSDictionary dictionaryWithObjectsAndKeys:
                               smallFont, NSFontAttributeName,
                               [UIColor redColor], NSForegroundColorAttributeName,
                               nil];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text
                                                                                       attributes:attrs];
    [attributedText setAttributes:smallAttrs range:setRange];
    
    return attributedText;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == PERSONAL_DETAIL_SECTION){
        return 110.0f;
    }else if(indexPath.section == CONTACT_INFO_SECTION){
        return 250.0f;
    }if(indexPath.section == OCCUPATION_INFO_SECTION){
        return 220.0f;
    }else if(indexPath.section == REMARK_SECTION){
        return 45.0f;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddContactCell *cell = nil;
    if(indexPath.section == PERSONAL_DETAIL_SECTION){
        cell = (AddContactCell *)[tableView dequeueReusableCellWithIdentifier:@"PersonalInfo"];
        cell.contact = self.contact;
        [cell loadPersonalInfo];
        [cell.btnDOB addTarget:self action:@selector(onBtnDOBClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnGroup addTarget:self action:@selector(loadGroup:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnProfilePic addTarget:self action:@selector(showPhotoPicker:) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.section == CONTACT_INFO_SECTION){
        cell = (AddContactCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactInfo"];
        cell.contact = self.contact;
        [cell loadContactInfo];
        [cell.btnResidentAddress1 addTarget:self action:@selector(onBtnAddress1Click:) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.section == OCCUPATION_INFO_SECTION){
        cell = (AddContactCell *)[tableView dequeueReusableCellWithIdentifier:@"OccupationInfo"];
        cell.contact = self.contact;
        [cell loadOccupationInfo];
    }else if(indexPath.section == REMARK_SECTION){
        cell = (AddContactCell *)[tableView dequeueReusableCellWithIdentifier:@"RemarkInfo"];
        cell.contact = self.contact;
        [cell loadRemarkInfo];
    }
    cell.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:243.0/255.0 alpha:1.0];
    return cell;
}

- (IBAction)onBtnAddress1Click:(UIButton *)sender {
    NSIndexPath *contactIndex = [NSIndexPath indexPathForRow:0 inSection:CONTACT_INFO_SECTION];
    AddContactCell *cell = (AddContactCell *)[self.tblAddContact cellForRowAtIndexPath:contactIndex];
    [self showAddressPickerViewController:[MyAppDelegate window].rootViewController.view.bounds
                                   inView:[MyAppDelegate window].rootViewController.view
                            callBackBlock:^(TblAddress *address) {
                                if(sender.tag == REG_ADD_TAG){
                                    self.contact.registeredAddress = address.rgsName;
                                    self.contact.registeredAddress2 = [address.rgCodeC stringValue];
                                }else{
                                    [cell.btnResidentAddress1 setTitle:address.rgsName forState:UIControlStateNormal];
                                    self.contact.residentialAddress = address.rgsName;
                                    self.contact.residentialAddress2 = [address.rgCodeC stringValue];
                                }
                            }];
}

- (IBAction)onBtnDOBClick:(id)sender {
    NSIndexPath *contactIndex = [NSIndexPath indexPathForRow:0 inSection:0];
    AddContactCell *cell = (AddContactCell *)[self.tblAddContact cellForRowAtIndexPath:contactIndex];
    [self showDatePopoverCtrl:UIDatePickerModeDate
                  minimumDate:nil
                  maximumDate:[NSDate date]
                         rect:cell.btnDOB.frame
                       inView:cell
                   pickerDate:^(NSDate *pickerDate) {
                       [cell.btnDOB setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:kDisplayDateFormatte]] forState:UIControlStateNormal];
                       cell.lblAge.text = [NSString stringWithFormat:@"%ld",(long)[DateUtils ageFromBirthday:pickerDate]];
                       if(self.contact){
                           self.contact.birthDate = [DateUtils stringToDate:cell.btnDOB.currentTitle dateFormat:kDisplayDateFormatte];
                           self.contact.age = [NSNumber numberWithInteger:[cell.lblAge.text integerValue]];
                       }
                   }];
}

- (IBAction)loadGroup:(id)sender{
    [self showAssignPopoverCtrl:nil
              withSelectedArray:self.groupArray
                      isContact:FALSE
                       isDelete:FALSE
                       isExport:FALSE
              isAssigneeContact:TRUE
                           rect:self.view.frame
                         inView:self.view
                selectedContact:^(NSArray *selectedArray) {
                    self.groupArray = [selectedArray mutableCopy];
                    NSMutableArray *selectedGroupArray = [[NSMutableArray alloc] init];
                    for (TblGroup *group in selectedArray) {
                        if(![group.groupName isEqualToString:UNGROUP])
                            [selectedGroupArray addObject:group.groupName];
                    }
                    [sender setTitle:[selectedGroupArray componentsJoinedByString:@","] forState:UIControlStateNormal];
                    if(self.contact){
                        [self.contact removeGroup:self.contact.group];
                        for (TblGroup *group in self.groupArray) {
                            [self.contact addGroupObject:group];
                        }
                    }
                }];
}

- (IBAction)showPhotoPicker:(id)sender{
    [self showPhotoActionSheet:sender withCircularCrop1:YES];
}

- (void)handleImagePicker:(UIImagePickerController *)thePicker withMediaInfo:(NSDictionary *)imageInfo
{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil) {
        
       UIImage *profileImage = imageInfo[UIImagePickerControllerEditedImage];
        if (!profileImage) profileImage = imageInfo[UIImagePickerControllerOriginalImage];
        profileImage = [self resizeImage:profileImage];
        NSIndexPath *personalIndex = [NSIndexPath indexPathForRow:0 inSection:0];
        AddContactCell *cell = (AddContactCell *)[self.tblAddContact cellForRowAtIndexPath:personalIndex];
        cell.imgProfilePic.image = profileImage;
        if(self.contact)
            self.contact.candidatePhoto = UIImagePNGRepresentation(cell.imgProfilePic.image);
        
        [cell.btnProfilePic setImage:nil forState:UIControlStateNormal];
    }
}

- (void) imagePickerController:(UIImagePickerController *)thePicker didFinishPickingMediaWithInfo:(NSDictionary *)imageInfo{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil) {
        UIImage *profileImage = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
        NSIndexPath *personalIndex = [NSIndexPath indexPathForRow:0 inSection:0];
        AddContactCell *cell = (AddContactCell *)[self.tblAddContact cellForRowAtIndexPath:personalIndex];
        cell.imgProfilePic.image = profileImage;
        if(self.contact)
            self.contact.candidatePhoto = UIImagePNGRepresentation(cell.imgProfilePic.image);
        
        [cell.btnProfilePic setImage:nil forState:UIControlStateNormal];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onBtnCancelClick:(id)sender {
    if(self.contact)
        [[CachingService sharedInstance].managedObjectContext refreshObject:self.contact mergeChanges:FALSE];
    
    NSArray *contactArray = [DbUtils fetchAllObject:@"TblContact"
                                      andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0", [CachingService sharedInstance].currentUserId]
                               andSortDescriptor:nil
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    if(contactArray.count == 0){
        [ApplicationFunction showToAgentProfile:NO];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:CONTACT_LIST_RELOAD_NOTIFICATION
                                                            object:self.contact];
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

- (IBAction)onBtnSaveClick:(id)sender {
    if([self validate]){
        [self saveLocalDatabase];
        [self synContactCall];
    }
}

- (void)synContactCall
{
    self.reachabilityManager = [AFNetworkReachabilityManager sharedManager];
    if (self.reachabilityManager.reachable)
    {
        self.syncObj = [[SyncModel alloc]init];
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [self.syncObj syncObjectsIntoDatabase:[CachingService sharedInstance].currentUserId success:^{
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:CONTACT_LIST_RELOAD_NOTIFICATION
                                                                object:self.contact];
            [self.navigationController popViewControllerAnimated:TRUE];
            
        }failure:^{
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];

        }];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:CONTACT_LIST_RELOAD_NOTIFICATION
                                                            object:self.contact];
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

- (void)saveLocalDatabase{
    if(!self.contact)
    {
        self.contact = [self.contactDataProvider createContact];
        self.contact.agentId = [CachingService sharedInstance].currentUserId;
        self.contact.iosAddressCode = [NSString stringWithFormat:@"%@",[[self.contact objectID] URIRepresentation]];
        self.contact.isDelete = [NSNumber numberWithBool:FALSE];
        NSDate * creationDate = [NSDate dateWithTimeIntervalSince1970:[[NSDate date] timeIntervalSince1970]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kServerDateFormatte];
        NSString * strdate = [formatter stringFromDate:creationDate];
        
        self.contact.creationDate = strdate;
    }
    
    AddContactCell *cell = nil;
    
    cell = (AddContactCell *)[self.view viewWithTag:PERSONAL_DETAIL_TAG];
    self.contact.isDelete = [NSNumber numberWithBool:FALSE];
    NSDate * modificationDate = [NSDate dateWithTimeIntervalSince1970:[[NSDate date] timeIntervalSince1970]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:kServerDateFormatte];
    NSString * strdate = [formatter stringFromDate:modificationDate];
    NSDate *newmodificationDate = [formatter dateFromString:strdate];
    self.contact.modificationDate = newmodificationDate;
    if(cell.imgProfilePic.image)
        self.contact.candidatePhoto = UIImagePNGRepresentation(cell.imgProfilePic.image);
    
    self.contact.name = cell.txtName.text;
    self.contact.gender = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_GENDER description:cell.btnGender.currentTitle];
    self.contact.birthDate = [DateUtils stringToDate:cell.btnDOB.currentTitle dateFormat:kDisplayDateFormatte];
    self.contact.referalSource = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_SOURCE_OF_REFERRAL description:cell.btnSOR.currentTitle];
    self.contact.age = [NSNumber numberWithInteger:[cell.lblAge.text integerValue]];
    
    if(self.contact.birthDate){
        self.contact.birthMonthDay = [DateUtils dateToString:self.contact.birthDate andFormate:@"dd-MMM"];
        self.contact.birthYear = [NSNumber numberWithInteger:[self.contact.birthDate year]];
    }

    cell = (AddContactCell *)[self.view viewWithTag:CONTACT_INFO_TAG];
    self.contact.residentialAddress = cell.btnResidentAddress1.currentTitle;
    self.contact.residentialAddress1 = cell.txtResidentAddress2.text;
    self.contact.nric = cell.txtNric.text;
    self.contact.eMailId = cell.txtEmail.text;
    self.contact.qq = cell.txtQQId.text;
    self.contact.weChat = cell.txtWeChat.text;
    self.contact.mobilePhoneNo = cell.txtMobile.text;
    self.contact.fixedLineNO = cell.txtFixedLine.text;
    self.contact.officePhoneNo = cell.txtOffice.text;
    self.contact.identityType = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_IDENTITY_TYPE description:cell.btnIdentityType.currentTitle];
    self.contact.newcomerSource = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_NEWCOMER_SOURCE description:cell.btnNewComerSource.currentTitle];
    
    cell = (AddContactCell *)[self.view viewWithTag:OCCUPATION_INFO_TAG];
    self.contact.marritalStatus = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_MARITAL_STATUS description:cell.btnMaritalStatus.currentTitle];
    self.contact.race = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_RACE description:cell.btnRace.currentTitle];
    self.contact.education = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_EDUCATION description:cell.btnEducation.currentTitle];
    self.contact.talentProfile = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_TALENT_PROFILE description:cell.btnTalentProfile.currentTitle];
    self.contact.reJoin = cell.btnRejoin.currentTitle;
    self.contact.recommendedRecruitmentScheme = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_RECOMMEND_SCHEME description:cell.btnRecommend.currentTitle];
    self.contact.workingYearExperience = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_WORK_EXPERIENCE description:cell.btnWorkExperience.currentTitle];
    self.contact.yearlyIncome = cell.txtAnnualIncome.text;
    self.contact.presentWorkCondition = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_PRESENT_EMPLOYMENT_CONDITION description:cell.btnPresentEmpCond.currentTitle];
    self.contact.purchasedAnyInsurance = cell.btnPurInsBefore.currentTitle;
    self.contact.salesExperience = cell.btnSaleExp.currentTitle;
    self.contact.natureOfBusiness = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_NATURE_OF_BUSINESS description:cell.btnNatureBusiness.currentTitle];
    
    cell = (AddContactCell *)[self.view viewWithTag:REMARK_TAG];;
    self.contact.remarks = cell.txtRemark.text;
    
    [self.contact removeGroup:self.contact.group];
    for (TblGroup *group in self.groupArray) {
        [self.contact addGroupObject:group];
    }
    
    if([self.contact.group count] == 0)
        [ApplicationFunction joinUngroup:self.contact];

    
    self.contact.isSync = [NSNumber numberWithBool:YES];
    [self.contactDataProvider saveLocalDBData];
}

- (BOOL)validate{
    NSMutableString *msg = [NSMutableString string];
    AddContactCell *cell = nil;
    cell = (AddContactCell *)[self.view viewWithTag:PERSONAL_DETAIL_TAG];
    if(isEmptyString(cell.txtName.text)) {
        [msg appendFormat:@"%@\r\n\r\n",NSLocalizedString(@"Please enter name", @"")];
    }
    else if(isEmptyString(cell.btnSOR.titleLabel.text)) {
        [msg appendFormat:@"%@\r\n\r\n",NSLocalizedString(@"Please select source of referal", @"")];
    }
    
    cell = (AddContactCell *)[self.view viewWithTag:CONTACT_INFO_TAG];
    if(!isEmptyString(cell.txtEmail.text)){
        if(![Utility validateEmail:cell.txtEmail.text])
            [msg appendFormat:@"%@\r\n\r\n",NSLocalizedString(@"Please enter valid email", @"")];
    }
    
    if(!isEmptyString(cell.txtResidentAddress2.text)){
        if([cell.txtResidentAddress2.text length] < 3 || ([cell.txtResidentAddress2.text length] + [[cell.btnResidentAddress1 currentTitle] length]) > 28)
            [msg appendFormat:@"%@\r\n\r\n",NSLocalizedString(@"Address line should be minimum 3 characters each and should not exceed 28 characters in total!", @"")];
        else if([cell.txtResidentAddress2.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location == NSNotFound){
            [msg appendFormat:@"%@\r\n\r\n",NSLocalizedString(@"Address line should contain at least a number", @"")];
        }
    }
    
    if(!isEmptyString(cell.txtNric.text)){
        if(!(cell.txtNric.text.length == NRIC_MAX_LENGTH) && !(cell.txtNric.text.length == NRIC_MIN_LENGTH))
            [msg appendFormat:@"%@\r\n\r\n",NSLocalizedString(@"Please enter valid nric", @"")];
    }
    
    if(!isEmptyString(cell.txtMobile.text)){
        if(![Utility isNumeric:cell.txtMobile.text])
            [msg appendFormat:@"%@\r\n\r\n",NSLocalizedString(@"Please enter only number in mobile", @"")];
        else if(!(cell.txtMobile.text.length == MOBILE_LENGTH))
            [msg appendFormat:@"%@\r\n\r\n",NSLocalizedString(@"Please enter must be 11 digit mobile number", @"")];
    }
    
    if(!isEmptyString(cell.txtFixedLine.text)){
        if(![Utility isNumeric:cell.txtFixedLine.text])
            [msg appendFormat:@"%@\r\n\r\n",NSLocalizedString(@"Please enter only number in fixed line", @"")];
    }

    if(!isEmptyString(msg)){
        [DisplayAlert showAlertInControllerWithTitle:@""
                                             message:msg
                                   cancelButtonTitle:nil
                                       okButtonTitle:NSLocalizedString(@"OK", @"")
                                         cancelBlock:^(UIAlertAction *action) {
                                         } okBlock:^(UIAlertAction *action) {
                                         }];
        return FALSE;
    }
    return TRUE;
}

@end
