//
//  GreetingsViewController.h
//  imo
//
//  Created by Smitesh Patel on 2014-11-20.
//  Copyright (c) 2014 iMO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface GreetingsViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UITableView *tblGrettingList;
@property (nonatomic, strong) IBOutlet UICollectionView *greetingsCollectionView;

@property (strong, nonatomic) TblContact *contact;
@property (strong, nonatomic) NSArray *greetingListArray;
@property (strong, nonatomic) NSArray *categoryListArray;
@property (strong, nonatomic) NSArray *categoryDataListArray;

@property (nonatomic, strong) NSArray *receipt;
@property (nonatomic) BOOL isBirthday;

- (IBAction)onBtnDoneClick:(id)sender;

@end
