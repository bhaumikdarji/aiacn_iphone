//
//  GroupViewController.h
//  AIAChina
//
//  Created by AIA on 20/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssignContactViewController.h"
#import "ContactDataProvider.h"

@interface GroupViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTotalGroupMember;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnCancelDelete;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSaveEdit;
@property (weak, nonatomic) IBOutlet UIImageView *imgGroup;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPhoto;
@property (weak, nonatomic) IBOutlet UIView *profileImageView;
@property (weak, nonatomic) IBOutlet UITextField *txtGroupName;
@property (weak, nonatomic) IBOutlet UITextField *txtDescription;
@property (weak, nonatomic) IBOutlet UIView *editProfileView;
@property (weak, nonatomic) IBOutlet UITableView *tblGroup;
@property (weak, nonatomic) IBOutlet UIButton *btnAddContact;

@property (nonatomic) BOOL isEditGroup;
@property (strong, nonatomic) TblGroup *group;
@property (strong, nonatomic) NSMutableArray *grpContactArray;
@property (strong, nonatomic) NSArray *originalAssContactArray;

@property (nonatomic, strong) ContactDataProvider *groupDataProvider;
@property (nonatomic, strong) CachingService *cachingService;
@property (strong, nonatomic) AssignContactViewController *assignViewCntrlr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAddContactHeightConstraint;
@property (strong, nonatomic) UIPopoverController *groupPopover;

@property (copy, nonatomic) void(^addContactBlock)(BOOL isAddContact);

- (IBAction)onBtnAddPhotoClick:(id)sender;
- (IBAction)onBtnAddContactClick:(id)sender;
- (IBAction)onBtnCancelDeleteClick:(id)sender;
- (IBAction)onBtnSaveEditClick:(id)sender;

- (void)addGroup:(NSNotification *)notifi;

@end
