//
//  FirstInterviewViewController.h
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstInterviewViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UITextView *txtRemark;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnPassFail;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnRecruitmentPlan;
@property (strong, nonatomic) NSString *oldValue;

@property (nonatomic, strong) TblContact *contact;
@property (copy, nonatomic) void(^callBackBlock)();

- (IBAction)onBtnPassFailClick:(id)sender;
- (IBAction)onBtnRecruitmentPlanClick:(id)sender;

- (IBAction)onBtnCancelClick:(id)sender;
- (IBAction)onBtnSaveClick:(id)sender;

@end
