//
//  CollectionDetailViewController.m
//  AIAChina
//
//  Created by Purva on 29/01/16.
//  Copyright © 2016 AIA. All rights reserved.
//

#import "CollectionDetailViewController.h"
#import "pageControlCell.h"

@interface CollectionDetailViewController ()

@end

@implementation CollectionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionV registerClass:[pageControlCell class] forCellWithReuseIdentifier:@"Cell"];
    if (self.selectedIndex > 0) {
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:self.selectedIndex inSection:0];
        [self.collectionV scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCancelTapped:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:NO];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return self.arrData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Dequeue a prototype cell and set the label to indicate the page
    pageControlCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.label.text = [NSString stringWithFormat:@"Page %d",indexPath.row + 1];
    cell.imagev.image = [self.arrData objectAtIndex:indexPath.row];
    
    cell.buttonMoveLeft.tag = indexPath.row;
    cell.buttonMoveRight.tag = indexPath.row;
    
    [cell.buttonMoveLeft addTarget:self action:@selector(moveLeft:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonMoveRight addTarget:self action:@selector(moveRight:) forControlEvents:UIControlEventTouchUpInside];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.bounds.size;
}

- (IBAction)moveLeft:(UIButton *)sender {
    if (sender.tag>0) {
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:sender.tag-1 inSection:0];
        [self.collectionV scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
}

-(IBAction)moveRight:(UIButton *)sender {
    if (sender.tag < self.arrData.count-1) {
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:sender.tag+1 inSection:0];
        [self.collectionV scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
}

@end
