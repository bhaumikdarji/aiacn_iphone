//
//  ContactNewListViewController.m
//  AIAChina
//
//  Created by imac on 18/01/16.
//  Copyright © 2016 AIA. All rights reserved.
//

#import "ContactNewListViewController.h"
#import "ContactListCell.h"
#import "HorizontalCollectionViewLayout.h"
#import "ContactDetailViewController.h"
#import "GroupCell.h"
#import "MenuAddViewController.h"
#import "MenuListViewController.h"
#import "AddContactViewController.h"
@interface ContactNewListViewController () <UICollectionViewDataSource,UICollectionViewDelegate,pagenumberDelegate,UITableViewDataSource,UITableViewDelegate>

@end

@implementation ContactNewListViewController

//----------------------------------------------------------------------------------------

#pragma mark - Memory Management Method

//----------------------------------------------------------------------------------------

- (void)dealloc {
    
}

//----------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//----------------------------------------------------------------------------------------

#pragma mark - Collection View Delegate Method

//----------------------------------------------------------------------------------------

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.contactArray count];
}

//----------------------------------------------------------------------------------------

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ContactListCell *cell=[self.tblContactList dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    if (!cell) {
        cell=[[ContactListCell alloc]init];
    }
    TblContact *contact=[self.contactArray objectAtIndex:indexPath.row];
    [cell loadContactData:contact];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedContact = [self.contactArray objectAtIndex:indexPath.row];
    
    ContactDetailViewController *vc;
    NSMutableArray *arr;
    NSMutableArray *arrMain;
    NSString *str = @"";
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[ContactDetailViewController class]]) {
            vc = (ContactDetailViewController *)controller;
            arr = [NSMutableArray new];
            arrMain = [NSMutableArray new];
            arr = (NSMutableArray *)self.navigationController.viewControllers;
            for (int i = 0; i < [arr count]; i++){
                if (![arr[i] isEqual:vc]) {
                    [arrMain addObject:arr[i]];
                }
            }
            arr = nil;
            [arrMain addObject:vc];
            str = @"";
            break;
        } else {
            str = @"Not Found";
        }
    }
    if ([str isEqualToString:@"Not Found"]) {
        vc = [self.storyboard instantiateViewControllerWithIdentifier:@"detailController"];
        vc.contact=self.selectedContact;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        vc.contact=self.selectedContact;
        [self.navigationController setViewControllers:arrMain animated:YES];
        arrMain = nil;
    }
//    [[NSNotificationCenter defaultCenter] postNotificationName:SPLIT_MASTER_DETAIL_TO_PROFILE_NOTIFICATION object:@{@"IsSync":RECURITMENT_STATUS_TRUE, @"object":self.selectedContact}];
}

//----------------------------------------------------------------------------------------

#pragma mark - UITableview DataSource Methods

//----------------------------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

//----------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.contactArray count];
}

//----------------------------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"GroupCell" forIndexPath:indexPath];
    TblContact *contact = self.contactArray[indexPath.row];
    [cell1 loadGroupContact:contact];
    return cell1;
}

//----------------------------------------------------------------------------------------

#pragma mark - UITableview Delegate Methods

//----------------------------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75.0;
}

//----------------------------------------------------------------------------------------

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedContact = [self.contactArray objectAtIndex:indexPath.row];
    ContactDetailViewController *vc;
    NSMutableArray *arr;
    NSMutableArray *arrMain;
    NSString *str = @"";
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[ContactDetailViewController class]]) {
            vc = (ContactDetailViewController *)controller;
            arr = [NSMutableArray new];
            arrMain = [NSMutableArray new];
            arr = (NSMutableArray *)self.navigationController.viewControllers;
            for (int i = 0; i < [arr count]; i++){
                if (![arr[i] isEqual:vc]) {
                    [arrMain addObject:arr[i]];
                }
            }
            arr = nil;
            [arrMain addObject:vc];
            str = @"";
            break;
        } else {
            str = @"Not Found";
        }
    }
    if ([str isEqualToString:@"Not Found"]) {
        vc = [self.storyboard instantiateViewControllerWithIdentifier:@"detailController"];
        vc.contact=self.selectedContact;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        vc.contact=self.selectedContact;
        [self.navigationController setViewControllers:arrMain animated:YES];
        arrMain = nil;
    }
    [self.tblContactListView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] animated:YES];
}

//----------------------------------------------------------------------------------------

#pragma mark - User Interaction Method

//----------------------------------------------------------------------------------------

- (IBAction)onBtnAddContactClick:(id)sender {
    AddContactViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"addContact"];
    [self.navigationController pushViewController:vc animated:YES];
}

//----------------------------------------------------------------------------------------

- (IBAction)onBtnMenuListClick:(id)sender {
    [self performSegueWithIdentifier:@"MenuListPopover" sender:self];
}

//----------------------------------------------------------------------------------------

#pragma mark - Custom Interaction method

//----------------------------------------------------------------------------------------

- (void)reloadContactList:(NSNotification *)notifi{
    self.navigationItem.title = NSLocalizedString(@"Contact", @"");
    if(notifi)
        self.selectedContact = [notifi object];
    self.isGroupList = FALSE;
    [self loadList];
}

//----------------------------------------------------------------------------------------

- (void)reloadGroupList:(NSNotification *)notifi{
    self.navigationItem.title = NSLocalizedString(@"Group", @"");
    if(notifi)
        self.selectedGroup = [notifi object];
    self.isGroupList = TRUE;
    [self loadList];
}

//----------------------------------------------------------------------------------------

-(void) setTotalPage:(int)pagenumber {
    self.pgControl.numberOfPages=pagenumber;
}

//----------------------------------------------------------------------------------------

-(void)setCurrentPage:(int)currentPage {
    [self.pgControl setCurrentPage:currentPage];
}

//----------------------------------------------------------------------------------------

- (NSMutableArray *)getNumberOfRowsInSection:(NSInteger)section isOrder:(BOOL)setOrdering{
    NSMutableArray *numberOfRowArray=[[NSMutableArray alloc] init];
    
    for (TblContact *contact in self.contactArray) {
        if(!isEmptyString(contact.name)){
            NSString *strFirstNameFirstChar = [[contact.name substringToIndex:1] uppercaseString];
            if([strFirstNameFirstChar isEqualToString:[self.contactArray objectAtIndex:section]]){
                [numberOfRowArray addObject:contact];
            }
        }
        
    }
    
    if(setOrdering){
        [numberOfRowArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            if(self.isGroupList){
                TblGroup *group1 = obj1, *group2 = obj2;
                return [group1.groupName caseInsensitiveCompare:group2.groupName];
            }else{
                TblContact *contact1 = obj1, *contact2 = obj2;
                return [contact1.name caseInsensitiveCompare:contact2.name];
            }
        }];
    }
    return numberOfRowArray;
}

//----------------------------------------------------------------------------------------

- (void)loadList{
    NSString *agentId = [CachingService sharedInstance].currentUserId;
    
    self.allcontactArray = [[NSMutableArray alloc]init];
    [self.contactArray removeAllObjects];
    [self.allcontactArray removeAllObjects];
    self.allcontactArray = [[DbUtils fetchAllObject:self.isGroupList ? @"TblGroup" : @"TblContact"
                                      andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0", agentId]
                               andSortDescriptor:nil
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    
    self.contactArray = [self.allcontactArray mutableCopy];
    if ([self.contactArray count]>0) {
        [self.tblContactList reloadData];
        [self.tblContactListView reloadData];
        
        if (![self.tblContactList isHidden]) {
            [self.vwNoContact setHidden:true];
            [self.tblContactListView setHidden:true];
            [self.tblContactList setHidden:false];
            [self.pgControl setHidden:false];
        } else {
            [self.vwNoContact setHidden:true];
            [self.tblContactListView setHidden:false];
            [self.tblContactList setHidden:true];
            [self.pgControl setHidden:true];
        }
        
    } else {
        [self.vwNoContact setHidden:false];
        [self.tblContactListView setHidden:true];
        [self.tblContactList setHidden:true];
        [self.pgControl setHidden:true];
    }
    
    [self.groupArray removeAllObjects];
    
    self.groupArray = [[DbUtils fetchAllObject:@"TblGroup" andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0", agentId] andSortDescriptor:nil managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    
    [self.groupNameArray removeAllObjects];
    self.groupNameArray = [[NSMutableArray alloc]init];
    for(int i = 0; i< self.groupArray.count; i++)
    {
        NSString *strGroupName = [[self.groupArray objectAtIndex:i]valueForKey:@"groupName"];
        [self.groupNameArray addObject:strGroupName];
        
    }
    
    [self.groupNameArray insertObject:@"All Contacts" atIndex:0];
    
    
    self.lblEntries.text = [NSString stringWithFormat:@"%d ENTRIES",[self.contactArray count]];
    
    
    
}
//----------------------------------------------------------------------------------------
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.tblContactList.frame.size.width;
    self.pgControl.currentPage = self.tblContactList.contentOffset.x / pageWidth;
}
//----------------------------------------------------------------------------------------

#pragma mark - Life Cycle Methods

//----------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    HorizontalCollectionViewLayout *layout=[[HorizontalCollectionViewLayout alloc]init];
    layout.delegate=self;
    [self.tblContactList setDataSource:self];
    [self.tblContactList setDelegate:self];
    [self.tblContactListView setDataSource:self];
    [self.tblContactListView setDelegate:self];
    [self.tblContactList setCollectionViewLayout:layout];
    [self.tblContactList setPagingEnabled:YES];
    [self.tblContactList setBackgroundColor:[UIColor whiteColor]];
    [self loadList];
    [self.view bringSubviewToFront:self.pgControl];
    [self.tblContactList setBackgroundColor:[UIColor colorWithRed:249.0/255 green:250.0/255.0 blue:243.0/255.0 alpha:1.0]];
    
    self.btnContactGroup.layer.borderWidth = 1.0;
    self.btnContactGroup.layer.borderColor = [UIColor colorWithRed:191.0/255 green:32.0/255.0 blue:75.0/255.0 alpha:1.0].CGColor;
    self.btnFilter.layer.borderWidth = 1.0;
    self.btnFilter.layer.borderColor = [UIColor colorWithRed:191.0/255 green:32.0/255.0 blue:75.0/255.0 alpha:1.0].CGColor;
    
    //dropDown = [[NIDropDown alloc]init];
    
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnLastUpdated];
    [self.btnLastUpdated addSubview:bottomBorder];
    
    
    
}

//----------------------------------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = [self prepareRightBarButton:YES];
    self.navigationItem.leftBarButtonItem = [self prepareLeftBarButton:YES withLogo:NO];
    [MyAppDelegate showAnnouncements:self.view];
    
    self.viewSearch.hidden = TRUE;
    self.viewSearch.layer.borderWidth =1.0;
    self.viewSearch.layer.borderColor = [UIColor blackColor].CGColor;
    self.isSearch = false;
    
    
}

//----------------------------------------------------------------------------------------

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - Day DropDwon Method


- (IBAction)btnContactGroup:(id)sender {
   // NSArray * arr = [[NSArray alloc] init];
   // arr = [NSArray arrayWithObjects:@"01", @"02", @"03", @"04", @"05", @"06", @"07",nil];
    
    NSMutableArray *arr  = [[NSMutableArray alloc]init];
    arr = [self.groupNameArray mutableCopy];
    
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    [self hideSearchView];
}
- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
   
}

-(void)rel{
    //    [dropDown release];
    
    NSString *strSelect = self.btnContactGroup.titleLabel.text;
    if([strSelect isEqualToString:@"All Contacts"])
    {
        NSLog(@"%lu",(unsigned long)self.allcontactArray.count);
        self.contactArray = [self.allcontactArray mutableCopy];
        
        if ([self.contactArray count]>0) {
            [self.tblContactList reloadData];
            [self.tblContactListView reloadData];
            if (![self.tblContactList isHidden]) {
                [self.vwNoContact setHidden:true];
                [self.tblContactListView setHidden:true];
                [self.tblContactList setHidden:false];
                [self.pgControl setHidden:false];
            } else {
                [self.vwNoContact setHidden:true];
                [self.tblContactListView setHidden:false];
                [self.tblContactList setHidden:true];
                [self.pgControl setHidden:true];
            }
        } else {
            [self.vwNoContact setHidden:false];
            [self.tblContactListView setHidden:true];
            [self.tblContactList setHidden:true];
            [self.pgControl setHidden:true];
        }
    }else {
    
        NSLog(@"%lu",(unsigned long)self.contactArray.count);
        
        [self.contactArray removeAllObjects];
        for (int i=0; i<self.allcontactArray.count;i++)
        {
            
            NSString *str = [[self.allcontactArray objectAtIndex:i]valueForKey:@"groupName"];
            if([str isEqualToString:strSelect])
            {
                [self.contactArray addObject:[self.allcontactArray objectAtIndex:i]];
            }
            
        }
        if ([self.contactArray count]>0) {
            [self.tblContactList reloadData];
            [self.tblContactListView reloadData];
            
            if (![self.tblContactList isHidden]) {
                [self.vwNoContact setHidden:true];
                [self.tblContactListView setHidden:true];
                [self.tblContactList setHidden:false];
                [self.pgControl setHidden:false];
            } else {
                [self.vwNoContact setHidden:true];
                [self.tblContactListView setHidden:false];
                [self.tblContactList setHidden:true];
                [self.pgControl setHidden:true];
            }

        } else {
            [self.vwNoContact setHidden:false];
            [self.tblContactListView setHidden:true];
            [self.tblContactList setHidden:true];
            [self.pgControl setHidden:true];
        }
        
    }
    
    
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnLastUpdated];
    [self.btnLastUpdated addSubview:bottomBorder];
    
    
    
    
    dropDown = nil;
}

//-----------------------------------------------------------------------------------------------

-(IBAction)btnSearch:(id)sender
{
    if (!self.isSearch) {
        
        self.viewSearch.frame =  CGRectMake(824, self.imgView.frame.origin.y + 44, 200, 0);
        
        
        [UIView animateWithDuration:0.400 delay:0.0
                            options:UIViewAnimationOptionTransitionCurlDown
                         animations:^{
                              self.viewSearch.frame =  CGRectMake(824, self.imgView.frame.origin.y + 44, 200, 44);
                             [self.viewSearch setFrame:CGRectMake(824, self.imgView.frame.origin.y + 44, 200, 44)];
                             self.viewSearch.hidden = FALSE;
                         } completion:^(BOOL finished) {
                             
                         }];
        self.isSearch = true;
    } else {

        [UIView animateWithDuration:0.400 delay:0.0
                            options:UIViewAnimationOptionTransitionCurlUp
                         animations:^{
                             self.viewSearch.frame =  CGRectMake(824, self.imgView.frame.origin.y + 44, 200, 0);
                             //[self.viewSearch setFrame:CGRectMake(824, self.imgView.frame.origin.y + 44, 200, 0)];
                             //self.viewSearch.hidden = TRUE;
                         } completion:^(BOOL finished) {
                             
                         }];

        self.isSearch = false;
    }
}

- (IBAction)showListView:(id)sender {
    [self.tblContactList setHidden:YES];
    [self.tblContactListView setHidden:NO];
    [self.pgControl setHidden:YES];
}

- (IBAction)showCollectionView:(id)sender {
    [self.tblContactList setHidden:NO];
    [self.tblContactListView setHidden:YES];
    [self.pgControl setHidden:NO];
}
//----------------------------------------------------------------------------------------

-(void)hideSearchView
{
    [UIView animateWithDuration:0.400 delay:0.0
                        options:UIViewAnimationOptionTransitionCurlUp
                     animations:^{
                         self.viewSearch.frame =  CGRectMake(824, self.imgView.frame.origin.y + 44, 200, 0);
                         self.viewSearch.hidden = TRUE;
                     } completion:^(BOOL finished) {
                         
                     }];
    
    self.isSearch = false;
}

//----------------------------------------------------------------------------------------

-(IBAction)btnLastUpdated:(id)sender
{
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnLastUpdated];
    [self.btnLastUpdated addSubview:bottomBorder];
    [self loadList];
}
//-----------------------------------------------------------------------------------------------

- (IBAction)btnAtoZ:(id)sender
{
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES]; // Describe the Key value using which you want to sort.
    NSArray * descriptors = [NSArray arrayWithObject:valueDescriptor]; // Add the value of the descriptor to array.
    NSArray *ar1 = [self.contactArray sortedArrayUsingDescriptors:descriptors]; // Now Sort the Array using descriptor.
    self.contactArray=[NSMutableArray arrayWithArray:ar1];
    [self.tblContactList reloadData];
    [self.tblContactListView reloadData];
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnAtoZ];
    [self.btnAtoZ addSubview:bottomBorder];
    
    [self hideSearchView];
    
    self.lblEntries.text = [NSString stringWithFormat:@"%d ENTRIES",[self.contactArray count]];

}
//-----------------------------------------------------------------------------------------------
- (IBAction)btnZtoA:(id)sender
{
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO]; // Describe the Key value using which you want to sort.
    NSArray * descriptors = [NSArray arrayWithObject:valueDescriptor]; // Add the value of the descriptor to array.
    NSArray *ar1 = [self.contactArray sortedArrayUsingDescriptors:descriptors]; // Now Sort the Array using descriptor.
    self.contactArray=[NSMutableArray arrayWithArray:ar1]; // Now Sort the Array using descriptor.
    
    [self.tblContactList reloadData];
    [self.tblContactListView reloadData];
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnZtoA];
    [self.btnZtoA addSubview:bottomBorder];
    
    [self hideSearchView];
    
    self.lblEntries.text = [NSString stringWithFormat:@"%d ENTRIES",[self.contactArray count]];
    
}
//-----------------------------------------------------------------------------------------------
- (IBAction)btnNewest:(id)sender
{
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modificationDate" ascending:NO];
    NSArray * descriptors = [NSArray arrayWithObject:valueDescriptor];
    if ([self.contactArray count]>0) {
        NSArray *ar1 = [self.contactArray sortedArrayUsingDescriptors:descriptors]; // Now Sort the Array using descriptor.
        self.contactArray=[NSMutableArray arrayWithArray:ar1];
        [self.tblContactList reloadData];
        [self.tblContactListView reloadData];
    }
    
    
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnNewest];
    [self.btnNewest addSubview:bottomBorder];
    
    [self hideSearchView];
    
    self.lblEntries.text = [NSString stringWithFormat:@"%d ENTRIES",[self.contactArray count]];
}
//-----------------------------------------------------------------------------------------------
- (IBAction)btnEarliest:(id)sender
{
    
   // NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"beginDate" ascending:TRUE];
   // [myMutableArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modificationDate" ascending:YES];
    NSArray * descriptors = [NSArray arrayWithObject:valueDescriptor];
    NSArray *ar1 = [self.contactArray sortedArrayUsingDescriptors:descriptors]; // Now Sort the Array using descriptor.
    self.contactArray=[NSMutableArray arrayWithArray:ar1];
    
    [self.tblContactList reloadData];
    [self.tblContactListView reloadData];
    [bottomBorder removeFromSuperview];
    [self buttonFram:self.btnEarliest];
    [self.btnEarliest addSubview:bottomBorder];
    
    [self hideSearchView];
    
    self.lblEntries.text = [NSString stringWithFormat:@"%d ENTRIES",[self.contactArray count]];
}

-(void)buttonFram :(UIButton *)btn
{
    bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(8, btn.frame.size.height - 1.0f, btn.frame.size.width-16, 2)];
    bottomBorder.backgroundColor = [UIColor colorWithRed:199.0/255.0 green:0.0/255.0 blue:54.0/255.0 alpha:1.0];
    
}
//-----------------------------------------------------------------------------------------------
-(IBAction)btnSearchField:(id)sender
{
    NSString *strSearch = self.txtSearch.text;
    NSArray *arr=[NSArray arrayWithArray:self.contactArray];
    [self.contactArray removeAllObjects];
    for(int i=0; i< arr.count; i++)
    {//a
        NSString *str = [[arr objectAtIndex:i]valueForKey:@"name"];
        
        if([str rangeOfString:strSearch options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            [self.contactArray addObject:[arr objectAtIndex:i]];
        }

    }
    if ([self.contactArray count]>0) {
        [self.tblContactList reloadData];
        [self.tblContactListView reloadData];
        
        if (![self.tblContactList isHidden]) {
            [self.vwNoContact setHidden:true];
            [self.tblContactListView setHidden:true];
            [self.tblContactList setHidden:false];
            [self.pgControl setHidden:false];
        } else {
            [self.vwNoContact setHidden:true];
            [self.tblContactListView setHidden:false];
            [self.tblContactList setHidden:true];
            [self.pgControl setHidden:true];
        }
        
    } else {
        [self.vwNoContact setHidden:false];
        [self.tblContactListView setHidden:true];
        [self.tblContactList setHidden:true];
        [self.pgControl setHidden:true];
    }
    [self hideSearchView];
    [self.txtSearch resignFirstResponder];
    self.txtSearch.text= @"";
    
    self.lblEntries.text = [NSString stringWithFormat:@"%d ENTRIES",[self.contactArray count]];
}


//-----------------------------------------------------------------------------------------------


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}


//----------------------------------------------------------------------------------------
-(void)createNewGroup
{
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.destinationViewController isKindOfClass:[MenuListViewController class]]){
        MenuListViewController *menuList = segue.destinationViewController;
        [menuList setShowAllContact:^(int selectedList) {
            if(selectedList == 0){
                [[NSNotificationCenter defaultCenter] postNotificationName:GROUP_TO_PROFILE_NOTIFICATION
                                                                    object:nil];
                [self reloadContactList:nil];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                                                    object:nil];
                [self reloadGroupList:nil];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }else if([segue.destinationViewController isKindOfClass:[MenuAddViewController class]]){
        MenuAddViewController *menuList = segue.destinationViewController;
        [menuList setShowIndex:^(int selectedList) {
            self.isAddContactGroup = TRUE;
            if(selectedList == 0){
                [self reloadContactList:nil];
            }else{
                [self reloadGroupList:nil];
            }
            [self addContactGroup:[NSNumber numberWithInteger:selectedList]];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }
}
- (void)addContactGroup:(NSNumber *)selectedList{
    if([selectedList isEqual: @0]){
        [[NSNotificationCenter defaultCenter] postNotificationName:SPLIT_MASTER_DETAIL_TO_ADD_CONTACT_NOTIFICATION
                                                            object:nil];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION
                                                            object:nil];
    }
}



@end
