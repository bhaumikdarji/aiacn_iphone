//
//  CompanyIntSuccessController.m
//  AIAChina
//
//  Created by MacMini on 28/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CompanyIntSuccessController.h"
#import "DropDownManager.h"

@interface CompanyIntSuccessController ()

@end

@implementation CompanyIntSuccessController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utility setRoundCornerRedius:self.imgHeader
                     cornerRedius:60.0];
    [Utility setRoundCornerRedius:self.mainView
                     cornerRedius:5.0];
    [self setGAMAData];
    
    NSString *str = [NSString stringWithFormat:@"%@ %@.", NSLocalizedString(@"Currently Candidate's rank is", @""), [Utility ordinalNumberFormat:self.candidateProgressRank]];
    NSRange rang = [str rangeOfString:[NSString stringWithFormat:@"%d",self.candidateProgressRank]];
    [self.lblTitle setAttributedText:[Utility setBoldTextOnSingleLable:str fontSize:14.0 range:rang setBoldFontColor:[UIColor redColor]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setGAMAData{
    NSArray *gamaArray = [DropDownManager sharedManager].gamaContentEnglish;
    NSDictionary *gamaDic = [gamaArray objectAtIndex:arc4random() % [gamaArray count]];
    self.lblGAMAQuoteEnglish.text = [gamaDic objectForKey:@"Description_en"];
    self.lblGAMAQuoteChinese.text = [gamaDic objectForKey:@"Description_cn"];
    self.lblGAMAAuthorEnglish.text = [gamaDic objectForKey:@"Author_en"];
}

- (IBAction)onBtnCancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
