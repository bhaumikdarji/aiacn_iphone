//
//  EOPRegistrationViewController.h
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface EOPRegistrationViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitel;
@property (weak, nonatomic) IBOutlet UITableView *tblEopReg;

@property (weak, nonatomic) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet UIButton *btnTopic;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;
@property (weak, nonatomic) IBOutlet UITextView *txtLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblSpeaker;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnEventMaterials;
@property (weak, nonatomic) IBOutlet UIButton *btnAfterEventMaterials;
@property (weak, nonatomic) IBOutlet UIButton *btnTotalCandidate;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;

@property (strong, nonatomic) TblContact *contact;
@property (copy, nonatomic) void(^callBackBlock)();

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *RVHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *EMHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *AEMHeightConstraint;

- (IBAction)onBtnTopicClick:(id)sender;
- (IBAction)onBtnEventMaterialsClick:(id)sender;
- (IBAction)onBtnAfterEventMaterialsClick:(id)sender;
- (IBAction)onBtnTotalCandidateClick:(id)sender;
- (IBAction)onBtnRegisterClick:(id)sender;

@end
