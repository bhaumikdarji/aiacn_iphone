//
//  ContactNewListViewController.h
//  AIAChina
//
//  Created by imac on 18/01/16.
//  Copyright © 2016 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "NIDropDown.h"

@interface ContactNewListViewController : AIAChinaViewController<NIDropDownDelegate>
{
    NIDropDown *dropDown;
    UIView *bottomBorder;
}


@property (weak, nonatomic) IBOutlet UICollectionView *tblContactList;
@property (nonatomic, strong) NSMutableArray *contactArray;
@property (nonatomic, strong) NSMutableArray *groupArray;
@property (nonatomic) BOOL isGroupList;
@property (nonatomic) BOOL isAddContactGroup;

@property (nonatomic, strong) NSMutableArray *allcontactArray;

@property (strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) TblContact *selectedContact;
@property (nonatomic, strong) TblGroup *selectedGroup;
@property (copy, nonatomic) void(^showContact)(NSArray *);
@property (nonatomic, strong) UIPopoverController *menuListPopOver;
@property (weak, nonatomic) IBOutlet UIPageControl *pgControl;
@property (nonatomic, strong) IBOutlet UIButton *btnContactGroup;
@property (nonatomic, strong) IBOutlet UIButton *btnFilter;
@property (nonatomic, strong) IBOutlet UIView *viewFilter;
@property (nonatomic, strong) IBOutlet UIView *viewSearch;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic)BOOL isSearch;
@property (weak, nonatomic) IBOutlet UIView *vwNoContact;

@property (nonatomic, strong) IBOutlet UIButton *btnLastUpdated;
@property (nonatomic, strong) IBOutlet UIButton *btnAtoZ;
@property (nonatomic, strong) IBOutlet UIButton *btnZtoA;
@property (nonatomic, strong) IBOutlet UIButton *btnNewest;
@property (nonatomic, strong) IBOutlet UIButton *btnEarliest;

@property (weak, nonatomic) IBOutlet UITableView *tblContactListView;

@property (nonatomic, strong) NSMutableArray *groupNameArray;

@property (weak, nonatomic) IBOutlet UIButton *btnListView;
@property (weak, nonatomic) IBOutlet UIButton *btnCollectionView;
@property (nonatomic, strong) IBOutlet UITextField *txtSearch;
@property (nonatomic, strong) IBOutlet UIButton *btnSearchField;
@property (nonatomic, strong) IBOutlet UILabel *lblEntries;



- (IBAction)onBtnAddContactClick:(id)sender;
- (IBAction)btnContactGroup:(id)sender;
- (IBAction)btnSearch:(id)sender;
- (IBAction)btnNewContact:(id)sender;
- (IBAction)showListView:(id)sender;
- (IBAction)showCollectionView:(id)sender;

- (IBAction)btnLastUpdated:(id)sender;
- (IBAction)btnAtoZ:(id)sender;
- (IBAction)btnZtoA:(id)sender;
- (IBAction)btnNewest:(id)sender;
- (IBAction)btnEarliest:(id)sender;

- (IBAction)btnSearchField:(id)sender;


//- (IBAction)onBtnSearchClick:(id)sender;
//- (IBAction)onBtnSyncClick:(id)sender;
//- (IBAction)onBtnDeleteClick:(id)sender;
//- (IBAction)onBtnExportClick:(id)sender;


@end
