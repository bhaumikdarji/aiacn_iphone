//
//  MapViewController.h
//  AIAChina
//
//  Created by MacMini on 16/09/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI/BMapKit.h>

@interface MapViewController : AIAChinaViewController<BMKMapViewDelegate, BMKLocationServiceDelegate>

@property (weak, nonatomic) IBOutlet BMKMapView *mapView;

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) BOOL isUpdating;
@property (strong, nonatomic) NSString *locationTitle;
@property (strong, nonatomic) BMKLocationService *locService;
@property (strong, nonatomic) BMKPointAnnotation *pointAnnotation;
@property (strong, nonatomic) BMKAnnotationView *annotationNew;
@property (nonatomic) BMKCoordinateRegion region;
@property (nonatomic) BMKUserLocation *uLoc;
@property (nonatomic) CLLocationCoordinate2D coor;
@property (strong, nonatomic) CLLocationManager *locManager;
@property (copy, nonatomic) void(^locationBlock)(double latitude, double longitude, NSString *address);

- (IBAction)onBtnDoneClick:(id)sender;

@end
