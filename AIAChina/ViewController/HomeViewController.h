//
//  HomeViewController.h
//  AIAChina
//
//  Created by AIA on 16/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "NVBnbCollectionView.h"

@interface HomeViewController : AIAChinaViewController<NVBnbCollectionViewDataSource, NVBnbCollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *imageBorderView;
@property (weak, nonatomic) IBOutlet UILabel *lblAgentName;
@property (weak, nonatomic) IBOutlet UILabel *lblAgentPosition;

@property (weak, nonatomic) IBOutlet UIButton *btnInteractive;
@property (weak, nonatomic) IBOutlet UIButton *btnResource;
@property (weak, nonatomic) IBOutlet UIButton *agentProfileImageButton;
@property (weak, nonatomic) IBOutlet UIButton *btnAddressbook;
@property (weak, nonatomic) IBOutlet UIButton *brnCalendar;
@property (weak, nonatomic) IBOutlet UIButton *btnGoal;
@property (weak, nonatomic) IBOutlet UIButton *btnReport;
@property (strong, nonatomic) NSArray *agentArray;

@property (strong, nonatomic) NSMutableArray *carouselImageArray;
@property (nonatomic) int currentIndex;
@property (strong, nonatomic) TblAgentDetails *agentProfile;
@property (nonatomic, readwrite) BOOL isInteractivePresenter;

- (IBAction)onBtnLeftArrowClick:(id)sender;
- (IBAction)onBtnRightArrowClick:(id)sender;
- (IBAction)onBtnInteractiveClick:(id)sender;
- (IBAction)onBtnResourceClick:(id)sender;
- (IBAction)onBtnAddressbookClick:(id)sender;
- (IBAction)onBrnCalendarClick:(id)sender;
- (IBAction)onBtnGoalClick:(id)sender;
- (IBAction)onBtnReportClick:(id)sender;
- (IBAction)onAgentProfileImageButtonClick:(id)sender;
- (void)openInteractivePresenter;
- (void)navigateToAddressBook;
- (void)btnSettingClick;

@end
