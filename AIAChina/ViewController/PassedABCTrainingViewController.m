//
//  PassedABCTrainingViewController.m
//  AIAChina
//
//  Created by MacMini on 07/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "PassedABCTrainingViewController.h"

@interface PassedABCTrainingViewController ()
@property (weak, nonatomic) IBOutlet UILabel *passAbcTrainingLabel;
@property (weak, nonatomic) IBOutlet UITextView *passStatus;

@end

@implementation PassedABCTrainingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Passed ABC Traning", @"");
    self.passAbcTrainingLabel.text = NSLocalizedString(@"ABC Training", @"");
    self.passStatus.text = NSLocalizedString(@"Pass", @"Pass");
    self.passStatus.textAlignment = NSTextAlignmentCenter;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if(self.callBackBlock)
        self.callBackBlock();
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
