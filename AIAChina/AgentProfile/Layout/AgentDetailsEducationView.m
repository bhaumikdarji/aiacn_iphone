//
//  AgentDetailsEducationView.m
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AgentDetailsEducationView.h"
#import "AgentDetailEducationCollectionViewCell.h"
#import "TblAgentDetails.h"
#import "CachingService.h"

@interface AgentDetailsEducationView () <UICollectionViewDataSource, UICollectionViewDelegate, AgentDetailEducationCollectionViewCellDelegate>

// UI
@property (nonatomic, strong) UIImageView *educationBackgroundView;
@property (nonatomic, strong) UILabel *educationTitleLabel;
@property (nonatomic, strong) UICollectionView *educationCollectionView;

// Data
@property (nonatomic, strong) TblAgentDetails *agentDetails;
@property (nonatomic, strong) NSMutableArray *data;

@end

@implementation AgentDetailsEducationView

static NSString *const kAgentDetailsEducationCellId = @"agentDetailsEducationCellId";

- (id)initWithFrame:(CGRect)frame andAgentDetails:(TblAgentDetails *)agentDetails
{
    self = [super initWithFrame:frame];
    if( self )
    {
        [self initUI];
        
        self.agentDetails = agentDetails;
        
        AgentDetailEducationModel *graduationSchool = [AgentDetailEducationModel new];
        graduationSchool.title = NSLocalizedString(@"Graduation School", nil);
        graduationSchool.subTitle = self.agentDetails.graduationSchool == nil || [self.agentDetails.graduationSchool isEqualToString:@""] ? @"-" : self.agentDetails.graduationSchool;
        
        AgentDetailEducationModel *graduationTime = [AgentDetailEducationModel new];
        graduationTime.title = NSLocalizedString(@"Graduation Time", nil);
        graduationTime.subTitle = self.agentDetails.graduationTime == nil ? @"-" : [DateUtils dateToString:self.agentDetails.graduationTime andFormate:kYearMonthFormat2];
        graduationTime.isDate = true;
        
        AgentDetailEducationModel *professionalInst = [AgentDetailEducationModel new];
        professionalInst.title = NSLocalizedString(@"Professional Institutions", nil);
        professionalInst.subTitle = self.agentDetails.professionalInstitutions == nil || [self.agentDetails.professionalInstitutions isEqualToString:@""] ? @"-" : self.agentDetails.professionalInstitutions;
        
        AgentDetailEducationModel *educationBackground = [AgentDetailEducationModel new];
        educationBackground.title = NSLocalizedString(@"Education Background", nil);
        educationBackground.subTitle = self.agentDetails.educationBackground == nil || [self.agentDetails.educationBackground isEqualToString:@""] ? @"-" : self.agentDetails.educationBackground;
        
        self.data = [NSMutableArray arrayWithObjects:graduationSchool, graduationTime, professionalInst, educationBackground, nil];
    }
    return self;
}

- (void)initUI
{
    self.educationTitleLabel = [UILabel new];
    self.educationTitleLabel.backgroundColor = [UIColor clearColor];
    self.educationTitleLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.educationTitleLabel.textColor = [UIColor grayColor];
    self.educationTitleLabel.text = NSLocalizedString(@"EducationTitle", nil);
    self.educationTitleLabel.frame = CGRectMake(45, 20, CGRectGetWidth(self.frame) - 45*2, self.educationTitleLabel.font.lineHeight);
    
    self.educationBackgroundView = [[UIImageView alloc] init]; // TODO: Use image
    self.educationBackgroundView.backgroundColor = [UIColor colorWithRed:1 green:0.99 blue:0.98 alpha:1];
    self.educationBackgroundView.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), 170);
    
    UICollectionViewFlowLayout *educationFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    educationFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    educationFlowLayout.itemSize = CGSizeMake(150, 90);
    educationFlowLayout.minimumInteritemSpacing = 10;
    
    self.educationCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(45, CGRectGetMaxY(self.educationTitleLabel.frame) + 10, CGRectGetWidth(self.frame) - 45*2, 100) collectionViewLayout:educationFlowLayout];
    self.educationCollectionView.delegate = self;
    self.educationCollectionView.dataSource = self;
    self.educationCollectionView.backgroundColor = [UIColor clearColor];
    [self.educationCollectionView registerClass:[AgentDetailEducationCollectionViewCell class] forCellWithReuseIdentifier:kAgentDetailsEducationCellId];
    
    [self addSubview:self.educationBackgroundView];
    [self addSubview:self.educationTitleLabel];
    [self addSubview:self.educationCollectionView];
}

#pragma mark - UICollectionViewDelegate/UICollectionViewDataSource

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:false];
    
    NSLog(@"Education selected %lu", indexPath.row);
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AgentDetailEducationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAgentDetailsEducationCellId forIndexPath:indexPath];
    cell.indexPath = indexPath;
    cell.delegate = self;
    [cell configureWithData:[self.data objectAtIndex:indexPath.row] andOther:indexPath.row%2==0];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}

#pragma mark - AgentDetailEducationCollectionViewCellDelegate

- (void)agentDetailEducationCollectionViewCellDidEndEditing:(AgentDetailEducationCollectionViewCell *)cell
{
    switch (cell.indexPath.row)
    {
        case 0:
        {
            self.agentDetails.graduationSchool = cell.educationSubtitleField.text;
        }
            break;
        case 2:
        {
            self.agentDetails.professionalInstitutions = cell.educationSubtitleField.text;
        }
            break;
        case 3:
        {
            self.agentDetails.educationBackground = cell.educationSubtitleField.text;
        }
            break;
        default:
            break;
    }
    
    [[CachingService sharedInstance] saveContext];
}

- (void)agentDetailEducationCollectionViewCellDidSelectDatePrompt:(AgentDetailEducationCollectionViewCell *)cell
{
    [self.viewController showDatePopoverCtrl:UIDatePickerModeDate
                                 minimumDate:nil
                                 maximumDate:nil
                                        rect:cell.dateButton.frame
                                      inView:cell.contentView
                                  pickerDate:^(NSDate *date) {
                                      
                                      self.agentDetails.graduationTime = date;
                                      [[CachingService sharedInstance] saveContext];
                                      
                                      [cell.dateButton setTitle:[DateUtils dateToString:date andFormate:kYearMonthFormat2] forState:UIControlStateNormal];
                                  }];
}

@end

@implementation AgentDetailEducationModel

- (id)init
{
    self = [super init];
    if( self )
    {
        self.title = @"";
        self.subTitle = @"";
        self.isDate = false;
    }
    return self;
}

@end