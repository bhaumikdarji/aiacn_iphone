//
//  AgenDetailsCareerProgressView.h
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AgentDetailsCareerProgressView;

@protocol AgentDetailsCareerProgressViewDelegate <NSObject>

@optional
- (void)agentDetailsCareerProgressViewDidSelectAddCareer:(AgentDetailsCareerProgressView*)view edit:(BOOL)editing indexPath:(NSInteger)row;


@end

@interface AgentDetailsCareerProgressView : UIView

@property (nonatomic, weak) id <AgentDetailsCareerProgressViewDelegate> delegate;
@property (nonatomic, strong) NSArray *data;

@end
