//
//  AgentDetailsPhotosView.m
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AgentDetailsPhotosView.h"

@interface AgentDetailsPhotosView () <UICollectionViewDataSource, UICollectionViewDelegate>

// UI
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *dataArray;
@end

@implementation AgentDetailsPhotosView

NSString *const kAgentDetailsPhotoCellId = @"agentDetailsPhotoCellId";

- (id)initWithFrame:(CGRect)frame withData:(NSArray *) dataSourceArray
{
    self = [super initWithFrame:frame];
    if( self )
    {
        AgentDetailsPhotosViewLayout *layout = [[AgentDetailsPhotosViewLayout alloc] init];
        
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame)) collectionViewLayout:layout];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        self.collectionView.scrollEnabled = YES;
        self.collectionView.backgroundColor = [UIColor clearColor];
        self.dataArray = dataSourceArray;
        [self.collectionView registerClass:[AgentDetailsPhotoCell class] forCellWithReuseIdentifier:kAgentDetailsPhotoCellId];
        [self addSubview:self.collectionView];
    }
    return self;
}

#pragma mark - UICollectionViewDataSource/UICollectionViewDelegate

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AgentDetailsPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAgentDetailsPhotoCellId forIndexPath:indexPath];
    [cell configureWithData:self.dataArray[indexPath.item]];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return MIN(self.dataArray.count, 8);
}

@end

#pragma mark - AgentDetailsPhotoCell

@interface AgentDetailsPhotoCell ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation AgentDetailsPhotoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
    {
        self.imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:self.imageView];
    }
    return self;
}

- (void)configureWithData:(id)data
{
    self.imageView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
    self.imageView.image = [UIImage imageWithData:data];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = self.bounds;
}

@end

#pragma mark - AgentDetailsPhotosViewLayout

@interface AgentDetailsPhotosViewLayout ()

@property (nonatomic, strong) NSMutableDictionary *layoutInfo;

@end

@implementation AgentDetailsPhotosViewLayout

- (void)prepareLayout
{
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    
    NSInteger sectionCount = [self.collectionView numberOfSections];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    for (NSInteger section = 0; section < sectionCount; section++)
    {
        NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < itemCount; item++)
        {
            indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            
            UICollectionViewLayoutAttributes *itemAttributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            itemAttributes.frame = [self frameForItemAtIndexPath:indexPath];
            
            cellLayoutInfo[indexPath] = itemAttributes;
        }
    }
    
    self.layoutInfo = cellLayoutInfo;
}

#pragma mark - Private

- (CGRect)frameForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat initialItemHeight = 100;
    if( indexPath.row == 0 )
        return CGRectMake(0, 0, CGRectGetWidth(self.collectionView.frame), initialItemHeight);
    
    CGFloat itemPaddingX = 5;
    CGFloat itemPaddingY = 5;
    CGFloat itemWidth = (CGRectGetWidth(self.collectionView.frame) - itemPaddingX*2)/3;
    CGFloat itemPosX = (indexPath.row-1)%3 * (itemWidth + itemPaddingX);
    CGFloat itemPosY = initialItemHeight + itemPaddingY + (floorf((indexPath.row-1)/3)) * (itemPaddingY + 50);
    
    return CGRectMake(itemPosX, itemPosY, itemWidth, 50);
}

#pragma mark - OVERRIDE

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath,
                                                         UICollectionViewLayoutAttributes *attributes,
                                                         BOOL *stop) {
        if (CGRectIntersectsRect(rect, attributes.frame)) {
            [allAttributes addObject:attributes];
        }
    }];
    
    return allAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.layoutInfo[indexPath];
}

- (CGSize)collectionViewContentSize
{
    return CGSizeMake(CGRectGetWidth(self.collectionView.bounds), CGRectGetHeight(self.collectionView.frame));
}

@end