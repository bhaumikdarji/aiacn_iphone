//
//  AgentDetailEducationCollectionViewCell.m
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AgentDetailEducationCollectionViewCell.h"
#import "AgentDetailsEducationView.h"

@interface AgentDetailEducationCollectionViewCell () <UITextViewDelegate>

// UI
@property (nonatomic, strong) UITextField *educationTitleField;
@property (nonatomic, strong) UITextView *educationSubtitleField;
@property (nonatomic, strong) UIButton *dateButton;

@end

@implementation AgentDetailEducationCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
    {
        self.contentView.layer.cornerRadius = 5;
        
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    self.educationTitleField = [UITextField new];
    self.educationTitleField.backgroundColor = [UIColor clearColor];
    self.educationTitleField.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.educationTitleField.textColor = [UIColor whiteColor];
    
    self.educationSubtitleField = [UITextView new];
    self.educationSubtitleField.backgroundColor = [UIColor clearColor];
    self.educationSubtitleField.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    self.educationSubtitleField.textColor = [UIColor whiteColor];
    self.educationSubtitleField.contentInset = UIEdgeInsetsZero;
    self.educationSubtitleField.textContainerInset = UIEdgeInsetsZero;
    self.educationSubtitleField.textContainer.lineFragmentPadding = 0;
    self.educationSubtitleField.delegate = self;
    
    self.dateButton = [UIButton new];
    self.dateButton.backgroundColor = [UIColor clearColor];
    self.dateButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    self.dateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.dateButton.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    [self.dateButton addTarget:self action:@selector(onDateButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:self.educationTitleField];
    [self.contentView addSubview:self.educationSubtitleField];
}

#pragma mark - Button Handler

- (void)onDateButton:(id)sender
{
    if( self.delegate && [self.delegate respondsToSelector:@selector(agentDetailEducationCollectionViewCellDidSelectDatePrompt:)] )
    {
        [self.delegate agentDetailEducationCollectionViewCellDidSelectDatePrompt:self];
    }
}

#pragma mark - Configure

- (void)configureWithData:(AgentDetailEducationModel*)data andOther:(BOOL)other
{
    self.contentView.backgroundColor = other ? [UIColor colorWithRed:0.84 green:0.16 blue:0.34 alpha:1] : [UIColor colorWithRed:0.68 green:0.68 blue:0.71 alpha:1];
    
    self.educationTitleField.text = data.title;
    self.educationSubtitleField.text = data.subTitle;
    [self.dateButton setTitle:data.subTitle forState:UIControlStateNormal];
    
    if( data.isDate )
    {
        [self.contentView addSubview:self.dateButton];
        [self.educationSubtitleField removeFromSuperview];
    }
    else
    {
        [self.contentView addSubview:self.educationSubtitleField];
        [self.dateButton removeFromSuperview];
    }
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // Remove hard-coded placeholder once editing begins
    if( [textView.text isEqualToString:@"-"] )
        textView.text = @"";
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if( self.delegate && [self.delegate respondsToSelector:@selector(agentDetailEducationCollectionViewCellDidEndEditing:)] )
    {
        [self.delegate agentDetailEducationCollectionViewCellDidEndEditing:self];
    }
}

#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat labelPaddingX = 10;
    CGFloat labelPaddingY = 10;

    self.educationTitleField.frame = CGRectMake(labelPaddingX, labelPaddingY, CGRectGetWidth(self.bounds) - 20, self.educationTitleField.font.lineHeight);
    
    self.educationSubtitleField.frame = CGRectMake(labelPaddingX, CGRectGetHeight(self.bounds) - 40 - 5, CGRectGetWidth(self.bounds) - labelPaddingX*2, 40);
    
    self.dateButton.frame = self.educationSubtitleField.frame;
}

@end
