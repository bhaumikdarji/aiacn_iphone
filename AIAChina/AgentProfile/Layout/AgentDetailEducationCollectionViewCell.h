//
//  AgentDetailEducationCollectionViewCell.h
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AgentDetailEducationModel;
@class AgentDetailEducationCollectionViewCell;

@protocol AgentDetailEducationCollectionViewCellDelegate <NSObject>

@optional
- (void)agentDetailEducationCollectionViewCellDidEndEditing:(AgentDetailEducationCollectionViewCell*)cell;
- (void)agentDetailEducationCollectionViewCellDidSelectDatePrompt:(AgentDetailEducationCollectionViewCell *)cell;

@end

@interface AgentDetailEducationCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id <AgentDetailEducationCollectionViewCellDelegate> delegate;
@property (nonatomic, weak) NSIndexPath *indexPath;

// UI
@property (nonatomic, readonly) UITextField *educationTitleField;
@property (nonatomic, readonly) UITextView *educationSubtitleField;
@property (nonatomic, readonly) UIButton *dateButton;

- (void)configureWithData:(AgentDetailEducationModel*)data andOther:(BOOL)other;

@end
