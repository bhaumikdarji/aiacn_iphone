//
//  AgentDetailProfileButton.h
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgentDetailProfileButton : UIButton

@property (nonatomic, readonly) UIImageView *profileImageView;

@end
