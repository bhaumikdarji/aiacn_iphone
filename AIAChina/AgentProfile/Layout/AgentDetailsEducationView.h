//
//  AgentDetailsEducationView.h
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TblAgentDetails;
@class AIAChinaViewController;

@interface AgentDetailsEducationView : UIView

- (id)initWithFrame:(CGRect)frame andAgentDetails:(TblAgentDetails*)agentDetails;

@property (nonatomic, weak) AIAChinaViewController *viewController;

@end

@interface AgentDetailEducationModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subTitle;
@property (nonatomic, assign) BOOL isDate;

@end