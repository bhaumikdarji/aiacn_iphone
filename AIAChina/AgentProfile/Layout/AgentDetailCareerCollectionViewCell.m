//
//  AgentDetailCareerCollectionViewCell.m
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AgentDetailCareerCollectionViewCell.h"
#import "TblCareerProgress.h"

@interface AgentDetailCareerCollectionViewCell ()

// Config


@end

@implementation AgentDetailCareerCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
    {
        self.leftSided = false;
        
        [self initUI];
    }
    return self;
}

- (void)initUI
{    
    self.redLine = [CALayer new];
    self.redLine.backgroundColor = [UIColor colorWithRed:0.82 green:0.13 blue:0.27 alpha:1.0].CGColor;
    self.redLine.frame = CGRectMake(0, 0, 35, 1.0);
    
    self.yearView = [[AgentDetailCareerYearView alloc] initWithFrame:CGRectMake(0, 0, 55, 55)];
    
    self.careerTitleLabel = [UILabel new];
    self.careerTitleLabel.backgroundColor = [UIColor clearColor];
    self.careerTitleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    self.careerTitleLabel.textColor = [UIColor blackColor];
    self.careerTitleLabel.frame = CGRectMake(0, 5, 160, self.careerTitleLabel.font.lineHeight);
    
    self.careerDetailLabel1 = [UILabel new];
    self.careerDetailLabel1.backgroundColor = [UIColor clearColor];
    self.careerDetailLabel1.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.careerDetailLabel1.textColor = [UIColor blackColor];
    self.careerDetailLabel1.frame = CGRectMake(0, 0, 160, self.careerDetailLabel1.font.lineHeight);
    self.careerDetailLabel1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    self.careerDetailLabel2 = [UILabel new];
    self.careerDetailLabel2.backgroundColor = [UIColor clearColor];
    self.careerDetailLabel2.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.careerDetailLabel2.textColor = [UIColor blackColor];
    self.careerDetailLabel2.frame = CGRectMake(0, CGRectGetMaxY(self.careerDetailLabel1.frame) + 2, 160, self.careerDetailLabel2.font.lineHeight);
    self.careerDetailLabel2.autoresizingMask = UIViewAutoresizingFlexibleWidth;

    self.careerDetailLabel3 = [UILabel new];
    self.careerDetailLabel3.backgroundColor = [UIColor clearColor];
    self.careerDetailLabel3.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.careerDetailLabel3.textColor = [UIColor blackColor];
    self.careerDetailLabel3.frame = CGRectMake(0, CGRectGetMaxY(self.careerDetailLabel2.frame) + 2, 160, self.careerDetailLabel3.font.lineHeight);
    self.careerDetailLabel3.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    self.detailContainerView = [UIView new];
    self.detailContainerView.frame = CGRectMake(0, CGRectGetMaxY(self.careerTitleLabel.frame) + 2, 160, 60);
    
    self.divider = [CALayer new];
    self.divider.backgroundColor = [UIColor blackColor].CGColor;
    self.divider.frame = CGRectMake(0, CGRectGetMinY(self.detailContainerView.frame), 4.0, 45);
    
    
    //The event handling method
       [self.contentView.layer addSublayer:self.redLine];
    [self.contentView addSubview:self.yearView];
    [self.contentView addSubview:self.careerTitleLabel];
    [self.contentView.layer addSublayer:self.divider];
    [self.contentView addSubview:self.detailContainerView];
    [self.detailContainerView addSubview:self.careerDetailLabel1];
    [self.detailContainerView addSubview:self.careerDetailLabel2];
    [self.detailContainerView addSubview:self.careerDetailLabel3];
}


#pragma mark - Configure

- (void)configureWithData:(TblCareerProgress*)data andLeftSided:(BOOL)leftSided
{
    static NSDateFormatter *dateFormatter = nil;
    if( dateFormatter == nil )
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM";
    }
    
    CGRect dividerFrame = self.divider.frame;
    CGRect detailContaierFrame = self.detailContainerView.frame;
    CGRect titleFrame = self.careerTitleLabel.frame;
    
    self.yearView.year = [NSString stringWithFormat:@"%lu", [[NSCalendar currentCalendar] component:NSCalendarUnitYear fromDate:data.startDate]];
    self.careerTitleLabel.text = data.position;
    self.careerDetailLabel1.text = [NSString stringWithFormat:@"%@ %@ %@", [dateFormatter stringFromDate:data.startDate], NSLocalizedString(@"to", nil), [dateFormatter stringFromDate:data.endDate]];
    self.careerDetailLabel2.text = data.branch;
    self.careerDetailLabel3.text = data.department;
    
    self.leftSided = leftSided;
    
    if( leftSided )
    {
        self.careerTitleLabel.textAlignment = NSTextAlignmentRight;
        self.careerDetailLabel1.textAlignment = NSTextAlignmentRight;
        self.careerDetailLabel2.textAlignment = NSTextAlignmentRight;
        self.careerDetailLabel3.textAlignment = NSTextAlignmentRight;
        dividerFrame.origin.x = 175;
        detailContaierFrame.origin.x = 10;
        titleFrame.origin.x = CGRectGetMaxX(dividerFrame) - CGRectGetWidth(titleFrame);
    }
    else
    {
        self.careerTitleLabel.textAlignment = NSTextAlignmentLeft;
        self.careerDetailLabel1.textAlignment = NSTextAlignmentLeft;
        self.careerDetailLabel2.textAlignment = NSTextAlignmentLeft;
        self.careerDetailLabel3.textAlignment = NSTextAlignmentLeft;
        dividerFrame.origin.x = 120;
        detailContaierFrame.origin.x = CGRectGetMaxX(dividerFrame) + 3;
        titleFrame.origin.x = CGRectGetMinX(dividerFrame);
    }
    
    self.divider.frame = dividerFrame;
    self.detailContainerView.frame = detailContaierFrame;
    self.careerTitleLabel.frame = titleFrame;
    
    [self layout];
}

- (void)layout
{
    CGRect redLineFrame = self.redLine.frame;
    CGRect yearViewFrame = self.yearView.frame;
    
    redLineFrame.origin.y = CGRectGetHeight(self.bounds)/2;
    yearViewFrame.origin.y = CGRectGetMinY(redLineFrame) - CGRectGetHeight(yearViewFrame)/2;
    
    if( self.leftSided )
    {
        redLineFrame.origin.x = CGRectGetWidth(self.bounds) - CGRectGetWidth(redLineFrame);
        yearViewFrame.origin.x = CGRectGetWidth(self.bounds) - CGRectGetWidth(redLineFrame) - CGRectGetWidth(yearViewFrame);
    }
    else
    {
        redLineFrame.origin.x = 0;
        yearViewFrame.origin.x = CGRectGetMaxX(self.redLine.frame);
    }
    
    self.redLine.frame = redLineFrame;
    self.yearView.frame = yearViewFrame;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self layout];
}

@end

#pragma mark - AgentDetailCareerYearView

@interface AgentDetailCareerYearView ()

// UI
@property (nonatomic, strong) UILabel *yearLabel;

@end

@implementation AgentDetailCareerYearView

- (void)setYear:(NSString *)year
{
    _year = [year copy];
    
    self.yearLabel.text = year;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
    {
        self.yearLabel = [UILabel new];
        self.yearLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
        self.yearLabel.backgroundColor = [UIColor clearColor];
        self.yearLabel.textColor = [UIColor whiteColor];
        self.yearLabel.textAlignment = NSTextAlignmentCenter;
        self.yearLabel.frame = self.bounds;
        
        [self addSubview:self.yearLabel];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextAddEllipseInRect(ctx, rect);
    CGContextSetFillColor(ctx, CGColorGetComponents([[UIColor colorWithRed:0.82 green:0.13 blue:0.27 alpha:1.0] CGColor]));
    CGContextFillPath(ctx);
}

@end