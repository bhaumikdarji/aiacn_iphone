//
//  AgentDetailsPhotosView.h
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AgentDetailsPhotosView : UIView
- (id)initWithFrame:(CGRect)frame withData:(NSArray *) dataSourceArray;
@end

@interface AgentDetailsPhotoCell : UICollectionViewCell

- (void)configureWithData:(id)data;

@end

@interface AgentDetailsPhotosViewLayout : UICollectionViewLayout

@end