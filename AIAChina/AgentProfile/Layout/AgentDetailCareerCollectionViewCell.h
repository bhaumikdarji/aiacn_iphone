//
//  AgentDetailCareerCollectionViewCell.h
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TblCareerProgress,AgentDetailCareerYearView;

@interface AgentDetailCareerCollectionViewCell : UICollectionViewCell
@property (nonatomic, assign) BOOL leftSided;

// UI
@property (nonatomic, strong) CALayer *redLine;
@property (nonatomic, strong) UILabel *careerTitleLabel;
@property (nonatomic, strong) CALayer *divider;
@property (nonatomic, strong) UIView *detailContainerView;
@property (nonatomic, strong) UILabel *careerDetailLabel1;
@property (nonatomic, strong) UILabel *careerDetailLabel2;
@property (nonatomic, strong) UILabel *careerDetailLabel3;
@property (nonatomic, strong) AgentDetailCareerYearView *yearView;
- (void)configureWithData:(TblCareerProgress*)data andLeftSided:(BOOL)leftSided;

@end

@interface AgentDetailCareerYearView : UIView

// Config
@property (nonatomic, copy) NSString *year;

@end
