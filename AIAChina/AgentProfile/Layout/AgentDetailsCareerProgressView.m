//
//  AgenDetailsCareerProgressView.m
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AgentDetailsCareerProgressView.h"
#import "AgentDetailsCareerProgressLayout.h"
#import "AgentDetailCareerCollectionViewCell.h"

@interface AgentDetailsCareerProgressView () <UICollectionViewDataSource, UICollectionViewDelegate>

// UI
@property (nonatomic, strong) UILabel *careerProgressTitleLabel;
@property (nonatomic, strong) UIButton *addCareerButton;
@property (nonatomic, strong) UICollectionView *carerrProgressCollectionView;
@property (nonatomic, strong) CALayer *redLine;

@end

@implementation AgentDetailsCareerProgressView

NSString *const kAgentDetailCareeerProgressCellId = @"agentDetailCareerProgressCellId";
NSString *const kAgentDetailCareeerProgressCellId2 = @"agentDetailCareerProgressCellId2";

#pragma mark - Setter

- (void)setData:(NSArray *)data
{
    _data = data;
    
    [self.carerrProgressCollectionView reloadData];
}

#pragma mark - Initialization

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
    {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    AgentDetailsCareerProgressLayout *flowLayout = [[AgentDetailsCareerProgressLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.itemSize = CGSizeMake(295, 85);
    
    self.careerProgressTitleLabel = [UILabel new];
    self.careerProgressTitleLabel.backgroundColor = [UIColor clearColor];
    self.careerProgressTitleLabel.text = NSLocalizedString(@"Career Progress", nil);
    self.careerProgressTitleLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.careerProgressTitleLabel.textColor = [UIColor grayColor];
    self.careerProgressTitleLabel.frame = CGRectMake(45, 20, CGRectGetWidth(self.frame) - 45*2, self.careerProgressTitleLabel.font.lineHeight);
    
    self.carerrProgressCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.carerrProgressCollectionView.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), 245);
    self.carerrProgressCollectionView.backgroundColor = [UIColor whiteColor];
    self.carerrProgressCollectionView.delegate = self;
    self.carerrProgressCollectionView.dataSource = self;
    [self.carerrProgressCollectionView registerClass:[AgentDetailCareerCollectionViewCell class] forCellWithReuseIdentifier:kAgentDetailCareeerProgressCellId];
    
    [self.carerrProgressCollectionView registerClass:[AgentDetailCareerCollectionViewCell class] forCellWithReuseIdentifier:kAgentDetailCareeerProgressCellId2];
    
    self.redLine = [CALayer new];
    self.redLine.backgroundColor = [UIColor colorWithRed:0.82 green:0.13 blue:0.27 alpha:1.0].CGColor;
    self.redLine.frame = CGRectMake(CGRectGetWidth(self.bounds)/2, 0, 1.0, CGRectGetHeight(self.carerrProgressCollectionView.frame));
    
    self.addCareerButton = [UIButton new];
    self.addCareerButton.layer.cornerRadius = 5;
    self.addCareerButton.backgroundColor = [UIColor lightGrayColor];
    [self.addCareerButton setTitle:NSLocalizedString(@"Add Career", nil) forState:UIControlStateNormal];
    self.addCareerButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    [self.addCareerButton addTarget:self action:@selector(onAddCareerButton:) forControlEvents:UIControlEventTouchUpInside];
    self.addCareerButton.frame = CGRectMake(45, CGRectGetMaxY(self.careerProgressTitleLabel.frame) + 5, 100, 25);
 
    [self addSubview:self.carerrProgressCollectionView];
    [self.layer addSublayer:self.redLine];
    [self addSubview:self.careerProgressTitleLabel];
    [self addSubview:self.addCareerButton];
}

#pragma mark - Button Handler

- (void)onAddCareerButton:(id)sender
{
    if( self.delegate && [self.delegate respondsToSelector:@selector(agentDetailsCareerProgressViewDidSelectAddCareer:edit:indexPath:)] )
        [self.delegate agentDetailsCareerProgressViewDidSelectAddCareer:self edit:NO indexPath:0];
}

#pragma mark - UICollectionViewDelegate/UICollectionViewDataSource

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:false];
    
    NSLog(@"Career selected %lu", indexPath.row);
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AgentDetailCareerCollectionViewCell *cell = nil;
    if( indexPath.row % 2 == 0 )
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAgentDetailCareeerProgressCellId forIndexPath:indexPath];
        
    else
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAgentDetailCareeerProgressCellId2 forIndexPath:indexPath];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [cell.yearView addGestureRecognizer:singleFingerTap];
    cell.yearView.tag = indexPath.row;
    [cell configureWithData:[self.data objectAtIndex:indexPath.row] andLeftSided:indexPath.row % 2 != 0];
    return cell;
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    if( self.delegate && [self.delegate respondsToSelector:@selector(agentDetailsCareerProgressViewDidSelectAddCareer:edit:indexPath:)] )
        [self.delegate agentDetailsCareerProgressViewDidSelectAddCareer:self edit:YES indexPath:recognizer.view.tag];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.count;
}

@end
