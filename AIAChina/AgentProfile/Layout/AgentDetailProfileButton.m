//
//  AgentDetailProfileButton.m
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AgentDetailProfileButton.h"
#import "Utility.h"

@interface AgentDetailProfileButton ()

// UI
@property (nonatomic, strong) UIImageView *profileImageView;
@property (nonatomic, strong) CAShapeLayer *maskLayer;

@end

@implementation AgentDetailProfileButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
    {
        [Utility setBorderAndRoundCorner:self
                             borderColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:.5]
                                   width:8.0
                            cornerRedius:CGRectGetWidth(frame)/2];
        
        [Utility setOuterShadow:self
                          color:[UIColor grayColor]
                        opacity:.8
                         radius:3];
        
        self.profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame))];
        self.profileImageView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
        
        self.maskLayer = [CAShapeLayer layer];
        [self.maskLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame))] CGPath]];
        
        self.profileImageView.layer.mask = self.maskLayer;
        
        [self addSubview:self.profileImageView];
    }
    return self;
}

@end
