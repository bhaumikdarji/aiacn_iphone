//
//  AgentDetailsCareerProgressLayout.m
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AgentDetailsCareerProgressLayout.h"

@interface AgentDetailsCareerProgressLayout ()

// Layout Config
@property (nonatomic, strong) NSMutableDictionary *layoutInfo;

@end

@implementation AgentDetailsCareerProgressLayout

- (void)prepareLayout
{
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    
    NSInteger sectionCount = [self.collectionView numberOfSections];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    for (NSInteger section = 0; section < sectionCount; section++)
    {
        NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < itemCount; item++)
        {
            indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            
            UICollectionViewLayoutAttributes *itemAttributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            itemAttributes.frame = [self frameForItemAtIndexPath:indexPath];
            
            cellLayoutInfo[indexPath] = itemAttributes;
        }
    }
    
    self.layoutInfo = cellLayoutInfo;
}

#pragma mark - Private

- (CGRect)frameForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rightSideX = CGRectGetWidth(self.collectionView.bounds)/2 ;
    CGFloat leftSideX = CGRectGetWidth(self.collectionView.bounds)/2 - self.itemSize.width + 1; // + 1 for red line
    
    CGFloat posX = indexPath.row % 2 == 0 ? rightSideX : leftSideX;
    CGFloat posY = indexPath.row * self.itemSize.height;
    
    return CGRectMake(posX, posY, self.itemSize.width, self.itemSize.height);
}

#pragma mark - OVERRIDE

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath,
                                                      UICollectionViewLayoutAttributes *attributes,
                                                      BOOL *stop) {
        if (CGRectIntersectsRect(rect, attributes.frame)) {
            [allAttributes addObject:attributes];
        }
    }];
    
    return allAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.layoutInfo[indexPath];
}

- (CGSize)collectionViewContentSize
{
    return CGSizeMake(CGRectGetWidth(self.collectionView.bounds), self.itemSize.height * [self.collectionView numberOfItemsInSection:0]);
}

@end
