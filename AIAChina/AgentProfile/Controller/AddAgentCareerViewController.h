//
//  AddAgentCareerViewController.h
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@class AddAgentCareerViewController;
@class TblCareerProgress;

@protocol AddAgentCareerViewControllerDelegate <NSObject>

@optional
- (void)addAgentCareerViewController:(AddAgentCareerViewController*)viewController didAddCareerProgress:(TblCareerProgress*)careerProgress;

@end

@class TblAgentDetails;

@interface AddAgentCareerViewController : AIAChinaViewController

@property (nonatomic, weak) id <AddAgentCareerViewControllerDelegate> delegate;
@property (nonatomic, weak) TblAgentDetails *agentDetails;
@property (nonatomic, strong) TblCareerProgress *careerProgress;
@property (nonatomic, assign) BOOL editCareerProgress;
@end
