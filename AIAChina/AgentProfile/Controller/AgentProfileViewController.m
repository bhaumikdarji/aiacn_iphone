//
//  AgentProfileViewController.m
//  AIAChina
//
//  Created by Burri on 2015-07-18.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AgentProfileViewController.h"
#import "AgentAwardDetailsViewController.h"
#import "TblAgentDetails.h"
#import "DropDownManager.h"
#import "ContactDataProvider.h"
#import "AgentDetailsViewController.h"

@interface AgentProfileViewController ()

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation AgentProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [Utility setBorderAndRoundCorner:self.imageViewBorder borderColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:.5] width:8.0 cornerRedius:self.imageViewBorder.frame.size.width/2];
    
//    [Utility setOuterShadow:self.imageViewBorder color:[UIColor grayColor] opacity:.8 radius:3];
    [Utility setRoundCornerRedius:self.btnAgentProfile cornerRedius:self.btnAgentProfile.frame.size.width/2];
//    self.btnAgentProfile.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self setProfileData];
}

- (void)setProfileData{
    NSString *userID =[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    self.agentProfile = (TblAgentDetails *)[DbUtils fetchObject:@"TblAgentDetails"
                                                     andPredict:[NSPredicate predicateWithFormat:@"userID == %@", userID]
                                              andSortDescriptor:nil
                                           managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    self.agentNameLabel.text = self.agentProfile.userName;
    self.agentCodeLabel.text = self.agentProfile.userID;
    self.agentEmailLabel.text = self.agentProfile.emailId;
    NSDictionary *branchMapping = @{@"0986":@"上海分公司",
                                    @"1086": @"深圳分公司",
                                    @"1186": @"北京分公司",
                                    @"1286": @"江苏分公司",
                                    @"2586": @"广东分公司",
                                    @"2686": @"佛山支公司",
                                    @"2786": @"江门支公司",
                                    @"2886": @"东莞支公司",
                                    @"9986": @"中国区",
                                   };
    NSString *branchDisplayValue = self.agentProfile.branch;
    if([branchMapping valueForKey:self.agentProfile.branch] != nil){
        branchDisplayValue = [branchMapping valueForKey:self.agentProfile.branch];
    }
    self.branchCodeLabel.text = branchDisplayValue;
    self.cityLabel.text = self.agentProfile.city;
    self.rankLabel.text = self.agentProfile.rank;
    self.contactNumberLabel.text = self.agentProfile.contactNumber;
    self.sscLabel.text = self.agentProfile.ssc;
    self.officeLabel.text = self.agentProfile.officeName;
    self.teamLabel.text = self.agentProfile.teamName;
    self.genderLabel.text = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_GENDER code:self.agentProfile.gender];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = TRUE;
    self.navigationItem.rightBarButtonItem = [self prepareRightBarButton:YES];
    self.navigationItem.leftBarButtonItem = [self prepareLeftBarButton:YES withLogo:NO];
    self.navigationItem.title=NSLocalizedString(@"Personal Information", nil);
    [MyAppDelegate showAnnouncements:self.view]; 
    [self.btnAgentProfile setBackgroundImage:(self.agentProfile.agentImage) ? [UIImage imageWithData:self.agentProfile.agentImage] : [UIImage imageNamed:@"male.png"] forState:UIControlStateNormal];
}


- (IBAction)openIBooksApp:(id)sender {
    NSString *stringURL = @"itms-bookss:";
    NSURL *url = [NSURL URLWithString:stringURL];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showAwardDetails:(id)sender{
    [self performSegueWithIdentifier:@"AgentProfileToAgentAward" sender:self];
}

- (IBAction)onBackButtonClick:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onBtnAgentProfileClick:(id)sender {
    [self showPhotoActionSheet:sender withCircularCrop1:YES];
}

- (IBAction)onBtnAgentDetailClick:(id)sender
{
    AgentDetailsViewController *detailVC = [[AgentDetailsViewController alloc] init];
    detailVC.view.frame = self.view.bounds;
    [self.navigationController pushViewController:detailVC animated:true];
}



- (void)handleImagePicker:(UIImagePickerController *)picker withMediaInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    if(info!=nil) {
        UIImage *profileImage = info[UIImagePickerControllerEditedImage];
        if (!profileImage) profileImage = info[UIImagePickerControllerOriginalImage];
        profileImage = [self resizeImage:profileImage];
        
        [self.btnAgentProfile setImage:profileImage forState:UIControlStateNormal];
        
        self.agentProfile.agentImage = UIImagePNGRepresentation(self.btnAgentProfile.currentImage);
        [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    }
}

- (void) imagePickerController:(UIImagePickerController *)thePicker didFinishPickingMediaWithInfo:(NSDictionary *)imageInfo{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil) {
        UIImage *profileImage = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
        [self.btnAgentProfile setImage:profileImage forState:UIControlStateNormal];
        
        self.agentProfile.agentImage = UIImagePNGRepresentation(self.btnAgentProfile.currentImage);
        [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
}

@end
