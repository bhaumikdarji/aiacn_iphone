//
//  AddAgentAwardController.h
//  AIAChina
//
//  Created by MacMini on 13/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactDataProvider.h"

@interface AddAgentAwardController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnAddSave;
@property (weak, nonatomic) IBOutlet UIImageView *imgAwardPic;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPhoto;
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDescription;

@property (nonatomic, strong) ContactDataProvider *agentAwardDataProvider;
@property (strong, nonatomic) UIPopoverController *addAgentCtrl;
@property (strong, nonatomic) TblAward *award;
@property (copy, nonatomic) void(^reloadAwardBlock)();

- (IBAction)onBtnDateClick:(id)sender;
- (IBAction)onBtnAddPhotoClick:(id)sender;
- (IBAction)onBtnCancelClick:(id)sender;
- (IBAction)onBtnAddClick:(id)sender;

@end
