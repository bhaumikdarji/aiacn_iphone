//
//  AgentDetailsViewController.m
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AgentDetailsViewController.h"
#import "AgentDetailsEducationView.h"
#import "AgentDetailsCareerProgressView.h"
#import "AgentDetailProfileButton.h"
#import "AgentDetailsPhotosView.h"
#import "AddAgentCareerViewController.h"
#import "TblAgentDetails.h"
#import "DbUtils.h"
#import "CachingService.h"

@interface AgentDetailsViewController () <AgentDetailsCareerProgressViewDelegate,
AddAgentCareerViewControllerDelegate, UITextViewDelegate>
// UI
@property (nonatomic, readonly) UIScrollView *scrollView;
@property (nonatomic, weak) UIView *activeField;
@property (nonatomic, strong) UIView *leftContainerView;
@property (nonatomic, strong) UIView *rightContainerView;
// >LeftContainer
@property (nonatomic, strong) CALayer *detailTitleBackground;
@property (nonatomic, strong) UILabel *detailTitleLabel;
@property (nonatomic, strong) AgentDetailsEducationView *educationView;
@property (nonatomic, strong) AgentDetailsCareerProgressView *careerProgressView;
@property (nonatomic, strong) UIView *freeTextContainerView;
@property (nonatomic, strong) UILabel *freeTextTitleLabel;
@property (nonatomic, strong) UITextView *freeTextDescriptionView;
// >RightContainer
@property (nonatomic, strong) AgentDetailProfileButton *profileButton;
@property (nonatomic, strong) UILabel *agentNameLabel;
@property (nonatomic, strong) UILabel *agentGenderLabel;
@property (nonatomic, strong) UILabel *agentCodeLabel;
@property (nonatomic, strong) UILabel *photosTitleLabel;
@property (nonatomic, strong) AgentDetailsPhotosView *photosView;

// Data
@property (nonatomic, strong) TblAgentDetails *agentProfile;

// Gesture
@property (nonatomic, strong) UITapGestureRecognizer *keyboardCancelGesture;

@end

@implementation AgentDetailsViewController

#pragma mark - Getter

- (UIScrollView*)scrollView
{
    return (UIScrollView*)self.view;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if( self )
    {
        self.automaticallyAdjustsScrollViewInsets = false;
        self.activeField = nil;
    }
    return self;
}

#pragma mark - View Management

- (void)loadView
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    self.view = scrollView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.translatesAutoresizingMaskIntoConstraints = true;
    
    self.navigationItem.title = NSLocalizedString(@"Agent Details", @"");
    
    self.view.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
    
    NSString *userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    self.agentProfile = (TblAgentDetails *)[DbUtils fetchObject:@"TblAgentDetails"
                                                     andPredict:[NSPredicate predicateWithFormat:@"userID == %@", userID]
                                              andSortDescriptor:nil
                                           managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    self.keyboardCancelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapGesture:)];
    self.keyboardCancelGesture.cancelsTouchesInView = false;
    [self.view addGestureRecognizer:self.keyboardCancelGesture];
    
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = FALSE;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)initUI
{
    [self initLeftContainer];
    [self initRightContainer];
}

- (void)initLeftContainer
{
    self.leftContainerView = [UIView new];
    self.leftContainerView.frame = CGRectMake(20, 84, 750, 655);
    self.leftContainerView.backgroundColor = [UIColor colorWithRed:0.87 green:0.87 blue:0.89 alpha:1];
    self.leftContainerView.layer.cornerRadius = 5;
    self.leftContainerView.clipsToBounds = true;
    
    self.detailTitleLabel = [UILabel new];
    self.detailTitleLabel.backgroundColor = [UIColor clearColor];
    self.detailTitleLabel.text = NSLocalizedString(@"Detail", nil);
    self.detailTitleLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.detailTitleLabel.textColor = [UIColor grayColor];
    self.detailTitleLabel.frame = CGRectMake(10, 0, CGRectGetWidth(self.leftContainerView.frame) - 10, 30);
    
    self.detailTitleBackground = [CALayer new];
    self.detailTitleBackground.backgroundColor = [UIColor colorWithRed:0.89 green:0.9 blue:0.91 alpha:1].CGColor;
    self.detailTitleBackground.frame = CGRectMake(0, 0, CGRectGetWidth(self.leftContainerView.frame), 30);
    
    // Education
    
    self.educationView = [[AgentDetailsEducationView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.detailTitleBackground.frame), CGRectGetWidth(self.leftContainerView.frame), 170) andAgentDetails:self.agentProfile];
    self.educationView.viewController = self;
    
    // Career Progress
    
    self.careerProgressView = [[AgentDetailsCareerProgressView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.educationView.frame), CGRectGetWidth(self.leftContainerView.frame), 245)];
    self.careerProgressView.delegate = self;
    
    // Free Text
    
    self.freeTextContainerView = [UIView new];
    self.freeTextContainerView.backgroundColor = [UIColor whiteColor];
    self.freeTextContainerView.frame = CGRectMake(0, CGRectGetMaxY(self.careerProgressView.frame) + 10, CGRectGetWidth(self.leftContainerView.frame), CGRectGetHeight(self.leftContainerView.frame) - CGRectGetMaxY(self.careerProgressView.frame));
    
    self.freeTextTitleLabel = [UILabel new];
    self.freeTextTitleLabel.backgroundColor = [UIColor clearColor];
    self.freeTextTitleLabel.text = NSLocalizedString(@"Free Text", nil);
    self.freeTextTitleLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.freeTextTitleLabel.textColor = [UIColor grayColor];
    self.freeTextTitleLabel.frame = CGRectMake(45, 20, CGRectGetWidth(self.freeTextContainerView.frame) - 45*2, self.freeTextTitleLabel.font.lineHeight);
    
    self.freeTextDescriptionView = [UITextView new];
    self.freeTextDescriptionView.backgroundColor = [UIColor clearColor];
    self.freeTextDescriptionView.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.freeTextDescriptionView.textColor = [UIColor grayColor];
    self.freeTextDescriptionView.text = self.agentProfile.freeText;
    self.freeTextDescriptionView.contentInset = UIEdgeInsetsZero;
    self.freeTextDescriptionView.textContainerInset = UIEdgeInsetsZero;
    self.freeTextDescriptionView.textContainer.lineFragmentPadding = 0;
    self.freeTextDescriptionView.delegate = self;
    self.freeTextDescriptionView.frame = CGRectMake(75, CGRectGetMaxY(self.freeTextTitleLabel.frame) + 10, CGRectGetWidth(self.freeTextContainerView.frame) - 75*2, CGRectGetHeight(self.freeTextContainerView.frame) - CGRectGetMaxY(self.freeTextTitleLabel.frame) - 40);
    
    [self.freeTextContainerView addSubview:self.freeTextTitleLabel];
    [self.freeTextContainerView addSubview:self.freeTextDescriptionView];
    
    // View Management
    
    [self.view addSubview:self.leftContainerView];
    [self.leftContainerView.layer addSublayer:self.detailTitleBackground];
    [self.leftContainerView addSubview:self.detailTitleLabel];
    [self.leftContainerView addSubview:self.educationView];
    [self.leftContainerView addSubview:self.careerProgressView];
    [self.leftContainerView addSubview:self.freeTextContainerView];
    
    [self loadCareeProgress];
}

- (void)initRightContainer
{
    self.rightContainerView = [UIView new];
    self.rightContainerView.frame = CGRectMake(CGRectGetMaxX(self.leftContainerView.frame) + 20, CGRectGetMinY(self.leftContainerView.frame), CGRectGetWidth(self.view.bounds) - CGRectGetMaxX(self.leftContainerView.frame) - 40, 580);
    self.rightContainerView.backgroundColor = [UIColor whiteColor];
    self.rightContainerView.layer.cornerRadius = 5;
    
    self.profileButton = [[AgentDetailProfileButton alloc] initWithFrame:CGRectMake(20, 20, CGRectGetWidth(self.rightContainerView.frame) - 40, CGRectGetWidth(self.rightContainerView.frame) - 40)];
    self.profileButton.profileImageView.image = (self.agentProfile.agentImage) ? [UIImage imageWithData:self.agentProfile.agentImage] : nil;
    [self.profileButton addTarget:self action:@selector(onProfileButton:) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat contentPaddingX = 20;
    CGFloat contentMaxWidth = CGRectGetWidth(self.rightContainerView.frame) - contentPaddingX*2;
    
    self.agentNameLabel = [UILabel new];
    self.agentNameLabel.backgroundColor = [UIColor clearColor];
    self.agentNameLabel.textColor = [UIColor blackColor];
    self.agentNameLabel.font = [UIFont fontWithName:@"Helvetica" size:22.0];
    self.agentNameLabel.text = [self.agentProfile.userName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    CGRect agentNameRect = [self.agentNameLabel.text boundingRectWithSize:CGSizeMake(contentMaxWidth - 30, 26) options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:self.agentNameLabel.font} context:nil];
    
    self.agentGenderLabel = [UILabel new];
    self.agentGenderLabel.backgroundColor = [UIColor clearColor];
    self.agentGenderLabel.textColor = [UIColor blackColor];
    self.agentGenderLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.agentGenderLabel.text = self.agentProfile.gender;
    CGRect agentGenderRect = [self.agentGenderLabel.text boundingRectWithSize:CGSizeMake(100, 100) options:(NSStringDrawingUsesFontLeading | NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:self.agentGenderLabel.font} context:nil];
    
    self.agentNameLabel.frame = CGRectMake(contentPaddingX + (contentMaxWidth - agentNameRect.size.width - agentGenderRect.size.width - 5)/2, CGRectGetMaxY(self.profileButton.frame) + 10, agentNameRect.size.width, agentNameRect.size.height);
    self.agentGenderLabel.frame = CGRectMake(CGRectGetMaxX(self.agentNameLabel.frame) + 5, CGRectGetMaxY(self.agentNameLabel.frame) - self.agentGenderLabel.font.lineHeight - 2, agentGenderRect.size.width, self.agentGenderLabel.font.lineHeight);
    
    self.agentCodeLabel = [UILabel new];
    self.agentCodeLabel.backgroundColor = [UIColor clearColor];
    self.agentCodeLabel.textColor = [UIColor lightGrayColor];
    self.agentCodeLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.agentCodeLabel.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Agent Code", nil), self.agentProfile.userID];
    self.agentCodeLabel.textAlignment = NSTextAlignmentCenter;
    self.agentCodeLabel.frame = CGRectMake(contentPaddingX, CGRectGetMaxY(self.agentNameLabel.frame) + 2, contentMaxWidth, self.agentCodeLabel.font.lineHeight);
    
    self.photosTitleLabel = [UILabel new];
    self.photosTitleLabel.backgroundColor = [UIColor clearColor];
    self.photosTitleLabel.textColor = [UIColor lightGrayColor];
    self.photosTitleLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    self.photosTitleLabel.text = NSLocalizedString(@"Photos", nil);
    self.photosTitleLabel.frame = CGRectMake(contentPaddingX, CGRectGetMaxY(self.agentCodeLabel.frame) + 20, contentMaxWidth, self.photosTitleLabel.font.lineHeight);
    
    self.photosView = [[AgentDetailsPhotosView alloc] initWithFrame:CGRectMake(contentPaddingX, CGRectGetMaxY(self.photosTitleLabel.frame) + 5, contentMaxWidth, 265) withData:self.agentProfile.agentGallayImages];
    
    [self.view addSubview:self.rightContainerView];
    [self.rightContainerView addSubview:self.profileButton];
    [self.rightContainerView addSubview:self.agentNameLabel];
    [self.rightContainerView addSubview:self.agentGenderLabel];
    [self.rightContainerView addSubview:self.agentCodeLabel];
//    [self.rightContainerView addSubview:self.photosTitleLabel];
    [self.rightContainerView addSubview:self.photosView];
}

- (void)loadCareeProgress
{
    NSArray *careerProgress = [[self.agentProfile.agentCareerProgress allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:TRUE]]];    
    self.careerProgressView.data = careerProgress;
}

#pragma mark - Button Handler

- (void)onProfileButton:(id)sender
{
    [self showPhotoActionSheet:sender withCircularCrop1:YES];
}

#pragma mark - Gestuer Handler

- (void)onTapGesture:(id)sender
{
    [self.view endEditing:true];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.activeField = textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.activeField = nil;
    
    self.agentProfile.freeText = textView.text;
    
    [[CachingService sharedInstance] saveContext];
}

#pragma mark - AgentDetailsCareerProgressViewDelegate

- (void)agentDetailsCareerProgressViewDidSelectAddCareer:(AgentDetailsCareerProgressView *)view edit:(BOOL)editing indexPath:(NSInteger)row
{
    AddAgentCareerViewController *vc = [[AddAgentCareerViewController alloc] init];
    vc.delegate = self;
    vc.agentDetails = self.agentProfile;
     NSArray *careerProgress = [[self.agentProfile.agentCareerProgress allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:TRUE]]];
    if (editing) {
        vc.careerProgress = [careerProgress objectAtIndex:row];
    }
    vc.editCareerProgress = editing;
    vc.view.frame = CGRectMake(0, 0, 640, 285);
    vc.preferredContentSize = CGSizeMake(640, 285);
    vc.modalPresentationStyle = UIModalPresentationPopover;
    vc.popoverPresentationController.sourceView = self.view;
    vc.popoverPresentationController.sourceRect = CGRectMake(100, 300, 100, 100);
    vc.popoverPresentationController.permittedArrowDirections = 0;
    
    [self presentViewController:vc animated:true completion:nil];
}

#pragma mark - AddAgentCareerViewControllerDelegate

- (void)addAgentCareerViewController:(AddAgentCareerViewController *)viewController didAddCareerProgress:(TblCareerProgress *)careerProgress
{
    [self loadCareeProgress];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)handleImagePicker:(UIImagePickerController *)thePicker withMediaInfo:(NSDictionary *)imageInfo
{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil)
    {
       UIImage *profileImage = imageInfo[UIImagePickerControllerEditedImage];
        if (!profileImage) profileImage = imageInfo[UIImagePickerControllerOriginalImage];
        profileImage = [self resizeImage:profileImage];

        [self.profileButton.profileImageView setImage:profileImage];
        
        self.agentProfile.agentImage = UIImagePNGRepresentation(profileImage);
        [[CachingService sharedInstance] saveContext];
    }
}

- (void)imagePickerController:(UIImagePickerController *)thePicker didFinishPickingMediaWithInfo:(NSDictionary *)imageInfo
{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil)
    {
        UIImage *profileImage = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
        [self.profileButton.profileImageView setImage:profileImage];
        
        self.agentProfile.agentImage = UIImagePNGRepresentation(profileImage);
        [[CachingService sharedInstance] saveContext];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Keyboard Notification Handlers

- (void)onKeyboardDidShow:(NSNotification*)notification
{
    if( self.activeField )
    {
        NSDictionary* info = [notification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        CGRect aRect = self.scrollView.frame;
        aRect.size.height -= kbSize.height;
        
        CGRect convertedRect = [self.activeField.superview convertRect:self.activeField.frame toView:self.scrollView];
        convertedRect.origin.x = 0;
        
        if ( !CGRectContainsRect(aRect, convertedRect) )
        {
            [self.scrollView setContentOffset:CGPointMake(0, CGRectGetMaxY(convertedRect) - (aRect.size.height - 15)) animated:true];
        }
    }
}

- (void)onKeyboardDidHide:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

@end
