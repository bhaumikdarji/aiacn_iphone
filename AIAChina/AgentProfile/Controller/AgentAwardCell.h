//
//  AgentAwardCell.h
//  AIAChina
//
//  Created by MacMini on 14/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgentAwardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgAward;
@property (weak, nonatomic) IBOutlet UILabel *lblAwardTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

- (void)loadAgetAward:(TblAward *)award;

@end
