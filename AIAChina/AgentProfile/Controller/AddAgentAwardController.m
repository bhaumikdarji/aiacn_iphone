//
//  AddAgentAwardController.m
//  AIAChina
//
//  Created by MacMini on 13/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AddAgentAwardController.h"
#import "DatePopoverController.h"

@interface AddAgentAwardController ()

@end

@implementation AddAgentAwardController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Add Agent Award", @"");
    self.agentAwardDataProvider = [ContactDataProvider sharedContactDataProvider];
    [Utility setBorderAndRoundCorner:self.imgAwardPic
                         borderColor:[UIColor colorWithRed:203.0/255.0
                                                     green:208.0/255.0
                                                      blue:213.0/255.0
                                                     alpha:1.0]
                               width:2.0
                        cornerRedius:self.imgAwardPic.frame.size.width/2];
    
    [Utility setBorderAndRoundCorner:self.txtViewDescription
                         borderColor:[UIColor colorWithRed:203.0/255.0
                                                     green:208.0/255.0
                                                      blue:213.0/255.0
                                                     alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    [self loadAwardData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnDateClick:(id)sender {
    [self showDatePopoverCtrl:UIDatePickerModeDate
                  minimumDate:nil
                  maximumDate:nil
                         rect:self.btnDate.frame
                       inView:self.view
                   pickerDate:^(NSDate *date) {
                       [self.btnDate setTitle:[DateUtils dateToString:date andFormate:kDisplayDateFormatte] forState:UIControlStateNormal];
                   }];
}

- (void)handleImagePicker:(UIImagePickerController *)thePicker withMediaInfo:(NSDictionary *)imageInfo
{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil) {
       UIImage *profileImage = imageInfo[UIImagePickerControllerEditedImage];
        if (!profileImage) profileImage = imageInfo[UIImagePickerControllerOriginalImage];
        profileImage = [self resizeImage:profileImage];
        self.imgAwardPic.image = profileImage;
        [self.btnAddPhoto setTitle:@"" forState:UIControlStateNormal];
    }
}

- (void)imagePickerController:(UIImagePickerController *)thePicker didFinishPickingMediaWithInfo:(NSDictionary *)imageInfo{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    if(imageInfo!=nil) {
        UIImage *profileImage = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
        self.imgAwardPic.image = profileImage;
        [self.btnAddPhoto setTitle:@"" forState:UIControlStateNormal];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onBtnAddPhotoClick:(id)sender {
    [self showPhotoActionSheet:sender withCircularCrop1:YES];
}

- (IBAction)onBtnCancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onBtnAddClick:(id)sender {
    if([self validation]){
        [self addAwardLocalDB];
        if(self.reloadAwardBlock)
            self.reloadAwardBlock();
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)addAwardLocalDB{
    if(!self.award)
        self.award = [self.agentAwardDataProvider createAward];
    
    if(self.imgAwardPic.image)
        self.award.awardImage = UIImagePNGRepresentation(self.imgAwardPic.image);
        
    self.award.awardTitle = self.txtTitle.text;
    self.award.awardDate = [DateUtils stringToDate:self.btnDate.currentTitle dateFormat:kDisplayDateFormatte];
    self.award.awardDesc = self.txtViewDescription.text;
    self.award.isDelete = [NSNumber numberWithBool:FALSE];
    [self.agentAwardDataProvider saveLocalDBData];
}

- (BOOL)validation{
    NSString *msg = nil;
    if(isEmptyString(self.txtTitle.text))
        msg = NSLocalizedString(@"Please add award title", @"")  ;
    else if(isEmptyString(self.btnDate.currentTitle))
        msg = NSLocalizedString(@"Please add date", @"");
    
    if(!isEmptyString(msg)){
        [DisplayAlert showAlertInControllerInView:self
                                            title:NSLocalizedString(@"Error", @"")
                                          message:msg
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return FALSE;
    }
    return TRUE;
}

- (void)loadAwardData{
    if(self.award){
        self.lblTitle.text = NSLocalizedString(@"Edit Agent Award", @"");
        [self.btnAddSave setTitle:NSLocalizedString(@"Save",@"") forState:UIControlStateNormal];
        if(self.award.awardImage){
            self.imgAwardPic.image = [UIImage imageWithData:self.award.awardImage];
            [self.btnAddPhoto setTitle:@"" forState:UIControlStateNormal];
        }
        
        self.txtTitle.text = self.award.awardTitle;
        [self.btnDate setTitle:[DateUtils dateToString:self.award.awardDate andFormate:@"dd-MMM-yyyy"] forState:UIControlStateNormal];
        self.txtViewDescription.text = self.award.awardDesc;
    }
}

@end
