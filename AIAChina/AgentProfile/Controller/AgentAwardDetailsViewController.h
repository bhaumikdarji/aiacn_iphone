//
//  AgentAwardDetailsViewController.h
//  AIAChina
//
//  Created by Burri on 2015-07-18.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgentAwardDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblAgentAward;


@property (strong, nonatomic) NSArray *awardArray;
@property (strong, nonatomic) UIPopoverController *agentPopoverCtrl;

- (IBAction)onBtnAddClick:(id)sender;

@end
