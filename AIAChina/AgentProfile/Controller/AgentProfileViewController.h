//
//  AgentProfileViewController.h
//  AIAChina
//
//  Created by Burri on 2015-07-18.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgentProfileViewController : AIAChinaViewController
@property (weak, nonatomic) IBOutlet UIView *imageViewBorder;
@property (weak, nonatomic) IBOutlet UIButton *btnAgentProfile;
@property (weak, nonatomic) IBOutlet UIButton *agentDetailsButton;
@property (weak, nonatomic) IBOutlet UIButton *awardsDetailsButton;
@property (weak, nonatomic) IBOutlet UIButton *iBooksButton;
@property (weak, nonatomic) IBOutlet UILabel *agentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;

@property (weak, nonatomic) IBOutlet UILabel *agentCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (weak, nonatomic) IBOutlet UILabel *branchCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *agentEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *officeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sscLabel;
@property (weak, nonatomic) IBOutlet UILabel *teamLabel;
@property (strong, nonatomic) TblAgentDetails *agentProfile;

@end
