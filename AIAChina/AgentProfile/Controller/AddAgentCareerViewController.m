//
//  AddAgentCareerViewController.m
//  AIAChina
//
//  Copyright © 2015 AIA. All rights reserved.
//

#import "AddAgentCareerViewController.h"
#import "TblCareerProgress.h"
#import "ContactDataProvider.h"
#import "Constants.h"
#import "CachingService.h"

@interface AddAgentCareerViewController ()

// Config
@property (nonatomic, assign) CGFloat contentMaxWidth;
@property (nonatomic, assign) CGFloat contentPadding;

// UI
@property (nonatomic, strong) UILabel *addCareerTitleLabel;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) UILabel *positionTitleLabel;
@property (nonatomic, strong) UITextField *positionTextField;
@property (nonatomic, strong) CALayer *positionTitleDivider;
@property (nonatomic, strong) UILabel *startDateTitleLabel;
@property (nonatomic, strong) UIButton *startDateButton;
@property (nonatomic, strong) CALayer *startDateDivider;
@property (nonatomic, strong) UILabel *endDateTitleLabel;
@property (nonatomic, strong) UIButton *endDateButton;
@property (nonatomic, strong) CALayer *endDateDividerLayer;
@property (nonatomic, strong) UILabel *branchTitleLabel;
@property (nonatomic, strong) UITextField *branchTextField;
@property (nonatomic, strong) CALayer *branchDividerLayer;
@property (nonatomic, strong) UILabel *departmentTitleLabel;
@property (nonatomic, strong) UITextField *deparmentTextField;
@property (nonatomic, strong) CALayer *departmentDividerLayer;

// Data

@end

@implementation AddAgentCareerViewController

#pragma mark - Initiailization

- (id)init
{
    self = [super init];
    if( self )
    {
        self.contentPadding = 10;
        self.contentMaxWidth = 645;
    }
    return self;
}

#pragma mark - View Management

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initUI];
    if (self.editCareerProgress)
    {
        self.positionTextField.text = self.careerProgress.position;
        [self.startDateButton setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:self.careerProgress.startDate andFormate:kDisplayDateFormatte]] forState:UIControlStateNormal];
        [self.endDateButton setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:self.careerProgress.endDate andFormate:kDisplayDateFormatte]] forState:UIControlStateNormal];
        self.branchTextField.text = self.careerProgress.branch;
        self.deparmentTextField.text = self.careerProgress.department;
    }
}

- (void)initUI
{
    self.addCareerTitleLabel = [UILabel new];
    self.addCareerTitleLabel.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:23.0/255.0 blue:67.0/255.0 alpha:1];
    self.addCareerTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.addCareerTitleLabel.text = NSLocalizedString(@"Add Career", nil);
    self.addCareerTitleLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:18.0];
    self.addCareerTitleLabel.textColor = [UIColor whiteColor];
    self.addCareerTitleLabel.frame = CGRectMake(0, 0, self.contentMaxWidth, 40);
    
    self.cancelButton = [UIButton new];
    self.cancelButton.backgroundColor = [UIColor clearColor];
    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:18.0];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.cancelButton addTarget:self action:@selector(onCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    self.cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.cancelButton.frame = CGRectMake(self.contentPadding, 0, 100, 40);
    
    self.doneButton = [UIButton new];
    self.doneButton.backgroundColor = [UIColor clearColor];
    self.doneButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:18.0];
    [self.doneButton setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
    [self.doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.doneButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [self.doneButton addTarget:self action:@selector(onDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.doneButton.frame = CGRectMake(self.contentMaxWidth - 100 - self.contentPadding - 10, 0, 100, 40);
    
    CGRect titleLabelRect;
    
    self.positionTitleLabel = [UILabel new];
    self.positionTitleLabel.backgroundColor = [UIColor clearColor];
    self.positionTitleLabel.textColor = [UIColor lightGrayColor];
    self.positionTitleLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    self.positionTitleLabel.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"Position", nil)];
    titleLabelRect = [self.positionTitleLabel.text boundingRectWithSize:CGSizeMake(self.contentMaxWidth/2, self.positionTitleLabel.font.lineHeight) options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:self.positionTitleLabel.font} context:nil];
    self.positionTitleLabel.frame = CGRectMake(20, CGRectGetMaxY(self.addCareerTitleLabel.frame) + 20, CGRectGetWidth(titleLabelRect), self.positionTitleLabel.font.lineHeight);
    
    self.positionTextField = [UITextField new];
    self.positionTextField.backgroundColor = [UIColor clearColor];
    self.positionTextField.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    self.positionTextField.frame = CGRectMake(CGRectGetMaxX(self.positionTitleLabel.frame) + 5, CGRectGetMinY(self.positionTitleLabel.frame), self.contentMaxWidth - CGRectGetMaxX(self.positionTitleLabel.frame) - 20, self.positionTextField.font.lineHeight);
    [self.positionTextField addTarget:self action:@selector(onTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.positionTitleDivider = [CALayer new];
    self.positionTitleDivider.backgroundColor = [UIColor lightGrayColor].CGColor;
    self.positionTitleDivider.frame = CGRectMake(10, CGRectGetMaxY(self.positionTitleLabel.frame) + 10, self.contentMaxWidth - 20, 1);
    
    self.startDateTitleLabel = [UILabel new];
    self.startDateTitleLabel.backgroundColor = [UIColor clearColor];
    self.startDateTitleLabel.textColor = [UIColor lightGrayColor];
    self.startDateTitleLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    self.startDateTitleLabel.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"Start Date", nil)];
    titleLabelRect = [self.startDateTitleLabel.text boundingRectWithSize:CGSizeMake(self.contentMaxWidth/2, self.startDateTitleLabel.font.lineHeight) options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:self.startDateTitleLabel.font} context:nil];
    self.startDateTitleLabel.frame = CGRectMake(20, CGRectGetMaxY(self.positionTitleDivider.frame) + 10, CGRectGetWidth(titleLabelRect), self.startDateTitleLabel.font.lineHeight);
    
    self.startDateButton = [UIButton new];
    self.startDateButton.backgroundColor = [UIColor clearColor];
    self.startDateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.startDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.startDateButton addTarget:self action:@selector(onStartDateButton:) forControlEvents:UIControlEventTouchUpInside];
    self.startDateButton.frame = CGRectMake(CGRectGetMaxX(self.startDateTitleLabel.frame) + 5, CGRectGetMinY(self.startDateTitleLabel.frame), self.contentMaxWidth - CGRectGetMaxX(self.startDateTitleLabel.frame) - 20, CGRectGetHeight(self.startDateTitleLabel.frame));
    
    self.startDateDivider = [CALayer new];
    self.startDateDivider.backgroundColor = [UIColor lightGrayColor].CGColor;
    self.startDateDivider.frame = CGRectMake(10, CGRectGetMaxY(self.startDateTitleLabel.frame) + 10, self.contentMaxWidth - 20, 1);
    
    self.endDateTitleLabel = [UILabel new];
    self.endDateTitleLabel.backgroundColor = [UIColor clearColor];
    self.endDateTitleLabel.textColor = [UIColor lightGrayColor];
    self.endDateTitleLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    self.endDateTitleLabel.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"End Date", nil)];
    titleLabelRect = [self.endDateTitleLabel.text boundingRectWithSize:CGSizeMake(self.contentMaxWidth/2, self.endDateTitleLabel.font.lineHeight) options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:self.endDateTitleLabel.font} context:nil];
    self.endDateTitleLabel.frame = CGRectMake(20, CGRectGetMaxY(self.startDateDivider.frame) + 10, CGRectGetWidth(titleLabelRect), self.endDateTitleLabel.font.lineHeight);
    
    self.endDateButton = [UIButton new];
    self.endDateButton.backgroundColor = [UIColor clearColor];
    self.endDateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.endDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.endDateButton addTarget:self action:@selector(onEndDateButton:) forControlEvents:UIControlEventTouchUpInside];
    self.endDateButton.frame = CGRectMake(CGRectGetMaxX(self.endDateTitleLabel.frame) + 5, CGRectGetMinY(self.endDateTitleLabel.frame), self.contentMaxWidth - CGRectGetMaxX(self.endDateTitleLabel.frame) - 20, CGRectGetHeight(self.endDateTitleLabel.frame));
    
    self.endDateDividerLayer = [CALayer new];
    self.endDateDividerLayer.backgroundColor = [UIColor lightGrayColor].CGColor;
    self.endDateDividerLayer.frame = CGRectMake(10, CGRectGetMaxY(self.endDateTitleLabel.frame) + 10, self.contentMaxWidth - 20, 1);
    
    self.branchTitleLabel = [UILabel new];
    self.branchTitleLabel.backgroundColor = [UIColor clearColor];
    self.branchTitleLabel.textColor = [UIColor lightGrayColor];
    self.branchTitleLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    self.branchTitleLabel.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"Branch", nil)];
    titleLabelRect = [self.branchTitleLabel.text boundingRectWithSize:CGSizeMake(self.contentMaxWidth/2, self.branchTitleLabel.font.lineHeight) options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:self.branchTitleLabel.font} context:nil];
    self.branchTitleLabel.frame = CGRectMake(20, CGRectGetMaxY(self.endDateDividerLayer.frame) + 10, CGRectGetWidth(titleLabelRect), self.branchTitleLabel.font.lineHeight);
    
    self.branchTextField = [UITextField new];
    self.branchTextField.backgroundColor = [UIColor clearColor];
    self.branchTextField.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    self.branchTextField.frame = CGRectMake(CGRectGetMaxX(self.branchTitleLabel.frame) + 5, CGRectGetMinY(self.branchTitleLabel.frame), self.contentMaxWidth - CGRectGetMaxX(self.branchTitleLabel.frame) - 20, self.branchTextField.font.lineHeight);
    [self.branchTextField addTarget:self action:@selector(onTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.branchDividerLayer = [CALayer new];
    self.branchDividerLayer.backgroundColor = [UIColor lightGrayColor].CGColor;
    self.branchDividerLayer.frame = CGRectMake(10, CGRectGetMaxY(self.branchTitleLabel.frame) + 10, self.contentMaxWidth - 20, 1);
    
    self.departmentTitleLabel = [UILabel new];
    self.departmentTitleLabel.backgroundColor = [UIColor clearColor];
    self.departmentTitleLabel.textColor = [UIColor lightGrayColor];
    self.departmentTitleLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    self.departmentTitleLabel.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"Department", nil)];
    titleLabelRect = [self.departmentTitleLabel.text boundingRectWithSize:CGSizeMake(self.contentMaxWidth/2, self.departmentTitleLabel.font.lineHeight) options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:self.departmentTitleLabel.font} context:nil];
    self.departmentTitleLabel.frame = CGRectMake(20, CGRectGetMaxY(self.branchDividerLayer.frame) + 10, CGRectGetWidth(titleLabelRect), self.departmentTitleLabel.font.lineHeight);
    
    self.deparmentTextField = [UITextField new];
    self.deparmentTextField.backgroundColor = [UIColor clearColor];
    self.deparmentTextField.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    self.deparmentTextField.frame = CGRectMake(CGRectGetMaxX(self.departmentTitleLabel.frame) + 5, CGRectGetMinY(self.departmentTitleLabel.frame), self.contentMaxWidth - CGRectGetMaxX(self.departmentTitleLabel.frame) - 20, self.deparmentTextField.font.lineHeight);
    [self.deparmentTextField addTarget:self action:@selector(onTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.departmentDividerLayer = [CALayer new];
    self.departmentDividerLayer.backgroundColor = [UIColor lightGrayColor].CGColor;
    self.departmentDividerLayer.frame = CGRectMake(10, CGRectGetMaxY(self.departmentTitleLabel.frame) + 10, self.contentMaxWidth - 20, 1);
    
    [self.view addSubview:self.addCareerTitleLabel];
    [self.view addSubview:self.cancelButton];
    [self.view addSubview:self.doneButton];
    [self.view addSubview:self.positionTitleLabel];
    [self.view addSubview:self.positionTextField];
    [self.view addSubview:self.startDateTitleLabel];
    [self.view addSubview:self.startDateButton];
    [self.view addSubview:self.endDateTitleLabel];
    [self.view addSubview:self.endDateButton];
    [self.view addSubview:self.branchTitleLabel];
    [self.view addSubview:self.branchTextField];
    [self.view addSubview:self.departmentTitleLabel];
    [self.view addSubview:self.deparmentTextField];
    [self.view.layer addSublayer:self.positionTitleDivider];
    [self.view.layer addSublayer:self.startDateDivider];
    [self.view.layer addSublayer:self.endDateDividerLayer];
    [self.view.layer addSublayer:self.branchDividerLayer];
    [self.view.layer addSublayer:self.departmentDividerLayer];
}

#pragma mark - Button Handlers

- (void)onCancelButton:(id)sender
{
    [self.view endEditing:true];
    
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void)onDoneButton:(id)sender
{
    [self.view endEditing:true];
    
    [self dismissViewControllerAnimated:true completion:nil];
    
    if( [self hasValidInputs] )
    {
        [self saveCareerProgress];
    }
}

- (void)onStartDateButton:(id)sender
{
    [self.view endEditing:TRUE];
    
    [self showDatePopoverCtrl:UIDatePickerModeDate
                  minimumDate:nil
                  maximumDate:nil
                         rect:self.startDateButton.frame
                       inView:self.view
                   pickerDate:^(NSDate *pickerDate) {
                       [self.startDateButton setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:kDisplayDateFormatte]] forState:UIControlStateNormal];
                       [self checkValidInputs];
                   }];
}

- (void)onEndDateButton:(id)sender
{
    [self.view endEditing:TRUE];
    
    [self showDatePopoverCtrl:UIDatePickerModeDate
                  minimumDate:nil
                  maximumDate:nil
                         rect:self.endDateButton.frame
                       inView:self.view
                   pickerDate:^(NSDate *pickerDate) {
                       [self.endDateButton setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:kDisplayDateFormatte]] forState:UIControlStateNormal];
                       [self checkValidInputs];
                   }];
}

- (void)onTextFieldDidChange:(id)sender
{
    [self checkValidInputs];
}

#pragma mark - DB Integration

- (void)checkValidInputs
{
    self.doneButton.enabled = [self hasValidInputs];
}

- (BOOL)hasValidInputs
{
    if( isEmptyString(self.positionTextField.text) ||
        isEmptyString(self.branchTextField.text) ||
        isEmptyString(self.startDateButton.currentTitle) ||
        isEmptyString(self.endDateButton.currentTitle) )
        return false;
    return true;
}

- (void)saveCareerProgress
{
    if (!self.careerProgress)
        self.careerProgress = [self createCareerProgress];
    
    self.careerProgress.position = self.positionTextField.text;
    self.careerProgress.startDate =  [DateUtils stringToDate:self.startDateButton.currentTitle dateFormat:kDisplayDateFormatte];
    self.careerProgress.endDate = [DateUtils stringToDate:self.endDateButton.currentTitle dateFormat:kDisplayDateFormatte];
    self.careerProgress.branch = self.branchTextField.text;
    self.careerProgress.department = self.deparmentTextField.text;

    [self.agentDetails addAgentCareerProgressObject:self.careerProgress];
    
    [[CachingService sharedInstance] saveContext];
    
    if( self.delegate && [self.delegate respondsToSelector:@selector(addAgentCareerViewController:didAddCareerProgress:)] )
    {
        [self.delegate addAgentCareerViewController:self didAddCareerProgress:self.careerProgress];
    }
}

- (TblCareerProgress*)createCareerProgress
{
    __block TblCareerProgress *careerProgress;
    [[CachingService sharedInstance].managedObjectContext performBlockAndWait:^{
        careerProgress = [TblCareerProgress insertInManagedObjectContext:[CachingService sharedInstance].managedObjectContext];
    }];
    return careerProgress;
}


@end
