//
//  AgentAwardCell.m
//  AIAChina
//
//  Created by MacMini on 14/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AgentAwardCell.h"

@implementation AgentAwardCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)loadAgetAward:(TblAward *)award{
    [Utility setRoundCornerRedius:self.imgAward
                     cornerRedius:self.imgAward.frame.size.width/2];
    
    if(award.awardImage)
        self.imgAward.image = [UIImage imageWithData:award.awardImage];
    else
        self.imgAward.image = DEFAULT_AWARD_IMAGE;
        
    self.lblAwardTitle.text = award.awardTitle;
    self.lblDate.text = [DateUtils dateToString:award.awardDate andFormate:@"dd-MMM-yyyy"];
    self.lblDescription.text = award.awardDesc;
}

@end
