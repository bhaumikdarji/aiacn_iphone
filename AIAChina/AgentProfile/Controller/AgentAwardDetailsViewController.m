//
//  AgentAwardDetailsViewController.m
//  AIAChina
//
//  Created by Burri on 2015-07-18.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AgentAwardDetailsViewController.h"
#import "AgentAwardCell.h"
#import "AddAgentAwardController.h"
#import "AgentAwardCell.h"

@interface AgentAwardDetailsViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation AgentAwardDetailsViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Agent Awards", @"");
    [self loadAward];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = FALSE;
}

- (void)loadAward{   
    self.awardArray = [DbUtils fetchAllObject:@"TblAward"
                                   andPredict:[NSPredicate predicateWithFormat:@"isDelete = 0"]
                            andSortDescriptor:nil
                         managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    [self.tblAgentAward reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.awardArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AgentAwardCell *cell = (AgentAwardCell *)[tableView dequeueReusableCellWithIdentifier:@"AgentAwardCell"];
    [cell loadAgetAward:self.awardArray[indexPath.row]];
    cell.btnEdit.tag = cell.btnDelete.tag = indexPath.row;
    [cell.btnEdit addTarget:self action:@selector(onbtnEditClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDelete addTarget:self action:@selector(onBtnDeleteClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (IBAction)onbtnEditClick:(UIButton *)sender{
    [self performSegueWithIdentifier:@"AgentDetailToAddAgentAward" sender:self.awardArray[[sender tag]]];
}

- (IBAction)onBtnDeleteClick:(UIButton *)sender{
    TblAward *award = self.awardArray[[sender tag]];
    award.isDelete = [NSNumber numberWithBool:TRUE];
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    [self loadAward];
}

- (IBAction)onBtnAddClick:(id)sender {
    [self performSegueWithIdentifier:@"AgentDetailToAddAgentAward" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"AgentDetailToAddAgentAward"]){
        AddAgentAwardController *addAgentCtrl = segue.destinationViewController;
        [addAgentCtrl setReloadAwardBlock:^{
            [self loadAward];
        }];
        if([sender isKindOfClass:[TblAward class]])
            addAgentCtrl.award = (TblAward *)sender;
    }
}

@end
