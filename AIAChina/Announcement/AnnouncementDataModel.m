//
//  AnnouncementDataModel.m
//  AIAChina
//
//  Created by Burri on 2015-07-21.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AnnouncementDataModel.h"
#import "PdfPool.h"
#import "PdfDownloadObject.h"
@implementation AnnouncementDataModel
- (id)init
{
    self = [super init];
    if (!self)
    {
        return nil;
    }
    
    self.announcementsArray = [[NSMutableArray alloc] initWithCapacity:0];
    self.importantAnnouncementsArray = [[NSMutableArray alloc] initWithCapacity:0];
    self.announcementSynchronizer = [[AnnouncementServiceSynchronizer alloc]init];
    return self;
}
- (void) syncAnnouncementsIntoDatabase:(NSArray *)responseObject returnNewAnnouncements:(CMSAnnouncementsSuccessCallback)importantAnnouncements
{
    
    for (NSDictionary *entries in responseObject)
    {
        NSString *annCode = [NSString stringWithFormat:@"%@",[entries objectForKey:@"annoucement_code"]];
        Announcement *existingAnnouncementObj = [self.announcementSynchronizer checkDuplicateAnnouncementCodeCount:annCode];
        if (existingAnnouncementObj)
        {
            Announcement *announcementObj = existingAnnouncementObj;
            announcementObj.attachmentPath = [entries objectForKey:@"attachmentPath"];
            announcementObj.bu = [entries objectForKey:@"bu"];
            announcementObj.buCode = [entries objectForKey:@"buCode"];
            announcementObj.buName = [entries objectForKey:@"buName"];
            announcementObj.city = [entries objectForKey:@"city"];
            announcementObj.cityCode = [entries objectForKey:@"cityCode"];
            announcementObj.createdBy = [entries objectForKey:@"createdBy"];
            announcementObj.creationDate = [entries objectForKey:@"creationDate"];
            announcementObj.dist = [entries objectForKey:@"dist"];
            announcementObj.district = [entries objectForKey:@"district"];
            announcementObj.expDate = [entries objectForKey:@"expDate"];
            announcementObj.message = [entries objectForKey:@"message"];
            
            announcementObj.modificationDate = [entries objectForKey:@"modificationDate"];
            announcementObj.modifiedBy = [entries objectForKey:@"modifiedBy"];
            announcementObj.msgType = [entries objectForKey:@"msgType"];
            announcementObj.publishedDate = [entries objectForKey:@"publishedDate"];
            announcementObj.ssc = [entries objectForKey:@"ssc"];
            announcementObj.sscCode = [entries objectForKey:@"sscCode"];
            announcementObj.status = [entries objectForKey:@"status"];
            announcementObj.subject = [entries objectForKey:@"subject"];
            announcementObj.token = [entries objectForKey:@"token"];
            announcementObj.userName = [entries objectForKey:@"userName"];
            announcementObj.agentId = [CachingService sharedInstance].currentUserId;
            announcementObj.isRead = announcementObj.isRead;
            PdfPool *pdfPool = [self.announcementSynchronizer createPdfPoolObject];
            pdfPool.pdfpath = [entries objectForKey:@"attachmentPath"];
            announcementObj.pdfDetails = pdfPool;
            [self.announcementsArray addObject:announcementObj];
            if ([announcementObj.msgType isEqualToString:@"Important"]) {
                [self.importantAnnouncementsArray addObject:announcementObj];
            }
            [self.announcementSynchronizer save];
            
        }
        else
        {
            Announcement *announcementObj;
            announcementObj = [self.announcementSynchronizer createAnnouncement];
            announcementObj.annoucement_code = [NSString stringWithFormat:@"%@",[entries objectForKey:@"annoucement_code"]];
            
            announcementObj.attachmentPath = [entries objectForKey:@"attachmentPath"];
            announcementObj.bu = [entries objectForKey:@"bu"];
            announcementObj.buCode = [entries objectForKey:@"buCode"];
            announcementObj.buName = [entries objectForKey:@"buName"];
            announcementObj.city = [entries objectForKey:@"city"];
            announcementObj.cityCode = [entries objectForKey:@"cityCode"];
            announcementObj.createdBy = [entries objectForKey:@"createdBy"];
            announcementObj.creationDate = [entries objectForKey:@"creationDate"];
            announcementObj.dist = [entries objectForKey:@"dist"];
            announcementObj.district = [entries objectForKey:@"district"];
            announcementObj.expDate = [entries objectForKey:@"expDate"];
            announcementObj.message = [entries objectForKey:@"message"];
            
            announcementObj.modificationDate = [entries objectForKey:@"modificationDate"];
            announcementObj.modifiedBy = [entries objectForKey:@"modifiedBy"];
            announcementObj.msgType = [entries objectForKey:@"msgType"];
            announcementObj.publishedDate = [entries objectForKey:@"publishedDate"];
            announcementObj.ssc = [entries objectForKey:@"ssc"];
            announcementObj.sscCode = [entries objectForKey:@"sscCode"];
            announcementObj.status = [entries objectForKey:@"status"];
            announcementObj.subject = [entries objectForKey:@"subject"];
            announcementObj.token = [entries objectForKey:@"token"];
            announcementObj.userName = [entries objectForKey:@"userName"];
            announcementObj.agentId = [CachingService sharedInstance].currentUserId;
            announcementObj.isRead = @0;
            PdfPool *pdfPool = [self.announcementSynchronizer createPdfPoolObject];
            pdfPool.pdfpath = [entries objectForKey:@"attachmentPath"];
            announcementObj.pdfDetails = pdfPool;
            [self.announcementsArray addObject:announcementObj];
            if ([announcementObj.msgType isEqualToString:@"Important"]) {
                [self.importantAnnouncementsArray addObject:announcementObj];
            }
            [self.announcementSynchronizer save];
        }
        
        
    }
    importantAnnouncements(self.importantAnnouncementsArray, [responseObject count]);
}
- (NSArray *)fetchAnnouncementDataListArray
{
    NSString *agentId = [CachingService sharedInstance].currentUserId;

    NSArray *announcement = [[DbUtils fetchAllObject:@"Announcement"
                                          andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND publishedDate <= %@ AND expDate > %@",agentId,[DateUtils dateToString:[NSDate date]andFormate:kServerDateFormatte],[DateUtils dateToString:[NSDate date]andFormate:kServerDateFormatte]]
                                   andSortDescriptor:nil
                                managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    return announcement;
}
- (void)deleteAnnouncements:(NSArray *)announcementArray
{
    for(int count = 0; count < [announcementArray count] ; count++)
    {
//        NSLog(@"coutn %d",count);
        [self.announcementSynchronizer deleteAnnoucement:[[announcementArray objectAtIndex:count] stringValue]];
    }
}

- (void)downloadPDFAndSave:(Announcement *)announcement status:(CMSAnnouncementPDFDownloadSuccessCallback)success
{
    PdfDownloadObject *pdfDownloadObj = [[PdfDownloadObject alloc]init];
    //NSArray *pathStringsArray = [announcement.attachmentPath componentsSeparatedByString:@"/"];
    NSString *fileName = [announcement.annoucement_code stringByAppendingString:@".pdf"];
    [pdfDownloadObj downloadFileWithFilePath:announcement.annoucement_code fileName:fileName finished:^(NSString *filePath, NSError *error) {
        Announcement *announcementObj = announcement;
        PdfPool *pdfPoolObj = announcementObj.pdfDetails;
        NSData *pdfData = [NSData dataWithContentsOfFile:filePath];
        pdfPoolObj.pdfdata = pdfData;
        announcementObj.pdfDetails = pdfPoolObj;
        [self.announcementSynchronizer save];
        success(announcement);
    }];
    
    
}
-(void)changeStatus:(Announcement *)announcementObj
{
    Announcement *announcement = announcementObj;
    announcement.isRead = [NSNumber numberWithBool:YES];
    [self.announcementSynchronizer save];
}
- (BOOL)showDownloadButtonOrNot:(Announcement *)announcement
{
    if(announcement.attachmentPath != nil && ![announcement.attachmentPath isEqualToString:@""])
    {
        PdfPool *pdfPoolObj = announcement.pdfDetails;
        if (pdfPoolObj.pdfdata) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return NO;
}
@end
