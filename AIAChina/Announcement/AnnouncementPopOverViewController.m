//
//  AnnouncementPopOverViewController.m
//  AIAChina
//
//  Created by Burri on 2015-07-21.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AnnouncementPopOverViewController.h"
#import "Announcement.h"
@interface AnnouncementPopOverViewController ()

@end

@implementation AnnouncementPopOverViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view from its nib.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.importantAnnouncementArray count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    Announcement *announcement = [self.importantAnnouncementArray objectAtIndex:indexPath.row];
    cell.textLabel.text = announcement.subject;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Announcement *announcement = [self.importantAnnouncementArray objectAtIndex:indexPath.row];
    NSDictionary* userInfo = @{@"code": announcement};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"importantAnnoucement"
                                                        object:self userInfo:userInfo];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
