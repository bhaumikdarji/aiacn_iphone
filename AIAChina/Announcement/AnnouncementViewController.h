//
//  AnnouncementViewController.h
//  AIAChina
//
//  Created by Burri on 2015-07-21.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Announcement.h"
@interface AnnouncementViewController : AIAChinaViewController
@property (nonatomic, retain) Announcement *ann;
@end
