//
//  AnnouncementDataModel.h
//  AIAChina
//
//  Created by Burri on 2015-07-21.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnnouncementServiceSynchronizer.h"
#import "Announcement.h"
typedef void (^CMSAnnouncementsSuccessCallback)(NSArray *success, NSInteger count);
typedef void (^CMSAnnouncementPDFDownloadSuccessCallback)(Announcement *announcement);
@interface AnnouncementDataModel : NSObject
@property (nonatomic, retain) NSMutableArray *announcementsArray;
@property (nonatomic, retain) NSMutableArray *importantAnnouncementsArray;
@property (nonatomic, strong) AnnouncementServiceSynchronizer *announcementSynchronizer;
- (void) syncAnnouncementsIntoDatabase:(NSArray *)responseObject returnNewAnnouncements:(CMSAnnouncementsSuccessCallback)importantAnnouncements;
- (NSArray *)fetchAnnouncementDataListArray;
- (BOOL)showDownloadButtonOrNot:(Announcement *)announcement;
- (void)downloadPDFAndSave:(Announcement *)announcement status:(CMSAnnouncementPDFDownloadSuccessCallback)success;
-(void)changeStatus:(Announcement *)announcementObj;
- (void)deleteAnnouncements:(NSArray *)announcementArray;
@end
