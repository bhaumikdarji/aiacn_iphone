//
//  AnnouncementViewController.m
//  AIAChina
//
//  Created by Burri on 2015-07-21.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AnnouncementViewController.h"
#import "AnnouncementDataModel.h"
#import "Announcement.h"
#import "AnnouncementServiceSynchronizer.h"
#import "PdfPool.h"
#import "ReaderViewController.h"
#import "ReaderDocument.h"
#import "MBProgressHUDUpd.h"
//#import "CalendarViewController.h"

@interface AnnouncementViewController ()<UITextFieldDelegate,ReaderViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *pageTitleText;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITableView *announcementsTableView;
@property (strong, nonatomic) IBOutlet UILabel *announcementTitle;
@property (strong, nonatomic) IBOutlet UILabel *announcementSubTitle;
@property (strong, nonatomic) IBOutlet UILabel *announcementDate;
@property (strong, nonatomic) IBOutlet UITextView *announcementBody;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) AnnouncementDataModel *dataModel;
@property (strong, nonatomic) NSArray *announcementArray;
@property (weak, nonatomic) IBOutlet UIButton *pdfDownloadButton;
@property (strong, nonatomic) NSArray *filteredArray;
@property (nonatomic, strong) AnnouncementServiceSynchronizer *announcementSynchronizer;
@property (strong, nonatomic) Announcement *selectedAnnouncement;
@property (nonatomic, retain) CachingService *cachingService;
@end
@implementation AnnouncementViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Announcements", @"");
    self.searchTextField.placeholder = NSLocalizedString(@"Search", @"");
    self.dataModel = [[AnnouncementDataModel alloc]init];
    self.announcementSynchronizer = [[AnnouncementServiceSynchronizer alloc]init];
    self.cachingService = [CachingService sharedInstance];
    self.filteredArray = [[NSArray alloc]init];
    self.searchTextField.delegate = self;
    CGFloat cornerRadius = 3.0f;
    CALayer *layer = [self.pdfDownloadButton layer];
    [[self.pdfDownloadButton layer] setBorderWidth:1.0f];
    [[self.pdfDownloadButton layer] setBorderColor:[UIColor blackColor].CGColor];
    [layer setMasksToBounds:YES];
    [layer setCornerRadius:cornerRadius];
    [self.searchTextField addTarget:self.searchTextField
                             action:@selector(resignFirstResponder)
                   forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    // indent the edit area in the search text field
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 00)];
    self.searchTextField.leftView = paddingView;
    self.searchTextField.leftViewMode = UITextFieldViewModeAlways;
    
    [self.searchTextField setReturnKeyType:UIReturnKeyDone];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = YES;
    [self loadData];
    
    
}
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"HomeToCalendar"]) {
        CalendarViewController *destViewController = segue.destinationViewController;
        NSLog(@"%@",destViewController);
    }
}
*/
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = [self prepareLeftBarButton:YES withLogo:NO];
    self.navigationItem.rightBarButtonItem = [self prepareRightBarButton:YES];
    [MyAppDelegate showAnnouncements:self.view];
}

- (void)loadData
{
    self.announcementArray = [self.dataModel fetchAnnouncementDataListArray];
    self.filteredArray = self.announcementArray;
    
    if (self.filteredArray.count > 0)
    {
        if (self.ann) {
            [self loadAnnouncementData:self.ann];
            [self.dataModel changeStatus:self.ann];
        }
        else{
            [self loadAnnouncementData:[self.announcementArray objectAtIndex:0]];
            [self.dataModel changeStatus:[self.announcementArray objectAtIndex:0]];
        }
        
    }
    else{
        self.announcementTitle.text = @"";
        self.announcementSubTitle.text = @"";
        self.announcementDate.text = @"";
        self.announcementBody.text = @"";
        self.pdfDownloadButton.hidden = YES;
    }
    
   
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.filteredArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[ UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
    }
    Announcement *announcement = [self.filteredArray objectAtIndex:indexPath.row];
    if ([announcement.isRead intValue] == 1) {
        cell.textLabel.textColor = [UIColor lightGrayColor];
    }
    if ([announcement.msgType isEqualToString:@"Important"]) {
        cell.textLabel.attributedText = [self attributedStringforMarketPlace:announcement.subject];
    }
    else
        cell.textLabel.text = announcement.subject;
    return cell;
}
-(NSMutableAttributedString*)attributedStringforMarketPlace:(NSString*)marketPlaceStr{
    
    NSString *newString = marketPlaceStr;
    marketPlaceStr = [NSString stringWithFormat:@"%@*",marketPlaceStr];
    NSRange range = [marketPlaceStr rangeOfString:@"*"];
    NSRange newrange = [marketPlaceStr rangeOfString:newString];
    NSRange selectedRange = NSMakeRange(range.location, 1);
    
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:marketPlaceStr];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:newrange];
    
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:255.0f/255.0f green:160.0f/255.0f blue:43.0f/255.0f alpha:1.0] range:selectedRange];
    [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:36.0f] range:selectedRange];
    
    //[attString addAttribute:(NSString *)kCTSuperscriptAttributeName value:@1 range:selectedRange];
    
    return attString;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor lightGrayColor];
    Announcement *announcement = [self.filteredArray objectAtIndex:indexPath.row];
    [self.dataModel changeStatus:announcement];
    [self loadAnnouncementData:announcement];
    
}
- (void)loadAnnouncementData:(Announcement *)announcement
{
    self.ann = announcement;
    self.selectedAnnouncement = nil;
    self.selectedAnnouncement = announcement;
    if ([announcement.msgType isEqualToString:@"Important"])
    {
        self.announcementTitle.attributedText = [self attributedStringforMarketPlace:announcement.subject];
    }
    else
        self.announcementTitle.text = announcement.subject;
    
    self.announcementSubTitle.text = announcement.userName;
    NSDate *date = [NSDate stringToDate:announcement.publishedDate dateFormat:kServerDateFormatte];
    NSString *publishedDateString = [NSDate dateToString:date andFormate:kDisplayDateFormatte];
    self.announcementDate.text = publishedDateString;
    self.announcementBody.text = announcement.message;
    if(announcement.attachmentPath != nil && ![announcement.attachmentPath isEqualToString:@""])
    {
        self.pdfDownloadButton.hidden = NO;
        self.titleLabel.hidden = NO;
        self.titleLabel.text = announcement.subject;
        PdfPool *pdfDetails = announcement.pdfDetails;
        if (pdfDetails.pdfdata != nil) {
            //[self.pdfDownloadButton setTitle:NSLocalizedString(@"Show Pdf", @"") forState:UIControlStateNormal];
            [self.pdfDownloadButton addTarget:self action:@selector(showPdf:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
           // [self.pdfDownloadButton setTitle:NSLocalizedString(@"Download Pdf", @"") forState:UIControlStateNormal];
            [self.pdfDownloadButton addTarget:self action:@selector(downloadPdf:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else
    {
        self.pdfDownloadButton.hidden = YES;
        self.titleLabel.hidden = YES;
    }
    
    //ann.readDate = [NSDate date];
    
}
- (void)showPdf:(id)sender
{
    Announcement *announcement = self.selectedAnnouncement;
    PdfPool *pool = announcement.pdfDetails;
    NSData *data               = pool.pdfdata;
    NSArray *pathStringsArray = [announcement.attachmentPath componentsSeparatedByString:@"/"];
    NSString *fileName = [pathStringsArray lastObject];
    NSString *pdfPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:fileName];
    [data writeToFile:pdfPath atomically:YES];
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:pdfPath password:nil];
    
    if (document != nil) {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self presentViewController:readerViewController animated:YES completion:^{}];
    }
}
- (void)downloadPdf:(id)sender
{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    [self.dataModel downloadPDFAndSave:self.selectedAnnouncement status:^(Announcement *announcement)
         {
             [MBProgressHUDUpd hideHUDForView:self.view animated:YES];
             
             //[self.pdfDownloadButton setTitle:NSLocalizedString(@"Show Pdf", @"") forState:UIControlStateNormal];
             [self.pdfDownloadButton addTarget:self action:@selector(showPdf:) forControlEvents:UIControlEventTouchUpInside];
             
             [self showPdf:self.pdfDownloadButton];
     
         }];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self.searchTextField isEditing]) {
        [textField resignFirstResponder];
        [self searchAnnoucements:textField.text];
        return NO;
    }
    return true;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self endSearch];
    return YES;
}

- (void)endSearch
{
    self.filteredArray = self.announcementArray;
    [self.announcementsTableView reloadData];
}
- (void)searchAnnoucements:(NSString *)text
{
    NSString *filter = [NSString stringWithFormat:@"*%@*", text];
    NSPredicate *filterDuplicateCodes = [NSPredicate predicateWithFormat:@"self isKindOfClass: %@ AND self.subject LIKE[cd] %@",[Announcement class], filter];
    
    NSArray *searchResultsArray = [self.announcementArray filteredArrayUsingPredicate: filterDuplicateCodes];
   
    if (searchResultsArray.count > 0)
    {
        self.filteredArray = searchResultsArray;
        [self.announcementsTableView reloadData];
        [self loadAnnouncementData:[self.filteredArray objectAtIndex:0]];
        
        
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Search", @"") message:NSLocalizedString(@"No Search result", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil] show];
        
    }
    
    
}
- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)downloadPdfBtnAcntion:(id)sender
{
    [self.dataModel downloadPDFAndSave:self.selectedAnnouncement status:^(Announcement *announcement)
    {
        NSLog(@"success");
        
        
    }];
}

@end
