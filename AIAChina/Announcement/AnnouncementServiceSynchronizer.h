//
//  AnnouncementServiceSynchronizer.h
//  AIAChina
//
//  Created by Burri on 2015-07-21.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Announcement,PdfPool;
@interface AnnouncementServiceSynchronizer : NSObject
- (Announcement *)createAnnouncement;
- (void)save;
- (PdfPool *)createPdfPoolObject;
- (NSArray *)fetchSavedListArray;
- (NSInteger) duplicateAnnouncementCodeCount: (Announcement *) announcementObj;
- (void)deleteAnnoucement:(NSString *)announcementCode;
- (Announcement *) checkDuplicateAnnouncementCodeCount:(NSString *)announcementCode;
@end
