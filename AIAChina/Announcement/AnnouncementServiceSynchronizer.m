//
//  AnnouncementServiceSynchronizer.m
//  AIAChina
//
//  Created by Burri on 2015-07-21.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AnnouncementServiceSynchronizer.h"
#import "CachingService.h"
#import "Announcement.h"
#import "PdfPool.h"
@interface AnnouncementServiceSynchronizer ()

@property (nonatomic, strong) CachingService *cachingService;
@end
@implementation AnnouncementServiceSynchronizer

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cachingService = [[CachingService alloc]init];
    }
    return self;
}

- (Announcement *)createAnnouncement
{
    __block Announcement *announcement;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        announcement = [Announcement insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return announcement;
}
- (PdfPool *)createPdfPoolObject
{
    __block PdfPool *pdfPoolObj;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        pdfPoolObj = [PdfPool insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return pdfPoolObj;
}
- (void)save
{
    [self.cachingService saveContext];
}
- (void)deleteAnnoucement:(NSString *)announcementCode
{
    NSArray *loadArray = [self fetchSavedListArray];
    NSString * announcementCodeEscaped = [NSRegularExpression escapedPatternForString: announcementCode];
    
    NSPredicate *filterDuplicateCodes = [NSPredicate predicateWithFormat:@"self isKindOfClass: %@ AND self.annoucement_code MATCHES[c] %@",[Announcement class], announcementCodeEscaped];
    
    NSArray *duplicates = [loadArray filteredArrayUsingPredicate: filterDuplicateCodes];
    if (duplicates.count > 0)
    {
        Announcement * existingClientCard = ( Announcement * ) [duplicates lastObject];
        [self.cachingService.managedObjectContext deleteObject:existingClientCard];
        [self.cachingService.managedObjectContext save:nil];
    }
    
}
- (NSArray *)fetchSavedListArray
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:Announcement.entityName];
    //[fetchRequest setFetchLimit:1];
    
    __block NSArray *savedListArray;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        NSError *error;
        savedListArray = [self.cachingService.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    }];
    
    return savedListArray;
}


- (NSInteger) duplicateAnnouncementCodeCount: (Announcement *) announcementObj
{
    NSArray *loadArray = [self fetchSavedListArray];
  
    
    NSInteger result = -1;
    
    NSString * announcementCode = announcementObj.annoucement_code;
    NSString * announcementCodeEscaped = [NSRegularExpression escapedPatternForString: announcementCode];
    
    NSPredicate *filterDuplicateCodes = [NSPredicate predicateWithFormat:@"self isKindOfClass: %@ AND self.annoucement_code MATCHES[c] %@",[Announcement class], announcementCodeEscaped];
    
    NSArray *duplicates = [loadArray filteredArrayUsingPredicate: filterDuplicateCodes];
    
    
    if (duplicates.count > 0)
    {
        result = [duplicates count];
        if (duplicates.count == 1)
        {
            Announcement * existingClientCard = ( Announcement * ) [duplicates lastObject];
            if (existingClientCard.objectID == announcementObj.objectID)     // allow overwriting the same card
            {
                result = 0;
            }
        }
        
        
    }
    
    return result;
}
- (Announcement *) checkDuplicateAnnouncementCodeCount:(NSString *)announcementCode
{
    NSArray *loadArray = [self fetchSavedListArray];
    
    
    NSInteger result = -1;
    
    NSString * announcementCodeEscaped = [NSRegularExpression escapedPatternForString: announcementCode];
    
    NSPredicate *filterDuplicateCodes = [NSPredicate predicateWithFormat:@"self isKindOfClass: %@ AND self.annoucement_code MATCHES[c] %@",[Announcement class], announcementCodeEscaped];
    
    NSArray *duplicates = [loadArray filteredArrayUsingPredicate: filterDuplicateCodes];
    
    
    if (duplicates.count > 0)
    {
        result = [duplicates count];
        if (duplicates.count == 1)
        {
            Announcement * existingClientCard = ( Announcement * ) [duplicates lastObject];
            return existingClientCard;
        }
        
    }
    return nil;
}


@end
