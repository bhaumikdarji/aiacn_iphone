//
//  AnnouncementPopOverViewController.h
//  AIAChina
//
//  Created by Burri on 2015-07-21.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnouncementPopOverViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *announcementTableView;
@property (nonatomic, retain) NSArray *importantAnnouncementArray;

@end
