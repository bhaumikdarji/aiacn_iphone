//
//  AppDelegate.m
//  AIAChina
//
//  Created by AIA on 16/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AppDelegate.h"
#import "WXApi.h"
#import "JNKeychain.h"
#import <ShareSDK/ShareSDK.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "Constants.h"
#import <CommonCrypto/CommonDigest.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

///usr/local/bin/mogenerator -m AIAChina/Model/AIAChina.xcdatamodeld -M AIAChina/Model/Machine -H AIAChina/Model/Human --template-var arc=true

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].statusBarHidden = NO;
    [WXApi registerApp:WECHAT_APP_ID];
    
    [ShareSDK registerApp:@"9b3c933e2258"];
    
    //qq的初始化
    [ShareSDK connectQQWithQZoneAppKey:QQ_APP_ID qqApiInterfaceCls:[QQApiInterface class] tencentOAuthCls:[TencentOAuth class]];
    
    //微信登陆的时候需要初始化
    [ShareSDK connectWeChatWithAppId:WECHAT_APP_ID        //此参数为申请的微信AppID
                           wechatCls:[WXApi class]];
    
    if ([Utility isFirstLaunch])
    {
        //[JNKeychain clearKeyChain];
         [JNKeychain deleteValueForKey:@"kMasterKeyInjectorKey"];
    }
    
    self.mapManager = [[BMKMapManager alloc]init];
    BOOL ret = [self.mapManager start:@"2baVzZPwOPUS9i14wMk9WaGU" generalDelegate:self];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"lastActiveTimeStamp"];

    self.serviceManager = [[CMSServiceManager alloc]init];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    if ([ApplicationFunction isLogin]) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastActiveTimeStamp"];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSDate *lastActiveTimeStamp  = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastActiveTimeStamp"];
    //
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSinceDate:lastActiveTimeStamp];
    if (timeStamp > 30*60) {
        // Do Logout notification and handle
        [ApplicationFunction doLogOut];
        [DisplayAlert showAlertInControllerInView:[MyAppDelegate window].rootViewController
                                            title:NSLocalizedString(@"Error", @"")
                                          message:NSLocalizedString(@"You have been logged out due to inactivity. Please login again to continue using iOS App", @"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
         [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"lastActiveTimeStamp"];
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

//- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
//    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
//}

- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    [ShareSDK handleOpenURL:url wxDelegate:self];
    return  [TencentOAuth HandleOpenURL:url];
    
    
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    [ShareSDK handleOpenURL:url
          sourceApplication:sourceApplication
                 annotation:annotation
                 wxDelegate:self];
    
    NSLog(@"url: %@, annotation: %@", url, annotation);
    return [TencentOAuth HandleOpenURL:url];
}
- (NSString *)md5StringForString:(NSString *)string {
    const char *str = [string UTF8String];
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (uint32_t)strlen(str), r);
    return [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]];
}
-(NSString *)prepareAgentCodeWithZero:(NSString *)str{
    int maxDigits = 9;
    int strDigits = [str length];
    int totalTimeLoop = maxDigits - strDigits;
    if (totalTimeLoop < 0) {
        totalTimeLoop = 0;
    }
    NSString *finalStr = [NSMutableString new];
    for (int i = 0; i < totalTimeLoop; i++) {
        finalStr = [finalStr stringByAppendingString:@"0"];
    }
    finalStr = [finalStr stringByAppendingString:str];
    return finalStr;
}

-(NSString *)removeZeroFromAgentCode:(NSString*)str {
    int x=[str intValue];
    if (x) {
        return [NSString stringWithFormat:@"%d",x];
    } else {
        return str;
    }
}
- (void)getAnnouncementsUpdate:(UIView *)view
{
    NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
    [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
    [parametersDict setValue:[CachingService sharedInstance].branchCode forKey:@"co"];
    [MBProgressHUDUpd showHUDAddedTo:view animated:YES];
    [[self.serviceManager getDataFromServiceCall:kCMSAnnouncementURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:view animated:YES];
        [self insertAnnouncementsIntoDatabase:responseObject withView:view];
        
    } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:view animated:YES];
        [self showAnnouncements:view];
    }]resume];
}

-(void)showAnnouncements:(UIView *)view
{
    NSString *agentId = [CachingService sharedInstance].currentUserId;
    
    NSArray *announcement = [[DbUtils fetchAllObject:@"Announcement"
                                          andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isRead == 0 AND msgType == %@ AND publishedDate <= %@ AND expDate > %@",agentId, @"Important",[DateUtils dateToString:[NSDate date]andFormate:kServerDateFormatte],[DateUtils dateToString:[NSDate date]andFormate:kServerDateFormatte]]
                                   andSortDescriptor:nil
                                managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    //    self.announcementButton.hidden = NO;
    //    [self.announcementButton setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[announcement count]] forState:UIControlStateNormal];
    if (announcement.count > 0) {
        //        [self showPopUP:announcement];
    } else {
        announcement = [[DbUtils fetchAllObject:@"Announcement"
                                     andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isRead == 0 AND publishedDate <= %@ AND expDate > %@",agentId,[DateUtils dateToString:[NSDate date]andFormate:kDisplayDateFormatte],[DateUtils dateToString:[NSDate date]andFormate:kDisplayDateFormatte]]
                              andSortDescriptor:nil
                           managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
        //        [self.announcementButton setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[announcement count]] forState:UIControlStateNormal];
    }
    if (self.announcementCount == nil) {
        self.announcementCount = @"0";
    }
    [self setAnnouncementCount:[NSString stringWithFormat:@"%lu",(unsigned long)[announcement count]]];
    
    NSDictionary* userInfo = @{@"total": self.announcementCount};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateAnnouncement" object:self userInfo:userInfo];
}

- (void)insertAnnouncementsIntoDatabase:(NSArray *)responseObject withView:(UIView *)view
{
    self.announcementDataModel = [[AnnouncementDataModel alloc]init];
    [self.announcementDataModel syncAnnouncementsIntoDatabase:responseObject returnNewAnnouncements:^(NSArray *importantArray, NSInteger count)
     {
         
         
         NSMutableDictionary *parametersDict = [[NSMutableDictionary alloc]init];
         [parametersDict setValue:[CachingService sharedInstance].currentUserId forKey:@"agentId"];
         [MBProgressHUDUpd showHUDAddedTo:view animated:YES];
         [[self.serviceManager getDataFromServiceCall:kCMSDeleteAnnouncementURL parametersDictionary:parametersDict success:^(NSURLSessionDataTask *task, id responseObject) {
             [MBProgressHUDUpd hideAllHUDsForView:view animated:YES];
             NSLog(@"%@",responseObject);
             NSArray *deletedArray = responseObject;
             if (deletedArray.count > 0) {
                 [self.announcementDataModel deleteAnnouncements:deletedArray];
             }
             [self showAnnouncements:view];
             
             
         } failure:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
             [MBProgressHUDUpd hideAllHUDsForView:view animated:YES];
             
             [self showAnnouncements:view];
             
         }]resume];
     }];
}

@end
