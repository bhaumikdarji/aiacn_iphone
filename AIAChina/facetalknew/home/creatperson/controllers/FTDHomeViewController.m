//
//  FTDHomeViewController.m
//  FaceTalk
//
//  Created by Mao-MacPro on 15/7/9.
//  Copyright (c) 2015年 wen. All rights reserved.
//

#import "FTDHomeViewController.h"
#import "FTDHomeLeadViewController.h"
#import "UIView+MaterialDesign.h"
#import "TFDNavViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "FTDDataPacketManager.h"
#import "FTJsonManager.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "FTDDataProvider.h"
#import "FTJsonManager.h"
#import "FTWDataManager.h"
#define remove_sp(a) [[NSUserDefaults standardUserDefaults] removeObjectForKey:a]
#define get_sp(a) [[NSUserDefaults standardUserDefaults] objectForKey:a]
#define get_Dsp(a) [[NSUserDefaults standardUserDefaults]dictionaryForKey:a]
#define set_sp(a,b) [[NSUserDefaults standardUserDefaults] setObject:b forKey:a]
#import "TblAgentDetails.h"
@interface FTDHomeViewController ()
{
    NSTimer *loadTimer;
    int timeIndex;
}
@end

@implementation FTDHomeViewController
@synthesize btnKey,scrollBG;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.contact) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:self.contact.name forKey:@"name"];
        [dic setObject:self.contact.addressCode forKey:@"personid"];
        NSDictionary *dic1 = [[NSDictionary alloc]initWithDictionary:dic];
        set_sp(@"DTALENTINFO", dic1);
        [FTWDataManager shareManager].selectedContact = self.contact;
        //set_sp(@"DTALENTINFO", self.talentInfoDic);
    }
    
    [self getlocalResourse];
    //[self reminderBtn];
    [self getJsonUrl];
    NSLog(@"%@",self.navigationController);
    TFDNavViewController *nav=(TFDNavViewController *) self.navigationController;
    nav.btnSlider.hidden=YES;
    nav.btnRightMenu.hidden=YES;
    
    scrollBG.contentSize=CGSizeMake(1024, 748);
    scrollBG.pagingEnabled=YES;
    scrollBG.scrollEnabled=NO;
    //[btnKey addTarget:self action:@selector(homeLeadViewclick:event:) forControlEvents:UIControlEventTouchUpInside];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlocalResourse) name:@"FTDUNZIPSUCCESS" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backIMOclick:) name:@"FTDREQUESTFAILED" object:nil];
    [self getLoginUserInfo];
    
 
}
-(void)getLoginUserInfo//由QUIX协助调用获取登录信息接口  说明下“TblProfile”模型的结构
{
    TblAgentDetails *agentDetails = [TblAgentDetails getCurrentAgentDetails];
    set_sp(@"DUSERINFO", agentDetails.userName);
}
-(void)getJsonUrl
{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    FTDDataProvider *dataProvider = [[FTDDataProvider alloc] init];
    __weak FTDHomeViewController *weakself = self;
    
    [dataProvider setFinishBlock:^(NSDictionary *resultDict){
        
        NSLog(@"json文件url：%@",resultDict);
        if ([[resultDict  objectForKey:@"success"] intValue] == 1) {
            
            [weakself updateJsonFile:[resultDict objectForKey:@"msg"]];
            
        }
        else{
            [SVProgressHUD dismiss];
        }
        
        
        
    }];
    
    [dataProvider setFailedBlock:^(NSString *strError){
        [SVProgressHUD dismiss];
    }];
    
    [dataProvider getJsonUrl];
}

-(void)updateJsonFile:(NSString *)url
{
    
    FTDDataProvider *dataProvider = [[FTDDataProvider alloc] init];
    __weak FTDHomeViewController *weakself = self;
    
    [dataProvider setFinishBlock:^(NSDictionary *resultDict){
        
        NSLog(@"登录结果：%@",resultDict);
        if ([[resultDict  objectForKey:@"success"] intValue] == 1) {
            [SVProgressHUD dismiss];
            [[FTJsonManager shareManager]initData];
            [weakself checkPicUpdate];
        }
        else{
            [SVProgressHUD dismiss];
        }
        
        
        
    }];
    
    [dataProvider setFailedBlock:^(NSString *strError){
        [SVProgressHUD dismiss];
    }];
    
    [dataProvider upDateJsonFile:url];
}
-(void)checkPicUpdate
{
    if ([[FTJsonManager shareManager].lastUpdatePicDate isEqualToString:get_sp(@"DLASTUPDATEPIC")]) {
        NSLog(@"没更新");
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"检测到图片有更新，是否更新资源包？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert show];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[FTDDataPacketManager sharedInstance] downloadFile];
    }
}


-(void)getlocalResourse
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dstResourcePath = [[FTDDataPacketManager sharedInstance]unzipDestinationPath];
    dstResourcePath =[dstResourcePath stringByAppendingPathComponent:@"uploads"];
    dstResourcePath =[dstResourcePath stringByAppendingPathComponent:@"data.json"];
    if ([fileManager fileExistsAtPath:dstResourcePath]) {
        [self getbackgroundImg];
        if ([FTDDataPacketManager sharedInstance].firstID == 0) {
            if (get_sp(@"DLASTUPDATEPIC") == nil) {
                //set_sp(@"DLASTUPDATEPIC", @"2014-12-29");
                
                set_sp(@"DLASTUPDATEPIC", [FTJsonManager shareManager].lastUpdatePicDate);
                [FTDDataPacketManager sharedInstance].firstID = 1;
            }
        }
        else{
            set_sp(@"DLASTUPDATEPIC", [FTJsonManager shareManager].lastUpdatePicDate);
        }

        loadTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(reminderBtn) userInfo:nil repeats:YES];
        [loadTimer fire];
    }
    else{
         
        [[FTDDataPacketManager sharedInstance]downloadFile];
    }
}


-(void)getbackgroundImg
{
    NSString *strImg=[FTJsonManager shareManager].index_background;
    UIImageView *scrollImage=[[UIImageView alloc]initWithFrame:CGRectMake(0,20, 1024,748)];
     scrollImage.image=[UIImage imageWithContentsOfFile:strImg] ;
     //scrollImage.image=[UIImage imageNamed:@"FTD_home_aiaoffice.png"];
    [scrollBG addSubview:scrollImage];
}


-(void)viewDidAppear:(BOOL)animated
{
     //[SVProgressHUD showProgress:0.5 status:@"已下载50%" maskType:SVProgressHUDMaskTypeBlack];
}
-(void)viewDidDisappear:(BOOL)animated
{
    btnKey.alpha=1;
    btnKey.frame=CGRectMake(700, 350, 260, 180);
    
}
-(void)viewWillAppear:(BOOL)animated
{
    self.btnyinKey.enabled = YES;
    TFDNavViewController *nav = (TFDNavViewController *)self.navigationController;
    nav.btnSlider.hidden = YES;
    nav.btnRightMenu.hidden = YES;
    nav.viewRightMenu.hidden = YES;
}
-(void)reminderBtn
{
//    CGAffineTransform trans = btnKey.transform;
//    btnKey.transform = CGAffineTransformScale(trans, 0.7, 0.7);
//    if (timeIndex==3) {
//        [loadTimer invalidate];
//        
//    }
    [UIView animateWithDuration:1.0 animations:^{
        btnKey.alpha =0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            btnKey.alpha =1;
        } completion:^(BOOL finished) {
            //timeIndex++;
        }];
    }];
    
}

-(void)homeLeadViewclick
{
    [loadTimer invalidate];
    btnKey.alpha = 1;

    __weak FTDHomeViewController *weak=self;
    FTDHomeLeadViewController *FTDHomeLeadViewCol=[[FTDHomeLeadViewController alloc]init];
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [UIView animateKeyframesWithDuration:2 delay:0 options:UIViewKeyframeAnimationOptionLayoutSubviews animations:^{
            btnKey.center=CGPointMake(512, 440);
            btnKey.alpha = 0;
        } completion:^(BOOL finished) {
            
            CGPoint exactTouchPosition = CGPointMake(512, 440);
            
            
            [UIView mdInflateTransitionFromView:weak.view toView:FTDHomeLeadViewCol.view originalPoint:exactTouchPosition duration:1.5 completion:^{
                NSLog(@"completed!");
                [weak.navigationController pushViewController:FTDHomeLeadViewCol animated:YES];
                
            }];
        }];
        
        // code to be executed on the main queue after delay
    });
    
    
    
    
//    CATransition *  tran=[CATransition animation];
//    tran.type =kCATransitionFade;
//     tran.subtype = kCATransitionFromRight;
//    tran.duration=1;
//    tran.delegate=self;
//    [self.view.superview.layer addAnimation:tran forKey:@"kongyu"];
    
}
- (IBAction)backIMOclick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)keyclick:(id)sender {
    self.btnyinKey.enabled = NO;
    [self homeLeadViewclick];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
