//
//  FTDDataProvider.m
//  FaceTalk
//
//  Created by Mao-MacPro on 15/12/21.
//  Copyright © 2015年 wen. All rights reserved.
//

#import "FTDDataProvider.h"
#import "FTDDataPacketManager.h"
#import <SVProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <AFNetworking/AFURLSessionManager.h>
#define FTD_BASE_URL @"https://aes.aia.com.cn/cms/attract/"
#define FTD_UPdateFile_URL @"https://aes.aia.com.cn/cms/attract/data.json"
@implementation FTDDataProvider
-(void)getJsonUrl
{
    NSString *urlString = [NSString stringWithFormat:@"%@downloadDataJson",FTD_BASE_URL];
    [self requestData:urlString];
}

-(void)upDateJsonFile:(NSString *)strurl
{
    
    
    //检查附件是否存在
    //    if ([fileManager fileExistsAtPath:fileName]) {
    //        NSData *audioData = [NSData dataWithContentsOfFile:fileName];
    //        //[self requestFinished:[NSDictionary dictionaryWithObject:audioData forKey:@"res"] tag:aTag];
    //    }else{
    //创建附件存储目录
    
    NSString *fileName=[[FTDDataPacketManager sharedInstance]unzipDestinationPath];
    
    fileName =[fileName stringByAppendingPathComponent:@"uploads"];
    fileName =[fileName stringByAppendingPathComponent:@"data.json"];
    //下载附件
    NSURL *url = [[NSURL alloc] initWithString:strurl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        
    }];
    operation.inputStream   = [NSInputStream inputStreamWithURL:url];
    operation.outputStream  = [NSOutputStream outputStreamToFileAtPath:fileName append:NO];
    
    //下载进度控制
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        NSLog(@"is download：%f", (float)totalBytesRead/totalBytesExpectedToRead);
    }];
    
    
    //已完成下载
    __weak FTDDataProvider  *weakSelf = self;
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"成功了");
        self.finishBlock(@{@"success":@"1"});
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"失败了");
        
        
        //下载失败
        //[self requestFailed:aTag];
    }];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
}

- (void)requestData:(NSString *)urlString
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    __weak FTDDataProvider *weakself = self;
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSString *html = operation.responseString;
        NSData* data=[html dataUsingEncoding:NSUTF8StringEncoding];
        weakself.resultDict=[NSJSONSerialization  JSONObjectWithData:data options:0 error:nil];
        weakself.finishBlock(self.resultDict);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        weakself.failedBlock([NSString stringWithFormat:@"%@",error]);
    }];
}

- (void)dealloc
{
    self.resultDict = nil;
    self.finishBlock = nil;
    self.failedBlock = nil;
}


@end
