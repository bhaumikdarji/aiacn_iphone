//
//  FTDDataProvider.h
//  FaceTalk
//
//  Created by Mao-MacPro on 15/12/21.
//  Copyright © 2015年 wen. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^dataProviderFinishBlock)(NSDictionary *);
typedef void(^dataProviderFailedBlock)(NSString *);
@interface FTDDataProvider : NSObject
@property(nonatomic, strong)dataProviderFinishBlock finishBlock;
@property(nonatomic, strong)dataProviderFailedBlock failedBlock;

@property(nonatomic, strong)NSDictionary *resultDict;
-(void)getJsonUrl;
-(void)upDateJsonFile:(NSString *)strurl;
@end
