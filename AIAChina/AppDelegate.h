//
//  AppDelegate.h
//  AIAChina
//
//  Created by AIA on 16/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <BaiduMapAPI/BMapKit.h>
#import "CMSServiceManager.h"
#import "AnnouncementDataModel.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, BMKGeneralDelegate>
#define WECHAT_APP_ID @"wx1b43befa88bbb92b"
#define QQ_APP_ID @"1104791471"
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BMKMapManager *mapManager;
@property (strong,nonatomic) NSString *sessionid;
- (NSString *)md5StringForString:(NSString *)string;
@property (nonatomic) NSInteger currentSelectedMonth;
-(NSString *)prepareAgentCodeWithZero:(NSString *)str;
-(NSString *)removeZeroFromAgentCode:(NSString*)str;
@property (nonatomic, strong) NSString *announcementCount;
@property (nonatomic,retain) CMSServiceManager *serviceManager;
@property (nonatomic,retain) AnnouncementDataModel *announcementDataModel;
-(void)getAnnouncementsUpdate:(UIView *)view;
-(void)showAnnouncements:(UIView *)view;
@end

