//
//  ApplicationFunction.m
//  AIAChina
//
//  Created by AIA on 31/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "ApplicationFunction.h"
#import "ContactDataProvider.h"
#import "AddAgentAwardController.h"
#import "SignatureViewController.h"
#import "ZXingObjC.h"
#import "GreetingsViewController.h"
#import "HomeViewController.h"
//#import "MapViewController.h"

@implementation ApplicationFunction

+ (UIStoryboard *)getCurrentStoryBoard{
    return [UIStoryboard storyboardWithName: IPAD_STORY_BOARD bundle:[NSBundle mainBundle]];
}

+ (BOOL)isLogin
{
    return [CachingService sharedInstance].currentUserId != nil;
}
+ (void)doLogOut
{
    [CachingService sharedInstance].currentUserId = nil;
    [ApplicationFunction showLoginView];
}

+ (void)showToAgentProfile:(BOOL)isPresenter
{
    [[[[MyAppDelegate window] subviews] objectAtIndex:0] removeFromSuperview];
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *homeNavCtrl = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewNavController"];
    HomeViewController *homeViewController = (HomeViewController *)[homeNavCtrl topViewController];
    homeViewController.isInteractivePresenter = isPresenter;
    [MyAppDelegate window].rootViewController = homeNavCtrl;
}

+ (void)showLoginView{
    [[[[MyAppDelegate window] subviews] objectAtIndex:0] removeFromSuperview];
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *homeNavCtrl = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewNavController"];
    [MyAppDelegate window].rootViewController = homeNavCtrl;
}

+ (void)showESignaturePopoverCtrl:(UIPopoverController *)popoverCtrl
                        withTitle:(NSString *)title
                            descr:(NSString *)descrp
                             rect:(CGRect)rect
                           inView:(UIView *)view
                    callBackBlock:(void (^)())callBackBlock{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    SignatureViewController *aleExamViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"SignatureViewController"];
    popoverCtrl = [[UIPopoverController alloc] initWithContentViewController:aleExamViewCtrl];
    popoverCtrl.backgroundColor = aleExamViewCtrl.view.backgroundColor;
    [popoverCtrl presentPopoverFromRect:rect
                                 inView:view
               permittedArrowDirections:0
                               animated:TRUE];
}

+ (void)setUpdateUserProcess:(NSString *)processName
                     contact:(TblContact *)contact
               processStatus:(NSString *)status
                 processDate:(NSDate *)date{
    
    TblCandidateProcess *candidateProcess = contact.candidateProcess;
    
    if(!candidateProcess){
        candidateProcess = [[ContactDataProvider sharedContactDataProvider] createCandidateProcess];
        contact.candidateProcess = candidateProcess;
    }
    
    if([processName isEqualToString:RECURITMENT_INTERACTIVE_PRESENER]){
        candidateProcess.interactivePresentDate = date;
        candidateProcess.interactivePresentStatus = status;
    }else if([processName isEqualToString:RECURITMENT_EOP_REGISTRATION]){
        candidateProcess.eopRegistrationDate = date;
        candidateProcess.eopRegistrationStatus = status;
    }else if([processName isEqualToString:RECURITMENT_FIRST_INTERVIEW]){
        candidateProcess.firstInterviewDate = date;
        candidateProcess.firstInterviewStatus = status;
    }else if([processName isEqualToString:RECURITMENT_ATTEND_EOP]){
        candidateProcess.attendEopDate = date;
        candidateProcess.attendEopStatus = status;
    }else if([processName isEqualToString:RECURITMENT_CC_TEST]){
        candidateProcess.ccTestDate = date;
        candidateProcess.ccTestStatus = status;
    }else if([processName isEqualToString:RECURITMENT_COMPANY_INTERVIEW]){
        candidateProcess.companyInterviewDate = date;
        candidateProcess.companyInterviewStatus = status;
    }else if([processName isEqualToString:RECURITMENT_ALE_EXAM]){
        candidateProcess.aleExamDate = date;
        candidateProcess.aleExamStatus = status;
    }else if([processName isEqualToString:RECURITMENT_ABC_TRAINING]){
        candidateProcess.abcTrainingDate = date;
        candidateProcess.abcTrainingStatus = status;
    }else if([processName isEqualToString:RECURITMENT_CONTRACT]){
        candidateProcess.contractDate = date;
        candidateProcess.contractStatus = status;
    }
    candidateProcess.completeProgressDate = [NSDate date];
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
}

+ (void)showDatePopoverCtrl:(UIPopoverController *)popoverCtrl
                       rect:(CGRect)rect
                     inView:(UIView *)view
                 pickerDate:(void (^)(NSDate *))pickerDate{
    
//    [self showDatePopoverCtrl:popoverCtrl withDatePickerMode:UIDatePickerModeDateAndTime rect:rect inView:view pickerDate:pickerDate];
}

+ (void)deleteCalManualEvent:(FFEvent *)event{
    NSError *error = nil;
    TblManualEntryCalendarEvents *tblCalendarEvent = (TblManualEntryCalendarEvents *)[[CachingService sharedInstance].managedObjectContext existingObjectWithID:event.manualEntryCalEventId
                                                                                                                                  error:&error];
    [[ContactDataProvider sharedContactDataProvider] deleteManualEntryCalendarEvent:tblCalendarEvent];
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
}

+ (UIImage *)generateBarcodeImageWithQRCode:(NSString *)QRValue{
    UIImage *barcodeImage;
    
    NSError *error = nil;
    ZXMultiFormatWriter *writer = [ZXMultiFormatWriter writer];
    ZXEncodeHints *hints = [ZXEncodeHints hints];
    hints.encoding = NSUTF8StringEncoding;
    ZXBitMatrix* result;
    
    @try {
                
        result = [writer encode:QRValue
                         format:kBarcodeFormatQRCode
                          width:300
                         height:300
                          hints:hints
                          error:&error];
        
        if (error) {
            NSLog(@"ZXMultiFormatWriter error = %@", [error localizedDescription]);
        } else {
            barcodeImage = [UIImage imageWithCGImage:[[ZXImage imageWithMatrix:result onColor:[[UIColor blackColor] CGColor] offColor:[[UIColor whiteColor] CGColor]] cgimage]];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"ZXMultiFormatWriter error: %@ - %@", exception.name, exception.reason);
        return nil;
    }
    return barcodeImage;
}


+ (void)openSafariBrowser:(NSString *)url{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *myURL;
    myURL = [NSURL URLWithString:url];
    [application openURL:myURL];
}

+ (NSString *)recruitmentProgress:(TblContact *)contact{
    if(isEmptyString(contact.candidateProcess.recruitmentType))
        return [NSString stringWithFormat:@"%lld/9",contact.candidateProcess.candidateProgressValue];
    else{
        if([contact.candidateProcess.recruitmentType isEqualToString:@"TypeOne"])
            return @"T";
        else
            return contact.candidateProcess.recruitmentType;
    }
}

+ (void)joinUngroup:(TblContact *)contact{
    TblGroup *group = (TblGroup *)[DbUtils fetchObject:@"TblGroup"
                                            andPredict:[NSPredicate predicateWithFormat:@"groupName = %@ AND agentId = %@",UNGROUP, contact.agentId]
                                     andSortDescriptor:nil
                                  managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    if(!group){
        group = [[ContactDataProvider sharedContactDataProvider] createGroup];
        group.isDelete = [NSNumber numberWithBool:NO];
        group.isSync = [NSNumber numberWithBool:YES];
        group.groupDescription = NSLocalizedString(@"Ungroup group", @"Ungroup group");
        group.groupName = UNGROUP;
        group.agentId = contact.agentId;
    }
    group.iosAddressCode = contact.iosAddressCode;
    [group addContactObject:contact];
}

+ (void)removeUngroup:(NSArray *)contactArray{
    TblGroup *ungroup = (TblGroup *)[DbUtils fetchObject:@"TblGroup"
                                              andPredict:[NSPredicate predicateWithFormat:@"groupName = %@ AND agentId = %@",UNGROUP, [CachingService sharedInstance].currentUserId]
                                       andSortDescriptor:nil
                                    managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    for (TblContact *contact in contactArray)
        [ungroup removeContactObject:contact];
}

+ (NSArray *)fetchYearlyBirthdayEvents:(NSInteger)year{
    NSFetchRequest* fetch = [NSFetchRequest fetchRequestWithEntityName:@"TblContact"];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"TblContact"
                                              inManagedObjectContext:[CachingService sharedInstance].managedObjectContext];
    NSAttributeDescription* statusDesc = [entity.attributesByName objectForKey:@"birthMonthDay"];
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath: @"birthMonthDay"]; // Does not really matter
    NSExpression *countExpression = [NSExpression expressionForFunction: @"count:"
                                                              arguments: [NSArray arrayWithObject:keyPathExpression]];
    NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
    [expressionDescription setName: @"count"];
    [expressionDescription setExpression: countExpression];
    [expressionDescription setExpressionResultType: NSInteger32AttributeType];
    
    [fetch setPropertiesToFetch:[NSArray arrayWithObjects:statusDesc, expressionDescription, nil]];
    [fetch setPropertiesToGroupBy:[NSArray arrayWithObject:statusDesc]];
    [fetch setResultType:NSDictionaryResultType];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"birthYear<=%d AND birthMonthDay.length>0 AND agentId == %@ AND isDelete == 0",year, [CachingService sharedInstance].currentUserId];
    [fetch setPredicate:predicate];
    
    NSError* error = nil;
    NSArray *results = [[CachingService sharedInstance].managedObjectContext executeFetchRequest:fetch
                                                                                           error:&error];
    
    return results;
}

+ (void)showGrettingView:(NSArray *)receipt{
    UIStoryboard *storyborad = [self getCurrentStoryBoard];
    UINavigationController *navCtrl = [storyborad instantiateViewControllerWithIdentifier:@"GreetingsNavController"];
    GreetingsViewController *greetingViewCtrl = navCtrl.viewControllers[0];
    greetingViewCtrl.isBirthday = TRUE;
    greetingViewCtrl.receipt = receipt;
    [(MyAppDelegate).window.rootViewController presentViewController:navCtrl animated:TRUE completion:nil];
}

+ (BOOL)isCompletePersonalInfo:(TblContact *)contact{
    if(isEmptyString(contact.name))
        return FALSE;
    else if(isEmptyString(contact.nric))
        return FALSE;
    else if(contact.birthDate == nil)
        return FALSE;
    else if(isEmptyString(contact.gender))
        return FALSE;
    else if(isEmptyString(contact.education))
        return FALSE;
    else if(isEmptyString(contact.eMailId))
        return FALSE;
    else if(isEmptyString(contact.marritalStatus))
        return FALSE;
    else if(isEmptyString(contact.yearlyIncome))
        return FALSE;
    else if(isEmptyString(contact.workingYearExperience))
        return FALSE;
    else if(isEmptyString(contact.registeredAddress))
        return FALSE;
    else if(isEmptyString(contact.registeredAddress1))
        return FALSE;
    else if(isEmptyString(contact.residentialAddress))
        return FALSE;
    else if(isEmptyString(contact.residentialAddress1))
        return FALSE;
    else if(isEmptyString(contact.mobilePhoneNo))
        return FALSE;
    else if(isEmptyString(contact.identityType))
        return FALSE;
    else if(isEmptyString(contact.purchasedAnyInsurance))
        return FALSE;
    else if(isEmptyString(contact.reJoin))
        return FALSE;
    else if(isEmptyString(contact.salesExperience))
        return FALSE;
    else if(isEmptyString(contact.newcomerSource))
        return FALSE;
    else
        return TRUE;
}

+ (BOOL)isCompleteEducation:(TblContact *)contact{
    for (TblEducation *education in contact.candidateEducations) {
        BOOL isCompleteEducation = TRUE;
        if(isEmptyString(education.startDate)){
            isCompleteEducation = FALSE;
        }else if(isEmptyString(education.endDate)){
            isCompleteEducation = FALSE;
        }else if(isEmptyString(education.witness)){
            isCompleteEducation = FALSE;
        }else if(isEmptyString(education.education)){
            isCompleteEducation = FALSE;
        }else if(isEmptyString(education.educationLevel)){
            isCompleteEducation = FALSE;
        }else if(isEmptyString(education.school)){
            isCompleteEducation = FALSE;
        }else if(isEmptyString(education.witnessContactNo)){
            isCompleteEducation = FALSE;
        }
        if(isCompleteEducation)
            return TRUE;
    }
    return FALSE;
}

+ (BOOL)isCompleteSignature:(TblContact *)contact{
    for (TblESignature *signature in contact.candidateESignatures) {
        BOOL isCompleteSignature = TRUE;
        if(isEmptyString(signature.applicationDate))
            isCompleteSignature = FALSE;
        else if(signature.eSignaturePhoto == nil)
            isCompleteSignature = FALSE;
        if(isCompleteSignature)
            return TRUE;
    }
    return FALSE;
}
//----------------------------------------------------------------------------------------
+ (NSArray *)fetchICalendarEvents:(NSInteger)year{
    NSFetchRequest* fetch = [NSFetchRequest fetchRequestWithEntityName:@"ICalendarEvents"];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"ICalendarEvents"
                                              inManagedObjectContext:[CachingService sharedInstance].managedObjectContext];
    NSAttributeDescription* statusDesc = [entity.attributesByName objectForKey:@"st_dt"];
    
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath: @"st_dt"]; // Does not really matter
    NSExpression *countExpression = [NSExpression expressionForFunction: @"count:"
                                                              arguments: [NSArray arrayWithObject:keyPathExpression]];
    NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
    [expressionDescription setName: @"count"];
    [expressionDescription setExpression: countExpression];
    [expressionDescription setExpressionResultType: NSInteger32AttributeType];
    
    [fetch setPropertiesToFetch:[NSArray arrayWithObjects:statusDesc, expressionDescription, nil]];
    [fetch setPropertiesToGroupBy:[NSArray arrayWithObject:statusDesc]];
    [fetch setResultType:NSDictionaryResultType];
    
    
    NSString *stragentcode = [NSString stringWithFormat:@"%@",[CachingService sharedInstance].currentUserIdWithZero];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"agentCode == %@",stragentcode];
    [fetch setPredicate:predicate];
    
    NSError* error = nil;
    NSArray *results = [[CachingService sharedInstance].managedObjectContext executeFetchRequest:fetch
                                                                                           error:&error];
    
    NSLog(@"results %@",results);
    return results;
}

@end
