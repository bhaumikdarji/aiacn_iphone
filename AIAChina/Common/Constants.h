//
//  Constants.h
//  AIAChina
//
//  Created by AIA on 17/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#ifndef AIAChina_Constants_h
#define AIAChina_Constants_h

#define APP_NAME @"AIA China"

#define kTimeDateFormatte @"hh:mm:ss a"
#define kServerDateFormatte @"yyyy-MM-dd HH:mm:ss"
#define kDateFormatte @"dd-MM-yyyy hh:mm:ss"
#define kDisplayDateFormatte @"yyyy-MM-dd"
#define kDisplayDetailFormat @"dd MMM yyyy"
#define kLoginDateFormat @"yyyyMMdd"
#define kNewDisplayDateFormatte @"yyyy-MM-dd HH:mm:ss"
#define kBuildDateFormat @"MMM dd, yyyy"
#define kYearMonthFormat2 @"yyyy/MM"
#define kYearMonthFormat @"yyyy MMM"
#define kCalDateFormate @"MMM dd, yyyy hh:mm:ss aa" //Aug 30, 2015 12:00:00 AM

#define isEmptyString(value)(value == nil || [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

#define MyAppDelegate (AppDelegate*)[[UIApplication sharedApplication] delegate]
#define ERROR_DIALOG_TITLE @"Error"

#define SPLIT_MASTER_DETAIL_TO_ADD_CONTACT_NOTIFICATION @"SPLIT_MASTER_DETAIL_TO_ADD_CONTACT_NOTIFICATION"
#define SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION @"SPLIT_MASTER_DETAIL_TO_ADD_GROUP_NOTIFICATION"
#define SPLIT_MASTER_DETAIL_TO_SEARCH_NOTIFICATION @"SPLIT_MASTER_DETAIL_TO_SEARCH_NOTIFICATION"
#define SPLIT_MASTER_DETAIL_TO_PROFILE_NOTIFICATION @"SPLIT_MASTER_DETAIL_TO_PROFILE_NOTIFICATION"
#define SPLIT_MASTER_DETAIL_TO_GROUP_NOTIFICATION @"SPLIT_MASTER_DETAIL_TO_GROUP_NOTIFICATION"

#define CONTACT_LIST_RELOAD_NOTIFICATION @"CONTACT_LIST_RELOAD_NOTIFICATION"
#define GROUP_LIST_RELOAD_NOTIFICATION @"GROUP_LIST_RELOAD_NOTIFICATION"
#define EDIT_CONTACT_GROUP_UNHIDE_TRANS_VIEW_NOTIFICATION @"EDIT_CONTACT_GROUP_UNHIDE_TRANS_VIEW_NOTIFICATION"
#define GROUP_TO_PROFILE_NOTIFICATION @"GROUP_TO_PROFILE_NOTIFICATION"
#define TIMEOUT_NOTIFICATION @"TIMEOUT_NOTIFICATION"
#define DEFAULT_PROFILE_IMAGE [UIImage imageNamed:nil]
#define DEFAULT_GROUP_IMAGE [UIImage imageNamed:@"group_default.jpg"]
#define DEFAULT_AWARD_IMAGE [UIImage imageNamed:@"award_default.png"]
#define REG_BG_COLOR [UIColor colorWithRed:247.0/255.0 green:129.0/255.0 blue:129.0/255.0 alpha:1.0]

#endif
