//
//  FPopUpView.m
//  Fantasia
//
//  Created by Smruti Consulting & Services on 03/12/14.
//  Copyright (c) 2014 Smruti Consulting & Services. All rights reserved.
//

#import "FPopUpView.h"

static FPopUpView *sharedInstance;

@implementation FPopUpView

+(FPopUpView *)sharedDialog{
    @synchronized(self){
        if (sharedInstance == nil){
            sharedInstance = [[FPopUpView alloc] init];
        }
    }
    return sharedInstance;
}

-(void) showPopUp:(UIView *)popUpView AndAnimationType:(int)animation{
    [popUpView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3]];
    [self animatePopUp:animation view:popUpView];
}

-(void) animatePopUp:(int)animationType view:(UIView *)popUpView{
    if(animationType == VIEWPOPUP_BOTTOM){
        popUpView.frame = CGRectMake(0,([UIApplication sharedApplication].keyWindow.bounds.size.height + 1), popUpView.frame.size.width,popUpView.frame.size.height);
        [[UIApplication sharedApplication].keyWindow addSubview:popUpView];
        
        [UIView beginAnimations:@"animateOff" context:NULL];
        [UIView setAnimationDuration:ANIMATION_TIME];
        [popUpView setFrame:CGRectMake(0,0,popUpView.frame.size.width,popUpView.frame.size.height)];
        [UIView commitAnimations];
        
    }else if(animationType == VIEWPOPUP_RIGHT){
        popUpView.frame = CGRectMake(([UIApplication sharedApplication].keyWindow.bounds.size.width + 1),0, popUpView.frame.size.width,popUpView.frame.size.height);
        [[UIApplication sharedApplication].keyWindow addSubview:popUpView];
        
        [UIView beginAnimations:@"animateOff" context:NULL];
        [UIView setAnimationDuration:ANIMATION_TIME];
        [popUpView setFrame:CGRectMake(0,0,popUpView.frame.size.width,popUpView.frame.size.height)];
        [UIView commitAnimations];
    }else if(animationType == VIEWPOPUP_LEFT){
        
    }else if(animationType == VIEWPOPUP_FADE){
        popUpView.alpha = 0.0;
        [[UIApplication sharedApplication].keyWindow addSubview:popUpView];
        [UIView beginAnimations:@"animateOff" context:NULL];
        [UIView setAnimationDuration:ANIMATION_TIME];
        popUpView.alpha = 1.0;
        [UIView commitAnimations];
    }
    
   // [[GameSoundManager sharedSoundManager] playSingleTimeSound:@"/popup.mp3"];
}

-(void) dismissPopUp:(int)animationType
           popUpView:(UIView *)popUpView
          afterDelay:(float) delay
     completionBlock:(void(^)(void))completionBlock {
    
    if(animationType == VIEWPOPUP_BOTTOM){
        [UIView animateWithDuration:ANIMATION_TIME
                              delay:delay
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [popUpView setFrame:CGRectMake(0,([UIApplication sharedApplication].keyWindow.bounds.size.height+1),popUpView.frame.size.width,popUpView.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             [popUpView removeFromSuperview];
                             if (completionBlock)
                                 completionBlock();
                             
                         }];
    }else if(animationType == VIEWPOPUP_RIGHT){
        [UIView animateWithDuration:ANIMATION_TIME
                              delay:delay
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [popUpView setFrame:CGRectMake(([UIApplication sharedApplication].keyWindow.bounds.size.width+1),0,popUpView.frame.size.width,popUpView.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             [popUpView removeFromSuperview];
                             if (completionBlock)
                                 completionBlock();
                             
                         }];
    }else if(animationType == VIEWPOPUP_FADE){
        [UIView animateWithDuration:ANIMATION_TIME
                              delay:delay
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             popUpView.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             [popUpView removeFromSuperview];
                             if (completionBlock)
                                 completionBlock();
                         }];
    }
}


@end
