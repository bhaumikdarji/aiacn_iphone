

#import <UIKit/UIKit.h>

@interface KeyboardToolbar : UIView

@property (weak, nonatomic) IBOutlet UIButton *btnPrev;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

+(id)keyBoardView;
- (void)setInputAccessoryViewForTextField:(NSArray *)textfieldArray;

@end
