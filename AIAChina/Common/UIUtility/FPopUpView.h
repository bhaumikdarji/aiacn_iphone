//
//  FPopUpView.h
//  Fantasia
//
//  Created by Smruti Consulting & Services on 03/12/14.
//  Copyright (c) 2014 Smruti Consulting & Services. All rights reserved.
//

#import <UIKit/UIKit.h>

enum fPopUpAnimation
{
    VIEWPOPUP_LEFT = 1,
    VIEWPOPUP_RIGHT = 2,
    VIEWPOPUP_TOP = 3,
    VIEWPOPUP_BOTTOM = 4,
    VIEWPOPUP_FADE = 5
};

#define OVERLAY_TAG 1010101
#define ANIMATION_TIME 0.4

@interface FPopUpView : UIView{
    
}

+(FPopUpView *)sharedDialog;

-(void) showPopUp:(UIView *)popUpView AndAnimationType:(int)animation;

-(void) dismissPopUp:(int)animationType
           popUpView:(UIView *)popUpView
          afterDelay:(float) delay
     completionBlock:(void(^)(void))completionBlock;

@end
