

#import "DisplayAlert.h"
#import "UIAlertController+Private.h"

@implementation DisplayAlert

+(void)displayErrorAlert:(NSString *)errorMsg
          withTitle:(NSString *)title
        andDelegate:(id)delegate{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                       message:errorMsg
                                                      delegate:delegate
                                             cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                             otherButtonTitles:nil];
    [alertView show];
}

+(void)displayAlert:(NSString *)errorMsg
          withTitle:(NSString *)title
            withTag:(int)tag
        andDelegate:(id)delegate{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:errorMsg
                                                       delegate:delegate
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
    if (tag>0) {
        alertView.tag = tag;
    }
    [alertView show];
}

+ (void)showAlertInControllerInView:(UIViewController *)view
                              title:(NSString *)alertTitle
                            message:(NSString *)alertMessage
                  cancelButtonTitle:(NSString *)cancelTitle
                      okButtonTitle:(NSString *)okTitle
                        cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                            okBlock:(void (^)(UIAlertAction *action))okHandler{
    
    [self showAlertInControllerWithView:view
                                  title:alertTitle
                              titleColor:nil
                                 message:alertMessage
                            messageColor:nil
                       cancelButtonTitle:cancelTitle
                           okButtonTitle:okTitle
                             cancelBlock:cancelHandler
                                 okBlock:okHandler];;
}

+ (void)showAlertInControllerWithTitle:(NSString *)alertTitle
                               message:(NSString *)alertMessage
                     cancelButtonTitle:(NSString *)cancelTitle
                         okButtonTitle:(NSString *)okTitle
                           cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                               okBlock:(void (^)(UIAlertAction *action))okHandler{
    
    [self showAlertInControllerWithView:nil
                                  title:alertTitle
                              titleColor:nil
                                 message:alertMessage
                            messageColor:nil
                       cancelButtonTitle:cancelTitle
                           okButtonTitle:okTitle
                             cancelBlock:cancelHandler
                                 okBlock:okHandler];;
}

+ (void)showAlertInControllerWithTitle:(NSString *)alertTitle
                            titleColor:(UIColor *)titleTextcolor
                               message:(NSString *)alertMessage
                     cancelButtonTitle:(NSString *)cancelTitle
                         okButtonTitle:(NSString *)okTitle
                           cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                               okBlock:(void (^)(UIAlertAction *action))okHandler{
    
    [self showAlertInControllerWithView:nil
                                  title:alertTitle
                              titleColor:titleTextcolor
                                 message:alertMessage
                            messageColor:nil
                       cancelButtonTitle:cancelTitle
                           okButtonTitle:okTitle
                             cancelBlock:cancelHandler
                                 okBlock:okHandler];
    
}

+ (void)showAlertInControllerWithTitle:(NSString *)alertTitle
                               message:(NSString *)alertMessage
                          messageColor:(UIColor *)msgTextColor
                     cancelButtonTitle:(NSString *)cancelTitle
                         okButtonTitle:(NSString *)okTitle
                           cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                               okBlock:(void (^)(UIAlertAction *action))okHandler{
    
    [self showAlertInControllerWithView:nil
                                  title:alertTitle
                              titleColor:nil
                                 message:alertMessage
                            messageColor:msgTextColor
                       cancelButtonTitle:cancelTitle
                           okButtonTitle:okTitle
                             cancelBlock:cancelHandler
                                 okBlock:okHandler];
    
}

+ (void)showAlertInControllerWithView:(UIViewController *)view
                                title:(NSString *)alertTitle
                           titleColor:(UIColor *)titleTextcolor
                              message:(NSString *)alertMessage
                         messageColor:(UIColor *)msgTextColor
                    cancelButtonTitle:(NSString *)cancelTitle
                        okButtonTitle:(NSString *)okTitle
                          cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                              okBlock:(void (^)(UIAlertAction *action))okHandler{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:alertTitle
                                          message:alertMessage
                                          preferredStyle:UIAlertControllerStyleAlert];
    if(titleTextcolor){
        NSMutableAttributedString *titleAtt = [[NSMutableAttributedString alloc] initWithString:alertTitle];
        [titleAtt addAttribute:NSForegroundColorAttributeName
                      value:titleTextcolor
                      range:NSMakeRange(0, [titleAtt length])];
        [alertController setValue:titleAtt forKey:@"attributedTitle"];
    }
        
    if(msgTextColor){
        NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:alertMessage];
        [hogan addAttribute:NSForegroundColorAttributeName
                      value:msgTextColor
                      range:NSMakeRange(0, [hogan length])];
        [alertController setValue:hogan forKey:@"attributedMessage"];
    }
    
    
    if(!isEmptyString(cancelTitle)){
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelTitle
                                                               style:UIAlertActionStyleCancel
                                                             handler:cancelHandler];
        [alertController addAction:cancelAction];
    }
    
    if(!isEmptyString(okTitle)){
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:okTitle
                                                           style:UIAlertActionStyleDefault
                                                         handler:okHandler];
        [alertController addAction:okAction];
    }
    
    if(view){
        [view presentViewController:alertController animated:YES completion:nil];
    }else{
        [alertController show:TRUE];
    }

    
}

@end
