

#import "DatePickerView.h"

#define PREV_BUTTON_TAG 101010
#define NEXT_BUTTON_TAG 202020
#define DONE_BUTTON_TAG 303030

@implementation DatePickerView

+ (id)datePickerViewWithTarget:(id)target prevClicked:(SEL)prevClicked nextClicked:(SEL)nextClicked doneClicked:(SEL)doneClicked{
    
    DatePickerView *datePickerView = [[[NSBundle mainBundle] loadNibNamed:@"DatePickerView" owner:nil options:nil] lastObject];
    if ([datePickerView isKindOfClass:[DatePickerView class]]){
        [(UIButton *)[datePickerView viewWithTag:PREV_BUTTON_TAG] addTarget:target action:prevClicked forControlEvents:UIControlEventTouchUpInside];
        [(UIButton *)[datePickerView viewWithTag:NEXT_BUTTON_TAG] addTarget:target action:nextClicked forControlEvents:UIControlEventTouchUpInside];
        [(UIButton *)[datePickerView viewWithTag:DONE_BUTTON_TAG] addTarget:target action:doneClicked forControlEvents:UIControlEventTouchUpInside];
        return datePickerView;
    }else{
        return nil;
    }
}

+ (id)datePickerView{
    DatePickerView *datePickerView = [[[NSBundle mainBundle] loadNibNamed:@"DatePickerView" owner:nil options:nil] lastObject];
    
    if ([datePickerView isKindOfClass:[DatePickerView class]]) {
        return datePickerView;
    }else{
        return nil;
    }
}

- (void)setSelectedDate:(NSDate *)date{
    if(date != nil)
        self.datePicker.date = [NSDate date];
}

@end
