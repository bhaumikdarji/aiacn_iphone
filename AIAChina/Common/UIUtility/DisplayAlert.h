

#import <Foundation/Foundation.h>

@interface DisplayAlert : NSObject

@property (nonatomic, strong) UIViewController *view;

+(void)displayErrorAlert:(NSString *)errorMsg
          withTitle:(NSString *)title
        andDelegate:(id)delegate;

+(void)displayAlert:(NSString *)errorMsg
          withTitle:(NSString *)title
            withTag:(int)tag
             andDelegate:(id)delegate;

+ (void)showAlertInControllerInView:(UIViewController *)view
                              title:(NSString *)alertTitle
                            message:(NSString *)alertMessage
                  cancelButtonTitle:(NSString *)cancelTitle
                      okButtonTitle:(NSString *)okTitle
                        cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                            okBlock:(void (^)(UIAlertAction *action))okHandler;

+ (void)showAlertInControllerWithTitle:(NSString *)alertTitle
                               message:(NSString *)alertMessage
                     cancelButtonTitle:(NSString *)cancelTitle
                         okButtonTitle:(NSString *)okTitle
                           cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                               okBlock:(void (^)(UIAlertAction *action))okHandler;

+ (void)showAlertInControllerWithTitle:(NSString *)alertTitle
                            titleColor:(UIColor *)titleTextcolor
                               message:(NSString *)alertMessage
                     cancelButtonTitle:(NSString *)cancelTitle
                         okButtonTitle:(NSString *)okTitle
                           cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                               okBlock:(void (^)(UIAlertAction *action))okHandler;

+ (void)showAlertInControllerWithTitle:(NSString *)alertTitle
                               message:(NSString *)alertMessage
                          messageColor:(UIColor *)msgTextColor
                     cancelButtonTitle:(NSString *)cancelTitle
                         okButtonTitle:(NSString *)okTitle
                           cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                               okBlock:(void (^)(UIAlertAction *action))okHandler;

+ (void)showAlertInControllerWithView:(UIViewController *)view
                                title:(NSString *)alertTitle
                           titleColor:(UIColor *)titleTextcolor
                              message:(NSString *)alertMessage
                         messageColor:(UIColor *)msgTextColor
                    cancelButtonTitle:(NSString *)cancelTitle
                        okButtonTitle:(NSString *)okTitle
                          cancelBlock:(void (^)(UIAlertAction *action))cancelHandler
                              okBlock:(void (^)(UIAlertAction *action))okHandler;

@end
