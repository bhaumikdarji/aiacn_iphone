

#import "KeyboardToolbar.h"

@implementation KeyboardToolbar

+ (id)keyBoardView{
    KeyboardToolbar *views = [[[NSBundle mainBundle] loadNibNamed:@"keyboardToolbar" owner:nil options:nil] lastObject];
    
    if ([views isKindOfClass:[KeyboardToolbar class]]) {
        return views;
    }
    return nil;
}

- (void)setInputAccessoryViewForTextField:(NSArray *)textfieldArray{
    for (UITextField *textField in textfieldArray) {
        textField.inputAccessoryView = self;
    }
}

@end
