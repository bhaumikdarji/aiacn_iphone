

#import <UIKit/UIKit.h>

@interface DatePickerView : UIView{
    
}
@property (strong, nonatomic) IBOutlet UIButton *btnPrev;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIButton *btnDone;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

+ (id)datePickerView;
- (void)setSelectedDate:(NSDate *)date;

@end
