//
//  PopoverViewController.m
//  AddressBook
//
//  Created by Quix Creations on 07/05/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "PickerPopoverViewController.h"
#import "Utils.h"
#import "Utility.h"

@interface PickerPopoverViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate>
@property (strong,nonatomic) NSMutableArray *filteredPickerArray;
@property (strong,nonatomic) NSMutableArray *alphabetArray;
@property (strong,nonatomic) NSMutableArray *pickerAlphabetArray;
@property (nonatomic,assign) BOOL showSearchAndIndex;
@end

@implementation PickerPopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showSearchAndIndex = NO;
    
    if (self.showSearchAndIndexType != nil && ![self.showSearchAndIndexType isEqualToString:@""]) {
        self.showSearchAndIndex = YES;
        self.filteredPickerArray = [NSMutableArray arrayWithCapacity:[self.pickerArray count]];
        self.alphabetArray = [[NSMutableArray alloc] init];
        [self.alphabetArray addObject:@"#"];
        for (int i = 0; i<26; i++)
            [self.alphabetArray addObject:[NSString stringWithFormat:@"%c", i+65]];
        //  @[@"#", @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
        
        [self setTableIndexBar];
        [self getAlphabetArray];
        
    }
    else
    {
        [self.searchBar removeFromSuperview];
        self.pickerTableView.frame = CGRectMake(self.pickerTableView.frame.origin.x, 0.0f, self.pickerTableView.frame.size.width, self.pickerTableView.frame.size.height+40.0f);
    }
    
    // Do any additional setup after loading the view from its nib.
}
- (void)getAlphabetArray
{
    self.pickerAlphabetArray = [[NSMutableArray alloc]init];
    for (NSString *string in self.pickerArray)
    {
        if (string != nil && ![string  isEqual: @""])
        {
            NSString *strFirstChar = [[string substringToIndex:1] uppercaseString];
            if([Utility isNumeric:strFirstChar])
            {
                if(![self.pickerAlphabetArray containsObject:@"#"])
                {
                    [self.pickerAlphabetArray addObject:@"#"];
                }
            }
            else if(![self.pickerAlphabetArray containsObject:strFirstChar])
            {
                [self.pickerAlphabetArray addObject:strFirstChar];
            }
            
        }
        else if ([string  isEqual: @""])
        {
            [self.pickerAlphabetArray addObject:@""];
        }
        
    }
    [self.pickerAlphabetArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}
- (void)setTableIndexBar{
    
    self.indexBar = [[GDIIndexBar alloc] initWithTableView:self.pickerTableView];
    self.indexBar.frame = CGRectMake(self.indexBar.frame.origin.x, self.indexBar.frame.origin.x+20, self.indexBar.frame.size.width, self.indexBar.frame.size.height);
    self.indexBar.delegate = self;
    [self.pickerTableView addSubview:self.indexBar];
    [[GDIIndexBar appearance] setTextColor:[UIColor colorWithRed:212.0/255.0 green:16.0/255.0 blue:60.0/255.0 alpha:1.0]];
    [[GDIIndexBar appearance] setTextShadowColor:[UIColor clearColor]];
    [[GDIIndexBar appearance] setTextFont:[UIFont systemFontOfSize:15]];
    [[GDIIndexBar appearance] setBarBackgroundColor:[UIColor clearColor]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - GDIIndexBar delegate methods

- (NSUInteger)numberOfIndexesForIndexBar:(GDIIndexBar *)indexBar
{
    return self.alphabetArray.count;
}

- (NSString *)stringForIndex:(NSUInteger)index
{
    return [self.alphabetArray objectAtIndex:index];
}
- (void)indexBar:(GDIIndexBar *)indexBar didSelectIndex:(NSUInteger)index
{
    for (int i = 0; i < [self.pickerAlphabetArray count]; i++) {
        if([[self.alphabetArray objectAtIndex:index] isEqualToString:[self.pickerAlphabetArray objectAtIndex:i]])
            [self.pickerTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]
                                       atScrollPosition:UITableViewScrollPositionTop
                                               animated:NO];
    }
}


- (NSMutableArray *)getNumberOfRowsInSection:(NSInteger)section isOrder:(BOOL)setOrdering{
    
    
    NSMutableArray *numberOfRowArray=[[NSMutableArray alloc] init];
    if([[self.pickerAlphabetArray objectAtIndex:section] isEqualToString:@""])
    {
        for (NSString *string in self.pickerArray)
        {
            if ([string isEqualToString:@""]) {
                [numberOfRowArray addObject:string];

            }
        }
    }
    if([[self.pickerAlphabetArray objectAtIndex:section] isEqualToString:@"#"])
    {
        for (NSString *string in self.pickerArray) {
            if(!isEmptyString(string)){
                
                NSString *strFirstNameFirstChar = [[string substringToIndex:1] uppercaseString];
                if([Utility isNumeric:strFirstNameFirstChar])
                    [numberOfRowArray addObject:string];
            }
        }
    }
    else
    {
        for (NSString *string in self.pickerArray) {
            if(!isEmptyString(string)){
                NSString *strFirstNameFirstChar = [[string substringToIndex:1] uppercaseString];
                if([strFirstNameFirstChar isEqualToString:[self.pickerAlphabetArray objectAtIndex:section]]){
                    [numberOfRowArray addObject:string];
                }
            }
            
        }
    }
    
    
    return numberOfRowArray;
}
#pragma mark- tableview datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.showSearchAndIndex) {
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            return [self.filteredPickerArray count];
        } else {
            return [self getNumberOfRowsInSection:section isOrder:TRUE].count;
        }
    }
    else
    {
        return [self.pickerArray count];
    }
    
    
    
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.showSearchAndIndex)
    {
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            return 1;
        } else {
            return self.pickerAlphabetArray.count;
        }
    }
    else
    {
        return 1;
    }
    
    
    
}
- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (self.showSearchAndIndex)
    {
        if (tableView == self.searchDisplayController.searchResultsTableView)
        {
            return @"";
        }
        else
        {
            return [self.pickerAlphabetArray objectAtIndex:section];
        }
    }
    else
    {
        return @"";
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.showSearchAndIndex)
    {
        if (tableView == self.searchDisplayController.searchResultsTableView)
        {
            return nil;
        }
        else
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.pickerTableView.frame.size.width, 30)];
            headerView.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
            
            UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, headerView.frame.origin.y, headerView.frame.size.width, 30)];
            lblHeader.backgroundColor = [UIColor clearColor];
            lblHeader.text = [NSString stringWithFormat:@"   %@",[self.pickerAlphabetArray objectAtIndex:section]];
            lblHeader.font = [UIFont boldSystemFontOfSize:12.0];
            
            lblHeader.textColor = [UIColor colorWithRed:48.0/255.0
                                                  green:48.0/255.0
                                                   blue:48.0/255.0
                                                  alpha:1.0];
            [headerView addSubview:lblHeader];
            return headerView;
        }
    }
    else
    {
        return nil;
    }
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.showSearchAndIndex)
    {
        return 30.0f;
    }
    else
        return 0.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (self.showSearchAndIndex)
    {
        NSArray *tmpArray;
        if (tableView == self.searchDisplayController.searchResultsTableView)
        {
            NSLog(@"search");
        }
        else
        {
            tmpArray = [self getNumberOfRowsInSection:indexPath.section isOrder:TRUE];
            
        }
        static NSString *CellIdentifier = @"Cell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (tableView == self.searchDisplayController.searchResultsTableView)
        {
            cell.textLabel.text = [self.filteredPickerArray objectAtIndex:indexPath.row];
        } else {
            cell.textLabel.text = [tmpArray objectAtIndex:indexPath.row];
        }
        
    }
    else
    {
        static NSString *CellIdentifier = @"Cell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [self.pickerArray objectAtIndex:indexPath.row];
    }
    return cell;
}

#pragma mark- tableview datasource methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.showSearchAndIndex)
    {
        if(self.selectedIndex){
            if (tableView == self.searchDisplayController.searchResultsTableView) {
                self.selectedIndex([self getNormalIndex:self.filteredPickerArray[indexPath.row]]);
            } else {
                NSMutableArray *array = [self getNumberOfRowsInSection:indexPath.section isOrder:TRUE];
                self.selectedIndex([self getNormalIndex:array[indexPath.row]]);
            }
        }
    }else
        self.selectedIndex(indexPath.row);
}

- (NSUInteger)getNormalIndex:(NSString *)new{
    return [self.pickerArray indexOfObject:new];
}

#pragma mark- search display controller delegate methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    [self.filteredPickerArray removeAllObjects];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
    self.filteredPickerArray = [NSMutableArray arrayWithArray:[self.pickerArray filteredArrayUsingPredicate:predicate]];
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    return YES;
}
- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.view.frame = CGRectMake(self.pickerTableView.frame.origin.x, 0.0f, self.pickerTableView.frame.size.width, self.pickerTableView.frame.size.height+260);

}


@end
