//
//  ContactDataProvider.m
//  AddressBook
//
//  Created by Smitesh Patel on 2015-05-09.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "ContactDataProvider.h"
#import "CachingService.h"
#import "TblPersonalCertification.h"
#import "TblEducation.h"
#import "TblExperience.h"
#import "TblExperience.h"
#import "TblFamily.h"
#import "TblESignature.h"
#import "TblManualEntryCalendarEvents.h"

@interface ContactDataProvider ()
@property (nonatomic,strong) CachingService *cachingService;
@end

@implementation ContactDataProvider

+ (instancetype)sharedContactDataProvider {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

- (instancetype)init{
    self = [super init];
    if (self)
        self.cachingService = [CachingService sharedInstance];
    return self;
}

- (TblContact *)createContact{
    __block TblContact *contact;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        contact = [TblContact insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return contact;
}

- (TblESignature *)createESignature
{
    __block TblESignature *eSignature;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        eSignature = [TblESignature insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return eSignature;
}
- (TblFamily *)createFamily{
    __block TblFamily *family;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        family = [TblFamily insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return family;
}
- (TblEducation *)createEducation
{
    __block TblEducation *education;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        education = [TblEducation insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return education;
}
- (TblExperience *)createExperience
{
    __block TblExperience *experience;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        experience = [TblExperience insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return experience;
}
- (TblPersonalCertification *)createPersonalCertification{
    __block TblPersonalCertification *personalCertification;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        personalCertification = [TblPersonalCertification insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return personalCertification;
}
- (void)deletePersonalCertification:(TblPersonalCertification *)cerfitifcation{
    [self.cachingService.managedObjectContext deleteObject:cerfitifcation];
    [self.cachingService.managedObjectContext save:nil];
}
- (void)deleteEducation:(TblEducation *)education{
    [self.cachingService.managedObjectContext deleteObject:education];
    [self.cachingService.managedObjectContext save:nil];
}
- (void)deleteFamily:(TblFamily *)family{
    [self.cachingService.managedObjectContext deleteObject:family];
    [self.cachingService.managedObjectContext save:nil];
}
- (void)deleteExperience:(TblExperience *)exp{
    [self.cachingService.managedObjectContext deleteObject:exp];
    [self.cachingService.managedObjectContext save:nil];
}
- (void)deleteContact:(TblContact *)contact{
    [self.cachingService.managedObjectContext deleteObject:contact];
    [self.cachingService.managedObjectContext save:nil];
}

- (TblGroup *)createGroup{
    __block TblGroup *group;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        group = [TblGroup insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return group;
}

- (void)deleteGroup:(TblGroup *)group{
    [self.cachingService.managedObjectContext deleteObject:group];
    [self.cachingService.managedObjectContext save:nil];
}

- (TblCandidateProcess *)createCandidateProcess{
    __block TblCandidateProcess *candidateProcess;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        candidateProcess = [TblCandidateProcess insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return candidateProcess;
}

- (TblNotes *)createNote{
    __block TblNotes *note;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        note = [TblNotes insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return note;
}

- (TblAward *)createAward{
    __block TblAward *award;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        award = [TblAward insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return award;
}

-  (TblManualEntryCalendarEvents *)createManualCalendarEvent {
    __block TblManualEntryCalendarEvents *calendarEvent;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        calendarEvent = [TblManualEntryCalendarEvents insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return calendarEvent;
}

- (void)deleteManualEntryCalendarEvent:(TblManualEntryCalendarEvents *)manualEntryCalendarEvent{
    [self.cachingService.managedObjectContext deleteObject:manualEntryCalendarEvent];
    [self.cachingService.managedObjectContext save:nil];
}
- (void)deleteEOPEvent:(TblEOP *)eopEvent{
    [self.cachingService.managedObjectContext deleteObject:eopEvent];
    [self.cachingService.managedObjectContext save:nil];
}
- (void)deleteInterviewEvent:(TblInterview *)interviewEvent{
    [self.cachingService.managedObjectContext deleteObject:interviewEvent];
    [self.cachingService.managedObjectContext save:nil];
}

- (TblCalendarEvent *)createCalendarEvent{
    __block TblCalendarEvent *calendarEvent;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        calendarEvent = [TblCalendarEvent insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return calendarEvent;
}

- (void)deleteCalendarEvent:(TblCalendarEvent *)calendarEvent{
    [self.cachingService.managedObjectContext deleteObject:calendarEvent];
    [self.cachingService.managedObjectContext save:nil];
}

- (TblAddress *)createAddress{
    __block TblAddress *address;
    [self.cachingService.managedObjectContext performBlockAndWait:^{
        address = [TblAddress insertInManagedObjectContext:self.cachingService.managedObjectContext];
    }];
    return address;
}

- (void)deleteAddress:(TblAddress *)address{
    [self.cachingService.managedObjectContext deleteObject:address];
    [self.cachingService.managedObjectContext save:nil];
}

- (void)saveLocalDBData{
    [self.cachingService saveContext];
}

@end
