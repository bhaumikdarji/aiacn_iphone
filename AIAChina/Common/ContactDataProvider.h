//
//  ContactDataProvider.h
//  AddressBook
//
//  Created by Smitesh Patel on 2015-05-09.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactDataProvider : NSObject{
    
}

+ (instancetype)sharedContactDataProvider;

- (TblContact *)createContact;
- (TblGroup *)createGroup;
- (TblCandidateProcess *)createCandidateProcess;
- (TblNotes *)createNote;
- (TblAward *)createAward;
- (TblManualEntryCalendarEvents *)createManualCalendarEvent;
- (void)deleteManualEntryCalendarEvent:(TblManualEntryCalendarEvents *)manualEntryCalendarEvent;
- (TblCalendarEvent *)createCalendarEvent;
- (void)deleteCalendarEvent:(TblCalendarEvent *)calendarEvent;
- (TblPersonalCertification *)createPersonalCertification;
- (TblEducation *)createEducation;
- (TblExperience *)createExperience;
- (TblESignature *)createESignature;
- (TblFamily *)createFamily;
- (void)deletePersonalCertification:(TblPersonalCertification *)cerfitifcation;
- (void)saveLocalDBData;
- (void)deleteEducation:(TblEducation *)education;
- (void)deleteFamily:(TblFamily *)family;
- (void)deleteExperience:(TblExperience *)exp;
- (void)deleteEOPEvent:(TblEOP *)eopEvent;
- (void)deleteInterviewEvent:(TblInterview *)interviewEvent;
- (TblAddress *)createAddress;
- (void)deleteAddress:(TblAddress *)address;

@end
