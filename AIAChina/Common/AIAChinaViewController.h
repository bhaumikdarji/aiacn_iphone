//
//  AIAChinaViewController.h
//  AIAChina
//
//  Created by AIA on 17/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "WXApi.h"

#import "TblContact.h"
#import "TblGroup.h"
#import "TblCandidateProcess.h"
#import "TblNotes.h"
#import "TblAward.h"
#import "TblAgentDetails.h"
#import "TblGreetings.h"
#import <BaiduMapAPI/BMapKit.h>
#import "TblAddress.h"
#import "TblEOP.h"
#import "AFNetworkReachabilityManager.h"
#import "MenuViewController.h"


#define NRIC_MIN_LENGTH 15
#define NRIC_MAX_LENGTH 18
#define MOBILE_LENGTH 11
#define tableWidth [[UIScreen mainScreen] bounds].size.width/4
#define tablePosX [[UIScreen mainScreen] bounds].size.width
#define tablePosY [[UIScreen mainScreen] bounds].origin.y
#define tableHeight [[UIScreen mainScreen] bounds].size.height


@interface AIAChinaViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIPopoverControllerDelegate, MFMailComposeViewControllerDelegate,WXApiDelegate,BMKGeoCodeSearchDelegate,MenuViewControllerDelegate>

@property (nonatomic, strong) UIPopoverController *pickerPopover;
@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) UIButton *closeButton;

@property (nonatomic, strong) UIButton *btnCount;
@property (nonatomic, strong) MenuViewController *menuVC;
@property (nonatomic, assign) BOOL showPanel;
@property (strong, nonatomic) UIView *attachBlackView;


- (UIBarButtonItem *)prepareRightBarButton:(BOOL)isPrepare;
- (UIBarButtonItem *)prepareLeftBarButton:(BOOL)isPrepare withLogo:(BOOL)isLogo;

- (void)showPhotoActionSheet:(UIButton *)btn withCircularCrop:(BOOL)isCircularCrop;

- (void)showPhotoActionSheet:(UIButton *)btn withCircularCrop:(BOOL)isCircularCrop withFullScreen:(BOOL)isFullScreen;
-(void)showPhotoActionSheet:(UIButton *)btn withCircularCrop1:(BOOL)isCircularCrop1;
- (void)showPhotoActionSheet:(UIButton *)btn withCircularCrop1:(BOOL)isCircularCrop withFullScreen:(BOOL)isFullScreen;


- (void)showAssignPopoverCtrl:(NSArray *)ignorArray
            withSelectedArray:(NSMutableArray *)selectedArray
                    isContact:(BOOL)isContact
                     isDelete:(BOOL)isDelete
                     isExport:(BOOL)isExport
            isAssigneeContact:(BOOL)isAssigneeContact
                         rect:(CGRect)rect
                       inView:(UIView *)view
              selectedContact:(void (^)(NSArray *))selectedContact;

- (void)showEOPRegistrationPopoverCtrl:(TblContact *)contact
                                  rect:(CGRect)rect
                                inView:(UIView *)view
                         callBackBlock:(void (^)())callBackBlock;

- (void)showEOPRegCandAndAttandPopoverCtrl:(CGRect)rect
                                    inView:(UIView *)view
                                 eventCode:(NSInteger)eventCode
                             callBackBlock:(void (^)())callBackBlock;

- (void)showFirstInterviewPopoverCtrl:(TblContact *)contact
                                 rect:(CGRect)rect
                               inView:(UIView *)view
                        callBackBlock:(void (^)())callBackBlock;

- (void)showEOPAttendancePopoverCtrl:(NSArray *)eopRegisteredCandidate
                                rect:(CGRect)rect
                              inView:(UIView *)view
                       callBackBlock:(void (^)(BOOL))callbackBlock;

- (void)showCCTestPopoverCtrl:(TblContact *)contact
                         rect:(CGRect)rect
                       inView:(UIView *)view
                callBackBlock:(void (^)())callBackBlock;

- (void)showCompanyInterviewPopoverCtrl:(TblContact *)contact
                                   rect:(CGRect)rect
                                 inView:(UIView *)view
                          callBackBlock:(void (^)(BOOL))callbackBlock
                          candidateForm:(void (^)())candidateFromBlock;

- (void)showInterviewRegCandAndAttandPopoverCtrl:(CGRect)rect
                                          inView:(UIView *)view
                                     interviewId:(TblInterviewID *)interviewId
                                   callBackBlock:(void (^)())callBackBlock;

- (void)showALEExamPopoverCtrl:(NSString *)title
                        descr:(NSString *)descrp
                         rect:(CGRect)rect
                       inView:(UIView *)view
                callBackBlock:(void (^)())callBackBlock;

- (void)showABCTrainingPassPopoverCtrl:(CGRect)rect
                                inView:(UIView *)view
                         callBackBlock:(void (^)())callBackBlock;

- (void)showCandidateRankingPassPopoverCtrl:(int)rank
                                       rect:(CGRect)rect
                                inView:(UIView *)view
                             candidateAgentCode:(NSString *)candidateAgentCode;

- (void)showRegisterSuccessfulPopoverCtrl:(int)rank
                            candidateName:(NSString *)name
                                     desc:(NSString *)desc
                                     rect:(CGRect)rect
                                   inView:(UIView *)view;

- (void)showCompanyIntSuccessPopoverCtrl:(int)rank
                                    rect:(CGRect)rect
                                  inView:(UIView *)view;

- (void)showResultViewPopoverCtrl:(TblContact *)contact
                        interview:(TblInterview *)interview
                             rect:(CGRect)rect
                           inView:(UIView *)view;

- (void)showSocialSharingPopoverCtrl:(TblContact *)contact
                              EOPObj:(TblEOP *)eop
                          isGreeting:(BOOL)isGreeting
                               isEOP:(BOOL)isEOP
                           isQRCode:(BOOL)isQRCode
                          shareImage:(UIImage *)shareImage
                      shareImageFile:(NSString *)shareImageFile
                            shareMsg:(NSString *)shareMsg
                            shareSub:(NSString *)shareSub
                                rect:(CGRect)rect
                              inView:(UIView *)view;

- (void)showAddressPickerViewController:(CGRect)rect
                                 inView:(UIView *)view
                          callBackBlock:(void (^)(TblAddress *address))callBackBlock;

- (void)showDatePopoverCtrl:(UIDatePickerMode)mode
                minimumDate:(NSDate *)minDate
                maximumDate:(NSDate *)maxDate
                       rect:(CGRect)rect
                     inView:(UIView *)view
                 pickerDate:(void (^)(NSDate *))pickerDate;

- (void)shareWithShareMenuOnly:(id)sender;

- (void)shareQQ:(UIImage *)shareImage
        subject:(NSString *)subject
            msg:(NSString *)msg
  callBackBlock:(void (^)(NSString *errorMsg))callBackBlock;

- (void)shareWeChat:(id)sender
              image:(UIImage *)shareImage
            subject:(NSString *)subject
                msg:(NSString *)msg
      callBackBlock:(void (^)(BOOL isInstalled, BOOL isSuccess))callBackBlock;

- (void)shareEmail:(UIImage *)shareImage
         recipient:(NSArray *)recipient
          fileName:(NSString *)fileName
           subject:(NSString *)subject
               msg:(NSString *)msg
     callBackBlock:(void (^)(BOOL isInstalled))callBackBlock;

- (void)showWebView;

- (void)closeWebView;

- ( void ) getSearchLocation:(NSString *)address;

- (void)handleImagePicker:(UIImagePickerController *)picker withMediaInfo:(NSDictionary *)info;

//- (void)openInteractivePresenter;

- (void)createNoteWithTblContact:(TblContact *)contact
                        noteDate:(NSDate *)noteDate
                        noteDesc:(NSString *)noteDesc;

- (BOOL)isInternetConnect;

- (UIImage *)resizeImage:(UIImage *)image;

- (void)addRecruitmentNote:(NSInteger)eventCode
           recruitmentStep:(enum recruitmentSteps)recruitmentStep
              activityDate:(NSDate *)activityDate
                   contact:(TblContact *)contact
               description:(NSString *)description;

@end
