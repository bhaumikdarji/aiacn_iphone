//
//  UIAlertController+Private.h
//  EventApp
//
//  Created by SCS4 on 02/11/15.
//  Copyright © 2015 SCS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Window)

- (void)show;
- (void)show:(BOOL)animated;

@end

@interface UIAlertController (Private){
    
}

@property (nonatomic, strong) UIWindow *alertWindow;

@end


