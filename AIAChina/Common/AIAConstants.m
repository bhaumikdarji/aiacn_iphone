//
//  AIAConstants.m
//  AIAChina
//
//  Created by AIA on 27/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAConstants.h"

@implementation AIAConstants

NSString *const UNGROUP = @"无组群";

NSString *const RECURITMENT_INTERACTIVE_PRESENER = @"Interactive Presenter";
NSString *const RECURITMENT_COMPANY_INTERVIEW = @"Company Interview";
NSString *const RECURITMENT_EOP_REGISTRATION = @"EOP Registration";
NSString *const RECURITMENT_ALE_EXAM = @"ALE Exam";
NSString *const RECURITMENT_FIRST_INTERVIEW = @"First Interview";
NSString *const RECURITMENT_ABC_TRAINING = @"ABC Training"; 
NSString *const RECURITMENT_ATTEND_EOP = @"Attend EOP";
NSString *const RECURITMENT_CONTRACT = @"Contract";
NSString *const RECURITMENT_CC_TEST = @"CC Test";
NSString *const RECURITMENT_STATUS_TRUE = @"TRUE";
NSString *const RECURITMENT_STATUS_FALSE = @"FALSE";

NSString *const URGENT_RECRUIT = @"Urgent";
NSString *const NORMAL_RECRUIT = @"Normal";
NSString *const CAUTION_RECRUIT = @"Caution";

NSString *const DROP_DOWN_GENDER = @"Gender";
NSString *const DROP_DOWN_EDUCATION = @"Education";
NSString *const DROP_DOWN_RACE = @"Race";
NSString *const DROP_DOWN_TALENT_PROFILE = @"Talent Profile";
NSString *const DROP_DOWN_NATURE_OF_BUSINESS = @"Nature Of Business";
NSString *const DROP_DOWN_SOURCE_OF_REFERRAL = @"Source Of Referral";
NSString *const DROP_DOWN_WORK_EXPERIENCE = @"Work Experience";
NSString *const DROP_DOWN_RECOMMEND_SCHEME = @"Recommend Scheme";
NSString *const DROP_DOWN_PRESENT_EMPLOYMENT_CONDITION = @"Present Employment Condition";
NSString *const DROP_DOWN_MARITAL_STATUS = @"Marital Status";
NSString *const DROP_DOWN_NEWCOMER_SOURCE = @"Newcomer Source";
NSString *const DROP_DOWN_IDENTITY_TYPE = @"Identity Type";
NSString *const DROP_DOWN_ADDRESS = @"Address";
NSString *const DROP_DOWN_EDUCATIONLEVEL = @"EducationLevel";


NSString *const ENUM_MARITAL_MARRIED = @"已婚";
NSString *const ENUM_MARITAL_SINGLE = @"单身";
NSString *const ENUM_MARITAL_WIDOWED = @"丧偶";
NSString *const ENUM_MARITAL_DIVORCED = @"离婚";

NSString *const FIRST_INTERVIEW_PASS = @"Pass";
NSString *const FIRST_INTERVIEW_FAIL = @"Fail";
NSString *const FIRST_INTERVIEW_GA = @"GA";
NSString *const FIRST_INTERVIEW_HA = @"HA";
NSString *const FIRST_INTERVIEW_GEN = @"Gen Y";
NSString *const FIRST_INTERVIEW_SA = @"SA";

NSString *const ACTIVITY_HOLIDAY = @"Holiday";
NSString *const ACTIVITY_NAP_TRANING = @"岗前培训";
NSString *const ACTIVITY_BIRTHDAY = @"生日";
NSString *const ACTIVITY_ALE = @"资格考试";
NSString *const ACTIVITY_EOP = @"EOP";
NSString *const ACTIVITY_INT = @"面试";
NSString *const ACTIVITY_APPOINTMENT = @"预约";
NSString *const ACTIVITY_OTHER = @"其它";
NSString *const ACTIVITY_TELEPHONE = @"电话会话";
NSString *const ACTIVITY_PARTICIPATION = @"参与";
NSString *const ACTIVITY_ICALENDAR = @"我的日程";


/* RECRUITMENT STEPS NOTE TEXT */

NSString *const INTERACTIVE_PRESENTER_COMPLETED_DESCRIPTION = @"Interactive Presenter";
NSString *const EOP_REGISTRATION_COMPLETED_DESCRIPTION = @"Registered for EOP Event Successfully";
NSString *const FIRST_INTERVIEW_COMPLETED_DESCRIPTION = @"Completed First Interview";
NSString *const ATTENDED_EOP_DESCRIPTION = @"Attended EOP Event";
NSString *const CC_TEST_COMPLETED_DESCRIPTION = @"Completed CC Test";
NSString *const COMPANY_INTERVIEW_COMPLETED_DESCRIPTION = @"Completed Company Interview";
NSString *const ALE_EXAM_COMPLETED_DESCRIPTION = @"Completed ALE Exam";
NSString *const ABC_TRAINING_COMPLETED_DESCRIPTION = @"Passed ABC Training";
NSString *const CONTRACT_COMPLETED_DESCRIPTION = @"Completed Contract";

const int CANDIDATE_NAME = 50;
const int NRIC = 18;
const int ADDRESS1 = 10;
const int ADDRESS2 = 10;
const int ADDRESS3 = 10;
const int ADDRESS = 28;
const int POSTAL_CODE = 6;
const int FIXED_LINE_NO = 20;
const int MOBILE_NO = 11;
const int EMAIL = 28;

@end
