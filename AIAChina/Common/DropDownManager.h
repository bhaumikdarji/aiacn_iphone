//
//  DropDownManager.h
//  AddressBook
//
//  Created by AIA on 01/06/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TblContact.h"
#import "TblGroup.h"

@interface DropDownManager : NSObject{
    
}

@property (nonatomic, strong) TblContact *createdContact;
@property (nonatomic, strong) TblGroup *createdGroup;

+(DropDownManager *)sharedManager;

@property (strong, nonatomic) NSArray *gender;
@property (strong, nonatomic) NSArray *education;
@property (strong, nonatomic) NSArray *educationLevel;
@property (strong, nonatomic) NSArray *race;
@property (strong, nonatomic) NSArray *maritalStatus;
@property (strong, nonatomic) NSArray *natureOfBusiness;
@property (strong, nonatomic) NSArray *presentEmploymentCondition;
@property (strong, nonatomic) NSArray *recommendScheme;
@property (strong, nonatomic) NSArray *sourceOfReferral;
@property (strong, nonatomic) NSArray *talentProfile;
@property (strong, nonatomic) NSArray *workExperience;
@property (strong, nonatomic) NSArray *gamaContentChinese;
@property (strong, nonatomic) NSArray *gamaContentEnglish;
@property (strong, nonatomic) NSArray *newcomerSource;
@property (strong, nonatomic) NSArray *identityType;
@property (strong, nonatomic) NSArray *csvData;

- (NSString *)getGender:(NSString *)code
              andBySource:(NSString *)source;

- (NSArray *)getDropDownDescArray:(NSString *)dropdown;

- (NSArray *)getDropDownArray:(NSString *)dropdown;

- (NSString *)getDropdownDesc:(NSString *)dropdown
                         code:(NSString *)code;

- (NSString *)getDropdownCode:(NSString *)dropdown
                  description:(NSString *)description;
@end
