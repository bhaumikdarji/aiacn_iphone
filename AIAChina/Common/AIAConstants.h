//
//  AIAConstants.h
//  AIAChina
//
//  Created by AIA on 27/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIAConstants : NSObject

extern NSString *const UNGROUP;

extern NSString *const RECURITMENT_INTERACTIVE_PRESENER;
extern NSString *const RECURITMENT_EOP_REGISTRATION;
extern NSString *const RECURITMENT_FIRST_INTERVIEW;
extern NSString *const RECURITMENT_ATTEND_EOP;
extern NSString *const RECURITMENT_CC_TEST;
extern NSString *const RECURITMENT_COMPANY_INTERVIEW;
extern NSString *const RECURITMENT_ALE_EXAM;
extern NSString *const RECURITMENT_ABC_TRAINING;
extern NSString *const RECURITMENT_CONTRACT;
extern NSString *const RECURITMENT_STATUS_TRUE;
extern NSString *const RECURITMENT_STATUS_FALSE;

extern NSString *const URGENT_RECRUIT;
extern NSString *const NORMAL_RECRUIT;
extern NSString *const CAUTION_RECRUIT;

extern NSString *const DROP_DOWN_GENDER;
extern NSString *const DROP_DOWN_EDUCATION;
extern NSString *const DROP_DOWN_RACE;
extern NSString *const DROP_DOWN_TALENT_PROFILE;
extern NSString *const DROP_DOWN_NATURE_OF_BUSINESS;
extern NSString *const DROP_DOWN_SOURCE_OF_REFERRAL;
extern NSString *const DROP_DOWN_WORK_EXPERIENCE;
extern NSString *const DROP_DOWN_RECOMMEND_SCHEME;
extern NSString *const DROP_DOWN_PRESENT_EMPLOYMENT_CONDITION;
extern NSString *const DROP_DOWN_MARITAL_STATUS;
extern NSString *const DROP_DOWN_NEWCOMER_SOURCE;
extern NSString *const DROP_DOWN_IDENTITY_TYPE;
extern NSString *const DROP_DOWN_ADDRESS;
extern NSString *const DROP_DOWN_EDUCATIONLEVEL;


extern NSString *const ENUM_MARITAL_MARRIED;
extern NSString *const ENUM_MARITAL_SINGLE;
extern NSString *const ENUM_MARITAL_WIDOWED;
extern NSString *const ENUM_MARITAL_DIVORCED;

extern NSString *const FIRST_INTERVIEW_PASS;
extern NSString *const FIRST_INTERVIEW_FAIL;
extern NSString *const FIRST_INTERVIEW_GA;
extern NSString *const FIRST_INTERVIEW_HA;
extern NSString *const FIRST_INTERVIEW_GEN;
extern NSString *const FIRST_INTERVIEW_SA;

extern NSString *const ACTIVITY_HOLIDAY;
extern NSString *const ACTIVITY_NAP_TRANING;
extern NSString *const ACTIVITY_BIRTHDAY;
extern NSString *const ACTIVITY_ALE;
extern NSString *const ACTIVITY_EOP;
extern NSString *const ACTIVITY_INT;
extern NSString *const ACTIVITY_APPOINTMENT;
extern NSString *const ACTIVITY_OTHER;
extern NSString *const ACTIVITY_TELEPHONE;
extern NSString *const ACTIVITY_PARTICIPATION;
extern NSString *const ACTIVITY_ICALENDAR;


/* RECRUITMENT STEPS NOTE TEXT */

extern NSString *const INTERACTIVE_PRESENTER_COMPLETED_DESCRIPTION;
extern NSString *const EOP_REGISTRATION_COMPLETED_DESCRIPTION;
extern NSString *const FIRST_INTERVIEW_COMPLETED_DESCRIPTION;
extern NSString *const ATTENDED_EOP_DESCRIPTION;
extern NSString *const CC_TEST_COMPLETED_DESCRIPTION;
extern NSString *const COMPANY_INTERVIEW_COMPLETED_DESCRIPTION;
extern NSString *const ALE_EXAM_COMPLETED_DESCRIPTION;
extern NSString *const ABC_TRAINING_COMPLETED_DESCRIPTION;
extern NSString *const CONTRACT_COMPLETED_DESCRIPTION;

extern const int CANDIDATE_NAME;
extern const int NRIC;
extern const int ADDRESS1;
extern const int ADDRESS2;
extern const int ADDRESS3;
extern const int ADDRESS;
extern const int POSTAL_CODE;
extern const int FIXED_LINE_NO;
extern const int MOBILE_NO;
extern const int EMAIL;

@end
