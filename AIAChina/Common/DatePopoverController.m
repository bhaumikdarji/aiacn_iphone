//
//  DatePopoverController.m
//  AIAChina
//
//  Created by MacMini on 12/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "DatePopoverController.h"

@interface DatePopoverController ()

@end

@implementation DatePopoverController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datePicker.datePickerMode = self.mode;
    if(self.maxDate)
        self.datePicker.maximumDate = self.maxDate;
    if(self.minDate)
        self.datePicker.minimumDate = self.minDate;
    if(self.selectDateBlock)
        self.selectDateBlock(self.datePicker.date);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)datePickerChange:(id)sender{
    if(self.selectDateBlock)
        self.selectDateBlock(self.datePicker.date);
}

@end
