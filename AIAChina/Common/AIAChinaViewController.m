//
//  AIAChinaViewController.m
//  AIAChina
//
//  Created by AIA on 17/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "AssignContactViewController.h"
#import "EOPRegistrationViewController.h"
#import "FirstInterviewViewController.h"
#import "EOPAttendanceViewController.h"
#import "CCTestViewController.h"
#import "CompanyInterviewViewController.h"
#import "ALEExamViewController.h"
#import "PassedABCTrainingViewController.h"
#import "DatePopoverController.h"
#import "CandidateRankingViewController.h"
#import "RegisterSuccessfulController.h"
#import "CompanyIntSuccessController.h"
#import "SocialSharingViewController.h"
#import "WXApi.h"
#import "InterviewRegCandAndAttandViewController.h"
#import "EOPRegCandAndAttandViewController.h"
#import "FTDHomeViewController.h"
#import <ShareSDK/ShareSDK.h>
#import "TencentOpenAPI/QQApiInterface.h"
#import "ResultViewController.h"
#import "MapViewController.h"
#import "AddressPickerViewController.h"
#import "MHFacebookImageViewer.h"

#import "DZNPhotoPickerController.h"
#import "UIImagePickerController+Edit.h"
#import "ContactDataProvider.h"
#import "HomeViewController.h"
#import "AgentProfileViewController.h"

@interface AIAChinaViewController ()
{
    UIPopoverController *_popoverController;
}

@property (nonatomic,strong) BMKGeoCodeSearch *searcher;
- (void)openInteractivePresenter;
@end

float const slide_timing = 0.25;

@implementation AIAChinaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showPanel = YES;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateAnnouncement" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(announcementCount:) name:@"updateAnnouncement" object:nil];
    
    [self.navigationController.navigationBar setTitleTextAttributes: @{
                                                                       NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                       NSFontAttributeName:[UIFont fontWithName:@"DIN Condensed" size:18]
                                                                       }];
    self.navigationItem.leftBarButtonItem = [self prepareLeftBarButton:NO withLogo:NO];
    self.navigationItem.rightBarButtonItem = [self prepareRightBarButton:NO];
    [self setMenuView];
    [self hideMenu];
    [self.menuVC hideTableViewMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAssignPopoverCtrl:(NSArray *)ignorArray
            withSelectedArray:(NSMutableArray *)selectedArray
                    isContact:(BOOL)isContact
                     isDelete:(BOOL)isDelete
                     isExport:(BOOL)isExport
            isAssigneeContact:(BOOL)isAssigneeContact
                         rect:(CGRect)rect
                       inView:(UIView *)view
              selectedContact:(void (^)(NSArray *))selectedContact{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    AssignContactViewController *assignViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"AssignContactViewController"];
    assignViewCtrl.isContact = isContact;
    assignViewCtrl.isDelete = isDelete;
    assignViewCtrl.isExport = isExport;
    assignViewCtrl.isAssigneeContact = isAssigneeContact;
    if(ignorArray)
        assignViewCtrl.ignoreArray = ignorArray;
    if(selectedArray)
        assignViewCtrl.selectedArray = selectedArray;
    
    assignViewCtrl.selectedContactBlock = selectedContact ;
    
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:assignViewCtrl];
    self.pickerPopover.backgroundColor = assignViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                 inView:view
               permittedArrowDirections:0
                               animated:TRUE];
}

- (void)showEOPRegistrationPopoverCtrl:(TblContact *)contact
                                  rect:(CGRect)rect
                                inView:(UIView *)view
                         callBackBlock:(void (^)())callBackBlock{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    EOPRegistrationViewController *eopReg = [storyboard instantiateViewControllerWithIdentifier:@"EOPRegistrationViewController"];
    eopReg.contact = contact;
    eopReg.callBackBlock = callBackBlock;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:eopReg];
    self.pickerPopover.backgroundColor = eopReg.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                 inView:view
               permittedArrowDirections:0
                               animated:TRUE];
}

- (void)showEOPRegCandAndAttandPopoverCtrl:(CGRect)rect
                                    inView:(UIView *)view
                                 eventCode:(NSInteger)eventCode
                             callBackBlock:(void (^)())callBackBlock{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    EOPRegCandAndAttandViewController *companyInterviewViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"TotalEOPCandidateController"];
    companyInterviewViewCtrl.eventCode = eventCode;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:companyInterviewViewCtrl];
    self.pickerPopover.backgroundColor = companyInterviewViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:0
                                      animated:TRUE];
}

- (void)showFirstInterviewPopoverCtrl:(TblContact *)contact
                                rect:(CGRect)rect
                              inView:(UIView *)view
                       callBackBlock:(void (^)())callBackBlock{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    FirstInterviewViewController *firstInterviewViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"FirstInterviewViewController"];
    firstInterviewViewCtrl.contact = contact;
    firstInterviewViewCtrl.callBackBlock = callBackBlock;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:firstInterviewViewCtrl];
    self.pickerPopover.backgroundColor = firstInterviewViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                 inView:view
               permittedArrowDirections:0
                               animated:TRUE];
}

- (void)showEOPAttendancePopoverCtrl:(NSArray *)eopRegisteredCandidate
                                rect:(CGRect)rect
                              inView:(UIView *)view
                       callBackBlock:(void (^)(BOOL))callbackBlock{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    EOPAttendanceViewController *eopAttend = [storyboard instantiateViewControllerWithIdentifier:@"EOPAttendanceViewController"];
    eopAttend.callBackBlock = callbackBlock;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:eopAttend];
    eopAttend.eopRegisteredCandidateArray = [eopRegisteredCandidate mutableCopy];
    self.pickerPopover.backgroundColor = eopAttend.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                 inView:view
               permittedArrowDirections:0
                               animated:TRUE];
}

- (void)showCCTestPopoverCtrl:(TblContact *)contact
                         rect:(CGRect)rect
                       inView:(UIView *)view
                callBackBlock:(void (^)())callBackBlock{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    CCTestViewController *ccTestViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"CCTestViewController"];
    ccTestViewCtrl.contact = contact;
    ccTestViewCtrl.callBackBlock = callBackBlock;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:ccTestViewCtrl];
    self.pickerPopover.backgroundColor = ccTestViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                 inView:view
               permittedArrowDirections:0
                               animated:TRUE];
}

- (void)showCompanyInterviewPopoverCtrl:(TblContact *)contact
                                   rect:(CGRect)rect
                                 inView:(UIView *)view
                          callBackBlock:(void (^)(BOOL))callbackBlock
                          candidateForm:(void (^)())candidateFromBlock
{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    CompanyInterviewViewController *companyInterviewViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"CompanyInterviewViewController"];
    companyInterviewViewCtrl.contact = contact;
    companyInterviewViewCtrl.callBackBlock = callbackBlock;
    companyInterviewViewCtrl.candidateApplicationBlock = candidateFromBlock;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:companyInterviewViewCtrl];
    self.pickerPopover.backgroundColor = companyInterviewViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:0
                                      animated:TRUE];
    
}

- (void)showInterviewRegCandAndAttandPopoverCtrl:(CGRect)rect
                                          inView:(UIView *)view
                                     interviewId:(TblInterviewID *)interviewId
                                   callBackBlock:(void (^)())callBackBlock{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    InterviewRegCandAndAttandViewController *companyInterviewViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"TotalCompanyCandidateController"];
    companyInterviewViewCtrl.interviewId = interviewId;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:companyInterviewViewCtrl];
    self.pickerPopover.backgroundColor = companyInterviewViewCtrl.view.backgroundColor;
    
    companyInterviewViewCtrl.parentPopOver = self.pickerPopover;
    
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:0
                                      animated:TRUE];
    
}


- (void)showALEExamPopoverCtrl:(NSString *)title
                         descr:(NSString *)descrp
                          rect:(CGRect)rect
                        inView:(UIView *)view
                 callBackBlock:(void (^)())callBackBlock{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    ALEExamViewController *aleExamViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"ALEExamViewController"];
    aleExamViewCtrl.popTitle = title;
    aleExamViewCtrl.popDescription = descrp;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:aleExamViewCtrl];
    self.pickerPopover.backgroundColor = aleExamViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                 inView:view
               permittedArrowDirections:0
                               animated:TRUE];
}

- (void)showABCTrainingPassPopoverCtrl:(CGRect)rect
                                inView:(UIView *)view
                         callBackBlock:(void (^)())callBackBlock{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    PassedABCTrainingViewController *passABCTrainingViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"PassedABCTrainingViewController"];
    passABCTrainingViewCtrl.callBackBlock = callBackBlock;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:passABCTrainingViewCtrl];
    self.pickerPopover.backgroundColor = passABCTrainingViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                 inView:view
               permittedArrowDirections:0
                               animated:TRUE];
}

- (void)showCandidateRankingPassPopoverCtrl:(int)rank
                                       rect:(CGRect)rect
                                     inView:(UIView *)view
                                    candidateAgentCode:(NSString *)candidateAgentCode{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    CandidateRankingViewController *candidateRankingViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"CandidateRankingViewController"];
    candidateRankingViewCtrl.candidateProgressRank = rank;
    candidateRankingViewCtrl.candidateAgentCode = candidateAgentCode;
    [candidateRankingViewCtrl setSearchBlock:^{
        [CachingService sharedInstance].searchForContactCompleted = YES;
        [self performSegueWithIdentifier:@"ContactDetailToSearch" sender:self];
    }];
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:candidateRankingViewCtrl];
    self.pickerPopover.backgroundColor = candidateRankingViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:0
                                      animated:TRUE];
}

- (void)showRegisterSuccessfulPopoverCtrl:(int)rank
                            candidateName:(NSString *)name
                                     desc:(NSString *)desc
                                     rect:(CGRect)rect
                                   inView:(UIView *)view{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    RegisterSuccessfulController *registerSuccessfulViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"RegisterSuccessfulController"];
    registerSuccessfulViewCtrl.candidateProgressRank = rank;
    registerSuccessfulViewCtrl.candidateName = name;
    registerSuccessfulViewCtrl.desc = desc;
    registerSuccessfulViewCtrl.view.alpha = 0.1;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:registerSuccessfulViewCtrl];
    
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:0
                                      animated:TRUE];
    if (([[[UIDevice currentDevice] systemVersion] floatValue] < 8)) {
        self.pickerPopover.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.08];
    }else{
        self.pickerPopover.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.01];
    }
    
    
}

- (void)showCompanyIntSuccessPopoverCtrl:(int)rank
                                    rect:(CGRect)rect
                                  inView:(UIView *)view{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    CompanyIntSuccessController *companyIntSuccessfulViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"CompanyIntSuccessController"];
    companyIntSuccessfulViewCtrl.candidateProgressRank = rank;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:companyIntSuccessfulViewCtrl];
    self.pickerPopover.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.08];
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:0
                                      animated:TRUE];
}

- (void)showResultViewPopoverCtrl:(TblContact *)contact
                        interview:(TblInterview *)interview
                             rect:(CGRect)rect
                           inView:(UIView *)view{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    ResultViewController *resultViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"ResultViewController"];
    resultViewCtrl.interview = interview;
    resultViewCtrl.contact = contact;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:resultViewCtrl];
    self.pickerPopover.backgroundColor = resultViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:0
                                      animated:TRUE];
}

- (void)showSocialSharingPopoverCtrl:(TblContact *)contact
                              EOPObj:(TblEOP *)eop
                          isGreeting:(BOOL)isGreeting
                               isEOP:(BOOL)isEOP
                           isQRCode:(BOOL)isQRCode
                          shareImage:(UIImage *)shareImage
                      shareImageFile:(NSString *)shareImageFile
                            shareMsg:(NSString *)shareMsg
                            shareSub:(NSString *)shareSub
                                rect:(CGRect)rect
                              inView:(UIView *)view{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    SocialSharingViewController *socialSharingViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"SocialSharingViewController"];
    socialSharingViewCtrl.contact = contact;
    socialSharingViewCtrl.eop = eop;
    socialSharingViewCtrl.isGreeting = isGreeting;
    socialSharingViewCtrl.isEOP = isEOP;
    socialSharingViewCtrl.isQRCode = isQRCode;
    socialSharingViewCtrl.shareImage = shareImage;
    socialSharingViewCtrl.shareImageFile = shareImageFile;
    socialSharingViewCtrl.shareMsg = shareMsg;
    socialSharingViewCtrl.shareSub = shareSub;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:socialSharingViewCtrl];
    self.pickerPopover.backgroundColor = socialSharingViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:UIPopoverArrowDirectionAny
                                      animated:TRUE];
}

- (void)showAddressPickerViewController:(CGRect)rect
                                 inView:(UIView *)view
                          callBackBlock:(void (^)(TblAddress *address))callBackBlock{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    AddressPickerViewController *addressPicketCntrlr = [storyboard instantiateViewControllerWithIdentifier:@"AddressPickerViewController"];
    addressPicketCntrlr.callBackBlock = callBackBlock;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:addressPicketCntrlr];
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:0
                                      animated:TRUE];
}

- (void)showDatePopoverCtrl:(UIDatePickerMode)mode
                minimumDate:(NSDate *)minDate
                maximumDate:(NSDate *)maxDate
                       rect:(CGRect)rect
                     inView:(UIView *)view
                 pickerDate:(void (^)(NSDate *))pickerDate{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    DatePopoverController *datePopoverViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"DatePopoverController"];
    datePopoverViewCtrl.mode = mode;
    datePopoverViewCtrl.selectDateBlock = pickerDate;
    datePopoverViewCtrl.maxDate = maxDate;
    datePopoverViewCtrl.minDate = minDate;
    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:datePopoverViewCtrl];
    self.pickerPopover.backgroundColor = datePopoverViewCtrl.view.backgroundColor;
    [self.pickerPopover presentPopoverFromRect:rect
                                        inView:view
                      permittedArrowDirections:UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown
                                      animated:TRUE];
}

- (void)showPhotoActionSheet:(UIButton *)btn withCircularCrop:(BOOL)isCircularCrop;
{
    [self showPhotoActionSheet:btn withCircularCrop:isCircularCrop withFullScreen:NO];
}

-(void)showPhotoActionSheet:(UIButton *)btn withCircularCrop1:(BOOL)isCircularCrop1{
    [self showPhotoActionSheet:btn withCircularCrop1:isCircularCrop1 withFullScreen:NO];
}

- (void)showPhotoActionSheet:(UIButton *)btn withCircularCrop:(BOOL)isCircularCrop withFullScreen:(BOOL)isFullScreen
{
    isFullScreen = YES;
    [self showImageinFullScreen:btn];
}

- (void)showPhotoActionSheet:(UIButton *)btn withCircularCrop1:(BOOL)isCircularCrop withFullScreen:(BOOL)isFullScreen
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    alert.view.tintColor = [UIColor colorWithRed:45.0/255.0
                                           green:45.0/255.0
                                            blue:45.0/255.0
                                           alpha:1.0];

    UIAlertAction *takePhoto = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"Camera", @"")
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action){
                             [self takePhotoCamera:isCircularCrop];
                         }];
    
    UIAlertAction *chooseFromLibrary = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"Gallery", @"")
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action){
                             [self takePhotoLibrary:btn isCropModeCircular:isCircularCrop];
                         }];
    
    
    [alert addAction:takePhoto];
    [alert addAction:chooseFromLibrary];
    if (btn.imageView.image && isFullScreen) {
        UIAlertAction *showFullScreen = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"Show in full screen", @"")
                                         style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action){
                                            [self showImageinFullScreen:btn];
                                         }];

        [alert addAction:showFullScreen];
       
}

    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    popPresenter.sourceView = self.view;
    [popPresenter setPermittedArrowDirections:0];
    popPresenter.sourceRect = self.view.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)takePhotoCamera:(BOOL)isCircularCrop
{
    BOOL ok = [UIImagePickerController isSourceTypeAvailable:
               UIImagePickerControllerSourceTypeCamera];
    if(!ok){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                        message:NSLocalizedString(@"No camera found on the device", @"")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }

    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [self presentImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera sender:nil isCropModeCircular:isCircularCrop];
    }
}

- (void)presentImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType sender:(id)sender isCropModeCircular:(BOOL)isCropModeCircular
{
    __weak __typeof(self)weakSelf = self;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = sourceType;
    picker.allowsEditing = YES;
    picker.delegate = self;
    if (isCropModeCircular)
        picker.cropMode = DZNPhotoEditorViewControllerCropModeCircular;
    
    picker.finalizationBlock = ^(UIImagePickerController *picker, NSDictionary *info) {
        
        // Dismiss when the crop mode was disabled
        if (picker.cropMode == DZNPhotoEditorViewControllerCropModeNone) {
            if ([weakSelf respondsToSelector:@selector(handleImagePicker:withMediaInfo:)]) {
                [weakSelf handleImagePicker:picker withMediaInfo:info];
            }
        }
    };
    
    picker.cancellationBlock = ^(UIImagePickerController *picker) {
        [picker dismissViewControllerAnimated:YES completion:nil];
        
    };
    
    [self presentController:picker sender:nil];
}

- (void)presentController:(UIViewController *)controller sender:(id)sender
{
    if (_popoverController.isPopoverVisible) {
        [_popoverController dismissPopoverAnimated:YES];
        _popoverController = nil;
    }
    
//    if (_actionSheet.isVisible) {
//        _actionSheet = nil;
//    }
//    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        controller.preferredContentSize = CGSizeMake(320.0, 520.0);
        
        _popoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        [_popoverController presentPopoverFromRect:CGRectMake(0, 0, 10, 10)
                                                inView:self.view
                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                              animated:TRUE];
        
    }
    else {
        [self presentViewController:controller animated:YES completion:NULL];
    }
}


- (void)takePhotoLibrary:(UIButton *)sender isCropModeCircular:(BOOL)isCropModeCircular
{
    BOOL ok = [UIImagePickerController isSourceTypeAvailable:
               UIImagePickerControllerSourceTypePhotoLibrary];
    if(!ok){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                        message:NSLocalizedString(@"No camera found on the device", @"")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    [self presentImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary sender:nil isCropModeCircular:isCropModeCircular];
//    UIImagePickerController* picker = [UIImagePickerController new];
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    picker.delegate = self;
//    
//    picker.navigationBar.barTintColor = [UIColor colorWithRed:209.0/255.0 green:24.0/255.0 blue:72.0/255.0 alpha:1.0];
//    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
//    
//    UIView *anchor = sender;
//    self.pickerPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
//    self.pickerPopover.backgroundColor = [UIColor colorWithRed:209.0/255.0 green:24.0/255.0 blue:72.0/255.0 alpha:1.0];
//    [self.pickerPopover presentPopoverFromRect:anchor.frame inView:anchor.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    self.pickerPopover.delegate = self;
}

- (void)showImageinFullScreen:(UIButton *)sender
{
    MHFacebookImageViewer *imageViewer = [[MHFacebookImageViewer alloc]init];
    imageViewer.senderView = sender.imageView;
    [imageViewer presentFromRootViewController];
}



- (void)shareQQ:(UIImage *)shareImage
        subject:(NSString *)subject
            msg:(NSString *)msg
  callBackBlock:(void (^)(NSString *errorMsg))callBackBlock{
    
    id<ISSContent> publishContent = nil;
    if(shareImage) {

        publishContent = [ShareSDK content:msg
                            defaultContent:@""
                                     image:[ShareSDK jpegImageWithImage:shareImage quality:1.0]
                                     title:@"E路领航"
                                       url:nil
                               description:@""
                                 mediaType:SSPublishContentMediaTypeImage];
    }
    else
        publishContent = [ShareSDK content:msg
                            defaultContent:@""
                                     image:nil
                                     title:subject
                                       url:nil
                               description:@""
                                 mediaType:SSPublishContentMediaTypeText];
    
    [ShareSDK shareContent:publishContent
                      type:ShareTypeQQ
               authOptions:nil
              shareOptions:nil
             statusBarTips:YES
                    result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                        if (state == SSPublishContentStateSuccess)
                        {
                            NSLog(NSLocalizedString(@"TEXT_SHARE_SUC", @"发表成功"));
                        }
                        else if (state == SSPublishContentStateFail)
                        {
                            NSLog(NSLocalizedString(@"TEXT_SHARE_FAI", @"发布失败!error code == %d, error code == %@"), [error errorCode], [error errorDescription]);
                            [DisplayAlert showAlertInControllerInView:self
                                                                title:@""
                                                              message:NSLocalizedString(@"Please install QQ before you are allowed to use this function", @"")
                                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                        okButtonTitle:@""
                                                          cancelBlock:nil
                                                              okBlock:nil];
                        }
                    }];
}

- (NSString *)handleSendResult:(QQApiSendResultCode)sendResult
{
    if(sendResult == EQQAPIAPPNOTREGISTED)
        return @"App未注册"; // "Unregistered App"
    else if(sendResult == EQQAPIMESSAGECONTENTINVALID || sendResult == EQQAPIMESSAGECONTENTNULL || sendResult == EQQAPIMESSAGETYPEINVALID)
        return @"发送参数错误";//Send parameter error";
    else if(sendResult == EQQAPIQQNOTINSTALLED) {
        
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"Please install QQ before you are allowed to use this function", @"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        return @"未安装手Q";//Hand Q is not installed";
    }
    else if(sendResult == EQQAPIQQNOTSUPPORTAPI)
        return @"API接口不支持";//API interface does not support";
    else if(sendResult == EQQAPISENDFAILD)
        return @"发送失败";//Failed to send";
    else if(sendResult == EQQAPISENDSUCESS)
        return @"成就";//Success
    else
        return @"";
}



- (void)shareWeChat:(id)sender
              image:(UIImage *)shareImage
            subject:(NSString *)subject
                msg:(NSString *)msg
      callBackBlock:(void (^)(BOOL isInstalled, BOOL isSuccess))callBackBlock
{
    if (![WXApi isWXAppInstalled]) {
        callBackBlock(FALSE, TRUE);
        return;
    }
    
    
    id<ISSContent> publishContent;
    
    if(shareImage) {
        
        publishContent = [ShareSDK content:msg
                            defaultContent:@""
                                     image:[ShareSDK jpegImageWithImage:shareImage quality:1.0]
                                     title:@"E路领航"
                                       url:nil
                               description:@""
                                 mediaType:SSPublishContentMediaTypeImage];
    }
    else
        publishContent = [ShareSDK content:msg
                            defaultContent:@""
                                     image:nil
                                     title:subject
                                       url:nil
                               description:@""
                                 mediaType:SSPublishContentMediaTypeText];
    
    
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    

    NSArray *shareList = [ShareSDK customShareListWithType:
                          SHARE_TYPE_NUMBER(ShareTypeWeixiSession),
                          SHARE_TYPE_NUMBER(ShareTypeWeixiTimeline),
                          nil];
    
    [ShareSDK showShareActionSheet:container
                         shareList:shareList
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                NSLog(@"=== response state :%zi ",state);
                                if (state == SSResponseStateSuccess)
                                {
                                    callBackBlock (TRUE, TRUE);
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    callBackBlock(TRUE, FALSE);
                                }
                            }];
}


- (void)shareWeChat:(UIImage *)shareImage
            subject:(NSString *)subject
                msg:(NSString *)msg
           isMoment:(BOOL)isMoment
      callBackBlock:(void (^)(BOOL isInstalled, BOOL isSuccess))callBackBlock{
    
    if (![WXApi isWXAppInstalled]) {
        callBackBlock(FALSE, TRUE);
        return;
    }
   
//    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
//    if(shareImage){
//        WXMediaMessage *message = [WXMediaMessage message];
//        WXImageObject *ext = [WXImageObject object];
//        ext.imageData = UIImageJPEGRepresentation(shareImage, 1);
//        message.mediaObject = ext;
//        
//        req.bText = NO;
//        req.message = message;
////        if(isMoment){
//            req.scene = WXSceneTimeline;
////        }else{
////            req.scene = WXSceneSession;
////        }
//    }else{
//        req.bText = YES;
//        req.text = msg;
//        if(isMoment){
//            req.scene = WXSceneTimeline;
//        }else{
//            req.scene = WXSceneSession;
//        }
//    }
//    
//    if ([WXApi sendReq:req]) {
//        callBackBlock (TRUE, TRUE);
//    }
//    else {
//        [self showErrorAlertWithMessage:NSLocalizedString(@"Please install WeChat before you are allowed to use this function", @"")];
//        callBackBlock(TRUE, FALSE);
//    }
}


- (void)shareEmail:(UIImage *)shareImage
         recipient:(NSArray *)recipient
          fileName:(NSString *)fileName
           subject:(NSString *)subject
               msg:(NSString *)msg
     callBackBlock:(void (^)(BOOL isInstalled))callBackBlock{

    if([MFMailComposeViewController canSendMail]){
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        
        if(shareImage){
            NSData* data = UIImageJPEGRepresentation(shareImage, 1.0);
            [mc addAttachmentData:data
                         mimeType:@"image/jpg"
                         fileName:fileName];
        }
        
        [mc setToRecipients:recipient];
        [mc setSubject:subject];
        [mc setMessageBody:msg isHTML:FALSE];

        [self presentViewController:mc animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }];
    }else{
       // [self showErrorAlertWithMessage:NSLocalizedString(@"can not send email,please configure email account", @"")];
        callBackBlock(FALSE);
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

//- (void)openInteractivePresenter
//{
//    FTDHomeViewController *FTDHomeViewCol=[[FTDHomeViewController alloc]init];
//    TFDNavViewController *navFTDHomeViewCol=[[TFDNavViewController alloc]initWithRootViewController:FTDHomeViewCol];
//    navFTDHomeViewCol.navigationBar.hidden=YES;
//    [navFTDHomeViewCol setCallBackBlock:^{
//    
//        NSLog(@"Show EOP events");
//    
//    }];
//    [self presentViewController:navFTDHomeViewCol animated:YES completion:nil];
//}

- (void)showErrorAlertWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showWebView
{
    self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(10.0f, 20.0f, [[UIScreen mainScreen] bounds].size.width - 20, [[UIScreen mainScreen] bounds].size.height - 10)];
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.frame = CGRectMake(985.0f, 19.0f, 32.0f, 32.0f);
    [self.closeButton setBackgroundImage:[UIImage imageNamed:@"close_red.png"] forState:UIControlStateNormal];
    self.webView.contentMode = UIViewContentModeScaleAspectFit;
    self.webView.scalesPageToFit = YES;
    [self.closeButton addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
    [[self.webView layer] setCornerRadius:10];
    [self.webView setClipsToBounds:YES];
    self.webView.delegate = nil;
}

- (void)closeWebView{
    [self.webView removeFromSuperview];
    [self.closeButton removeFromSuperview];
}


- ( void ) getSearchLocation:(NSString*)address
{
    // initialize the search object
    if (!_searcher) {
        _searcher = [ [ BMKGeoCodeSearch alloc ] init ] ;
        _searcher.delegate  = self ;
    }
    BMKGeoCodeSearchOption * geoCodeSearchOption =  [ [ BMKGeoCodeSearchOption alloc ] init ] ;
    geoCodeSearchOption.address  = address;
    BOOL Flag =  [ _searcher geoCode : geoCodeSearchOption ] ;
    if ( Flag )
    {
        NSLog ( @ "geo sent successfully retrieved" ) ;
    }
    else
    {
        NSLog ( @ "geo retrieve sending failed" ) ;
    }
    
    // Initiate reverse geocoding to retrieve
    // CLLocationCoordinate2D pt = (CLLocationCoordinate2D) {39.915, 116.404};
    // BMKReverseGeoCodeOption * reverseGeoCodeSearchOption = [[
    // BMKReverseGeoCodeOption alloc] init];
    //reverseGeoCodeSearchOption.reverseGeoPoint = pt;
    // BOOL Flag = [_searcher reverseGeocode: reverseGeoCodeSearchOption];
    // [reverseGeoCodeSearchOption Release];
    // if (Flag)
    // {
    // NSLog (@ "Anti geo sent successfully retrieved");
    //}
    // else
    // {
    // NSLog (@ "Anti geo retrieve sending failed");
    //}
}

// Handle the callback result to achieve Deleage
// receive a positive result of coding

-  ( void ) onGetGeoCodeResult : ( BMKGeoCodeSearch * ) Searcher result : ( BMKGeoCodeResult * ) result errorCode : ( BMKSearchErrorCode ) error {
    if  ( error == BMK_SEARCH_NO_ERROR )  {
        // In this process, a normal result
        CLLocationCoordinate2D location  = result.location;
        NSLog(@"%0.2f lat , %0.2f long",location.latitude,location.longitude);
        UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
        UINavigationController *navCtrl = [storyboard instantiateViewControllerWithIdentifier:@"MapNavController"];
        MapViewController *mapViewCtrl = navCtrl.viewControllers[0];
        mapViewCtrl.latitude = location.latitude;
        mapViewCtrl.longitude = location.longitude;
        [mapViewCtrl setLocationBlock:^(double latitide, double longitude, NSString *address) {
//            self.latitude = latitide;
//            self.longitude = longitude;
//            self.txtLocation.text = address;
        }];
        [self presentViewController:navCtrl animated:YES completion:nil];
    }
    else  {
        NSLog ( @ "Sorry, no results found " ) ;
    }
}

- (void)createNoteWithTblContact:(TblContact *)contact
                        noteDate:(NSDate *)noteDate
                        noteDesc:(NSString *)noteDesc{
    
    TblNotes *note = [[ContactDataProvider sharedContactDataProvider] createNote];
    note.iosAddressCode = [NSString stringWithFormat:@"%@",[[note objectID] URIRepresentation]];
    note.isSystemValue = TRUE;
    note.activityDate = noteDate;
    note.activityType = NSLocalizedString(@"Others", @"");
    note.isDelete = [NSNumber numberWithBool:FALSE];
    note.desc = noteDesc;
    note.activityStatus = [NSNumber numberWithInt:1];
    contact.lastContactedDate = [NSDate date];
    [contact addCandidateNotesObject:note];
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
}

- (BOOL)isInternetConnect{
    if(![AFNetworkReachabilityManager sharedManager].isReachable){
        [DisplayAlert showAlertInControllerInView:self
                                            title:NSLocalizedString(@"There is no internet connection currently", @"")
                                          message:@""
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return FALSE;
    }
    return TRUE;
}

- (UIImage *)resizeImage:(UIImage *)image;
{
    NSData *imageDataWithoutCompression = UIImageJPEGRepresentation(image,0.1);
    return [UIImage imageWithData:imageDataWithoutCompression];
    
}

- (void)addRecruitmentNote:(NSInteger)eventCode
           recruitmentStep:(enum recruitmentSteps)recruitmentStep
              activityDate:(NSDate *)activityDate
                   contact:(TblContact *)contact
               description:(NSString *)description{
    
    TblNotes *note = nil;
    NSArray *noteArray = nil;
    BOOL shouldSave = FALSE;
    
    if(recruitmentStep == RS_COMPANY_INTERVIEW ||
       recruitmentStep == RS_ABC_TRANING){
        noteArray = [[contact.candidateNotes allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"desc=%@",description]];
    }else{
        noteArray = [[contact.candidateNotes allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"desc=%@ AND activityDate=%@",description,activityDate]];
    }
    
    if(noteArray.count<=0){
        note = [[ContactDataProvider sharedContactDataProvider] createNote];
        shouldSave = TRUE;
    }

    if(shouldSave){
        note.eventCode = [NSNumber numberWithInteger:eventCode];
        note.iosAddressCode = [NSString stringWithFormat:@"%@",[[note objectID] URIRepresentation]];
        note.activityDate = activityDate;
        note.activityType = @"Others";
        note.desc = description;
        note.activityStatus = [NSNumber numberWithInt:1];
        note.recruitmentStep = [NSNumber numberWithInteger:recruitmentStep];
        note.isDelete = [NSNumber numberWithBool:FALSE];
        note.isSync = [NSNumber numberWithBool:TRUE];
        note.isSystem = [NSNumber numberWithBool:TRUE];
        contact.lastContactedDate = [NSDate date];
        [contact addCandidateNotesObject:note];
        [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    }
}

- (UIBarButtonItem *)prepareLeftBarButton:(BOOL)isPrepare withLogo:(BOOL)isLogo{
    if (isPrepare) {
        UIView *leftBarView = [[UIView alloc]init];
        [leftBarView setFrame:CGRectMake(16, 2, 90, 40)];
        
        if (isLogo) {
            UIImageView *logo = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"aia-logo.png"]];
            [leftBarView addSubview:logo];
        } else {
            UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
            [back setFrame:CGRectMake(0, 0, 80, 40)];
            [back setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
            [back setImageEdgeInsets:UIEdgeInsetsMake(10, 0, 5, 60)];
            [back setHighlighted:NO];
            [back addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
            [back setExclusiveTouch:YES];
            [leftBarView addSubview:back];
        }
        
        UIBarButtonItem *barBtn = [[UIBarButtonItem alloc] initWithCustomView:leftBarView];
        return barBtn;
    }
    return nil;
}

- (void)back:(id)sender{
    if ([[self presentingViewController] presentedViewController] ) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (UIBarButtonItem *)prepareRightBarButton:(BOOL)isPrepare{
    if (isPrepare) {
        UIView *rightBarView = [[UIView alloc]init];
        [rightBarView setFrame:CGRectMake(913, 0, 95, 51)];
        [rightBarView setBackgroundColor:[UIColor clearColor]];
        
        UIButton *btnBell = [UIButton buttonWithType:UIButtonTypeCustom]; 
        [btnBell setFrame:CGRectMake(12,13,18,20)];
        [btnBell setImage:[UIImage imageNamed:@"bell"] forState:UIControlStateNormal];
        [btnBell addTarget:self action:@selector(openAnnouncement) forControlEvents:UIControlEventTouchUpInside];
        [rightBarView addSubview:btnBell];
        
        _btnCount = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.btnCount setFrame:CGRectMake(22,13,23,22)];
        [self.btnCount setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnCount addTarget:self action:@selector(openAnnouncement) forControlEvents:UIControlEventTouchUpInside];
        [self.btnCount setBackgroundColor:[UIColor colorWithRed:249.0/255.0 green:180.0/255.0 blue:70.0/255.0 alpha:1.0]];
        [self.btnCount.titleLabel setFont:[UIFont fontWithName:@"DIN Condensed" size:12]];
        [self.btnCount.layer setCornerRadius:11];
        [rightBarView addSubview:self.btnCount];
        
        UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnMenu setFrame:CGRectMake(53,0,45,45)];
        [btnMenu setImage:[UIImage imageNamed:@"btn_menu"] forState:UIControlStateNormal];
        [btnMenu setImageEdgeInsets:UIEdgeInsetsMake(18,15,13,16)];
        [btnMenu addTarget:self action:@selector(menuPressed) forControlEvents:UIControlEventTouchUpInside];
        [rightBarView addSubview:btnMenu];
        
        UIBarButtonItem *barBtn = [[UIBarButtonItem alloc] initWithCustomView:rightBarView];
        return barBtn;
    }
    return nil;
}

- (void)announcementCount:(NSNotification*)notification{
    NSDictionary* userInfo = notification.userInfo;
    [self.btnCount setTitle:userInfo[@"total"] forState:UIControlStateNormal];
}

- (void) setMenuView
{
    self.menuVC = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
    self.menuVC.delegate = self;
    [self.menuVC.view setHidden:YES];
    
    self.attachBlackView = [[UIView alloc]initWithFrame:CGRectMake(0, tablePosY, tablePosX, tableHeight)];
    [self.attachBlackView setBackgroundColor:[UIColor blackColor]];
    [self.attachBlackView setAlpha:0.4];
    [self.attachBlackView setHidden:YES];
    
    [self.navigationController.view addSubview:self.attachBlackView];
    [self.navigationController.view addSubview:self.menuVC.view];
}

-(void) menuPressed {
    
    if (self.showPanel) {
        [self showMenu];
    } else {
        [self hideMenu];
    }
}

-(UIView *)getMenuView
{
    if (self.menuVC == nil) {
        [self setMenuView];
    }
    UIView *view = self.menuVC.view;
    return view;
}

-(void)showMenu
{
    [self.view endEditing:YES];
    self.showPanel = NO;
    [self.menuVC.view setHidden:NO];
    [self.attachBlackView setHidden:NO];
    UIView *childView = [self getMenuView];
    [self.view bringSubviewToFront:childView];
    
    [UIView animateWithDuration:slide_timing delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.menuVC.view.frame = CGRectMake(tablePosX,tablePosY, -1*ceil(tableWidth), tableHeight);
        [self.menuVC showTableViewMenu];
        [self.attachBlackView setAlpha:0.4];
    } completion:^(BOOL finished) {
        if (finished) {
        }
    }];
    UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, nil);
}

- (void)hideMenu {
    self.showPanel = YES;
    
    [UIView animateWithDuration:slide_timing delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.menuVC.view.frame = CGRectMake(tablePosX , tablePosY,ceil(tableWidth), tableHeight);
        [self.attachBlackView setAlpha:0.0];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.menuVC.view setHidden:YES];
            [self.attachBlackView setHidden:YES];
        }
    }];
    UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, nil);
}
- (void)hideView{
    [self hideMenu];
}

- (void)menuItemSelected:(int)index{
    [self hideMenu];
    
    switch (index) {
            
        case 0:
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[HomeViewController class]]) {
                    [self.navigationController popToViewController:controller animated:YES];
                    break;
                }
            }
            break;
            
        case 1:
            
            [(HomeViewController *)[self navigateToMainParent] openInteractivePresenter];
            break;
            
        case 2:
            
            if (![[self restorationIdentifier] isEqualToString:@"contactController"]) {
                [(HomeViewController *)[self navigateToMainParent] navigateToAddressBook];
            }
            break;
            
        case 3:
            if (![[self restorationIdentifier] isEqualToString:@"CalendarViewController"]) {
                [(HomeViewController *)[self navigateToMainParent] onBrnCalendarClick:(HomeViewController *)[self navigateToMainParent]];
            }
            break;
            
        case 4:
            break;
            
        case 5:
            break;
            
        case 6:
            break;
            
        case 7:
            
            if (![[self restorationIdentifier] isEqualToString:@"AgentProfileStoryboard"]) {
                [(HomeViewController *)[self navigateToMainParent] onAgentProfileImageButtonClick:(HomeViewController *)[self navigateToMainParent]];
            }
            break;
            
        case 8:
            
            if (![[self restorationIdentifier] isEqualToString:@"SettingViewController"]) {
                [(HomeViewController *)[self navigateToMainParent] btnSettingClick];
            }
            break;
    }
}

- (UIViewController *)navigateToMainParent{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[HomeViewController class]]) {
//            [self.navigationController popToViewController:controller animated:NO];
            return controller;
        }
    }
    return 0;
}

- (void)openAnnouncement{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openAnnouncement" object:nil];
}

@end
