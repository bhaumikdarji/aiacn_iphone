
#import <Foundation/Foundation.h>

enum DeviceType {
    IPHONE = 0,
    IPAD = 1,
    IPOD =2,
    IPHONE_SIMULATOR = 3,
    IPAD_SIMULATOR = 4,
};
@interface DeviceInfo : NSObject {
    int type;
    NSString *model;
}
@property (assign) int type;
@property (nonatomic,retain) NSString *model;

+(DeviceInfo *) sharedInfo;
@end