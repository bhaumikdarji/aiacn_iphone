//
//  DateUtils.h
//  EventApp
//
//  Created by Quix Creations on 24/11/14.
//  Copyright (c) 2014 Quix Creations. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtils : NSObject

+ (NSString *)dateToString:(NSDate *)date
                andFormate:(NSString *)formate;

+ (NSDate *)stringToDate:(NSString *)dateString
              dateFormat:(NSString *)dateFormat;

+ (NSString *)suffixDateStringFromDate:(NSDate *)DateLocal
                            dateFormat:(NSString *)dateFormat;

+ (NSString *)getLocalizedStringFromDate:(NSDate *)date
                                 formate:(NSString *)formate;

+ (NSString *)timeFormattedOnTotalSecond:(int)totalSeconds;

+ (NSString *)timeFormattedOnTotalMinute:(int)totalMinute;

+ (NSString *)timeFormattedOnTotalHour:(int)totalHour;

+ (NSInteger)ageFromBirthday:(NSDate *)birthdate;

+ (bool)isDate:(NSDate *) aDate isLessThan:(int) days;

+ (NSDate *)monthAgo:(int)month;

+ (NSDate *)addOneMonth:(NSDate *)date;

@end
