

#import "Utility.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CTStringAttributes.h>
#import <CoreText/CoreText.h>

NSString *const DataLoadExceptionJson = @"DataLoadException";

@implementation Utility

+ (void)roundCornerProfileImage:(UIColor *)borderColor
                    borderWidth:(float)borderWidth
                      imageView:(UIImageView *)imageView{
    if(borderWidth>0){
        imageView.layer.borderWidth = 3.0f;
        if(borderColor){
            imageView.layer.borderColor = [borderColor CGColor];
        }else{
            imageView.layer.borderColor = [UIColor whiteColor].CGColor;
        }
    }
    
    imageView.layer.cornerRadius = imageView.frame.size.width / 2;
    imageView.clipsToBounds = YES;
}

+ (void)setRoundCornerRedius:(id)control
                cornerRedius:(float)radius{
    ((UIControl *)control).layer.masksToBounds = TRUE;
    ((UIControl *)control).layer.cornerRadius = radius;
}

+ (void)setBorderWithColor:(id)control
               borderColor:(UIColor *)borderColor
               borderWidth:(float)width{
    ((UIControl *)control).layer.borderColor = borderColor.CGColor;
    ((UIControl *)control).layer.borderWidth = width;
}

+ (void)setOuterShadow:(id)control
                 color:(UIColor *)shadowColor
               opacity:(float)opacity
                radius:(float)radius{
    [((UIControl *)control).layer setShadowColor:shadowColor.CGColor];
    [((UIControl *)control).layer setShadowOpacity:opacity];
    [((UIControl *)control).layer setShadowRadius:radius];
    [((UIControl *)control).layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

+ (void)setTextFieldPlaceholderTextColor:(UITextField *)textField
                        placeholderColor:(UIColor *)color{
    [textField setValue:color forKeyPath:@"_placeholderLabel.textColor"];
}

+ (void)setBorderAndRoundCorner:(id)control borderColor:(UIColor *)color width:(CGFloat)width cornerRedius:(CGFloat)radius{
    [self setRoundCornerRedius:control cornerRedius:radius];
    [self setBorderWithColor:control borderColor:color borderWidth:width];
}

+ (void)setTextFieldLeftView:(UITextField *)textField imgName:(NSString *)imageName{
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    if(![imageName isEqualToString:@""]){
        UIImageView *imgLeftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        
        leftView.frame = CGRectMake(leftView.frame.origin.x, leftView.frame.origin.y, imgLeftView.image.size.width + 15, imgLeftView.image.size.height);
        imgLeftView.frame = CGRectMake(8, ((imgLeftView.image.size.height / 2) - (leftView.frame.size.height / 2)), imgLeftView.image.size.width, imgLeftView.image.size.height);
        
        [leftView addSubview:imgLeftView];
    }
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

+ (void)addPaddingToTextField:(UITextField *)textField
                  leftPadding:(float)leftPadding
                 rightPadding:(float)rightPadding{
    if(leftPadding>0){
        UIView *leftPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leftPadding, textField.frame.size.height)];
        textField.leftView = leftPaddingView;
        textField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    if(rightPadding>0){
        UIView *rightPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rightPadding, textField.frame.size.height)];
        textField.rightView = rightPaddingView;
        textField.rightViewMode = UITextFieldViewModeAlways;
    }
}

+ (void)animateViewVertically:(UIView *)view
                       Offset:(float)offset{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    CGRect rect = view.frame;
    rect.origin.y -=offset;
    view.frame = rect;
    [UIView commitAnimations];
}

+ (void)animateWithConstraint:(NSLayoutConstraint *)layoutConstrainst
                constantValue:(float)constantValue
                    superView:(UIView *)superView
            animationDuration:(NSTimeInterval)timeInterval{
    [UIView animateWithDuration:timeInterval
                     animations:^{
                         layoutConstrainst.constant = constantValue;
                         [superView layoutIfNeeded];
                     }];
}

+ (NSDictionary *)parametersDictionaryFromQueryString:(NSString *)queryString {
    
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    
    NSArray *queryComponents = [queryString componentsSeparatedByString:@"&"];
    
    for(NSString *s in queryComponents) {
        NSArray *pair = [s componentsSeparatedByString:@"="];
        if([pair count] != 2) continue;

        NSString *key = pair[0];
        NSString *value = pair[1];
        
        md[key] = value;
    }
    
    return md;
}

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime
                     andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

+ (NSDateComponents *)differenceBetween:(NSDate*)fromDateTime
                                andDate:(NSDate*)toDateTime{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute
                                               fromDate:fromDateTime
                                                 toDate:toDateTime
                                                options:0];
    
    return components;
}

+ (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+ (NSDate *) startOfDay:(NSDate *)date {
    NSCalendar* gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* dateComponents = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    
    return [gregorian dateFromComponents:dateComponents];
}

+ (NSDate *) endOfDay:(NSDate *)date {
    NSCalendar* gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* dateComponents = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    
    [dateComponents setHour:23];
    [dateComponents setMinute:59];
    [dateComponents setSecond:59];
    
    return [gregorian dateFromComponents:dateComponents];
}

+ (NSDate *)midnightUTC:(NSDate *)date {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:[NSTimeZone localTimeZone]];
    
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                                   fromDate:date];
    [dateComponents setHour:00];
    [dateComponents setMinute:00];
    [dateComponents setSecond:00];
    [dateComponents setNanosecond:00];
    
    NSDate *midnightUTC = [calendar dateFromComponents:dateComponents];
    
    return midnightUTC;
}

+ (NSDictionary *)getDataOfQueryString:(NSString *)url{
    
    NSArray *strURLParse = [url componentsSeparatedByString:@"?"];
    if ([strURLParse count] < 2) {
        return nil;
    }
    NSArray *arrQueryString = [[strURLParse objectAtIndex:1] componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    for (int i=0; i < [arrQueryString count]; i++) {
        NSArray *arrElement = [[arrQueryString objectAtIndex:i] componentsSeparatedByString:@"="];
        if ([arrElement count] == 2) {
            [params setObject:[arrElement objectAtIndex:1] forKey:[arrElement objectAtIndex:0]];
        }
    }
    
    return params;
}

+ (BOOL)isNumeric:(NSString*)value{
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *punc = [NSCharacterSet punctuationCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:value];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet] || [punc isSupersetOfSet:stringSet];
    return isValid;
}

+ (UIImage *)blurWithCoreImage:(UIImage *)sourceImage setView:(UIView *)view{
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    // Apply Affine-Clamp filter to stretch the image so that it does not
    // look shrunken when gaussian blur is applied
    CGAffineTransform transform = CGAffineTransformIdentity;
    CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    [clampFilter setValue:inputImage forKey:@"inputImage"];
    [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    // Apply gaussian blur filter with radius of 30
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
    [gaussianBlurFilter setValue:@4 forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
    
    // Set up output context.
    UIGraphicsBeginImageContext(view.frame.size);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    
    // Invert image coordinates
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -view.frame.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, view.frame, cgImage);
    
    // Apply white tint
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5].CGColor);
    CGContextFillRect(outputContext, view.frame);
    CGContextRestoreGState(outputContext);
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}

+ (NSMutableAttributedString *)setUnderline:(NSString *)string withTextColor:(UIColor *)textColor{
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",string]];
    [commentString addAttributes:@{NSForegroundColorAttributeName : textColor,
                                   NSUnderlineStyleAttributeName : [NSNumber numberWithInteger:NSUnderlineStyleSingle]
                                   } range:NSMakeRange(0, [commentString length])];
    return commentString;
}

+ (id)loadJsonDataFromPath:(NSString *)path{
    NSError *error = nil;
    
    @try {
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:[path stringByDeletingPathExtension] ofType:[path pathExtension]];
        
        NSData *data = [NSData dataWithContentsOfFile:resourcePath options:0 error:&error];
        if (error)
            [NSException raise:DataLoadExceptionJson format:@"%@", error];
        
        error = nil;
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if (error)
            [NSException raise:DataLoadExceptionJson format:@"%@", error];
        
        return json;
    }
    @catch (NSException *exception) {
        NSLog(@"*** Error loading json file %@. Error:%@", path, exception.description);
    }
}

+ (NSAttributedString *)setBoldTextOnSingleLable:(NSString *)text
                                        fontSize:(CGFloat)fontSize
                                           range:(NSRange)setRange
                                setBoldFontColor:(UIColor *)boldColor
{
    
    UIFont *boldFont = [UIFont boldSystemFontOfSize:fontSize+5];
    UIFont *regularFont = [UIFont systemFontOfSize:fontSize];
    
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                           regularFont, NSFontAttributeName,
                           nil];
    
    NSDictionary *boldAttrs = [NSDictionary dictionaryWithObjectsAndKeys:
                               boldFont, NSFontAttributeName,
                               boldColor, NSForegroundColorAttributeName,
                              nil];
    
//    NSDictionary *supAttrs = [NSDictionary dictionaryWithObjectsAndKeys:
//                               @1, kCTSuperscriptAttributeName,
//                               nil];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text
                                                                                       attributes:attrs];
    [attributedText setAttributes:boldAttrs range:setRange];
    //[attributedText setAttributes:supAttrs range:NSMakeRange(text.length - 3, 2)];
    
    return attributedText;
}

+ (NSAttributedString *)setSupAttrOnSingleLable:(NSString *)text
                                       boldFont:(UIFont *)boldFont
                                    regularFont:(UIFont *)regularFont
                                          range:(NSRange)setRange
                               setBoldFontColor:(UIColor *)boldColor{
    
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                           regularFont, NSFontAttributeName,
                           nil];
    
    NSDictionary *supAttrs = [NSDictionary dictionaryWithObjectsAndKeys:
                              boldFont, NSFontAttributeName,
                              boldColor, NSForegroundColorAttributeName,
                              nil];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text
                                                                                       attributes:attrs];
    [attributedText setAttributes:supAttrs range:setRange];
    
    return attributedText;
}

+ (NSString*)ordinalNumberFormat:(int)num {
    NSString *ending;
    
    int ones = num % 10;
    int tens = floor(num / 10);
    tens = tens % 10;
    
    if(tens == 1){
        ending = @"th";
    } else {
        switch (ones) {
            case 1:
                ending = @"st";
                break;
            case 2:
                ending = @"nd";
                break;
            case 3:
                ending = @"rd";
                break;
            default:
                ending = @"th";
                break;
        }
    }
    return [NSString stringWithFormat:@"%d%@", num, ending];
}

+ (BOOL)isFirstLaunch
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"FirstRun"]) {
        // Delete values from keychain here
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FirstRun"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return YES;
    } else {
        return NO;
    }
}

@end
