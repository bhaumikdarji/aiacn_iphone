//
//  DbUtils.m
//  CDS-Elite
//
//  Created by Smruti Consulting & Services on 23/05/14.
//  Copyright (c) 2014 CDS. All rights reserved.
//

#import "DbUtils.h"

@implementation DbUtils
+ (NSManagedObject *)fetchObject:(NSString *)enityName
                      andPredict:(NSPredicate *)predicate
               andSortDescriptor:(NSSortDescriptor *)sortDescriptor
            managedObjectContext:(NSManagedObjectContext *)manageObjectContext{
    
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:enityName
                                              inManagedObjectContext:manageObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    [request setIncludesSubentities:FALSE];
    NSError *error = nil;
    NSArray *array = [manageObjectContext executeFetchRequest:request error:&error];
    
    
    if(!error){
        
        if(predicate)
            array = [array filteredArrayUsingPredicate:predicate];
        
        if(sortDescriptor)
            array =  [array sortedArrayUsingDescriptors:@[sortDescriptor]];
    }
    
        if(array!=nil && array.count>0){
            return [array objectAtIndex:0];
    }
    
    return nil;
}

+ (NSArray *)fetchAllObject:(NSString *)enityName
                 andPredict:(NSPredicate *)predicate
          andSortDescriptor:(NSSortDescriptor *)sortDescriptor
       managedObjectContext:(NSManagedObjectContext *)manageObjectContext{
    
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:enityName
                                              inManagedObjectContext:manageObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSError *error = nil;
    NSArray *array = [manageObjectContext executeFetchRequest:request error:&error];
    
    
    if(!error){
        
        if(predicate)
            array = [array filteredArrayUsingPredicate:predicate];
        
        if(sortDescriptor)
            array =  [array sortedArrayUsingDescriptors:@[sortDescriptor]];
        if(array!=nil && array.count>0){
            return array;
        }
    }
    
    return nil;
}

+ (NSArray *)fetchAllObject:(NSString *)enityName
                 andPredict:(NSPredicate *)predicate
     andSortDescriptorArray:(NSArray *)sortDescriptorArray
       managedObjectContext:(NSManagedObjectContext *)manageObjectContext{
    
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:enityName
                                              inManagedObjectContext:manageObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSError *error = nil;
    NSArray *array = [manageObjectContext executeFetchRequest:request error:&error];
    
    
    if(!error){
        
        if(predicate)
            array = [array filteredArrayUsingPredicate:predicate];
        
        if(sortDescriptorArray)
            array =  [array sortedArrayUsingDescriptors:sortDescriptorArray];
        if(array!=nil && array.count>0){
            return array;
        }
    }
    
    return nil;
}

+ (NSArray *)fetchAttribute:(NSString *)enityName
              attributeName:(NSString *)attributeName
               andPredicate:(NSPredicate *)predicate
       managedObjectContext:(NSManagedObjectContext *)manageObjectContext{
    
    NSEntityDescription *entity = [NSEntityDescription  entityForName:enityName
                                               inManagedObjectContext:manageObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    [request setResultType:NSDictionaryResultType];
    [request setReturnsDistinctResults:YES];
    [request setPropertiesToFetch:@[attributeName]];
    
    if(predicate)
        [request setPredicate:predicate];

    NSError *error = nil;
    NSArray *objects = [manageObjectContext executeFetchRequest:request
                                                          error:&error];
    
    if(!error){
        return objects;
    }
    
    return nil;
}

+ (NSArray *)fetchDistinctValues:(NSString *)entityName
                   attributeName:(NSString *)attributeName
                    andPredicate:(NSPredicate *)predicate
               andSortDescriptor:(NSSortDescriptor *)sortDescriptor
            managedObjectContext:(NSManagedObjectContext *)manageObjectContext{
    NSEntityDescription *entity = [NSEntityDescription  entityForName:entityName
                                               inManagedObjectContext:manageObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    [request setResultType:NSDictionaryResultType];
    [request setReturnsDistinctResults:YES];
    [request setPropertiesToFetch:@[attributeName]];
    
    if(predicate)
        [request setPredicate:predicate];
    
    if(sortDescriptor)
        [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    // Execute the fetch.
    NSError *error;
    NSArray *resultArray = [manageObjectContext executeFetchRequest:request error:&error];
    
    if(!error){
        return resultArray;
    }
    
    return nil;
}

+ (void)deleteAllObjectsInContext{
//    NSArray *entities = (MyAppDelegate).managedObjectModel.entities;
//    for (NSEntityDescription *entityDescription in entities) {
//        [DbUtils deleteAllObjectsWithEntityName:entityDescription.name
//                                      inContext:(MyAppDelegate).managedObjectContext];
//    }
}

+ (void)deleteAllObjectsWithEntityName:(NSString *)entityName
                             inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *fetchRequest =
    [NSFetchRequest fetchRequestWithEntityName:entityName];
    fetchRequest.includesPropertyValues = NO;
    fetchRequest.includesSubentities = NO;
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *managedObject in items) {
        [context deleteObject:managedObject];
    }
    
    if(![context save:nil]){
        
    }
}

+ (void)deleteAllObjectsWithEntityName:(NSString *)entityName
                               predict:(NSPredicate *)predict
                             inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *fetchRequest =
    [NSFetchRequest fetchRequestWithEntityName:entityName];
    fetchRequest.includesPropertyValues = NO;
    fetchRequest.includesSubentities = NO;
    [fetchRequest setPredicate:predict];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *managedObject in items) {
        [context deleteObject:managedObject];
    }
    
    if(![context save:nil]){
        
    }
}

+ (NSInteger)getCountForEntity:(NSString *)entityName
                    andPredict:(NSPredicate *)predicate
          managedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    NSFetchRequest* fetch = [NSFetchRequest fetchRequestWithEntityName:entityName];
    
    if(predicate!=nil){
        [fetch setPredicate:predicate];
    }
    
    return [managedObjectContext countForFetchRequest:fetch error:nil];
}

@end
