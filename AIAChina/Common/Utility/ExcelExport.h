//
//  Excel.h
//  libxl-example
//
//  Created by Rickey Tom on 2015-06-07.
//  Copyright (c) 2015 xlware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExcelExport : NSObject
+(NSString *) ExportNotesToFile:(NSArray *) dataDict;
+(NSString *) ExportContactsToFile:(NSArray *) dataDict;


@end
