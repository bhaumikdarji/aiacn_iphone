//
//  UIEventButton.h
//  AIAChina
//
//  Created by Tankar Shah on 28/12/16.
//  Copyright (c)  All rights reserved.

#import <UIKit/UIKit.h>

@interface UIEventButton : UIButton {
    id eventData;
}

@property (nonatomic, readwrite, retain) id eventData;

@end