//
//  DbUtils.h
//  CDS-Elite
//
//  Created by Smruti Consulting & Services on 23/05/14.
//  Copyright (c) 2014 CDS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DbUtils : NSObject{
    
}

+ (NSManagedObject *)fetchObject:(NSString *)enityName
                       andPredict:(NSPredicate *)predicate
                andSortDescriptor:(NSSortDescriptor *)sortDescriptor
             managedObjectContext:(NSManagedObjectContext *)manageObjectContext;

+ (NSArray *)fetchAllObject:(NSString *)enityName
                 andPredict:(NSPredicate *)predicate
          andSortDescriptor:(NSSortDescriptor *)sortDescriptor
       managedObjectContext:(NSManagedObjectContext *)manageObjectContext;

+ (NSArray *)fetchAllObject:(NSString *)enityName
                 andPredict:(NSPredicate *)predicate
     andSortDescriptorArray:(NSArray *)sortDescriptorArray
       managedObjectContext:(NSManagedObjectContext *)manageObjectContext;

+ (void)deleteAllObjectsInContext;

+ (void)deleteAllObjectsWithEntityName:(NSString *)entityName
                             inContext:(NSManagedObjectContext *)context;

+ (void)deleteAllObjectsWithEntityName:(NSString *)entityName
                               predict:(NSPredicate *)predict
                             inContext:(NSManagedObjectContext *)context;

+ (NSInteger)getCountForEntity:(NSString *)entityName
                    andPredict:(NSPredicate *)predicate
          managedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSArray *)fetchAttribute:(NSString *)enityName
              attributeName:(NSString *)attributeName
               andPredicate:(NSPredicate *)predicate
       managedObjectContext:(NSManagedObjectContext *)manageObjectContext;

+ (NSArray *)fetchDistinctValues:(NSString *)entityName
                   attributeName:(NSString *)attributeName
                    andPredicate:(NSPredicate *)predicate
               andSortDescriptor:(NSSortDescriptor *)sortDescriptor
            managedObjectContext:(NSManagedObjectContext *)manageObjectContext;

@end
