//
//  ValueParser.h
//  CDS-Elite
//
//  Created by Smruti Consulting & Services on 26/05/14.
//  Copyright (c) 2014 CDS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValueParser : NSObject

+(NSString *)parseString:(id)value;
+(int)parseInt:(id)value;
+(double)parseDouble:(id)value;
+(NSDate *)parseDate:(id)value
          andFormate:(NSString *)format;

@end
