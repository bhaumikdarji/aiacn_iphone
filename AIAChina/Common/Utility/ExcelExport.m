//
//  Excel.m
//  libxl-example
//
//  Created by Rickey Tom on 2015-06-07.
//  Copyright (c) 2015 xlware. All rights reserved.
//

#import "ExcelExport.h"
#include "LibXL/libxl.h"
#import "ValueParser.h"
#import "Utils.h"
#import "DropDownManager.h"
#import "AIAConstants.h"
#import "NSString+isNumeric.h"
#import "TblGroup.h"

@interface ExcelExport()

@end
char *const ExcelOwner = "Gordon Yeo";
char *const ExcelKey = "ios-a0e29c2fb25545f1e8ec103376jff8ma";

@implementation ExcelExport

+(NSString *) ExportNotesToFile:(NSArray *) dataDict
{
    BookHandle book = xlCreateBookCA();
    
    @try
    {
        xlBookSetKey(book, ExcelOwner, ExcelKey);
        
        xlBookLoadA(book, [[self templateNotesFileName] UTF8String]);
        const char *bookError = xlBookErrorMessageA(book);
        if(strcmp(bookError, "ok") != 0)
        {
            NSLog(@"WARNING: Error loading excel file. Description %s", bookError);
        }
        else
        {
            FormatHandle fmt = xlBookAddFormat(book, 0);
            xlFormatSetNumFormat(fmt, NUMFORMAT_NUMBER_SEP);
            xlFormatSetFont(fmt, [ExcelExport fontForBook:book]);
            xlFormatSetBorderRight(fmt, BORDERSTYLE_THIN);
            xlFormatSetBorderTop(fmt, BORDERSTYLE_THIN);
            
            FormatHandle dateFmt = xlBookAddFormat(book, 0);
            xlFormatSetFont(dateFmt, [ExcelExport fontForBook:book]);
            
            SheetHandle sheet = xlBookGetSheet(book, 0);
            if (sheet != nil)
            {
                int row = 2;
                int column = 1;
                
                [ExcelExport dateSheet:sheet row:0 column:3 usingFormat:dateFmt];
                for (TblNotes *notes in dataDict)
                {
                    ++row;
                    column = 0;
                    
                    NSString *activityDate;
                    if (notes.activityDate != nil)
                    {
                        activityDate = [NSString stringWithFormat:@"%@",[DateUtils dateToString:[NSDate date] andFormate:@"YYYY-MM-dd"]];
                    }
                    else
                    {
                        activityDate = @"";
                    }
                    
                    xlSheetWriteNum(sheet, row, column, row-2 , fmt);
                    column++;
                    if (notes.desc!=nil)
                        xlSheetWriteStr(sheet, row, column, [notes.desc UTF8String], fmt);
                    column++;
                    if (notes.activityType != nil)
                        xlSheetWriteStr(sheet, row, column, [notes.activityType UTF8String], fmt);
                    column++;
                    if (notes.activityDate!=nil)
                        xlSheetWriteStr(sheet, row, column, [activityDate UTF8String], fmt);
                }
            }
            
            // Save it.
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
            NSString* documentsDir = [paths objectAtIndex:0];
            NSString* myFilePath = [NSString stringWithFormat:@"%@/%@", documentsDir, @"ExportNotes.xls"];
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:myFilePath error:&error];
            xlBookSave(book, [myFilePath UTF8String]);
            
            NSLog(@"open %@", myFilePath);
            return myFilePath;
        }
    }
    @finally
    {
        xlBookReleaseA(book);
    }
    return nil;
}

+(NSString *) ExportContactsToFile:(NSArray *) dataDict
{
    BookHandle book = xlCreateBookCA();
 
    @try
    {
        xlBookSetKey(book, ExcelOwner, ExcelKey);
        
        xlBookLoadA(book, [[self templateExcelFileName] UTF8String]);
        const char *bookError = xlBookErrorMessageA(book);
        if(strcmp(bookError, "ok") != 0)
        {
            NSLog(@"WARNING: Error loading excel file. Description %s", bookError);
        }
        else
        {
            FormatHandle fmt = xlBookAddFormat(book, 0);
            xlFormatSetNumFormat(fmt, NUMFORMAT_NUMBER_SEP);
            xlFormatSetFont(fmt, [ExcelExport fontForBook:book]);
            xlFormatSetBorderRight(fmt, BORDERSTYLE_THIN);
            xlFormatSetBorderTop(fmt, BORDERSTYLE_THIN);

            FormatHandle dateFmt = xlBookAddFormat(book, 0);
            xlFormatSetFont(dateFmt, [ExcelExport fontForBook:book]);
            
            SheetHandle sheet = xlBookGetSheet(book, 0);
            if (sheet != nil)
            {
                int row = 2;
                int column = 0;
                NSString *str;
                [ExcelExport dateSheet:sheet row:0 column:9 usingFormat:dateFmt];
                
                for (TblContact *contact in dataDict)
                {
                    ++row;
                    column = 0;
                    NSString *birthDate,*age;
                    if (contact.birthDate != nil)
                    {
                        birthDate = [NSString stringWithFormat:@"%@",[DateUtils dateToString:[NSDate date] andFormate:@"YYYY-MM-dd"]];
                    }
                    else
                    {
                        birthDate = @"";
                    }
                    if (contact.age != nil) {
                        age = [contact.age stringValue];
                    }
                    else
                    {
                        age = @"";
                    }

                    xlSheetWriteNum(sheet, row, column, row-2, fmt);
                    column++;
                    if (contact.name!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.name UTF8String], fmt);
                    column++;
                    if (age!=nil)
                        xlSheetWriteStr(sheet, row, column, [age UTF8String], fmt);
                    column++;
                    if (birthDate!=nil)
                        xlSheetWriteStr(sheet, row, column, [birthDate UTF8String], fmt);
                    column++;
                    if (contact.gender!=nil) {
                        NSString *gender = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_GENDER code:[(NSString *)contact.gender getTrimmedValue]];
                        xlSheetWriteStr(sheet, row, column, [gender UTF8String], fmt);
                    }
                    column++;
                    if (contact.referalSource!=nil) {
                        NSString *refSource = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_SOURCE_OF_REFERRAL code:contact.referalSource];
                        xlSheetWriteStr(sheet, row, column, [refSource UTF8String], fmt);
                    }
                    column++;
                    if (contact.group !=nil) {
                        xlSheetWriteStr(sheet, row, column, [[[contact.group allObjects][0] groupName] UTF8String], fmt);
                    }
                    column++;
                    if (contact.registeredAddress!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.registeredAddress UTF8String], fmt);
                    column++;
                    if (contact.nric!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.nric UTF8String], fmt);
                    column++;
                    if (contact.eMailId!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.eMailId UTF8String], fmt);
                    column++;
                    if (contact.mobilePhoneNo!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.mobilePhoneNo UTF8String], fmt);
                    column++;

                    if (contact.fixedLineNO!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.fixedLineNO UTF8String], fmt);
                    column++;
                    if (contact.qq!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.qq UTF8String], fmt);
                    column++;
                    if (contact.weChat!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.weChat UTF8String], fmt);
                    column++;
                    if (contact.officePhoneNo!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.officePhoneNo UTF8String], fmt);
                    column++;
                    if (contact.marritalStatus!=nil) {
                        NSString *maritalStatus = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_MARITAL_STATUS code:contact.marritalStatus];
                        xlSheetWriteStr(sheet, row, column, [maritalStatus UTF8String], fmt);
                    }
                    column++;
                    if (contact.workingYearExperience!=nil) {
                        NSString *workExp = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_WORK_EXPERIENCE code:contact.workingYearExperience];
                        xlSheetWriteStr(sheet, row, column, [workExp UTF8String], fmt);
                    }
                    column++;
                    if (contact.race!=nil) {
                        NSString *race = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_RACE code:contact.race];
                        xlSheetWriteStr(sheet, row, column, [race UTF8String], fmt);
                    }
                    column++;
                    if (contact.education!=nil) {
                        NSString *education = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_EDUCATION code:contact.education];
                        xlSheetWriteStr(sheet, row, column, [education UTF8String], fmt);
                    }
                    column++;
                    if (contact.talentProfile!=nil) {
                        NSString *talentProfile = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_TALENT_PROFILE code:contact.talentProfile];
                        xlSheetWriteStr(sheet, row, column, [talentProfile UTF8String], fmt);
                    }
                    column++;
                    if (contact.reJoin!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.reJoin UTF8String], fmt);
                    column++;
                    if (contact.recommendedRecruitmentScheme!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.recommendedRecruitmentScheme UTF8String], fmt);
                    column++;
                    if (contact.yearlyIncome!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.yearlyIncome UTF8String], fmt);
                    column++;
                    if (contact.presentWorkCondition!=nil) {
                        NSString *presentWorkCondition = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_PRESENT_EMPLOYMENT_CONDITION code:contact.presentWorkCondition];
                        xlSheetWriteStr(sheet, row, column, [presentWorkCondition UTF8String], fmt);
                    }
                    column++;
                    if (contact.purchasedAnyInsurance!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.purchasedAnyInsurance UTF8String], fmt);
                    column++;
                    if (contact.salesExperience!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.salesExperience UTF8String], fmt);
                    column++;
                    if (contact.remarks!=nil)
                        xlSheetWriteStr(sheet, row, column, [contact.remarks UTF8String], fmt);

                }
            }
            
            // Save it.
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
            NSString* documentsDir = [paths objectAtIndex:0];
            NSString* myFilePath = [NSString stringWithFormat:@"%@/%@", documentsDir, @"ExportContacts.xls"];
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtPath:myFilePath error:&error];
            xlBookSave(book, [myFilePath UTF8String]);
            
            NSLog(@"open %@", myFilePath);
            return myFilePath;
        }
    }
    @finally
    {
        xlBookReleaseA(book);
    }
    return nil;
}

+ (NSString *)getDropdownValue:(NSArray *)dropdownArray code:(NSString *)code{
    NSArray *array = [dropdownArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Code=%@",code]];
    if(array.count > 0)
        return [[array objectAtIndex:0] valueForKey:@"Description"];
    else
        return @"";
}

+(void) dateSheet:(SheetHandle) sheet row:(int) row column:(int) col usingFormat:(FormatHandle) fmt
{
    xlSheetWriteStr(sheet, row, col, [[[ExcelExport dateFormatter] stringFromDate:[NSDate date]] UTF8String], fmt);
}

+(FontHandle) fontForBook:(BookHandle) book
{
    FontHandle font = xlBookAddFont(book, 0);
    xlFontSetSize(font, 6);
    xlFontSetBold(font, false);
    return font;
}

+(FontHandle) fontForBook9:(BookHandle) book
{
    FontHandle font = xlBookAddFont(book, 0);
    xlFontSetSize(font, 9);
    xlFontSetBold(font, false);
    return font;
}


+(NSDateFormatter *) dateFormatter
{
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken1 = 0;
    dispatch_once(&onceToken1, ^ {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    });
    
    return dateFormatter;
}

+ (NSString*)templateExcelFileName
{
    static NSString *defaultFile = nil;
    static dispatch_once_t token2 = 0;
    dispatch_once(&token2, ^ {
        defaultFile = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Contacts_Template.xls"];
        NSLog(@"defaultFile :%@",defaultFile);
    });
    return defaultFile;
}

+ (NSString*)templateNotesFileName
{
    static NSString *defaultFile = nil;
    static dispatch_once_t token2 = 0;
    dispatch_once(&token2, ^ {
        defaultFile = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Notes_Template.xls"];
        NSLog(@"defaultFile :%@",defaultFile);
    });
    return defaultFile;
}

@end
