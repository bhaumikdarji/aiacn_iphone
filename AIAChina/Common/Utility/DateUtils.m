//
//  DateUtils.m
//  EventApp
//
//  Created by Quix Creations on 24/11/14.
//  Copyright (c) 2014 Quix Creations. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

+ (NSString *)dateToString:(NSDate *)date
                andFormate:(NSString *)formate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setDateFormat:formate];
    NSString *stringFromDate = [formatter stringFromDate:date];
    if(stringFromDate)
        return stringFromDate;
    return @"";
}

+ (NSDate *)stringToDate:(NSString *)dateString
              dateFormat:(NSString *)dateFormat{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setDateFormat:dateFormat];
    NSDate *dateFromString = nil;
    dateFromString = [dateFormatter dateFromString:dateString];
    return dateFromString;
}

+ (NSString *)suffixDateStringFromDate:(NSDate *)DateLocal
                            dateFormat:(NSString *)dateFormat{
    
    // set formate this @"MMMM d., yyyy" 1st Jan 2015
    
    NSDateFormatter *prefixDateFormatter = [[NSDateFormatter alloc] init];
    [prefixDateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [prefixDateFormatter setDateFormat:dateFormat];//June 13th, 2013
    NSString * prefixDateString = [prefixDateFormatter stringFromDate:DateLocal];
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init];
    [monthDayFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [monthDayFormatter setDateFormat:@"d"];
    int date_day = [[monthDayFormatter stringFromDate:DateLocal] intValue];
    NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
    NSString *suffix = [suffixes objectAtIndex:date_day];
    
    prefixDateString = [prefixDateString stringByReplacingOccurrencesOfString:@"." withString:suffix];
    NSString *dateString =prefixDateString;
    return dateString;
}

+ (NSString *)getLocalizedStringFromDate:(NSDate *)date formate:(NSString *)formate{
    if(date){
        NSDateFormatter *dateFormate = [[NSDateFormatter alloc] init];
        [dateFormate setDateFormat:formate];
        [dateFormate setTimeZone:[NSTimeZone defaultTimeZone]];
        NSString *strDate = [dateFormate stringFromDate:date];
        if(strDate != nil){
            return [[strDate componentsSeparatedByString:@"+"] objectAtIndex:0];
        }
    }
    return @"";
}

+ (NSString *)timeFormattedOnTotalSecond:(int)totalSeconds{
//    int seconds = totalSeconds % 60;
    int minutes = totalSeconds % 3600 / 60;
    int hours = (totalSeconds % (8 * 3600)) / 3600;
    int day = totalSeconds / (8 * 3600);
    
    NSString *timeString = @"";
    if(day > 0)
        timeString = [timeString stringByAppendingString:[NSString stringWithFormat:@"%dd",day]];
    
    if(hours > 0)
        timeString = [timeString stringByAppendingString:[NSString stringWithFormat:@"%dh",hours]];
    
    if(minutes > 0)
        timeString = [timeString stringByAppendingString:[NSString stringWithFormat:@"%dm",minutes]];
    
    return timeString;
}

+ (NSString *)timeFormattedOnTotalMinute:(int)totalMinute{
    int totalSecond = totalMinute * 60;
    return [self timeFormattedOnTotalSecond:totalSecond];
}

+ (NSString *)timeFormattedOnTotalHour:(int)totalHour{
    int totalMinute = totalHour * 60;
    return [self timeFormattedOnTotalMinute:totalMinute];
}

+ (NSInteger)ageFromBirthday:(NSDate *)birthdate {
    if(birthdate){
        NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                           components:NSYearCalendarUnit
                                           fromDate:birthdate
                                           toDate:[NSDate date]
                                           options:0];
        return ageComponents.year;
    }
    return 0;
}

+ (bool)isDate:(NSDate *) aDate isLessThan:(int) days
{
    NSUInteger unitFlags = NSDayCalendarUnit;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:aDate toDate:[NSDate date ]options:0];
    return [components day] <= days;
}

+ (NSDate *)monthAgo:(int)month{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponent = [[NSDateComponents alloc] init];
    [dateComponent setMonth:-month];
    [dateComponent setDay:1];
    return [gregorian dateByAddingComponents:dateComponent
                                      toDate:[NSDate date]
                                     options:0];
}

+ (NSDate *)addOneMonth:(NSDate *)date{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponent = [[NSDateComponents alloc] init];
    [dateComponent setMonth:1];
    [dateComponent setDay:-1];
    return [gregorian dateByAddingComponents:dateComponent
                                      toDate:date
                                     options:0];
}

@end
