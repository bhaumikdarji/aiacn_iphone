

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface Utility : NSObject{

}

+ (void)roundCornerProfileImage:(UIColor *)borderColor
                    borderWidth:(float)borderWidth
                      imageView:(UIImageView *)imageView;

+ (void)setBorderAndRoundCorner:(id)control
                    borderColor:(UIColor *)color
                          width:(CGFloat)width
                   cornerRedius:(CGFloat)radius;

+ (void)setRoundCornerRedius:(id)control
                cornerRedius:(float)radius;

+ (void)setBorderWithColor:(id)control
               borderColor:(UIColor *)borderColor
               borderWidth:(float)width;

+ (void)setOuterShadow:(id)control
                 color:(UIColor *)shadowColor
               opacity:(float)opacity
                radius:(float)radius;

+ (void)setTextFieldPlaceholderTextColor:(UITextField *)textField
                        placeholderColor:(UIColor *)color;

+ (void)setTextFieldLeftView:(UITextField *)textField
                     imgName:(NSString *)imageName;

+ (void)addPaddingToTextField:(UITextField *)textField
                  leftPadding:(float)leftPadding
                 rightPadding:(float)rightPadding;

+ (void)animateViewVertically:(UIView *)view
                       Offset:(float)offset;

+ (void)animateWithConstraint:(NSLayoutConstraint *)layoutConstrainst
                constantValue:(float)constantValue
                    superView:(UIView *)superView
            animationDuration:(NSTimeInterval)timeInterval;

+ (NSDictionary *)parametersDictionaryFromQueryString:(NSString *)queryString;

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime
                     andDate:(NSDate*)toDateTime;

+ (NSDateComponents *)differenceBetween:(NSDate*)fromDateTime
                                andDate:(NSDate*)toDateTime;

+ (BOOL)validateEmail:(NSString *)candidate;

+ (NSDate *)midnightUTC:(NSDate *)date;

+ (NSDate *)startOfDay:(NSDate *)date;

+ (NSDate *)endOfDay:(NSDate *)date;

+ (NSDictionary *)getDataOfQueryString:(NSString *)url;

+ (BOOL)isNumeric:(NSString*)value;

+ (UIImage *)blurWithCoreImage:(UIImage *)sourceImage setView:(UIView *)view;

+ (NSMutableAttributedString *)setUnderline:(NSString *)string withTextColor:(UIColor *)textColor;

+ (id)loadJsonDataFromPath:(NSString *)path;

+ (NSAttributedString *)setBoldTextOnSingleLable:(NSString *)text fontSize:(CGFloat)fontSize range:(NSRange)setRange setBoldFontColor:(UIColor *)boldColor;

+ (NSAttributedString *)setSupAttrOnSingleLable:(NSString *)text
                                       boldFont:(UIFont *)boldFont
                                    regularFont:(UIFont *)regularFont
                                          range:(NSRange)setRange
                               setBoldFontColor:(UIColor *)boldColor;

+ (NSString*)ordinalNumberFormat:(int)num;

+ (BOOL)isFirstLaunch;

@end
