

#import "DeviceInfo.h"
#import <UIKit/UIKit.h>


static DeviceInfo *deviceInfo = nil;
@implementation DeviceInfo
@synthesize type;
@synthesize model;

+(DeviceInfo *) sharedInfo{
    @synchronized(self){
        if (deviceInfo == nil) {
            deviceInfo = [[DeviceInfo alloc] init];
        }
    }
    return deviceInfo;
}

-(id) init{
    if((self = [super init])){
        self.model= [[UIDevice currentDevice] model];
        if ([model isEqualToString:@"iPhone"]){
            type = IPHONE;
        }else if([model isEqualToString:@"iPad"]){
            type = IPAD;
        }else if ([model isEqualToString:@"iPhone Simulator"]) {
            type = IPHONE_SIMULATOR;
        }else if ([model isEqualToString:@"iPad Simulator"]) {
            type = IPAD_SIMULATOR;
        }else if ([model isEqualToString:@"iPod"]) {
            type = IPOD;
        }
    }
    return self;
}

@end