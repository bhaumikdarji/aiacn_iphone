//
//  ValueParser.m
//  CDS-Elite
//
//  Created by Smruti Consulting & Services on 26/05/14.
//  Copyright (c) 2014 CDS. All rights reserved.
//

#import "ValueParser.h"

@implementation ValueParser

+(NSString *)parseString:(id)value{
    if(value!=nil && value != [NSNull null] && ![[value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"(null)"])
        return value;
    
    return @"";
}

+(int)parseInt:(id)value{
    if(value != [NSNull null]){
        return [value intValue];
    }
    return 0;
}

+(double)parseDouble:(id)value{
    if(value != [NSNull null]){
        return [value doubleValue];
    }
    return 0.0;
}

+(NSDate *)parseDate:(id)value
          andFormate:(NSString *)format{
    if(value!=nil && value != [NSNull null]){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
//        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:format];
        return [dateFormatter dateFromString:value];
    }
    
    return nil;
}

@end
