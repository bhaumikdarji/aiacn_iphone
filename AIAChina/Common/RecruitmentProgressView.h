//
//  RecruitmentProgressView.h
//  AIAChina
//
//  Created by MacMini on 24/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecruitmentProgressView : UIView

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgProgress;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblProgress;

+ (id)loadRecruitmentProgressView;
- (void)setGui;
- (void)setSelectedProgress:(TblContact *)contact;

@end
