//
//  ApplicationFunction.h
//  AIAChina
//
//  Created by AIA on 31/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FFEvent.h"
#import "TblContact.h"
#import "TblEducation.h"
#import "TblESignature.h"

@interface ApplicationFunction : NSObject<UIPopoverControllerDelegate>

+ (UIStoryboard *)getCurrentStoryBoard;

+ (BOOL)isLogin;

+ (void)doLogOut;

+ (void)showToAgentProfile:(BOOL)isPresenter;

+ (void)showLoginView;

+ (void)setUpdateUserProcess:(NSString *)processName
                     contact:(TblContact *)contact
               processStatus:(NSString *)status
                 processDate:(NSDate *)date;

+ (void)showDatePopoverCtrl:(UIPopoverController *)popoverCtrl
                       rect:(CGRect)rect
                     inView:(UIView *)view
                 pickerDate:(void (^)(NSDate *))pickerDate;

+ (void)showESignaturePopoverCtrl:(UIPopoverController *)popoverCtrl
                        withTitle:(NSString *)title
                            descr:(NSString *)descrp
                             rect:(CGRect)rect
                           inView:(UIView *)view
                    callBackBlock:(void (^)())callBackBlock;

+ (void)deleteCalManualEvent:(FFEvent *)event;

+ (UIImage *)generateBarcodeImageWithQRCode:(NSString *)QRValue;

+ (void)openSafariBrowser:(NSString *)url;

+ (NSString *)recruitmentProgress:(TblContact *)contact;

+ (void)joinUngroup:(TblContact *)contact;

+ (void)removeUngroup:(NSArray *)contactArray;

+ (NSArray *)fetchYearlyBirthdayEvents:(NSInteger)year;

+ (NSArray *)fetchICalendarEvents:(NSInteger)year;

+ (void)showGrettingView:(NSArray *)receipt;

+ (BOOL)isCompletePersonalInfo:(TblContact *)contact;

+ (BOOL)isCompleteEducation:(TblContact *)contact;

+ (BOOL)isCompleteSignature:(TblContact *)contact;

@end
