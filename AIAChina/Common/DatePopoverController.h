//
//  DatePopoverController.h
//  AIAChina
//
//  Created by MacMini on 12/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePopoverController : UIViewController

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic) UIDatePickerMode mode;
@property (nonatomic, retain) NSDate *maxDate;
@property (nonatomic, retain) NSDate *minDate;
@property (nonatomic, copy) void (^selectDateBlock)(NSDate *selectedDate);

- (IBAction)datePickerChange:(id)sender;

@end
