//
//  PopoverViewController.h
//  AddressBook
//
//  Created by Quix Creations on 07/05/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GDIIndexBar.h"

@interface PickerPopoverViewController : UIViewController<GDIIndexBarDelegate>

@property (nonatomic, strong) NSArray *pickerArray;
@property (nonatomic, strong) NSString *oldValue;
@property (copy, nonatomic) void(^selectedIndex)(int selectedRow);
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong,nonatomic) NSString *showSearchAndIndexType;
@property (strong, nonatomic) GDIIndexBar *indexBar;

@property (weak, nonatomic) IBOutlet UITableView *pickerTableView;
@end
