 //
//  DropDownManager.m
//  AddressBook
//
//  Created by AIA on 01/06/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "DropDownManager.h"
#import "Utility.h"

@implementation DropDownManager

+ (DropDownManager *)sharedManager{
    static DropDownManager *sharedInstance;
    @synchronized(self){
        if (sharedInstance == nil){
            sharedInstance = [[DropDownManager alloc] init];
        }
    }
    return sharedInstance;
}

- (NSArray *)education{
    if(_education)
        return _education;
    
    NSDictionary *educationDict = [Utility loadJsonDataFromPath:@"education.json"];
    _education = [educationDict objectForKey:@"Education Codes & Description"];
    return _education;
}
- (NSArray *)educationLevel{
    if(_educationLevel)
        return _educationLevel;
    
    NSDictionary *educationLevelDict = [Utility loadJsonDataFromPath:@"educationLevel.json"];
    _educationLevel = [educationLevelDict objectForKey:@"EducationLevel Codes & Description"];
    return _educationLevel;
}

- (NSArray *)gender{
    if(_gender)
        return _gender;
    
    NSDictionary *genderDict = [Utility loadJsonDataFromPath:@"gender.json"];
    _gender = [genderDict objectForKey:@"Gender Codes & Description (T3582)"];
    return _gender;
}

- (NSArray *)race{
    if(_race)
        return _race;
    
    NSDictionary *raceDict = [Utility loadJsonDataFromPath:@"race.json"];
    _race = [raceDict objectForKey:@"Race Codes & Description (TY020)"];
    return _race;
}

- (NSArray *)maritalStatus{
    if(_maritalStatus)
        return _maritalStatus;
    
    NSDictionary *maritalStatusDict = [Utility loadJsonDataFromPath:@"marital_status.json"];
    _maritalStatus = [maritalStatusDict objectForKey:@"Marital Status Codes & Description (T3582)"];
    return _maritalStatus;
}

- (NSArray *)newcomerSource{
    if(_newcomerSource)
        return _newcomerSource;
    
    NSDictionary *newcomerSourceDict = [Utility loadJsonDataFromPath:@"newcomer_source.json"];
    _newcomerSource = [newcomerSourceDict objectForKey:@"Newcomer Source"];
    return _newcomerSource;
}

- (NSArray *)identityType{
    if(_identityType)
        return _identityType;
    
    NSDictionary *newcomerSourceDict = [Utility loadJsonDataFromPath:@"identity_type.json"];
    _identityType = [newcomerSourceDict objectForKey:@"Identity Type"];
    return _identityType;
}

- (NSArray *)natureOfBusiness{
    if(_natureOfBusiness)
        return _natureOfBusiness;
    
    NSDictionary *natureOfBusinessDict = [Utility loadJsonDataFromPath:@"nature_of_business.json"];
    _natureOfBusiness = [natureOfBusinessDict objectForKey:@"Nature Of Business Codes & Description (T3582)"];
    return _natureOfBusiness;
}

- (NSArray *)presentEmploymentCondition{
    if(_presentEmploymentCondition)
        return _presentEmploymentCondition;
    
    NSDictionary *presentEmploymentConditionDict = [Utility loadJsonDataFromPath:@"present_employment_condition.json"];
    _presentEmploymentCondition = [presentEmploymentConditionDict objectForKey:@"Present Employment Condition Codes & Description (T3582)"];
    return _presentEmploymentCondition;
}

- (NSArray *)recommendScheme{
    if(_recommendScheme)
        return _recommendScheme;
    
    NSDictionary *recommendSchemeDict = [Utility loadJsonDataFromPath:@"recommend_scheme.json"];
    _recommendScheme = [recommendSchemeDict objectForKey:@"Recommend Scheme Codes & Description (T3582)"];
    return _recommendScheme;
}

- (NSArray *)workExperience{
    if(_workExperience)
        return _workExperience;
    
    NSDictionary *workSchemeDict = [Utility loadJsonDataFromPath:@"work_experience.json"];
    _workExperience = [workSchemeDict objectForKey:@"Work Experience Codes & Description (T3582)"];
    return _workExperience;
}

- (NSArray *)sourceOfReferral{
    if(_sourceOfReferral)
        return _sourceOfReferral;
    
    NSDictionary *sourceOfReferralDict = [Utility loadJsonDataFromPath:@"source_of_referral.json"];
    _sourceOfReferral = [sourceOfReferralDict objectForKey:@"Source Of Referral Codes & Description (T3582)"];
    return _sourceOfReferral;
}

- (NSArray *)talentProfile{
    if(_talentProfile)
        return _talentProfile;
    
    NSDictionary *talentProfileDict = [Utility loadJsonDataFromPath:@"talent_profile.json"];
    _talentProfile = [talentProfileDict objectForKey:@"Talent Profile Codes & Description (T3582)"];
    return _talentProfile;
}

- (NSArray *)gamaContentChinese{
    if(_gamaContentChinese)
        return _gamaContentChinese;
    
    NSDictionary *gamaContentChineseDict = [Utility loadJsonDataFromPath:@"gama_content_cn.json"];
    _gamaContentChinese = [gamaContentChineseDict objectForKey:@"Talent Profile Codes & Description (T3582)"];
    return _gamaContentChinese;
}

- (NSArray *)gamaContentEnglish{
    if(_gamaContentEnglish)
        return _gamaContentEnglish;
    
    NSDictionary *gamaContentEnglishDict = [Utility loadJsonDataFromPath:@"gama_content_en.json"];
    _gamaContentEnglish = [gamaContentEnglishDict objectForKey:@"Talent Profile Codes & Description (T3582)"];
    return _gamaContentEnglish;
}

- (NSArray *)csvData{
    if(_csvData)
        return _csvData;
    
    _csvData = [Utility loadJsonDataFromPath:@"address.json"];
//    _csvData = [csvDict objectForKey:@"Talent Profile Codes & Description (T3582)"];
    return _csvData;
}

- (NSString *)getGender:(NSString *)code
            andBySource:(NSString *)source{
    NSArray *filteredArray = [self.gender filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Code=%@ AND SourceSystem=%@",code,source]];
    if(filteredArray.count>0)
        return [[filteredArray objectAtIndex:0] objectForKey:@"Description"];
    return nil;
}

- (NSArray *)getDropDownDescArray:(NSString *)dropdown{
    return [[self getDropDownArray:dropdown] valueForKey:@"Description"];
}

- (NSArray *)getDropDownArray:(NSString *)dropdown{
    if([dropdown isEqualToString:DROP_DOWN_MARITAL_STATUS])
        return self.maritalStatus;
    else if([dropdown isEqualToString:DROP_DOWN_NATURE_OF_BUSINESS])
        return self.natureOfBusiness;
    else if([dropdown isEqualToString:DROP_DOWN_PRESENT_EMPLOYMENT_CONDITION])
        return self.presentEmploymentCondition;
    else if([dropdown isEqualToString:DROP_DOWN_RECOMMEND_SCHEME])
        return self.recommendScheme;
    else if([dropdown isEqualToString:DROP_DOWN_SOURCE_OF_REFERRAL])
        return self.sourceOfReferral;
    else if([dropdown isEqualToString:DROP_DOWN_TALENT_PROFILE])
        return self.talentProfile;
    else if([dropdown isEqualToString:DROP_DOWN_EDUCATION])
        return self.education;
    else if([dropdown isEqualToString:DROP_DOWN_GENDER])
        return self.gender;
    else if([dropdown isEqualToString:DROP_DOWN_RACE])
        return self.race;
    else if([dropdown isEqualToString:DROP_DOWN_WORK_EXPERIENCE])
        return self.workExperience;
    else if([dropdown isEqualToString:DROP_DOWN_NEWCOMER_SOURCE])
        return self.newcomerSource;
    else if([dropdown isEqualToString:DROP_DOWN_IDENTITY_TYPE])
        return self.identityType;
    else if([dropdown isEqualToString:DROP_DOWN_ADDRESS])
        return self.csvData;
    else
        return nil;
}

- (NSString *)getDropdownDesc:(NSString *)dropdown
                         code:(NSString *)code{
    
    NSArray *genderArray = [self getDropDownArray:dropdown];
    NSArray *array = [genderArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Code=%@",[code stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]];
    if(array.count > 0)
        return [[array objectAtIndex:0] valueForKey:@"Description"];
    else
        return @"";
}

- (NSString *)getDropdownCode:(NSString *)dropdown
                  description:(NSString *)description{
    
    NSArray *array = [[self getDropDownArray:dropdown] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Description=%@",description]];
    if(array.count > 0)
        return [[array objectAtIndex:0] valueForKey:@"Code"];
    else
        return @"";
}

@end
