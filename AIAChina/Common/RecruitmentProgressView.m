//
//  RecruitmentProgressView.m
//  AIAChina
//
//  Created by MacMini on 24/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "RecruitmentProgressView.h"

@implementation RecruitmentProgressView

+ (id)loadRecruitmentProgressView{
    RecruitmentProgressView *recruitmentProgress = [[[NSBundle mainBundle] loadNibNamed:@"RecruitmentProgressView" owner:self options:nil] lastObject];
    if ([recruitmentProgress isKindOfClass:[RecruitmentProgressView class]]) {
        return recruitmentProgress;
    }
    return nil;
}

- (void)setGui{
    for (UIImageView *progressImage in self.imgProgress) {
        [Utility setRoundCornerRedius:progressImage
                         cornerRedius:progressImage.frame.size.width/2];
    }
}

- (void)setSelectedProgress:(TblContact *)contact{
    [self setGui];
    UIColor *greenColor = [UIColor colorWithRed:137.0/255.0
                                          green:204.0/255.0
                                           blue:62.0/255.0
                                          alpha:1.0];
    
    UIColor *redColor = [UIColor redColor];

    
    TblCandidateProcess *candidateProcess = contact.candidateProcess;
    if([candidateProcess.interactivePresentStatus isEqualToString:RECURITMENT_STATUS_TRUE])
        [self.imgProgress[0] setBackgroundColor:greenColor];
    
    if([candidateProcess.eopRegistrationStatus isEqualToString:RECURITMENT_STATUS_TRUE])
        [self.imgProgress[1] setBackgroundColor:greenColor];

    if(!isEmptyString(candidateProcess.firstInterviewStatus)){
        if([candidateProcess.firstInterviewStatus isEqualToString:RECURITMENT_STATUS_TRUE])
            [self.imgProgress[2] setBackgroundColor:greenColor];
        else
            [self.imgProgress[2] setBackgroundColor:redColor];
    }
    
    if([candidateProcess.attendEopStatus isEqualToString:RECURITMENT_STATUS_TRUE])
        [self.imgProgress[3] setBackgroundColor:greenColor];
    
    if(!isEmptyString(candidateProcess.ccTestStatus)){
        if([candidateProcess.ccTestStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
            [self.imgProgress[4] setBackgroundColor:greenColor];
        }else{
            [self.imgProgress[4] setBackgroundColor:redColor];
        }
    }
    
    if([candidateProcess.companyInterviewStatus isEqualToString:RECURITMENT_STATUS_TRUE])
        [self.imgProgress[5] setBackgroundColor:greenColor];
    
    if(!isEmptyString(candidateProcess.abcTrainingStatus)){
        if([candidateProcess.abcTrainingStatus isEqualToString:RECURITMENT_STATUS_TRUE])
            [self.imgProgress[7] setBackgroundColor:greenColor];
        else
            [self.imgProgress[7] setBackgroundColor:redColor];
    }

    if([candidateProcess.contractStatus isEqualToString:RECURITMENT_STATUS_TRUE]) {
        [self.imgProgress[6] setBackgroundColor:greenColor];
        [self.imgProgress[8] setBackgroundColor:greenColor];
    }
}

@end
