//
//  NVBnbCollectionView.m
//  NVBnbCollectionView
//
//  Created by Nguyen Vinh on 8/8/15.
//
//

#import "NVBnbCollectionView.h"

@implementation NVBnbCollectionView {
    __weak id<NVBnbCollectionViewDataSource> _bnbDataSource;
    CADisplayLink *_displayLink;
    UIInterfaceOrientation _currentOrientation;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setUp];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setUp];
    }
    
    return self;
}

- (void)setUp {
    self.enableLoadMore = true;
    _currentOrientation = UIInterfaceOrientationLandscapeLeft;
    ((NVBnbCollectionViewLayout *) self.collectionViewLayout).currentOrientation = _currentOrientation;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    [self addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    [self setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:240.0/255.0 blue:230.0/255.0 alpha:1.0]];
    self.backImgView = [[UIImageView alloc]init];
    int deduct = 100;
    [self.backImgView setFrame:CGRectMake(300 + deduct, (self.frame.size.height - deduct) /2 - deduct/2 , 440, 190)];
    [self.backImgView setImage:[UIImage imageNamed:@"aia-logo-background"]];
    [self addSubview:self.backImgView];
}

- (void)didChangeValueForKey:(NSString *)key {
    if ([key isEqualToString:@"contentOffset"] && !CGPointEqualToPoint(self.contentOffset, CGPointZero)) {
        if ((UIInterfaceOrientationIsPortrait(_currentOrientation) && self.contentOffset.y > (self.contentSize.height - self.frame.size.height))
            || (UIInterfaceOrientationIsLandscape(_currentOrientation) && self.contentOffset.x > (self.contentSize.width - self.frame.size.width))) {
           // NSLog(@"contentOffset changed: %@", NSStringFromCGPoint(self.contentOffset));
        }
    }
}

- (void)dealloc {
    [_displayLink invalidate];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeObserver:self forKeyPath:@"contentOffset"];
}

#pragma mark - Accessors

- (void)setDataSource:(id<UICollectionViewDataSource>)dataSource {
    [super setDataSource:self];
    
    _bnbDataSource = (id<NVBnbCollectionViewDataSource>) dataSource;
}

- (void)setEnableLoadMore:(BOOL)enableLoadMore {
    _enableLoadMore = enableLoadMore;
    [self.collectionViewLayout invalidateLayout];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([_bnbDataSource respondsToSelector:@selector(numberOfItemsInBnbCollectionView:)]) {
        NSInteger count = [_bnbDataSource numberOfItemsInBnbCollectionView:self];
        if (count > 0)
            [self.backImgView setHidden:YES];
        else
            [self.backImgView setHidden:NO];
            
        return count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (![_bnbDataSource respondsToSelector:@selector(bnbCollectionView:cellForItemAtIndexPath:)]) {
        return nil;
    }
    
    return [_bnbDataSource bnbCollectionView:self cellForItemAtIndexPath:indexPath];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:NVBnbCollectionElementKindHeader]) {
        UICollectionReusableView *header = nil;
        
        if ([_bnbDataSource respondsToSelector:@selector(bnbCollectionView:headerAtIndexPath:)]) {
            header = [_bnbDataSource bnbCollectionView:self headerAtIndexPath:indexPath];
        }
        
        return header;
    }
    
    return nil;
}

#pragma mark - Orientation

- (void)orientationChanged:(NSNotification *)notification {
    _currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
 
    
    NSLog(@"current orientation %d",_currentOrientation);
    if (_currentOrientation == 4 || _currentOrientation ==3) {
//        ((NVBnbCollectionViewLayout *) self.collectionViewLayout).currentOrientation = _currentOrientation;
        [self.collectionViewLayout invalidateLayout];
    }
    else {
        [self.collectionViewLayout invalidateLayout];

    }
  
    NSLog(@"orientationChanged");
}

@end
