//
//  FFMonthHeaderView.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 3/18/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFMonthHeaderView.h"

#import "FFImportantFilesForCalendar.h"

@implementation FFMonthHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
//        NSArray *arrayColor = @[[UIColor grayColor], [UIColor blackColor], [UIColor blackColor], [UIColor blackColor], [UIColor blackColor], [UIColor blackColor], [UIColor grayColor]];
        
        CGFloat width = (self.frame.size.width-6*SPACE_COLLECTIONVIEW_CELL)/7.;
        for (int i = 0; i < [arrayWeekAbrev count]; i++) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(i*(width+SPACE_COLLECTIONVIEW_CELL), 0., width-10., self.frame.size.height)];
            [label setTextAlignment:NSTextAlignmentRight];
            [label setText:NSLocalizedString([arrayWeekAbrev objectAtIndex:i], @"")];
            [label setTextColor:[UIColor colorWithRed:76.0/255.0 green:76.0/255.0 blue:76.0/255.0 alpha:1.0]];
            [label setFont:[UIFont fontWithName:@"HelveticaNeue" size:label.font.pointSize]];
            [label setAutoresizingMask:AR_LEFT_RIGHT | UIViewAutoresizingFlexibleWidth];
            [self addSubview:label];
            
            NSDateComponents *components = [NSDate componentsOfDate:[NSDate date]];
            if ((long)components.weekday == i+1) {
                UILabel *lblBottom = [[UILabel alloc] initWithFrame:CGRectMake(i*(width+SPACE_COLLECTIONVIEW_CELL), self.frame.size.height - 2, width, 2)];
                lblBottom.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:33.0/255.0 blue:8.0/255.0 alpha:1.0];
                [lblBottom setAutoresizingMask:AR_LEFT_RIGHT | UIViewAutoresizingFlexibleWidth];
                [self addSubview:lblBottom];
            }
        }
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
