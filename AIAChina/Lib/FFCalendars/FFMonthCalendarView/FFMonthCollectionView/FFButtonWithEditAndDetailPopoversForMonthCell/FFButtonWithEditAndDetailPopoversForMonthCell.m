//
//  FFButtonWithEditAndDetailPopoversForMonthCell.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/15/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFButtonWithEditAndDetailPopoversForMonthCell.h"

#import "FFEventDetailPopoverController.h"
#import "FFEditEventPopoverController.h"
#import "CreateEventViewController.h"
#import "ActivityType.h"
#import "EOPDetailViewController.h"
#import "InterviewDetailViewController.h"
#import "BirthDayViewController.h"
#import "TrainingDetailViewController.h"
#import "ICalendarDetailViewController.h"

@interface FFButtonWithEditAndDetailPopoversForMonthCell () <FFEventDetailPopoverControllerProtocol, FFEditEventPopoverControllerProtocol>
@property (nonatomic, strong) FFEventDetailPopoverController *popoverControllerDetails;
@property (nonatomic, strong) FFEditEventPopoverController *popoverControllerEditar;
@property (nonatomic,retain) UIPopoverController *popoverController;
@end

@implementation FFButtonWithEditAndDetailPopoversForMonthCell

#pragma mark - Synthesize

@synthesize protocol;
@synthesize event;
@synthesize popoverControllerDetails;
@synthesize popoverControllerEditar;
@synthesize popoverController;

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
        [self addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)setEvent:(FFEvent *)_event {
    event = _event;
}

#pragma mark - Button Action

- (IBAction)buttonAction:(id)sender {
    
    if (event) {
        if([event.activityType isEqualToString:ACTIVITY_HOLIDAY]){
            
        }else if([event.activityType isEqualToString:ACTIVITY_NAP_TRANING]){
            [self showTraining:event];
        }else if([event.activityType isEqualToString:ACTIVITY_BIRTHDAY]){
            [self showBirthDay:event];
        }else if([event.activityType isEqualToString:ACTIVITY_ALE]){
            
        }else if([event.activityType isEqualToString:ACTIVITY_EOP]){
            [self showEOPDetail:event];
        }else if([event.activityType isEqualToString:ACTIVITY_INT]){
            [self showInterviewDetailEvent:event];
        }else if([event.activityType isEqualToString:ACTIVITY_ICALENDAR]){
            //need to change
            [self showICalendarDetailEvent:event];
        }else{
            [self showPopoverEditWithEvent:event];
        }
    }
}

#pragma mark - FFEventDetailPopoverController Protocol

- (void)showTraining:(FFEvent *)_event{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    TrainingDetailViewController *trainingDetailViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"TrainingDetailViewController"];
    trainingDetailViewCtrl.event = _event;
    popoverController = [[UIPopoverController alloc] initWithContentViewController:trainingDetailViewCtrl];
    [trainingDetailViewCtrl setDoneTrainingDetail:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController.backgroundColor = trainingDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:self.frame
                                       inView:[super superview]
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showBirthDay:(FFEvent *)_event{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    BirthDayViewController *birthDayViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"BirthDayViewController"];
    birthDayViewCtrl.birthDate = _event.dateDay;
    [birthDayViewCtrl setCancelBirthDayView:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    [birthDayViewCtrl setSendEGreeting:^(NSMutableArray *emailArray) {
        [popoverController dismissPopoverAnimated:TRUE];
        [ApplicationFunction showGrettingView:emailArray];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:birthDayViewCtrl];
    popoverController.backgroundColor = birthDayViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:self.frame
                                       inView:[super superview]
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showEOPDetail:(FFEvent *)_event{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navConroller = [storyboard instantiateViewControllerWithIdentifier:@"EOPDetailNavController"];
    EOPDetailViewController *eopDetailViewCtrl = navConroller.viewControllers[0];
    eopDetailViewCtrl.event = _event;
    [eopDetailViewCtrl setDismissPopOverBlock:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:navConroller];
    popoverController.backgroundColor = eopDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:self.frame
                                       inView:[super superview]
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showInterviewDetailEvent:(FFEvent *)_event{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navConroller = [storyboard instantiateViewControllerWithIdentifier:@"InterviewDetailNavController"];
    InterviewDetailViewController *interviewDetailViewCtrl = navConroller.viewControllers[0];
    interviewDetailViewCtrl.event = _event;
    [interviewDetailViewCtrl setDismissPopOverBlock:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:navConroller];
    popoverController.backgroundColor = interviewDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:self.frame
                                       inView:[super superview]
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
    
}

- (void)showICalendarDetailEvent:(FFEvent *)_event{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navConroller = [storyboard instantiateViewControllerWithIdentifier:@"iCalendarDetailNavController"];
    ICalendarDetailViewController *iCalendarDetailViewCtrl = navConroller.viewControllers[0];
    iCalendarDetailViewCtrl.event = _event;
    [iCalendarDetailViewCtrl setDismissPopOverBlock:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:navConroller];
    //    popoverController.backgroundColor = iCalendarDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:self.frame
                                       inView:[super superview]
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showPopoverEditWithEvent:(FFEvent *)_event {
    
    CreateEventViewController *createEventPopoverView = [[CreateEventViewController alloc] initWithNibName:@"CreateEventViewController" bundle:nil];
    createEventPopoverView.event = _event;
    [createEventPopoverView setEditEvent:^(FFEvent *editEvent, NSDate *sdate, NSDate *eDate,MBProgressHUDUpd *hud) {
        [self deleteEvent:sdate oldEndDate:eDate];
        [self saveEditedEvent:editEvent];
        [hud hide:TRUE];
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    
    [createEventPopoverView setDeleteEvent:^(FFEvent *deleteEvent, NSDate *sdate, NSDate *eDate) {
        [self deleteEvent:sdate oldEndDate:eDate];
        [ApplicationFunction deleteCalManualEvent:deleteEvent];
                [popoverController dismissPopoverAnimated:TRUE];
    }];
    
    [createEventPopoverView setCancelEvent:^(UIView *view) {
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    
    popoverController = [[UIPopoverController alloc] initWithContentViewController:createEventPopoverView];
    popoverController.popoverContentSize = createEventPopoverView.view.frame.size;
    popoverController.backgroundColor = createEventPopoverView.view.backgroundColor;
    [popoverController presentPopoverFromRect:self.frame
                                       inView:[super superview]
                     permittedArrowDirections:UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight
                                     animated:YES];
    
}

#pragma mark - FFEditEventPopoverController Protocol

- (void)saveEditedEvent:(FFEvent *)eventNew {
    
    if (protocol != nil && [protocol respondsToSelector:@selector(saveEditedEvent:ofButton:)]) {
        [protocol saveEditedEvent:eventNew ofButton:self];
    }
}

- (void)deleteEvent: (NSDate *)oldStartDate oldEndDate:(NSDate *)oldEndDate  {
    if (protocol != nil && [protocol respondsToSelector:@selector(deleteEventOfButton:oldStartDate:oldEndDate:)]) {
        [protocol deleteEventOfButton:self oldStartDate:oldStartDate oldEndDate:oldEndDate];
    }
}

- (void)deleteEvent{
    
}


@end
