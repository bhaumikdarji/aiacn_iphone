//
//  FFMonthCell.m
//  FFCalendar
//
//  Created by Felipe Rocha on 14/02/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFMonthCell.h"
#import "UIEventButton.h"
#import "FFButtonWithEditAndDetailPopoversForMonthCell.h"
#import "FFImportantFilesForCalendar.h"
#import "ActivityType.h"
#import "PickerPopoverViewController.h"


@interface FFMonthCell () <FFButtonWithEditAndDetailPopoversForMonthCellProtocol>
@property (nonatomic, strong) NSMutableArray *arrayButtons;
@property (nonatomic, strong) UIPopoverController *popOVer;

@end

@implementation FFMonthCell

#pragma mark - Synthesize

@synthesize protocol;
@synthesize arrayButtons;
@synthesize arrayEvents;
@synthesize labelDay;
@synthesize imageViewCircle;
@synthesize imageViewBoder;

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)initLayout {
    
    if (!imageViewCircle) {
        imageViewCircle = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-32.-3., 3., 32., 32.)];
        [imageViewCircle setAutoresizingMask:AR_LEFT_BOTTOM];
        [self addSubview:imageViewCircle];
        
        labelDay = [[UILabel alloc] initWithFrame:CGRectMake((imageViewCircle.frame.size.width-20.)/2., (imageViewCircle.frame.size.height-20.)/2., 20., 20.)];
        [labelDay setAutoresizingMask:AR_LEFT_BOTTOM];
        [labelDay setTextAlignment:NSTextAlignmentCenter];
        [imageViewCircle addSubview:labelDay];
        
        imageViewBoder = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [imageViewBoder setAutoresizingMask:AR_WIDTH_HEIGHT];
        [self addSubview:imageViewBoder];
    }
    
    [self setBackgroundColor:[UIColor whiteColor]];
    [self.labelDay setText:@""];
    [self.labelDay setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
    [self.labelDay setTextColor:[UIColor blackColor]];
    [self.imageViewCircle setImage:nil];
    
    self.imageViewBoder.layer.borderColor = [UIColor clearColor].CGColor;
    self.imageViewBoder.layer.borderWidth = 0.0;
    
    for (UIButton *button in arrayButtons) {
        [button removeFromSuperview];
    }
}

#pragma mark - Custom Layouts

- (void)markAsWeekend {
    
    [self setBackgroundColor:[UIColor colorWithRed:252.0/255.0 green:252.0/255.0 blue:252.0/255.0 alpha:1.0]];
    [self.labelDay setTextColor:[UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0]];
}

- (void)markAsCurrentDay {
    imageViewBoder.layer.borderColor = [UIColor colorWithRed:206.0/255.0 green:33.0/255.0 blue:8.0/255.0 alpha:1.0].CGColor;
    imageViewBoder.layer.borderWidth = 2.0;
    
//    [self.labelDay setTextColor:[UIColor greenColor]];
//    [self.imageViewCircle setImage:[UIImage imageNamed:@"redCircle"]];
}

#pragma mark - Showing Events

- (void)setArrayEvents:(NSMutableArray *)_array {
    
    if(isEmptyString([ActivityType sharedActivity].selectedMode))
        arrayEvents = _array;
    else
        arrayEvents = [[_array filteredArrayUsingPredicate:[[ActivityType sharedActivity] setSelectedModePredicate]] mutableCopy];
    
    arrayButtons = [NSMutableArray new];
    
    if ([arrayEvents count] > 0) {
        
        int maxNumOfButtons = 4;
        CGFloat yFirstButton = imageViewCircle.frame.origin.y+imageViewCircle.frame.size.height;
        CGFloat height = (self.frame.size.height-yFirstButton)/maxNumOfButtons;
        
        int buttonOfNumber = 0;
        for (int i = 0; i < [arrayEvents count] ; i++) {
            
            buttonOfNumber++;
            FFButtonWithEditAndDetailPopoversForMonthCell *button = [[FFButtonWithEditAndDetailPopoversForMonthCell alloc] initWithFrame:CGRectMake(0, yFirstButton+(buttonOfNumber-1)*height, self.frame.size.width, height)];
            [button setAutoresizingMask:AR_TOP_BOTTOM | UIViewAutoresizingFlexibleWidth];
            [self addSubview:button];
            [arrayButtons addObject:button];
            
            if ((buttonOfNumber == maxNumOfButtons) && ([arrayEvents count] - maxNumOfButtons > 0)) {
                UIEventButton *btnMore = [UIEventButton buttonWithType:UIButtonTypeCustom];
                [btnMore setFrame:button.frame];
                [btnMore addTarget:self action:@selector(moreButton:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:btnMore];
                //                [button setTitle:[NSString stringWithFormat:@"%lu more...", (long)[arrayEvents count] - maxNumOfButtons] forState:UIControlStateNormal];
                [button setTitle:@"+" forState:UIControlStateNormal];
                [button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]];
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                
                NSMutableArray *arr = [NSMutableArray new];
                NSMutableArray *arrMore = [NSMutableArray new];
                for (int i = buttonOfNumber; i < [arrayEvents count]; i++) {
                    FFEvent *event = [arrayEvents objectAtIndex:i];
                    [arr addObject:event];
                    if ([event.eventName length] > 0) {
                        [arrMore addObject:event.eventName];
                    } else {
                        [arrMore addObject:@""];
                    }
                }
                NSDictionary *temp = [[NSDictionary alloc]initWithObjectsAndKeys:button,@"Sender",arr,@"EventsData",arrMore,@"EventsTitle", nil];
                [btnMore setEventData:temp];
                break;
            } else {
                FFEvent *event = [arrayEvents objectAtIndex:i];
                if([event.activityType isEqualToString:ACTIVITY_INT]){
                    [button setTitle:NSLocalizedString(event.eventName,@"") forState:UIControlStateNormal];
                }else if([event.activityType isEqualToString:ACTIVITY_ICALENDAR]){
                    NSString *str = @"iCalendar";
                    //Made change for count
                    NSString *stragentcode = [NSString stringWithFormat:@"%@",[CachingService sharedInstance].currentUserIdWithZero];
                    NSString *strdate = [NSString stringWithFormat:@"%@",event.startDate];
                    NSString *newString = [strdate substringToIndex:10];
                    NSArray *eventArr = [[NSArray alloc]init];
                    eventArr = [DbUtils fetchAllObject:@"ICalendarEvents"
                                            andPredict:[NSPredicate predicateWithFormat:@"startTime contains[cd] %@ AND agentCode == %@",newString,stragentcode]
                                     andSortDescriptor:nil
                                  managedObjectContext:[CachingService sharedInstance].managedObjectContext];
                    if (eventArr.count == 0) {
                        return;//avoid to show 0 iCalender as a count
                    }

                    [button setTitle:[NSString stringWithFormat:@"%d %@",eventArr.count, NSLocalizedString(str, @"")] forState:UIControlStateNormal];
                } else {
                    [button setTitle:event.eventName forState:UIControlStateNormal];
                }
                [button setEvent:event];
                [button setProtocol:self];
                button.backgroundColor = [[ActivityType sharedActivity] setBgColorOnActivityType:event.activityType];
                [button setTitleColor:[[ActivityType sharedActivity] setTextColorOnActivityType:event.activityType] forState:UIControlStateNormal];
            }
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)];
        }
    }
}
-(void)moreButton:(id)sender{
    NSDictionary *temp = [sender eventData];
    NSArray *arrTitles = [NSArray arrayWithArray:[temp valueForKey:@"EventsTitle"]];
    NSArray *arrEvents = [NSArray arrayWithArray:[temp valueForKey:@"EventsData"]];
    
    [self setPopoverView:[temp valueForKey:@"Sender"] pickerArray:arrTitles withOriginalEventsArray:arrEvents];
}

- (void)setPopoverView:(FFButtonWithEditAndDetailPopoversForMonthCell *)btn pickerArray:(NSArray *)pickerArray withOriginalEventsArray:(NSArray *)arrOriginal{
    NSMutableArray *pickerDataArray = [[NSMutableArray alloc] init];
    [pickerDataArray addObjectsFromArray:pickerArray];
    
    PickerPopoverViewController *popoverViewCtrl = [[PickerPopoverViewController alloc] initWithNibName:@"PickerPopoverViewController" bundle:nil];
    
    if(isEmptyString(btn.titleLabel.text))
        [btn setTitle:pickerDataArray[0] forState:UIControlStateNormal];
    else
        popoverViewCtrl.oldValue = btn.titleLabel.text;
    
    [popoverViewCtrl setPickerArray:pickerDataArray];
    
    __weak typeof(self) weakSelf = self;
    [popoverViewCtrl setSelectedIndex:^(int selectedRowIndexPath) {
        int selectedIndex = selectedRowIndexPath;
        [weakSelf.popOVer dismissPopoverAnimated:YES];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [btn setEvent:arrOriginal[selectedIndex]];
                [btn buttonAction:btn];
            });
        });
    }];
    
    self.popOVer = [[UIPopoverController alloc] initWithContentViewController:popoverViewCtrl];
    self.popOVer.popoverContentSize = CGSizeMake(200, 200);
    
    [self.popOVer presentPopoverFromRect:btn.frame
                                  inView:self
     
                permittedArrowDirections:UIPopoverArrowDirectionUnknown
                                animated:TRUE];
}

#pragma mark - FFButtonWithEditAndDetailPopoversForMonthCell Protocol

- (void)saveEditedEvent:(FFEvent *)eventNew ofButton:(UIButton *)button {
    
    long i = [arrayButtons indexOfObject:button];
    
    if (protocol != nil && [protocol respondsToSelector:@selector(saveEditedEvent:ofCell:atIndex:)]) {
        [protocol saveEditedEvent:eventNew ofCell:self atIndex:i];
    }
}

- (void)deleteEventOfButton:(UIButton *)button oldStartDate:(NSDate *)oldStartDate oldEndDate:(NSDate *)oldEndDate{
    
    long i = [arrayButtons indexOfObject:button];
    if([oldStartDate isEqualToDateIgnoringTime:oldEndDate]){
        if (protocol != nil && [protocol respondsToSelector:@selector(deleteEventOfCell:atIndex:)]) {
            [protocol deleteEventOfCell:self atIndex:i];
        }
    }else{
        if(protocol != nil && [protocol respondsToSelector:@selector(deleteEventOfCell:atIndex:oldStartDate:oldEndDate:)]){
            [protocol deleteEventOfCell:self atIndex:i oldStartDate:oldStartDate oldEndDate:oldEndDate];
        }
    }
}

@end
