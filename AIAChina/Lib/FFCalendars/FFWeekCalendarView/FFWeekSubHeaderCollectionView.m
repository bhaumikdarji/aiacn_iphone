//
//  FFWeekSubHeaderCollectionView.m
//  AddressBook
//
//  Created by smitesh on 2015-07-01.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "FFWeekSubHeaderCollectionView.h"
#import "FFWeekSubHeaderCell.h"
#import "FFWeekCollectionAllDayFlowLayout.h"
#import "FFImportantFilesForCalendar.h"
#import "DbUtils.h"
#import "Constants.h"
#import "ActivityType.h"
#import "FFEvent.h"
#import "Utils.h"
#import "TblContact.h"
#import "Utility.h"
#import "CachingService.h"

@interface FFWeekSubHeaderCollectionView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (nonatomic) CGFloat lastContentOffset;
@property (nonatomic) BOOL boolGoPrevious;
@property (nonatomic) BOOL boolGoNext;
@end
@implementation FFWeekSubHeaderCollectionView
@synthesize lastContentOffset;
@synthesize protocol;
@synthesize boolGoPrevious;
@synthesize boolGoNext;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout {
    
    self = [super initWithFrame:frame collectionViewLayout:[FFWeekCollectionAllDayFlowLayout new]];
    
    if (self) {
        // Initialization code
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(todayClick:)
                                                     name:DATE_MANAGER_TODAY_DATE
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dateManagerMonthDateTap:)
                                                     name:DATE_MANAGER_MONTH_DATE_TAP
                                                   object:nil];
        [self setDataSource:self];
        [self setDelegate:self];
        
        [self setBackgroundColor:[UIColor whiteColor]];
        self.dayEventArray = [[NSMutableArray alloc]init];
        [self registerClass:[FFWeekSubHeaderCell class] forCellWithReuseIdentifier:REUSE_IDENTIFIER_MONTH_CELL];
        
        [self setScrollEnabled:NO];
        [self setPagingEnabled:YES];
        [self setScrollsToTop:YES];
        boolGoNext = NO;
        boolGoPrevious = NO;
        
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    }
    return self;
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
- (void)dateManagerMonthDateTap:(NSNotification *)notification{
    self.filterDate = [notification object];
    //[self loadEvents];
}
- (void)todayClick:(NSNotification *)noti{
    self.selectedDate = noti.object;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    long intDay = 7*([[[FFDateManager sharedManager] currentDate] numberOfWeekInMonthCount]+2);
    return intDay;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDateComponents *comp = [NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]];
    
    NSDate *dateFirstDayOfMonth = [NSDate dateWithYear:comp.year month:comp.month day:1];
    NSDateComponents *componentsFirstDayOfMonth = [NSDate componentsOfDate:dateFirstDayOfMonth];
    NSDate *dateOfLabel = [NSDate dateWithYear:comp.year month:comp.month day:1+indexPath.row-(componentsFirstDayOfMonth.weekday-1)-7];
    NSDateComponents *compDateOfLabel = [NSDate componentsOfDate:dateOfLabel];
    self.filterDate = dateOfLabel;
    FFWeekSubHeaderCell *cell = (FFWeekSubHeaderCell *)[collectionView dequeueReusableCellWithReuseIdentifier:REUSE_IDENTIFIER_MONTH_CELL forIndexPath:indexPath];
    
    [self.dayEventArray removeAllObjects];
//    NSPredicate *predic = [NSPredicate predicateWithFormat:@"!(activityType IN %@) AND (sDate=%@ || (%@>=startDate AND %@<=endDate))",[[ActivityType sharedActivity] getActivityModeToType:ACTIVITY_OPPORTUNITY],self.filterDate,self.filterDate,self.filterDate];
//    NSArray *appointmentActivities = [DbUtils fetchAllObject:@"TblCalendarEvent"
//                                                  andPredict:predic
//                                           andSortDescriptor:nil
//                                        managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
/*    for (TblCalendarEvent *calendarEvent in appointmentActivities)
    {
        
        NSLog(@"entered:%@",self.filterDate);
        [self.dayEventArray addObject:@{@"TIME":[calendarEvent.allDay boolValue]?@"All-Day":[Utility dateToString:calendarEvent.startDate andFormate:@"hh a"],
                                        @"TITLE":calendarEvent.eventName,
                                        @"BTN_TITLE":@"View Appointment",
                                        @"BTN_ACTION":[NSNumber numberWithInteger:VIEW_APPOINTMENT_ACTION],
                                        @"OBJECTID":calendarEvent.objectID,
                                        @"DATE":self.filterDate
                                        }];
    }
 */
   // [cell cleanSubHeaderCell];
    cell.arrayOfAllDayEvents = self.dayEventArray;
    [cell.tableView reloadData];
    return cell;
}



- (int)xPositionOfImage:(int)weekDay{
    if(weekDay == 1)
        return -17;
    else if(weekDay == 2 || weekDay == 4)
        return -14;
    else if(weekDay == 3 || weekDay == 7)
        return -22;
    else if(weekDay == 5)
        return -19;
    else
        return -28;
}

- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

#pragma mark - UICollectionView Delegate FlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((self.frame.size.width)/7, self.frame.size.height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
- (void)loadEvents
{
    }



#pragma mark - UIScrollView Delegate

/*- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (protocol != nil && [protocol respondsToSelector:@selector(subheaderDidScroll)]) {
        [protocol subheaderDidScroll];
    }
    
    if (!boolGoPrevious && scrollView.contentOffset.x < 0) {
        boolGoPrevious = YES;
    }
    
    if (!boolGoNext && scrollView.contentOffset.x >  ([[[FFDateManager sharedManager] currentDate] numberOfWeekInMonthCount]-1)*scrollView.frame.size.width) {
        boolGoNext = YES;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    lastContentOffset = scrollView.contentOffset.x;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    NSDateComponents *comp = [NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]];
    
    ScrollDirection scrollDirection;
    if (lastContentOffset > scrollView.contentOffset.x || boolGoPrevious) {
        scrollDirection = ScrollDirectionRight;
        comp.day -= 7;
    } else if (lastContentOffset < scrollView.contentOffset.x || boolGoNext) {
        scrollDirection = ScrollDirectionLeft;
        comp.day += 7;
    } else {
        scrollDirection = ScrollDirectionNone;
    }
    
    [[FFDateManager sharedManager] setCurrentDate:[NSDate dateWithYear:comp.year month:comp.month day:comp.day]];
    
    boolGoPrevious = NO;
    boolGoNext = NO;
}*/
@end
