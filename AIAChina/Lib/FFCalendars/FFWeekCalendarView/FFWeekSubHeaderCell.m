//
//  FFWeekSubHeaderCell.m
//  AddressBook
//
//  Created by smitesh on 2015-07-02.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "FFWeekSubHeaderCell.h"

@implementation FFWeekSubHeaderCell
@synthesize tableView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.arrayOfAllDayEvents = [[NSMutableArray alloc]init];
        tableView = [[UITableView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height) style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.separatorColor = [UIColor clearColor];
        [tableView setShowsHorizontalScrollIndicator:NO];
        [tableView setShowsVerticalScrollIndicator:NO];
        
        [self addSubview:tableView];
        
        
    }
    return self;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.arrayOfAllDayEvents count] >1) {
        self.arrayOfAllDayEvents1 = self.arrayOfAllDayEvents;
    }
    return [self.arrayOfAllDayEvents count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UILabel *myLabel;
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        // create a new label with the size we need
        myLabel = [[UILabel alloc] init];
        myLabel.frame = CGRectMake(10, 10, 200, 25);
        // set the label's tag value
        myLabel.tag = indexPath.row;
        myLabel.text = @"Event";
        // add the new label to the content view
        [cell.contentView addSubview:myLabel];
        
        
    }
    return cell;
}
- (void)cleanSubHeaderCell
{
    [self.arrayOfAllDayEvents removeAllObjects];
     
}

@end
