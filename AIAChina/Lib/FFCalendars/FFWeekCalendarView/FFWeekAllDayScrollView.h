//
//  FFWeekAllDayScrollView.h
//  AddressBook
//
//  Created by smitesh on 2015-07-05.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFWeekAllDayCollectionView.h"
@interface FFWeekAllDayScrollView : UIScrollView
@property (nonatomic, strong) NSMutableDictionary *dictEvents;
@property (nonatomic, strong) FFWeekAllDayCollectionView *collectionViewWeek;
@property (nonatomic, strong) UILabel *labelWithActualHour;
@property (nonatomic, strong) UILabel *labelGrayWithActualHour;
- (void)updateContentSize:(int)number;
@end
