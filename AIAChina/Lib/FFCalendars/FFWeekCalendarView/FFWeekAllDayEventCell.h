//
//  FFWeekAllDayEventCell.h
//  AddressBook
//
//  Created by smitesh on 2015-07-05.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "FFEvent.h"

@protocol FFAllDayWeekCellProtocol <NSObject>
@required
- (void)saveEditedEvent:(FFEvent *)eventNew;
- (void)deleteEventOfCell:(FFEvent *)eventNew;
@end
@interface FFWeekAllDayEventCell : UICollectionViewCell
@property (nonatomic, strong) id<FFAllDayWeekCellProtocol>protocol;
@property (nonatomic, strong) NSDate *date;
@property (strong, nonatomic) UIView *overlay;

- (void)clean;

- (void)showEvents:(NSArray *)array;
@end
