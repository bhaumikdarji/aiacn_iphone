//
//  FFWeekAllDayEventCell.m
//  AddressBook
//
//  Created by smitesh on 2015-07-05.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "FFWeekAllDayEventCell.h"
#import "FFHourAndMinLabel.h"
#import "FFBlueButton.h"
#import "FFEventDetailPopoverController.h"
#import "CreateEventViewController.h"
#import "FFEditEventPopoverController.h"
#import "FFImportantFilesForCalendar.h"
#import "ActivityType.h"
#import "EOPDetailViewController.h"
#import "InterviewDetailViewController.h"
#import "BirthDayViewController.h"
#import "TrainingDetailViewController.h"
#import "ICalendarDetailViewController.h"

@interface FFWeekAllDayEventCell () <FFEventDetailPopoverControllerProtocol, FFEditEventPopoverControllerProtocol>
@property (nonatomic, strong) NSMutableArray *arrayLabelsHourAndMin;
@property (nonatomic, strong) NSMutableArray *arrayButtonsEvents;
@property (nonatomic, strong) FFEventDetailPopoverController *popoverControllerDetails;
@property (nonatomic, strong) FFEditEventPopoverController *popoverControllerEditar;
@property (nonatomic, strong) FFBlueButton *button;

@property (nonatomic,retain) UIPopoverController *popoverController;

@end
@implementation FFWeekAllDayEventCell
@synthesize protocol;
@synthesize date;
@synthesize arrayLabelsHourAndMin;
@synthesize arrayButtonsEvents;
@synthesize popoverControllerDetails;
@synthesize popoverControllerEditar;
@synthesize button;
@synthesize popoverController;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setBackgroundColor:[UIColor whiteColor]];
        
        arrayLabelsHourAndMin = [NSMutableArray new];
        arrayButtonsEvents = [NSMutableArray new];
        
       // [self addLines];
    }
    return self;
}

- (void)showEvents:(NSArray *)array {
    if(!isEmptyString([ActivityType sharedActivity].selectedMode))
        array = [[array filteredArrayUsingPredicate:[[ActivityType sharedActivity] setSelectedModePredicate]] mutableCopy];
    
    [self addButtonsWithArray:array];
}

- (void)addLines {
    
    CGFloat y = 0;
    
    for (int hour=0; hour<=23; hour++) {
        
        
        
        FFHourAndMinLabel *labelHourMin = [[FFHourAndMinLabel alloc] initWithFrame:CGRectMake(0, y, self.frame.size.width, 20) date:[NSDate dateWithHour:hour min:0]];
        [labelHourMin setTextColor:[UIColor grayColor]];
        
        
        [self addSubview:labelHourMin];
        [arrayLabelsHourAndMin addObject:labelHourMin];
        
        y += 1;
        
    }
}

- (void)clean {
    
    [arrayButtonsEvents removeAllObjects];
    
    for (UIView *subview in self.subviews) {
        if ([subview isKindOfClass:[FFBlueButton class]]) {
            [subview removeFromSuperview];
        }
    }
}

- (void)addButtonsWithArray:(NSArray *)array {
    if (array) {
        
        NSMutableArray *arrayFrames = [NSMutableArray new];
        NSMutableDictionary *dictButtonsWithSameFrame = [NSMutableDictionary new];
        BOOL isEop = FALSE;
        BOOL isInt = FALSE;
        float buttonFrame = 2.0f;
        float height = 25.0f;
        for (FFEvent *event in array)
        {
            if (event.allDay)
            {
                if(!isEop && [event.activityType isEqualToString:@"EOP"]){
                    isEop = TRUE;
                }
                if(!isInt && [event.activityType isEqualToString:@"INT"]){
                    isInt = TRUE;
                }
                FFBlueButton *_button = [[FFBlueButton alloc] initWithFrame:CGRectMake(0, buttonFrame, self.frame.size.width, height)];
                CGSize constraintSize;
                _button.titleLabel.numberOfLines = 3;
                constraintSize.width = self.frame.size.width-10;
                
                constraintSize.height = MAXFLOAT;
                
                
                [_button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside]; //amar
                
                //                if([event.activityType isEqualToString:CAL_ACTIVITY_APPOINTMENT])
                //                {
                //                    //Green
                //                    [_button setImage:[UIImage imageNamed:@"ic_cm_business.png"] forState:UIControlStateNormal];
                //                }
                //                else if([event.activityType isEqualToString:CAL_ACTIVITY_BIRTHDAY] ||
                //                        [event.activityType isEqualToString:CAL_ACTIVITY_POLICY_ANNIVERSARY] ||
                //                        [event.activityType isEqualToString:CAL_ACTIVITY_POLICY_MATURITY] ||
                //                        [event.activityType isEqualToString:CAL_ACTIVITY_POLICY_PREMUIM_DUE])
                //                {
                //                    //Red
                //                    [_button setImage:[UIImage imageNamed:@"ic_cm_opportunity.png"] forState:UIControlStateNormal];
                //                }
                //                else if([event.activityType isEqualToString:CAL_ACTIVITY_PERSONAL] ||
                //                        [event.activityType isEqualToString:CAL_ACTIVITY_MEAL] ||
                //                        [event.activityType isEqualToString:CAL_ACTIVITY_OTHER])
                //                {
                //                    //Blue
                //                    [_button setImage:[UIImage imageNamed:@"ic_cm_personal.png"] forState:UIControlStateNormal];
                //                }
                //                else if([event.activityType isEqualToString:CAL_ACTIVITY_COMPANY_EVENT] ||
                //                        [event.activityType isEqualToString:CAL_ACTIVITY_OFFICE_WORK] ||
                //                        [event.activityType isEqualToString:CAL_ACTIVITY_TRAINING])
                //                {
                //                    //orange
                //                    [_button setImage:[UIImage imageNamed:@"ic_cm_office_work.png"] forState:UIControlStateNormal];
                //                }
                
                if([event.activityType isEqualToString:ACTIVITY_INT]){
                    [_button setTitle:NSLocalizedString(event.eventName,@"") forState:UIControlStateNormal];
                }else if([event.activityType isEqualToString:ACTIVITY_ICALENDAR]){
                    NSString *str = @"iCalendar";
                    NSString *stragentcode = [NSString stringWithFormat:@"%@",[CachingService sharedInstance].currentUserIdWithZero];
                    NSString *strdate = [NSString stringWithFormat:@"%@",event.startDate];
                    NSString *newString = [strdate substringToIndex:10];
                    NSArray *eventArr = [[NSArray alloc]init];
                    eventArr = [DbUtils fetchAllObject:@"ICalendarEvents"
                                            andPredict:[NSPredicate predicateWithFormat:@"startTime contains[cd] %@ AND agentCode == %@",newString,stragentcode]
                                     andSortDescriptor:nil
                                  managedObjectContext:[CachingService sharedInstance].managedObjectContext];
                    
                    NSLog(@"Count is %d",eventArr.count);
                    if (eventArr.count == 0) {
                        return;//avoid to show 0 iCalender as a count
                    }
                    [_button setTitle:[NSString stringWithFormat:@"%d %@",eventArr.count,NSLocalizedString(str, @"")] forState:UIControlStateNormal];
                } else {
                    [_button setTitle:event.eventName forState:UIControlStateNormal];
                }
                
                CGSize size = [_button.titleLabel.text sizeWithFont:_button.titleLabel.font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
                [_button setTitleColor:[[ActivityType sharedActivity] setTextColorOnActivityType:event.activityType] forState:UIControlStateNormal];
                [_button setEvent:event];
                [_button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 2.0, 0.0, 0.0)];
                _button.backgroundColor = [[ActivityType sharedActivity] setBgColorOnActivityType:event.activityType];
                
                _button.frame = CGRectMake(0, buttonFrame, constraintSize.width, size.height);
                [arrayButtonsEvents addObject:_button];
                [self addSubview:_button];
                
                // Save Frames for next step
                //                NSLog(@"height:::%f,%f",_button.frame.size.height,size.height);
                buttonFrame += _button.frame.size.height+2.0f;
            }
        }
    }
}

#pragma mark - Button Action

- (IBAction)buttonAction:(id)sender {
    
    button = (FFBlueButton *)sender;
    if([button.event.activityType isEqualToString:ACTIVITY_HOLIDAY]){
        
    }else if([button.event.activityType isEqualToString:ACTIVITY_NAP_TRANING]){
        [self showTraining:button.event];
    }else if([button.event.activityType isEqualToString:ACTIVITY_BIRTHDAY]){
        [self showBirthDay:button.event];
    }else if([button.event.activityType isEqualToString:ACTIVITY_ALE]){
        
    }else if([button.event.activityType isEqualToString:ACTIVITY_EOP]){
        [self showEOPDetail:button.event];
    }else if([button.event.activityType isEqualToString:ACTIVITY_INT]){
        [self showInterviewDetailEvent:button.event];
    }else if([button.event.activityType isEqualToString:ACTIVITY_ICALENDAR]){
        [self showICalenderDetailEvent:button.event];
    }else{
        [self showPopoverEditWithEvent:button.event];
    }
}

#pragma mark - FFEventDetailPopoverController Protocol

- (void)showTraining:(FFEvent *)_event{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    TrainingDetailViewController *trainingDetailViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"TrainingDetailViewController"];
    trainingDetailViewCtrl.event = _event;
    popoverController = [[UIPopoverController alloc] initWithContentViewController:trainingDetailViewCtrl];
    [trainingDetailViewCtrl setDoneTrainingDetail:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController.backgroundColor = trainingDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showBirthDay:(FFEvent *)_event{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    BirthDayViewController *birthDayViewCtrl = [storyboard instantiateViewControllerWithIdentifier:@"BirthDayViewController"];
    birthDayViewCtrl.birthDate = _event.dateDay;
    [birthDayViewCtrl setCancelBirthDayView:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    [birthDayViewCtrl setSendEGreeting:^(NSMutableArray *emailArray) {
        [popoverController dismissPopoverAnimated:TRUE];
        [ApplicationFunction showGrettingView:emailArray];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:birthDayViewCtrl];
    popoverController.backgroundColor = birthDayViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showEOPDetail:(FFEvent *)_event{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navConroller = [storyboard instantiateViewControllerWithIdentifier:@"EOPDetailNavController"];
    EOPDetailViewController *eopDetailViewCtrl = navConroller.viewControllers[0];
    eopDetailViewCtrl.event = _event;
    [eopDetailViewCtrl setDismissPopOverBlock:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:navConroller];
    popoverController.backgroundColor = eopDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showInterviewDetailEvent:(FFEvent *)_event{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navConroller = [storyboard instantiateViewControllerWithIdentifier:@"InterviewDetailNavController"];
    InterviewDetailViewController *interviewDetailViewCtrl = navConroller.viewControllers[0];
    interviewDetailViewCtrl.event = _event;
    [interviewDetailViewCtrl setDismissPopOverBlock:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:navConroller];
    popoverController.backgroundColor = interviewDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showICalenderDetailEvent:(FFEvent *)_event{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navConroller = [storyboard instantiateViewControllerWithIdentifier:@"iCalendarDetailNavController"];
    ICalendarDetailViewController *iCalendarDetailViewCtrl = navConroller.viewControllers[0];
    iCalendarDetailViewCtrl.event = _event;
    [iCalendarDetailViewCtrl setDismissPopOverBlock:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:navConroller];
    popoverController.backgroundColor = iCalendarDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showPopoverEditWithEvent:(FFEvent *)_event {

    CreateEventViewController *createEventPopoverView = [[CreateEventViewController alloc] initWithNibName:@"CreateEventViewController" bundle:nil];
    createEventPopoverView.event = _event;
    [createEventPopoverView setEditEvent:^(FFEvent *event, NSDate *sdate, NSDate *eDate,MBProgressHUDUpd *hud)
    {
        [self deleteEvent:event];
        [self saveEditedEvent:event];
        [hud hide:TRUE];
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    
    [createEventPopoverView setDeleteEvent:^(FFEvent *event, NSDate *oldStartDate, NSDate *oldEndDate) {
        [self deleteEvent:event];
        [ApplicationFunction deleteCalManualEvent:event];
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    
    [createEventPopoverView setCancelEvent:^(UIView *view) {
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    
    popoverController = [[UIPopoverController alloc] initWithContentViewController:createEventPopoverView];
    popoverController.popoverContentSize = createEventPopoverView.view.frame.size;
    popoverController.backgroundColor = createEventPopoverView.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight
                                     animated:YES];
    

}

#pragma mark - FFEditEventPopoverController Protocol

- (void)saveEditedEvent:(FFEvent *)eventNew {
    
    if (protocol != nil && [protocol respondsToSelector:@selector(saveEditedEvent:)]) {
        [protocol saveEditedEvent:eventNew];
    }
}

- (void)deleteEvent: (FFEvent *)eventNew
{
        if (protocol != nil && [protocol respondsToSelector:@selector(deleteEventOfCell:)])
        {
            [protocol deleteEventOfCell:eventNew];
        }
   
}

- (void)deleteEvent{
    
}
@end
