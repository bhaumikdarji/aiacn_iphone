//
//  FFWeekCell.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/22/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFWeekCell.h"

#import "FFHourAndMinLabel.h"
#import "FFBlueButton.h"
#import "FFEventDetailPopoverController.h"
#import "EOPDetailViewController.h"
#import "InterviewDetailViewController.h"
#import "CreateEventViewController.h"
#import "FFEditEventPopoverController.h"
#import "FFImportantFilesForCalendar.h"
#import "ActivityType.h"
#import "ICalendarDetailViewController.h"

@interface FFWeekCell () <FFEventDetailPopoverControllerProtocol, FFEditEventPopoverControllerProtocol>
@property (nonatomic, strong) NSMutableArray *arrayLabelsHourAndMin;
@property (nonatomic, strong) NSMutableArray *arrayButtonsEvents;
@property (nonatomic, strong) FFEventDetailPopoverController *popoverControllerDetails;
@property (nonatomic, strong) FFEditEventPopoverController *popoverControllerEditar;
@property (nonatomic, strong) FFBlueButton *button;

@property (nonatomic,retain) UIPopoverController *popoverController;

@end

@implementation FFWeekCell

@synthesize protocol;
@synthesize date;
@synthesize arrayLabelsHourAndMin;
@synthesize arrayButtonsEvents;
@synthesize popoverControllerDetails;
@synthesize popoverControllerEditar;
@synthesize button;
@synthesize popoverController;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setBackgroundColor:[UIColor whiteColor]];
        
        arrayLabelsHourAndMin = [NSMutableArray new];
        arrayButtonsEvents = [NSMutableArray new];
        
        [self addLines];
    }
    return self;
}

- (void)showEvents:(NSArray *)array {
    if(!isEmptyString([ActivityType sharedActivity].selectedMode))
        array = [[array filteredArrayUsingPredicate:[[ActivityType sharedActivity] setSelectedModePredicate]] mutableCopy];
    
    [self addButtonsWithArray:array];
}

- (void)addLines {
    
    CGFloat y = 0;
    
    for (int hour=0; hour<=23; hour++) {
        
        for (int min=0; min<=45; min=min+MINUTES_PER_LABEL) {
            
            FFHourAndMinLabel *labelHourMin = [[FFHourAndMinLabel alloc] initWithFrame:CGRectMake(0, y, self.frame.size.width, HEIGHT_CELL_MIN) date:[NSDate dateWithHour:hour min:min]];
            [labelHourMin setTextColor:[UIColor grayColor]];
            if (min == 0) {
                UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT_CELL_MIN/2., self.frame.size.width, 1.)];
                [view setBackgroundColor:[UIColor colorWithRed:214.0/255.0 green:214.0/255.0 blue:214.0/255.0 alpha:1.0]];
                [labelHourMin addSubview:view];
                [view setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
                [labelHourMin setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
            }
            [self addSubview:labelHourMin];
            [arrayLabelsHourAndMin addObject:labelHourMin];
            
            y += HEIGHT_CELL_MIN;
        }
    }
}

- (void)clean {
    
     [arrayButtonsEvents removeAllObjects];
    
    for (UIView *subview in self.subviews) {
        if ([subview isKindOfClass:[FFBlueButton class]]) {
            [subview removeFromSuperview];
        }
    }
}

- (void)addButtonsWithArray:(NSArray *)array {
    
    if (array) {
        
        NSMutableArray *arrayFrames = [NSMutableArray new];
        NSMutableDictionary *dictButtonsWithSameFrame = [NSMutableDictionary new];
        BOOL isEop = FALSE;
        BOOL isInt = FALSE;
        for (FFEvent *event in array) {
            if(!isEop && [event.activityType isEqualToString:@"EOP"]){
                isEop = TRUE;
            }
            if(!isInt && [event.activityType isEqualToString:@"INT"]){
                isInt = TRUE;
            }
            
            CGFloat yTimeBegin = 0;
            CGFloat yTimeEnd = 0;
            
            for (FFHourAndMinLabel *label in arrayLabelsHourAndMin) {
                NSDateComponents *compLabel = [NSDate componentsOfDate:label.dateHourAndMin];
                NSDateComponents *compEventBegin = [NSDate componentsOfDate:event.dateTimeBegin];
                NSDateComponents *compEventEnd = [NSDate componentsOfDate:event.dateTimeEnd];
                
                if(event.startDate){
                    if(![event.startDate isEqualToDateIgnoringTime:event.endDate]){
                        compEventBegin = [NSDate componentsOfDate:[event getTimeBeginForMultiEvent:self.date]];
                        compEventEnd = [NSDate componentsOfDate:[event getTimeEndForMultiEvent:self.date]];
                    }
                }
                
                if (compLabel.hour == compEventBegin.hour && compLabel.minute <= compEventBegin.minute && compEventBegin.minute < compLabel.minute+MINUTES_PER_LABEL) {
                    yTimeBegin = label.frame.origin.y+label.frame.size.height/2.;
                }
                if (compLabel.hour == compEventEnd.hour && compLabel.minute <= compEventEnd.minute && compEventEnd.minute < compLabel.minute+MINUTES_PER_LABEL) {
                    yTimeEnd = label.frame.origin.y+label.frame.size.height;
                }
            }
            
            FFBlueButton *_button = [[FFBlueButton alloc] initWithFrame:CGRectMake(0, yTimeBegin, self.frame.size.width, yTimeEnd-yTimeBegin)];
            [_button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside]; //amar
            if (event.allDay) {
                [_button setBackgroundColor:[UIColor clearColor]];
                [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            else
            {
                [_button setBackgroundColor:[[ActivityType sharedActivity] setBgColorOnActivityType:event.activityType]];
            }
            if([event.activityType isEqualToString:ACTIVITY_INT]){
                [_button setTitle:NSLocalizedString(event.eventName,@"") forState:UIControlStateNormal];
            }else{
                [_button setTitle:event.eventName forState:UIControlStateNormal];
            }
            
            [_button setEvent:event];
            [_button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)];
            
            [arrayButtonsEvents addObject:_button];
            
            if (!event.allDay)
            {
                [self addSubview:_button];
            }
            
            // Save Frames for next step
            NSValue *value = [NSValue valueWithCGRect:_button.frame];
            if ([arrayFrames containsObject:value]) {
                NSMutableArray *array = [dictButtonsWithSameFrame objectForKey:value];
                if (!array){
                    array = [[NSMutableArray alloc] initWithObjects:[arrayButtonsEvents objectAtIndex:[arrayFrames indexOfObject:value]], nil];
                    
                }
                [array addObject:_button];
                [dictButtonsWithSameFrame setObject:array forKey:value];
            }
            [arrayFrames addObject:value];
        }
        
        // Recaulate frames of buttons that have the same begin and end date
        for (NSValue *value in dictButtonsWithSameFrame) {
            NSArray *array = [dictButtonsWithSameFrame objectForKey:value];
            CGFloat width = (self.frame.size.width)/array.count;
            for (int i = 0; i < array.count; i++) {
                UIButton *buttonInsideArray = [array objectAtIndex:i];
                [buttonInsideArray setFrame:CGRectMake(i*width, buttonInsideArray.frame.origin.y, width, buttonInsideArray.frame.size.height)];
            }
        }
    }
}

#pragma mark - Button Action

- (IBAction)buttonAction:(id)sender {
    
    button = (FFBlueButton *)sender;
    if([button.event.activityType isEqualToString:ACTIVITY_HOLIDAY]){
        
    }else if([button.event.activityType isEqualToString:ACTIVITY_NAP_TRANING]){
        
    }else if([button.event.activityType isEqualToString:ACTIVITY_BIRTHDAY]){
        
    }else if([button.event.activityType isEqualToString:ACTIVITY_ALE]){
        
    }else if([button.event.activityType isEqualToString:ACTIVITY_EOP]){
        [self showEOPDetail:button.event];
    }else if([button.event.activityType isEqualToString:ACTIVITY_INT]){
        [self showInterviewDetailEvent:button.event];
    }else if([button.event.activityType isEqualToString:ACTIVITY_ICALENDAR]){
        [self showICalendarDetailEvent:button.event];
    }else{
        [self showPopoverEditWithEvent:button.event];
    }
}


#pragma mark - FFEventDetailPopoverController Protocol

- (void)showEOPDetail:(FFEvent *)_event{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navConroller = [storyboard instantiateViewControllerWithIdentifier:@"EOPDetailNavController"];
    EOPDetailViewController *eopDetailViewCtrl = navConroller.viewControllers[0];
    eopDetailViewCtrl.event = _event;
    [eopDetailViewCtrl setDismissPopOverBlock:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:navConroller];
    popoverController.backgroundColor = eopDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showInterviewDetailEvent:(FFEvent *)_event{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navConroller = [storyboard instantiateViewControllerWithIdentifier:@"InterviewDetailNavController"];
    InterviewDetailViewController *interviewDetailViewCtrl = navConroller.viewControllers[0];
    interviewDetailViewCtrl.event = _event;
    [interviewDetailViewCtrl setDismissPopOverBlock:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:navConroller];
    popoverController.backgroundColor = interviewDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showICalendarDetailEvent:(FFEvent *)_event{
    
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navConroller = [storyboard instantiateViewControllerWithIdentifier:@"iCalendarDetailNavController"];
    ICalendarDetailViewController *icalendarDetailViewCtrl = navConroller.viewControllers[0];
    icalendarDetailViewCtrl.event = _event;
    [icalendarDetailViewCtrl setDismissPopOverBlock:^{
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:navConroller];
    popoverController.backgroundColor = icalendarDetailViewCtrl.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:TRUE];
}

- (void)showPopoverEditWithEvent:(FFEvent *)_event {

    CreateEventViewController *createEventPopoverView = [[CreateEventViewController alloc] initWithNibName:@"CreateEventViewController" bundle:nil];
    createEventPopoverView.event = _event;
    [createEventPopoverView setEditEvent:^(FFEvent *event, NSDate *sdate, NSDate *eDate, MBProgressHUDUpd *hud) {
        [self deleteEvent:sdate oldEndDate:eDate];
        [self saveEditedEvent:event];
        [hud hide:TRUE];
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    
    [createEventPopoverView setDeleteEvent:^(FFEvent *event, NSDate *oldStartDate, NSDate *oldEndDate) {
        [self deleteEvent:oldStartDate oldEndDate:oldEndDate];
        [ApplicationFunction deleteCalManualEvent:event];
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    
    [createEventPopoverView setCancelEvent:^(UIView *view) {
        [popoverController dismissPopoverAnimated:TRUE];
    }];
    
    popoverController = [[UIPopoverController alloc] initWithContentViewController:createEventPopoverView];
    popoverController.popoverContentSize = createEventPopoverView.view.frame.size;
    popoverController.backgroundColor = createEventPopoverView.view.backgroundColor;
    [popoverController presentPopoverFromRect:button.frame
                                       inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight
                                     animated:YES];
    
}

#pragma mark - FFEditEventPopoverController Protocol

- (void)saveEditedEvent:(FFEvent *)eventNew {
    
    if (protocol != nil && [protocol respondsToSelector:@selector(saveEditedEvent:ofCell:atIndex:)]) {
        [protocol saveEditedEvent:eventNew ofCell:self atIndex:[arrayButtonsEvents indexOfObject:button]];
    }
}

- (void)deleteEvent: (NSDate *)oldStartDate oldEndDate:(NSDate *)oldEndDate {
    if([oldStartDate isEqualToDateIgnoringTime:oldEndDate]){
        if (protocol != nil && [protocol respondsToSelector:@selector(deleteEventOfCell:atIndex:)]) {
            [protocol deleteEventOfCell:self atIndex:[arrayButtonsEvents indexOfObject:button]];
        }
    }else{
        if (protocol != nil && [protocol respondsToSelector:@selector(deleteEventOfCell:atIndex:oldStartDate:oldEndDate:)]) {
            [protocol deleteEventOfCell:self atIndex:[arrayButtonsEvents indexOfObject:button] oldStartDate:oldStartDate oldEndDate:oldEndDate];
        }
    }
}

- (void)deleteEvent{
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
