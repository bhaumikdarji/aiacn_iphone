//
//  FFWeekSubHeaderCollectionView.h
//  AddressBook
//
//  Created by smitesh on 2015-07-01.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FFWeekSubHeaderCollectionViewProtocol <NSObject>
@required
- (void)subheaderDidScroll;
- (void)showHourLine:(BOOL)show;
@end
@interface FFWeekSubHeaderCollectionView : UICollectionView
@property id<FFWeekSubHeaderCollectionViewProtocol> protocol;
@property (nonatomic, strong) NSDate *selectedDate;
@property (strong, nonatomic) NSMutableArray *dayEventArray;
@property (strong, nonatomic) NSDate *filterDate;
@end
