//
//  FFWeekAllDayCollectionView.h
//  AddressBook
//
//  Created by smitesh on 2015-07-05.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FFWeekAllDayCollectionViewProtocol <NSObject>
@required
- (void)collectionViewAllDayScroll;
- (void)setNewDictionary:(NSDictionary *)dict;
@end

@interface FFWeekAllDayCollectionView : UICollectionView
@property (nonatomic, strong) NSMutableDictionary *dictEvents;
@property (nonatomic, strong) id<FFWeekAllDayCollectionViewProtocol> protocol;
@end
