//
//  FFWeekHeaderCell.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/26/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFWeekHeaderCell.h"
#import "FFImportantFilesForCalendar.h"

@implementation FFWeekHeaderCell

@synthesize labelDate;
@synthesize labelDay;
@synthesize imageView;
@synthesize date;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        labelDay = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., self.frame.size.width, self.frame.size.height)];
        [labelDay setAutoresizingMask:AR_LEFT_RIGHT];
        [self addSubview:labelDay];
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(35., 0., 32.0, self.frame.size.height)];
        [imageView setAutoresizingMask:AR_LEFT_RIGHT];
        [imageView setContentMode:UIViewContentModeCenter];
        [self addSubview:imageView];

        labelDate = [[UILabel alloc] initWithFrame:CGRectMake(0, 0., imageView.frame.size.width, imageView.frame.size.height)];
        [labelDate setAutoresizingMask:AR_LEFT_RIGHT];
        [labelDate setTextAlignment:NSTextAlignmentCenter];
        [imageView addSubview:labelDate];

        self.lblcurrentDateSelection = [[UILabel alloc] initWithFrame:CGRectMake(0., self.frame.size.height - 3, self.frame.size.width-1, 2)];
        self.lblcurrentDateSelection.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:33.0/255.0 blue:8.0/255.0 alpha:1.0];
        self.lblcurrentDateSelection.hidden = TRUE;
        [self addSubview:self.lblcurrentDateSelection];
        
    }
    return self;
}

- (void)cleanCell {
    
    [imageView setImage:nil];
    [labelDate setText:@""];
    [labelDate setTextColor:[UIColor colorWithRed:76.0/255.0 green:76.0/255.0 blue:76.0/255.0 alpha:1.0]];
    [labelDay setText:@""];
    [labelDay setTextColor:[UIColor colorWithRed:76.0/255.0 green:76.0/255.0 blue:76.0/255.0 alpha:1.0]];
    date = nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
