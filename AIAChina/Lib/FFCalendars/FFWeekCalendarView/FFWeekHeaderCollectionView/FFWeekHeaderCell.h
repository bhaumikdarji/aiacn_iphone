//
//  FFWeekHeaderCell.h
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/26/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import <UIKit/UIKit.h>

@interface FFWeekHeaderCell : UICollectionViewCell

@property (nonatomic, strong) UILabel *labelDate;
@property (nonatomic, strong) UILabel *labelDay;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) UILabel *lblcurrentDateSelection;

- (void)cleanCell;

@end
