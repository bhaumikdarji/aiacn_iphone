//
//  FFWeekSubHeaderCell.h
//  AddressBook
//
//  Created by smitesh on 2015-07-02.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFWeekSubHeaderCell : UICollectionViewCell<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *arrayOfAllDayEvents;
@property (nonatomic, retain) NSMutableArray *arrayOfAllDayEvents1;
- (void)cleanSubHeaderCell;
@end
