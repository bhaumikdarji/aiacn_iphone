//
//  FFWeekAllDayScrollView.m
//  AddressBook
//
//  Created by smitesh on 2015-07-05.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "FFWeekAllDayScrollView.h"
#import "FFImportantFilesForCalendar.h"
#import "FFViewWithHourLines.h"
@interface FFWeekAllDayScrollView () <UIScrollViewDelegate>
@property (nonatomic, strong) FFViewWithHourLines *viewWithHourLines;
@end
@implementation FFWeekAllDayScrollView
@synthesize dictEvents;
@synthesize viewWithHourLines;
@synthesize collectionViewWeek;
@synthesize labelWithActualHour;
@synthesize labelGrayWithActualHour;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setAutoresizingMask:AR_WIDTH_HEIGHT];
    }
    return self;
}

- (void)setDictEvents:(NSMutableDictionary *)_dictEvents
{
    
    dictEvents = _dictEvents;  
    if (!viewWithHourLines) {
        
        viewWithHourLines = [[FFViewWithHourLines alloc] initWithFrame:CGRectMake(0, 0, 80., self.frame.size.height)];
        //[self addSubview:viewWithHourLines];
        
        collectionViewWeek = [[FFWeekAllDayCollectionView alloc] initWithFrame:CGRectMake(viewWithHourLines.frame.size.width,viewWithHourLines.frame.origin.y,self.frame.size.width-viewWithHourLines.frame.size.width,370)collectionViewLayout:[UICollectionViewFlowLayout new]];
        [self scrollToPage:(int)[NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]].weekOfMonth+1];
        collectionViewWeek.backgroundColor = [UIColor clearColor];
        [self addSubview:collectionViewWeek];
        
        labelWithActualHour = [viewWithHourLines labelWithCurrentHourWithWidth:self.frame.size.width];
        [labelWithActualHour setFrame:CGRectMake(labelWithActualHour.frame.origin.x, labelWithActualHour.frame.origin.y+viewWithHourLines.frame.origin.y, labelWithActualHour.frame.size.width, labelWithActualHour.frame.size.height)];
       // [self addSubview:labelWithActualHour];
        labelGrayWithActualHour = viewWithHourLines.labelWithSameYOfCurrentHour;
        [labelGrayWithActualHour setAlpha:0.];
        
        [self setContentSize:CGSizeMake(700, 370)];
        [self setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    [collectionViewWeek setDictEvents:dictEvents];
    [collectionViewWeek reloadData];
}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
- (void)updateContentSize:(int)number
{
    [self setContentSize:CGSizeMake(700, 170)];
}
- (void)scrollToPage:(int)_intPage {
    //    NSInteger intIndex = 7*_intPage-1;
    //    [collectionViewWeek scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:intIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
    [collectionViewWeek setContentOffset:CGPointMake((_intPage-1)*collectionViewWeek.frame.size.width, 0.)];
}
@end
