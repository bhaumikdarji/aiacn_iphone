//
//  FFWeekCalendarView.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/21/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFWeekCalendarView.h"

#import "FFWeekHeaderCollectionView.h"
#import "FFWeekScrollView.h"
#import "FFImportantFilesForCalendar.h"
#import "FFWeekSubHeaderCollectionView.h"
#import "FFWeekAllDayScrollView.h"
@interface FFWeekCalendarView () <FFWeekCollectionViewProtocol, FFWeekHeaderCollectionViewProtocol,FFWeekSubHeaderCollectionViewProtocol,FFWeekAllDayCollectionViewProtocol>
@property (nonatomic, strong) FFWeekScrollView *weekContainerScroll;
@property (nonatomic, strong) FFWeekAllDayScrollView *weekAllDayScroll;
;
@end

@implementation FFWeekCalendarView

#pragma mark - Synthesize

@synthesize dictEvents;
@synthesize scrollViewHeaderWeek;
@synthesize weekContainerScroll;
@synthesize weekAllDayScroll;
@synthesize protocol;
@synthesize scrollViewSubHeaderWeek;
#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dateChanged:) name:DATE_MANAGER_DATE_CHANGED object:nil];
        [self setBackgroundColor:[UIColor whiteColor]];
        
        [self setAutoresizingMask: AR_WIDTH_HEIGHT];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setDictEvents:(NSMutableDictionary *)_dictEvents {
    
    dictEvents = _dictEvents;
    
    if (!scrollViewHeaderWeek) {
        UIView *viewLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80., HEADER_HEIGHT_SCROLL)];
        [viewLeft setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:viewLeft];
        
        scrollViewHeaderWeek = [[FFWeekHeaderCollectionView alloc] initWithFrame:CGRectMake(viewLeft.frame.size.width, 0, self.frame.size.width-viewLeft.frame.size.width, HEADER_HEIGHT_SCROLL)];
        [scrollViewHeaderWeek setProtocol:self];
        
        [self scrollToPage:(int)[NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]].weekOfMonth+1];
        [self addSubview:scrollViewHeaderWeek];
        
        weekAllDayScroll = [[FFWeekAllDayScrollView alloc] initWithFrame:CGRectMake(0, HEADER_HEIGHT_SCROLL, self.frame.size.width,HEADER_HEIGHT_SCROLL)];
        weekAllDayScroll.backgroundColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0];
        [self addSubview:weekAllDayScroll];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10.0, HEADER_HEIGHT_SCROLL, 80, 70.0f)];
        label.text = NSLocalizedString(@"All-day", @"");
        label.backgroundColor = [UIColor clearColor];
        [self addSubview:label];
        
        weekContainerScroll = [[FFWeekScrollView alloc] initWithFrame:CGRectMake(0, 2*HEADER_HEIGHT_SCROLL, self.frame.size.width, self.frame.size.height-2*HEADER_HEIGHT_SCROLL)];
        [self addSubview:weekContainerScroll];
    }
    UILabel *bottomSpep = [[UILabel alloc] initWithFrame:CGRectMake(0, HEADER_HEIGHT_SCROLL - 1, self.frame.size.width, 1.)];
    bottomSpep.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0];
    [self addSubview:bottomSpep];
    [weekAllDayScroll setDictEvents:dictEvents];
    [weekAllDayScroll.collectionViewWeek setProtocol:self];
    [weekContainerScroll setDictEvents:dictEvents];
    [weekContainerScroll.collectionViewWeek setProtocol:self];
    
}

- (void)scrollToPage:(int)_intPage {
    //scroll to page complete
    NSInteger intIndex = 7*_intPage-1;
    [scrollViewHeaderWeek scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:intIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:NO];
}

#pragma mark - Invalidate Layout

- (void)invalidateLayout {
    
    [scrollViewHeaderWeek.collectionViewLayout invalidateLayout];
    [weekContainerScroll.collectionViewWeek.collectionViewLayout invalidateLayout];
    [self dateChanged:nil];
}

#pragma mark - FFDateManager Notification

- (void)dateChanged:(NSNotification *)not {
    
    [scrollViewHeaderWeek reloadData];
    [self scrollToPage:(int)[NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]].weekOfMonth+1];
    
    [weekContainerScroll.collectionViewWeek reloadData];
    [weekAllDayScroll.collectionViewWeek reloadData];
}

#pragma mark - FFWeekCollectionView Protocol

- (void)collectionViewDidScroll {
    
    CGPoint offset = scrollViewHeaderWeek.contentOffset;
    offset.x = weekContainerScroll.collectionViewWeek.contentOffset.x;
    [scrollViewHeaderWeek setContentOffset:offset];
}

- (void)showHourLine:(BOOL)show {
    
    [weekContainerScroll showlabelsWithActualHourWithAlpha:show];
}

- (void)setNewDictionary:(NSDictionary *)dict {
    
    if (protocol != nil && [protocol respondsToSelector:@selector(setNewDictionary:)]) {
        [protocol setNewDictionary:dictEvents];
    }
}
- (void)collectionViewAllDayScroll{
    CGPoint offset = scrollViewHeaderWeek.contentOffset;
    offset.x = weekAllDayScroll.collectionViewWeek.contentOffset.x;
    [weekAllDayScroll setContentOffset:offset];

    
}
#pragma mark - FFWeekHeaderCollectionView Protocol

- (void)headerDidScroll {
    
    CGPoint offset = weekContainerScroll.collectionViewWeek.contentOffset;
    offset.x = scrollViewHeaderWeek.contentOffset.x;
    [weekContainerScroll.collectionViewWeek setContentOffset:offset];
    [weekAllDayScroll.collectionViewWeek setContentOffset:offset];
    //[scrollViewSubHeaderWeek setContentOffset:offset];
}
- (void)subheaderDidScroll
{
    NSLog(@"");
}
@end
