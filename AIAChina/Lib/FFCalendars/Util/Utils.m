//
//  Utils.m
//  iPos
//
//  Created by Rickey Tom on 2015-02-24.
//  Copyright (c) 2015 quixcreations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "CachingService.h"

NSString *const DataLoadException = @"DataLoadException";

@implementation Utils

+(NSString *) dateFromNumber:(NSNumber *)dateNumber {
    
    double nDate = [dateNumber doubleValue];
    // Calculations seems to be off by 2 days.
    nDate -= 2;
    NSDate *year1900 = [[Utils dateFormatter] dateFromString:@"01-Jan-1900"];

    NSTimeInterval numSeconds = nDate*24*60*60.0;
    NSDate *date = [[NSDate alloc] initWithTimeInterval: numSeconds
                                      sinceDate: year1900];
    
    return [[Utils dateFormatter] stringFromDate:date];
}

+(NSString *) stringFromDate:(NSDate *) date
{
    if (date == nil)
    {
        return @"";
    }
    else
    {
        return [[Utils dateFormatter] stringFromDate:date];
    }
}

+(NSDateFormatter *) ddmmyyyyDateFormatter
{
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken1 = 0;
    dispatch_once(&onceToken1, ^ {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    });
    
    return dateFormatter;
}


+(NSDateFormatter *) dateFormatter
{
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^ {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    });
    
    return dateFormatter;
}

+(bool) IsValidEmail:(NSString *) emailStringToCheck
{
    bool stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    bool valid = [emailTest evaluateWithObject:emailStringToCheck];
    if (!valid)
    {
        [Utils displayMessage:NSLocalizedString(@"Please enter a valid email address.", @"")  withTitle:NSLocalizedString(@"WarningMessageBoxTitleKey", nil)];
        return false;
    }
    
    return true;
}

+ (id)loadDataFromPath:(NSString *)path
{
    NSError *error = nil;
    @try {
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:[path stringByDeletingPathExtension] ofType:[path pathExtension]];

        NSData *data = [NSData dataWithContentsOfFile:resourcePath options:0 error:&error];
        if (error)
            [NSException raise:DataLoadException format:@"%@", error];
        
        return data;
        
    
    }
    @catch(NSException *exception) {
        NSLog(@"*** Error loading json file %@. Error:%@", path, exception.description);
    }
}

+ (id)loadJsonDataFromPath:(NSString *)path
{
    NSError *error = nil;
    
    @try {
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:[path stringByDeletingPathExtension] ofType:[path pathExtension]];
        
        NSData *data = [NSData dataWithContentsOfFile:resourcePath options:0 error:&error];
        if (error)
            [NSException raise:DataLoadException format:@"%@", error];
        
        error = nil;
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if (error)
            [NSException raise:DataLoadException format:@"%@", error];
        
        return json;
    }
    @catch (NSException *exception) {
        NSLog(@"*** Error loading json file %@. Error:%@", path, exception.description);
    }
}

+(NSString *) dateOfFirstDayOfNextMonth
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    int year = (int)[components year];
    int month = (int)[components month] + 1;
    
    if (month==13) {
        month = 1;
        year += 1;
    }
    
    [components setDay:1];
    [components setMonth:month];
    [components setYear:year];
    NSDate *dt = [calendar dateFromComponents:components];
    return [Utils stringFromDate:dt];
}

+(NSString *) ageLastBirhDate:(NSDate *) birthdate
{
    return [self ageLastBirhDate:birthdate relativeToEffectiveDate:[NSDate date]];
}

+(NSString *) ageLastBirhDate:(NSDate *) birthdate relativeToEffectiveDate:(NSDate *) effectiveDate
{
    NSString *alb = @"";
    if (birthdate != nil)
    {
        // If the date does not include a time component, the method, gregorianCalendar
        // generates an exception. So we will get the date, and then create a date with time.
        NSString *bd = [[Utils dateFormatter] stringFromDate:birthdate];

        @try {
            // Expected format of the date string
            // 01-Oct-1975
            NSDate *date = [[Utils dateFormatter] dateFromString: bd];
            
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *components = [gregorianCalendar components:(NSDayCalendarUnit |NSYearCalendarUnit )
                                                                fromDate:date
                                                                  toDate:effectiveDate
                                                                 options:0];
            long years = [components year];
            
            if (years > 0)
            {
                alb = [NSString stringWithFormat:@"%ld years", years];
            }
            else
            {
                long days = [components day];
                alb = [NSString stringWithFormat:@"%ld days", days];
            }
            
        }
        @catch (NSException *exception) {
            
        }
    }
    return alb;
}

+(long) ageLastBirhDateInYears:(NSString *) birthdate
{
    NSDate *date = [[Utils dateFormatter] dateFromString: birthdate];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:(NSDayCalendarUnit |NSYearCalendarUnit )
                                                        fromDate:date
                                                          toDate:[NSDate date]
                                                         options:0];
    return [components year];
}

+(NSString *) currencyFromNumber:(double) number
{
    static NSNumberFormatter *numberFormatter = nil;
    static dispatch_once_t token38 = 0;
    dispatch_once(&token38, ^ {
        numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    });
    
    @try {
        return [numberFormatter stringFromNumber:[NSNumber numberWithDouble:number]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error formatting %f", number);
        return  [NSString stringWithFormat:@"%.2f", number];
    }
}

+(bool) isValidPhoneNumber:(NSString *) phone
{
    // This one looks for a specific format (###) ###-#####
    //NSString *phoneRegex2 = @"^(\\([0-9]{3})\\) [0-9]{3}-[0-9]{4}$";
    //NSPredicate *phoneTest2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex2];
    //return [phoneTest2 evaluateWithObject:phone];
    
    // This one just looks for valid characters
    static NSString *ACCEPTABLE_CHARECTERS = @",0123456789(-) ";
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
    NSString *filtered = [[phone componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [phone isEqualToString:filtered];
}

+(void) displayMessage:(NSString *) msg withTitle:(NSString *) title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    [alert addButtonWithTitle:NSLocalizedString(@"OK", nil)];
    [alert show];
}

@end
