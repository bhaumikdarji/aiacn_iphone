//
//  FFEvent.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/16/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFEvent.h"
#import "FFCalendar.h"
#import "ContactDataProvider.h"
#import "DateUtils.h"
#import "TblManualEntryCalendarEvents.h"
#import "TblTraninigEvent.h"

@implementation FFEvent

- (void)createEvent:(NSDictionary *)eventDic{
    
    self.activityType = [ValueParser parseString:[eventDic valueForKey:@"activityType"]];
    self.allDay = ([[ValueParser parseString:[eventDic valueForKey:@"allDay"]] isEqualToString:@"yes"]) ? TRUE : FALSE;
    self.eventName = [ValueParser parseString:[eventDic valueForKey:@"eventName"]];
    self.notes = [ValueParser parseString:[eventDic valueForKey:@"notes"]];
    self.location = [ValueParser parseString:[eventDic valueForKey:@"location"]];
    self.latitude = [ValueParser parseDouble:[eventDic valueForKey:@"latitude"]];
    self.longitude = [ValueParser parseDouble:[eventDic valueForKey:@"longitude"]];
    self.reminder = [ValueParser parseString:[eventDic valueForKey:@"reminder"]];
    self.reminderMode = [ValueParser parseString:[eventDic valueForKey:@"reminderMode"]];
    self.startDate = [NSDate stringToDate:[eventDic valueForKey:@"startDate"] dateFormat:kDateFormatte];
    self.endDate = [NSDate stringToDate:[eventDic valueForKey:@"endDate"] dateFormat:kDateFormatte];
    self.candidateArray = [eventDic valueForKey:@"candidateArray"];
    
    [self setEventStartEndTime];
    [self saveEventInLocalDB];
}

- (void)setCalendarEvent:(TblManualEntryCalendarEvents *)manualEntryCalendarEvent{
    self.eventName = manualEntryCalendarEvent.eventName;
    self.activityType = manualEntryCalendarEvent.activityType;
    self.location = manualEntryCalendarEvent.location;
    self.latitude = [manualEntryCalendarEvent.latitude doubleValue];
    self.longitude = [manualEntryCalendarEvent.longitude doubleValue];
    self.allDay = [manualEntryCalendarEvent.allDay boolValue];
    self.endDate = manualEntryCalendarEvent.endDate;
    self.notes = manualEntryCalendarEvent.notes;
    self.startDate = manualEntryCalendarEvent.startDate;
    self.candidateArray = (NSMutableArray *)manualEntryCalendarEvent.candidateName;
    self.manualEntryCalEventId = manualEntryCalendarEvent.objectID;
    self.reminder = manualEntryCalendarEvent.reminder;
    self.reminderMode = manualEntryCalendarEvent.reminderMode;
    [self setEventStartEndTime];
}

- (void)saveEventInLocalDB
{

    TblManualEntryCalendarEvents *tblManualEntryCalendarEvents = nil;
    NSError *error = nil;
    if(self.manualEntryCalEventId){
        tblManualEntryCalendarEvents = (TblManualEntryCalendarEvents *)[[CachingService sharedInstance].managedObjectContext existingObjectWithID:self.manualEntryCalEventId
                                                                                                                    error:&error];
    }else {
        tblManualEntryCalendarEvents = [[ContactDataProvider sharedContactDataProvider] createManualCalendarEvent];
        tblManualEntryCalendarEvents.agentId = [[CachingService sharedInstance] currentUserId];

    }
    
    tblManualEntryCalendarEvents.activityType = [ValueParser parseString:self.activityType];
    tblManualEntryCalendarEvents.allDay = [NSNumber numberWithBool:self.allDay];
    tblManualEntryCalendarEvents.endDate = self.endDate;
    tblManualEntryCalendarEvents.eventName = self.eventName;
    tblManualEntryCalendarEvents.location = self.location;
    tblManualEntryCalendarEvents.latitude = [NSNumber numberWithFloat:self.latitude];
    tblManualEntryCalendarEvents.longitude = [NSNumber numberWithFloat:self.longitude];
    tblManualEntryCalendarEvents.notes = self.notes;
    tblManualEntryCalendarEvents.reminder = self.reminder;
    tblManualEntryCalendarEvents.reminderMode = self.reminderMode;
    tblManualEntryCalendarEvents.startDate = self.startDate;
    tblManualEntryCalendarEvents.sDate = [NSDate dateWithYear:self.startDate.componentsOfDate.year
                                            month:self.startDate.componentsOfDate.month
                                              day:self.startDate.componentsOfDate.day];
    
    tblManualEntryCalendarEvents.eDate = [NSDate dateWithYear:self.endDate.componentsOfDate.year
                                            month:self.endDate.componentsOfDate.month
                                              day:self.endDate.componentsOfDate.day];
    

    tblManualEntryCalendarEvents.candidateName = self.candidateArray;
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    self.manualEntryCalEventId = tblManualEntryCalendarEvents.objectID;
}

- (void)setEventStartEndTime{
    if(self.allDay){
        [self setDateDay:[NSDate dateWithYear:self.startDate.componentsOfDate.year month:self.startDate.componentsOfDate.month day:self.startDate.componentsOfDate.day]];
        [self setDateTimeBegin:[self.startDate dateAtStartOfDay]];
        [self setDateTimeEnd:[self.endDate dateAtEndOfDay]];
    }else if(self.startDate){
        [self setDateDay:[NSDate dateWithYear:self.startDate.componentsOfDate.year month:self.startDate.componentsOfDate.month day:self.startDate.componentsOfDate.day]];
        [self setDateTimeBegin:[NSDate dateWithHour:self.startDate.componentsOfDate.hour min:self.startDate.componentsOfDate.minute]];
        [self setDateTimeEnd:[NSDate dateWithHour:self.endDate.componentsOfDate.hour min:self.endDate.componentsOfDate.minute]];
    }
}

- (NSDate *)getDateDay:(NSDate *)date{
    return [NSDate dateWithYear:date.componentsOfDate.year month:date.componentsOfDate.month day:date.componentsOfDate.day];
}

- (NSDate *)getTimeBeginForMultiEvent:(NSDate*)date{
    if([self.startDate isEqualToDateIgnoringTime:date]){
        return [NSDate dateWithHour:self.startDate.componentsOfDate.hour min:self.startDate.componentsOfDate.minute];
    }else{
        NSDate *startOfDate = [date dateAtStartOfDay];
        return [NSDate dateWithHour:startOfDate.componentsOfDate.hour min:startOfDate.componentsOfDate.minute];
    }
    return nil;
}

- (NSDate *)getTimeEndForMultiEvent:(NSDate*)date{
    if([self.endDate isEqualToDateIgnoringTime:date]){
        return [NSDate dateWithHour:self.endDate.componentsOfDate.hour min:self.endDate.componentsOfDate.minute];
    }else{
        NSDate *endOfDate = [date dateAtEndOfDay];
        return [NSDate dateWithHour:endOfDate.componentsOfDate.hour min:endOfDate.componentsOfDate.minute];
    }
    return nil;
}

- (NSMutableDictionary *)editEvent:(FFEvent *)eventNew dicEvent:(NSMutableDictionary *)dictEvents{

    if(eventNew.activityType != ACTIVITY_EOP &&
       eventNew.activityType != ACTIVITY_INT &&
       eventNew.activityType != ACTIVITY_NAP_TRANING &&
       eventNew.activityType != ACTIVITY_BIRTHDAY){
        [self saveEventInLocalDB];
    }
    if([eventNew.startDate isEqualToDateIgnoringTime:eventNew.endDate]){
        NSDate *dateNew = eventNew.dateDay;
        
        NSMutableArray *arrayNew = [dictEvents objectForKey:dateNew];
        if (!arrayNew) {
            arrayNew = [NSMutableArray new];
            [dictEvents setObject:arrayNew forKey:dateNew];
        }
        [arrayNew addObject:eventNew];
    }else{
        NSDate *date = eventNew.startDate;
        NSDate *currentEndDate = [eventNew.endDate dateByAddingDays:1];
//        eventNew.allDay = TRUE;
        do{
            NSDate *currentDateDay = [eventNew getDateDay:date];
            NSMutableArray *arrayNew = [dictEvents objectForKey:currentDateDay];
            if (!arrayNew) {
                arrayNew = [NSMutableArray new];
                [dictEvents setObject:arrayNew forKey:currentDateDay];
            }
            
            [arrayNew addObject:eventNew];
            date = [date dateByAddingDays:1];
            
        }while (![date isEqualToDateIgnoringTime:currentEndDate]);
    }
    return dictEvents;
}

- (NSMutableDictionary *)deleteEvent:(FFEvent *)event dicEvent:(NSMutableDictionary *)dictEvents{
   
    NSMutableArray *arrayEvents = [dictEvents objectForKey:event.dateDay];
    [arrayEvents removeObject:event];
    if (arrayEvents.count == 0) {
        [dictEvents removeObjectForKey:event.dateDay];
    }
    return dictEvents;
}

- (NSMutableDictionary *)deleteMultipleEvent:(FFEvent *)event dicEvent:(NSMutableDictionary *)dictEvents oldStartDate:(NSDate *)oldStartDate oldEndDate:(NSDate *)oldEndDate{

    NSDate *date = oldStartDate;
    NSDate *currentEndDate = [oldEndDate dateByAddingDays:1];
    
    do{
        NSDate *currentDateDay = [event getDateDay:date];
        NSMutableArray *arrayEvents = [dictEvents objectForKey:currentDateDay];
        [arrayEvents removeObject:event];
        if (arrayEvents.count == 0) {
            [dictEvents removeObjectForKey:currentDateDay];
        }
        
        date = [date dateByAddingDays:1];
    }while (![date isEqualToDateIgnoringTime:currentEndDate]);
    return dictEvents;
}
- (void)setiCalendarEvent:(NSDictionary *)dict{
    self.activityType = ACTIVITY_ICALENDAR;
    self.location = dict[@"address"];
    self.allDay = YES;
    self.myEvent=dict[@"MyEvent"];
    self.eventName=dict[@"subject"];
    
    NSDateFormatter *format = [[NSDateFormatter alloc]init];
    [format setDateFormat:kServerDateFormatte];
    //    self.eventDate = [format dateFromString:dict[@"startTime"]];
    
    //    NSDate *eventEndTime = [DateUtils stringToDate:dict[@"endTime"] dateFormat:kServerDateFormatte];
    //    NSDate *eventStartTime = [DateUtils stringToDate:dict[@"startTime"] dateFormat:kServerDateFormatte];
    
    //    self.endDate = [NSDate dateWithYear:self.eventDate.year month:self.eventDate.month day:self.eventDate.day hour:eventEndTime.hour min:eventEndTime.minute];
    //    self.startDate = [NSDate dateWithYear:self.eventDate.year month:self.eventDate.month day:self.eventDate.day hour:eventStartTime.hour min:eventStartTime.minute];
    self.endDate = [DateUtils stringToDate:dict[@"endTime"] dateFormat:kServerDateFormatte];
    self.startDate = [DateUtils stringToDate:dict[@"startTime"] dateFormat:kServerDateFormatte];
    
    self.sEndDate = dict[@"endTime"];
    self.sStartDate = dict[@"startTime"];
    
    [self setEventStartEndTime];
}

- (void)setEOPEvent:(TblEOP *)eop{
    self.eventName = eop.eventName;
    self.activityType = ACTIVITY_EOP;
    self.location = eop.location;
    self.allDay = eop.calendarEvent.allDayValue;
    self.eventCode = [eop.eventCode integerValue];
    self.topicPath = eop.topicPath;
    self.profilePath = eop.profilePath;
    self.speaker = eop.speaker;
    self.profile = eop.profilePath;
    self.eopDesc = eop.eopDescription;
    self.organizer = [eop.organizer stringValue];
    self.topic = eop.topic;
    if (eop.openToRegistration)
        self.openToRegistration = eop.openToRegistration;
    else
        self.openToRegistration = @"N";
    self.totalCandidate = [eop.registeredCount stringValue];
    
    self.eventDate = eop.eventDate;
    NSDate *eventEndTime = [DateUtils stringToDate:eop.endTime dateFormat:kServerDateFormatte];
    NSDate *eventStartTime = [DateUtils stringToDate:eop.startTime dateFormat:kServerDateFormatte];
    
    self.endDate = [NSDate dateWithYear:self.eventDate.year month:self.eventDate.month day:self.eventDate.day hour:eventEndTime.hour min:eventEndTime.minute];
    self.startDate = [NSDate dateWithYear:self.eventDate.year month:self.eventDate.month day:self.eventDate.day hour:eventStartTime.hour min:eventStartTime.minute];
    self.eopId = eop.objectID;
    
    [self setEventStartEndTime];
}

- (void)setInterviewEvent:(TblInterview *)interview{
    self.eventName = interview.interviewType;
    self.activityType = ACTIVITY_INT;
    self.location = interview.location;
    self.allDay = interview.calendarEvent.allDayValue;
    self.interviewCode = [interview.interviewCode integerValue];
    self.estimatedCondidates = [interview.estimatedCondidates integerValue];
    self.registeredCount = [interview.registeredCount integerValue];
    self.interviewDescription = interview.buName;
    self.interviewMaterial = interview.interviewMaterial;
    self.totalCandidate = [interview.registeredCount stringValue];
    
    self.eventDate = interview.interviewDate;
    NSDate *eventEndTime = [DateUtils stringToDate:interview.endTime dateFormat:kServerDateFormatte];
    NSDate *eventStartTime = [DateUtils stringToDate:interview.startTime dateFormat:kServerDateFormatte];
    
    self.endDate = [NSDate dateWithYear:self.eventDate.year month:self.eventDate.month day:self.eventDate.day hour:eventEndTime.hour min:eventEndTime.minute];
    self.startDate = [NSDate dateWithYear:self.eventDate.year month:self.eventDate.month day:self.eventDate.day hour:eventStartTime.hour min:eventStartTime.minute];
    self.interviewId = interview.objectID;
    
    [self setEventStartEndTime];
}

- (void)setTrainingEvent:(TblTraninigEvent *)trainingEvent
{
    self.eventName = trainingEvent.courseName;
    self.curriculamCode = trainingEvent.curriculamCode;
    self.courseCode = trainingEvent.courseCode;
    self.courseType = trainingEvent.courseType;
    self.activityType = ACTIVITY_NAP_TRANING;
    self.allDay = YES;
    self.eventDate = trainingEvent.startDate;
    self.startDate = self.endDate = [NSDate dateWithYear:self.eventDate.year month:self.eventDate.month day:self.eventDate.day hour:self.eventDate.hour min:self.eventDate.minute];
    self.trainingId = trainingEvent.objectID;
    
    [self setEventStartEndTime];
}

- (void)setHolidayEvent:(TblHoliday *)holiday{
    self.eventName = holiday.holidayName;
    self.activityType = ACTIVITY_HOLIDAY;
    self.allDay = holiday.calendarEvent.allDayValue;
    self.endDate = [DateUtils stringToDate:holiday.endDate dateFormat:kServerDateFormatte];
    self.startDate = [DateUtils stringToDate:holiday.startDate dateFormat:kServerDateFormatte];
    self.holidayId = holiday.objectID;
    
    [self setEventStartEndTime];
}

@end
