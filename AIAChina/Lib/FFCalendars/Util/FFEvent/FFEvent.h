//
//  FFEvent.h
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/16/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import <Foundation/Foundation.h>
#import "TblEOP.h"
#import "TblCalendarEvent.h"
#import "TblInterview.h"
#import "TblHoliday.h"
#import "TblManualEntryCalendarEvents.h"
#import "TblTraninigEvent.h"

@interface FFEvent : NSObject

@property (nonatomic, strong) NSString *agentCode;
@property (nonatomic, strong) NSMutableArray *candidateArray;
@property (nonatomic, strong) NSString *eventName;
@property (nonatomic, strong) NSString *activityType;
@property (nonatomic) BOOL allDay;
@property (nonatomic, strong) NSString *notes;
@property (nonatomic, strong) NSString *location;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic, strong) NSString *reminder;
@property (nonatomic, strong) NSString *reminderMode;
@property (nonatomic, strong) NSDate *eventDate;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic,strong)NSString *myEvent;
@property (nonatomic, strong) NSString *sStartDate;
@property (nonatomic, strong) NSString *sEndDate;

@property (nonatomic) int eventCode;
@property (nonatomic) int interviewCode;

@property (nonatomic, strong) NSString *speaker;
@property (nonatomic, strong) NSString *profile;
@property (nonatomic, strong) NSString *eopDesc;
@property (nonatomic, strong) NSString *organizer;
@property (nonatomic, strong) NSString *topic;
@property (nonatomic, strong) NSString *topicPath;
@property (nonatomic, strong) NSString *profilePath;
@property (nonatomic, strong) NSString *totalCandidate;
@property (nonatomic, strong) NSString *openToRegistration;

@property (nonatomic, strong) NSString *interviewDescription;
@property (nonatomic, strong) NSString *interviewMaterial;
@property (nonatomic) int estimatedCondidates;
@property (nonatomic) int registeredCount;

@property (nonatomic, strong) NSString *curriculamCode;
@property (nonatomic, strong) NSString *courseCode;
@property (nonatomic, strong) NSString *courseType;

@property (nonatomic, strong) NSDate *dateDay;
@property (nonatomic, strong) NSDate *dateTimeBegin;
@property (nonatomic, strong) NSDate *dateTimeEnd;

@property (nonatomic, strong) TblManualEntryCalendarEventsID *manualEntryCalEventId;
@property (nonatomic, strong) TblEOPID *eopId;
@property (nonatomic, strong) TblInterviewID *interviewId;
@property (nonatomic, strong) TblHolidayID *holidayId;
@property (nonatomic, strong) TblTraninigEventID *trainingId;

- (void)createEvent:(NSDictionary *)eventDic;
- (void)setiCalendarEvent:(NSDictionary *)dict;
- (void)setCalendarEvent:(TblManualEntryCalendarEvents *)calendarEvent;
- (void)setEOPEvent:(TblEOP *)eop;
- (void)setInterviewEvent:(TblInterview *)interview;
- (void)setHolidayEvent:(TblHoliday *)holiday;
- (void)setTrainingEvent:(TblTraninigEvent *)trainingEvent;

- (NSDate *)getDateDay:(NSDate *)date;
- (NSDate *)getTimeBeginForMultiEvent:(NSDate*)date;
- (NSDate *)getTimeEndForMultiEvent:(NSDate*)date;
- (void)saveEventInLocalDB;

- (NSMutableDictionary *)editEvent:(FFEvent *)eventNew dicEvent:(NSMutableDictionary *)dictEvents;
- (NSMutableDictionary *)deleteEvent:(FFEvent *)event dicEvent:(NSMutableDictionary *)dictEvents;
- (NSMutableDictionary *)deleteMultipleEvent:(FFEvent *)event dicEvent:(NSMutableDictionary *)dictEvents oldStartDate:(NSDate *)oldStartDate oldEndDate:(NSDate *)oldEndDate;

@end
