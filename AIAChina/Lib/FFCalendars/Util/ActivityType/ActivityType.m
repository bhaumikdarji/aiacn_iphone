//
//  ActivityType.m
//  Calendar
//
//  Created by Quix Creations on 26/05/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "ActivityType.h"
#import "FFConstants.h"
#import "Constants.h"

@implementation ActivityType

+ (ActivityType *)sharedActivity{
    static ActivityType *sharedObj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedObj = [[ActivityType alloc] init];
        [sharedObj setActivityDic];
    });
    return sharedObj;
}

- (void)setActivityDic{
    if(self.activityTypeDic)
        return;
    
    self.activityTypeDic = [[NSMutableDictionary alloc] init];
    [self.activityTypeDic setObject:@[ACTIVITY_HOLIDAY] forKey:ACTIVITY_HOLIDAY];
    [self.activityTypeDic setObject:@[ACTIVITY_NAP_TRANING] forKey:ACTIVITY_NAP_TRANING];
    [self.activityTypeDic setObject:@[ACTIVITY_BIRTHDAY] forKey:ACTIVITY_BIRTHDAY];
    [self.activityTypeDic setObject:@[ACTIVITY_ALE] forKey:ACTIVITY_ALE];
    [self.activityTypeDic setObject:@[ACTIVITY_EOP] forKey:ACTIVITY_EOP];
    [self.activityTypeDic setObject:@[ACTIVITY_INT] forKey:ACTIVITY_INT];
    [self.activityTypeDic setObject:@[ACTIVITY_APPOINTMENT] forKey:ACTIVITY_APPOINTMENT];
    [self.activityTypeDic setObject:@[ACTIVITY_OTHER] forKey:ACTIVITY_OTHER];
    [self.activityTypeDic setObject:@[ACTIVITY_TELEPHONE] forKey:ACTIVITY_TELEPHONE];
    [self.activityTypeDic setObject:@[ACTIVITY_PARTICIPATION] forKey:ACTIVITY_PARTICIPATION];
}

- (NSArray *)getAllActitvityType{
    [self setActivityDic];
    NSMutableArray *activityTypeArray = [[NSMutableArray alloc] init];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_HOLIDAY]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_NAP_TRANING]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_BIRTHDAY]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_ALE]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_EOP]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_INT]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_APPOINTMENT]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_OTHER]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_TELEPHONE]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_PARTICIPATION]];
    return [activityTypeArray mutableCopy];
}

- (NSArray *)getAllManualActitvityType{
    [self setActivityDic];
    NSMutableArray *activityTypeArray = [[NSMutableArray alloc] init];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_APPOINTMENT]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_OTHER]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_TELEPHONE]];
    [activityTypeArray addObjectsFromArray:[self.activityTypeDic valueForKey:ACTIVITY_PARTICIPATION]];
    return [activityTypeArray mutableCopy];
}

- (NSArray *)getAllActivityMode{
    [self setActivityDic];
    return [self.activityTypeDic allKeys];
}

- (NSString *)getActivityTypeToMode:(NSString *)activityType{
    if([activityType isEqualToString:ACTIVITY_HOLIDAY]){
        return ACTIVITY_HOLIDAY;
    }else if([activityType isEqualToString:ACTIVITY_NAP_TRANING]){
        return ACTIVITY_NAP_TRANING;
    }else if([activityType isEqualToString:ACTIVITY_BIRTHDAY]){
        return ACTIVITY_BIRTHDAY;
    }else if([activityType isEqualToString:ACTIVITY_ALE]){
        return ACTIVITY_ALE;
    }else if([activityType isEqualToString:ACTIVITY_EOP]){
        return ACTIVITY_EOP;
    }else if([activityType isEqualToString:ACTIVITY_INT]){
        return ACTIVITY_INT;
    }else if([activityType isEqualToString:ACTIVITY_ICALENDAR]){
        return ACTIVITY_ICALENDAR;
    }
    return @"";
}

- (NSArray *)getActivityModeToType:(NSString *)activityMode{
    return [self.activityTypeDic objectForKey:activityMode];
}

- (UIColor *)setBgColorOnActivityType:(NSString *)activityType{
    if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_HOLIDAY])
        return [UIColor colorWithRed:254.0/255.0 green:232.0/255.0 blue:52.0/255.0 alpha:1.0];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_NAP_TRANING])
        return [UIColor colorWithRed:163.0/255.0 green:36.0/255.0 blue:119.0/255.0 alpha:1.0];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_BIRTHDAY])
        return [UIColor colorWithRed:10.0/255.0 green:40.0/255.0 blue:170.0/255.0 alpha:1.0];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_ALE])
        return [UIColor colorWithRed:215.0/255.0 green:43.0/255.0 blue:79.0/255.0 alpha:1.0];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_EOP])
        return [UIColor colorWithRed:112.0/255.0 green:179.0/255.0 blue:70.0/255.0 alpha:1.0];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_INT])
        return [UIColor colorWithRed:241.0/255.0 green:150.0/255.0 blue:49.0/255.0 alpha:1.0];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_ICALENDAR])
        return [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:0.0/255.0 alpha:1.0];
    else
        return [UIColor colorWithRed:255.0/255.0 green:0.0/255.0 blue:228.0/255.0 alpha:1.0];
}

- (UIColor *)setTextColorOnActivityType:(NSString *)activityType{
    if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_HOLIDAY])
        return [UIColor blackColor];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_NAP_TRANING])
        return [UIColor whiteColor];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_BIRTHDAY])
        return [UIColor whiteColor];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_ALE])
        return [UIColor whiteColor];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_EOP])
        return [UIColor whiteColor];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_INT])
        return [UIColor whiteColor];
    else if([[self getActivityTypeToMode:activityType] isEqualToString:ACTIVITY_ICALENDAR])
        return [UIColor blackColor];
    else
        return [UIColor whiteColor];
}

- (NSPredicate *)setSelectedModePredicate{
    NSPredicate *predicate = nil;
//    if([[[ActivityType sharedActivity] getActivityTypeToMode:self.selectedMode] isEqualToString:ACTIVITY_OPPORTUNITY])
//        predicate = [NSPredicate predicateWithFormat:@"activityType == %@",self.selectedMode];
//    else
        predicate = [NSPredicate predicateWithFormat:@"activityType IN %@",[self getActivityModeToType:self.selectedMode]];
    return predicate;
}

@end
