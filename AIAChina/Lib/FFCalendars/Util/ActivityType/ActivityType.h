//
//  ActivityType.h
//  Calendar
//
//  Created by Quix Creations on 26/05/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FFConstants.h"

@interface ActivityType : NSObject

@property (nonatomic, strong) NSMutableDictionary *activityTypeDic;
@property (nonatomic, strong) NSString *selectedMode;

+ (ActivityType *)sharedActivity;
- (void)setActivityDic;
- (NSArray *)getAllActitvityType;
- (NSArray *)getAllManualActitvityType;
- (NSArray *)getAllActivityMode;
- (NSString *)getActivityTypeToMode:(NSString *)activityType;
- (NSArray *)getActivityModeToType:(NSString *)activityMode;
- (UIColor *)setBgColorOnActivityType:(NSString *)activityType;
- (UIColor *)setTextColorOnActivityType:(NSString *)activityType;
- (NSPredicate *)setSelectedModePredicate;

@end
