//
//  ICalendarDetailViewController.h
//  AIAChina
//
//  Created by AIA on 03/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface ICalendarDetailViewController : AIAChinaViewController<UITableViewDataSource,UITableViewDelegate>


@property (nonatomic, strong) FFEvent *event;
@property (nonatomic) int quata;
@property (copy, nonatomic) void(^dismissPopOverBlock)();
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSArray *eventArr;


@end
