//
//  ICalendarDetailViewController.m
//  AIAChina
//
//  Created by AIA on 03/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "ICalendarDetailViewController.h"
#import "CMSServiceManager.h"
#import "ContactEventModel.h"
#import "CalendarCell.h"

@interface ICalendarDetailViewController ()
@property (nonatomic, retain) ContactEventModel *eventModel;
@end

@implementation ICalendarDetailViewController


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"iCalendar", @"");
//    self.eventModel = [ContactEventModel sharedInstance];
    
    [self loadData];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
        self.preferredContentSize=CGSizeMake(340, 420);
    else
        self.presentingViewController.presentedViewController.preferredContentSize = CGSizeMake(340, 420);
}

- (void)loadData{

    NSString *strdate = [NSString stringWithFormat:@"%@",self.event.startDate];
    NSString *newString = [strdate substringToIndex:10];
    self.eventArr = [DbUtils fetchAllObject:@"ICalendarEvents"
                                      andPredict:[NSPredicate predicateWithFormat:@"startTime contains[cd] %@ AND agentCode == %@",newString,[CachingService sharedInstance].currentUserIdWithZero]
                               andSortDescriptor:nil
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    NSLog(@"event array %@",self.eventArr);
    
    [self.tblView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.eventArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CalendarCell *cell = (CalendarCell *)[tableView dequeueReusableCellWithIdentifier:@"iCalendarCell"];
    if (!cell) {
        cell=[[CalendarCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"iCalendarCell"];
    }
    NSLog(@"%@",[self.eventArr objectAtIndex:indexPath.row]);
          
//    [cell.lblActivity setText:self.event.activityType];
    NSLog(@"%@",self.event.startDate);
    NSDictionary *datadic = [self.eventArr objectAtIndex:indexPath.row];
    
    NSString *startime=[NSString stringWithFormat:@"%@",[datadic valueForKey:@"startTime"]];
    startime=[[startime componentsSeparatedByString:@" "]objectAtIndex:1];
    startime=[NSString stringWithFormat:@"%@:%@",[[startime componentsSeparatedByString:@":"] objectAtIndex:0],[[startime componentsSeparatedByString:@":"] objectAtIndex:1]];
    
    NSString *endtime=[NSString stringWithFormat:@"%@",[datadic valueForKey:@"endTime"]];
    endtime=[[endtime componentsSeparatedByString:@" "]objectAtIndex:1];
    endtime=[NSString stringWithFormat:@"%@:%@",[[endtime componentsSeparatedByString:@":"] objectAtIndex:0],[[endtime componentsSeparatedByString:@":"] objectAtIndex:1]];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:[datadic valueForKey:@"typeName"] forKey:@"eventType"];
    [dict setValue:[NSString stringWithFormat:@"%@-%@",startime,endtime] forKey:@"eventTime"];
    [dict setValue:[datadic valueForKey:@"subject"] forKey:@"eventName"];
    [dict setValue:[datadic valueForKey:@"address"] forKey:@"eventLocation"];
    [cell loadiCalendarCell:dict];
    dict = nil;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

@end
