//
//  FFEventDetailView.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/19/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFEventDetailView.h"
#import "FFImportantFilesForCalendar.h"
#import "ICalendarDetailViewController.h"

@interface FFEventDetailView ()
@property (nonatomic, strong) FFEvent *event;
@property (nonatomic, strong) UILabel *labelCustomerName;
@property (nonatomic, strong) UILabel *labelDate;
@property (nonatomic, strong) UILabel *labelHours;
@property (nonatomic, strong) UILabel *labelSeprator;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *labelTimeRemaining;
@property (nonatomic, strong) UILabel *labelLocation;
@property (nonatomic, strong) UILabel *labelNote;
@property (nonatomic, strong) UINavigationController *navConroller;

@end

@implementation FFEventDetailView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Synthesize

@synthesize protocol;
@synthesize event;
@synthesize buttonEditPopover;
@synthesize labelCustomerName;
@synthesize labelDate;
@synthesize labelHours;
@synthesize labelSeprator;
@synthesize imageView;
@synthesize labelTimeRemaining;
@synthesize labelLocation;
@synthesize labelNote;

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame event:(FFEvent *)_event
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        event = _event;
        
        [self.layer setBorderColor:[UIColor lightGrayCustom].CGColor];
        [self.layer setBorderWidth:2.];
        
        [self addViewInSubView:frame.size];
    }
    return self;
}


- (id)initWithEvent:(FFEvent *)eventInit {
    
    CGSize size = CGSizeMake(320., 70.);
    
    self = [super initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    
    if (self) {
        
        event = eventInit;
        
        [self.layer setBorderColor:[UIColor lightGrayCustom].CGColor];
        [self.layer setBorderWidth:2.];
        [self addViewInSubView:size];
    }
    return self;
}

- (void)addViewInSubView:(CGSize)size{
    if ([self.event.activityType isEqualToString:ACTIVITY_ICALENDAR]) {
        UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
        self.navConroller = [storyboard instantiateViewControllerWithIdentifier:@"iCalendarDetailNavController"];
        ICalendarDetailViewController *iCalendarDetailViewCtrl = self.navConroller.viewControllers[0];
        iCalendarDetailViewCtrl.event = self.event;
        [self.navConroller.view setFrame:CGRectMake(0, 0, size.width, size.width)];
        [self addSubview:self.navConroller.view];
        
        
    } else {
        [self addButtonEditPopoverWithViewSize:size];
        [self addLabelCustomerNameWithViewSize:size];
        [self addLabelDateWithViewSize:size];
        if(!self.event.allDay)
            [self addLabelHoursWithViewSize:size];
        [self addSeprator1WithViewSize:size];
        [self addIconInfo:size];
        [self addRemainingTimeWithViewSize:size];
        [self addSeprator2WithViewSize:size];
        [self addIconLocation:size];
        [self addLocatonWithViewSize:size];
        [self addSeprator3WithViewSize:size];
        [self addIconNote:size];
        [self addNoteWithViewSize:size];
        [self addButtonDeletePopoverWithViewSize:size];
    }
}

#pragma mark - Button Actions

- (IBAction)buttonEditPopoverAction:(id)sender {
    
    if ([protocol respondsToSelector:@selector(showEditViewWithEvent:)]) {
        [protocol showEditViewWithEvent:event];
    }
}

- (IBAction)buttonDeletePopoverAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"")
                                                    message:NSLocalizedString(@"Are you sure you want to delete?", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel",@"")
                                          otherButtonTitles:NSLocalizedString(@"Ok", @""), nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:DELETE_EVENT_NOTIFICATION
//                                                            object:event];
        if([event.startDate isEqualToDateIgnoringTime:event.endDate]){
            if([protocol respondsToSelector:@selector(deleteEvent:)]){
                [protocol deleteEvent:event];
            }
        }else{
            if([protocol respondsToSelector:@selector(deleteEvent:startDate:andEndDate:)]){
                [protocol deleteEvent:event startDate:event.startDate andEndDate:event.endDate];
            }
        }
        [ApplicationFunction deleteCalManualEvent:event];
    }
}


#pragma mark - Add Subviews

- (void)addButtonEditPopoverWithViewSize:(CGSize)sizeView {
    
    CGFloat width = 50;
    CGFloat height = BUTTON_HEIGHT;
    CGFloat gap = 30;
    
    buttonEditPopover = [[UIButton alloc] initWithFrame:CGRectMake(sizeView.width-width-gap, 20, width, height)];
    [buttonEditPopover setImage:[UIImage imageNamed:@"ic_edit_event"] forState:UIControlStateNormal];
    [buttonEditPopover addTarget:self action:@selector(buttonEditPopoverAction:) forControlEvents:UIControlEventTouchUpInside]; //amar
    [buttonEditPopover setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    
    [self addSubview:buttonEditPopover];
}

- (void)addLabelCustomerNameWithViewSize:(CGSize)sizeView {
    
    CGFloat gap = 30;
    
    labelCustomerName = [[UILabel alloc] initWithFrame:CGRectMake(gap, buttonEditPopover.frame.origin.y, sizeView.width-3*gap-buttonEditPopover.frame.size.width, buttonEditPopover.frame.size.height + 10)];
    if([event.activityType isEqualToString:ACTIVITY_INT]){
        [labelCustomerName setText:NSLocalizedString(event.eventName,@"")];
    }else{
        [labelCustomerName setText:event.eventName];
    }
    
    [labelCustomerName setFont:[UIFont boldSystemFontOfSize:labelCustomerName.font.pointSize]];
    labelCustomerName.textColor = [UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1.0];
    [self addSubview:labelCustomerName];
}

- (void)addLabelDateWithViewSize:(CGSize)sizeView {
    
    CGFloat gap = 30;
    
    labelDate = [[UILabel alloc] initWithFrame:CGRectMake(gap, labelCustomerName.frame.origin.y+labelCustomerName.frame.size.height - 10, 170., labelCustomerName.frame.size.height)];
    [labelDate setText:[NSDate stringDayOfDate:event.dateDay]];
    [labelDate setTextColor:[UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0]];
    [labelDate setFont:[UIFont systemFontOfSize:labelCustomerName.font.pointSize-3]];
    
    [self addSubview:labelDate];
}

- (void)addLabelHoursWithViewSize:(CGSize)sizeView {
    
    CGFloat gap = 30;
    CGFloat x = labelDate.frame.origin.x + labelDate.frame.size.width+gap;
    
    labelHours = [[UILabel alloc] initWithFrame:CGRectMake(x, labelDate.frame.origin.y, sizeView.width-x-gap, labelDate.frame.size.height)];
    [labelHours setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    [labelHours setText:[NSString stringWithFormat:@"%@ to %@", [NSDate stringTimeOfDate12:event.dateTimeBegin], [NSDate stringTimeOfDate12:event.dateTimeEnd]]];
    [labelHours setTextAlignment:NSTextAlignmentRight];
    [labelHours setTextColor:[UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0]];
    [labelHours setFont:labelDate.font];
    
    [self addSubview:labelHours];
}

- (void)addSeprator1WithViewSize:(CGSize)sizeView{

    CGFloat gap = 30;
    labelSeprator = [[UILabel alloc] initWithFrame:CGRectMake(gap, labelDate.frame.origin.y+labelDate.frame.size.height, sizeView.width - gap * 2, 1)];
    labelSeprator.backgroundColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0];
    [self addSubview:labelSeprator];
}

- (void)addIconInfo:(CGSize)sizeView{
    CGFloat gap = 35;
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(gap, labelSeprator.frame.origin.y + labelSeprator.frame.size.height, 20, labelCustomerName.frame.size.height)];
    imageView.image = [UIImage imageNamed:@"ic_info"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageView];
}

- (void)addRemainingTimeWithViewSize:(CGSize)sizeView{
    
    CGFloat gap = 30;
    CGFloat x = imageView.frame.origin.x + imageView.frame.size.width+gap;
    
    labelTimeRemaining = [[UILabel alloc] initWithFrame:CGRectMake(x, labelSeprator.frame.origin.y, sizeView.width-x-gap, labelCustomerName.frame.size.height)];
    [labelTimeRemaining setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    [labelTimeRemaining setText:[NSString stringWithFormat:@"%@ >", [NSDate stringTimeOfDate:event.dateTimeBegin]]];
    [labelTimeRemaining setTextAlignment:NSTextAlignmentRight];
    [labelTimeRemaining setTextColor:[UIColor colorWithRed:175.0/255.0 green:175.0/255.0 blue:175.0/255.0 alpha:1.0]];
    [labelTimeRemaining setFont:labelDate.font];
    
//    [self addSubview:labelTimeRemaining];
    
}

- (void)addSeprator2WithViewSize:(CGSize)sizeView{
    
    CGFloat gap = 30;
    labelSeprator = [[UILabel alloc] initWithFrame:CGRectMake(gap, labelTimeRemaining.frame.origin.y+labelTimeRemaining.frame.size.height, sizeView.width - gap * 2, 1)];
    labelSeprator.backgroundColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0];
    [self addSubview:labelSeprator];
}

- (void)addIconLocation:(CGSize)sizeView{
    CGFloat gap = 35;
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(gap, labelSeprator.frame.origin.y + labelSeprator.frame.size.height, 20, labelCustomerName.frame.size.height)];
    imageView.image = [UIImage imageNamed:@"ic_location"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageView];
}

- (void)addLocatonWithViewSize:(CGSize)sizeView{
    
    CGFloat gap = 30;
    CGFloat x = imageView.frame.origin.x + imageView.frame.size.width + gap;
    
    labelLocation = [[UILabel alloc] initWithFrame:CGRectMake(x,
                                                              labelSeprator.frame.origin.y + labelSeprator.frame.size.height,
                                                              sizeView.width - x - gap,
                                                              labelCustomerName.frame.size.height)];
    [labelLocation setText:event.location];
    [labelLocation setTextColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    [labelLocation setFont:[UIFont systemFontOfSize:labelCustomerName.font.pointSize-3]];
    
    [self addSubview:labelLocation];
}

- (void)addSeprator3WithViewSize:(CGSize)sizeView{
    
    CGFloat gap = 30;
    labelSeprator = [[UILabel alloc] initWithFrame:CGRectMake(gap, labelLocation.frame.origin.y+labelLocation.frame.size.height, sizeView.width - gap * 2, 1)];
    labelSeprator.backgroundColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0];
    [self addSubview:labelSeprator];
}

- (void)addIconNote:(CGSize)sizeView{
    CGFloat gap = 35;
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(gap, labelSeprator.frame.origin.y + labelSeprator.frame.size.height, 20, labelCustomerName.frame.size.height)];
    imageView.image = [UIImage imageNamed:@"ic_edit_note"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageView];
}

- (void)addNoteWithViewSize:(CGSize)sizeView{
    CGFloat gap = 30;
    CGFloat x = imageView.frame.origin.x + imageView.frame.size.width + gap;
    
    labelNote = [[UILabel alloc] initWithFrame:CGRectMake(x,
                                                              labelSeprator.frame.origin.y + labelSeprator.frame.size.height,
                                                              sizeView.width - x - gap,
                                                              labelCustomerName.frame.size.height)];
    [labelNote setText:event.notes];
    [labelNote setTextColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    [labelNote setFont:[UIFont systemFontOfSize:labelCustomerName.font.pointSize-3]];
    
    [self addSubview:labelNote];
}

- (void)addButtonDeletePopoverWithViewSize:(CGSize)sizeView {
    
    CGFloat width = 50;
    CGFloat height = BUTTON_HEIGHT;
    CGFloat gap = 30;
    
    buttonEditPopover = [[UIButton alloc] initWithFrame:CGRectMake(sizeView.width-width-gap, labelNote.frame.origin.y + labelNote.frame.size.height-10, width, height)];
    [buttonEditPopover setImage:[UIImage imageNamed:@"ic_delete_event"] forState:UIControlStateNormal];
    [buttonEditPopover addTarget:self action:@selector(buttonDeletePopoverAction:) forControlEvents:UIControlEventTouchUpInside]; // amar
    [buttonEditPopover setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    
    [self addSubview:buttonEditPopover];
}

@end
