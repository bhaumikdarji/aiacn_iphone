//
//  EOPDetailViewController.h
//  AIAChina
//
//  Created by AIA on 03/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "FFCalendar.h"

@interface EOPDetailViewController : AIAChinaViewController
@property (weak, nonatomic) IBOutlet UILabel *lblOrganizer;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDateText;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTimeText;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTimeText;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationText;
@property (weak, nonatomic) IBOutlet UITextView *txtLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblSpeakerText;
@property (weak, nonatomic) IBOutlet UILabel *lblSpeaker;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnEventMaterials;
@property (weak, nonatomic) IBOutlet UIButton *btnAfterEventMaterials;
@property (weak, nonatomic) IBOutlet UILabel *lblTopicText;
@property (weak, nonatomic) IBOutlet UILabel *lblTopic;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalCandidatesText;
@property (weak, nonatomic) IBOutlet UIButton *btnTotalCandidate;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property (nonatomic, strong) FFEvent *event;
@property (copy, nonatomic) void(^dismissPopOverBlock)();

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *EMHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *AEMHeightConstraint;

- (IBAction)onBtnRegisterClick:(id)sender;
- (IBAction)onBtnEventMaterialsClick:(id)sender;
- (IBAction)onBtnAfterEventMaterialsClick:(id)sender;
- (IBAction)onBtnTotalCandidateClick:(id)sender;
- (IBAction)OnBtnLocationClick:(id)sender;
- (IBAction)onBtnShareClick:(id)sender;


@end
