//
//  EOPDetailViewController.m
//  AIAChina
//
//  Created by AIA on 03/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EOPDetailViewController.h"
#import "ContactEventModel.h"
#import "InterviewRegistrationViewController.h"
#import "PdfDownloadObject.h"
#import "EOPRegCandAndAttandViewController.h"

@interface EOPDetailViewController ()<UIWebViewDelegate>
@property (nonatomic,retain) ContactEventModel *contactEventModel;
@property (nonatomic, retain) UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (nonatomic, retain) UIButton *closeButton;
@end

@implementation EOPDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"EOP Details", @"");
    self.contactEventModel = [ContactEventModel sharedInstance];
}

- (void)showWebView
{
    self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(10.0f, 20.0f, 1010, 740)];
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.frame = CGRectMake(986.0f, 23.0f, 32.0f, 32.0f);
    [self.closeButton setBackgroundImage:[UIImage imageNamed:@"close_red.png"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
    [[self.webView layer] setCornerRadius:10];
    [self.webView setClipsToBounds:YES];
    self.webView.delegate = self;
}
- (void)closeWebView
{
    [self.webView removeFromSuperview];
    [self.closeButton removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self loadData];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
        self.preferredContentSize=CGSizeMake(350, 475);
    else
        self.presentingViewController.presentedViewController.preferredContentSize = CGSizeMake(350, 525);
}

- (void)loadData{
    self.lblDate.text = [NSDate dateToString:self.event.dateDay andFormate:kDisplayDateFormatte];
    self.lblStartTime.text = [NSDate dateToString:self.event.startDate andFormate:@"HH:mma"];
    self.lblEndTime.text = [NSDate dateToString:self.event.endDate andFormate:@"HH:mma"];
    self.txtLocation.text = self.event.location;
    self.lblSpeaker.text = self.event.speaker;
    self.lblOrganizer.text = self.event.organizer;
    self.txtDescription.text = self.event.eopDesc;
    self.lblTopic.text = self.event.topic;
    if ([self.event.openToRegistration isEqualToString:@"Y"])
    {
        self.btnShare.hidden = NO;
    } else {
        self.btnShare.hidden = YES;
    }
    [self getTotalRegCandidateCountApi];
    
    if(isEmptyString(self.event.profilePath))
        self.EMHeightConstraint.constant = 0;
    else{
        [self.btnEventMaterials setAttributedTitle:[Utility setUnderline:[[self.event.profilePath componentsSeparatedByString:@"/"] lastObject]
                                                           withTextColor:self.btnEventMaterials.currentTitleColor]
                                          forState:UIControlStateNormal];
        self.EMHeightConstraint.constant = 36;
    }
    
    if(isEmptyString(self.event.topicPath))
        self.AEMHeightConstraint.constant = 0;
    else{
        [self.btnAfterEventMaterials setAttributedTitle:[Utility setUnderline:[[self.event.topicPath componentsSeparatedByString:@"/"] lastObject]
                                                                withTextColor:self.btnAfterEventMaterials.currentTitleColor]
                                               forState:UIControlStateNormal];
        self.AEMHeightConstraint.constant = 36;
    }
    NSDate *eventDate = [NSDate dateWithYear:self.event.dateDay.year
                                       month:self.event.dateDay.month
                                         day:self.event.dateDay.day
                                        hour:self.event.startDate.hour
                                         min:self.event.startDate.minute];
    [self.btnRegister setTitle:NSLocalizedString(@"注册", @"") forState:UIControlStateNormal];

    
    if([eventDate isEarlierThanDate:[NSDate date]])
        self.btnRegister.hidden = TRUE;
    else
        self.btnRegister.hidden = FALSE;
}

- (void)getTotalRegCandidateCountApi{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:TRUE];
    [self.contactEventModel getEOPRegisteredCandidateCount:[NSString stringWithFormat:@"%d",self.event.eventCode] agentID:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        for (NSArray *response in responseObject) {
            self.event.totalCandidate = [NSString stringWithFormat:@"%d",[ValueParser parseInt:[response valueForKey:@"registeredCount"]]];
            [self.btnTotalCandidate setAttributedTitle:[Utility setUnderline:isEmptyString(self.event.totalCandidate) ? @"" : self.event.totalCandidate
                                                               withTextColor:self.btnTotalCandidate.currentTitleColor]
                                              forState:UIControlStateNormal];
        }
    } failure:^(id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        [self.btnTotalCandidate setAttributedTitle:[Utility setUnderline:isEmptyString(self.event.totalCandidate) ? @"" : self.event.totalCandidate
                                                           withTextColor:self.btnTotalCandidate.currentTitleColor]
                                          forState:UIControlStateNormal];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"EOPDetailToRegistration"]){
        InterviewRegistrationViewController *interViewRegCtrl = segue.destinationViewController;
        interViewRegCtrl.isEop = TRUE;
        interViewRegCtrl.event = self.event;
        [interViewRegCtrl setCallBackBlock:^(NSString *regCount) {
            self.event.totalCandidate = regCount;
            [self.btnTotalCandidate setAttributedTitle:[Utility setUnderline:isEmptyString(self.event.totalCandidate) ? @"" : self.event.totalCandidate
                                                               withTextColor:self.btnTotalCandidate.currentTitleColor]
                                              forState:UIControlStateNormal];
        }];
    }else if([segue.identifier isEqualToString:@"EOPDetailToAttand"]){
        EOPRegCandAndAttandViewController *eopRegCandAndAttandViewController = segue.destinationViewController;
        eopRegCandAndAttandViewController.eventCode = self.event.eventCode;
    }
}

- (IBAction)onBtnRegisterClick:(id)sender {
    [self performSegueWithIdentifier:@"EOPDetailToRegistration" sender:self];
}

- (IBAction)onBtnEventMaterialsClick:(id)sender
{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    
    PdfDownloadObject *pdfDownloadObj = [[PdfDownloadObject alloc]init];
    NSString *eventCode = [NSString stringWithFormat:@"%d",self.event.eventCode];
    [pdfDownloadObj downloadFile:eventCode afterMaterial:NO finished:^(id response) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        [self showWebView];
        if([response isKindOfClass:[NSData class]])
        {
            UIWindow * window = [UIApplication sharedApplication].windows.lastObject;
            if ([self.event.topicPath hasSuffix:@"jpg"])
            {
                [self.webView loadData:response MIMEType:@"application/jpg" textEncodingName:@"utf-8" baseURL:nil];
                
                
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([self.event.topicPath hasSuffix:@"png"])
            {
                [self.webView loadData:response MIMEType:@"application/png" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([self.event.topicPath hasSuffix:@"pdf"])
            {
                [self.webView loadData:response MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([self.event.topicPath hasSuffix:@"xls"])
            {
                [self.webView loadData:response MIMEType:@"application/xls" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            
        }
    } failure:^(NSError *error) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:@"暂时无法下载文档，请稍后再尝试"
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
    }];
}

- (IBAction)onBtnAfterEventMaterialsClick:(id)sender
{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
    NSString *eventCode = [NSString stringWithFormat:@"%d",self.event.eventCode];
    PdfDownloadObject *pdfDownloadObj = [[PdfDownloadObject alloc]init];
    [pdfDownloadObj downloadFile:eventCode afterMaterial:YES finished:^(id response) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
        [self showWebView];
        if([response isKindOfClass:[NSData class]])
        {
            UIWindow * window = [UIApplication sharedApplication].windows.lastObject;
            
            if ([self.event.profile hasSuffix:@"jpg"])
            {
                [self.webView loadData:response MIMEType:@"application/jpg" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([self.event.profile hasSuffix:@"png"])
            {
                [self.webView loadData:response MIMEType:@"application/png" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([self.event.profile hasSuffix:@"pdf"])
            {
                [self.webView loadData:response MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
            else if ([self.event.profile hasSuffix:@"xls"])
            {
                [self.webView loadData:response MIMEType:@"application/xls" textEncodingName:@"utf-8" baseURL:nil];
                [window addSubview:self.webView];
                [window addSubview:self.closeButton];
            }
        }
    } failure:^(NSError *error) {
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:@"暂时无法下载文档，请稍后再尝试"
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:@""
                                      cancelBlock:nil
                                          okBlock:nil];
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
    }];
}

- (IBAction)onBtnTotalCandidateClick:(id)sender {
        if([self.btnTotalCandidate.titleLabel.text integerValue] > 0){
        [self.contactEventModel getEOPRegisteredCandidate:[NSString stringWithFormat:@"%d",self.event.eventCode] agentId:[CachingService sharedInstance].currentUserId
                                                  success:^(id responseObject) {
                                                      [self performSegueWithIdentifier:@"EOPDetailToAttand" sender:self];
                                                  } failure:^(id responseObject, NSError *error) {
                                                      [self performSegueWithIdentifier:@"EOPDetailToAttand" sender:self];
                                                  }];
    }
}

- (IBAction)OnBtnLocationClick:(id)sender {
    
    NSString *address = self.txtLocation.text;
    [self getSearchLocation:address];
}

- (IBAction)onBtnShareClick:(id)sender {
    TblEOP *eop = (TblEOP *)[DbUtils fetchObject:@"TblEOP"
                                 andPredict:[NSPredicate predicateWithFormat:@"eventCode == %d", self.event.eventCode]
                          andSortDescriptor:nil
                       managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    [self showSocialSharingPopoverCtrl:nil
                                EOPObj:eop
                            isGreeting:FALSE
                                 isEOP:TRUE
                                 isQRCode:NO
                            shareImage:[ApplicationFunction generateBarcodeImageWithQRCode:[eop getLocalizedQRCodeString]]
                        shareImageFile:nil
                              shareMsg:nil
                              shareSub:self.event.eventName
                                  rect:((UIButton *)sender).frame
                                inView:self.view];
}

@end
