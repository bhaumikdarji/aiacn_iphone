//
//  EOPRegCandAndAttandViewController.h
//  AIAChina
//
//  Created by AIA on 03/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface EOPRegCandAndAttandViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UITableView *tblEOPRegister;
@property (weak, nonatomic) UIPopoverController *parentPopOver;

@property (strong, nonatomic) NSArray *eopCandidateArray;

@property (nonatomic) NSInteger eventCode;


@end
