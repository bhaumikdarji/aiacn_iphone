//
//  EOPRegCandAndAttandViewController.m
//  AIAChina
//
//  Created by AIA on 03/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EOPRegCandAndAttandViewController.h"
#import "EOPRegCandAndAttandCell.h"

@interface EOPRegCandAndAttandViewController ()

@end

@implementation EOPRegCandAndAttandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"EOP Registered Candidates & Attendance", @"");
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleBordered target:self action:@selector(popView)];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    self.eopCandidateArray = [DbUtils fetchAllObject:@"TblEOPCandidate"
                                          andPredict:[NSPredicate predicateWithFormat:@"eventCode=%@",[NSNumber numberWithInteger:self.eventCode]]
                                   andSortDescriptor:nil
                                managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    if(self.eopCandidateArray.count<=0){
        [DisplayAlert showAlertInControllerInView:nil
                                            title:NSLocalizedString(@"Error", @"")
                                          message:NSLocalizedString(@"Not currently connected to the Internet",@"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:^(UIAlertAction *action) {
                                          if([self.parentViewController isKindOfClass:[UINavigationController class]]){
                                              [self.navigationController popViewControllerAnimated:YES];
                                          }else if(self.parentPopOver){
                                              [self.parentPopOver dismissPopoverAnimated:TRUE];
                                          }
                                      }
                                          okBlock:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.eopCandidateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EOPRegCandAndAttandCell *cell = (EOPRegCandAndAttandCell *)[tableView dequeueReusableCellWithIdentifier:@"EOPRegisterCell"];
    TblEOPCandidate *eopCandidate = self.eopCandidateArray[indexPath.row];
    [cell loadEOPCandidateData:eopCandidate];
    return cell;
}
- (void) popView
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
