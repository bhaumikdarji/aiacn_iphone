//
//  FFButtonWithDatePopover.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/16/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFButtonWithDatePopover.h"

#import "FFDatePopoverController.h"
#import "FFImportantFilesForCalendar.h"

@interface FFButtonWithDatePopover () <FFDatePopoverControllerProtocol>
@property (nonatomic, strong) FFDatePopoverController *popoverControllerDate;
@end

@implementation FFButtonWithDatePopover

#pragma mark - Synthesize

@synthesize popoverControllerDate;
@synthesize dateOfButton;
@synthesize timeOfButton;

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setTitleColor:[UIColor colorWithRed:86.0/255.0 green:86.0/255.0 blue:86.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [self setBackgroundColor:[UIColor clearColor]];
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame date:(NSDate *)date{
    
    self = [self initWithFrame:frame];
    
    self.fullDate = date;
    NSDateComponents *compDate = date.componentsOfDate;
    
    if (self) {
        [self setDateAndTime:compDate];
    }
    return self;
}

#pragma mark - Button Action

- (IBAction)buttonAction:(id)sender {
    [self.superview endEditing:TRUE];
    NSDate *dateTime = [NSDate dateWithYear:dateOfButton.componentsOfDate.year
                                      month:dateOfButton.componentsOfDate.month
                                        day:dateOfButton.componentsOfDate.day
                                       hour:timeOfButton.componentsOfDate.hour
                                        min:timeOfButton.componentsOfDate.minute];
    
    popoverControllerDate = [[FFDatePopoverController alloc] initWithDate:dateTime datePickerType:self.datePickerType];
    [popoverControllerDate setProtocol:self];
        
    [popoverControllerDate presentPopoverFromRect:self.frame
                                           inView:[super superview]
                         permittedArrowDirections:UIPopoverArrowDirectionAny
                                         animated:YES];
}

#pragma mark - FFDatePopoverController Protocol

- (void)valueChanged:(NSDate *)newDate {
    self.fullDate = newDate;
    NSDateComponents *compNewDate = newDate.componentsOfDate;
    [self setDateAndTime:compNewDate];
}

- (void)setDateAndTime:(NSDateComponents *)compDate{
    dateOfButton = [NSDate dateWithYear:compDate.year month:compDate.month day:compDate.day];
    timeOfButton = [NSDate dateWithHour:compDate.hour min:compDate.minute];
    if(self.isAllDay){
        [self setTitle:[NSDate stringDayOfDate:dateOfButton] forState:UIControlStateNormal];
    }else{
        [self setTitle:[NSString stringWithFormat:@"%@, %@",[NSDate stringDayOfDate:dateOfButton], [NSDate stringTimeOfDate12:timeOfButton]] forState:UIControlStateNormal];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
