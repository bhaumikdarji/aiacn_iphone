//
//  FFButtonWithDatePopover.h
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/16/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import <UIKit/UIKit.h>

@interface FFButtonWithDatePopover : UIButton

@property (nonatomic, strong) NSDate *dateOfButton;
@property (nonatomic, strong) NSDate *timeOfButton;
@property (nonatomic, strong) NSDate *fullDate;
@property (nonatomic) UIDatePickerMode datePickerType;
@property (nonatomic) BOOL isAllDay;

- (id)initWithFrame:(CGRect)frame date:(NSDate *)date;

@end
