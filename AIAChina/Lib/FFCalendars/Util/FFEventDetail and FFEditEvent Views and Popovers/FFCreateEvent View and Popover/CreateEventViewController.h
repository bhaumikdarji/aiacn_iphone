//
//  CreateEventViewController.h
//  Calendar
//
//  Created by Quix Creations on 23/04/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFEvent.h"
#import "FFButtonWithDatePopover.h"
#import "MLPAutoCompleteTextField.h"
#import "MBProgressHUDUpd.h"

enum{
    ALERT_VIEW_DELETE = 111,
    ALERT_VIEW_CANCEL
};

@interface CreateEventViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txtEvent;
@property (weak, nonatomic) IBOutlet UITextField *txtLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnActivityType;

@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *txtCandidate;
@property (weak, nonatomic) IBOutlet UITableView *tblCandidateList;

@property (weak, nonatomic) IBOutlet UISwitch *allDaySwitch;
@property (strong, nonatomic) IBOutlet FFButtonWithDatePopover *btnStartDate;
@property (strong, nonatomic) IBOutlet FFButtonWithDatePopover *btnEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnReminderMode;
@property (weak, nonatomic) IBOutlet UIButton *btnReminder;
@property (weak, nonatomic) IBOutlet UITextView *txtNote;

@property (weak, nonatomic) IBOutlet UIButton *btnAddEvent;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteEvent;

@property (strong, nonatomic) NSMutableArray *candidateArray;
@property (strong, nonatomic) NSArray *allContactNameArray;

@property (nonatomic, strong) FFEvent *event;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) BOOL isDayView;
@property (nonatomic, strong) UIPopoverController *popoverCtrl;

@property (copy, nonatomic) void(^createEvent)(NSDictionary *eventDic, MBProgressHUDUpd *hud);
@property (copy, nonatomic) void(^deleteEvent)(FFEvent *eventDic, NSDate *oldStartDate, NSDate *oldEndDate);
@property (copy, nonatomic) void(^editEvent)(FFEvent *eventDic, NSDate *oldStartDate, NSDate *oldEndDate, MBProgressHUDUpd *hud);
@property (copy, nonatomic) void(^cancelEvent)(UIView *eventView);

@property (nonatomic, strong) NSDate *oldStartDate;
@property (nonatomic, strong) NSDate *oldEndDate;

@property (strong, nonatomic) UIView *overlay;
@property (strong, nonatomic) MBProgressHUDUpd *hud;
@property (nonatomic) BOOL isMap;

- (IBAction)onBtnLocationClick:(id)sender;



@end
