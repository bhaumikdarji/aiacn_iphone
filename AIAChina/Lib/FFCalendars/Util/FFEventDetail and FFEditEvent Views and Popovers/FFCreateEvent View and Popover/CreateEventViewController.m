//
//  CreateEventViewController.m
//  Calendar
//
//  Created by Quix Creations on 23/04/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "CreateEventViewController.h"
#import "FFEvent.h"
#import "CandidateCell.h"
#import "FFDateManager.h"
#import "FFRepeatReminderPopoverViewController.h"
#import "Utils.h"
#import "ActivityType.h"
#import "MapViewController.h"

@interface CreateEventViewController ()

@end

@implementation CreateEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.txtEvent.placeholder = NSLocalizedString(@"Event Title", @"");
    self.txtLocation.placeholder = NSLocalizedString(@"Location", @"");
    self.txtCandidate.placeholder = NSLocalizedString(@"Add Candidate", @"");
    self.candidateArray = [[NSMutableArray alloc] init];
    
    [self.txtCandidate setAutoCompleteTableBorderColor:[UIColor colorWithRed:165.0/255.0 green:12.0/255.0 blue:87.0/255.0 alpha:1.0]];
    [self.txtCandidate setAutoCompleteTableBorderWidth:1.0];
    self.txtCandidate.applyBoldEffectToAutoCompleteSuggestions = NO;
    [self.txtCandidate setAutoCompleteTableCellTextColor:self.txtCandidate.textColor];
    
    self.allContactNameArray = [DbUtils fetchDistinctValues:@"TblContact"
                                              attributeName:@"name"
                                               andPredicate:[NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete == 0",[CachingService sharedInstance].currentUserId]
                                          andSortDescriptor:nil
                                       managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 560);
    self.btnStartDate = [[FFButtonWithDatePopover alloc] initWithFrame:CGRectMake(10, 275, self.scrollView.frame.size.width - 20, 43)
                                                                  date:(self.event.startDate) ? self.event.startDate : [NSDate date]];
    [self.btnStartDate setAutoresizingMask: UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin];
    [self.scrollView addSubview:self.btnStartDate];
    self.btnEndDate = [[FFButtonWithDatePopover alloc] initWithFrame:CGRectMake(10, 318, self.scrollView.frame.size.width - 20, 43)
                                                                date:(self.event.endDate) ? self.event.endDate : [NSDate date]];
    
    
    [self.btnEndDate setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin];
    [self.scrollView addSubview:self.btnEndDate];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardDidShow:(NSNotification *)aNotification {
    if(self.isDayView)
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 900);
}

- (void)keyboardDidHide:(NSNotification *)aNotification {
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 560);
    self.scrollView.contentOffset = CGPointMake(0, 0);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if([textField isEqual:self.txtCandidate]){
        self.scrollView.contentOffset = CGPointMake(0, 100);
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if([textView isEqual:self.txtNote]){
        self.scrollView.contentOffset = CGPointMake(0, 350);
    }
}

- (void)loadData{
    if(self.event){
        self.lblTitle.text = NSLocalizedString(@"Modify Activity", @"");
        self.btnDeleteEvent.hidden = FALSE;
        self.oldStartDate = self.event.startDate;
        self.oldEndDate = self.event.endDate;
    }else{
        self.lblTitle.text = NSLocalizedString(@"New Activity", @"");
        self.btnDeleteEvent.hidden = TRUE;
    }
    if([self.event.activityType isEqualToString:ACTIVITY_INT])
        self.txtEvent.text = NSLocalizedString(self.event.eventName, @"");
    else
        self.txtEvent.text = self.event.eventName;
    self.txtLocation.text = self.event.location;
    self.latitude = self.event.latitude;
    self.longitude = self.event.longitude;
    self.txtNote.text = [ValueParser parseString:self.event.notes];
    [self.btnActivityType setTitle:isEmptyString(self.event.activityType) ? NSLocalizedString(@"Select", @"") : self.event.activityType forState:UIControlStateNormal];
    [self.btnReminder setTitle:isEmptyString(self.event.reminder) ? NSLocalizedString(@"Never", @"") : self.event.reminder forState:UIControlStateNormal];
    [self.btnReminderMode setTitle:isEmptyString(self.event.reminderMode) ? NSLocalizedString(@"Never", @"") : self.event.reminderMode forState:UIControlStateNormal];
    if (self.event.allDay) {
        [self.allDaySwitch setOn:YES];
    }
    else
    {
        [self.allDaySwitch setOn:NO];
    }
    [self setDateTitle];
    if(self.event.candidateArray)
        self.candidateArray = self.event.candidateArray;
    
    [self.tblCandidateList reloadData];
}

- (IBAction)onAllDaySwitchClick:(id)sender {
    [self.view endEditing:TRUE];
    [self setDateTitle];
}

- (void)setDateTitle{
    NSDateComponents *compStartDate = self.btnStartDate.fullDate.componentsOfDate;
    NSDateComponents *compEndDate = self.btnEndDate.fullDate.componentsOfDate;
    self.btnStartDate.isAllDay = self.btnEndDate.isAllDay = self.allDaySwitch.isOn;
    if(self.allDaySwitch.isOn){
        self.event.allDay = YES;
        self.btnStartDate.datePickerType = self.btnEndDate.datePickerType = UIDatePickerModeDate;
        [self.btnStartDate setTitle:[NSDate stringDayOfDate:[NSDate dateWithYear:compStartDate.year month:compStartDate.month day:compStartDate.day]] forState:UIControlStateNormal];
        
        [self.btnEndDate setTitle:[NSDate stringDayOfDate:[NSDate dateWithYear:compEndDate.year month:compEndDate.month day:compEndDate.day]] forState:UIControlStateNormal];
    }else{
        self.event.allDay = NO;
        self.btnStartDate.datePickerType = self.btnEndDate.datePickerType = UIDatePickerModeDateAndTime;
        [self.btnStartDate setTitle:[NSString stringWithFormat:@"%@, %@",[NSDate stringDayOfDate:[NSDate dateWithYear:compStartDate.year month:compStartDate.month day:compStartDate.day]], [NSDate stringTimeOfDate12:[NSDate dateWithHour:compStartDate.hour min:compStartDate.minute]]] forState:UIControlStateNormal];
        
        [self.btnEndDate setTitle:[NSString stringWithFormat:@"%@, %@",[NSDate stringDayOfDate:[NSDate dateWithYear:compEndDate.year month:compEndDate.month day:compEndDate.day]], [NSDate stringTimeOfDate12:[NSDate dateWithHour:compEndDate.hour min:compEndDate.minute]]] forState:UIControlStateNormal];
    }
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
            completionHandler:(void (^)(NSArray *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        NSArray *completions = self.allContactNameArray;
        handler(completions);
    });
}

- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"STRING : %@",selectedString);
//    [self.candidateArray addObject:selectedString];
//    [self.tblCandidateList reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.candidateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CandidateCell *cell = (CandidateCell *)[tableView dequeueReusableCellWithIdentifier:@"CandidateCell"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CandidateCell" owner:nil options:nil] objectAtIndex:0];
    }
    
    cell.lblCandidateName.text = self.candidateArray[indexPath.row];
    cell.btnDelete.tag = indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(removeCandidate:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (IBAction)removeCandidate:(UIButton *)sender{
    [self.view endEditing:TRUE];
    [self.candidateArray removeObjectAtIndex:[sender tag]];
    [self.tblCandidateList reloadData];
}

- (IBAction)onBtnActivityTypeClick:(id)sender {
    [self.view endEditing:TRUE];
    [self displayPopOver:sender dataArray:[[ActivityType sharedActivity] getAllManualActitvityType] isShowView:YES];
}

- (IBAction)onBtnReminderModeClick:(id)sender {
    [self.view endEditing:TRUE];
    [self displayPopOver:sender dataArray:@[NSLocalizedString(@"Never", @""), NSLocalizedString(@"Notification", @"")] isShowView:NO];
}

- (IBAction)onBtnReminderClick:(id)sender {
    [self.view endEditing:TRUE];
    [self displayPopOver:sender dataArray:@[NSLocalizedString(@"Never", @""),
                                            NSLocalizedString(@"5 minutes before", @""),
                                            NSLocalizedString(@"15 minutes before", @""),
                                            NSLocalizedString(@"30 minutes before", @""),
                                            NSLocalizedString(@"1 hour before", @""),
                                            NSLocalizedString(@"2 hour before", @""),
                                            NSLocalizedString(@"1 day before", @""),
                                            NSLocalizedString(@"2 day before", @""),
                                            NSLocalizedString(@"1 week before", @"")] isShowView:NO];
}

- (void)displayPopOver:(UIButton *)btn dataArray:(NSArray *)array isShowView:(BOOL)showView{
    FFRepeatReminderPopoverViewController *reminderViewPopover = [[FFRepeatReminderPopoverViewController alloc] initWithNibName:@"FFRepeatReminderPopoverViewController"
                                                                                                                         bundle:nil];
    if(isEmptyString(btn.titleLabel.text) || ([btn isEqual:self.btnActivityType] && [btn.titleLabel.text isEqualToString:NSLocalizedString(@"Select", @"")]))
        [btn setTitle:[array objectAtIndex:0] forState:UIControlStateNormal];
    else
        reminderViewPopover.oldValue = btn.titleLabel.text;
    
    reminderViewPopover.pickerArray = array;
    reminderViewPopover.isShowImgView = showView;
    [reminderViewPopover setSelectedValue:^(NSString *selectedValue) {
        [btn setTitle:selectedValue forState:UIControlStateNormal];
    }];
    
    self.popoverCtrl = [[UIPopoverController alloc] initWithContentViewController:reminderViewPopover];
    self.popoverCtrl.popoverContentSize = showView ? CGSizeMake(350, reminderViewPopover.view.frame.size.height) : reminderViewPopover.view.frame.size;
    
    [self.popoverCtrl presentPopoverFromRect:btn.frame
                                      inView:self.scrollView
                    permittedArrowDirections:UIPopoverArrowDirectionAny
                                    animated:YES];
}

- (NSDictionary *)createEventDic{
    NSDictionary *eventDic = @{@"eventName" : [ValueParser parseString:self.txtEvent.text],
                               @"location" : [ValueParser parseString:self.txtLocation.text],
                               @"latitude" : [ValueParser parseString:[NSString stringWithFormat:@"%f",self.latitude]],
                               @"longitude" : [ValueParser parseString:[NSString stringWithFormat:@"%f",self.longitude]],
                               @"activityType" : [ValueParser parseString:self.btnActivityType.titleLabel.text],
                               @"notes" : [ValueParser parseString:self.txtNote.text],
                               @"startDate" : [NSDate dateToString:self.btnStartDate.fullDate andFormate:kDateFormatte],
                               @"endDate" : [NSDate dateToString:self.btnEndDate.fullDate andFormate:kDateFormatte],
                               @"allDay": self.allDaySwitch.isOn ? @"yes" : @"no",
                               @"candidateArray" : self.candidateArray,
                               @"reminder" : [ValueParser parseString:self.btnReminder.titleLabel.text],
                               @"reminderMode" : [ValueParser parseString:self.btnReminderMode.titleLabel.text]
                               };
    return eventDic;
}

- (void)setEditEvent{
    self.event.eventName = [ValueParser parseString:self.txtEvent.text];
    self.event.location = [ValueParser parseString:self.txtLocation.text];
    self.event.latitude = self.latitude;
    self.event.longitude = self.longitude;
    self.event.activityType = [ValueParser parseString:self.btnActivityType.titleLabel.text];
    self.event.notes = [ValueParser parseString:self.txtNote.text];
    self.event.startDate = self.btnStartDate.fullDate;
    self.event.endDate = self.btnEndDate.fullDate;
    self.event.allDay = self.allDaySwitch.isOn ? TRUE : FALSE;
//    self.event.candidates = self.inviteArray;
    self.event.reminder = [ValueParser parseString:self.btnReminder.titleLabel.text];
    self.event.reminderMode = [ValueParser parseString:self.btnReminderMode.titleLabel.text];
    
    if(self.event.allDay){
        [self.event setDateDay:[NSDate dateWithYear:self.event.startDate.componentsOfDate.year month:self.event.startDate.componentsOfDate.month day:self.event.startDate.componentsOfDate.day]];
        [self.event setDateTimeBegin:[self.event.startDate dateAtStartOfDay]];
        [self.event setDateTimeEnd:[self.event.endDate dateAtEndOfDay]];
    }else if(self.event.startDate){
        [self.event setDateDay:[NSDate dateWithYear:self.event.startDate.componentsOfDate.year month:self.event.startDate.componentsOfDate.month day:self.event.startDate.componentsOfDate.day]];
        [self.event setDateTimeBegin:[NSDate dateWithHour:self.event.startDate.componentsOfDate.hour min:self.event.startDate.componentsOfDate.minute]];
        [self.event setDateTimeEnd:[NSDate dateWithHour:self.event.endDate.componentsOfDate.hour min:self.event.endDate.componentsOfDate.minute]];
    }
}

- (void)showProgressHud{
    UIWindow *tempKeyboardWindow = [[[UIApplication sharedApplication] windows] lastObject];
    self.hud = [[MBProgressHUDUpd alloc] initWithWindow:tempKeyboardWindow];
    self.hud.dimBackground = YES;
    self.hud.mode = MBProgressHUDModeIndeterminateUpd;
    [tempKeyboardWindow addSubview:self.hud];
    [self.hud show:TRUE];
}

- (IBAction)onAddEventClick:(id)sender {
    [self.view endEditing:TRUE];
    if([self validation]){
        [self showProgressHud];
        if(self.event){
            if(self.editEvent){
                [self setEditEvent];
                self.editEvent(self.event, self.oldStartDate, self.oldEndDate, self.hud);
            }
        }else{
            if(self.createEvent){
                [[FFDateManager sharedManager] setCurrentDate:[NSDate dateWithYear:self.btnStartDate.fullDate.componentsOfDate.year
                                                                             month:self.btnStartDate.fullDate.componentsOfDate.month
                                                                               day:self.btnStartDate.fullDate.componentsOfDate.day
                                                                              hour:self.btnStartDate.fullDate.componentsOfDate.hour
                                                                               min:self.btnStartDate.fullDate.componentsOfDate.minute]];
                self.createEvent([self createEventDic],self.hud);
            }
        }
    }
}

- (IBAction)onBtnDeleteClick:(id)sender {
    [self.view endEditing:TRUE];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"")
                                                    message:NSLocalizedString(@"Are you sure you want to delete?", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                          otherButtonTitles:NSLocalizedString(@"Ok", @""), nil];
    alert.tag = ALERT_VIEW_DELETE;
    [alert show];
}

- (IBAction)onBtnCancelClick:(id)sender {
    [self.view endEditing:TRUE];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"")
                                                    message:NSLocalizedString(@"Do you want to discard the Activity information?", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"No", @"")
                                          otherButtonTitles:NSLocalizedString(@"Yes", @""), nil];
    alert.tag = ALERT_VIEW_CANCEL;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (alertView.tag == ALERT_VIEW_DELETE) {
        if(buttonIndex == 1) {
            if(self.deleteEvent){
                self.deleteEvent(self.event, self.oldStartDate, self.oldEndDate);
            }
        }
    }else if (alertView.tag == ALERT_VIEW_CANCEL){
        if(buttonIndex == 1) {
            if(self.cancelEvent){
                self.cancelEvent(self.view);
            }
        }
    }
}

- (BOOL)validation{
    NSString *msg = nil;
    if(isEmptyString(self.txtEvent.text)){
        msg = NSLocalizedString(@"You are required to input an Event Title.", @"");
    }else if([self.btnActivityType.titleLabel.text isEqualToString:NSLocalizedString(@"Select", @"")]){
        msg = NSLocalizedString(@"You are required to select an Activity Type.", @"");
    }else if(!self.allDaySwitch.isOn){
        if([self.btnStartDate.fullDate compare:self.btnEndDate.fullDate] == NSOrderedSame ||
           [self.btnStartDate.fullDate compare:self.btnEndDate.fullDate] == NSOrderedDescending){
           msg = NSLocalizedString(@"End date must be greater than start date.", @"");
        }
    }
    if(msg){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"")
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert show];
        return FALSE;
    }
    return TRUE;
}

- (IBAction)onBtnLocationClick:(id)sender {
    [self.view endEditing:TRUE];
    [self performSelector:@selector(showMapView) withObject:nil afterDelay:1.0];
}

- (void)showMapView{
    UIStoryboard *storyboard = [ApplicationFunction getCurrentStoryBoard];
    UINavigationController *navCtrl = [storyboard instantiateViewControllerWithIdentifier:@"MapNavController"];
    MapViewController *mapViewCtrl = navCtrl.viewControllers[0];
    mapViewCtrl.latitude = self.latitude;
    mapViewCtrl.longitude = self.longitude;
    [mapViewCtrl setLocationBlock:^(double latitide, double longitude, NSString *address) {
        self.latitude = latitide;
        self.longitude = longitude;
        self.txtLocation.text = address;
        self.isMap = TRUE;
    }];
    [self presentViewController:navCtrl animated:YES completion:nil];
}

- (IBAction)onBtnAddClick:(id)sender {
    if(!isEmptyString(self.txtCandidate.text)){
        [self.candidateArray addObject:self.txtCandidate.text];
        [self.tblCandidateList reloadData];
        self.txtCandidate.text = @"";
    }
}

@end
