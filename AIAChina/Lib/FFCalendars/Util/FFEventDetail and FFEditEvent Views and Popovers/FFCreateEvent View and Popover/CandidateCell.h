//
//  CandidateCell.h
//  AIAChina
//
//  Created by AIA on 20/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CandidateCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblCandidateName;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@end
