//
//  FFRepeatReminderPopoverViewController.m
//  Calendar
//
//  Created by Quix Creations on 04/05/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "FFRepeatReminderPopoverViewController.h"
#import "Utility.h"
#import "ActivityType.h"
#import "FFConstants.h"

@interface FFRepeatReminderPopoverViewController ()

@end

@implementation FFRepeatReminderPopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    int row = 0;
    for (NSString *str in self.pickerArray) {
        if([str isEqualToString:self.oldValue])
            break;
        row++;
    }
    [self.picker selectRow:row inComponent:0 animated:FALSE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.pickerArray.count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UIView *containView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.picker.frame.size.width, 40)];
    if(self.isShowImgView){
        
        UIImageView *imgType = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 30, 30)];
        NSString *imgName = @"";
        if(self.isCalModeImg){
            imgType.frame = CGRectMake(20, 12,  15, 15);
//            if([[[ActivityType sharedActivity] getActivityTypeToMode:self.pickerArray[row]] isEqualToString:ACTIVITY_OPPORTUNITY]){
//                imgName = [NSString stringWithFormat:@"ic_cm_%@.png",[[ACTIVITY_OPPORTUNITY stringByReplacingOccurrencesOfString:@" "
//                                                                                                                     withString:@"_"] lowercaseString]];
//            }else{
                imgName = [NSString stringWithFormat:@"ic_cm_%@.png",[[self.pickerArray[row] stringByReplacingOccurrencesOfString:@" "
                                                                                                                       withString:@"_"] lowercaseString]];
//            }
        }else{
            imgName = [NSString stringWithFormat:@"ic_%@",[[[self.pickerArray[row] stringByReplacingOccurrencesOfString:@" "
                                                                                                             withString:@"_"] stringByReplacingOccurrencesOfString:@"/" withString:@"_"] lowercaseString]];
        }
        imgType.image = [UIImage imageNamed:imgName];
        imgType.backgroundColor = [UIColor clearColor];

        [containView addSubview:imgType];
    }
    
    UILabel *lblType = [[UILabel alloc] initWithFrame:CGRectMake(self.isShowImgView ? 50 : 10, 0, self.picker.frame.size.width - 60, 40)];
    lblType.text = self.pickerArray[row];
    lblType.textColor = [UIColor colorWithRed:48.0/255.0 green:48.0/255.0 blue:48.0/255.0 alpha:1.0];
    lblType.backgroundColor = [UIColor clearColor];
    [containView addSubview:lblType];
    return containView;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(self.selectedValue){
        self.selectedValue(self.pickerArray[row]);
    }
}

@end
