//
//  FFRepeatReminderPopoverViewController.h
//  Calendar
//
//  Created by Quix Creations on 04/05/15.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFRepeatReminderPopoverViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (nonatomic, strong) NSArray *pickerArray;
@property (nonatomic, strong) NSString *oldValue;
@property (nonatomic) BOOL isShowImgView;
@property (nonatomic) BOOL isCalModeImg;
@property (copy, nonatomic) void(^selectedValue)(NSString *selectedValue);

@end
