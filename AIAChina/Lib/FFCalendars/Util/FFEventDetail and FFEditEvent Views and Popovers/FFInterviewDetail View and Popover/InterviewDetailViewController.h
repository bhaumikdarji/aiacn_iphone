//
//  InterviewDetailViewController.h
//  AIAChina
//
//  Created by AIA on 03/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface InterviewDetailViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UILabel *lblInterviewTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDateText;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTimeText;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTimeText;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationText;
@property (weak, nonatomic) IBOutlet UITextView *txtLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblQuotaBalance;

@property (weak, nonatomic) IBOutlet UILabel *lblnterviewMaterialText;
@property (weak, nonatomic) IBOutlet UITextView *txtInterviewMaterial;
@property (weak, nonatomic) IBOutlet UILabel *lbltotalCandidatesText;
@property (weak, nonatomic) IBOutlet UIButton *btnTotalCandidates;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property (nonatomic, strong) FFEvent *event;
@property (nonatomic) int quata;
@property (copy, nonatomic) void(^dismissPopOverBlock)();

- (IBAction)onBtnLocationClick:(id)sender;
- (IBAction)onBtnTotalCandidatesClick:(id)sender;
- (IBAction)onBtnRegisterClick:(id)sender;


@end
