//
//  InterviewRegistrationViewController.m
//  AIAChina
//
//  Created by AIA on 10/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "InterviewRegistrationViewController.h"
#import "DropDownManager.h" 
#import "PickerPopoverViewController.h"
#import "ContactEventModel.h"
#import "ContactDataProvider.h"
#import "AFHTTPRequestOperationManager.h"

@interface InterviewRegistrationViewController ()
@property (nonatomic, retain) ContactEventModel *eventModel;
@property (nonatomic, strong) ContactDataProvider *contactDataProvider;
@end

@implementation InterviewRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(!self.isEop){
        self.interview = (TblInterview *)[[CachingService sharedInstance].managedObjectContext existingObjectWithID:self.event.interviewId
                                                                                                                   error:nil];
    }
    
    if(self.isEop){
        self.title = NSLocalizedString(@"EOP Registration",@"");
    }else{
        if([self.interview.interviewType isEqualToString:@"2nd"]){
            self.title = NSLocalizedString(@"Second Interview Registration",@"");
        }else{
            self.title = NSLocalizedString(@"Third Interview Registration",@"");
        }
    }
    
    [self.navigationItem setHidesBackButton:YES];
    
    self.eventModel = [ContactEventModel sharedInstance];

    NSString *name = NSLocalizedString(@"Name*:", @"");
    [self.lblName setAttributedText:[Utility setSupAttrOnSingleLable:name
                                                            boldFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0]
                                                         regularFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0]
                                                               range:[name rangeOfString:@"*"]
                                                   setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    
    NSString *gender = NSLocalizedString(@"Gender*:", @"");
    [self.lblGender setAttributedText:[Utility setSupAttrOnSingleLable:gender
                                                            boldFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0]
                                                         regularFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0]
                                                               range:[gender rangeOfString:@"*"]
                                                    setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *dob = NSLocalizedString(@"D.O.B*:", @"");
    [self.lblDob setAttributedText:[Utility setSupAttrOnSingleLable:dob
                                                            boldFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0]
                                                         regularFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0]
                                                               range:[dob rangeOfString:@"*"]
                                                    setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    
    UIColor *borderColor = [UIColor colorWithRed:201.0/255.0 green:201.0/255.0 blue:201.0/255.0 alpha:1.0];
    [Utility setBorderAndRoundCorner:self.btnGender
                         borderColor:borderColor
                               width:1.0
                        cornerRedius:5.0];

    [Utility setBorderAndRoundCorner:self.btnDob
                         borderColor:borderColor
                               width:1.0
                        cornerRedius:5.0];

    [Utility setBorderAndRoundCorner:self.btnEducation
                         borderColor:borderColor
                               width:1.0
                        cornerRedius:5.0];

    self.txtName.borderStyle = UITextBorderStyleRoundedRect;

    [self.txtName setAutoCompleteTableBorderColor:[UIColor colorWithRed:165.0/255.0 green:12.0/255.0 blue:87.0/255.0 alpha:1.0]];
    [self.txtName setAutoCompleteTableBorderWidth:1.0];
    
    self.txtName.applyBoldEffectToAutoCompleteSuggestions = NO;
    [self.txtName setAutoCompleteTableAppearsAsKeyboardAccessory:NO];
    self.allContactNameArray = [DbUtils fetchAllObject:@"TblContact"
                                            andPredict:[NSPredicate predicateWithFormat:@"agentId == %@ AND isDelete==0",[CachingService sharedInstance].currentUserId]
                                     andSortDescriptor:nil
                                  managedObjectContext:[CachingService sharedInstance].managedObjectContext];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHideWithNotification:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:self];
}

- (void)keyboardDidHideWithNotification:(NSNotification *)aNotification{
    [self.view endEditing:true];
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
            completionHandler:(void (^)(NSArray *))handler
{
    NSArray *resultArray = [self.allContactNameArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" SELF.name CONTAINS[c] %@",textField.text]];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        NSArray *completions = resultArray;
        handler(completions);
    });
}

- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedObject){
        self.contact = (TblContact *)selectedObject;
        [self loadCandidateData];
    }
}

- (void)loadCandidateData{
    if(self.contact){
        [self.btnGender setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_GENDER code:self.contact.gender]  forState:UIControlStateNormal];
        [self.btnDob setTitle:[DateUtils dateToString:self.contact.birthDate andFormate:kDisplayDateFormatte] forState:UIControlStateNormal];
        [self.btnEducation setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_EDUCATION code:self.contact.education] forState:UIControlStateNormal];
        self.txtWeChat.text = self.contact.weChat;
        self.txtEmail.text = self.contact.eMailId;
        self.txtId.text = self.contact.nric;
        self.txtTeleNumber.text = self.contact.mobilePhoneNo;
    }
}

- (IBAction)onBtnGenderClick:(id)sender {
    [self.view endEditing:true];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager].gender valueForKeyPath:@"Description"]];
}

- (IBAction)onBtnDobClick:(id)sender {
    [self.view endEditing:true];
    
    [self showDatePopoverCtrl:UIDatePickerModeDate
                  minimumDate:nil
                  maximumDate:nil
                         rect:self.btnDob.frame
                       inView:self.view
                   pickerDate:^(NSDate *date) {
                       [self.btnDob setTitle:[DateUtils dateToString:date andFormate:kDisplayDateFormatte] forState:UIControlStateNormal];
                   }];    
}

- (IBAction)onBtnEducationClick:(id)sender {
    [self.view endEditing:true];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager].education valueForKeyPath:@"Description"]];
}

- (IBAction)onBtnConfirmClick:(id)sender {
    [self.view endEditing:true];
    if([self isInternetConnect]){
        if([self validation]){
            [self updateLocalDb];
            if(self.isEop)
                [self eopRegisterCall];
            else{
                if([self.event.eventName isEqualToString:@"3rd"]){
                    [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
                    [[ContactEventModel sharedInstance] getCandidateInterviewStatus:@"2nd"
                                                                   forCandidateCode:[self.contact.addressCode stringValue]
                                                                            success:^(id responseObject) {
                                                                                [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                                                for (NSArray *resArray in responseObject) {
                                                                                    NSString *response = [resArray valueForKey:@"interviewStatus"];
                                                                                    
                                                                        NSLog(@"interviewStatus:%@",response);
                                                                                    if([response isEqualToString:@"No Status"] || [response isEqualToString:@"FAIL"]){
                                                                                        [DisplayAlert showAlertInControllerInView:self
                                                                                                                            title:@""
                                                                                                                          message:NSLocalizedString(@"2nd interview don't completed.", @"")
                                                                                                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                                                                                    okButtonTitle:nil
                                                                                                                      cancelBlock:nil
                                                                                                                          okBlock:nil];
                                                                                        return;
                                                                                    }else
                                                                                        [self interviewRegisterCall];
                                                                                }
                                                                            } failure:^(id responseObject, NSError *error) {
                                                                                [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
                                                                            }];
                }else
                   [self interviewRegisterCall];
            }
        }
    }
}

- (void)updateLocalDb{

    self.contact.gender = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_GENDER description:self.btnGender.currentTitle];
    self.contact.birthDate = [DateUtils stringToDate:self.btnDob.currentTitle dateFormat:kDisplayDateFormatte];
    if(!isEmptyString(self.btnEducation.currentTitle))
        self.contact.education = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_EDUCATION description:self.btnEducation.currentTitle];
    if(!isEmptyString(self.txtWeChat.text))
        self.contact.weChat = self.txtWeChat.text;
    if(!isEmptyString(self.txtEmail.text))
        self.contact.eMailId = self.txtEmail.text;
    if(!isEmptyString(self.txtId.text))
        self.contact.nric = self.txtId.text;
    if(!isEmptyString(self.txtTeleNumber.text))
        self.contact.mobilePhoneNo = self.txtTeleNumber.text;
    
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
}

- (BOOL)validation{
    NSString *msg = nil;
    
    if(isEmptyString(self.txtName.text)){
        msg = NSLocalizedString(@"Please add candidate name", @"");
    }else if(isEmptyString(self.btnGender.currentTitle)){
        msg = NSLocalizedString(@"Please add gender", @"");
    }else if(isEmptyString(self.btnDob.currentTitle)){
        msg = NSLocalizedString(@"Please add date of birth", @"");
    }
    
    if(isEmptyString(msg)){
        if(self.isEop){
            TblContact *contact = (TblContact *)[DbUtils fetchObject:@"TblContact"
                                                          andPredict:[NSPredicate predicateWithFormat:@"name = %@ AND agentId == %@ AND isDelete==0",self.txtName.text,[CachingService sharedInstance].currentUserId]
                                                   andSortDescriptor:nil
                                                managedObjectContext:[CachingService sharedInstance].managedObjectContext];
            if(!contact) {
                self.contact = [self createContact];
            }
            else {
                self.contact = contact;
            }
        }else{
            if(self.contact.addressCodeValue == 0){
                msg = NSLocalizedString(@"User does not exist in address book", @"");
            }else if([self.interview.interviewType isEqualToString:@"2nd"]){
                if(![self.contact.candidateProcess.firstInterviewResult isEqualToString:FIRST_INTERVIEW_PASS]){
                    msg = NSLocalizedString(@"First interview don't completed.", @"");
                }else if(![ApplicationFunction isCompletePersonalInfo:self.contact]){
                    msg = NSLocalizedString(@"Personal info don't completed.", @"");
                }else if(![ApplicationFunction isCompleteEducation:self.contact]){
                    msg = NSLocalizedString(@"Education info don't completed.", @"");
                }else if(![ApplicationFunction isCompleteSignature:self.contact]){
                    msg = NSLocalizedString(@"ESignature don't completed.", @"");
                }
            }
        }
    }
    
    if(!isEmptyString(msg)){
        [DisplayAlert showAlertInControllerInView:self
                                            title:NSLocalizedString(@"Error", @"")
                                          message:msg
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:^(UIAlertAction *action) {
                                          
                                      } okBlock:nil];
        return FALSE;
    }
    return TRUE;
}

- (IBAction)onBtnBackClick:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)eopRegisterCall{
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window]  animated:YES];
    
    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc]init];
    [bodyDictionary setValue:self.contact.addressCode forKey:@"candidateCode"];
    [bodyDictionary setValue:[NSString stringWithFormat:@"%d",self.event.eventCode] forKey:@"eventCode"];
    [bodyDictionary setValue:self.contact.name forKey:@"candidateName"];
    [bodyDictionary setValue:[CachingService sharedInstance].currentUserId forKey:@"servicingAgent"];
    [bodyDictionary setValue:self.contact.referalSource forKey:@"sourceOfReferal"];
    [bodyDictionary setValue:self.contact.age forKey:@"age"];
    [bodyDictionary setValue:[DateUtils dateToString:self.contact.birthDate andFormate:kDisplayDateFormatte] forKey:@"dob"];
    [bodyDictionary setValue:@"" forKey:@"dobStr"];
    [bodyDictionary setValue:self.contact.gender forKey:@"gender"];
    [bodyDictionary setValue:self.contact.mobilePhoneNo forKey:@"contactNumber"];
    [bodyDictionary setValue:@"true" forKey:@"statusStr"];
    [bodyDictionary setValue:@"" forKey:@"token"];
    [self.eventModel postCandidateRegistrationServiceCall:bodyDictionary agentId:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
        for (NSDictionary *response in responseObject) {
            BOOL status = [[response valueForKey:@"status"] boolValue];
            BOOL duplicate = [[response valueForKey:@"isDuplicate"] boolValue];
            int registeredCount=[[response valueForKey:@"registeredCount"] intValue];
            
            AFHTTPRequestOperationManager *ASAPIManager=[[AFHTTPRequestOperationManager alloc]init];
            AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
            NSString *sessionId=delegate.sessionid;
            if (sessionId == nil || [sessionId isEqualToString:@""]) {
            }
            else
            {
                if (registeredCount==1 && status && !duplicate) {
                    
                    NSString *strMainTime = [DateUtils dateToString:self.event.eventDate andFormate:kServerDateFormatte];
                    NSString *subMainTime = [strMainTime substringToIndex:10];
                    
                    NSString *strStartTime = [DateUtils dateToString:self.event.startDate andFormate:kServerDateFormatte];
                    NSString *subStartTime = [strStartTime substringFromIndex:11];
                    subStartTime = [subStartTime stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    NSString *strEndTime = [DateUtils dateToString:self.event.endDate andFormate:kServerDateFormatte];
                    NSString *subEndTime =  [strEndTime substringFromIndex:11];
                    subEndTime = [subEndTime stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    strStartTime = [NSString stringWithFormat:@"%@##%@",subMainTime,subStartTime];
                    strEndTime = [NSString stringWithFormat:@"%@##%@",subMainTime,subEndTime];
                    
                    
                    NSString *eventCode=[NSString stringWithFormat:@"%d",self.event.eventCode];
                    NSDictionary *ob1 = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@-%@",eventCode,[CachingService sharedInstance].currentUserId],@"referenceId",
                                         [CachingService sharedInstance].branchCode,@"co",
                                         [CachingService sharedInstance].currentUserIdWithZero,@"agentcode",
                                         @"09",@"typeId",
                                         NSLocalizedString(@"EOP", @""),@"typeName",
                                         self.event.topic,@"subject",
                                         self.event.eopDesc,@"description" ,
                                         self.event.location,@"address",
                                         strStartTime,@"startTime",
                                         strEndTime,@"endTime",
                                         @"N",@"openFlag",nil];
                    
                    NSArray *arr = [[NSArray alloc]initWithObjects:ob1,nil];
                    //from 03 is for erecruitment , version is static 2.0.1 & option A is for Add and arr is the dictionary of events.
                    
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:kCMSVersionConstant,@"version",@"A",@"option",@"03",@"from",arr,@"events",nil];
                    NSDictionary *postDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:dict,@"RequestMessage",nil];
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDictionary options:NSJSONWritingPrettyPrinted error:nil];
                    NSString *jsonString;
                    //convert the dictionary to json
                    if (! jsonData) {
                    } else {
                        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    }
                    
                    jsonString=[jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    jsonString=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    jsonString=[jsonString stringByReplacingOccurrencesOfString:@"##" withString:@" "];
                    
                    NSString *encodedStr=[delegate md5StringForString:jsonString];
                    
                    [ASAPIManager.securityPolicy setAllowInvalidCertificates:YES];
                    [ASAPIManager.requestSerializer setValue:@"multipart/form-data;charset=UTF-8;boundary=--AIACalendar--" forHTTPHeaderField:@"Content-Type"];
                    [ASAPIManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html" ,nil]];
                    [ASAPIManager POST:KErecruitmentEnhancementURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                        [formData appendPartWithFormData:[sessionId dataUsingEncoding:NSUTF8StringEncoding] name:@"SessionId"];
                        [formData appendPartWithFormData:[@"UTF-8" dataUsingEncoding:NSUTF8StringEncoding] name:@"Encoding"];
                        [formData appendPartWithFormData:[encodedStr dataUsingEncoding:NSUTF8StringEncoding] name:@"Signature"];
                        [formData appendPartWithFormData:[@"OneCalendarEventService" dataUsingEncoding:NSUTF8StringEncoding] name:@"Service"];
                        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding] name:@"Data"];
                        
                    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSDictionary *Resposedict=[responseObject valueForKey:@"ResponseMessage"];
                        BOOL exists = [Resposedict objectForKey:@"returnMessage"] != nil;
                        if (!exists) {
                            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                            [alert show];
                        }
                        
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        
                        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                        [alert show];
                    }];
                }
            }
            if(!status && !duplicate){
                NSLog(@"Error");
            }else{
                [self updateRecruitMentProgress:TRUE];
                if(duplicate){
                    [DisplayAlert showAlertInControllerInView:self
                                                        title:NSLocalizedString(@"Registration",@"")
                                                      message:NSLocalizedString(@"Already registration",@"")
                                            cancelButtonTitle:NSLocalizedString(@"OK",@"")
                                                okButtonTitle:nil
                                                  cancelBlock:nil
                                                      okBlock:nil];
                }else
                    [self updateTotalCandidate:TRUE];
            }
        }
        [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
    } failure:^(id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
        [self onBtnBackClick:nil];
    }];
}

- (void)interviewRegisterCall{
    [MBProgressHUDUpd showHUDAddedTo:[MyAppDelegate window] animated:YES];
    NSDictionary *bodyDictionary = @{@"candidateCode" : self.contact.addressCode,
                                     @"interviewCode" : [NSNumber numberWithInt:self.event.interviewCode],
                                     @"candidateName" : self.contact.name,
                                     @"servicingAgent" : [CachingService sharedInstance].currentUserId,
                                     @"sourceOfReferal" : self.contact.referalSource,
                                     @"age" : [NSNumber numberWithInteger:[ValueParser parseInt:self.contact.age]],
                                     @"dob" : [DateUtils dateToString:self.contact.birthDate andFormate:kDisplayDateFormatte],
                                     @"dobStr" : @"",
                                     @"gender" : self.contact.gender,
                                     @"contactNumber" : self.contact.mobilePhoneNo,
                                     @"ccTestResult" : self.contact.candidateProcess.ccTestStatus ? self.contact.candidateProcess.ccTestStatus : @"",
                                     @"recruitmentScheme" : self.contact.recommendedRecruitmentScheme,
                                     @"p100" : [NSNumber numberWithInt:0],
                                     @"interviewResult" : @"",
                                     @"remarks" : self.contact.remarks ? self.contact.remarks : @"",
                                     @"statusStr" : @"true",
                                     @"token" : @""
                                     };
    
    [[ContactEventModel sharedInstance] postInterviewCandidateRegister:bodyDictionary
                                                               agentId:[CachingService sharedInstance].currentUserId
                                                               success:^(id responseObject) {
                                                                   for (NSDictionary *response in responseObject) {
                                                                       BOOL status = [[response valueForKey:@"status"] boolValue];
                                                                       BOOL duplicate = [[response valueForKey:@"isDuplicate"] boolValue];
                                                                       int registeredCount=[[response valueForKey:@"registeredCount"]intValue];
                                                                       if (registeredCount==1 && status && !duplicate) {
                                                                           NSString *strMainTime = [DateUtils dateToString:self.interview.interviewDate andFormate:kServerDateFormatte];
                                                                           NSString *subMainTime = [strMainTime substringToIndex:10];
                                                                           
                                                                           NSString *strStartTime = [DateUtils dateToString:self.interview.startTime andFormate:kServerDateFormatte];
                                                                           NSString *subStartTime = [strStartTime substringFromIndex:11];
                                                                           subStartTime = [subStartTime stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                                                           
                                                                           NSString *strEndTime = [DateUtils dateToString:self.interview.endTime andFormate:kServerDateFormatte];
                                                                           NSString *subEndTime =  [strEndTime substringFromIndex:11];
                                                                           subEndTime = [subEndTime stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                                                           
                                                                           strStartTime = [NSString stringWithFormat:@"%@##%@",subMainTime,subStartTime];
                                                                           strEndTime = [NSString stringWithFormat:@"%@##%@",subMainTime,subEndTime];
                                                                           
                                                                           
                                                                           NSDictionary *ob1 = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@-%@",self.interview.interviewCode,[CachingService sharedInstance].currentUserId],@"referenceId",
                                                                                                [CachingService sharedInstance].branchCode,@"co",
                                                                                                [CachingService sharedInstance].currentUserIdWithZero,@"agentcode",
                                                                                                @"09",@"typeId",
                                                                                                NSLocalizedString(@"EOP", @""),@"typeName",
                                                                                                self.interview.interviewSessionName,@"subject",
                                                                                                @"",@"description" ,
                                                                                                self.interview.location,@"address",
                                                                                                strStartTime,@"startTime",
                                                                                                strEndTime,@"endTime",
                                                                                                @"N",@"openFlag",nil];
                                                                           NSArray *arr = [[NSArray alloc]initWithObjects:ob1,nil];
                                                                           NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:kCMSVersionConstant,@"version",@"A",@"option",@"03",@"from",arr,@"events",nil];
                                                                           NSDictionary *postDictionary = [[NSDictionary alloc]initWithObjectsAndKeys:dict,@"RequestMessage",nil];
                                                                           NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDictionary options:NSJSONWritingPrettyPrinted error:nil];
                                                                           NSString *jsonString;
                                                                           if (! jsonData) {
                                                                           } else {
                                                                               jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                                                           }
                                                                           
                                                                           jsonString=[jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                                                                           jsonString=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                                                                           jsonString=[jsonString stringByReplacingOccurrencesOfString:@"##" withString:@" "];
                                                                           
                                                                           
                                                                           AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
                                                                           NSString *encodedStr=[delegate md5StringForString:jsonString];
                                                                           
                                                                           NSString *sessionId=delegate.sessionid;
                                                                           if (sessionId == nil || [sessionId isEqualToString:@""]) {
                                                                           } else {
                                                                               AFHTTPRequestOperationManager *ASAPIManager=[[AFHTTPRequestOperationManager alloc]init];
                                                                               [ASAPIManager.securityPolicy setAllowInvalidCertificates:YES];
                                                                               [ASAPIManager.requestSerializer setValue:@"multipart/form-data;charset=UTF-8;boundary=--AIACalendar--" forHTTPHeaderField:@"Content-Type"];
                                                                               [ASAPIManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html" ,nil]];
                                                                               [ASAPIManager POST:@"https://211.144.219.243/AIATouchBE/IService.do" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                                                   [formData appendPartWithFormData:[sessionId dataUsingEncoding:NSUTF8StringEncoding] name:@"SessionId"];
                                                                                   [formData appendPartWithFormData:[@"UTF-8" dataUsingEncoding:NSUTF8StringEncoding] name:@"Encoding"];
                                                                                   [formData appendPartWithFormData:[encodedStr dataUsingEncoding:NSUTF8StringEncoding] name:@"Signature"];
                                                                                   [formData appendPartWithFormData:[@"OneCalendarEventService" dataUsingEncoding:NSUTF8StringEncoding] name:@"Service"];
                                                                                   [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding] name:@"Data"];
                                                                                   
                                                                               } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                                   NSDictionary *Resposedict=[responseObject valueForKey:@"ResponseMessage"];
                                                                                   BOOL exists = [Resposedict objectForKey:@"returnMessage"] != nil;
                                                                                   if (!exists) {
                                                                                       UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                                                                                       [alert show];
                                                                                   }
                                                                                   
                                                                               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                   UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registration",@"") message:NSLocalizedString(@"Synchronization failed, please update the entry in iCalendar system",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
                                                                                   [alert show];
                                                                               }];
                                                                           }
                                                                       }
                                                                       if(!status && !duplicate){
                                                                           NSLog(@"Error");
                                                                       }else{
                                                                           [self updateRecruitMentProgress:FALSE];
                                                                           if(duplicate){
                                                                               [DisplayAlert showAlertInControllerInView:self
                                                                                                                   title:NSLocalizedString(@"Registration",@"")
                                                                                                                 message:NSLocalizedString(@"Already registration",@"")
                                                                                                       cancelButtonTitle:NSLocalizedString(@"OK",@"")
                                                                                                           okButtonTitle:nil
                                                                                                             cancelBlock:nil
                                                                                                                 okBlock:nil];
                                                                           }else
                                                                               [self updateTotalCandidate:FALSE];
                                                                           
                                                                       }
                                                                   }
                                                                   [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
                                                               } failure:^(id responseObject, NSError *error) {
                                                                   [MBProgressHUDUpd hideAllHUDsForView:[MyAppDelegate window] animated:YES];
                                                                   [self onBtnBackClick:nil];
                                                               }];
}

- (void)updateTotalCandidate:(BOOL)isEop{
    NSString *totalRegCount = @"";
    NSError *error = nil;
    if(isEop){
        TblEOP *eop = (TblEOP *)[[CachingService sharedInstance].managedObjectContext existingObjectWithID:self.event.eopId
                                                                                                     error:&error];
        eop.registeredCount = [NSNumber numberWithInteger:[eop.registeredCount integerValue] + 1];
        totalRegCount = [eop.registeredCount stringValue];
    }else{
        self.interview.registeredCount = [NSNumber numberWithInteger:[self.interview.registeredCount integerValue] + 1];
        totalRegCount = [self.interview.registeredCount stringValue];
    }
    
    [[ContactDataProvider sharedContactDataProvider] saveLocalDBData];
    [self onBtnBackClick:nil];
    if(self.callBackBlock)
        self.callBackBlock(totalRegCount);
}

- (void)updateRecruitMentProgress:(BOOL)isEop{
    if(isEop){
        if([self.contact.candidateProcess.eopRegistrationStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
            if([self.event.eventDate isEarlierThanDate:self.contact.candidateProcess.eopRegistrationDate])
                [ApplicationFunction setUpdateUserProcess:RECURITMENT_EOP_REGISTRATION
                                                  contact:self.contact
                                            processStatus:RECURITMENT_STATUS_TRUE
                                              processDate:self.event.eventDate];
        }else
            [ApplicationFunction setUpdateUserProcess:RECURITMENT_EOP_REGISTRATION
                                              contact:self.contact
                                        processStatus:RECURITMENT_STATUS_TRUE
                                          processDate:self.event.eventDate];
    }else{
        if([self.contact.candidateProcess.companyInterviewStatus isEqualToString:RECURITMENT_STATUS_TRUE]){
            if([self.event.eventDate isEarlierThanDate:self.contact.candidateProcess.companyInterviewDate])
                [ApplicationFunction setUpdateUserProcess:RECURITMENT_COMPANY_INTERVIEW
                                                  contact:self.contact
                                            processStatus:RECURITMENT_STATUS_TRUE
                                              processDate:self.event.eventDate];
        }else
            [ApplicationFunction setUpdateUserProcess:RECURITMENT_COMPANY_INTERVIEW
                                              contact:self.contact
                                        processStatus:RECURITMENT_STATUS_TRUE
                                          processDate:self.event.eventDate];
    }
}

- (void)setPopoverView:(UIButton *)btn pickerArray:(NSArray *)pickerArray{
    NSMutableArray *pickerDataArray = [[NSMutableArray alloc] initWithObjects:@"", nil];
    [pickerDataArray addObjectsFromArray:[pickerArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    
    PickerPopoverViewController *popoverViewCtrl = [[PickerPopoverViewController alloc] initWithNibName:@"PickerPopoverViewController" bundle:nil];
    if(isEmptyString(btn.titleLabel.text))
        [btn setTitle:pickerDataArray[0] forState:UIControlStateNormal];
    else
        popoverViewCtrl.oldValue = btn.titleLabel.text;
    
    [popoverViewCtrl setPickerArray:pickerDataArray];
    [popoverViewCtrl setSelectedIndex:^(int selectedRowIndexPath) {
        [btn setTitle:pickerDataArray[selectedRowIndexPath] forState:UIControlStateNormal];
        [self.regPopoverCtrl dismissPopoverAnimated:TRUE];
    }];
    
    self.regPopoverCtrl = [[UIPopoverController alloc] initWithContentViewController:popoverViewCtrl];
    self.regPopoverCtrl.popoverContentSize = popoverViewCtrl.view.frame.size;
    [self.regPopoverCtrl presentPopoverFromRect:btn.frame
                                         inView:self.view
                       permittedArrowDirections:UIPopoverArrowDirectionRight
                                       animated:TRUE];
}

-(TblContact *)createContact 
{
    self.contactDataProvider = [ContactDataProvider sharedContactDataProvider];
    TblContact *newContact = [self.contactDataProvider createContact];
    newContact.addressCode = [NSNumber numberWithInteger:0];
    newContact.name = self.txtName.text;
    newContact.gender = self.btnGender.titleLabel.text;
    newContact.birthDate = [newContact convertBirthDateToDate:self.btnDob.titleLabel.text];
    if (self.txtEmail.text.length > 0) {
        newContact.eMailId = self.txtEmail.text;
    }
    if (self.txtTeleNumber.text.length > 0) {
        newContact.mobilePhoneNo = self.txtTeleNumber.text;
    }
    
    if (self.txtWeChat.text.length > 0) {
        newContact.weChat = self.txtWeChat.text;
    }
    newContact.agentId = [[CachingService sharedInstance] currentUserId];
    newContact.iosAddressCode = [NSString stringWithFormat:@"%@",[[newContact objectID]
                                                                  URIRepresentation]];
    [ApplicationFunction joinUngroup:newContact];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:kServerDateFormatte];
    newContact.modificationDate = [NSDate date];
    newContact.isSync = [NSNumber numberWithBool:YES];
    newContact.isDelete = [NSNumber numberWithBool:NO];
    return newContact;
}

@end
