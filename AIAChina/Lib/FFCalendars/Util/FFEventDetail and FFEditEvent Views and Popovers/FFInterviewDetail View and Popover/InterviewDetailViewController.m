//
//  InterviewDetailViewController.m
//  AIAChina
//
//  Created by AIA on 03/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "InterviewDetailViewController.h"
#import "CMSServiceManager.h"
#import "ContactEventModel.h"
#import "InterviewRegistrationViewController.h"
#import "InterviewRegCandAndAttandViewController.h"

@interface InterviewDetailViewController ()
@property (nonatomic, retain) ContactEventModel *eventModel;
@end

@implementation InterviewDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"InterView Details", @"");
    self.eventModel = [ContactEventModel sharedInstance];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
        self.preferredContentSize=CGSizeMake(340, 380);
    else
        self.presentingViewController.presentedViewController.preferredContentSize = CGSizeMake(340, 380);
}

- (void)loadData{
    self.lblDate.text = [NSDate dateToString:self.event.dateDay andFormate:kDisplayDateFormatte];
    self.lblStartTime.text = [NSDate dateToString:self.event.startDate andFormate:@"HH:mma"];
    self.lblEndTime.text = [NSDate dateToString:self.event.endDate andFormate:@"HH:mma"];
    self.txtLocation.text = self.event.location;
    self.txtInterviewMaterial.text = self.event.interviewMaterial;
    self.txtLocation.textColor = self.txtInterviewMaterial.textColor = [UIColor colorWithRed:99.0/255.0 green:99.0/255.0 blue:99.0/255.0 alpha:1.0];
    self.quata = self.event.estimatedCondidates - self.event.registeredCount;
    self.lblQuotaBalance.text = [NSString stringWithFormat:@"%d", self.quata];

    [self.btnRegister setTitle:NSLocalizedString(@"注册", @"") forState:UIControlStateNormal];
    [self getTotalRegCandidateCountApi];
}

- (void)getTotalRegCandidateCountApi{
    [MBProgressHUDUpd showHUDAddedTo:self.view animated:TRUE];
    [self.eventModel getInterviewRegisteredCandidateCount:[NSString stringWithFormat:@"%d", self.event.interviewCode] agentID:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        for (NSArray *response in responseObject) {
            self.event.totalCandidate = [NSString stringWithFormat:@"%d",[ValueParser parseInt:[response valueForKey:@"registeredCount"]]];
            [self.btnTotalCandidates setAttributedTitle:[Utility setUnderline:isEmptyString(self.event.totalCandidate) ? @"" : self.event.totalCandidate
                                                                withTextColor:self.btnTotalCandidates.currentTitleColor]
                                               forState:UIControlStateNormal];
        }
    } failure:^(id responseObject, NSError *error) {
        [MBProgressHUDUpd hideAllHUDsForView:self.view animated:TRUE];
        [self.btnTotalCandidates setAttributedTitle:[Utility setUnderline:isEmptyString(self.event.totalCandidate) ? @"" : self.event.totalCandidate
                                                            withTextColor:self.btnTotalCandidates.currentTitleColor]
                                           forState:UIControlStateNormal];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"InterviewDetailToRegistration"]){
        InterviewRegistrationViewController *interViewRegCtrl = segue.destinationViewController;
        interViewRegCtrl.isEop = FALSE;
        interViewRegCtrl.event = self.event;
        [interViewRegCtrl setCallBackBlock:^(NSString *regCount) {
            self.event.totalCandidate = regCount;
            [self.btnTotalCandidates setAttributedTitle:[Utility setUnderline:isEmptyString(self.event.totalCandidate) ? @"" : self.event.totalCandidate
                                                                withTextColor:self.btnTotalCandidates.currentTitleColor]
                                               forState:UIControlStateNormal];
        }];
    }else if([segue.identifier isEqualToString:@"InterviewDetailToAttand"]){
        InterviewRegCandAndAttandViewController *interviewRegCandAndAttandViewController = segue.destinationViewController;
        interviewRegCandAndAttandViewController.interviewId = self.event.interviewId;
    }
}

- (IBAction)onBtnTotalCandidatesClick:(id)sender {
    if([self.btnTotalCandidates.titleLabel.text integerValue] > 0){
        [self.eventModel getInterviewRegisteredCandidate:[NSString stringWithFormat:@"%d",self.event.interviewCode] agentID:[CachingService sharedInstance].currentUserId success:^(id responseObject) {
            [self performSegueWithIdentifier:@"InterviewDetailToAttand" sender:self];
        } failure:^(id responseObject, NSError *error) {
            [self performSegueWithIdentifier:@"InterviewDetailToAttand" sender:self];
        }];
    }
}

- (IBAction)onBtnRegisterClick:(id)sender {
    if(self.quata == 0){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:NSLocalizedString(@"Don't available quota", @"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:nil
                                          okBlock:nil];
        return;
    }
    [self performSegueWithIdentifier:@"InterviewDetailToRegistration" sender:self];
}

- (IBAction)onBtnLocationClick:(id)sender {
    NSString *address = self.txtLocation.text;
    [self getSearchLocation:address];
}

@end
