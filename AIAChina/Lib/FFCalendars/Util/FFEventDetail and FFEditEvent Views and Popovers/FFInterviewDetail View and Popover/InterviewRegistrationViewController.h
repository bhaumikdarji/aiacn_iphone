//
//  InterviewRegistrationViewController.h
//  AIAChina
//
//  Created by AIA on 10/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
#import "MLPAutoCompleteTextField.h"

@interface InterviewRegistrationViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *txtName;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UIButton *btnGender;
@property (weak, nonatomic) IBOutlet UILabel *lblDob;
@property (weak, nonatomic) IBOutlet UIButton *btnDob;
@property (weak, nonatomic) IBOutlet UIButton *btnEducation;
@property (weak, nonatomic) IBOutlet UITextField *txtWeChat;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtId;
@property (weak, nonatomic) IBOutlet UITextField *txtTeleNumber;

@property (strong, nonatomic) NSArray *allContactNameArray;
@property (nonatomic) BOOL isEop;
@property (strong, nonatomic) UIPopoverController *regPopoverCtrl;
@property (strong, nonatomic) TblContact *contact;
@property (strong, nonatomic) TblInterview *interview;
@property (strong, nonatomic) FFEvent *event;

@property (copy, nonatomic) void(^callBackBlock)(NSString *totalRegCount);

- (IBAction)onBtnGenderClick:(id)sender;
- (IBAction)onBtnDobClick:(id)sender;
- (IBAction)onBtnEducationClick:(id)sender;
- (IBAction)onBtnConfirmClick:(id)sender;
- (IBAction)onBtnBackClick:(id)sender;

@end
