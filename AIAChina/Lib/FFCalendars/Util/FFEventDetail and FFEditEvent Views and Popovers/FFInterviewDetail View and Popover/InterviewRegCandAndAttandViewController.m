//
//  InterviewRegCandAndAttandViewController.m
//  AIAChina
//
//  Created by AIA on 04/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "InterviewRegCandAndAttandViewController.h"
#import "InterviewRegCandAndAttandCell.h"

@interface InterviewRegCandAndAttandViewController ()

@end

@implementation InterviewRegCandAndAttandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Interview Registered Candidates & Attendance", @"");
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleBordered target:self action:@selector(popView)];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    TblInterview *interview = (TblInterview *)[[CachingService sharedInstance].managedObjectContext existingObjectWithID:self.interviewId
                                                                                                 error:nil];
    self.interviewCandidateArray = [DbUtils fetchAllObject:@"TblInterviewCandidate"
                                                andPredict:[NSPredicate predicateWithFormat:@"interviewCode=%@",interview.interviewCode]
                                         andSortDescriptor:nil
                                      managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    
    if(self.interviewCandidateArray.count<=0){
        [DisplayAlert showAlertInControllerInView:nil
                                            title:NSLocalizedString(@"Error", @"")
                                          message:NSLocalizedString(@"Not currently connected to the Internet",@"")
                                cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                    okButtonTitle:nil
                                      cancelBlock:^(UIAlertAction *action) {
                                          if([self.parentViewController isKindOfClass:[UINavigationController class]]){
                                              [self.navigationController popViewControllerAnimated:YES];
                                          }else if(self.parentPopOver){
                                              [self.parentPopOver dismissPopoverAnimated:TRUE];
                                          }
                                      }
                                          okBlock:nil];
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    self.preferredContentSize = CGSizeMake(550, 350);
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.interviewCandidateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    InterviewRegCandAndAttandCell *cell = (InterviewRegCandAndAttandCell *)[tableView dequeueReusableCellWithIdentifier:@"InterviewRegisterCell"];
    TblInterviewCandidate *interviewCandidate = self.interviewCandidateArray[indexPath.row];
    [cell loadInterviewCandidateData:interviewCandidate];
    return cell;
}
- (void) popView
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
