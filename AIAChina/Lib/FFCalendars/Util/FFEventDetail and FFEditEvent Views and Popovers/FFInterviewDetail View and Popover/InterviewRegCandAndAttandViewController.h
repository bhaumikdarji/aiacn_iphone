//
//  InterviewRegCandAndAttandViewController.h
//  AIAChina
//
//  Created by AIA on 04/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"

@interface InterviewRegCandAndAttandViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UITableView *tblInterview;
@property (weak, nonatomic) UIPopoverController *parentPopOver;

@property (strong, nonatomic) NSArray *interviewCandidateArray;
@property (strong, nonatomic) TblInterviewID *interviewId;

@end
