//
//  TrainingDetailViewController.m
//  AIAChina
//
//  Created by MacMini on 10/09/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "TrainingDetailViewController.h"

@interface TrainingDetailViewController ()

@end

@implementation TrainingDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTrainingData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadTrainingData{
    self.lblStartDate.text = [DateUtils dateToString:self.event.startDate andFormate:kDisplayDateFormatte];

    self.lblCourseCode.text = self.event.courseCode;
    self.lblCourseType.text = self.event.courseType;
    NSMutableString *courseType = [NSMutableString string];
    if ([self.event.courseType rangeOfString:@"1"].location != NSNotFound) {
        if (courseType.length > 0) {
            [courseType appendString:@","];
        }
        [courseType appendString:@"SA"];
    }

    if ([self.event.courseType rangeOfString:@"2"].location != NSNotFound)
    {
        if (courseType.length > 0) {
            [courseType appendString:@","];
        }
        [courseType appendString:@"HA"];
    }
    if(courseType.length == 0) {
        courseType  = self.event.courseType;
    }
    self.lblCourseType.text = courseType;
    
    self.lblCourseName.text = [NSString stringWithFormat:@"%@ %@",courseType,@"岗前培训班"];
    self.lblCurriculumCode.text = self.event.curriculamCode;
}

- (IBAction)onBtnDoneClick:(id)sender {
    if(self.doneTrainingDetail)
        self.doneTrainingDetail();
}


@end
