//
//  TrainingDetailViewController.h
//  AIAChina
//
//  Created by MacMini on 10/09/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrainingDetailViewController : AIAChinaViewController

@property (weak, nonatomic) IBOutlet UILabel *lblCourseCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCourseName;
@property (weak, nonatomic) IBOutlet UILabel *lblCourseType;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCurriculumCode;
@property (copy, nonatomic) void(^doneTrainingDetail)();
@property (strong, nonatomic) FFEvent *event;

- (void)loadTrainingData;

- (IBAction)onBtnDoneClick:(id)sender;

@end
