//
//  Utils.h
//  iPos
//
//  Created by Rickey Tom on 2015-02-24.
//  Copyright (c) 2015 quixcreations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject

 /*
  * Returns the name in dd-MMM-YYYY format given a number that represents the
  * number of days since Jan 1, 1900.
  */
+(NSString *) dateFromNumber:(NSNumber *) dateNumber;

+(NSDateFormatter *) dateFormatter;

+ (id)loadDataFromPath:(NSString *)path;

+(id)loadJsonDataFromPath:(NSString *) path;

/**
 * Returns true if the email string appears to be valid.
 **/
+(bool) IsValidEmail:(NSString *) emailStringToCheck;

/**
 * Gets a NSString given the NSDate.
 * The date outputted will have the format dd-MMM-yyyy. e.g., 10-Oct-2015.
 **/
+(NSString *) stringFromDate:(NSDate *) date;

/**
 * Formats the double (e.g., 12345.67) as currency (e.g., $12,345.67).
 **/
+(NSString *) currencyFromNumber:(double) number;

/**
 * Get the first date for the first day of the next month.
 * If this is April 3, 2015 method shall return May 1, 2015.
 **/
+(NSString *) dateOfFirstDayOfNextMonth;

/**
 * Returns the age of last birthdate. If less then 1 year, then method returns a value in days.
 * e.g., 39 years
 *       14 days.
 **/
+(NSString *) ageLastBirhDate:(NSDate *) birthdate relativeToEffectiveDate:(NSDate *) effectiveDate;

+(long) ageLastBirhDateInYears:(NSDate *) birthdate;

+(void) displayMessage:(NSString *) msg withTitle:(NSString *) title;

/**
 * Returns true if the phone is valid.
 **/
+(bool) isValidPhoneNumber:(NSString *) phone;

@end
