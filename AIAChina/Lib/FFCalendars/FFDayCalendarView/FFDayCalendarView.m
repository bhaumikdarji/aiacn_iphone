//
//  FFDayCalendarView.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/23/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFDayCalendarView.h"
#import "FFAllDayCalendarTableView.h"
#import "FFDayHeaderCollectionView.h"
#import "FFDayScrollView.h"

#import "FFEventDetailView.h"
#import "FFEditEventView.h"

#import "FFImportantFilesForCalendar.h"
#import "CreateEventViewController.h"
#import "Utility.h"
#import "Constants.h"

#define EDIT_EVENT_VIEW_TAG 111

@interface FFDayCalendarView () <FFDayCellProtocol, FFEditEventViewProtocol, FFEventDetailViewProtocol, FFDayHeaderCollectionViewProtocol, FFDayCollectionViewProtocol, UIGestureRecognizerDelegate>
@property (nonatomic, strong) FFDayHeaderCollectionView *collectionViewHeaderDay;
@property (nonatomic, strong) FFDayScrollView *dayContainerScroll;
@property (nonatomic, strong) FFEventDetailView *viewDetail;
@property (nonatomic, strong) FFEditEventView *viewEdit;
@property (nonatomic, strong) CreateEventViewController *editEventViewCtrl ;
@property (nonatomic, strong) UILabel *lblNoSelected;
@property (nonatomic, strong) FFAllDayCalendarTableView *allDayTableViewHeader;
@property (nonatomic) BOOL boolAnimate;

@end

@implementation FFDayCalendarView

#pragma mark - Synthesize

@synthesize dictEvents;
@synthesize collectionViewHeaderDay;
@synthesize dayContainerScroll;
@synthesize viewDetail;
@synthesize viewEdit;
@synthesize protocol;
@synthesize lblNoSelected;
@synthesize boolAnimate;

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dateChanged:) name:DATE_MANAGER_DATE_CHANGED object:nil];
        [self setBackgroundColor:[UIColor whiteColor]];
        [self performSelector:@selector(setNoEventPlaceholder) withObject:nil afterDelay:1.0];
       
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [gesture setDelegate:self];
        [self addGestureRecognizer:gesture];
        
        UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLong:)];
        [longGesture setDelegate:self];
        [longGesture setMinimumPressDuration:.5];
//        [self addGestureRecognizer:longGesture];
        
        boolAnimate = NO;
        
        [self setAutoresizingMask: AR_WIDTH_HEIGHT];
    }
    return self;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setDictEvents:(NSMutableDictionary *)_dictEvents {
    
    dictEvents = _dictEvents;

    if (dayContainerScroll) [dayContainerScroll removeFromSuperview];
    
    collectionViewHeaderDay = [[FFDayHeaderCollectionView alloc] initWithFrame:CGRectMake(0., 0., self.frame.size.width, HEADER_HEIGHT_SCROLL)];
    [collectionViewHeaderDay setProtocol:self];
    [collectionViewHeaderDay scrollToDate:[[FFDateManager sharedManager] currentDate]];
    [self addSubview:collectionViewHeaderDay];
    
    self.allDayTableViewHeader = [[FFAllDayCalendarTableView alloc]initWithFrame:CGRectMake(0., HEADER_HEIGHT_SCROLL, self.frame.size.width/3, HEADER_HEIGHT_SCROLL)];
    self.allDayTableViewHeader.backgroundColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0];
    [self addSubview:self.allDayTableViewHeader];
    
    dayContainerScroll = [[FFDayScrollView alloc] initWithFrame:CGRectMake(0, 2*HEADER_HEIGHT_SCROLL, self.frame.size.width/2., self.frame.size.height-140)];
    [self addSubview:dayContainerScroll];

    [dayContainerScroll setDictEvents:dictEvents];
    [dayContainerScroll.collectionViewDay setProtocol:self];
    
    [self.allDayTableViewHeader setDictEvents:dictEvents];
    [self.allDayTableViewHeader setProtocol:self];
}

- (void)refresh{
    [dayContainerScroll.collectionViewDay reloadData];
}

#pragma mark - Invalidate Layout

- (void)invalidateLayout {
    [collectionViewHeaderDay.collectionViewLayout invalidateLayout];
    [dayContainerScroll.collectionViewDay.collectionViewLayout invalidateLayout];
    [self dateChanged:nil];
}

#pragma mark - FFDateManager Notification

- (void)dateChanged:(NSNotification *)not {
    
    [dayContainerScroll.collectionViewDay reloadData];
    [self.allDayTableViewHeader.tableView reloadData];
    [dayContainerScroll.collectionViewDay scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:[NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]].day-1+7 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:boolAnimate];
    
    float y = [NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]].hour;
    [dayContainerScroll scrollRectToVisible:CGRectMake(0, y*dayContainerScroll.frame.size.height - 10, dayContainerScroll.frame.size.width, dayContainerScroll.frame.size.height) animated:NO];
    
    boolAnimate = NO;
    
    [self updateHeader];
    
    if ([NSDate isTheSameDateTheCompA:[NSDate componentsOfCurrentDate] compB:[NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]]]) {
        [dayContainerScroll scrollRectToVisible:CGRectMake(0, dayContainerScroll.labelWithActualHour.frame.origin.y, dayContainerScroll.frame.size.width, dayContainerScroll.frame.size.height) animated:YES];
    }
}

#pragma mark - Long Gesture

- (void)handleLong:(UILongPressGestureRecognizer *)recognizer{
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        [self handleTap:nil];
        [viewDetail removeFromSuperview];
        viewDetail = nil;
        
        [lblNoSelected removeFromSuperview];
        lblNoSelected = nil;
        
        __weak typeof(self) weakSelf = self;
        self.editEventViewCtrl = [[CreateEventViewController alloc] initWithNibName:@"CreateEventViewController" bundle:nil];
        self.editEventViewCtrl.isDayView = TRUE;
        self.editEventViewCtrl.view.frame = CGRectMake(self.frame.size.width/2. + 20, HEADER_HEIGHT_SCROLL, self.frame.size.width/2. - 20, self.frame.size.height-HEADER_HEIGHT_SCROLL);
        [self.editEventViewCtrl setCreateEvent:^(NSDictionary *eventDic, MBProgressHUDUpd *hud) {
            FFEvent *event = [[FFEvent alloc] init];
            [event createEvent:eventDic];
            [weakSelf saveEvent:event];
            [weakSelf handleTap:nil];
            [hud hide:TRUE];
        }];
        
        [self.editEventViewCtrl setCancelEvent:^(UIView *view){
            [weakSelf handleTap:nil];
        }];
        
        self.editEventViewCtrl.view.tag = EDIT_EVENT_VIEW_TAG;
        [self addSubview:self.editEventViewCtrl.view];
    }
}

#pragma mark - Tap Gesture

- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    [self setNoEventPlaceholder];
    for (UIView *subviews in self.subviews) {
        if ([subviews isKindOfClass:[FFEventDetailView class]] || subviews.tag == EDIT_EVENT_VIEW_TAG) {
            [subviews removeFromSuperview];
        }
    }
}

#pragma mark - UIGestureRecognizer Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint point = [gestureRecognizer locationInView:self];
    return ! ((viewEdit.superview != nil && CGRectContainsPoint(viewEdit.frame, point)) ||
              (self.editEventViewCtrl.view.superview != nil && CGRectContainsPoint(self.editEventViewCtrl.view.frame, point)));
}

- (void)removeEditEventView{
    [self setNoEventPlaceholder];
    
    for (UIView *subviews in self.subviews) {
        if (subviews.tag == EDIT_EVENT_VIEW_TAG) {
            [subviews removeFromSuperview];
        }
    }
}

#pragma mark - FFDayCollectionView Protocol

- (void)updateHeader {
    [collectionViewHeaderDay reloadData];
    [collectionViewHeaderDay scrollToDate:[[FFDateManager sharedManager] currentDate]];
}

#pragma mark - FFDayHeaderCollectionView Protocol

- (void)daySelected:(NSDate *)date {
    boolAnimate = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:DATE_MANAGER_MONTH_DATE_TAP
                                                        object:date];
    [[NSNotificationCenter defaultCenter] postNotificationName:DATE_MANAGER_TODAY_DATE
                                                        object:date];
}

- (void)longPressEvent:(FFEvent *)_event cell:(UICollectionViewCell *)cell{
//    Exiting event long press
}

#pragma mark - FFDayCell Protocol

- (void)showViewDetailsWithEvent:(FFEvent *)_event cell:(UICollectionViewCell *)cell {

    for (UIView *subviews in self.subviews) {
        if ([subviews isKindOfClass:[FFEventDetailView class]] || subviews.tag == EDIT_EVENT_VIEW_TAG) {
            [subviews removeFromSuperview];
        }
    }
    
    [lblNoSelected performSelectorOnMainThread:@selector(removeFromSuperview) withObject:nil waitUntilDone:NO];
    
    [viewDetail removeFromSuperview];
    viewDetail = [[FFEventDetailView alloc] initWithFrame:CGRectMake(self.frame.size.width/2., HEADER_HEIGHT_SCROLL, self.frame.size.width/2., self.frame.size.height-HEADER_HEIGHT_SCROLL) event:_event];
    [viewDetail setAutoresizingMask:AR_WIDTH_HEIGHT | UIViewAutoresizingFlexibleLeftMargin];
    [viewDetail setProtocol:self];
    [self addSubview:viewDetail];
}

#pragma mark - FFEventDetailView Protocol

- (void)showEditViewWithEvent:(FFEvent *)_event {
    
    
    __weak typeof(self) weakSelf = self;
    
    self.editEventViewCtrl = [[CreateEventViewController alloc] initWithNibName:@"CreateEventViewController" bundle:nil];
    self.editEventViewCtrl.event = _event;
    self.editEventViewCtrl.isDayView = TRUE;
    self.editEventViewCtrl.view.frame = CGRectMake(self.frame.size.width/2. + 20, HEADER_HEIGHT_SCROLL, self.frame.size.width/2. - 20, self.frame.size.height - HEADER_HEIGHT_SCROLL);
    
    [self.editEventViewCtrl setEditEvent:^(FFEvent *event, NSDate *oldStartDate, NSDate *oldEndDate, MBProgressHUDUpd *hud) {
        if([oldStartDate isEqualToDateIgnoringTime:oldEndDate]){
            [weakSelf deleteEvent:event];
        }else{
            [weakSelf deleteEvent:event startDate:oldStartDate andEndDate:oldEndDate];
        }
        [weakSelf saveEvent:event];
        [hud hide:TRUE];
    }];
    
    [self.editEventViewCtrl setDeleteEvent:^(FFEvent *event, NSDate *oldStartDate, NSDate *oldEndDate) {
        if([oldStartDate isEqualToDateIgnoringTime:oldEndDate]){
            [weakSelf deleteEvent:event];
        }else{
            [weakSelf deleteEvent:event startDate:oldStartDate andEndDate:oldEndDate];
        }
        [ApplicationFunction deleteCalManualEvent:event];
        [weakSelf setNoEventPlaceholder];
    }];

    [self.editEventViewCtrl setCancelEvent:^(UIView *view){
        [weakSelf removeThisView:view];
        [weakSelf setNoEventPlaceholder];
    }];
    self.editEventViewCtrl.view.tag = EDIT_EVENT_VIEW_TAG;
    [self addSubview:self.editEventViewCtrl.view];
        
    [viewDetail removeFromSuperview];
    viewDetail = nil;
}

- (void)showViewPortfolio:(NSString *)type andDate:(NSDate *)date{
    NSDictionary *dic = @{@"POLICY_TYPE":type,
                          @"DATE":date};
//    [[NSNotificationCenter defaultCenter] postNotificationName:PORTFOLIYO_EVENT_NOTIFICATION
//                                                        object:dic];
}

#pragma mark - FFEditEventView Protocol

- (void)saveEvent:(FFEvent *)_event {
    self.dictEvents = [_event editEvent:_event dicEvent:self.dictEvents];
    [self removeThisView:self.editEventViewCtrl.view];
}

- (void)deleteEvent:(FFEvent *)_event {
    NSMutableArray *arrayEvents = [dictEvents objectForKey:_event.dateDay];
    [arrayEvents removeObject:_event];
    if (arrayEvents.count == 0) {
        [dictEvents removeObjectForKey:_event.dateDay];
    }
    
    if (protocol != nil && [protocol respondsToSelector:@selector(setNewDictionary:)]) {
        [protocol setNewDictionary:dictEvents];
    } else {
        [dayContainerScroll.collectionViewDay reloadData];
    }
    [self removeThisView:self.editEventViewCtrl.view];
    [viewDetail removeFromSuperview];
    viewDetail = nil;
}

- (void)deleteEvent:(FFEvent *)_event startDate:(NSDate *)sDate andEndDate:(NSDate *)eDate{
    dictEvents = [_event deleteMultipleEvent:_event
                                    dicEvent:dictEvents
                                oldStartDate:sDate
                                  oldEndDate:eDate];
    if (protocol != nil && [protocol respondsToSelector:@selector(setNewDictionary:)]) {
        [protocol setNewDictionary:dictEvents];
    } else {
        [dayContainerScroll.collectionViewDay reloadData];
    }
    [self removeThisView:self.editEventViewCtrl.view];
    [viewDetail removeFromSuperview];
    viewDetail = nil;
}

- (void)removeThisView:(UIView *)view {
    
    [view removeFromSuperview];
    view = nil;
}

- (void)setNoEventPlaceholder{
    
    [lblNoSelected performSelectorOnMainThread:@selector(removeFromSuperview) withObject:nil waitUntilDone:NO];
    
    lblNoSelected = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width/2., HEADER_HEIGHT_SCROLL, self.frame.size.width/2., self.frame.size.height-HEADER_HEIGHT_SCROLL)];
    lblNoSelected.text = NSLocalizedString(@"No selected event", @"");
    lblNoSelected.textAlignment = NSTextAlignmentCenter;
    lblNoSelected.textColor = [UIColor grayColor];
    lblNoSelected.font = [UIFont boldSystemFontOfSize:lblNoSelected.font.pointSize];
    
    [self addSubview:lblNoSelected];
}

@end
