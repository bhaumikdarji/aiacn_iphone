//
//  FFDayHeaderCell.m
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/26/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFDayHeaderCell.h"

#import "FFImportantFilesForCalendar.h"
@implementation FFDayHeaderCell

@synthesize button;
@synthesize date;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        HeaderCell

        self.image = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2+5, 20., 30, 30)];
        self.image.backgroundColor = [UIColor clearColor];
        [self addSubview:self.image];
        
        self.lblDay = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., self.frame.size.width/2, self.frame.size.height)];
        self.lblDay.backgroundColor = [UIColor clearColor];
        self.lblDay.textAlignment = NSTextAlignmentRight;
        [self addSubview:self.lblDay];
        
        self.lblDate = [[UILabel alloc] initWithFrame:CGRectMake(self.lblDay.frame.size.width + 10, 0., self.frame.size.width/2, self.frame.size.height)];
        self.lblDate.backgroundColor = [UIColor clearColor];
        [self addSubview:self.lblDate];
       
        self.lblcurrentDateSelection = [[UILabel alloc] initWithFrame:CGRectMake(0., self.frame.size.height - 2, self.frame.size.width, 2)];
        self.lblcurrentDateSelection.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:33.0/255.0 blue:8.0/255.0 alpha:1.0];
        self.lblcurrentDateSelection.hidden = TRUE;
        [self addSubview:self.lblcurrentDateSelection];
        
        button = [[FFDayHeaderButton alloc] initWithFrame:CGRectMake(0., 0., self.frame.size.width, self.frame.size.height)];
        button.protocol = self;
        [self addSubview:button];
        
        [self setAutoresizingMask:AR_LEFT_RIGHT | UIViewAutoresizingFlexibleWidth];
    }
    return self;
}

- (void)setDate:(NSDate *)_date {
    self.lblcurrentDateSelection.hidden = TRUE;
    if ([NSDate isTheSameDateTheCompA:[NSDate componentsOfCurrentDate] compB:[NSDate componentsOfDate:_date]]) {
         self.lblcurrentDateSelection.hidden = FALSE;
    }
    
    date = _date;
    [button setDate:_date];
}

- (void)selectedEvent:(BOOL)selected{
    if(selected){
        [self.image setImage:[UIImage imageNamed:@"ic_red_circle"]];
        self.lblDay.textColor = [UIColor colorWithRed:166.0/255.0 green:166.0/255.0 blue:166.0/255.0 alpha:1.0];
        self.lblDate.textColor = [UIColor whiteColor];
    }else{
        self.image.image = nil;
        self.lblDay.textColor = [UIColor colorWithRed:76.0/255.0 green:76.0/255.0 blue:76.0/255.0 alpha:1.0];
        self.lblDate.textColor = [UIColor colorWithRed:76.0/255.0 green:76.0/255.0 blue:76.0/255.0 alpha:1.0];
    }
}

@end
