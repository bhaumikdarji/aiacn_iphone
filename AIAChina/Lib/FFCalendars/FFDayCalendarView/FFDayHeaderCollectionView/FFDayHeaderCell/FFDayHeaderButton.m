//
//  FFDayHeaderButton.m
//  FFCalendar
//
//  Created by Felipe Rocha on 19/02/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import "FFDayHeaderButton.h"

#import "FFImportantFilesForCalendar.h"

static UIImage *imageCircleRed;
static UIImage *imageCircleBlack;

@implementation FFDayHeaderButton

#pragma mark - Synthesize

@synthesize date;
@synthesize protocol;

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [[UIImageView appearanceWhenContainedIn:[FFDayHeaderButton class], nil] setContentMode:UIViewContentModeScaleAspectFit];
        
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentMode:UIViewContentModeScaleAspectFit];

        if (!imageCircleBlack) {
//            imageCircleRed = [UIImage imageNamed:@"ic_red_circle"];
        }
        
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    }
    return self;
}

#pragma mark - Set Public Property

- (void)setSelected:(BOOL)selected {
    [self setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    if (protocol && [protocol respondsToSelector:@selector(selectedEvent:)]) {
        [protocol selectedEvent:selected];
    }
}

@end
