//
//  FFAllDayCalendarTableView.m
//  AddressBook
//
//  Created by smitesh on 2015-07-02.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import "FFAllDayCalendarTableView.h"
#import "FFImportantFilesForCalendar.h"
#import "DbUtils.h"
#import "Constants.h"
#import "ActivityType.h"
#import "FFEvent.h"
#import "Utils.h"
#import "TblContact.h"
#import "Utility.h"
#import "CachingService.h"
#import "FFBlueButton.h"
#import "FFImportantFilesForCalendar.h"
#import "ActivityType.h"
#import "Utils.h"
@interface FFAllDayCalendarTableView ()<UIGestureRecognizerDelegate>
@property (nonatomic, strong) FFBlueButton *_button;
@property (nonatomic, strong) FFBlueButton *button;
@end
@implementation FFAllDayCalendarTableView

@synthesize dictEvents;
@synthesize protocol;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.dayEventArray = [[NSMutableArray alloc]init];
        self.dictAllEvents = [[NSMutableDictionary alloc]init];
        self.label = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 23.0, 70.0, 30.0)];
        self.label.text = NSLocalizedString(@"All-day", @"");
        [self addSubview:self.label];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dateManagerMonthDateTap:)
                                                     name:DATE_MANAGER_MONTH_DATE_TAP
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(filterActivityModeNotification:)
                                                     name:DATE_MANAGER_FILTER_ACTIVITY_MODE
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(todayClick:)
                                                     name:DATE_MANAGER_TODAY_DATE
                                                   object:nil];
        self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(70.0, 0.0, self.frame.size.width+50.0f, self.frame.size.height) style:UITableViewStylePlain];
        self.tableView.delegate = self;
       self.tableView.dataSource = self;
        self.tableView.separatorColor = [UIColor clearColor];
        [self.tableView setShowsHorizontalScrollIndicator:NO];
        [self.tableView setShowsVerticalScrollIndicator:NO];
        [self.tableView setBackgroundColor:[UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0]];
        self.filterDate = [[FFDateManager sharedManager] currentDate];
        [self addSubview:self.tableView];
        
    }
    return self;
}
- (void)setDictEvents:(NSMutableDictionary *)_dictEvents
{
    self.dictAllEvents = _dictEvents;
    //NSDateComponents *comp = [NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]];
    if (isEmptyString([ActivityType sharedActivity].selectedMode))
    {
        [self loadEvents:@"All"];
    }
    else
    {
        [self loadEvents:[ActivityType sharedActivity].selectedMode];
    }
    [self.tableView reloadData];
    
   
    
    
}
- (void)addAllDayEvents:(NSArray *)arrayEvents withActivityType:(NSString *)activityType
{
    [self.dayEventArray removeAllObjects];
    if (arrayEvents)
    {
        
        for (FFEvent *event in arrayEvents)
        {
            if (isEmptyString(activityType) || [activityType isEqualToString:@"All"])
            {
                if (event.allDay)
                {
                    [self.dayEventArray addObject:event];
                }
            }
            else
            {
//                if ([activityType isEqualToString:ACTIVITY_BUSINESS] || [activityType isEqualToString:ACTIVITY_OFFICE_WORK] || [activityType isEqualToString:ACTIVITY_PERSONAL] || [activityType isEqualToString:ACTIVITY_OPPORTUNITY])
//                {
//                    NSArray *activityModes = [[ActivityType sharedActivity] getActivityModeToType:[ActivityType sharedActivity].selectedMode];
//                    if ([activityModes containsObject:event.activityType])
//                    {
//                        if (event.allDay)
//                        {
//                            [self.dayEventArray addObject:event];
//                        }
//                    }
//                }
//                else
//                {
                    if ([event.activityType isEqualToString:activityType])
                    {
                        if (event.allDay)
                        {
                            [self.dayEventArray addObject:event];
                        }
                    }
//                }
                
                
            }
            
        }
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.dayEventArray count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FFEvent *event = [self.dayEventArray objectAtIndex:[indexPath row]];
    NSString *text = event.eventName;
    
    CGSize constraint = CGSizeMake(230, 20000.0f);
    
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    
    CGFloat height = MAX(size.height, 30.0f);
    
    return height ;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    FFEvent *event = [self.dayEventArray objectAtIndex:indexPath.row];
    _button = [[FFBlueButton alloc] initWithFrame:CGRectMake(10, 3, 230, 20)];
    CGSize constraintSize;
    _button.titleLabel.numberOfLines = 3;
    constraintSize.width = 230;
    
    constraintSize.height = MAXFLOAT;
    [_button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    if([event.activityType isEqualToString:ACTIVITY_INT])
        [_button setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(event.eventName, @"")] forState:UIControlStateNormal];
    else if([event.activityType isEqualToString:ACTIVITY_ICALENDAR]){
        NSString *str = @"iCalendar";
        NSString *stragentcode = [NSString stringWithFormat:@"%@",[CachingService sharedInstance].currentUserIdWithZero];
        NSString *strdate = [NSString stringWithFormat:@"%@",event.startDate];
        NSString *newString = [strdate substringToIndex:10];
        NSArray *eventArr = [[NSArray alloc]init];
        eventArr = [DbUtils fetchAllObject:@"ICalendarEvents"
                                andPredict:[NSPredicate predicateWithFormat:@"startTime contains[cd] %@ AND agentCode == %@",newString,stragentcode]
                         andSortDescriptor:nil
                      managedObjectContext:[CachingService sharedInstance].managedObjectContext];
        
        NSLog(@"Count is %d",eventArr.count);
        if (eventArr.count == 0) {
            //Written this condition to
            //avoid to show 0 iCalender as a count
        } else {
            [_button setTitle:[NSString stringWithFormat:@"%d %@",eventArr.count,NSLocalizedString(str, @"")] forState:UIControlStateNormal];
        }
    } else {
        [_button setTitle:[NSString stringWithFormat:@"%@",event.eventName] forState:UIControlStateNormal];
    }
    if([event.activityType isEqualToString:ACTIVITY_ICALENDAR]){
        [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    } else {
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    [_button setEvent:event];
    [_button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)];
    [_button sizeToFit];
    //rgb(135,206,250)
    _button.backgroundColor = [[ActivityType sharedActivity] setBgColorOnActivityType:event.activityType];
    //_button.backgroundColor = [UIColor clearColor];
    
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLong:)];
    [longGesture setDelegate:self];
    [longGesture setMinimumPressDuration:.5];
    [_button addGestureRecognizer:longGesture];
    [cell setBackgroundColor:[UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0]];
    [cell.contentView addSubview:_button];
    return cell;
}
- (void)cleanSubHeaderCell
{
    [self.allDayEventArray removeAllObjects];
    
}
- (void)loadEvents:(NSString *)activityType
{
    NSArray *array = [self.dictAllEvents objectForKey:self.filterDate];
    [self addAllDayEvents:array withActivityType:activityType];
    [self.tableView reloadData];
    
    
    
}
- (void)dateManagerMonthDateTap:(NSNotification *)notification{
    
    self.filterDate = [notification object];
    
    [self.dayEventArray removeAllObjects];
    if (isEmptyString([ActivityType sharedActivity].selectedMode))
    {
        [self loadEvents:@"All"];
    }
    else
    {
        [self loadEvents:[ActivityType sharedActivity].selectedMode];
    }
    [self.tableView reloadData];
    
}
- (IBAction)buttonAction:(id)sender {
    
    _button = (FFBlueButton *)sender;
    
    if (protocol != nil && [protocol respondsToSelector:@selector(showViewDetailsWithEvent:cell:)]) {
        [protocol showViewDetailsWithEvent:_button.event cell:nil];
    }
}

- (void)handleLong:(UILongPressGestureRecognizer *)recognizer{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        FFBlueButton *btnLongPress = (FFBlueButton *)recognizer.view;
        if(protocol != nil && [protocol respondsToSelector:@selector(longPressEvent:cell:)]){
            [protocol longPressEvent:btnLongPress.event cell:nil];
        }
    }
}
- (void)filterActivityModeNotification:(NSNotification *)notification
{
    
    if (isEmptyString([ActivityType sharedActivity].selectedMode))
    {
        [self loadEvents:@"All"];
    }
    else
    {
        [self loadEvents:[ActivityType sharedActivity].selectedMode];
    }
    
}
- (void)todayClick:(NSNotification *)noti{
    self.filterDate = noti.object;
    if (isEmptyString([ActivityType sharedActivity].selectedMode))
    {
        [self loadEvents:@"All"];
    }
    else
    {
        [self loadEvents:[ActivityType sharedActivity].selectedMode];
    }
    [self.tableView reloadData];
}
@end
