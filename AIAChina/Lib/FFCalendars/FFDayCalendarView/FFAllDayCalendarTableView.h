//
//  FFAllDayCalendarTableView.h
//  AddressBook
//
//  Created by smitesh on 2015-07-02.
//  Copyright (c) 2015 Quix Creations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFEvent.h"
#import "FFBlueButton.h"
@protocol FFDayCellProtocol <NSObject>
- (void)showViewDetailsWithEvent:(FFEvent *)_event cell:(UICollectionViewCell *)cell;
- (void)longPressEvent:(FFEvent *)_event cell:(UICollectionViewCell *)cell;
@end
@interface FFAllDayCalendarTableView : UIView<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) id<FFDayCellProtocol> protocol;
@property (nonatomic,retain) UILabel *label;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *allDayEventArray;
@property (nonatomic, strong) NSDate *selectedDate;
@property (strong, nonatomic) NSMutableArray *dayEventArray;
@property (strong, nonatomic) NSDate *filterDate;
@property (strong, nonatomic) NSMutableDictionary *dictAllEvents;
@property (strong, nonatomic) NSMutableDictionary *dictEvents;
@end
