//
//  EOPAttendanceCell.h
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EOPAttendanceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnEventdate;
@property (weak, nonatomic) IBOutlet UIButton *btnAttendance;

- (void)loadAttendanceData:(NSDictionary *)dic;

@end
