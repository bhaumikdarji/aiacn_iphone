//
//  CompanyInterviewCell.h
//  AIAChina
//
//  Created by AIA on 07/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyInterviewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnInterviewDate;
@property (weak, nonatomic) IBOutlet UIButton *btnInterviewStatus;
@property (weak, nonatomic) IBOutlet UIImageView *registeredImageView;

- (void)loadCompanyInterviewData:(TblInterview *)interview;

@end
