//
//  EOPRegistrationCell.h
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EOPRegistrationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnEventDate;
@property (weak, nonatomic) IBOutlet UIButton *btnTopicName;
@property (weak, nonatomic) IBOutlet UILabel *lblTopic;
@property (weak, nonatomic) IBOutlet UIButton *btnRegistration;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIImageView *registeredImageView;


- (void)loadRegData:(TblEOP *)eop;

@end
