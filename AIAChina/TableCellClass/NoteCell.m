//
//  NoteCell.m
//  AIAChina
//
//  Created by iMac AIA on 20/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "NoteCell.h"

@implementation NoteCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadNoteData:(TblNotes *)note{
    self.lblActivityDetail.text = NSLocalizedString(note.activityType, @"");
    self.lblDateDetail.text = [NSDate dateToString:note.activityDate andFormate:kDisplayDateFormatte];;
    self.lblDescriptionDetail.text = NSLocalizedString(note.desc, @"");
}

@end
