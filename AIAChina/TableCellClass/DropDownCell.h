//
//  DropDownCell.h
//  AIAChina
//
//  Created by AIA on 30/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropDownCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDropDownValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSeprator;

- (void)loadDropDownData:(NSString *)dropdownValue;

@end
