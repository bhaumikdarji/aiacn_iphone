//
//  CalendarCell.h
//  AIAChina
//
//  Created by SCS2 on 09/09/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UILabel *lblActivity;
@property (nonatomic,strong) IBOutlet UILabel *lblName;
@property (nonatomic,strong) IBOutlet UILabel *lblTime;
@property (nonatomic,strong) IBOutlet UILabel *lblLocation;


- (void)loadiCalendarCell:(NSDictionary *)dict;
@end
