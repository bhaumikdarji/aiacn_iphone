//
//  ContactListCell.h
//  AIAChina
//
//  Created by AIA on 23/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactListCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecruitmentProgress;
@property (weak, nonatomic) IBOutlet UILabel *lblSeprator;

- (void)loadContactData:(TblContact *)contact;
- (void)loadGroupData:(TblGroup *)group;

@end
