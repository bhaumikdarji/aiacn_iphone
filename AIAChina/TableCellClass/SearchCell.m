//
//  SearchCell.m
//  AIAChina
//
//  Created by iMac SCNS on 21/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "SearchCell.h"
#import "NoteCell.h"
#import "DropDownManager.h"

@implementation SearchCell

- (void)awakeFromNib {
    // Initialization code
    [Utility setBorderAndRoundCorner:self.tableBGView
                         borderColor:[UIColor colorWithRed:236.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    
    [Utility setBorderAndRoundCorner:self.lblRecruitmentProgress
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:10.0];
    
    [Utility roundCornerProfileImage:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
                         borderWidth:2.0
                           imageView:self.imgProfile];
    
    [Utility setBorderAndRoundCorner:self.tblBorderView
                         borderColor:[UIColor colorWithRed:236.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0]
                               width:1.0
                        cornerRedius:5.0];
    
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)loadSeachData:(TblContact *)contact{
    if(contact.candidatePhoto)
        self.imgProfile.image = [UIImage imageWithData:contact.candidatePhoto];
    else
        self.imgProfile.image = nil;
    
    self.lblName.text = contact.name;
    self.lblDob.text = [DateUtils dateToString:contact.birthDate andFormate:kDisplayDateFormatte];
    self.lblGender.text = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_GENDER code:contact.gender];
    self.lblAge.text = [contact.age stringValue];
    self.lblMobileNo.text = contact.mobilePhoneNo;
    self.lblSourceofReferral.text = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_SOURCE_OF_REFERRAL code:contact.referalSource];
    self.lblRecruitmentProgress.text = [NSString stringWithFormat:@"%lld/9",contact.candidateProcess.candidateProgressValue];
}

- (void)loadSeachNoteData:(NSArray *)noteArray{
    if(noteArray){
        self.noteArray = noteArray;
        if(isEmptyString(self.sortBy))
            self.sortBy = @"activityDate";
    }
    self.noteArray = [self.noteArray sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:self.sortBy ascending:TRUE]]];
    [self.tblNote reloadData];
}

- (IBAction)onBtnActivityTypeClick:(id)sender {
    self.sortBy = @"activityType";
    [self loadSeachNoteData:nil];
}

- (IBAction)onBtnActivityDateClick:(id)sender {
    self.sortBy = @"activityDate";
    [self loadSeachNoteData:nil];
}

- (IBAction)onBtnDescClick:(id)sender {
    self.sortBy = @"desc";
    [self loadSeachNoteData:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.noteArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NoteCell *cell = (NoteCell *)[tableView dequeueReusableCellWithIdentifier:@"NoteCell" forIndexPath:indexPath];
    TblNotes *note = self.noteArray[indexPath.row];
    [cell loadNoteData:note];
    return cell;
}

@end
