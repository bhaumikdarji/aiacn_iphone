//
//  InterviewRegCandAndAttandCell.m
//  AIAChina
//
//  Created by AIA on 04/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "InterviewRegCandAndAttandCell.h"
#import "TblInterviewCandidate.h"

@implementation InterviewRegCandAndAttandCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadInterviewCandidateData:(TblInterviewCandidate *)interviewCandidate{
    [Utility setRoundCornerRedius:self.imgProfile
                     cornerRedius:self.imgProfile.frame.size.width/2];
    self.lblName.text = interviewCandidate.candidateName;


    TblContact *contact =  (TblContact *) [DbUtils fetchObject:@"TblContact"
                                                    andPredict:[NSPredicate predicateWithFormat:@"name == %@ AND agentId == %@ AND isDelete == 0", interviewCandidate.candidateName,interviewCandidate.servicingAgent]
                                             andSortDescriptor:nil
                                          managedObjectContext:[CachingService sharedInstance].managedObjectContext] ;
    if (contact) {
        self.lblRecruitmentProgress.text = [ApplicationFunction recruitmentProgress:contact];
    } else {
        self.lblRecruitmentProgress.text = @"0/9";
    }
    
    self.lblRecruitmentType.text = contact.candidateProcess.firstInterviewRecruitment;

}

@end
