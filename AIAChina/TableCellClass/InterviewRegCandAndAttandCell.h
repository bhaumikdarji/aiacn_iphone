//
//  InterviewRegCandAndAttandCell.h
//  AIAChina
//
//  Created by AIA on 04/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterviewRegCandAndAttandCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecruitmentType;
@property (weak, nonatomic) IBOutlet UILabel *lblRecruitmentProgress;

- (void)loadInterviewCandidateData:(TblInterviewCandidate *)interviewCandidate;

@end
