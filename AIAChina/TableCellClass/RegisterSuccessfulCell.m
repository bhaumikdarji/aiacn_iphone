//
//  RegisterSuccessfulCell.m
//  AIAChina
//
//  Created by MacMini on 24/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "RegisterSuccessfulCell.h"

@implementation RegisterSuccessfulCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadData:(TblContact *)contact{
    [Utility setBorderAndRoundCorner:self.imgProfilePic
                         borderColor:[UIColor lightGrayColor]
                               width:1.0
                        cornerRedius:20.0];
    
    [Utility setRoundCornerRedius:self.lblRank
                     cornerRedius:13.0];

    if(contact.candidateProcess.candidateRank)
        self.lblRank.text = [contact.candidateProcess.candidateRank stringValue];
    else
        self.lblRank.text = @"0";
    
    if(contact.candidatePhoto)
        self.imgProfilePic.image = [UIImage imageWithData:contact.candidatePhoto];
    else
        self.imgProfilePic.image = DEFAULT_PROFILE_IMAGE;
    
    self.lblName.text = contact.name;
    
    RecruitmentProgressView *progress = [RecruitmentProgressView loadRecruitmentProgressView];
    [progress setSelectedProgress:contact];
    [self.progressView addSubview:progress];
}


@end
