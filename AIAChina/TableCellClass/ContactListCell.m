//
//  ContactListCell.m
//  AIAChina
//
//  Created by AIA on 23/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "ContactListCell.h"

@implementation ContactListCell

- (void)awakeFromNib {
    [Utility setBorderAndRoundCorner:self.imgProfilePicture
                         borderColor:[UIColor clearColor]
                               width:1.0
                        cornerRedius:38.0];
    // Initialization code
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];

    // Configure the view for the selected state
}

- (void)loadContactData:(TblContact *)contact{
    
    if(((int)[contact.candidatePhoto length]!=0))
        self.imgProfilePicture.image = [UIImage imageWithData:contact.candidatePhoto];
    else
        if ([contact.gender isEqualToString:@"F"]) {
            self.imgProfilePicture.image = [UIImage imageNamed:@"female.png"];
        } else {
            self.imgProfilePicture.image = [UIImage imageNamed:@"male.png"];
        }
    
    self.lblFirstName.text = contact.name;
    
//    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
//    [dateformate setDateFormat:@"dd-MM-yyyy"]; // Date formater
//    NSString *date = [dateformate stringFromDate:contact.modificationDate]; // Convert date to string
//    NSLog(@"date :%@",date);
    
//    self.lblFirstName.text =[NSString stringWithFormat:@"%@-%@",contact.name,date];
    
    self.lblRecruitmentProgress.text = [ApplicationFunction recruitmentProgress:contact];
}

- (void)loadGroupData:(TblGroup *)group{
    if(group.image){
        self.imgProfilePicture.image = [UIImage imageWithData:group.image];
    }else{
        self.imgProfilePicture.image = nil;
    }
    self.lblFirstName.text = group.groupName;
}

@end
