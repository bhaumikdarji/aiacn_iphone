//
//  SettingCell.h
//  AIAChina
//
//  Created by Tankar on 22/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgShowImage;
@property (strong, nonatomic) IBOutlet UIButton *btnAddImage;


@end
