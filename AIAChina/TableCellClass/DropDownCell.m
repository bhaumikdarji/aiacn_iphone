//
//  DropDownCell.m
//  AIAChina
//
//  Created by AIA on 30/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "DropDownCell.h"

@implementation DropDownCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadDropDownData:(NSString *)dropdownValue{
    self.lblDropDownValue.text = dropdownValue;
}

@end
