//
//  CompanyInterviewCell.m
//  AIAChina
//
//  Created by AIA on 07/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CompanyInterviewCell.h"

@implementation CompanyInterviewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadCompanyInterviewData:(TblInterview *)interview{
    [self.btnInterviewDate setAttributedTitle:[Utility setUnderline:[NSDate dateToString:interview.interviewDate andFormate:@"yyyy-M-dd"]
                                                      withTextColor:self.btnInterviewDate.currentTitleColor]
                                     forState:UIControlStateNormal];
    
    [self.btnInterviewStatus setAttributedTitle:[Utility setUnderline:interview.interviewSessionName
                                                      withTextColor:self.btnInterviewDate.currentTitleColor]
                                     forState:UIControlStateNormal];
    //[self.btnInterviewStatus setTitle:interview.interviewSessionName forState:UIControlStateNormal];
    
//    NSDate *startDate = [NSDate stringToDate:interview.startTime dateFormat:kServerDateFormatte];
//    NSDate *eventDate = [NSDate dateWithYear:interview.interviewDate.year month:interview.interviewDate.month day:interview.interviewDate.day hour:startDate.hour min:startDate.minute];
//    
//    if([eventDate isEarlierThanDate:[NSDate date]])
//        self.btnInterviewStatus.hidden = TRUE;
//    else{
//        self.btnInterviewStatus.hidden = FALSE;
//        if([interview.isRegistered boolValue])
//            [self.btnInterviewStatus setTitle:NSLocalizedString(@"Cancel",@"") forState:UIControlStateNormal];
//        else
//            [self.btnInterviewStatus setTitle:NSLocalizedString(@"Registration",@"") forState:UIControlStateNormal];
//    }
}

@end
