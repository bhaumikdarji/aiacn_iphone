//
//  EOPRegistrationCell.m
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EOPRegistrationCell.h"

@implementation EOPRegistrationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadRegData:(TblEOP *)eop{
    [self.btnEventDate setAttributedTitle:[Utility setUnderline:[NSDate dateToString:eop.eventDate andFormate:@"yyyy-M-dd"]
                                                  withTextColor:self.btnEventDate.currentTitleColor]
                                 forState:UIControlStateNormal];
    
    [self.btnTopicName setAttributedTitle:[Utility setUnderline:eop.topic
                                                  withTextColor:self.btnTopicName.currentTitleColor]
                                 forState:UIControlStateNormal];

    
    //self.lblTopic.text = eop.topic;
//    NSDate *startDate = [NSDate stringToDate:eop.startTime dateFormat:kServerDateFormatte];
//    NSDate *eventDate = [NSDate dateWithYear:eop.eventDate.year month:eop.eventDate.month day:eop.eventDate.day hour:startDate.hour min:startDate.minute];
//    
//    if([eventDate isEarlierThanDate:[NSDate date]])
//        self.btnRegistration.hidden = TRUE;
//    else{
//        self.btnRegistration.hidden = FALSE;
//        if([eop.isRegistered boolValue])
//            [self.btnRegistration setTitle:NSLocalizedString(@"Cancel",@"") forState:UIControlStateNormal];
//        else
//            [self.btnRegistration setTitle:NSLocalizedString(@"Registration",@"") forState:UIControlStateNormal];
//    }
}

@end
