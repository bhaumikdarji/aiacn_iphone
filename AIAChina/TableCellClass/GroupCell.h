//
//  GroupCell.h
//  AIAChina
//
//  Created by AIA on 22/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstName;
@property (weak, nonatomic) IBOutlet UILabel *lblDOB;
@property (weak, nonatomic) IBOutlet UILabel *lblMobile;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblSOR;
@property (weak, nonatomic) IBOutlet UILabel *recruitmentStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

- (void)loadGroupContact:(TblContact *)groupContact;


@end
