//
//  GreetingCell.h
//  AIAChina
//
//  Created by MacMini on 20/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GreetingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblGreetingName;

@end
