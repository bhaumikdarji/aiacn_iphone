//
//  EOPAttendanceCell.m
//  AIAChina
//
//  Created by AIA on 05/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EOPAttendanceCell.h"

@implementation EOPAttendanceCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadAttendanceData:(NSDictionary *)dic{
    [self.btnEventdate setTitle:[NSDate stringDateToString:[dic objectForKey:@"eventDate"]
                                        currentDateForamte:kServerDateFormatte
                                        displayDateFormate:@"yyyy-M-dd"]
                       forState:UIControlStateNormal];
    NSString *isAttended = [dic objectForKey:@"attendedStatus"];
    NSString *attendedStatus;
    if ([isAttended isEqualToString: @"Y"]) {
        attendedStatus = NSLocalizedString(@"Attended", @"");
    } else {
        attendedStatus = NSLocalizedString(@"Not Attended", @"");
    }
    
    [self.btnAttendance setTitle:attendedStatus forState:UIControlStateNormal];
}
@end
