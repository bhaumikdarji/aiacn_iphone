//
//  GroupCell.m
//  AIAChina
//
//  Created by AIA on 22/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "GroupCell.h"
#import "DropDownManager.h"

@implementation GroupCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadGroupContact:(TblContact *)contact{
    
    [Utility setBorderAndRoundCorner:self.imgProfilePicture
                         borderColor:[UIColor grayColor]
                               width:1.0
                        cornerRedius:self.imgProfilePicture.frame.size.width/2];
    
    if(((int)[contact.candidatePhoto length]!=0))
        self.imgProfilePicture.image = [UIImage imageWithData:contact.candidatePhoto];
    else
        if ([contact.gender isEqualToString:@"F"]) {
            self.imgProfilePicture.image = [UIImage imageNamed:@"female.png"];
        } else {
            self.imgProfilePicture.image = [UIImage imageNamed:@"male.png"];
        }
    
    self.lblFirstName.text = contact.name;
    self.lblDOB.text = [DateUtils dateToString:contact.birthDate andFormate:kDisplayDateFormatte];
    self.lblMobile.text = [NSString stringWithFormat:@"%@",contact.mobilePhoneNo];
    NSString *gender = contact.gender;
    gender = [gender stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.lblGender.text = [[DropDownManager sharedManager]getDropdownDesc:DROP_DOWN_GENDER code:gender];
    self.lblAge.text = [NSString stringWithFormat:@"%@",contact.age];
    self.lblSOR.text = [[DropDownManager sharedManager]getDropdownDesc:DROP_DOWN_SOURCE_OF_REFERRAL code:contact.referalSource];
//    [NSString stringWithFormat:@"%@",contact.referalSource];
    if (contact) {
        self.recruitmentStatus.text = [ApplicationFunction recruitmentProgress:contact];
    } else{
        self.recruitmentStatus.text = @"0/9";
    }
}

@end
