//
//  SearchCell.h
//  AIAChina
//
//  Created by iMac SCNS on 21/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView  *tableBGView;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblRecruitmentProgress;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDOBText;
@property (weak, nonatomic) IBOutlet UILabel *lblDob;
@property (weak, nonatomic) IBOutlet UILabel *lblGenderText;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblAgeText;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileNoText;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileNo;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceofReferralText;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceofReferral;

@property (weak, nonatomic) IBOutlet UIView *tblBorderView;
@property (weak, nonatomic) IBOutlet UITableView *tblNote;
@property (strong, nonatomic) NSArray *noteArray;
@property (strong, nonatomic) NSString *sortBy;

- (void)loadSeachData:(TblContact *)contact;
- (void)loadSeachNoteData:(NSArray *)noteArray;

- (IBAction)onBtnActivityTypeClick:(id)sender;
- (IBAction)onBtnActivityDateClick:(id)sender;
- (IBAction)onBtnDescClick:(id)sender;

@end
