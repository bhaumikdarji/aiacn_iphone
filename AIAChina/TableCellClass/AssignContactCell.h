//
//  AssignContactCell.h
//  AIAChina
//
//  Created by AIA on 30/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignContactCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
//@property (weak, nonatomic) IBOutlet UILabel *lblContact;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;

@property (weak, nonatomic) IBOutlet UILabel *lblGroupMember;

- (void)loadContactData:(TblContact *)contact;
- (void)loadGroupData:(TblGroup *)group;

@end
