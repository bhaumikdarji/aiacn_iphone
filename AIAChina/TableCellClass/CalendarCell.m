//
//  BirthDayCell.m
//  AIAChina
//
//  Created by SCS2 on 09/09/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CalendarCell.h"
#import "DropDownManager.h" 

@implementation CalendarCell
/*
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setFrame:CGRectMake(0, 0, 510, 210)];
        _lblActivity = [[UILabel alloc]initWithFrame:CGRectMake(10,10,150, 50)];
        [self addSubview:self.lblActivity];
        _lblTime = [[UILabel alloc]initWithFrame:CGRectMake(160, 10, 150, 50)];
        [self addSubview:self.lblTime];
        _lblName = [[UILabel alloc]initWithFrame:CGRectMake(10, 60, 150, 50)];
        [self addSubview:self.lblName];
        _lblLocation = [[UILabel alloc]initWithFrame:CGRectMake(160, 60, 150, 50)];
        [self addSubview:self.lblLocation];
    }
    return self;
}*/
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadiCalendarCell:(NSDictionary *)dict{

    self.lblActivity.text = dict[@"eventType"];
    self.lblLocation.text = dict[@"eventLocation"];
    self.lblTime.text = dict[@"eventTime"];
    self.lblName.text = dict[@"eventName"];
//    self.lblAge.text = dict[@"name"];
//    self.lblGender.text = dict[@"name"]
//    self.lblBirthDate.text = [DateUtils dateToString:dict[@"name"] andFormate:kDisplayDateFormatte];
//    self.lblContactNumber.text = contact.mobilePhoneNo;
}

@end
