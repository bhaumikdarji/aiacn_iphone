//
//  SettingCell.m
//  AIAChina
//
//  Created by Tankar on 22/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "SettingCell.h"
#import "DropDownManager.h"

@implementation SettingCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
