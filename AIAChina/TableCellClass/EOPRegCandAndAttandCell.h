//
//  EOPRegCandAndAttandCell.h
//  AIAChina
//
//  Created by AIA on 04/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TblEOPCandidate.h"
@interface EOPRegCandAndAttandCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAttendedEOP;
@property (weak, nonatomic) IBOutlet UILabel *lblRecProgress;

- (void)loadEOPCandidateData:(TblEOPCandidate *)eopCandidate;

@end
