//
//  NoteCell.h
//  AIAChina
//
//  Created by iMac AIA on 20/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblActivityDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblDateDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblDescriptionDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;

- (void)loadNoteData:(TblNotes *)note;

@end
