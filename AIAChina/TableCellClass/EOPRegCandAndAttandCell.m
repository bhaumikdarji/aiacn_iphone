//
//  EOPRegCandAndAttandCell.m
//  AIAChina
//
//  Created by AIA on 04/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EOPRegCandAndAttandCell.h"
#import "TblEOPCandidate.h"
#import "TblContact.h"

@implementation EOPRegCandAndAttandCell

- (void)awakeFromNib {
 
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadEOPCandidateData:(TblEOPCandidate *)eopCandidate{
    
    
    [Utility setRoundCornerRedius:self.imgProfile cornerRedius:15.0];
    self.lblName.text = eopCandidate.candidateName;

    TblContact *contact =  (TblContact *) [DbUtils fetchObject:@"TblContact"
                                                andPredict:[NSPredicate predicateWithFormat:@"name == %@ AND agentId == %@ AND isDelete == 0", eopCandidate.candidateName,eopCandidate.servicingAgent]
                                         andSortDescriptor:nil
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext] ;
    if (contact) {
        self.lblRecProgress.text = [ApplicationFunction recruitmentProgress:contact];
    } else{
        self.lblRecProgress.text = @"0/9";
    }
    if (eopCandidate.attendedEventCount)
        self.lblAttendedEOP.text = [NSString stringWithFormat:@"%d",[eopCandidate.attendedEventCount intValue]];
    else {
        self.lblAttendedEOP.text = @"";
    }
}

@end
