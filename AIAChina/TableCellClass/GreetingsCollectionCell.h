//
//  GreetingsCollectionCell.h
//  AIAChina
//
//  Created by MacMini on 20/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GreetingsCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgGreeting;

- (void)loadData:(TblGreetings *)greeting;

@end
