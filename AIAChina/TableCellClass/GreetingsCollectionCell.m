//
//  GreetingsCollectionCell.m
//  AIAChina
//
//  Created by MacMini on 20/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "GreetingsCollectionCell.h"
#import "UIImageView+AFNetworking.h"

@implementation GreetingsCollectionCell

- (void)loadData:(TblGreetings *)greeting{
    NSString *url = [NSString stringWithFormat:@"%@rest/egreeting/downloadFile?egreetingCode=%@",kCMSBaseURLString,greeting.eGreetingCode];
    [self.imgGreeting setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]
                            placeholderImage:nil
                                     success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                         self.imgGreeting.image = image;
                                     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                                         NSLog(@"Download iamge error : %@",error.description);
                                         self.imgGreeting.image = nil;
                                     }];
}

@end