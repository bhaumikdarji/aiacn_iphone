//
//  AddContactCell.h
//  AIAChina
//
//  Created by AIA on 20/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TblContact.h"

@interface AddContactCell : UITableViewCell<UIPopoverControllerDelegate>


@property (weak, nonatomic) IBOutlet UIView *personalIngBgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UIButton *btnProfilePic;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIButton *btnDOB;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UIButton *btnGender;
@property (weak, nonatomic) IBOutlet UIButton *btnSOR;
@property (weak, nonatomic) IBOutlet UIButton *btnGroup;
@property (weak, nonatomic) IBOutlet UILabel *lblLastContacted;

@property (weak, nonatomic) IBOutlet UIView *contactInfoBgView;
@property (weak, nonatomic) IBOutlet UIButton *btnResidentAddress1;
@property (weak, nonatomic) IBOutlet UITextField *txtResidentAddress2;
@property (weak, nonatomic) IBOutlet UIButton *btnIdentityType;
@property (weak, nonatomic) IBOutlet UITextField *txtNric;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtQQId;
@property (weak, nonatomic) IBOutlet UITextField *txtWeChat;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtFixedLine;
@property (weak, nonatomic) IBOutlet UITextField *txtOffice;
@property (weak, nonatomic) IBOutlet UIButton *btnNewComerSource;

@property (weak, nonatomic) IBOutlet UIView *occupationInfoBgView;
@property (weak, nonatomic) IBOutlet UIButton *btnMaritalStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnRace;
@property (weak, nonatomic) IBOutlet UIButton *btnEducation;
@property (weak, nonatomic) IBOutlet UIButton *btnTalentProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnRejoin;
@property (weak, nonatomic) IBOutlet UIButton *btnRecommend;
@property (weak, nonatomic) IBOutlet UIButton *btnWorkExperience;
@property (weak, nonatomic) IBOutlet UITextField *txtAnnualIncome;
@property (weak, nonatomic) IBOutlet UIButton *btnPresentEmpCond;
@property (weak, nonatomic) IBOutlet UIButton *btnPurInsBefore;
@property (weak, nonatomic) IBOutlet UIButton *btnSaleExp;
@property (weak, nonatomic) IBOutlet UIButton *btnNatureBusiness;

@property (weak, nonatomic) IBOutlet UIView *remarkBgView;
@property (weak, nonatomic) IBOutlet UITextView *txtRemark;

@property (nonatomic, strong) NSDate *dob;
@property (nonatomic, strong) TblContact *contact;
@property (nonatomic, strong) UIPopoverController *popoverController;

- (void)loadPersonalInfo;
- (void)loadContactInfo;
- (void)loadOccupationInfo;
- (void)loadRemarkInfo;

- (IBAction)onBtnMaritalStatusClick:(id)sender;
- (IBAction)onBtnRaceClick:(id)sender;
- (IBAction)onBtnEducationClick:(id)sender;
- (IBAction)onBtnTalentProfileClick:(id)sender;
- (IBAction)onBtnPresentEmpCondClick:(id)sender;
- (IBAction)onBtnNatureBusinessClick:(id)sender;
- (IBAction)onBtnGenderClick:(id)sender;
- (IBAction)onBtnSORClick:(id)sender;
- (IBAction)onBtnRecommendClick:(id)sender;
- (IBAction)onBtnRejoinClick:(id)sender;
- (IBAction)onBtnPurInsBeforeClick:(id)sender;
- (IBAction)onBtnSaleExpClick:(id)sender;
- (IBAction)onBtnIdentityTypeClick:(id)sender;
- (IBAction)onBtnNewComerSourceClick:(id)sender;

@end
