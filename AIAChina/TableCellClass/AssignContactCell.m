//
//  AssignContactCell.m
//  AIAChina
//
//  Created by AIA on 30/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AssignContactCell.h"
#import "DropDownManager.h"

@implementation AssignContactCell

- (void)awakeFromNib {
    [Utility setBorderAndRoundCorner:self.imgProfilePic
                         borderColor:[UIColor lightGrayColor]
                               width:1.0
                        cornerRedius:17.0];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)loadContactData:(TblContact *)contact{
    self.lblName.text = contact.name;
    NSString *gender = contact.gender;
    gender = [gender stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.lblGender.text = [[DropDownManager sharedManager]getDropdownDesc:DROP_DOWN_GENDER code:gender];
//    self.lblContact.text = contact.mobilePhoneNo;
    self.imgProfilePic.image = (contact.candidatePhoto) ? [UIImage imageWithData:contact.candidatePhoto] : DEFAULT_PROFILE_IMAGE;
}

- (void)loadGroupData:(TblGroup *)group{
    self.lblName.text = group.groupName;
    //self.lblGroupMember.text = [NSString stringWithFormat:@"%lu",(unsigned long)[group.contact allObjects].count];
    self.imgProfilePic.image = [UIImage imageWithData:group.image];
}

@end
