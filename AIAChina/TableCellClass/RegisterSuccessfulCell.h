//
//  RegisterSuccessfulCell.h
//  AIAChina
//
//  Created by MacMini on 24/08/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecruitmentProgressView.h"

@interface RegisterSuccessfulCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblRank;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *progressView;

- (void)loadData:(TblContact *)contact;

@end
