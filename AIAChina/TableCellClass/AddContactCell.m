//
//  AddContactCell.m
//  AIAChina
//
//  Created by AIA on 20/07/15.
//  Copyright (c) 2015 AIA. All rights reserved.
////

#import "AddContactCell.h"
#import "PickerPopoverViewController.h"
#import "DropDownManager.h"

@implementation AddContactCell

- (void)awakeFromNib {
    [Utility setRoundCornerRedius:self.personalIngBgView cornerRedius:5.0];
    [Utility setBorderAndRoundCorner:self.imgProfilePic
                         borderColor:[UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:240.0/255.0 alpha:1.0]
                               width:2.0
                        cornerRedius:self.imgProfilePic.frame.size.width/2];
    
    [Utility setRoundCornerRedius:self.contactInfoBgView cornerRedius:5.0];
    [Utility setRoundCornerRedius:self.occupationInfoBgView cornerRedius:5.0];
    [Utility setRoundCornerRedius:self.remarkBgView cornerRedius:5.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)onBtnWorkExperienceClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_WORK_EXPERIENCE]];
}

- (IBAction)onBtnMaritalStatusClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_MARITAL_STATUS]];
}

- (IBAction)onBtnRaceClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_RACE]];
}

- (IBAction)onBtnEducationClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_EDUCATION]];
}

- (IBAction)onBtnTalentProfileClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_TALENT_PROFILE]];
}

- (IBAction)onBtnPresentEmpCondClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_PRESENT_EMPLOYMENT_CONDITION]];
}

- (IBAction)onBtnNatureBusinessClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_NATURE_OF_BUSINESS]];
}

- (IBAction)onBtnGenderClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_GENDER]];
}

- (IBAction)onBtnSORClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_SOURCE_OF_REFERRAL]];
}

- (IBAction)onBtnRecommendClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_RECOMMEND_SCHEME]];
}

- (IBAction)onBtnRejoinClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:@[@"是", @"否"]];
}

- (IBAction)onBtnPurInsBeforeClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:@[@"是", @"否"]];
}

- (IBAction)onBtnSaleExpClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:@[@"有", @"无"]];
}

- (IBAction)onBtnIdentityTypeClick:(id)sender{
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_IDENTITY_TYPE]];
}

- (IBAction)onBtnNewComerSourceClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_NEWCOMER_SOURCE]];
}

- (void)loadPersonalInfo{
    self.lblLastContacted.text = [DateUtils dateToString:[NSDate date] andFormate:kDisplayDateFormatte];
    if(self.contact){
        if(self.contact.candidatePhoto){
            self.imgProfilePic.image = [UIImage imageWithData:self.contact.candidatePhoto];
            [self.btnProfilePic setImage:nil forState:UIControlStateNormal];
            [self.btnProfilePic setTitle:@"" forState:UIControlStateNormal];
        }else{
            [self.btnProfilePic setImage:[UIImage imageNamed:@"add_photo.png"] forState:UIControlStateNormal];
            [self.btnProfilePic setTitle:NSLocalizedString(@"Add Photo", @"") forState:UIControlStateNormal];
        }
        self.txtName.text = self.contact.name;
        NSString *genderCode = self.contact.gender;
        if (genderCode != nil) {
            genderCode = [genderCode stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        
        [self.btnGender setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_GENDER code:genderCode] forState:UIControlStateNormal];
        [self.btnDOB setTitle:[DateUtils dateToString:self.contact.birthDate andFormate:kDisplayDateFormatte] forState:UIControlStateNormal];
        [self.btnSOR setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_SOURCE_OF_REFERRAL code:self.contact.referalSource] forState:UIControlStateNormal];
        if ([self.contact.group allObjects].count > 0) {
            NSMutableArray *groupArray = [[NSMutableArray alloc]init];
            for (TblGroup *group in [self.contact.group allObjects]) {
                if(![group.groupName isEqualToString:UNGROUP])
                    [groupArray addObject:group.groupName];
            }
            [self.btnGroup setTitle:[groupArray componentsJoinedByString:@","] forState:UIControlStateNormal];
        }
        self.lblAge.text = [self.contact.age stringValue];
    }
}

- (void)loadContactInfo{
    if(self.contact){
        [self.btnResidentAddress1 setTitle:self.contact.residentialAddress forState:UIControlStateNormal];
        self.txtResidentAddress2.text = self.contact.residentialAddress1;
        self.txtNric.text = self.contact.nric;
        self.txtMobile.text = self.contact.mobilePhoneNo;
        self.txtEmail.text = self.contact.eMailId;
        self.txtFixedLine.text = self.contact.fixedLineNO;
        self.txtQQId.text = self.contact.qq;
        self.txtOffice.text = self.contact.officePhoneNo;
        self.txtWeChat.text = self.contact.weChat;
        [self.btnIdentityType setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_IDENTITY_TYPE code:self.contact.identityType] forState:UIControlStateNormal];
        [self.btnNewComerSource setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_NEWCOMER_SOURCE code:self.contact.newcomerSource] forState:UIControlStateNormal];
    }
}

- (void)loadOccupationInfo{
    if(self.contact){
        [self.btnMaritalStatus setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_MARITAL_STATUS code:self.contact.marritalStatus] forState:UIControlStateNormal];
        [self.btnWorkExperience setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_WORK_EXPERIENCE code:self.contact.workingYearExperience] forState:UIControlStateNormal];
        [self.btnRace setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_RACE code:self.contact.race] forState:UIControlStateNormal];
        self.txtAnnualIncome.text = self.contact.yearlyIncome;
        [self.btnEducation setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_EDUCATION code:self.contact.education] forState:UIControlStateNormal];
        [self.btnPresentEmpCond setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_PRESENT_EMPLOYMENT_CONDITION code:self.contact.presentWorkCondition] forState:UIControlStateNormal];
        [self.btnTalentProfile setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_TALENT_PROFILE code:self.contact.talentProfile] forState:UIControlStateNormal];
        [self.btnPurInsBefore setTitle:self.contact.purchasedAnyInsurance forState:UIControlStateNormal];
        [self.btnRejoin setTitle:self.contact.reJoin forState:UIControlStateNormal];
        [self.btnSaleExp setTitle:self.contact.salesExperience forState:UIControlStateNormal];
        [self.btnRecommend setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_RECOMMEND_SCHEME code:self.contact.recommendedRecruitmentScheme] forState:UIControlStateNormal];
        [self.btnNatureBusiness setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_NATURE_OF_BUSINESS code:self.contact.natureOfBusiness] forState:UIControlStateNormal];
    }
}

- (void)loadRemarkInfo{
    if(self.contact)
        self.txtRemark.text = [ValueParser parseString:self.contact.remarks];
}

- (void)setPopoverView:(UIButton *)btn pickerArray:(NSArray *)pickerArray{
    NSMutableArray *pickerDataArray = [[NSMutableArray alloc] initWithObjects:@"", nil];
    [pickerDataArray addObjectsFromArray:pickerArray];
    PickerPopoverViewController *popoverViewCtrl = [[PickerPopoverViewController alloc] initWithNibName:@"PickerPopoverViewController" bundle:nil];
    if(isEmptyString(btn.titleLabel.text))
        [btn setTitle:pickerDataArray[0] forState:UIControlStateNormal];
    else
        popoverViewCtrl.oldValue = btn.titleLabel.text;
    
    [popoverViewCtrl setPickerArray:pickerDataArray];
    [popoverViewCtrl setSelectedIndex:^(int selectedRowIndexPath) {
        [btn setTitle:pickerDataArray[selectedRowIndexPath] forState:UIControlStateNormal];
        if(self.contact)
            [self setUpdateContact:btn];
        [self.popoverController dismissPopoverAnimated:TRUE];
    }];
    
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverViewCtrl];
    if ([btn isEqual:self.btnSOR]) {
        self.popoverController.popoverContentSize = CGSizeMake(300, 650);
    }
    else
    {
        self.popoverController.popoverContentSize = popoverViewCtrl.view.frame.size;
    }
    [self.popoverController presentPopoverFromRect:btn.frame
                                            inView:self
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:TRUE];
}

- (void)setUpdateContact:(id)sender{
    if([sender isEqual:self.txtName])
        self.contact.name = self.txtName.text;
    else if([sender isEqual:self.btnGender])
        self.contact.gender = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_GENDER description:self.btnGender.currentTitle];
    else if([sender isEqual:self.btnSOR])
        self.contact.referalSource = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_SOURCE_OF_REFERRAL description:self.btnSOR.currentTitle];
    else if([sender isEqual:self.txtResidentAddress2])
        self.contact.residentialAddress1 = self.txtResidentAddress2.text;
    else if([sender isEqual:self.txtNric])
        self.contact.nric = self.txtNric.text;
    else if([sender isEqual:self.txtMobile])
        self.contact.mobilePhoneNo = self.txtMobile.text;
    else if([sender isEqual:self.txtEmail])
        self.contact.eMailId = self.txtEmail.text;
    else if([sender isEqual:self.txtFixedLine])
        self.contact.fixedLineNO = self.txtFixedLine.text;
    else if([sender isEqual:self.txtQQId])
        self.contact.qq = self.txtQQId.text;
    else if([sender isEqual:self.txtOffice])
        self.contact.officePhoneNo = self.txtOffice.text;
    else if([sender isEqual:self.txtWeChat])
        self.contact.weChat = self.txtWeChat.text;
    else if([sender isEqual:self.btnIdentityType])
        self.contact.identityType = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_IDENTITY_TYPE description:self.btnIdentityType.currentTitle];
    else if([sender isEqual:self.btnNewComerSource])
        self.contact.newcomerSource = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_NEWCOMER_SOURCE description:self.btnNewComerSource.currentTitle];
    else if([sender isEqual:self.btnMaritalStatus])
        self.contact.marritalStatus = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_MARITAL_STATUS description:self.btnMaritalStatus.currentTitle];
    else if([sender isEqual:self.btnWorkExperience])
        self.contact.workingYearExperience = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_WORK_EXPERIENCE description:self.btnWorkExperience.currentTitle];
    else if([sender isEqual:self.btnRace])
        self.contact.race = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_RACE description:self.btnRace.currentTitle];
    else if([sender isEqual:self.txtAnnualIncome])
        self.contact.yearlyIncome = self.txtAnnualIncome.text;
    else if([sender isEqual:self.btnEducation])
        self.contact.education = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_EDUCATION description:self.btnEducation.currentTitle];
    else if([sender isEqual:self.btnPresentEmpCond])
        self.contact.presentWorkCondition = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_PRESENT_EMPLOYMENT_CONDITION description:self.btnPresentEmpCond.currentTitle];
    else if([sender isEqual:self.btnTalentProfile])
        self.contact.talentProfile = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_TALENT_PROFILE description:self.btnTalentProfile.currentTitle];
    else if([sender isEqual:self.btnPurInsBefore])
        self.contact.purchasedAnyInsurance = self.btnPurInsBefore.currentTitle;
    else if([sender isEqual:self.btnRejoin])
        self.contact.reJoin = self.btnRejoin.currentTitle;
    else if([sender isEqual:self.btnSaleExp])
        self.contact.salesExperience = self.btnSaleExp.currentTitle;
    else if([sender isEqual:self.btnRecommend])
        self.contact.recommendedRecruitmentScheme = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_RECOMMEND_SCHEME description:self.btnRecommend.currentTitle];
    else if([sender isEqual:self.btnNatureBusiness])
        self.contact.natureOfBusiness = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_NATURE_OF_BUSINESS description:self.btnNatureBusiness.currentTitle];
    else if([sender isEqual:self.txtRemark])
        self.contact.remarks = self.txtRemark.text;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.txtName && (textField.text.length+string.length) <= CANDIDATE_NAME)
        return YES;
    else if (textField == self.txtNric && (textField.text.length+string.length) <= NRIC)
        return YES;
    else if (textField == self.txtResidentAddress2 && (textField.text.length+string.length + self.btnResidentAddress1.titleLabel.text.length) <= ADDRESS)
        return YES;
    else if (textField == self.txtFixedLine && [Utility isNumeric:string] && (textField.text.length+string.length) <= FIXED_LINE_NO)
        return YES;
    else if (textField == self.txtMobile && [Utility isNumeric:string] && (textField.text.length+string.length) <= MOBILE_NO)
        return YES;
    else
        return NO;
}

@end
