//
//  BirthDayCell.m
//  AIAChina
//
//  Created by SCS2 on 09/09/15.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "BirthDayCell.h"
#import "DropDownManager.h" 

@implementation BirthDayCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadBirthDayCell:(TblContact *)contact{
    [Utility setRoundCornerRedius:self.imgProfile
                     cornerRedius:self.imgProfile.frame.size.width/2];

    if(contact.candidatePhoto)
        self.imgProfile.image = [UIImage imageWithData:contact.candidatePhoto];
    else
        self.imgProfile.image = DEFAULT_PROFILE_IMAGE;
    
    self.lblCandidateName.text = contact.name;
    self.lblAge.text = [contact.age stringValue];
    self.lblGender.text = [[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_GENDER code:contact.gender];
    self.lblBirthDate.text = [DateUtils dateToString:contact.birthDate andFormate:kDisplayDateFormatte];
    self.lblContactNumber.text = contact.mobilePhoneNo;
}

@end
