//
//  CandidateApplicationFormViewController.h
//  AIAChina
//
//  Created by Burri on 2015-07-25.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "AIAChinaViewController.h"
typedef NS_ENUM (NSInteger, ApplicationFormCellType) {
    ApplicationFormCellPersonalInfo = 0,
    ApplicationFormCellFamilyInfo,
    ApplicationFormCellWorkExperience,
    ApplicationFormCellEducation,
    ApplicationFormCellPersonalCertification,
    ApplicationFormCellESignature
};

@interface CandidateApplicationFormViewController : AIAChinaViewController

@property (nonatomic, strong) UIPopoverController *candidatePopoverController;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (nonatomic,retain) NSString *addressCode;
@property (nonatomic,retain) UIImage *signImage;
@property (strong, nonatomic) NSArray *groupArray;

@property (copy, nonatomic) void(^completionBlock)();

@end

