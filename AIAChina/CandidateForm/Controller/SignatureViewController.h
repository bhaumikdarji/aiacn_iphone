//
//  SignatureViewController.h
//  AIAChina
//
//  Created by Burri on 2015-08-19.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPSSignatureView.h"
@protocol SignatureViewControllerDelegate
- (void)savedImage:(UIImage *)image;
@end
@interface SignatureViewController : UIViewController
@property (nonatomic, retain) id<SignatureViewControllerDelegate> delegate;
@end
