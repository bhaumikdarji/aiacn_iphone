//
//  CandidateApplicationFormViewController.m
//  AIAChina
//
//  Created by Burri on 2015-07-25.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CandidateApplicationFormViewController.h"
#import "PersonalInformationTableViewCell.h"
#import "FamilyInfoTableViewCell.h"
#import "WorkExperienceTableViewCell.h"
#import "EsignatureTableViewCell.h"
#import "EducationTableViewCell.h"
#import "PersonalCertificationTableViewCell.h"
#import "CandidateApplicationModel.h"
#import "ContactDataProvider.h"
#import "TblPersonalCertification.h"
#import "TblEducation.h"
#import "TblFamily.h"
#import "TblExperience.h"
#import "TblContact.h"
#import "SignatureViewController.h"
#import "TblAgentDetails.h"
#import "SyncModel.h"

#define REG_ADD_TAG 111
#define RES_ADD_TAG 222

enum candidateCellTags{
    PERSONAL_DETAIL_TAG = 10000
};

@interface CandidateApplicationFormViewController ()<UITableViewDataSource, UITableViewDelegate,EsignatureTableViewCellDelegate,SignatureViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *applicationFormTableview;
@property (weak, nonatomic) IBOutlet UIButton *personalInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *familyInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *educationButton;
@property (weak, nonatomic) IBOutlet UIButton *workExperienceButton;
@property (weak, nonatomic) IBOutlet UIButton *personalCertificationButton;
@property (weak, nonatomic) IBOutlet UIButton *eSignatureButton;
@property (nonatomic, readwrite) NSInteger cellType;
@property (nonatomic, retain) TblContact *contact;
@property (nonatomic, retain) ContactDataProvider *contactDataProvider;
@property (nonatomic, retain) CandidateApplicationModel *applicationFormModel;
@property (nonatomic, retain) UIPopoverController *newpopoverController;
@property (nonatomic, retain) AFNetworkReachabilityManager *reachabilityManager;
@end

@implementation CandidateApplicationFormViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    self.navigationItem.title = NSLocalizedString(@"Application Form", @"Application Form");
    self.contactDataProvider = [ContactDataProvider sharedContactDataProvider];
    self.cellType = ApplicationFormCellPersonalInfo;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = YES;
    self.applicationFormModel = [[CandidateApplicationModel alloc] init];
    [self.applicationFormModel loadDataFromDatabase:self.addressCode];
    
}

- (void)loadArray
{
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)doneButtonAction:(id)sender {
    if([self validate]){
        BOOL isComplete = TRUE;
        if(![ApplicationFunction isCompletePersonalInfo:self.contact])
            isComplete = FALSE;
        else if(![ApplicationFunction isCompleteEducation:self.contact])
            isComplete = FALSE;
        else if(![ApplicationFunction isCompleteSignature:self.contact])
            isComplete = FALSE;
        
        if(isComplete){
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Congratulations, you have completed Applcation Form, you may proceed to register for interviews!", @"")
                                    cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                        okButtonTitle:nil
                                          cancelBlock:^(UIAlertAction *action) {
                                              [self saveApplicationForm];
                                            } okBlock:nil];
        }else{
            [DisplayAlert showAlertInControllerInView:self
                                                title:@""
                                              message:NSLocalizedString(@"Application Form is incomplete, please confirm to exit?", @"")
                                    cancelButtonTitle:NSLocalizedString(@"No", @"")
                                        okButtonTitle:NSLocalizedString(@"Yes", @"")
                                          cancelBlock:nil
                                              okBlock:^(UIAlertAction *action) {
                                                  [self saveApplicationForm];
                                          }];
        }
    }
}

- (void)saveApplicationForm{
    self.contact.isSync = [NSNumber numberWithBool:YES];
    NSDate * modificationDate = [NSDate dateWithTimeIntervalSince1970:[[NSDate date] timeIntervalSince1970]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:kServerDateFormatte];
    NSString * strdate = [formatter stringFromDate:modificationDate];
    NSDate *newmodificationDate = [formatter dateFromString:strdate];
    self.contact.modificationDate = newmodificationDate;
    [self.contactDataProvider saveLocalDBData];
    [self synContactCall];
    [self.navigationController dismissViewControllerAnimated:TRUE
                                                  completion:^{
                                                      if(self.completionBlock){
                                                          self.completionBlock();
                                                      }
                                                  }];
}

- (BOOL)validate{
    NSString *msg = nil;
    if(isEmptyString(self.contact.name))
        msg = NSLocalizedString(@"Please enter name", @"");
    if(!isEmptyString(self.contact.eMailId)){
        if(![Utility validateEmail:self.contact.eMailId])
            msg = NSLocalizedString(@"Please enter valid email", @"");
    }
    
    if(!isEmptyString(self.contact.nric)){
        //[self.contact.nric appendString:@""];
        if(!(((NSString *)self.contact.nric).length == NRIC_MAX_LENGTH) && !(((NSString *)self.contact.nric).length == NRIC_MIN_LENGTH))
            msg = NSLocalizedString(@"Please enter valid nric", @"");
    }
    
    if(!isEmptyString(self.contact.mobilePhoneNo)){
        if(!(((NSString *)self.contact.mobilePhoneNo).length == MOBILE_LENGTH))
            msg = NSLocalizedString(@"Please enter must be 11 digit mobile number", @"");
    }
    
    if(!isEmptyString(self.contact.registeredAddress1)){
        if([self.contact.registeredAddress1 length] < 3 || ([self.contact.registeredAddress1 length] + [self.contact.registeredAddress length]) > 28)
            msg = NSLocalizedString(@"Address line should be minimum 3 characters each and should not exceed 28 characters in total!", @"");
        else if([self.contact.registeredAddress1 rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location == NSNotFound){
            msg = NSLocalizedString(@"Address line should contain at least a number", @"");
        }
    }
    
    if(!isEmptyString(self.contact.residentialAddress1)){
        if([self.contact.residentialAddress1 length] < 3 || ([self.contact.residentialAddress1 length] + [self.contact.residentialAddress length]) > 28)
            msg = NSLocalizedString(@"Address line should be minimum 3 characters each and should not exceed 28 characters in total!", @"");
        else if([self.contact.residentialAddress1 rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location == NSNotFound){
            msg = NSLocalizedString(@"Address line should contain at least a number", @"");
        }
    }
    
    if(!isEmptyString(msg)){
        [DisplayAlert showAlertInControllerInView:self
                                            title:@""
                                          message:msg
                                cancelButtonTitle:nil
                                    okButtonTitle:NSLocalizedString(@"OK", @"")
                                      cancelBlock:^(UIAlertAction *action) {
                                      } okBlock:^(UIAlertAction *action) {
                                      }];
        return FALSE;
    }
    return TRUE;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    switch (self.cellType) {
        case ApplicationFormCellPersonalInfo:
            count = [self.applicationFormModel.personalInfoArray count];
            break;
        case ApplicationFormCellFamilyInfo:
            count = [self candidateFamilyCellCount];
            break;
        case ApplicationFormCellWorkExperience:
            count = [self candidateWorkExperienceCellCount];
            break;
        case ApplicationFormCellEducation:
            count = [self candidateEducationCellCount];
            break;
        case ApplicationFormCellPersonalCertification:
            count = [self candidatePersonalCertificationCellCount];
            break;
        case ApplicationFormCellESignature:
            count = [self candidateESignatureCellCount];
            break;
   
        default:
            break;
    }
    return count;
}
- (NSInteger)candidatePersonalCertificationCellCount
{
    NSInteger count;
    if (self.applicationFormModel.personalCertificationArray.count > 0) {
        count = self.applicationFormModel.personalCertificationArray.count;
    }
    else
    {
        TblPersonalCertification *personalCertification = [self.contactDataProvider createPersonalCertification];
        personalCertification.iosAddressCode = [NSString stringWithFormat:@"%@",[[personalCertification objectID] URIRepresentation]];
        personalCertification.contact = self.applicationFormModel.contact;
        [self.applicationFormModel.personalCertificationArray addObject:personalCertification];
        count = self.applicationFormModel.personalCertificationArray.count;
    }
    return count;
}
- (NSInteger)candidateWorkExperienceCellCount
{
    NSInteger count;
    if (self.applicationFormModel.workExperienceArray.count > 0) {
        count = self.applicationFormModel.workExperienceArray.count;
    }
    else
    {
        TblExperience *experience = [self.contactDataProvider createExperience];
        experience.iosAddressCode = [NSString stringWithFormat:@"%@",[[experience objectID] URIRepresentation]];
        experience.contact = self.applicationFormModel.contact;
        [self.applicationFormModel.workExperienceArray addObject:experience];
        count = self.applicationFormModel.workExperienceArray.count;
    }
    return count;
}
- (NSInteger)candidateEducationCellCount
{
    NSInteger count;
    if (self.applicationFormModel.educationArray.count > 0) {
        count = self.applicationFormModel.educationArray.count;
    }
    else
    {
        TblEducation *education = [self.contactDataProvider createEducation];
        education.iosAddressCode = [NSString stringWithFormat:@"%@",[[education objectID] URIRepresentation]];
        education.contact = self.applicationFormModel.contact;
        [self.applicationFormModel.educationArray addObject:education];
        count = self.applicationFormModel.educationArray.count;
    }
    return count;
}
- (NSInteger)candidateFamilyCellCount
{
    NSInteger count;
    if (self.applicationFormModel.familyArray.count > 0) {
        count = self.applicationFormModel.familyArray.count;
    }
    else
    {
        TblFamily *family = [self.contactDataProvider createFamily];
        family.iosAddressCode = [NSString stringWithFormat:@"%@",[[family objectID] URIRepresentation]];
        family.contact = self.applicationFormModel.contact;
        [self.applicationFormModel.familyArray addObject:family];
        count = self.applicationFormModel.familyArray.count;
    }
    return count;
}
- (NSInteger)candidateESignatureCellCount
{
    NSInteger count;
    if (self.applicationFormModel.esignatureArray.count > 0) {
        count = self.applicationFormModel.esignatureArray.count;
    }
    else
    {
        NSString *userID = self.contact.agentId; //[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
        
        NSArray *agentProfileArray = [[DbUtils fetchAllObject:@"TblAgentDetails"
                                               andPredict:[NSPredicate predicateWithFormat:@"userID == %@", userID]
                                        andSortDescriptor:nil
                                     managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
        TblAgentDetails *agentProfile = [agentProfileArray objectAtIndex:0];
        TblESignature *eSignature = [self.contactDataProvider createESignature];
        eSignature.iosAddressCode = [NSString stringWithFormat:@"%@",[[eSignature objectID] URIRepresentation]];
        eSignature.contact = self.applicationFormModel.contact;
        eSignature.candidateName = self.contact.name;
        eSignature.branch = agentProfile.branch;
        eSignature.city = agentProfile.city;
        eSignature.agentId = agentProfile.userID;
        eSignature.serviceDepartment = agentProfile.serviceDepartment;
        [self.applicationFormModel.esignatureArray addObject:eSignature];
        count = self.applicationFormModel.esignatureArray.count;
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.cellType == ApplicationFormCellFamilyInfo) {
        FamilyInfoTableViewCell *cell = (FamilyInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FamilyInfoTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"FamilyInfoTableViewCell" owner:nil options:nil] objectAtIndex:0];
        }
        	
        [cell loadFamilyDetails:[self.applicationFormModel.familyArray objectAtIndex:indexPath.row]];
        [cell.deleteCellButton addTarget:self action:@selector(onDeleteButtonFamilyCell:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    else if (self.cellType == ApplicationFormCellPersonalInfo)
    {
        PersonalInformationTableViewCell *cell = (PersonalInformationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PersonalInformationTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"PersonalInformationTableViewCell" owner:nil options:nil] objectAtIndex:0];
        }
        
        cell.tag = PERSONAL_DETAIL_TAG;
        self.contact = [self.applicationFormModel.personalInfoArray objectAtIndex:indexPath.row];
        self.groupArray = [[self.contact.group allObjects] mutableCopy];
        [cell loadPersonalDetails:[self.applicationFormModel.personalInfoArray objectAtIndex:indexPath.row]];
        cell.dateOfBirthButton.tag = cell.btnAddress1.tag = cell.btnResidentailAddress1.tag = indexPath.row;
        [cell.dateOfBirthButton addTarget:self action:@selector(onBtnPerDob:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnAddress1 addTarget:self action:@selector(onBtnAddress1Click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnResidentailAddress1 addTarget:self action:@selector(onBtnAddress1Click:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if (self.cellType == ApplicationFormCellWorkExperience)
    {
        WorkExperienceTableViewCell *cell = (WorkExperienceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"WorkExperienceTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"WorkExperienceTableViewCell" owner:nil options:nil] objectAtIndex:0];
        }
        [cell loadWorkExperienceDetails:[self.applicationFormModel.workExperienceArray objectAtIndex:indexPath.row]];
        [cell.cellDeleteButton addTarget:self action:@selector(onDeleteButtonWorkExperienceCell:) forControlEvents:UIControlEventTouchUpInside];
        cell.startDate.tag = cell.endDate.tag = indexPath.row;
        [cell.startDate addTarget:self action:@selector(onBtnWorkStartDate:) forControlEvents:UIControlEventTouchUpInside];
        [cell.endDate addTarget:self action:@selector(onBtnWorkEndDate:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
        
    }
    else if (self.cellType == ApplicationFormCellEducation)
    {
        EducationTableViewCell *cell = (EducationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EducationTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"EducationTableViewCell" owner:nil options:nil] objectAtIndex:0];
        }
        [cell loadEducational:[self.applicationFormModel.educationArray objectAtIndex:indexPath.row]];
        [cell.educationCellDeleteButton addTarget:self action:@selector(onEducationCellDeleteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.educationStartDate.tag = cell.educationEndDate.tag = indexPath.row;
        [cell.educationStartDate addTarget:self action:@selector(onBtnEduStartDate:) forControlEvents:UIControlEventTouchUpInside];
        [cell.educationEndDate addTarget:self action:@selector(onBtnEduEndDate:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if (self.cellType == ApplicationFormCellPersonalCertification)
    {
        PersonalCertificationTableViewCell *cell = (PersonalCertificationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PersonalCertificationTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"PersonalCertificationTableViewCell" owner:nil options:nil] objectAtIndex:0];
        }
        [cell loadCertification:[self.applicationFormModel.personalCertificationArray objectAtIndex:indexPath.row]];
        [cell.personalCertificationCellDeleteButton addTarget:self action:@selector(onPersonalCertificationCellDeleteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.charterDateButton.tag  = indexPath.row;
        [cell.charterDateButton addTarget:self action:@selector(onBtnCharterDate:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    
    }
    else if (self.cellType == ApplicationFormCellESignature)
    {
        EsignatureTableViewCell *cell = (EsignatureTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EsignatureTableViewCell"];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"EsignatureTableViewCell" owner:nil options:nil] objectAtIndex:0];
        }
        cell.delegate = self;
        TblESignature *eSignature = [self.applicationFormModel.esignatureArray objectAtIndex:indexPath.row];
        if (self.signImage != nil) {
            eSignature.applicationDate = [NSString stringWithFormat:@"%@",[DateUtils dateToString:[NSDate date] andFormate:kServerDateFormatte]];
            eSignature.eSignaturePhoto =  [NSData dataWithData:UIImagePNGRepresentation(self.signImage)];
        }
        [cell loadESignature:eSignature];
        return cell;
    }
    return nil;
}

- (IBAction)onBtnAddress1Click:(UIButton *)sender {
    
    TblContact *contact = self.applicationFormModel.personalInfoArray[[sender tag]];
    PersonalInformationTableViewCell *cell = (PersonalInformationTableViewCell *)[self.applicationFormTableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [self showAddressPickerViewController:self.view.bounds
                                   inView:self.view
                            callBackBlock:^(TblAddress *address) {
                                if(sender == cell.btnAddress1){
                                    [cell.btnAddress1 setTitle:address.rgsName forState:UIControlStateNormal];
                                    cell.addressLine2TextField.text = @"";
                                    contact.registeredAddress = address.rgsName;
                                    contact.registeredAddress2 = [address.rgCodeC stringValue];
                                }else{
                                    [cell.btnResidentailAddress1 setTitle:address.rgsName forState:UIControlStateNormal];
                                    cell.residentialAddressLine2TextField.text = @"";
                                    contact.residentialAddress = address.rgsName;
                                    contact.residentialAddress2 = [address.rgCodeC stringValue];
                                }
                            }];
}

- (IBAction)loadGroup:(id)sender{
    [self showAssignPopoverCtrl:nil
              withSelectedArray:self.groupArray
                      isContact:FALSE
                       isDelete:FALSE
                       isExport:FALSE
              isAssigneeContact:TRUE
                           rect:self.view.frame
                         inView:self.view
                selectedContact:^(NSArray *selectedArray) {
                    self.groupArray = [selectedArray mutableCopy];
                    NSMutableArray *selectedGroupArray = [[NSMutableArray alloc] init];
                    for (TblGroup *group in selectedArray) {
                        [selectedGroupArray addObject:group.groupName];
                    }
                    [sender setTitle:[selectedGroupArray componentsJoinedByString:@","] forState:UIControlStateNormal];
                    
                    [self.contact removeGroup:self.contact.group];
                    for (TblGroup *group in self.groupArray)
                        [self.contact addGroupObject:group];
                    
                    if([self.contact.group count] == 0)
                        [ApplicationFunction joinUngroup:self.contact];
                }];
}

- (IBAction)onBtnCharterDate:(UIButton *)sender{
    [self datePopover:sender
     dateSelectBlock:^(NSDate *pickerDate) {
         [sender setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]] forState:UIControlStateNormal];
         TblPersonalCertification *personalCertificate = self.applicationFormModel.personalCertificationArray[[sender tag]];
         personalCertificate.charterDate = [NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]];
     }];
}

- (IBAction)onBtnWorkStartDate:(UIButton *)sender{
    [self datePopover:sender
     dateSelectBlock:^(NSDate *pickerDate) {
         [sender setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]] forState:UIControlStateNormal];
         TblExperience *workExperience = self.applicationFormModel.workExperienceArray[[sender tag]];
         workExperience.startDate = [NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]];
     }];
}

- (IBAction)onBtnWorkEndDate:(UIButton *)sender{
    [self datePopover:sender
     dateSelectBlock:^(NSDate *pickerDate) {
         [sender setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]] forState:UIControlStateNormal];
         TblExperience *workExperience = self.applicationFormModel.workExperienceArray[[sender tag]];
         workExperience.endDate = [NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]];
     }];
}


- (IBAction)onBtnEduStartDate:(UIButton *)sender{
    [self datePopover:sender
      dateSelectBlock:^(NSDate *pickerDate) {
          [sender setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]] forState:UIControlStateNormal];
          TblEducation *education = self.applicationFormModel.educationArray[[sender tag]];
          education.startDate = [NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]];
      }];
}

- (IBAction)onBtnEduEndDate:(UIButton *)sender{
    [self datePopover:sender
      dateSelectBlock:^(NSDate *pickerDate) {
          [sender setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]] forState:UIControlStateNormal];
          TblEducation *education = self.applicationFormModel.educationArray[[sender tag]];
          education.endDate = [NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]];
     }];
}

- (IBAction)onBtnPerDob:(UIButton *)sender{
    [self datePopover:sender
      dateSelectBlock:^(NSDate *pickerDate) {
          [sender setTitle:[NSString stringWithFormat:@"%@",[DateUtils dateToString:pickerDate andFormate:@"YYYY-MM-dd"]] forState:UIControlStateNormal];
          TblContact *contact = self.applicationFormModel.personalInfoArray[[sender tag]];
          contact.birthDate = pickerDate;
          NSString *text = [NSString stringWithFormat:@"%ld",(long)[DateUtils ageFromBirthday:pickerDate]];
          PersonalInformationTableViewCell *cell = (PersonalInformationTableViewCell *)[self.applicationFormTableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
          [cell.ageButton setTitle:text forState:UIControlStateNormal];
          contact.age = [NSNumber numberWithInt:[text intValue]];
     }];
}

- (void)datePopover:(UIButton *)btn
    dateSelectBlock:(void (^)(NSDate *))dateSelectBlock{
    [self showDatePopoverCtrl:UIDatePickerModeDate
                  minimumDate:nil
                  maximumDate:nil
                         rect:btn.frame
                       inView:self.view
                   pickerDate:dateSelectBlock];
}

- (IBAction)onPersonalCertificationCellDeleteButtonAction:(id)sender
{
    UIButton *personalCertificationCellDeleteButton = (UIButton *)sender;
    PersonalCertificationTableViewCell *cell= nil;
    id superView = personalCertificationCellDeleteButton.superview;
    
    while (true) {
        if([superView isKindOfClass:[PersonalCertificationTableViewCell class]]){
            cell = (PersonalCertificationTableViewCell *)superView;
            break;
        }
        superView = ((UIView *)superView).superview;
    }
    self.indexPath = [self.applicationFormTableview indexPathForCell:cell];
    TblPersonalCertification *certification = [self.applicationFormModel.personalCertificationArray objectAtIndex:self.indexPath.row];
    [self.contactDataProvider deletePersonalCertification:certification];
    [self.applicationFormModel.personalCertificationArray removeObjectAtIndex:self.indexPath.row];
    
    [self.applicationFormTableview reloadData];
}
- (IBAction)onEducationCellDeleteButtonAction:(id)sender
{
    
    UIButton *educationCellDeleteButton = (UIButton *)sender;
    EducationTableViewCell *cell= nil;
    id superView = educationCellDeleteButton.superview;
    
    while (true) {
        if([superView isKindOfClass:[EducationTableViewCell class]]){
            cell = (EducationTableViewCell *)superView;
            break;
        }
        superView = ((UIView *)superView).superview;
    }
    self.indexPath = [self.applicationFormTableview indexPathForCell:cell];
    TblEducation *education = [self.applicationFormModel.educationArray objectAtIndex:self.indexPath.row];
    [self.contactDataProvider deleteEducation:education];
    [self.applicationFormModel.educationArray removeObjectAtIndex:self.indexPath.row];
    [self.applicationFormTableview reloadData];
}
- (IBAction)onDeleteButtonFamilyCell:(id)sender
{
    
    UIButton *deleteCellButton = (UIButton *)sender;
    FamilyInfoTableViewCell *cell= nil;
    id superView = deleteCellButton.superview;
    
    while (true) {
        if([superView isKindOfClass:[FamilyInfoTableViewCell class]]){
            cell = (FamilyInfoTableViewCell *)superView;
            break;
        }
        superView = ((UIView *)superView).superview;
    }
    self.indexPath = [self.applicationFormTableview indexPathForCell:cell];
    TblFamily *family = [self.applicationFormModel.familyArray objectAtIndex:self.indexPath.row];
    [self.contactDataProvider deleteFamily:family];
    [self.applicationFormModel.familyArray removeObjectAtIndex:self.indexPath.row];
    [self.applicationFormTableview reloadData];
}
- (IBAction)onDeleteButtonWorkExperienceCell:(id)sender
{
    
    UIButton *cellDeleteButton = (UIButton *)sender;
    WorkExperienceTableViewCell *cell= nil;
    id superView = cellDeleteButton.superview;
    
    while (true) {
        if([superView isKindOfClass:[WorkExperienceTableViewCell class]]){
            cell = (WorkExperienceTableViewCell *)superView;
            break;
        }
        superView = ((UIView *)superView).superview;
    }
    self.indexPath = [self.applicationFormTableview indexPathForCell:cell];
    TblExperience *experience = [self.applicationFormModel.workExperienceArray objectAtIndex:self.indexPath.row];
    [self.contactDataProvider deleteExperience:experience];
    [self.applicationFormModel.workExperienceArray removeObjectAtIndex:self.indexPath.row];
    [self.applicationFormTableview reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.cellType == ApplicationFormCellPersonalInfo) {
        return 733.0f;
    }
    else if(self.cellType == ApplicationFormCellFamilyInfo)
    {
        return 130;
    }
    else if (self.cellType == ApplicationFormCellWorkExperience)
    {
        return 178;
    }
    else if (self.cellType == ApplicationFormCellEducation)
    {
        return 211;
    }
    else if (self.cellType == ApplicationFormCellPersonalCertification)
    {
        return 130;
    }
    else if (self.cellType == ApplicationFormCellESignature)
    {
        return 647.0f;
    }
    return 130.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = tableView.frame;
    
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-30, 8, 24, 24)];
    [addButton setImage:[UIImage imageNamed:@"ic_plus.png"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(addNewCell:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *saveButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-70, 8, 44, 24)];
    [saveButton setTitle:NSLocalizedString(@"Save",@"") forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveDataIntoCoreData:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 300, 30)];
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    if (self.cellType == ApplicationFormCellPersonalInfo) {
        title.text = @"个人信息（带*为必填项）";
    }
    else if(self.cellType == ApplicationFormCellFamilyInfo)
    {
        title.text = NSLocalizedString(@"Family Information", "");
        [headerView addSubview:addButton];
    }
    else if(self.cellType == ApplicationFormCellWorkExperience)
    {
        title.text = NSLocalizedString(@"Work Experience", "");
        [headerView addSubview:addButton];
    }
    else if(self.cellType == ApplicationFormCellEducation)
    {
        title.text = NSLocalizedString(@"EducationHeaderTitle", "");
        [headerView addSubview:addButton];
    }
    else if(self.cellType == ApplicationFormCellPersonalCertification)
    {
        title.text = NSLocalizedString(@"Personal Certification", "");
        [headerView addSubview:addButton];
    }
    else if(self.cellType == ApplicationFormCellESignature)
    {
        title.text = NSLocalizedString(@"E-Signature", "");
//        [headerView addSubview:saveButton];
    }
    [headerView addSubview:title];
    
    return headerView;
}

- (void)saveDataIntoCoreData:(id)sender
{
    if([self validate]){
        self.contact.isSync = [NSNumber numberWithBool:YES];
        NSDate * modificationDate = [NSDate dateWithTimeIntervalSince1970:[[NSDate date] timeIntervalSince1970]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kServerDateFormatte];
        NSString * strdate = [formatter stringFromDate:modificationDate];
        NSDate *newmodificationDate = [formatter dateFromString:strdate];
        self.contact.modificationDate = newmodificationDate;
        [self.contactDataProvider saveLocalDBData];
    }
}

- (void)synContactCall
{
    self.reachabilityManager = [AFNetworkReachabilityManager sharedManager];
    if (self.reachabilityManager.reachable)
    {
        SyncModel *syncObj = [[SyncModel alloc]init];
        [MBProgressHUDUpd showHUDAddedTo:self.view animated:YES];
        [syncObj syncObjectsIntoDatabase:[CachingService sharedInstance].currentUserId success:^{
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:CONTACT_LIST_RELOAD_NOTIFICATION
                                                                object:self.contact];
            [self.navigationController popViewControllerAnimated:TRUE];
            
        }failure:^{
            [MBProgressHUDUpd hideAllHUDsForView:self.view animated:YES];
            
        }];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:CONTACT_LIST_RELOAD_NOTIFICATION
                                                            object:self.contact];
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

- (void)addNewCell:(id)sender
{
    if (self.cellType == ApplicationFormCellFamilyInfo)
    {
        if (self.applicationFormModel.familyArray.count < 6) {
            TblFamily *family = [self.contactDataProvider createFamily];
            family.iosAddressCode = [NSString stringWithFormat:@"%@",[[family objectID] URIRepresentation]];
            family.contact = self.applicationFormModel.contact;
            [self.applicationFormModel.familyArray addObject:family];
            [self.applicationFormTableview reloadData];
        }
        else
        {
            

            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"Info", @"")
                                                             message:NSLocalizedString(@"You can not add more than six family members data.", @"")
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                   otherButtonTitles: nil];
            [alert show];
        }
            
        

    }
    else if(self.cellType == ApplicationFormCellWorkExperience)
    {
        if (self.applicationFormModel.workExperienceArray.count < 6)
        {
            TblExperience *experience = [self.contactDataProvider createExperience];
            experience.iosAddressCode = [NSString stringWithFormat:@"%@",[[experience objectID] URIRepresentation]];
            experience.contact = self.applicationFormModel.contact;

            [self.applicationFormModel.workExperienceArray addObject:experience];
            [self.applicationFormTableview reloadData];

        }
        else
        {
            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"Info", @"")
                                                             message: NSLocalizedString(@"You can not add more than six work experince data.", @"")
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                   otherButtonTitles: nil];
            [alert show];
        }


    }
    else if(self.cellType == ApplicationFormCellEducation)
    {
        if (self.applicationFormModel.educationArray.count < 3) {
            TblEducation *education = [self.contactDataProvider createEducation];
            education.iosAddressCode = [NSString stringWithFormat:@"%@",[[education objectID] URIRepresentation]];
            education.contact = self.applicationFormModel.contact;
            [self.applicationFormModel.educationArray addObject:education];
            [self.applicationFormTableview reloadData];
        }
        else
        {
            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"Info", @"")
                                                             message:NSLocalizedString(@"You can not add more than three educational information.", @"")
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                   otherButtonTitles: nil];
            [alert show];
        }
        
    }
    else if(self.cellType == ApplicationFormCellPersonalCertification)
    {
        if (self.applicationFormModel.personalCertificationArray.count < 5)
        {
            TblPersonalCertification *personalCertification = [self.contactDataProvider createPersonalCertification];
            personalCertification.iosAddressCode = [NSString stringWithFormat:@"%@",[[personalCertification objectID] URIRepresentation]];
            personalCertification.contact = self.applicationFormModel.contact;
            [self.applicationFormModel.personalCertificationArray addObject:personalCertification];
            [self.applicationFormTableview reloadData];
        }
        else
        {
            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"Info", @"")
                                                             message:NSLocalizedString(@"You can not add more than five certication information.", @"")
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                   otherButtonTitles: nil];
            [alert show];
        }
        
    }
}
- (IBAction)categoryButtonAction:(id)sender
{
    if ([sender isEqual:self.familyInfoButton])
    {
        self.cellType = ApplicationFormCellFamilyInfo;
    }
    else if([sender isEqual:self.personalInfoButton])
    {
        self.cellType = ApplicationFormCellPersonalInfo;
    }
    else if ([sender isEqual:self.workExperienceButton])
    {
        self.cellType = ApplicationFormCellWorkExperience;
    }
    else if ([sender isEqual:self.educationButton])
    {
        self.cellType = ApplicationFormCellEducation;
    }
    else if ([sender isEqual:self.personalCertificationButton])
    {
        self.cellType = ApplicationFormCellPersonalCertification;
    }
    else if ([sender isEqual:self.eSignatureButton])
    {
        self.cellType = ApplicationFormCellESignature;
    }
    [self.applicationFormTableview reloadData];
}
- (void)keyboardWillShow:(NSNotification *)notification{
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, 450, 0);
    [self.applicationFormTableview setContentInset:edgeInsets];
    [self.applicationFormTableview setScrollIndicatorInsets:edgeInsets];
}

- (void)keyboardWillHide:(NSNotification *)notification{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.applicationFormTableview.contentInset = contentInsets;
    self.applicationFormTableview.scrollIndicatorInsets = contentInsets;
}
- (void)savedImage:(UIImage *)image
{
    NSLog(@"coming");
    self.signImage = image;
    [self.applicationFormTableview reloadData];
}
- (void)esignature{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignatureViewController *signature  = [storyboard instantiateViewControllerWithIdentifier:@"SignatureViewController"];
    signature.delegate = self;
    UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:signature];
    modalViewNavController.navigationBar.hidden = YES;
    modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:modalViewNavController animated:YES completion:nil];
//    [ApplicationFunction showESignaturePopoverCtrl:self.newpopoverController
//                                         withTitle:@"ESignature"
//                                             descr:@"ESignature"
//                                              rect:[MyAppDelegate window].rootViewController.view.bounds
//                                            inView:[MyAppDelegate window].rootViewController.view
//                                     callBackBlock:^{
//                                     }];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    SignatureViewController *signature  = [storyboard instantiateViewControllerWithIdentifier:@"SignatureViewController"];
//    [self.navigationController pushViewController:signature animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
