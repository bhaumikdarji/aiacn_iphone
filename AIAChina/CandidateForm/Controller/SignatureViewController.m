//
//  SignatureViewController.m
//  AIAChina
//
//  Created by Burri on 2015-08-19.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "SignatureViewController.h"
#import "CandidateApplicationFormViewController.h"

@interface SignatureViewController ()
@property (nonatomic, retain) PPSSignatureView *sign;
@property (nonatomic, retain) EAGLContext *newcontext;
@end

@implementation SignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.sign) {
        self.sign = [[PPSSignatureView alloc] initWithFrame:CGRectMake(0, 40, 600, 480) context:nil];

    }
    [self.view addSubview:self.sign];
}
- (IBAction)onBtnSaveSignatureClick:(id)sender {
    UIImage* signature = [self.sign signatureImage];
    
    [self.parentViewController dismissViewControllerAnimated:YES completion:^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CandidateApplicationFormViewController *signatureController  = [storyboard instantiateViewControllerWithIdentifier:@"CandidateApplicationFormViewController"];
        signatureController.signImage = signature;
        [self.delegate savedImage:signature];
    }];
    
}

-(void)getSignature {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
