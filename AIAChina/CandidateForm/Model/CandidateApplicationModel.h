//
//  CandidateApplicationModel.h
//  AIAChina
//
//  Created by Burri on 2015-07-26.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TblContact.h"
@interface CandidateApplicationModel : NSObject
@property (nonatomic, retain) NSMutableArray *familyArray;
@property (nonatomic, retain) NSMutableArray *personalInfoArray;
@property (nonatomic, retain) NSMutableArray *workExperienceArray;
@property (nonatomic, retain) NSMutableArray *educationArray;
@property (nonatomic, retain) NSMutableArray *personalCertificationArray;
@property (nonatomic, retain) NSMutableArray *esignatureArray;
@property (nonatomic, retain) TblContact *contact;
- (void)loadDataFromDatabase:(NSString *)iosAddressCode;

@end
