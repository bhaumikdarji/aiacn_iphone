//
//  CandidateApplicationModel.m
//  AIAChina
//
//  Created by Burri on 2015-07-26.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "CandidateApplicationModel.h"
#import "TblFamily.h"
#import "TblEducation.h"
#import "TblExperience.h"
#import "TblPersonalCertification.h"
#import "TblESignature.h"

@implementation CandidateApplicationModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (void)loadDataFromDatabase:(NSString *)iosAddressCode
{
    NSArray *contactArray = [[DbUtils fetchAllObject:@"TblContact"
                                      andPredict:[NSPredicate predicateWithFormat:@"iosAddressCode == %@",iosAddressCode]
                               andSortDescriptor:nil
                            managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    if (contactArray.count > 0)
    {
        self.contact = [contactArray objectAtIndex:0];
        self.personalInfoArray = [[NSMutableArray alloc]init];
        [self.personalInfoArray addObject:self.contact];
    }
    NSArray *tempFamilyArray = [self.contact.candidateFamilyInfos allObjects];
    self.familyArray = [[NSMutableArray alloc]init];
    if (tempFamilyArray.count > 0) {
        for (TblFamily *family in tempFamilyArray) {
            [self.familyArray addObject:family];
        }
        
    }
    
    
    NSArray *tempWorkExperienceArray = [self.contact.candidateWorkExperiences allObjects];
    self.workExperienceArray = [[NSMutableArray alloc]init];
    if (tempWorkExperienceArray.count > 0) {
        
        for (TblExperience *experience in tempWorkExperienceArray) {
            [self.workExperienceArray addObject:experience];
        }
    }
    
    
    NSArray *tempEducationArray = [self.contact.candidateEducations allObjects];
    self.educationArray = [[NSMutableArray alloc]init];

    if (tempEducationArray.count > 0) {
        for (TblEducation *education in tempEducationArray) {
            [self.educationArray addObject:education];
        }
    }
    
    
    
    NSArray *tempPersonalCertificationArray = [self.contact.candidateProfessionalCertifications allObjects];
    self.personalCertificationArray = [[NSMutableArray alloc]init];

    if (tempPersonalCertificationArray.count > 0)
    {
        for (TblPersonalCertification *certification in tempPersonalCertificationArray) {
            [self.personalCertificationArray addObject:certification];
        }
    }
    
    
    NSArray *tempEsignatureArray = [self.contact.candidateESignatures allObjects];
    self.esignatureArray = [[NSMutableArray alloc]init];
    if (tempEsignatureArray.count > 0) {
        for (TblESignature *eSignature in tempEsignatureArray) {
            [self.esignatureArray addObject:eSignature];
        }
    }
    
    
    
}
-(void)save
{
    
}

@end
