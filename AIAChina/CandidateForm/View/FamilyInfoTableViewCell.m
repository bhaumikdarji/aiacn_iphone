//
//  FamilyInfoTableViewCell.m
//  AIAChina
//
//  Created by Burri on 2015-07-25.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "FamilyInfoTableViewCell.h"
#import "TblFamily.h"
@implementation FamilyInfoTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)loadFamilyDetails:(id)familyInfoDict
{
    self.family = familyInfoDict;
    if (self.family != nil)
    {
        if (self.family.name != nil) {
            self.nameTextField.text = self.family.name;
        }
        if (self.family.unit != nil) {
            self.unitTextField.text = self.family.unit;
        }
        if (self.family.position != nil) {
            self.positionTextField.text = self.family.position;
        }
        if (self.family.phoneNo != nil) {
            self.phoneTextField.text = self.family.phoneNo;
        }
        if (self.family.relationship != nil) {
            self.relationTextField.text = self.family.relationship;
        }
        if (self.family.occupation != nil) {
            self.occupationTextField.text = self.family.occupation;
        }
    }
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
- (IBAction)textChangedInTextField:(id)sender {
    [self saveFamilyInformation:sender];
}
- (void)saveFamilyInformation:(id)sender
{
    if ([sender isEqual:self.nameTextField]) {
        self.family.name = self.nameTextField.text;
    }
    else if([sender isEqual:self.unitTextField])
    {
        self.family.unit = self.unitTextField.text;
    }
    else if ([sender isEqual:self.positionTextField])
    {
        self.family.position = self.positionTextField.text;
    }
    else if ([sender isEqual:self.phoneTextField])
    {
        self.family.phoneNo = self.phoneTextField.text;
    }
    else if ([sender isEqual:self.relationTextField])
    {
        self.family.relationship = self.relationTextField.text;
    }
    else if ([sender isEqual:self.occupationTextField])
    {
        self.family.occupation = self.occupationTextField.text;
    }
}
@end
