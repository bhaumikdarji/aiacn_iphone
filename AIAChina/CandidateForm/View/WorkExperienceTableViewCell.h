//
//  WorkExperienceTableViewCell.h
//  AIAChina
//
//  Created by Burri on 2015-07-25.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TblExperience.h"
@interface WorkExperienceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *cellDeleteButton;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UITextField *witnessTextField;
@property (weak, nonatomic) IBOutlet UITextField *witnessContactNoTextField;

@property (weak, nonatomic) IBOutlet UIButton *endDate;
@property (weak, nonatomic) IBOutlet UITextField *positionTextField;
@property (weak, nonatomic) IBOutlet UITextField *occupationTextField;
@property (weak, nonatomic) IBOutlet UITextField *incomeTextField;
@property (weak, nonatomic) IBOutlet UIButton *startDate;

@property (weak, nonatomic) IBOutlet UITextField *unitTextField;
- (void)loadWorkExperienceDetails:(id)workExpDict;
@property (strong, nonatomic) NSMutableDictionary *workExperienceDict;
@property (strong, nonatomic) TblExperience *experience;
@property (nonatomic, strong) UIPopoverController *popoverController;
@end
