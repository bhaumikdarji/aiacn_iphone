//
//  EducationTableViewCell.h
//  AIAChina
//
//  Created by Burri on 2015-07-26.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TblEducation.h"
@interface EducationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *educationCellDeleteButton;
@property (weak, nonatomic) IBOutlet UIButton *educationStartDate;

@property (weak, nonatomic) IBOutlet UITextField *witnessTextField;
@property (weak, nonatomic) IBOutlet UITextField *educationLevelTextField;
@property (weak, nonatomic) IBOutlet UITextField *schoolTextField;

@property (weak, nonatomic) IBOutlet UIButton *educationEndDate;
@property (weak, nonatomic) IBOutlet UITextField *educationTextField;
@property (strong, nonatomic) IBOutlet UIButton *educationDropDown;
@property (strong, nonatomic) IBOutlet UIButton *educationLevelDropDown;
@property (weak, nonatomic) IBOutlet UILabel *educationHeaderLabel;
@property (weak, nonatomic) IBOutlet UITextField *witnesscontactNoTextField;

- (void)loadEducational:(id)educationDict;
@property (nonatomic,retain) NSMutableDictionary *educationalDict;
@property (nonatomic,retain) TblEducation *education;
@property (nonatomic, strong) UIPopoverController *popoverController;
@end
