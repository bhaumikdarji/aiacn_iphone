//
//  EsignatureTableViewCell.h
//  AIAChina
//
//  Created by Burri on 2015-07-26.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TblESignature.h"
@protocol EsignatureTableViewCellDelegate
- (void)esignature;
@end
@interface EsignatureTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *servicingDeptTextField;
@property (weak, nonatomic) IBOutlet UITextField *agentCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *branchTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *candidateNameTextField;
@property (nonatomic, retain) TblESignature *eSignature;
- (void)loadESignature:(id)eSignature;
@property (weak, nonatomic) IBOutlet UIButton *eSignatureButton;
@property (weak, nonatomic) IBOutlet UIButton *attachedWithInsuraceCoYesButton;
@property (weak, nonatomic) IBOutlet UIButton *attachedWithInsuranceCoNoButton;
@property (weak, nonatomic) IBOutlet UIButton *contactWithAIAYesButton;
@property (weak, nonatomic) IBOutlet UIButton *contactWithAIANoButton;
@property (weak, nonatomic) IBOutlet UIButton *takenPSPTestYesButton;
@property (weak, nonatomic) IBOutlet UIButton *takenPSPTestNoButton;

@property (weak, nonatomic) IBOutlet UIView *signatureViewForBorder;
@property (weak, nonatomic) IBOutlet UIButton *applicationDateButton;
@property (nonatomic,retain) NSMutableDictionary *eSignatureDict;
@property (nonatomic, strong) UIPopoverController *popoverController;
@property (nonatomic, retain) id<EsignatureTableViewCellDelegate> delegate;
@end
