//
//  PersonalInformationTableViewCell.m
//  AIAChina
//
//  Created by Burri on 2015-07-25.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "PersonalInformationTableViewCell.h"
#import "TblContact.h"
#import "NSDate+FFDaysCount.h"
#import "ApplicationFunction.h"
#import "PickerPopoverViewController.h"
#import "DropDownManager.h"

@implementation PersonalInformationTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initializeSubviews];
    [self setTextOnLable];
}

- (void)initializeSubviews
{
    self.btnSameAsRegisteredAddress.layer.cornerRadius = 5.0f;
}

- (void)setTextOnLable{
    
    UIFont *boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0];
    UIFont *regFont = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
    
    NSString *canName = NSLocalizedString(@"Candidate's Name* :", @"");
    [self.lblCandidateName setAttributedText:[Utility setSupAttrOnSingleLable:canName
                                                                     boldFont:boldFont
                                                                  regularFont:regFont
                                                                        range:[canName rangeOfString:@"*"]
                                                             setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *nric = NSLocalizedString(@"NRIC* :", @"");
    [self.lblNric setAttributedText:[Utility setSupAttrOnSingleLable:nric
                                                            boldFont:boldFont
                                                         regularFont:regFont
                                                               range:[nric rangeOfString:@"*"]
                                                    setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *emailText = NSLocalizedString(@"Email* :", @"");
    [self.lblEmail setAttributedText:[Utility setSupAttrOnSingleLable:emailText
                                                            boldFont:boldFont
                                                         regularFont:regFont
                                                               range:[emailText rangeOfString:@"*"]
                                                    setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    
    NSString *dob = NSLocalizedString(@"Date of Birth* :", @"");
    [self.lblDob setAttributedText:[Utility setSupAttrOnSingleLable:dob
                                                           boldFont:boldFont
                                                        regularFont:regFont
                                                              range:[dob rangeOfString:@"*"]
                                                   setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *gender = NSLocalizedString(@"Gender* :", @"");
    [self.lblGender setAttributedText:[Utility setSupAttrOnSingleLable:gender
                                                              boldFont:boldFont
                                                           regularFont:regFont
                                                                 range:[gender rangeOfString:@"*"]
                                                      setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *age = NSLocalizedString(@"Age* :", @"");
    [self.lblAge setAttributedText:[Utility setSupAttrOnSingleLable:age
                                                           boldFont:boldFont
                                                        regularFont:regFont
                                                              range:[age rangeOfString:@"*"]
                                                   setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *education = NSLocalizedString(@"Education* :", @"");
    [self.lblEducation setAttributedText:[Utility setSupAttrOnSingleLable:education
                                                                 boldFont:boldFont
                                                              regularFont:regFont
                                                                    range:[education rangeOfString:@"*"]
                                                         setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *maritalStatus = NSLocalizedString(@"Marital Status* :", @"");
    [self.lblMaritalStatus setAttributedText:[Utility setSupAttrOnSingleLable:maritalStatus
                                                                     boldFont:boldFont
                                                                  regularFont:regFont
                                                                        range:[maritalStatus rangeOfString:@"*"]
                                                             setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *annualIncome = NSLocalizedString(@"Annual Income* :", @"");
    [self.lblAnnualIncome setAttributedText:[Utility setSupAttrOnSingleLable:annualIncome
                                                                    boldFont:boldFont
                                                                 regularFont:regFont
                                                                       range:[annualIncome rangeOfString:@"*"]
                                                                setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *workExp = NSLocalizedString(@"Work Experience* :", @"");
    [self.lblWorkingExp setAttributedText:[Utility setSupAttrOnSingleLable:workExp
                                                                     boldFont:boldFont
                                                                  regularFont:regFont
                                                                        range:[workExp rangeOfString:@"*"]
                                                             setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    
    NSString *sourceOfRefer = NSLocalizedString(@"Source of refer* :", @"");
    [self.lblSOR setAttributedText:[Utility setSupAttrOnSingleLable:sourceOfRefer
                                                                  boldFont:boldFont
                                                               regularFont:regFont
                                                                     range:[sourceOfRefer rangeOfString:@"*"]
                                                          setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];

    NSString *address = NSLocalizedString(@"Address* :", @"");
    [self.lblAdd1 setAttributedText:[Utility setSupAttrOnSingleLable:address
                                                            boldFont:boldFont
                                                         regularFont:regFont
                                                               range:[address rangeOfString:@"*"]
                                                    setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    [self.lblAdd2 setAttributedText:[Utility setSupAttrOnSingleLable:address
                                                            boldFont:boldFont
                                                         regularFont:regFont
                                                               range:[address rangeOfString:@"*"]
                                                    setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    [self.lblAdd3 setAttributedText:[Utility setSupAttrOnSingleLable:address
                                                            boldFont:boldFont
                                                         regularFont:regFont
                                                               range:[address rangeOfString:@"*"]
                                                    setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    
    NSString *resAddress = NSLocalizedString(@"Residential Address* :", @"");
    [self.lblResAdd1 setAttributedText:[Utility setSupAttrOnSingleLable:resAddress
                                                               boldFont:boldFont
                                                            regularFont:regFont
                                                                  range:[resAddress rangeOfString:@"*"]
                                                       setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    [self.lblResAdd2 setAttributedText:[Utility setSupAttrOnSingleLable:resAddress
                                                               boldFont:boldFont
                                                            regularFont:regFont
                                                                  range:[resAddress rangeOfString:@"*"]
                                                       setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    [self.lblResAdd3 setAttributedText:[Utility setSupAttrOnSingleLable:resAddress
                                                               boldFont:boldFont
                                                            regularFont:regFont
                                                                  range:[resAddress rangeOfString:@"*"]
                                                       setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *mobileNo = NSLocalizedString(@"Mobile No* :", @"");
    [self.lblMobile setAttributedText:[Utility setSupAttrOnSingleLable:mobileNo
                                                              boldFont:boldFont
                                                           regularFont:regFont
                                                                 range:[mobileNo rangeOfString:@"*"]
                                                      setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *identyType = NSLocalizedString(@"Identity Type* :", @"");
    [self.lblIdType setAttributedText:[Utility setSupAttrOnSingleLable:identyType
                                                              boldFont:boldFont
                                                           regularFont:regFont
                                                                 range:[identyType rangeOfString:@"*"]
                                                      setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *newcomer = NSLocalizedString(@"Newcomer source* :", @"");
    [self.lblNewcomer setAttributedText:[Utility setSupAttrOnSingleLable:newcomer
                                                                boldFont:boldFont
                                                             regularFont:regFont
                                                                   range:[newcomer rangeOfString:@"*"]
                                                        setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *boughtIns = NSLocalizedString(@"Bought Insurance before?*", @"");
    [self.lblBoughtIns setAttributedText:[Utility setSupAttrOnSingleLable:boughtIns
                                                                 boldFont:boldFont
                                                              regularFont:regFont
                                                                    range:[boughtIns rangeOfString:@"*"]
                                                         setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *returnAgent = NSLocalizedString(@"Return agent?*", @"");
    [self.lblReturnAgent setAttributedText:[Utility setSupAttrOnSingleLable:returnAgent
                                                                   boldFont:boldFont
                                                                regularFont:regFont
                                                                      range:[returnAgent rangeOfString:@"*"]
                                                           setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
    NSString *saleExp = NSLocalizedString(@"Have sales experience?*", @"");
    [self.lblHaveSaleExp setAttributedText:[Utility setSupAttrOnSingleLable:saleExp
                                                                   boldFont:boldFont
                                                                regularFont:regFont
                                                                      range:[saleExp rangeOfString:@"*"]
                                                           setBoldFontColor:[UIColor colorWithRed:203.0/255.0 green:32.0/255.0 blue:41.0/255.0 alpha:1.0]]];
}

- (IBAction)onBtnGenderClick:(id)sender {
     [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_GENDER]];
}

- (IBAction)onBtnMaritalStatusClick:(id)sender {
    [self endEditing:YES];
     [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_MARITAL_STATUS]];
}

- (IBAction)onBtnEducationClick:(id)sender {
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_EDUCATION]];
}

- (IBAction)personalInforTextFieldEditing:(id)sender {
    [self savePersonalInformation:sender];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.nameTextField && (textField.text.length+string.length) <= CANDIDATE_NAME)
        return YES;
    else if (textField == self.nricTextField && (textField.text.length+string.length) <= NRIC)
        return YES;
    else if (textField == self.addressLine2TextField && (textField.text.length+string.length + self.btnAddress1.titleLabel.text.length) <= ADDRESS)
        return YES;
    else if (textField == self.residentialAddressLine2TextField && (textField.text.length+string.length + self.btnResidentailAddress1.titleLabel.text.length) <= ADDRESS)
        return YES;
    else if (textField == self.fixedLineNoTextField && [Utility isNumeric:string] && (textField.text.length+string.length) <= FIXED_LINE_NO)
        return YES;
    else if (textField == self.mobileNumberTextField && [Utility isNumeric:string] && (textField.text.length+string.length) <= MOBILE_NO)
        return YES;
    else if (textField == self.mobileNumberTextField && (textField.text.length+string.length) <= MOBILE_NO)
        return YES;
    else if (textField == self.annualIncomeTextField)
        return YES;
    else if (textField == self.emailTextField) {
        return YES;
    }
    else
        return NO;
}

- (void)savePersonalInformation:(id)sender
{
    if ([sender isEqual:self.nameTextField]) {
        self.contact.name = self.nameTextField.text;
    }
    else if([sender isEqual:self.addressLine2TextField])
    {
        self.contact.registeredAddress1 = self.addressLine2TextField.text;
    }
    else if ([sender isEqual:self.mobileNumberTextField])
    {
        self.contact.mobilePhoneNo = self.mobileNumberTextField.text;
    }
    else if ([sender isEqual:self.nricTextField])
    {
        self.contact.nric = self.nricTextField.text;
    }
    else if([sender isEqual:self.annualIncomeTextField])
    {
        self.contact.yearlyIncome = self.annualIncomeTextField.text;
    }
    else if([sender isEqual:self.residentialAddressLine2TextField])
    {
        self.contact.residentialAddress1 = self.residentialAddressLine2TextField.text;
    }
    else if([sender isEqual:self.fixedLineNoTextField])
    {
        self.contact.fixedLineNO = self.fixedLineNoTextField.text;
    }
    else if([sender isEqual:self.emailTextField])
    {
        self.contact.eMailId = self.emailTextField.text;
    }
}

- (void)loadPersonalDetails:(id)contact{
    
        self.contact = contact;
        if (self.contact != nil) {
            if (self.contact.name != nil) {
                self.nameTextField.text = self.contact.name;
            }
            if (self.contact.registeredAddress != nil) {
                [self.btnAddress1 setTitle:self.contact.registeredAddress forState:UIControlStateNormal];
            }
            if (self.contact.registeredAddress1 != nil) {
                self.addressLine2TextField.text = self.contact.registeredAddress1;
            }
            if (self.contact.nric != nil) {
                self.nricTextField.text = self.contact.nric;
            }
            if (self.contact.workingYearExperience != nil) {
                [self.btnWorkExperience setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_WORK_EXPERIENCE code:self.contact.workingYearExperience] forState:UIControlStateNormal];
            }
            if (self.contact.yearlyIncome != nil) {
                self.annualIncomeTextField.text = self.contact.yearlyIncome;
            }
            
            if (self.contact.eMailId != nil) {
                self.emailTextField.text = self.contact.eMailId;
            }
            
            if (self.contact.mobilePhoneNo != nil) {
                self.mobileNumberTextField.text = self.contact.mobilePhoneNo;
            }
            if (self.contact.age != nil) {
                [self.ageButton setTitle:[self.contact.age stringValue]forState:UIControlStateNormal];
            }
            
            if (self.contact.residentialAddress != nil) {
                [self.btnResidentailAddress1 setTitle:self.contact.residentialAddress forState:UIControlStateNormal];
            }
            if (self.contact.residentialAddress1 != nil) {
                self.residentialAddressLine2TextField.text = self.contact.residentialAddress1;
            }
            
            if (self.contact.fixedLineNO != nil) {
                self.fixedLineNoTextField.text = self.contact.fixedLineNO;
            }
            
            if (self.contact.marritalStatus != nil) {
                [self.maritalStatusButton setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_MARITAL_STATUS code:self.contact.marritalStatus] forState:UIControlStateNormal];
            }
            if (self.contact.gender != nil) {
                NSString *genderString = [self.contact.gender stringByReplacingOccurrencesOfString:@" " withString:@""];
                [self.genderButton setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_GENDER code:genderString] forState:UIControlStateNormal];
            }
            if (self.contact.education != nil){
                [self.educationButton setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_EDUCATION code:self.contact.education] forState:UIControlStateNormal];
            }
            if (self.contact.birthDate != nil) {
                [self.dateOfBirthButton setTitle:[NSDate dateToString:self.contact.birthDate andFormate:kDisplayDateFormatte] forState:UIControlStateNormal];
            }
            if (self.contact.reJoin != nil){
                [self.returnAgentButton setTitle:self.contact.reJoin forState:UIControlStateNormal];
            }
            if (self.contact.purchasedAnyInsurance != nil){
                [self.boughtInsuranceBeforeButton setTitle:self.contact.purchasedAnyInsurance forState:UIControlStateNormal];

            }
            if (self.contact.salesExperience != nil){
                [self.haveSaleExperienceButton setTitle:self.contact.salesExperience forState:UIControlStateNormal];

            }
            if (self.contact.newcomerSource != nil){
                [self.btnNewComerSource setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_NEWCOMER_SOURCE code:self.contact.newcomerSource]   forState:UIControlStateNormal];
                
            }
            if (self.contact.identityType != nil){
                [self.btnIdentityType setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_IDENTITY_TYPE code:self.contact.identityType] forState:UIControlStateNormal];
                
            }
            if(self.contact.referalSource != nil){
              [self.btnSOR setTitle:[[DropDownManager sharedManager] getDropdownDesc:DROP_DOWN_SOURCE_OF_REFERRAL code:self.contact.referalSource] forState:UIControlStateNormal];
            }
        }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

 - (IBAction)onBtnRejoinClick:(id)sender {
 [self endEditing:YES];
 [self setPopoverView:sender pickerArray:@[@"是", @"否"]];
 }
 
- (IBAction)onBtnPurInsBeforeClick:(id)sender {
     [self endEditing:YES];
     [self setPopoverView:sender pickerArray:@[@"是", @"否"]];
}

- (IBAction)btnIdentityTypeClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_IDENTITY_TYPE]];
}

- (IBAction)btnSameAsRegisteredAddressClick:(id)sender {
    [self.btnAddress1 setTitle:[self.btnResidentailAddress1 currentTitle] forState:UIControlStateNormal];
    self.addressLine2TextField.text =  self.residentialAddressLine2TextField.text;
    
    self.contact.registeredAddress = [self.btnResidentailAddress1 currentTitle];
    self.contact.registeredAddress1 = self.residentialAddressLine2TextField.text;
}

- (IBAction)onBtnSaleExpClick:(id)sender {
     [self endEditing:YES];
     [self setPopoverView:sender pickerArray:@[@"有", @"无"]];
}
- (IBAction)btnNewComerSourceClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_NEWCOMER_SOURCE]];
}

- (void)setPopoverView:(UIButton *)btn pickerArray:(NSArray *)pickerArray{
    NSMutableArray *pickerDataArray = [[NSMutableArray alloc] initWithObjects:@"", nil];
    [pickerDataArray addObjectsFromArray:pickerArray];
    PickerPopoverViewController *popoverViewCtrl = [[PickerPopoverViewController alloc] initWithNibName:@"PickerPopoverViewController" bundle:nil];
    if(isEmptyString(btn.titleLabel.text))
        [btn setTitle:pickerDataArray[0] forState:UIControlStateNormal];
    else
        popoverViewCtrl.oldValue = btn.titleLabel.text;
    
    [popoverViewCtrl setPickerArray:pickerDataArray];
    [popoverViewCtrl setSelectedIndex:^(int selectedRowIndexPath) {
        [btn setTitle:pickerDataArray[selectedRowIndexPath] forState:UIControlStateNormal];
        if(self.contact){
            if ([btn isEqual:self.educationButton]) {
                self.contact.education = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_EDUCATION description:pickerDataArray[selectedRowIndexPath]];
            }
            else if ([btn isEqual:self.genderButton])
            {
                self.contact.gender = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_GENDER description:pickerDataArray[selectedRowIndexPath]];
            }
            else if ([btn isEqual:self.maritalStatusButton])
            {
                self.contact.marritalStatus = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_MARITAL_STATUS description:pickerDataArray[selectedRowIndexPath]];
            }
            else if ([btn isEqual:self.btnNewComerSource])
            {
                self.contact.newcomerSource = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_NEWCOMER_SOURCE description:pickerDataArray[selectedRowIndexPath]];
            }
            else if ([btn isEqual:self.btnIdentityType])
            {
                self.contact.identityType = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_IDENTITY_TYPE description:pickerDataArray[selectedRowIndexPath]];
            }
            else if ([btn isEqual:self.boughtInsuranceBeforeButton])
            {
                self.contact.purchasedAnyInsurance = self.boughtInsuranceBeforeButton.currentTitle;
            }
            else if ([btn isEqual:self.haveSaleExperienceButton])
            {
                self.contact.salesExperience = self.haveSaleExperienceButton.currentTitle;

            }
            else if ([btn isEqual:self.returnAgentButton])
            {
                self.contact.reJoin = self.returnAgentButton.currentTitle;
            }
            else if ([btn isEqual:self.btnSOR])
            {
                self.contact.referalSource = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_SOURCE_OF_REFERRAL description:self.btnSOR.currentTitle];
            }
            
            else if ([btn isEqual:self.btnWorkExperience])
            {
                self.contact.workingYearExperience = [[DropDownManager sharedManager] getDropdownCode:DROP_DOWN_WORK_EXPERIENCE description:self.btnWorkExperience.currentTitle];
            }
        }else{
            [self.popoverController dismissPopoverAnimated:TRUE];
        }
    }];
    
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverViewCtrl];
    if ([btn isEqual:self.btnSOR]) {
        self.popoverController.popoverContentSize = CGSizeMake(300, 650);
    }
    else
    {
        self.popoverController.popoverContentSize = popoverViewCtrl.view.frame.size;
    }
    [self.popoverController presentPopoverFromRect:btn.frame
                                            inView:self
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:TRUE];
}

- (IBAction)onBtnWorkExpClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_WORK_EXPERIENCE]];
}

- (IBAction)onBtnSORClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_SOURCE_OF_REFERRAL]];
}

- (IBAction)onBtnRaceClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_RACE]];
}

- (IBAction)onBtnTalentProfileClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_TALENT_PROFILE]];
}

- (IBAction)onBtnRecommendSchemeClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_RECOMMEND_SCHEME]];
}

- (IBAction)onBtnPresentEmpConditionClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_PRESENT_EMPLOYMENT_CONDITION]];
}

- (IBAction)onBtnNatureOfBusinessClick:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_NATURE_OF_BUSINESS]];
}

@end
