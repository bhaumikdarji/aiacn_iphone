//
//  WorkExperienceTableViewCell.m
//  AIAChina
//
//  Created by Burri on 2015-07-25.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "WorkExperienceTableViewCell.h"
#import "TblExperience.h"
#import "CandidateExperienceMTLModel.h"
@implementation WorkExperienceTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)textIsEditingInTextField:(id)sender {
    [self saveWorkExpInformation:sender];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (void)saveWorkExpInformation:(id)sender
{
   
    if ([sender isEqual:self.witnessTextField]) {
        self.experience.witness = self.witnessTextField.text;
    }
    else if([sender isEqual:self.witnessContactNoTextField])
    {
        self.experience.witnessContactNo = self.witnessContactNoTextField.text;
    }
    else if ([sender isEqual:self.positionTextField])
    {
        self.experience.position = self.positionTextField.text;
    }
    else if ([sender isEqual:self.unitTextField])
    {
        self.experience.unit = self.unitTextField.text;
    }
    else if ([sender isEqual:self.incomeTextField])
    {
        self.experience.income = self.incomeTextField.text;
    }
    else if ([sender isEqual:self.occupationTextField])
    {
        self.experience.occupation = self.occupationTextField.text;
    }
}

- (void)loadWorkExperienceDetails:(id)workExpDict
{
    self.experience = workExpDict;
    
      if (self.experience != nil) {
          if (self.experience.witness != nil) {
              self.witnessTextField.text = self.experience.witness;
          }
          if (self.experience.witnessContactNo != nil) {
              self.witnessContactNoTextField.text = self.experience.witnessContactNo;
          }
          if (self.experience.position != nil) {
              self.positionTextField.text = self.experience.position;
          }
          
          if (self.experience.unit != nil) {
              self.unitTextField.text = self.experience.unit;
          }
          if (self.experience.income != nil) {
              self.incomeTextField.text = self.experience.income;
          }
          if (self.experience.occupation != nil) {
              self.occupationTextField.text = self.experience.occupation;
          }
          if (self.experience.startDate != nil ) {
              [self.startDate setTitle:self.experience.startDate  forState:UIControlStateNormal];
          }
          if (self.experience.endDate != nil) {
              [self.endDate setTitle:self.experience.endDate forState:UIControlStateNormal];
          }
          
        
    }
}
@end
