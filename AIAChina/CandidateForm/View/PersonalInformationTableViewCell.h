//
//  PersonalInformationTableViewCell.h
//  AIAChina
//
//  Created by Burri on 2015-07-25.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TblContact.h"
@interface PersonalInformationTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblCandidateName;
@property (weak, nonatomic) IBOutlet UILabel *lblDob;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblEducation;
@property (weak, nonatomic) IBOutlet UILabel *lblMobile;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblAnnualIncome;
@property (weak, nonatomic) IBOutlet UILabel *lblAdd1;
@property (weak, nonatomic) IBOutlet UILabel *lblAdd2;
@property (weak, nonatomic) IBOutlet UILabel *lblAdd3;
@property (weak, nonatomic) IBOutlet UILabel *lblIdType;
@property (weak, nonatomic) IBOutlet UILabel *lblNric;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblMaritalStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkingExp;
@property (weak, nonatomic) IBOutlet UILabel *lblResAdd1;
@property (weak, nonatomic) IBOutlet UILabel *lblResAdd2;
@property (weak, nonatomic) IBOutlet UILabel *lblResAdd3;
@property (weak, nonatomic) IBOutlet UILabel *lblSOR;
@property (weak, nonatomic) IBOutlet UILabel *lblBoughtIns;
@property (weak, nonatomic) IBOutlet UILabel *lblHaveSaleExp;
@property (weak, nonatomic) IBOutlet UILabel *lblNewcomer;
@property (weak, nonatomic) IBOutlet UILabel *lblReturnAgent;

@property (weak, nonatomic) IBOutlet UIButton *dateOfBirthButton;
@property (weak, nonatomic) IBOutlet UIButton *ageButton;
@property (weak, nonatomic) IBOutlet UIButton *educationButton;
@property (weak, nonatomic) IBOutlet UIButton *btnAddress1;
@property (weak, nonatomic) IBOutlet UIButton *btnIdentityType;
@property (weak, nonatomic) IBOutlet UIButton *genderButton;
@property (weak, nonatomic) IBOutlet UIButton *maritalStatusButton;
@property (weak, nonatomic) IBOutlet UIButton *btnWorkExperience;
@property (weak, nonatomic) IBOutlet UIButton *btnResidentailAddress1;
@property (weak, nonatomic) IBOutlet UIButton *btnResidentailAddress3;
@property (weak, nonatomic) IBOutlet UIButton *btnSOR;
@property (weak, nonatomic) IBOutlet UIButton *boughtInsuranceBeforeButton;
@property (weak, nonatomic) IBOutlet UIButton *haveSaleExperienceButton;
@property (weak, nonatomic) IBOutlet UIButton *btnNewComerSource;
@property (weak, nonatomic) IBOutlet UIButton *returnAgentButton;
@property (weak, nonatomic) IBOutlet UIButton *btnSameAsRegisteredAddress;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *annualIncomeTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressLine2TextField;
@property (weak, nonatomic) IBOutlet UITextField *nricTextField;
@property (weak, nonatomic) IBOutlet UITextField *fixedLineNoTextField;
@property (weak, nonatomic) IBOutlet UITextField *residentialAddressLine2TextField;

@property (nonatomic, retain) TblContact *contact;
@property (strong, nonatomic) NSMutableDictionary *personalInfoDict;
@property (nonatomic, strong) UIPopoverController *popoverController;

- (IBAction)onBtnSORClick:(id)sender;
- (IBAction)onBtnRaceClick:(id)sender;
- (IBAction)onBtnTalentProfileClick:(id)sender;
- (IBAction)onBtnRecommendSchemeClick:(id)sender;
- (IBAction)onBtnPresentEmpConditionClick:(id)sender;
- (IBAction)onBtnNatureOfBusinessClick:(id)sender;
- (void)loadPersonalDetails:(id)contact;

@end

