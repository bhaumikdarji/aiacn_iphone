//
//  PersonalCertificationTableViewCell.h
//  AIAChina
//
//  Created by Burri on 2015-07-26.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TblPersonalCertification.h"
@interface PersonalCertificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *personalCertificationCellDeleteButton;
@property (weak, nonatomic) IBOutlet UILabel *personalCellHeaderLabel;
@property (weak, nonatomic) IBOutlet UITextField *certificationNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *charterDateButton;
@property (weak, nonatomic) IBOutlet UITextField *charterAgencyTextField;
- (void)loadCertification:(id)personalCertificationDict;
@property (nonatomic,retain) NSMutableDictionary *certificationDict;
@property (nonatomic, strong) UIPopoverController *popoverController;
@property (nonatomic, retain) TblPersonalCertification *personalCertification;
@end
