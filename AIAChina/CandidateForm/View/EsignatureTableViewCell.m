//
//  EsignatureTableViewCell.m
//  AIAChina
//
//  Created by Burri on 2015-07-26.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EsignatureTableViewCell.h"
#import "CandidateESignatureMTLModel.h"
#import "ApplicationFunction.h"
#import "PickerPopoverViewController.h"
#import "DropDownManager.h"
#import "SignatureViewController.h"
@implementation EsignatureTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.signatureViewForBorder.layer.borderWidth = 1.0f;
    self.signatureViewForBorder.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)signatureTextFieldEditing:(id)sender
{
    [self saveESignatureInfo:sender];
}

- (void)loadESignature:(id)eSignature
{
            self.eSignature = eSignature;
        if (self.eSignature != nil)
        {
            if (self.eSignature.serviceDepartment != nil) {
                self.servicingDeptTextField.text = self.eSignature.serviceDepartment;
            }
            if (self.eSignature.agentId != nil) {
                self.agentCodeTextField.text = self.eSignature.agentId;
            }
            if (self.eSignature.branch != nil) {
                self.branchTextField.text = self.eSignature.branch;
            }
            if (self.eSignature.candidateName != nil) {
                self.candidateNameTextField.text = self.eSignature.candidateName;
            }
            if (self.eSignature.city != nil) {
                self.cityTextField.text = self.eSignature.city;
            }
            if (self.eSignature.applicationDate != nil)
            {
                NSDate *date = [NSDate stringToDate:self.eSignature.applicationDate dateFormat:kServerDateFormatte];
                NSString *applicationDate = [NSDate dateToString:date andFormate:kDisplayDateFormatte];
                [self.applicationDateButton setTitle:applicationDate forState:UIControlStateNormal];
            }
            if (self.eSignature.eSignaturePhoto != nil) {
                [self.eSignatureButton setBackgroundImage:[UIImage imageWithData:self.eSignature.eSignaturePhoto] forState:UIControlStateNormal];
            }
            if (self.eSignature.takenPspTestValue) {
                [self.takenPSPTestYesButton setSelected:YES];
            }
            else
            {
                [self.takenPSPTestNoButton setSelected:YES];
            }
            if (self.eSignature.atachedWithInsuranceCoValue) {
                [self.attachedWithInsuraceCoYesButton setSelected:YES];
            }
            else
            {
                [self.attachedWithInsuranceCoNoButton setSelected:YES];
            }
            if (self.eSignature.contactWithAiaValue) {
                [self.contactWithAIAYesButton setSelected:YES];
            }
            else
            {
                [self.contactWithAIANoButton setSelected:YES];
            }

            
            
            
        }
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
- (IBAction)onApplicationDateButtonClick:(id)sender{
    
}
- (void)saveESignatureInfo:(id)sender
{
    if ([sender isEqual:self.servicingDeptTextField]) {
        self.eSignature.serviceDepartment = self.servicingDeptTextField.text;
    }
    else if([sender isEqual:self.agentCodeTextField])
    {
        self.eSignature.agentId = self.agentCodeTextField.text;
    }
    else if ([sender isEqual:self.branchTextField]) {
        self.eSignature.branch = self.branchTextField.text;
    }
    else if([sender isEqual:self.candidateNameTextField])
    {
        self.eSignature.candidateName = self.candidateNameTextField.text;
    }
    
    else if ([sender isEqual:self.cityTextField]) {
        self.eSignature.city = self.cityTextField.text;

    }
   

    
    
}
- (IBAction)onYesOrNoButtonClick:(id)sender {
    if ([sender isEqual:self.attachedWithInsuraceCoYesButton])
    {
        UIButton *btn = (UIButton *)sender;
        if([btn isSelected]){
            [btn setSelected:FALSE];
            [self.attachedWithInsuranceCoNoButton setSelected:TRUE];
            self.eSignature.atachedWithInsuranceCoValue = NO;
            
        }else{
            [btn setSelected:TRUE];
            [self.attachedWithInsuranceCoNoButton setSelected:FALSE];
            self.eSignature.atachedWithInsuranceCoValue = YES;
        }
    }
    else if ([sender isEqual:self.attachedWithInsuranceCoNoButton])
    {
        UIButton *btn = (UIButton *)sender;
        if([btn isSelected]){
            [btn setSelected:FALSE];
            [self.attachedWithInsuraceCoYesButton setSelected:TRUE];
            self.eSignature.atachedWithInsuranceCoValue = YES;
            
        }else{
            [btn setSelected:TRUE];
            [self.attachedWithInsuraceCoYesButton setSelected:FALSE];
            self.eSignature.atachedWithInsuranceCoValue = NO;
        }
    }
    if ([sender isEqual:self.contactWithAIAYesButton])
    {
        UIButton *btn = (UIButton *)sender;
        if([btn isSelected]){
            [btn setSelected:FALSE];
            [self.contactWithAIANoButton setSelected:TRUE];
            self.eSignature.contactWithAiaValue = NO;
            
        }else{
            [btn setSelected:TRUE];
            [self.contactWithAIANoButton setSelected:FALSE];
            self.eSignature.contactWithAiaValue = YES;
        }
    }
    else if ([sender isEqual:self.contactWithAIANoButton])
    {
        UIButton *btn = (UIButton *)sender;
        if([btn isSelected]){
            [btn setSelected:FALSE];
            [self.contactWithAIAYesButton setSelected:TRUE];
            self.eSignature.contactWithAiaValue = YES;
            
        }else{
            [btn setSelected:TRUE];
            [self.contactWithAIAYesButton setSelected:FALSE];
            self.eSignature.contactWithAiaValue = NO;
        }
    }
    if ([sender isEqual:self.takenPSPTestYesButton])
    {
        UIButton *btn = (UIButton *)sender;
        if([btn isSelected]){
            [btn setSelected:FALSE];
            [self.takenPSPTestNoButton setSelected:TRUE];
            self.eSignature.takenPspTestValue = NO;
            
        }else{
            [btn setSelected:TRUE];
            [self.takenPSPTestNoButton setSelected:FALSE];
            self.eSignature.takenPspTestValue = YES;
        }
    }
    else if ([sender isEqual:self.takenPSPTestNoButton])
    {
        UIButton *btn = (UIButton *)sender;
        if([btn isSelected]){
            [btn setSelected:FALSE];
            [self.takenPSPTestYesButton setSelected:TRUE];
            self.eSignature.takenPspTestValue = YES;
            
        }else{
            [btn setSelected:TRUE];
            [self.takenPSPTestYesButton setSelected:FALSE];
            self.eSignature.takenPspTestValue = NO;
        }
    }
}
- (IBAction)onESignatureButtonClick:(id)sender {
    [self.delegate esignature];
    
    
}
@end
