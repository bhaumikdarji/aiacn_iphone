//
//  PersonalCertificationTableViewCell.m
//  AIAChina
//
//  Created by Burri on 2015-07-26.
//  Copyright (c) 2015 AIA. All rights reserved.
//
//
#import "PersonalCertificationTableViewCell.h"
#import "PersonalCertificationMTLModel.h"
#import "TblPersonalCertification.h"
#import "ApplicationFunction.h"
#import "PickerPopoverViewController.h"
#import "DropDownManager.h"
@implementation PersonalCertificationTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)personalCertificationCellTextFieldTextEditing:(id)sender {
    [self saveCertificationInformation:sender];
}
- (void)loadCertification:(id)personalCertificationDict
{
    self.personalCertification = personalCertificationDict;
    self.certificationDict = personalCertificationDict;
    if (self.certificationDict != nil) {
        if (self.personalCertification.certificateName)
        {
            self.certificationNameTextField.text = self.personalCertification.certificateName;
            
        }
        if (self.personalCertification.charterAgency)
        {
            self.charterAgencyTextField.text = self.personalCertification.charterAgency;
        }
        if (self.personalCertification.charterDate)
        {
            [self.charterDateButton setTitle:self.personalCertification.charterDate forState:UIControlStateNormal];
            
        }
        
       
        
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
- (void)saveCertificationInformation:(id)sender
{
    if ([sender isEqual:self.certificationNameTextField]) {
        self.personalCertification.certificateName = self.certificationNameTextField.text;
    }
    else if([sender isEqual:self.charterAgencyTextField])
    {
        self.personalCertification.charterAgency = self.charterAgencyTextField.text;
    }
    
}
@end
