//
//  EducationTableViewCell.m
//  AIAChina
//
//  Created by Burri on 2015-07-26.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import "EducationTableViewCell.h"
#import "NSDate+FFDaysCount.h"
#import "ApplicationFunction.h"
#import "PickerPopoverViewController.h"
#import "DropDownManager.h"
@implementation EducationTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)educationTextFieldEditing:(id)sender {
    [self saveEducationalInformation:sender];
}
- (void)loadEducational:(id)educationDict
{
    self.education = educationDict;
    if (self.education != nil)
    {
        if (self.education.witness != nil) {
            self.witnessTextField.text = self.education.witness;
        }
        if (self.education.educationLevel != nil)
        {
//            self.educationLevelTextField.text = self.education.educationLevel;
            [self.educationLevelDropDown setTitle:self.education.educationLevel forState:UIControlStateNormal];
        }
        if (self.education.school != nil) {
            self.schoolTextField.text = self.education.school;

        }
        if (self.education.education != nil) {
//            self.educationTextField.text = self.education.education;
            [self.educationDropDown setTitle:self.education.education forState:UIControlStateNormal];
        }
        if (self.education.witnessContactNo != nil) {
            self.witnesscontactNoTextField.text = self.education.witnessContactNo;
            
        }
        if (self.education.startDate != nil) {
            [self.educationStartDate setTitle:self.education.startDate forState:UIControlStateNormal];
        }
        if (self.education.endDate != nil) {
            [self.educationEndDate setTitle:self.education.endDate forState:UIControlStateNormal];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (void)saveEducationalInformation:(id)sender{
    if ([sender isEqual:self.witnessTextField]) {
        self.education.witness = self.witnessTextField.text;
    }
    else if([sender isEqual:self.educationLevelDropDown])
    {
        //        self.education.educationLevel = self.educationLevelTextField.text;
        self.education.educationLevel = self.educationLevelDropDown.titleLabel.text;
    }
    else if ([sender isEqual:self.schoolTextField])
    {
        self.education.school = self.schoolTextField.text;
        
    }
    else if ([sender isEqual:self.educationDropDown])
    {
        //        self.education.education = self.educationTextField.text;
        self.education.education = self.educationDropDown.titleLabel.text;
        
    }
    else if ([sender isEqual:self.witnesscontactNoTextField])
    {
        self.education.witnessContactNo = self.witnesscontactNoTextField.text;
    }
    
}
- (IBAction)onBtneducationClicked:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_EDUCATION]];
}
- (IBAction)onBtnEducationLevelClicked:(id)sender {
    [self endEditing:YES];
    [self setPopoverView:sender pickerArray:[[DropDownManager sharedManager] getDropDownDescArray:DROP_DOWN_EDUCATIONLEVEL]];
}

- (void)setPopoverView:(UIButton *)btn pickerArray:(NSArray *)pickerArray{
    NSMutableArray *pickerDataArray = [[NSMutableArray alloc] initWithObjects:@"", nil];
    [pickerDataArray addObjectsFromArray:pickerArray];
    PickerPopoverViewController *popoverViewCtrl = [[PickerPopoverViewController alloc] initWithNibName:@"PickerPopoverViewController" bundle:nil];
    if(isEmptyString(btn.titleLabel.text))
        [btn setTitle:pickerDataArray[0] forState:UIControlStateNormal];
    else
        popoverViewCtrl.oldValue = btn.titleLabel.text;
    
    [popoverViewCtrl setPickerArray:pickerDataArray];
    [popoverViewCtrl setSelectedIndex:^(int selectedRowIndexPath) {
        [btn setTitle:pickerDataArray[selectedRowIndexPath] forState:UIControlStateNormal];
        [self.popoverController dismissPopoverAnimated:TRUE];
        [self saveEducationalInformation:btn];
    }];
    
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverViewCtrl];
    self.popoverController.popoverContentSize = popoverViewCtrl.view.frame.size;
    [self.popoverController presentPopoverFromRect:btn.frame
                                            inView:self
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:TRUE];
}
@end
