//
//  FamilyInfoTableViewCell.h
//  AIAChina
//
//  Created by Burri on 2015-07-25.
//  Copyright (c) 2015 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TblFamily.h"
@interface FamilyInfoTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *unitTextField;
@property (weak, nonatomic) IBOutlet UITextField *positionTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *relationTextField;

@property (weak, nonatomic) IBOutlet UITextField *occupationTextField;
@property (strong, nonatomic) NSMutableDictionary *familyDict;
@property (weak, nonatomic) IBOutlet UIButton *deleteCellButton;
@property (weak, nonatomic) IBOutlet UILabel *cellHeaderLabel;
@property (nonatomic, retain) TblFamily *family;
- (void)loadFamilyDetails:(id)familyInfoDict;
@end
