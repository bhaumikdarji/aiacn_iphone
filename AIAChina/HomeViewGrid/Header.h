//
//  Header.h
//  AIAChina
//
//  Created by Purva on 22/01/16.
//  Copyright © 2016 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Header : UICollectionReusableView
@property (nonatomic, weak) IBOutlet UIButton *btnimage;
@property (nonatomic, weak) IBOutlet UILabel *lblUserAgentName;
@property (nonatomic, weak) IBOutlet UILabel *lblUserAgentDesignation;

@end
