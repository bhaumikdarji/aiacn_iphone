//
//  pageControlCell.m
//  AIAChina
//
//  Created by Purva on 28/01/16.
//  Copyright © 2016 AIA. All rights reserved.
//

#import "pageControlCell.h"

@implementation pageControlCell

- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        //label
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 24)];
        self.label.textColor  = [UIColor whiteColor];
        self.label.backgroundColor = [UIColor whiteColor];
        self.label.text = @"test";
        [self addSubview:self.label];
        //image view
        self.imagev = [[UIImageView alloc] initWithFrame:self.contentView.frame];
        //left and right button
        self.buttonMoveLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        self.buttonMoveLeft.frame = CGRectMake(0, self.frame.size.height/2, 30, 30);
        [self.buttonMoveLeft setBackgroundImage:[UIImage imageNamed:@"Slideshow-LeftArrow"] forState:UIControlStateNormal];

        self.buttonMoveRight = [UIButton buttonWithType:UIButtonTypeCustom];
        self.buttonMoveRight.frame = CGRectMake(self.frame.size.width-30, self.frame.size.height/2, 30, 30);
        [self.buttonMoveRight setBackgroundImage:[UIImage imageNamed:@"Slideshow-RightArrow"] forState:UIControlStateNormal];
        
        [self addSubview:self.imagev];
        [self addSubview:self.buttonMoveLeft];
        [self addSubview:self.buttonMoveRight];
    }
    return self;
}
@end
