//
//  pageControlCell.h
//  AIAChina
//
//  Created by Purva on 28/01/16.
//  Copyright © 2016 AIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pageControlCell : UICollectionViewCell

@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UIImageView *imagev;
@property (strong, nonatomic) UIButton *buttonMoveLeft,*buttonMoveRight;

@end
