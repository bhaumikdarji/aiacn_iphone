//
//  MenuViewController.h
//  AIAChina

/*
 -----------------------------------------------------------------------------------------------------------------------------------------
 Copyright (c) 2015 Quix Creation Pte. Ltd. All Rights Reserved. This SOURCE CODE FILE, which has been provided by Quix as part of a 
 Quix Creations product for use ONLY by licensed users of the product, includes CONFIDENTIAL and PROPRIETARY information of Quix Creations.

 USE OF THIS SOFTWARE IS GOVERNED BY THE TERMS AND CONDITIONS OF THE LICENSE STATEMENT AND LIMITED WARRANTY FURNISHED WITH THE PRODUCT.
 -----------------------------------------------------------------------------------------------------------------------------------------
*/


#import <UIKit/UIKit.h>

@protocol MenuViewControllerDelegate
@optional
@required
/*
 * menuItemSelected
 * params : menu - the menu's model
 * description: delegate method sending the notification when a menu item is selected.
 */

- (void)menuItemSelected:(int)index;
@optional
-(void)hideView
;
@end


@interface MenuViewController : UIViewController


@property (nonatomic,weak) id<MenuViewControllerDelegate> delegate;


@property (nonatomic, strong) NSMutableArray *arrayOfSectionItems;
@property (nonatomic, strong) NSMutableArray *arrayOfSubsectionItems;
@property (nonatomic, strong) NSMutableArray *twoDimSubSection;
@property (nonatomic) NSArray *tableData;
@property (nonatomic) NSArray *optionIconData;


@property (nonatomic,strong) NSString *testURL;
@property (nonatomic) CGRect menuTableFrame;
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;

- (void)hideTableViewMenu;
- (void)showTableViewMenu;
- (void)generateMenuData;
- (void)generateMenuModel;

@end
