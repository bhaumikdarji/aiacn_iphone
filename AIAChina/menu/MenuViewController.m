//
//  MenuViewController.m
//  AIAChina

/*
 -----------------------------------------------------------------------------------------------------------------------------------------
 Copyright (c) 2015 Quix Creation Pte. Ltd. All Rights Reserved. This SOURCE CODE FILE, which has been provided by Quix as part of a 
 Quix Creations product for use ONLY by licensed users of the product, includes CONFIDENTIAL and PROPRIETARY information of Quix Creations.

 USE OF THIS SOFTWARE IS GOVERNED BY THE TERMS AND CONDITIONS OF THE LICENSE STATEMENT AND LIMITED WARRANTY FURNISHED WITH THE PRODUCT.
 -----------------------------------------------------------------------------------------------------------------------------------------
*/


#import "MenuViewController.h"
#import "AppDelegate.h"
#import "MenuTableViewCell.h"

#define CELL_FONT(s)     [UIFont fontWithName:@"AauxProLight" size:s]
#define CELL_MEDIUMFONT(s) [UIFont fontWithName:@"AauxProMedium" size:s]
#define tableWidth [[UIScreen mainScreen] bounds].size.width/4

@interface MenuViewController () <UIAlertViewDelegate>
@property (nonatomic) NSArray *tableImages;
@property (nonatomic,retain) AppDelegate *appDelegate;

//Table data contains the array for each table sections
@property (nonatomic) NSArray *tableSections;

// Table Header view

@property (nonatomic) UIView *tableHeaderView;

@end

@implementation MenuViewController

//----------------------------------------------------------------------------------------

#pragma mark - Init Method

//----------------------------------------------------------------------------------------

static NSString *cellIdentifier = @"MyCellIdentifier";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//----------------------------------------------------------------------------------------

#pragma mark - Custom Method

//----------------------------------------------------------------------------------------

- (void)generateMenuData{
//    self.tableSections
    
    self.tableData = @[NSLocalizedString(@"menuHome", @""),
                       NSLocalizedString(@"menuiPresenter", @""),
                       NSLocalizedString(@"menuMyContacts", @""),
                       NSLocalizedString(@"menuMyCalendar", @""),
                       NSLocalizedString(@"menuResourceCenter", @""),
                       NSLocalizedString(@"menuGoalSetting", @""),
                       NSLocalizedString(@"menuReports", @""),
                       NSLocalizedString(@"menuMyProfile", @""),
                       NSLocalizedString(@"menuSettings", @""),
                       ];
    
    [self.menuTableView reloadData];
}

//----------------------------------------------------------------------------------------
//Hide the side panel
- (void) hideTableViewMenu {
    self.menuTableView.hidden = YES;
}

//----------------------------------------------------------------------------------------
//show the side panel
- (void) showTableViewMenu{
    self.menuTableView.hidden = NO;
}

//----------------------------------------------------------------------------------------

#pragma mark - Memory Management Method

//----------------------------------------------------------------------------------------

- (void)dealloc {
    
}

//----------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//----------------------------------------------------------------------------------------

#pragma mark - UIAlertView Delegate Methods

//----------------------------------------------------------------------------------------

#pragma mark - Custom Method

//----------------------------------------------------------------------------------------
-(void)hideMe:(id)sender{
    [self.delegate hideView];
}

//----------------------------------------------------------------------------------------

#pragma mark - UITableview DataSource Methods

//----------------------------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
//    NSLog(@"the number of sctions are %lu",(unsigned long)self.tableSections.count);
//    return [self.tableSections count];
}

//----------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableData count];
}

//----------------------------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuTableViewCell *cell = (MenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    CGRect frame;
    cell.menuLabel.text = self.tableData[indexPath.row];
    frame = cell.menuLabel.frame;
    frame.size.height = cell.frame.size.height;
    [cell.menuLabel setFrame:frame];
    
    [cell.optionIcon setImage:[UIImage imageNamed:self.optionIconData[indexPath.row]]];
    frame = cell.optionIcon.frame;
    frame.origin.y = ((cell.frame.size.height)/2 - cell.optionIcon.frame.size.height/2);
    [cell.optionIcon setFrame:frame];
    
    cell.backgroundColor = [UIColor colorWithRed:48.0/255.0 green:47.0/255.0 blue:45.0/255.0 alpha:1.0];
    [cell.menuLabel setTextColor:[UIColor whiteColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (indexPath.row == 6) {
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 45, cell.bounds.size.width, 2)];
        line.backgroundColor = [UIColor colorWithRed:59.0/255.0 green:58.0/255.0 blue:56.0/255.0 alpha:1.0];
        [cell addSubview:line];
    }
    return cell;
}

//----------------------------------------------------------------------------------------

#pragma mark - UITableview Delegate Methods

//----------------------------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 6)
        return 60;
    else
        return 50;
}

//----------------------------------------------------------------------------------------

- (void)createTableHeaderView {
    for (UIView *view in self.tableHeaderView.subviews) {
        [view removeFromSuperview];
    }

    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(tableWidth - 55, 30, 50, 15);
    [closeButton setImage:[UIImage imageNamed:@"Menu-Close.png"] forState:UIControlStateNormal];
    [closeButton setImageEdgeInsets:UIEdgeInsetsMake(1, 19, 1, 19)];
    [closeButton addTarget:self action:@selector(closeMenu) forControlEvents:UIControlEventTouchUpInside];

    self.tableHeaderView.backgroundColor = [UIColor colorWithRed:48.0/255.0 green:47.0/255.0 blue:45.0/255.0 alpha:1.0];
    [self.tableHeaderView addSubview:closeButton];
    self.menuTableView.tableHeaderView = self.tableHeaderView;
    self.menuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

//----------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate menuItemSelected:(int)indexPath.row];
}

//----------------------------------------------------------------------------------------

- (void)closeMenu{
    [self hideMe:self];
}

//----------------------------------------------------------------------------------------

#pragma mark - Life Cycle Methods

//----------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    /*
    [self.view setFrame:CGRectMake(0, 0,-1*ceil([[UIScreen mainScreen] bounds].size.width / 3), [[UIScreen mainScreen] bounds].size.height)];
    UISwipeGestureRecognizer *gesture=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(hideMe:)];
    [gesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:gesture];*/
    
    CGRect tableFrame = self.menuTableView.frame;
    tableFrame.size.height = [[UIScreen mainScreen] bounds].size.height-20;
    tableFrame.size.width = ceil(tableWidth);
    self.menuTableView.frame = tableFrame;
    self.menuTableFrame = tableFrame;
    
    self.menuTableView.backgroundColor = [UIColor colorWithRed:48.0/255.0 green:47.0/255.0 blue:45.0/255.0 alpha:1.0];
    [self.menuTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ceil(tableWidth), 50)];
    self.tableHeaderView.backgroundColor = [UIColor colorWithRed:48.0/255.0 green:47.0/255.0 blue:45.0/255.0 alpha:1.0];
    UINib *cellNib = [UINib nibWithNibName:@"MenuTableViewCell" bundle:nil];
    [self.menuTableView registerNib:cellNib forCellReuseIdentifier:cellIdentifier];
        
    [self generateMenuData];
    [self createTableHeaderView];
    self.optionIconData = [NSArray arrayWithObjects:
                           @"Menu-Home.png",
                           @"Menu-iPresenter.png",
                           @"Menu-MyContacts.png",
                           @"Menu-Calendar.png",
                           @"Menu-Resource.png",
                           @"Menu-GoalSetting.png",
                           @"Menu-Reports.png",
                           @"Menu-Profile.png",
                           @"Menu-Settings.png", nil];

}

//----------------------------------------------------------------------------------------

@end
