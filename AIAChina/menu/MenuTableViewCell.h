//
//  MenuTableViewCell.h
//  AIAChina

/*
 -----------------------------------------------------------------------------------------------------------------------------------------
 Copyright (c) 2015 Quix Creation Pte. Ltd. All Rights Reserved. This SOURCE CODE FILE, which has been provided by Quix as part of a
 Quix Creations product for use ONLY by licensed users of the product, includes CONFIDENTIAL and PROPRIETARY information of Quix Creations.
 
 USE OF THIS SOFTWARE IS GOVERNED BY THE TERMS AND CONDITIONS OF THE LICENSE STATEMENT AND LIMITED WARRANTY FURNISHED WITH THE PRODUCT.
 -----------------------------------------------------------------------------------------------------------------------------------------
 */

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *optionIcon;
@property (nonatomic, strong) IBOutlet UILabel *menuLabel;
@end
