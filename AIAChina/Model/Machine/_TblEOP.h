// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblEOP.h instead.

#import <CoreData/CoreData.h>

extern const struct TblEOPAttributes {
	__unsafe_unretained NSString *agentTeam;
	__unsafe_unretained NSString *attendeeCount;
	__unsafe_unretained NSString *buCode;
	__unsafe_unretained NSString *buName;
	__unsafe_unretained NSString *cityCode;
	__unsafe_unretained NSString *completedCount;
	__unsafe_unretained NSString *createdBy;
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *district;
	__unsafe_unretained NSString *endTime;
	__unsafe_unretained NSString *eopDescription;
	__unsafe_unretained NSString *estimatedCandidates;
	__unsafe_unretained NSString *eventCode;
	__unsafe_unretained NSString *eventDate;
	__unsafe_unretained NSString *eventDateStr;
	__unsafe_unretained NSString *eventName;
	__unsafe_unretained NSString *eventType;
	__unsafe_unretained NSString *isRegistered;
	__unsafe_unretained NSString *location;
	__unsafe_unretained NSString *modifiedBy;
	__unsafe_unretained NSString *openTo;
	__unsafe_unretained NSString *openToRegistration;
	__unsafe_unretained NSString *openToSpGrp;
	__unsafe_unretained NSString *oraganiserStr;
	__unsafe_unretained NSString *organizer;
	__unsafe_unretained NSString *profilePath;
	__unsafe_unretained NSString *publicUrl;
	__unsafe_unretained NSString *registeredCount;
	__unsafe_unretained NSString *speaker;
	__unsafe_unretained NSString *sscCode;
	__unsafe_unretained NSString *startTime;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *timeEnd;
	__unsafe_unretained NSString *timeStart;
	__unsafe_unretained NSString *token;
	__unsafe_unretained NSString *topic;
	__unsafe_unretained NSString *topicPath;
	__unsafe_unretained NSString *userName;
} TblEOPAttributes;

extern const struct TblEOPRelationships {
	__unsafe_unretained NSString *calendarEvent;
	__unsafe_unretained NSString *eopCandidate;
} TblEOPRelationships;

@class TblCalendarEvent;
@class TblEOPCandidate;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblEOPID : NSManagedObjectID {}
@end

@interface _TblEOP : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblEOPID* objectID;

@property (nonatomic, strong) id agentTeam;

//- (BOOL)validateAgentTeam:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* attendeeCount;

@property (atomic) int64_t attendeeCountValue;
- (int64_t)attendeeCountValue;
- (void)setAttendeeCountValue:(int64_t)value_;

//- (BOOL)validateAttendeeCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* buCode;

@property (atomic) int64_t buCodeValue;
- (int64_t)buCodeValue;
- (void)setBuCodeValue:(int64_t)value_;

//- (BOOL)validateBuCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id buName;

//- (BOOL)validateBuName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* cityCode;

//- (BOOL)validateCityCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* completedCount;

@property (atomic) int64_t completedCountValue;
- (int64_t)completedCountValue;
- (void)setCompletedCountValue:(int64_t)value_;

//- (BOOL)validateCompletedCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id createdBy;

//- (BOOL)validateCreatedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* district;

@property (atomic) int64_t districtValue;
- (int64_t)districtValue;
- (void)setDistrictValue:(int64_t)value_;

//- (BOOL)validateDistrict:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id endTime;

//- (BOOL)validateEndTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id eopDescription;

//- (BOOL)validateEopDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* estimatedCandidates;

@property (atomic) int64_t estimatedCandidatesValue;
- (int64_t)estimatedCandidatesValue;
- (void)setEstimatedCandidatesValue:(int64_t)value_;

//- (BOOL)validateEstimatedCandidates:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* eventCode;

@property (atomic) int64_t eventCodeValue;
- (int64_t)eventCodeValue;
- (void)setEventCodeValue:(int64_t)value_;

//- (BOOL)validateEventCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* eventDate;

//- (BOOL)validateEventDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id eventDateStr;

//- (BOOL)validateEventDateStr:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id eventName;

//- (BOOL)validateEventName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id eventType;

//- (BOOL)validateEventType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRegistered;

@property (atomic) BOOL isRegisteredValue;
- (BOOL)isRegisteredValue;
- (void)setIsRegisteredValue:(BOOL)value_;

//- (BOOL)validateIsRegistered:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id location;

//- (BOOL)validateLocation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id modifiedBy;

//- (BOOL)validateModifiedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id openTo;

//- (BOOL)validateOpenTo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id openToRegistration;

//- (BOOL)validateOpenToRegistration:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id openToSpGrp;

//- (BOOL)validateOpenToSpGrp:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id oraganiserStr;

//- (BOOL)validateOraganiserStr:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* organizer;

@property (atomic) int64_t organizerValue;
- (int64_t)organizerValue;
- (void)setOrganizerValue:(int64_t)value_;

//- (BOOL)validateOrganizer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id profilePath;

//- (BOOL)validateProfilePath:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id publicUrl;

//- (BOOL)validatePublicUrl:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* registeredCount;

@property (atomic) int64_t registeredCountValue;
- (int64_t)registeredCountValue;
- (void)setRegisteredCountValue:(int64_t)value_;

//- (BOOL)validateRegisteredCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id speaker;

//- (BOOL)validateSpeaker:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sscCode;

//- (BOOL)validateSscCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id startTime;

//- (BOOL)validateStartTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) BOOL statusValue;
- (BOOL)statusValue;
- (void)setStatusValue:(BOOL)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id timeEnd;

//- (BOOL)validateTimeEnd:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id timeStart;

//- (BOOL)validateTimeStart:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id token;

//- (BOOL)validateToken:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id topic;

//- (BOOL)validateTopic:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* topicPath;

//- (BOOL)validateTopicPath:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id userName;

//- (BOOL)validateUserName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblCalendarEvent *calendarEvent;

//- (BOOL)validateCalendarEvent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *eopCandidate;

- (NSMutableSet*)eopCandidateSet;

@end

@interface _TblEOP (EopCandidateCoreDataGeneratedAccessors)
- (void)addEopCandidate:(NSSet*)value_;
- (void)removeEopCandidate:(NSSet*)value_;
- (void)addEopCandidateObject:(TblEOPCandidate*)value_;
- (void)removeEopCandidateObject:(TblEOPCandidate*)value_;

@end

@interface _TblEOP (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveAgentTeam;
- (void)setPrimitiveAgentTeam:(id)value;

- (NSNumber*)primitiveAttendeeCount;
- (void)setPrimitiveAttendeeCount:(NSNumber*)value;

- (int64_t)primitiveAttendeeCountValue;
- (void)setPrimitiveAttendeeCountValue:(int64_t)value_;

- (NSNumber*)primitiveBuCode;
- (void)setPrimitiveBuCode:(NSNumber*)value;

- (int64_t)primitiveBuCodeValue;
- (void)setPrimitiveBuCodeValue:(int64_t)value_;

- (id)primitiveBuName;
- (void)setPrimitiveBuName:(id)value;

- (NSString*)primitiveCityCode;
- (void)setPrimitiveCityCode:(NSString*)value;

- (NSNumber*)primitiveCompletedCount;
- (void)setPrimitiveCompletedCount:(NSNumber*)value;

- (int64_t)primitiveCompletedCountValue;
- (void)setPrimitiveCompletedCountValue:(int64_t)value_;

- (id)primitiveCreatedBy;
- (void)setPrimitiveCreatedBy:(id)value;

- (id)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(id)value;

- (NSNumber*)primitiveDistrict;
- (void)setPrimitiveDistrict:(NSNumber*)value;

- (int64_t)primitiveDistrictValue;
- (void)setPrimitiveDistrictValue:(int64_t)value_;

- (id)primitiveEndTime;
- (void)setPrimitiveEndTime:(id)value;

- (id)primitiveEopDescription;
- (void)setPrimitiveEopDescription:(id)value;

- (NSNumber*)primitiveEstimatedCandidates;
- (void)setPrimitiveEstimatedCandidates:(NSNumber*)value;

- (int64_t)primitiveEstimatedCandidatesValue;
- (void)setPrimitiveEstimatedCandidatesValue:(int64_t)value_;

- (NSNumber*)primitiveEventCode;
- (void)setPrimitiveEventCode:(NSNumber*)value;

- (int64_t)primitiveEventCodeValue;
- (void)setPrimitiveEventCodeValue:(int64_t)value_;

- (NSDate*)primitiveEventDate;
- (void)setPrimitiveEventDate:(NSDate*)value;

- (id)primitiveEventDateStr;
- (void)setPrimitiveEventDateStr:(id)value;

- (id)primitiveEventName;
- (void)setPrimitiveEventName:(id)value;

- (id)primitiveEventType;
- (void)setPrimitiveEventType:(id)value;

- (NSNumber*)primitiveIsRegistered;
- (void)setPrimitiveIsRegistered:(NSNumber*)value;

- (BOOL)primitiveIsRegisteredValue;
- (void)setPrimitiveIsRegisteredValue:(BOOL)value_;

- (id)primitiveLocation;
- (void)setPrimitiveLocation:(id)value;

- (id)primitiveModifiedBy;
- (void)setPrimitiveModifiedBy:(id)value;

- (id)primitiveOpenTo;
- (void)setPrimitiveOpenTo:(id)value;

- (id)primitiveOpenToRegistration;
- (void)setPrimitiveOpenToRegistration:(id)value;

- (id)primitiveOpenToSpGrp;
- (void)setPrimitiveOpenToSpGrp:(id)value;

- (id)primitiveOraganiserStr;
- (void)setPrimitiveOraganiserStr:(id)value;

- (NSNumber*)primitiveOrganizer;
- (void)setPrimitiveOrganizer:(NSNumber*)value;

- (int64_t)primitiveOrganizerValue;
- (void)setPrimitiveOrganizerValue:(int64_t)value_;

- (id)primitiveProfilePath;
- (void)setPrimitiveProfilePath:(id)value;

- (id)primitivePublicUrl;
- (void)setPrimitivePublicUrl:(id)value;

- (NSNumber*)primitiveRegisteredCount;
- (void)setPrimitiveRegisteredCount:(NSNumber*)value;

- (int64_t)primitiveRegisteredCountValue;
- (void)setPrimitiveRegisteredCountValue:(int64_t)value_;

- (id)primitiveSpeaker;
- (void)setPrimitiveSpeaker:(id)value;

- (NSString*)primitiveSscCode;
- (void)setPrimitiveSscCode:(NSString*)value;

- (id)primitiveStartTime;
- (void)setPrimitiveStartTime:(id)value;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (BOOL)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(BOOL)value_;

- (id)primitiveTimeEnd;
- (void)setPrimitiveTimeEnd:(id)value;

- (id)primitiveTimeStart;
- (void)setPrimitiveTimeStart:(id)value;

- (id)primitiveToken;
- (void)setPrimitiveToken:(id)value;

- (id)primitiveTopic;
- (void)setPrimitiveTopic:(id)value;

- (NSString*)primitiveTopicPath;
- (void)setPrimitiveTopicPath:(NSString*)value;

- (id)primitiveUserName;
- (void)setPrimitiveUserName:(id)value;

- (TblCalendarEvent*)primitiveCalendarEvent;
- (void)setPrimitiveCalendarEvent:(TblCalendarEvent*)value;

- (NSMutableSet*)primitiveEopCandidate;
- (void)setPrimitiveEopCandidate:(NSMutableSet*)value;

@end
