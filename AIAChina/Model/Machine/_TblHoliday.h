// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblHoliday.h instead.

#import <CoreData/CoreData.h>

extern const struct TblHolidayAttributes {
	__unsafe_unretained NSString *buCode;
	__unsafe_unretained NSString *buName;
	__unsafe_unretained NSString *cityCode;
	__unsafe_unretained NSString *createdBy;
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *district;
	__unsafe_unretained NSString *endDate;
	__unsafe_unretained NSString *holidayCode;
	__unsafe_unretained NSString *holidayName;
	__unsafe_unretained NSString *iconSelection;
	__unsafe_unretained NSString *modificationDate;
	__unsafe_unretained NSString *modifiedBy;
	__unsafe_unretained NSString *postedBy;
	__unsafe_unretained NSString *sscCode;
	__unsafe_unretained NSString *startDate;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *token;
} TblHolidayAttributes;

extern const struct TblHolidayRelationships {
	__unsafe_unretained NSString *calendarEvent;
} TblHolidayRelationships;

@class TblCalendarEvent;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblHolidayID : NSManagedObjectID {}
@end

@interface _TblHoliday : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblHolidayID* objectID;

@property (nonatomic, strong) NSNumber* buCode;

@property (atomic) int64_t buCodeValue;
- (int64_t)buCodeValue;
- (void)setBuCodeValue:(int64_t)value_;

//- (BOOL)validateBuCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id buName;

//- (BOOL)validateBuName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* cityCode;

//- (BOOL)validateCityCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* createdBy;

@property (atomic) int64_t createdByValue;
- (int64_t)createdByValue;
- (void)setCreatedByValue:(int64_t)value_;

//- (BOOL)validateCreatedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* district;

@property (atomic) int64_t districtValue;
- (int64_t)districtValue;
- (void)setDistrictValue:(int64_t)value_;

//- (BOOL)validateDistrict:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id endDate;

//- (BOOL)validateEndDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* holidayCode;

@property (atomic) int64_t holidayCodeValue;
- (int64_t)holidayCodeValue;
- (void)setHolidayCodeValue:(int64_t)value_;

//- (BOOL)validateHolidayCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id holidayName;

//- (BOOL)validateHolidayName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id iconSelection;

//- (BOOL)validateIconSelection:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id modificationDate;

//- (BOOL)validateModificationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id modifiedBy;

//- (BOOL)validateModifiedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id postedBy;

//- (BOOL)validatePostedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sscCode;

//- (BOOL)validateSscCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id startDate;

//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) BOOL statusValue;
- (BOOL)statusValue;
- (void)setStatusValue:(BOOL)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id token;

//- (BOOL)validateToken:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblCalendarEvent *calendarEvent;

//- (BOOL)validateCalendarEvent:(id*)value_ error:(NSError**)error_;

@end

@interface _TblHoliday (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveBuCode;
- (void)setPrimitiveBuCode:(NSNumber*)value;

- (int64_t)primitiveBuCodeValue;
- (void)setPrimitiveBuCodeValue:(int64_t)value_;

- (id)primitiveBuName;
- (void)setPrimitiveBuName:(id)value;

- (NSString*)primitiveCityCode;
- (void)setPrimitiveCityCode:(NSString*)value;

- (NSNumber*)primitiveCreatedBy;
- (void)setPrimitiveCreatedBy:(NSNumber*)value;

- (int64_t)primitiveCreatedByValue;
- (void)setPrimitiveCreatedByValue:(int64_t)value_;

- (id)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(id)value;

- (NSNumber*)primitiveDistrict;
- (void)setPrimitiveDistrict:(NSNumber*)value;

- (int64_t)primitiveDistrictValue;
- (void)setPrimitiveDistrictValue:(int64_t)value_;

- (id)primitiveEndDate;
- (void)setPrimitiveEndDate:(id)value;

- (NSNumber*)primitiveHolidayCode;
- (void)setPrimitiveHolidayCode:(NSNumber*)value;

- (int64_t)primitiveHolidayCodeValue;
- (void)setPrimitiveHolidayCodeValue:(int64_t)value_;

- (id)primitiveHolidayName;
- (void)setPrimitiveHolidayName:(id)value;

- (id)primitiveIconSelection;
- (void)setPrimitiveIconSelection:(id)value;

- (id)primitiveModificationDate;
- (void)setPrimitiveModificationDate:(id)value;

- (id)primitiveModifiedBy;
- (void)setPrimitiveModifiedBy:(id)value;

- (id)primitivePostedBy;
- (void)setPrimitivePostedBy:(id)value;

- (NSString*)primitiveSscCode;
- (void)setPrimitiveSscCode:(NSString*)value;

- (id)primitiveStartDate;
- (void)setPrimitiveStartDate:(id)value;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (BOOL)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(BOOL)value_;

- (id)primitiveToken;
- (void)setPrimitiveToken:(id)value;

- (TblCalendarEvent*)primitiveCalendarEvent;
- (void)setPrimitiveCalendarEvent:(TblCalendarEvent*)value;

@end
