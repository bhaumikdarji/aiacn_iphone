// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblEOP.m instead.

#import "_TblEOP.h"

const struct TblEOPAttributes TblEOPAttributes = {
	.agentTeam = @"agentTeam",
	.attendeeCount = @"attendeeCount",
	.buCode = @"buCode",
	.buName = @"buName",
	.cityCode = @"cityCode",
	.completedCount = @"completedCount",
	.createdBy = @"createdBy",
	.creationDate = @"creationDate",
	.district = @"district",
	.endTime = @"endTime",
	.eopDescription = @"eopDescription",
	.estimatedCandidates = @"estimatedCandidates",
	.eventCode = @"eventCode",
	.eventDate = @"eventDate",
	.eventDateStr = @"eventDateStr",
	.eventName = @"eventName",
	.eventType = @"eventType",
	.isRegistered = @"isRegistered",
	.location = @"location",
	.modifiedBy = @"modifiedBy",
	.openTo = @"openTo",
	.openToRegistration = @"openToRegistration",
	.openToSpGrp = @"openToSpGrp",
	.oraganiserStr = @"oraganiserStr",
	.organizer = @"organizer",
	.profilePath = @"profilePath",
	.publicUrl = @"publicUrl",
	.registeredCount = @"registeredCount",
	.speaker = @"speaker",
	.sscCode = @"sscCode",
	.startTime = @"startTime",
	.status = @"status",
	.timeEnd = @"timeEnd",
	.timeStart = @"timeStart",
	.token = @"token",
	.topic = @"topic",
	.topicPath = @"topicPath",
	.userName = @"userName",
};

const struct TblEOPRelationships TblEOPRelationships = {
	.calendarEvent = @"calendarEvent",
	.eopCandidate = @"eopCandidate",
};

@implementation TblEOPID
@end

@implementation _TblEOP

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblEOP" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblEOP";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblEOP" inManagedObjectContext:moc_];
}

- (TblEOPID*)objectID {
	return (TblEOPID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"attendeeCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"attendeeCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"buCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"buCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"completedCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"completedCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"districtValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"district"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"estimatedCandidatesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"estimatedCandidates"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"eventCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"eventCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isRegisteredValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRegistered"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"organizerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"organizer"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"registeredCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"registeredCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic agentTeam;

@dynamic attendeeCount;

- (int64_t)attendeeCountValue {
	NSNumber *result = [self attendeeCount];
	return [result longLongValue];
}

- (void)setAttendeeCountValue:(int64_t)value_ {
	[self setAttendeeCount:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveAttendeeCountValue {
	NSNumber *result = [self primitiveAttendeeCount];
	return [result longLongValue];
}

- (void)setPrimitiveAttendeeCountValue:(int64_t)value_ {
	[self setPrimitiveAttendeeCount:[NSNumber numberWithLongLong:value_]];
}

@dynamic buCode;

- (int64_t)buCodeValue {
	NSNumber *result = [self buCode];
	return [result longLongValue];
}

- (void)setBuCodeValue:(int64_t)value_ {
	[self setBuCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBuCodeValue {
	NSNumber *result = [self primitiveBuCode];
	return [result longLongValue];
}

- (void)setPrimitiveBuCodeValue:(int64_t)value_ {
	[self setPrimitiveBuCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic buName;

@dynamic cityCode;

@dynamic completedCount;

- (int64_t)completedCountValue {
	NSNumber *result = [self completedCount];
	return [result longLongValue];
}

- (void)setCompletedCountValue:(int64_t)value_ {
	[self setCompletedCount:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveCompletedCountValue {
	NSNumber *result = [self primitiveCompletedCount];
	return [result longLongValue];
}

- (void)setPrimitiveCompletedCountValue:(int64_t)value_ {
	[self setPrimitiveCompletedCount:[NSNumber numberWithLongLong:value_]];
}

@dynamic createdBy;

@dynamic creationDate;

@dynamic district;

- (int64_t)districtValue {
	NSNumber *result = [self district];
	return [result longLongValue];
}

- (void)setDistrictValue:(int64_t)value_ {
	[self setDistrict:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveDistrictValue {
	NSNumber *result = [self primitiveDistrict];
	return [result longLongValue];
}

- (void)setPrimitiveDistrictValue:(int64_t)value_ {
	[self setPrimitiveDistrict:[NSNumber numberWithLongLong:value_]];
}

@dynamic endTime;

@dynamic eopDescription;

@dynamic estimatedCandidates;

- (int64_t)estimatedCandidatesValue {
	NSNumber *result = [self estimatedCandidates];
	return [result longLongValue];
}

- (void)setEstimatedCandidatesValue:(int64_t)value_ {
	[self setEstimatedCandidates:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveEstimatedCandidatesValue {
	NSNumber *result = [self primitiveEstimatedCandidates];
	return [result longLongValue];
}

- (void)setPrimitiveEstimatedCandidatesValue:(int64_t)value_ {
	[self setPrimitiveEstimatedCandidates:[NSNumber numberWithLongLong:value_]];
}

@dynamic eventCode;

- (int64_t)eventCodeValue {
	NSNumber *result = [self eventCode];
	return [result longLongValue];
}

- (void)setEventCodeValue:(int64_t)value_ {
	[self setEventCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveEventCodeValue {
	NSNumber *result = [self primitiveEventCode];
	return [result longLongValue];
}

- (void)setPrimitiveEventCodeValue:(int64_t)value_ {
	[self setPrimitiveEventCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic eventDate;

@dynamic eventDateStr;

@dynamic eventName;

@dynamic eventType;

@dynamic isRegistered;

- (BOOL)isRegisteredValue {
	NSNumber *result = [self isRegistered];
	return [result boolValue];
}

- (void)setIsRegisteredValue:(BOOL)value_ {
	[self setIsRegistered:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsRegisteredValue {
	NSNumber *result = [self primitiveIsRegistered];
	return [result boolValue];
}

- (void)setPrimitiveIsRegisteredValue:(BOOL)value_ {
	[self setPrimitiveIsRegistered:[NSNumber numberWithBool:value_]];
}

@dynamic location;

@dynamic modifiedBy;

@dynamic openTo;

@dynamic openToRegistration;

@dynamic openToSpGrp;

@dynamic oraganiserStr;

@dynamic organizer;

- (int64_t)organizerValue {
	NSNumber *result = [self organizer];
	return [result longLongValue];
}

- (void)setOrganizerValue:(int64_t)value_ {
	[self setOrganizer:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveOrganizerValue {
	NSNumber *result = [self primitiveOrganizer];
	return [result longLongValue];
}

- (void)setPrimitiveOrganizerValue:(int64_t)value_ {
	[self setPrimitiveOrganizer:[NSNumber numberWithLongLong:value_]];
}

@dynamic profilePath;

@dynamic publicUrl;

@dynamic registeredCount;

- (int64_t)registeredCountValue {
	NSNumber *result = [self registeredCount];
	return [result longLongValue];
}

- (void)setRegisteredCountValue:(int64_t)value_ {
	[self setRegisteredCount:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRegisteredCountValue {
	NSNumber *result = [self primitiveRegisteredCount];
	return [result longLongValue];
}

- (void)setPrimitiveRegisteredCountValue:(int64_t)value_ {
	[self setPrimitiveRegisteredCount:[NSNumber numberWithLongLong:value_]];
}

@dynamic speaker;

@dynamic sscCode;

@dynamic startTime;

@dynamic status;

- (BOOL)statusValue {
	NSNumber *result = [self status];
	return [result boolValue];
}

- (void)setStatusValue:(BOOL)value_ {
	[self setStatus:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result boolValue];
}

- (void)setPrimitiveStatusValue:(BOOL)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithBool:value_]];
}

@dynamic timeEnd;

@dynamic timeStart;

@dynamic token;

@dynamic topic;

@dynamic topicPath;

@dynamic userName;

@dynamic calendarEvent;

@dynamic eopCandidate;

- (NSMutableSet*)eopCandidateSet {
	[self willAccessValueForKey:@"eopCandidate"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"eopCandidate"];

	[self didAccessValueForKey:@"eopCandidate"];
	return result;
}

@end

