// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblContact.m instead.

#import "_TblContact.h"

const struct TblContactAttributes TblContactAttributes = {
	.addressCode = @"addressCode",
	.age = @"age",
	.agentId = @"agentId",
	.attenededEOPCount = @"attenededEOPCount",
	.birthDate = @"birthDate",
	.birthMonthDay = @"birthMonthDay",
	.birthPlace = @"birthPlace",
	.birthYear = @"birthYear",
	.candidatePhoto = @"candidatePhoto",
	.createdBy = @"createdBy",
	.creationDate = @"creationDate",
	.eMailId = @"eMailId",
	.education = @"education",
	.fixedLineNO = @"fixedLineNO",
	.gender = @"gender",
	.groupName = @"groupName",
	.idType = @"idType",
	.identityType = @"identityType",
	.iosAddressCode = @"iosAddressCode",
	.isDelete = @"isDelete",
	.isSync = @"isSync",
	.lastContactedDate = @"lastContactedDate",
	.marritalStatus = @"marritalStatus",
	.mobilePhoneNo = @"mobilePhoneNo",
	.modificationDate = @"modificationDate",
	.modifiedBy = @"modifiedBy",
	.name = @"name",
	.natureOfBusiness = @"natureOfBusiness",
	.newcomerSource = @"newcomerSource",
	.nric = @"nric",
	.officePhoneNo = @"officePhoneNo",
	.outlookApperance = @"outlookApperance",
	.presentWorkCondition = @"presentWorkCondition",
	.presenterReport = @"presenterReport",
	.presenterReportDate = @"presenterReportDate",
	.purchasedAnyInsurance = @"purchasedAnyInsurance",
	.qq = @"qq",
	.race = @"race",
	.reJoin = @"reJoin",
	.recommendedRecruitmentScheme = @"recommendedRecruitmentScheme",
	.recruitedBy = @"recruitedBy",
	.recruitmentProgressStatus = @"recruitmentProgressStatus",
	.referalSource = @"referalSource",
	.registeredAddress = @"registeredAddress",
	.registeredAddress1 = @"registeredAddress1",
	.registeredAddress2 = @"registeredAddress2",
	.registeredPostalCode = @"registeredPostalCode",
	.remarks = @"remarks",
	.residentialAddress = @"residentialAddress",
	.residentialAddress1 = @"residentialAddress1",
	.residentialAddress2 = @"residentialAddress2",
	.residentialPostalCode = @"residentialPostalCode",
	.salesExperience = @"salesExperience",
	.sourceComerNew = @"sourceComerNew",
	.talentProfile = @"talentProfile",
	.weChat = @"weChat",
	.weibo = @"weibo",
	.workingYearExperience = @"workingYearExperience",
	.yearlyIncome = @"yearlyIncome",
};

const struct TblContactRelationships TblContactRelationships = {
	.candidateESignatures = @"candidateESignatures",
	.candidateEducations = @"candidateEducations",
	.candidateFamilyInfos = @"candidateFamilyInfos",
	.candidateNotes = @"candidateNotes",
	.candidateProcess = @"candidateProcess",
	.candidateProfessionalCertifications = @"candidateProfessionalCertifications",
	.candidateWorkExperiences = @"candidateWorkExperiences",
	.group = @"group",
};

@implementation TblContactID
@end

@implementation _TblContact

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblContact" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblContact";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblContact" inManagedObjectContext:moc_];
}

- (TblContactID*)objectID {
	return (TblContactID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"addressCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"addressCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"age"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"attenededEOPCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"attenededEOPCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"birthYearValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"birthYear"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isDeleteValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isDelete"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isSyncValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isSync"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic addressCode;

- (int64_t)addressCodeValue {
	NSNumber *result = [self addressCode];
	return [result longLongValue];
}

- (void)setAddressCodeValue:(int64_t)value_ {
	[self setAddressCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveAddressCodeValue {
	NSNumber *result = [self primitiveAddressCode];
	return [result longLongValue];
}

- (void)setPrimitiveAddressCodeValue:(int64_t)value_ {
	[self setPrimitiveAddressCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic age;

- (int64_t)ageValue {
	NSNumber *result = [self age];
	return [result longLongValue];
}

- (void)setAgeValue:(int64_t)value_ {
	[self setAge:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveAgeValue {
	NSNumber *result = [self primitiveAge];
	return [result longLongValue];
}

- (void)setPrimitiveAgeValue:(int64_t)value_ {
	[self setPrimitiveAge:[NSNumber numberWithLongLong:value_]];
}

@dynamic agentId;

@dynamic attenededEOPCount;

- (int64_t)attenededEOPCountValue {
	NSNumber *result = [self attenededEOPCount];
	return [result longLongValue];
}

- (void)setAttenededEOPCountValue:(int64_t)value_ {
	[self setAttenededEOPCount:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveAttenededEOPCountValue {
	NSNumber *result = [self primitiveAttenededEOPCount];
	return [result longLongValue];
}

- (void)setPrimitiveAttenededEOPCountValue:(int64_t)value_ {
	[self setPrimitiveAttenededEOPCount:[NSNumber numberWithLongLong:value_]];
}

@dynamic birthDate;

@dynamic birthMonthDay;

@dynamic birthPlace;

@dynamic birthYear;

- (int64_t)birthYearValue {
	NSNumber *result = [self birthYear];
	return [result longLongValue];
}

- (void)setBirthYearValue:(int64_t)value_ {
	[self setBirthYear:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBirthYearValue {
	NSNumber *result = [self primitiveBirthYear];
	return [result longLongValue];
}

- (void)setPrimitiveBirthYearValue:(int64_t)value_ {
	[self setPrimitiveBirthYear:[NSNumber numberWithLongLong:value_]];
}

@dynamic candidatePhoto;

@dynamic createdBy;

@dynamic creationDate;

@dynamic eMailId;

@dynamic education;

@dynamic fixedLineNO;

@dynamic gender;

@dynamic groupName;

@dynamic idType;

@dynamic identityType;

@dynamic iosAddressCode;

@dynamic isDelete;

- (BOOL)isDeleteValue {
	NSNumber *result = [self isDelete];
	return [result boolValue];
}

- (void)setIsDeleteValue:(BOOL)value_ {
	[self setIsDelete:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsDeleteValue {
	NSNumber *result = [self primitiveIsDelete];
	return [result boolValue];
}

- (void)setPrimitiveIsDeleteValue:(BOOL)value_ {
	[self setPrimitiveIsDelete:[NSNumber numberWithBool:value_]];
}

@dynamic isSync;

- (BOOL)isSyncValue {
	NSNumber *result = [self isSync];
	return [result boolValue];
}

- (void)setIsSyncValue:(BOOL)value_ {
	[self setIsSync:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsSyncValue {
	NSNumber *result = [self primitiveIsSync];
	return [result boolValue];
}

- (void)setPrimitiveIsSyncValue:(BOOL)value_ {
	[self setPrimitiveIsSync:[NSNumber numberWithBool:value_]];
}

@dynamic lastContactedDate;

@dynamic marritalStatus;

@dynamic mobilePhoneNo;

@dynamic modificationDate;

@dynamic modifiedBy;

@dynamic name;

@dynamic natureOfBusiness;

@dynamic newcomerSource;

@dynamic nric;

@dynamic officePhoneNo;

@dynamic outlookApperance;

@dynamic presentWorkCondition;

@dynamic presenterReport;

@dynamic presenterReportDate;

@dynamic purchasedAnyInsurance;

@dynamic qq;

@dynamic race;

@dynamic reJoin;

@dynamic recommendedRecruitmentScheme;

@dynamic recruitedBy;

@dynamic recruitmentProgressStatus;

@dynamic referalSource;

@dynamic registeredAddress;

@dynamic registeredAddress1;

@dynamic registeredAddress2;

@dynamic registeredPostalCode;

@dynamic remarks;

@dynamic residentialAddress;

@dynamic residentialAddress1;

@dynamic residentialAddress2;

@dynamic residentialPostalCode;

@dynamic salesExperience;

@dynamic sourceComerNew;

@dynamic talentProfile;

@dynamic weChat;

@dynamic weibo;

@dynamic workingYearExperience;

@dynamic yearlyIncome;

@dynamic candidateESignatures;

- (NSMutableSet*)candidateESignaturesSet {
	[self willAccessValueForKey:@"candidateESignatures"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"candidateESignatures"];

	[self didAccessValueForKey:@"candidateESignatures"];
	return result;
}

@dynamic candidateEducations;

- (NSMutableSet*)candidateEducationsSet {
	[self willAccessValueForKey:@"candidateEducations"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"candidateEducations"];

	[self didAccessValueForKey:@"candidateEducations"];
	return result;
}

@dynamic candidateFamilyInfos;

- (NSMutableSet*)candidateFamilyInfosSet {
	[self willAccessValueForKey:@"candidateFamilyInfos"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"candidateFamilyInfos"];

	[self didAccessValueForKey:@"candidateFamilyInfos"];
	return result;
}

@dynamic candidateNotes;

- (NSMutableSet*)candidateNotesSet {
	[self willAccessValueForKey:@"candidateNotes"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"candidateNotes"];

	[self didAccessValueForKey:@"candidateNotes"];
	return result;
}

@dynamic candidateProcess;

@dynamic candidateProfessionalCertifications;

- (NSMutableSet*)candidateProfessionalCertificationsSet {
	[self willAccessValueForKey:@"candidateProfessionalCertifications"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"candidateProfessionalCertifications"];

	[self didAccessValueForKey:@"candidateProfessionalCertifications"];
	return result;
}

@dynamic candidateWorkExperiences;

- (NSMutableSet*)candidateWorkExperiencesSet {
	[self willAccessValueForKey:@"candidateWorkExperiences"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"candidateWorkExperiences"];

	[self didAccessValueForKey:@"candidateWorkExperiences"];
	return result;
}

@dynamic group;

- (NSMutableSet*)groupSet {
	[self willAccessValueForKey:@"group"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"group"];

	[self didAccessValueForKey:@"group"];
	return result;
}

@end

