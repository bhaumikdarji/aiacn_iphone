// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblNotes.h instead.

#import <CoreData/CoreData.h>

extern const struct TblNotesAttributes {
	__unsafe_unretained NSString *activityDate;
	__unsafe_unretained NSString *activityStatus;
	__unsafe_unretained NSString *activityType;
	__unsafe_unretained NSString *desc;
	__unsafe_unretained NSString *eventCode;
	__unsafe_unretained NSString *iosAddressCode;
	__unsafe_unretained NSString *isDelete;
	__unsafe_unretained NSString *isSync;
	__unsafe_unretained NSString *isSystem;
	__unsafe_unretained NSString *recruitmentStep;
} TblNotesAttributes;

extern const struct TblNotesRelationships {
	__unsafe_unretained NSString *contact;
} TblNotesRelationships;

@class TblContact;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblNotesID : NSManagedObjectID {}
@end

@interface _TblNotes : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblNotesID* objectID;

@property (nonatomic, strong) NSDate* activityDate;

//- (BOOL)validateActivityDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* activityStatus;

@property (atomic) BOOL activityStatusValue;
- (BOOL)activityStatusValue;
- (void)setActivityStatusValue:(BOOL)value_;

//- (BOOL)validateActivityStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id activityType;

//- (BOOL)validateActivityType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id desc;

//- (BOOL)validateDesc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* eventCode;

@property (atomic) int64_t eventCodeValue;
- (int64_t)eventCodeValue;
- (void)setEventCodeValue:(int64_t)value_;

//- (BOOL)validateEventCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id iosAddressCode;

//- (BOOL)validateIosAddressCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isDelete;

@property (atomic) BOOL isDeleteValue;
- (BOOL)isDeleteValue;
- (void)setIsDeleteValue:(BOOL)value_;

//- (BOOL)validateIsDelete:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isSync;

@property (atomic) BOOL isSyncValue;
- (BOOL)isSyncValue;
- (void)setIsSyncValue:(BOOL)value_;

//- (BOOL)validateIsSync:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isSystem;

@property (atomic) BOOL isSystemValue;
- (BOOL)isSystemValue;
- (void)setIsSystemValue:(BOOL)value_;

//- (BOOL)validateIsSystem:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* recruitmentStep;

@property (atomic) int64_t recruitmentStepValue;
- (int64_t)recruitmentStepValue;
- (void)setRecruitmentStepValue:(int64_t)value_;

//- (BOOL)validateRecruitmentStep:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblContact *contact;

//- (BOOL)validateContact:(id*)value_ error:(NSError**)error_;

@end

@interface _TblNotes (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveActivityDate;
- (void)setPrimitiveActivityDate:(NSDate*)value;

- (NSNumber*)primitiveActivityStatus;
- (void)setPrimitiveActivityStatus:(NSNumber*)value;

- (BOOL)primitiveActivityStatusValue;
- (void)setPrimitiveActivityStatusValue:(BOOL)value_;

- (id)primitiveActivityType;
- (void)setPrimitiveActivityType:(id)value;

- (id)primitiveDesc;
- (void)setPrimitiveDesc:(id)value;

- (NSNumber*)primitiveEventCode;
- (void)setPrimitiveEventCode:(NSNumber*)value;

- (int64_t)primitiveEventCodeValue;
- (void)setPrimitiveEventCodeValue:(int64_t)value_;

- (id)primitiveIosAddressCode;
- (void)setPrimitiveIosAddressCode:(id)value;

- (NSNumber*)primitiveIsDelete;
- (void)setPrimitiveIsDelete:(NSNumber*)value;

- (BOOL)primitiveIsDeleteValue;
- (void)setPrimitiveIsDeleteValue:(BOOL)value_;

- (NSNumber*)primitiveIsSync;
- (void)setPrimitiveIsSync:(NSNumber*)value;

- (BOOL)primitiveIsSyncValue;
- (void)setPrimitiveIsSyncValue:(BOOL)value_;

- (NSNumber*)primitiveIsSystem;
- (void)setPrimitiveIsSystem:(NSNumber*)value;

- (BOOL)primitiveIsSystemValue;
- (void)setPrimitiveIsSystemValue:(BOOL)value_;

- (NSNumber*)primitiveRecruitmentStep;
- (void)setPrimitiveRecruitmentStep:(NSNumber*)value;

- (int64_t)primitiveRecruitmentStepValue;
- (void)setPrimitiveRecruitmentStepValue:(int64_t)value_;

- (TblContact*)primitiveContact;
- (void)setPrimitiveContact:(TblContact*)value;

@end
