// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblCCTest.m instead.

#import "_TblCCTest.h"

const struct TblCCTestAttributes TblCCTestAttributes = {
	.assessmentResult = @"assessmentResult",
	.cautionRecruit = @"cautionRecruit",
	.noramalRecruit = @"noramalRecruit",
	.urgentRecruit = @"urgentRecruit",
};

@implementation TblCCTestID
@end

@implementation _TblCCTest

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblCCTest" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblCCTest";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblCCTest" inManagedObjectContext:moc_];
}

- (TblCCTestID*)objectID {
	return (TblCCTestID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cautionRecruitValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cautionRecruit"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"noramalRecruitValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"noramalRecruit"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"urgentRecruitValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"urgentRecruit"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic assessmentResult;

@dynamic cautionRecruit;

- (BOOL)cautionRecruitValue {
	NSNumber *result = [self cautionRecruit];
	return [result boolValue];
}

- (void)setCautionRecruitValue:(BOOL)value_ {
	[self setCautionRecruit:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCautionRecruitValue {
	NSNumber *result = [self primitiveCautionRecruit];
	return [result boolValue];
}

- (void)setPrimitiveCautionRecruitValue:(BOOL)value_ {
	[self setPrimitiveCautionRecruit:[NSNumber numberWithBool:value_]];
}

@dynamic noramalRecruit;

- (BOOL)noramalRecruitValue {
	NSNumber *result = [self noramalRecruit];
	return [result boolValue];
}

- (void)setNoramalRecruitValue:(BOOL)value_ {
	[self setNoramalRecruit:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveNoramalRecruitValue {
	NSNumber *result = [self primitiveNoramalRecruit];
	return [result boolValue];
}

- (void)setPrimitiveNoramalRecruitValue:(BOOL)value_ {
	[self setPrimitiveNoramalRecruit:[NSNumber numberWithBool:value_]];
}

@dynamic urgentRecruit;

- (BOOL)urgentRecruitValue {
	NSNumber *result = [self urgentRecruit];
	return [result boolValue];
}

- (void)setUrgentRecruitValue:(BOOL)value_ {
	[self setUrgentRecruit:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUrgentRecruitValue {
	NSNumber *result = [self primitiveUrgentRecruit];
	return [result boolValue];
}

- (void)setPrimitiveUrgentRecruitValue:(BOOL)value_ {
	[self setPrimitiveUrgentRecruit:[NSNumber numberWithBool:value_]];
}

@end

