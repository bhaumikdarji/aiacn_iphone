// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblCalendarEvent.m instead.

#import "_TblCalendarEvent.h"

const struct TblCalendarEventAttributes TblCalendarEventAttributes = {
	.agentCode = @"agentCode",
	.allDay = @"allDay",
};

const struct TblCalendarEventRelationships TblCalendarEventRelationships = {
	.eopEvents = @"eopEvents",
	.holidayEvents = @"holidayEvents",
	.interviewEvents = @"interviewEvents",
	.trainingEvents = @"trainingEvents",
};

@implementation TblCalendarEventID
@end

@implementation _TblCalendarEvent

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblCalendarEvent" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblCalendarEvent";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblCalendarEvent" inManagedObjectContext:moc_];
}

- (TblCalendarEventID*)objectID {
	return (TblCalendarEventID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"allDayValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"allDay"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic agentCode;

@dynamic allDay;

- (BOOL)allDayValue {
	NSNumber *result = [self allDay];
	return [result boolValue];
}

- (void)setAllDayValue:(BOOL)value_ {
	[self setAllDay:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAllDayValue {
	NSNumber *result = [self primitiveAllDay];
	return [result boolValue];
}

- (void)setPrimitiveAllDayValue:(BOOL)value_ {
	[self setPrimitiveAllDay:[NSNumber numberWithBool:value_]];
}

@dynamic eopEvents;

- (NSMutableSet*)eopEventsSet {
	[self willAccessValueForKey:@"eopEvents"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"eopEvents"];

	[self didAccessValueForKey:@"eopEvents"];
	return result;
}

@dynamic holidayEvents;

- (NSMutableSet*)holidayEventsSet {
	[self willAccessValueForKey:@"holidayEvents"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"holidayEvents"];

	[self didAccessValueForKey:@"holidayEvents"];
	return result;
}

@dynamic interviewEvents;

- (NSMutableSet*)interviewEventsSet {
	[self willAccessValueForKey:@"interviewEvents"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"interviewEvents"];

	[self didAccessValueForKey:@"interviewEvents"];
	return result;
}

@dynamic trainingEvents;

- (NSMutableSet*)trainingEventsSet {
	[self willAccessValueForKey:@"trainingEvents"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"trainingEvents"];

	[self didAccessValueForKey:@"trainingEvents"];
	return result;
}

@end

