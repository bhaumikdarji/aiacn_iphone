// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblEOPCandidate.h instead.

#import <CoreData/CoreData.h>

extern const struct TblEOPCandidateAttributes {
	__unsafe_unretained NSString *age;
	__unsafe_unretained NSString *agencyLeaderCode;
	__unsafe_unretained NSString *agencyLeaderName;
	__unsafe_unretained NSString *agentName;
	__unsafe_unretained NSString *attendedEventCount;
	__unsafe_unretained NSString *attendedStatus;
	__unsafe_unretained NSString *buCode;
	__unsafe_unretained NSString *buName;
	__unsafe_unretained NSString *candidateCode;
	__unsafe_unretained NSString *candidateName;
	__unsafe_unretained NSString *cityCode;
	__unsafe_unretained NSString *completeStatus;
	__unsafe_unretained NSString *contactNumber;
	__unsafe_unretained NSString *distName;
	__unsafe_unretained NSString *districtCode;
	__unsafe_unretained NSString *dob;
	__unsafe_unretained NSString *dobStr;
	__unsafe_unretained NSString *eventCandidateCode;
	__unsafe_unretained NSString *eventCode;
	__unsafe_unretained NSString *gender;
	__unsafe_unretained NSString *servicingAgent;
	__unsafe_unretained NSString *sourceOfReferal;
	__unsafe_unretained NSString *sscCode;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *token;
} TblEOPCandidateAttributes;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblEOPCandidateID : NSManagedObjectID {}
@end

@interface _TblEOPCandidate : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblEOPCandidateID* objectID;

@property (nonatomic, strong) NSNumber* age;

@property (atomic) int64_t ageValue;
- (int64_t)ageValue;
- (void)setAgeValue:(int64_t)value_;

//- (BOOL)validateAge:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agencyLeaderCode;

//- (BOOL)validateAgencyLeaderCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agencyLeaderName;

//- (BOOL)validateAgencyLeaderName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agentName;

//- (BOOL)validateAgentName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* attendedEventCount;

@property (atomic) int64_t attendedEventCountValue;
- (int64_t)attendedEventCountValue;
- (void)setAttendedEventCountValue:(int64_t)value_;

//- (BOOL)validateAttendedEventCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id attendedStatus;

//- (BOOL)validateAttendedStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* buCode;

@property (atomic) int64_t buCodeValue;
- (int64_t)buCodeValue;
- (void)setBuCodeValue:(int64_t)value_;

//- (BOOL)validateBuCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id buName;

//- (BOOL)validateBuName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* candidateCode;

@property (atomic) int64_t candidateCodeValue;
- (int64_t)candidateCodeValue;
- (void)setCandidateCodeValue:(int64_t)value_;

//- (BOOL)validateCandidateCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id candidateName;

//- (BOOL)validateCandidateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* cityCode;

//- (BOOL)validateCityCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id completeStatus;

//- (BOOL)validateCompleteStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id contactNumber;

//- (BOOL)validateContactNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id distName;

//- (BOOL)validateDistName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* districtCode;

@property (atomic) int64_t districtCodeValue;
- (int64_t)districtCodeValue;
- (void)setDistrictCodeValue:(int64_t)value_;

//- (BOOL)validateDistrictCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* dob;

//- (BOOL)validateDob:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id dobStr;

//- (BOOL)validateDobStr:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id eventCandidateCode;

//- (BOOL)validateEventCandidateCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* eventCode;

@property (atomic) int64_t eventCodeValue;
- (int64_t)eventCodeValue;
- (void)setEventCodeValue:(int64_t)value_;

//- (BOOL)validateEventCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id gender;

//- (BOOL)validateGender:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id servicingAgent;

//- (BOOL)validateServicingAgent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id sourceOfReferal;

//- (BOOL)validateSourceOfReferal:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sscCode;

//- (BOOL)validateSscCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) BOOL statusValue;
- (BOOL)statusValue;
- (void)setStatusValue:(BOOL)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id token;

//- (BOOL)validateToken:(id*)value_ error:(NSError**)error_;

@end

@interface _TblEOPCandidate (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAge;
- (void)setPrimitiveAge:(NSNumber*)value;

- (int64_t)primitiveAgeValue;
- (void)setPrimitiveAgeValue:(int64_t)value_;

- (id)primitiveAgencyLeaderCode;
- (void)setPrimitiveAgencyLeaderCode:(id)value;

- (id)primitiveAgencyLeaderName;
- (void)setPrimitiveAgencyLeaderName:(id)value;

- (id)primitiveAgentName;
- (void)setPrimitiveAgentName:(id)value;

- (NSNumber*)primitiveAttendedEventCount;
- (void)setPrimitiveAttendedEventCount:(NSNumber*)value;

- (int64_t)primitiveAttendedEventCountValue;
- (void)setPrimitiveAttendedEventCountValue:(int64_t)value_;

- (id)primitiveAttendedStatus;
- (void)setPrimitiveAttendedStatus:(id)value;

- (NSNumber*)primitiveBuCode;
- (void)setPrimitiveBuCode:(NSNumber*)value;

- (int64_t)primitiveBuCodeValue;
- (void)setPrimitiveBuCodeValue:(int64_t)value_;

- (id)primitiveBuName;
- (void)setPrimitiveBuName:(id)value;

- (NSNumber*)primitiveCandidateCode;
- (void)setPrimitiveCandidateCode:(NSNumber*)value;

- (int64_t)primitiveCandidateCodeValue;
- (void)setPrimitiveCandidateCodeValue:(int64_t)value_;

- (id)primitiveCandidateName;
- (void)setPrimitiveCandidateName:(id)value;

- (NSString*)primitiveCityCode;
- (void)setPrimitiveCityCode:(NSString*)value;

- (id)primitiveCompleteStatus;
- (void)setPrimitiveCompleteStatus:(id)value;

- (id)primitiveContactNumber;
- (void)setPrimitiveContactNumber:(id)value;

- (id)primitiveDistName;
- (void)setPrimitiveDistName:(id)value;

- (NSNumber*)primitiveDistrictCode;
- (void)setPrimitiveDistrictCode:(NSNumber*)value;

- (int64_t)primitiveDistrictCodeValue;
- (void)setPrimitiveDistrictCodeValue:(int64_t)value_;

- (NSDate*)primitiveDob;
- (void)setPrimitiveDob:(NSDate*)value;

- (id)primitiveDobStr;
- (void)setPrimitiveDobStr:(id)value;

- (id)primitiveEventCandidateCode;
- (void)setPrimitiveEventCandidateCode:(id)value;

- (NSNumber*)primitiveEventCode;
- (void)setPrimitiveEventCode:(NSNumber*)value;

- (int64_t)primitiveEventCodeValue;
- (void)setPrimitiveEventCodeValue:(int64_t)value_;

- (id)primitiveGender;
- (void)setPrimitiveGender:(id)value;

- (id)primitiveServicingAgent;
- (void)setPrimitiveServicingAgent:(id)value;

- (id)primitiveSourceOfReferal;
- (void)setPrimitiveSourceOfReferal:(id)value;

- (NSString*)primitiveSscCode;
- (void)setPrimitiveSscCode:(NSString*)value;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (BOOL)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(BOOL)value_;

- (id)primitiveToken;
- (void)setPrimitiveToken:(id)value;

@end
