// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblAward.h instead.

#import <CoreData/CoreData.h>

extern const struct TblAwardAttributes {
	__unsafe_unretained NSString *awardDate;
	__unsafe_unretained NSString *awardDesc;
	__unsafe_unretained NSString *awardImage;
	__unsafe_unretained NSString *awardTitle;
	__unsafe_unretained NSString *isDelete;
} TblAwardAttributes;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblAwardID : NSManagedObjectID {}
@end

@interface _TblAward : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblAwardID* objectID;

@property (nonatomic, strong) NSDate* awardDate;

//- (BOOL)validateAwardDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id awardDesc;

//- (BOOL)validateAwardDesc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id awardImage;

//- (BOOL)validateAwardImage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id awardTitle;

//- (BOOL)validateAwardTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isDelete;

@property (atomic) BOOL isDeleteValue;
- (BOOL)isDeleteValue;
- (void)setIsDeleteValue:(BOOL)value_;

//- (BOOL)validateIsDelete:(id*)value_ error:(NSError**)error_;

@end

@interface _TblAward (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveAwardDate;
- (void)setPrimitiveAwardDate:(NSDate*)value;

- (id)primitiveAwardDesc;
- (void)setPrimitiveAwardDesc:(id)value;

- (id)primitiveAwardImage;
- (void)setPrimitiveAwardImage:(id)value;

- (id)primitiveAwardTitle;
- (void)setPrimitiveAwardTitle:(id)value;

- (NSNumber*)primitiveIsDelete;
- (void)setPrimitiveIsDelete:(NSNumber*)value;

- (BOOL)primitiveIsDeleteValue;
- (void)setPrimitiveIsDeleteValue:(BOOL)value_;

@end
