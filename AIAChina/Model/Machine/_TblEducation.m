// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblEducation.m instead.

#import "_TblEducation.h"

const struct TblEducationAttributes TblEducationAttributes = {
	.education = @"education",
	.educationLevel = @"educationLevel",
	.endDate = @"endDate",
	.iosAddressCode = @"iosAddressCode",
	.school = @"school",
	.startDate = @"startDate",
	.witness = @"witness",
	.witnessContactNo = @"witnessContactNo",
};

const struct TblEducationRelationships TblEducationRelationships = {
	.contact = @"contact",
};

@implementation TblEducationID
@end

@implementation _TblEducation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblEducation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblEducation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblEducation" inManagedObjectContext:moc_];
}

- (TblEducationID*)objectID {
	return (TblEducationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic education;

@dynamic educationLevel;

@dynamic endDate;

@dynamic iosAddressCode;

@dynamic school;

@dynamic startDate;

@dynamic witness;

@dynamic witnessContactNo;

@dynamic contact;

@end

