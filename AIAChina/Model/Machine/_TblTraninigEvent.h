// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblTraninigEvent.h instead.

#import <CoreData/CoreData.h>

extern const struct TblTraninigEventAttributes {
	__unsafe_unretained NSString *courseCode;
	__unsafe_unretained NSString *courseName;
	__unsafe_unretained NSString *courseType;
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *curriculamCode;
	__unsafe_unretained NSString *detailCode;
	__unsafe_unretained NSString *startDate;
} TblTraninigEventAttributes;

extern const struct TblTraninigEventRelationships {
	__unsafe_unretained NSString *calendarEvent;
} TblTraninigEventRelationships;

@class TblCalendarEvent;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblTraninigEventID : NSManagedObjectID {}
@end

@interface _TblTraninigEvent : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblTraninigEventID* objectID;

@property (nonatomic, strong) id courseCode;

//- (BOOL)validateCourseCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id courseName;

//- (BOOL)validateCourseName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id courseType;

//- (BOOL)validateCourseType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id curriculamCode;

//- (BOOL)validateCurriculamCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* detailCode;

@property (atomic) int16_t detailCodeValue;
- (int16_t)detailCodeValue;
- (void)setDetailCodeValue:(int16_t)value_;

//- (BOOL)validateDetailCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* startDate;

//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblCalendarEvent *calendarEvent;

//- (BOOL)validateCalendarEvent:(id*)value_ error:(NSError**)error_;

@end

@interface _TblTraninigEvent (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveCourseCode;
- (void)setPrimitiveCourseCode:(id)value;

- (id)primitiveCourseName;
- (void)setPrimitiveCourseName:(id)value;

- (id)primitiveCourseType;
- (void)setPrimitiveCourseType:(id)value;

- (id)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(id)value;

- (id)primitiveCurriculamCode;
- (void)setPrimitiveCurriculamCode:(id)value;

- (NSNumber*)primitiveDetailCode;
- (void)setPrimitiveDetailCode:(NSNumber*)value;

- (int16_t)primitiveDetailCodeValue;
- (void)setPrimitiveDetailCodeValue:(int16_t)value_;

- (NSDate*)primitiveStartDate;
- (void)setPrimitiveStartDate:(NSDate*)value;

- (TblCalendarEvent*)primitiveCalendarEvent;
- (void)setPrimitiveCalendarEvent:(TblCalendarEvent*)value;

@end
