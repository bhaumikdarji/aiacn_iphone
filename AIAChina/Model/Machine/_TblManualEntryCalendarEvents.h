// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblManualEntryCalendarEvents.h instead.

#import <CoreData/CoreData.h>

extern const struct TblManualEntryCalendarEventsAttributes {
	__unsafe_unretained NSString *activityType;
	__unsafe_unretained NSString *agentId;
	__unsafe_unretained NSString *allDay;
	__unsafe_unretained NSString *candidateName;
	__unsafe_unretained NSString *eDate;
	__unsafe_unretained NSString *endDate;
	__unsafe_unretained NSString *eventName;
	__unsafe_unretained NSString *latitude;
	__unsafe_unretained NSString *location;
	__unsafe_unretained NSString *longitude;
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *reminder;
	__unsafe_unretained NSString *reminderMode;
	__unsafe_unretained NSString *sDate;
	__unsafe_unretained NSString *startDate;
} TblManualEntryCalendarEventsAttributes;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblManualEntryCalendarEventsID : NSManagedObjectID {}
@end

@interface _TblManualEntryCalendarEvents : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblManualEntryCalendarEventsID* objectID;

@property (nonatomic, strong) id activityType;

//- (BOOL)validateActivityType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agentId;

//- (BOOL)validateAgentId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* allDay;

@property (atomic) BOOL allDayValue;
- (BOOL)allDayValue;
- (void)setAllDayValue:(BOOL)value_;

//- (BOOL)validateAllDay:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id candidateName;

//- (BOOL)validateCandidateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* eDate;

//- (BOOL)validateEDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* endDate;

//- (BOOL)validateEndDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id eventName;

//- (BOOL)validateEventName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* latitude;

@property (atomic) double latitudeValue;
- (double)latitudeValue;
- (void)setLatitudeValue:(double)value_;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id location;

//- (BOOL)validateLocation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* longitude;

@property (atomic) double longitudeValue;
- (double)longitudeValue;
- (void)setLongitudeValue:(double)value_;

//- (BOOL)validateLongitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id notes;

//- (BOOL)validateNotes:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id reminder;

//- (BOOL)validateReminder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id reminderMode;

//- (BOOL)validateReminderMode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* sDate;

//- (BOOL)validateSDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* startDate;

//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;

@end

@interface _TblManualEntryCalendarEvents (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveActivityType;
- (void)setPrimitiveActivityType:(id)value;

- (id)primitiveAgentId;
- (void)setPrimitiveAgentId:(id)value;

- (NSNumber*)primitiveAllDay;
- (void)setPrimitiveAllDay:(NSNumber*)value;

- (BOOL)primitiveAllDayValue;
- (void)setPrimitiveAllDayValue:(BOOL)value_;

- (id)primitiveCandidateName;
- (void)setPrimitiveCandidateName:(id)value;

- (NSDate*)primitiveEDate;
- (void)setPrimitiveEDate:(NSDate*)value;

- (NSDate*)primitiveEndDate;
- (void)setPrimitiveEndDate:(NSDate*)value;

- (id)primitiveEventName;
- (void)setPrimitiveEventName:(id)value;

- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (double)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(double)value_;

- (id)primitiveLocation;
- (void)setPrimitiveLocation:(id)value;

- (NSNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSNumber*)value;

- (double)primitiveLongitudeValue;
- (void)setPrimitiveLongitudeValue:(double)value_;

- (id)primitiveNotes;
- (void)setPrimitiveNotes:(id)value;

- (id)primitiveReminder;
- (void)setPrimitiveReminder:(id)value;

- (id)primitiveReminderMode;
- (void)setPrimitiveReminderMode:(id)value;

- (NSDate*)primitiveSDate;
- (void)setPrimitiveSDate:(NSDate*)value;

- (NSDate*)primitiveStartDate;
- (void)setPrimitiveStartDate:(NSDate*)value;

@end
