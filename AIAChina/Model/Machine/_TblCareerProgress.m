// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblCareerProgress.m instead.

#import "_TblCareerProgress.h"

const struct TblCareerProgressAttributes TblCareerProgressAttributes = {
	.branch = @"branch",
	.department = @"department",
	.endDate = @"endDate",
	.position = @"position",
	.startDate = @"startDate",
};

const struct TblCareerProgressRelationships TblCareerProgressRelationships = {
	.agentDetail = @"agentDetail",
};

@implementation TblCareerProgressID
@end

@implementation _TblCareerProgress

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblCareerProgress" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblCareerProgress";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblCareerProgress" inManagedObjectContext:moc_];
}

- (TblCareerProgressID*)objectID {
	return (TblCareerProgressID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic branch;

@dynamic department;

@dynamic endDate;

@dynamic position;

@dynamic startDate;

@dynamic agentDetail;

@end

