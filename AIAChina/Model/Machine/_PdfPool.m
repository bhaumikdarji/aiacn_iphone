// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PdfPool.m instead.

#import "_PdfPool.h"

const struct PdfPoolAttributes PdfPoolAttributes = {
	.pdfdata = @"pdfdata",
	.pdfpath = @"pdfpath",
};

@implementation PdfPoolID
@end

@implementation _PdfPool

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PdfPool" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PdfPool";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PdfPool" inManagedObjectContext:moc_];
}

- (PdfPoolID*)objectID {
	return (PdfPoolID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic pdfdata;

@dynamic pdfpath;

@end

