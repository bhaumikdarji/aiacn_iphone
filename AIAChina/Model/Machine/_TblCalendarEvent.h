// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblCalendarEvent.h instead.

#import <CoreData/CoreData.h>

extern const struct TblCalendarEventAttributes {
	__unsafe_unretained NSString *agentCode;
	__unsafe_unretained NSString *allDay;
} TblCalendarEventAttributes;

extern const struct TblCalendarEventRelationships {
	__unsafe_unretained NSString *eopEvents;
	__unsafe_unretained NSString *holidayEvents;
	__unsafe_unretained NSString *interviewEvents;
	__unsafe_unretained NSString *trainingEvents;
} TblCalendarEventRelationships;

@class TblEOP;
@class TblHoliday;
@class TblInterview;
@class TblTraninigEvent;

@class NSObject;

@interface TblCalendarEventID : NSManagedObjectID {}
@end

@interface _TblCalendarEvent : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblCalendarEventID* objectID;

@property (nonatomic, strong) id agentCode;

//- (BOOL)validateAgentCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* allDay;

@property (atomic) BOOL allDayValue;
- (BOOL)allDayValue;
- (void)setAllDayValue:(BOOL)value_;

//- (BOOL)validateAllDay:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *eopEvents;

- (NSMutableSet*)eopEventsSet;

@property (nonatomic, strong) NSSet *holidayEvents;

- (NSMutableSet*)holidayEventsSet;

@property (nonatomic, strong) NSSet *interviewEvents;

- (NSMutableSet*)interviewEventsSet;

@property (nonatomic, strong) NSSet *trainingEvents;

- (NSMutableSet*)trainingEventsSet;

@end

@interface _TblCalendarEvent (EopEventsCoreDataGeneratedAccessors)
- (void)addEopEvents:(NSSet*)value_;
- (void)removeEopEvents:(NSSet*)value_;
- (void)addEopEventsObject:(TblEOP*)value_;
- (void)removeEopEventsObject:(TblEOP*)value_;

@end

@interface _TblCalendarEvent (HolidayEventsCoreDataGeneratedAccessors)
- (void)addHolidayEvents:(NSSet*)value_;
- (void)removeHolidayEvents:(NSSet*)value_;
- (void)addHolidayEventsObject:(TblHoliday*)value_;
- (void)removeHolidayEventsObject:(TblHoliday*)value_;

@end

@interface _TblCalendarEvent (InterviewEventsCoreDataGeneratedAccessors)
- (void)addInterviewEvents:(NSSet*)value_;
- (void)removeInterviewEvents:(NSSet*)value_;
- (void)addInterviewEventsObject:(TblInterview*)value_;
- (void)removeInterviewEventsObject:(TblInterview*)value_;

@end

@interface _TblCalendarEvent (TrainingEventsCoreDataGeneratedAccessors)
- (void)addTrainingEvents:(NSSet*)value_;
- (void)removeTrainingEvents:(NSSet*)value_;
- (void)addTrainingEventsObject:(TblTraninigEvent*)value_;
- (void)removeTrainingEventsObject:(TblTraninigEvent*)value_;

@end

@interface _TblCalendarEvent (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveAgentCode;
- (void)setPrimitiveAgentCode:(id)value;

- (NSNumber*)primitiveAllDay;
- (void)setPrimitiveAllDay:(NSNumber*)value;

- (BOOL)primitiveAllDayValue;
- (void)setPrimitiveAllDayValue:(BOOL)value_;

- (NSMutableSet*)primitiveEopEvents;
- (void)setPrimitiveEopEvents:(NSMutableSet*)value;

- (NSMutableSet*)primitiveHolidayEvents;
- (void)setPrimitiveHolidayEvents:(NSMutableSet*)value;

- (NSMutableSet*)primitiveInterviewEvents;
- (void)setPrimitiveInterviewEvents:(NSMutableSet*)value;

- (NSMutableSet*)primitiveTrainingEvents;
- (void)setPrimitiveTrainingEvents:(NSMutableSet*)value;

@end
