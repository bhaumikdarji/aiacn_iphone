// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblInterviewCandidate.m instead.

#import "_TblInterviewCandidate.h"

const struct TblInterviewCandidateAttributes TblInterviewCandidateAttributes = {
	.age = @"age",
	.agencyLeaderCode = @"agencyLeaderCode",
	.agencyLeaderName = @"agencyLeaderName",
	.agentName = @"agentName",
	.buCode = @"buCode",
	.buName = @"buName",
	.candidateCode = @"candidateCode",
	.candidateName = @"candidateName",
	.ccTestResult = @"ccTestResult",
	.cityCode = @"cityCode",
	.contactNumber = @"contactNumber",
	.distName = @"distName",
	.districtCode = @"districtCode",
	.dob = @"dob",
	.dobStr = @"dobStr",
	.fileNameList = @"fileNameList",
	.gender = @"gender",
	.interviewCode = @"interviewCode",
	.interviewResult = @"interviewResult",
	.nric = @"nric",
	.p100 = @"p100",
	.passedStatus = @"passedStatus",
	.recruitmentScheme = @"recruitmentScheme",
	.remarks = @"remarks",
	.servicingAgent = @"servicingAgent",
	.sourceOfReferal = @"sourceOfReferal",
	.sscCode = @"sscCode",
	.status = @"status",
	.token = @"token",
};

@implementation TblInterviewCandidateID
@end

@implementation _TblInterviewCandidate

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblInterviewCandidate" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblInterviewCandidate";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblInterviewCandidate" inManagedObjectContext:moc_];
}

- (TblInterviewCandidateID*)objectID {
	return (TblInterviewCandidateID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"ageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"age"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"buCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"buCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"candidateCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"candidateCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"districtCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"districtCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"interviewCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"interviewCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"p100Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"p100"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic age;

- (int64_t)ageValue {
	NSNumber *result = [self age];
	return [result longLongValue];
}

- (void)setAgeValue:(int64_t)value_ {
	[self setAge:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveAgeValue {
	NSNumber *result = [self primitiveAge];
	return [result longLongValue];
}

- (void)setPrimitiveAgeValue:(int64_t)value_ {
	[self setPrimitiveAge:[NSNumber numberWithLongLong:value_]];
}

@dynamic agencyLeaderCode;

@dynamic agencyLeaderName;

@dynamic agentName;

@dynamic buCode;

- (int64_t)buCodeValue {
	NSNumber *result = [self buCode];
	return [result longLongValue];
}

- (void)setBuCodeValue:(int64_t)value_ {
	[self setBuCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBuCodeValue {
	NSNumber *result = [self primitiveBuCode];
	return [result longLongValue];
}

- (void)setPrimitiveBuCodeValue:(int64_t)value_ {
	[self setPrimitiveBuCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic buName;

@dynamic candidateCode;

- (int64_t)candidateCodeValue {
	NSNumber *result = [self candidateCode];
	return [result longLongValue];
}

- (void)setCandidateCodeValue:(int64_t)value_ {
	[self setCandidateCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveCandidateCodeValue {
	NSNumber *result = [self primitiveCandidateCode];
	return [result longLongValue];
}

- (void)setPrimitiveCandidateCodeValue:(int64_t)value_ {
	[self setPrimitiveCandidateCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic candidateName;

@dynamic ccTestResult;

@dynamic cityCode;

@dynamic contactNumber;

@dynamic distName;

@dynamic districtCode;

- (int64_t)districtCodeValue {
	NSNumber *result = [self districtCode];
	return [result longLongValue];
}

- (void)setDistrictCodeValue:(int64_t)value_ {
	[self setDistrictCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveDistrictCodeValue {
	NSNumber *result = [self primitiveDistrictCode];
	return [result longLongValue];
}

- (void)setPrimitiveDistrictCodeValue:(int64_t)value_ {
	[self setPrimitiveDistrictCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic dob;

@dynamic dobStr;

@dynamic fileNameList;

@dynamic gender;

@dynamic interviewCode;

- (int64_t)interviewCodeValue {
	NSNumber *result = [self interviewCode];
	return [result longLongValue];
}

- (void)setInterviewCodeValue:(int64_t)value_ {
	[self setInterviewCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveInterviewCodeValue {
	NSNumber *result = [self primitiveInterviewCode];
	return [result longLongValue];
}

- (void)setPrimitiveInterviewCodeValue:(int64_t)value_ {
	[self setPrimitiveInterviewCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic interviewResult;

@dynamic nric;

@dynamic p100;

- (int64_t)p100Value {
	NSNumber *result = [self p100];
	return [result longLongValue];
}

- (void)setP100Value:(int64_t)value_ {
	[self setP100:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveP100Value {
	NSNumber *result = [self primitiveP100];
	return [result longLongValue];
}

- (void)setPrimitiveP100Value:(int64_t)value_ {
	[self setPrimitiveP100:[NSNumber numberWithLongLong:value_]];
}

@dynamic passedStatus;

@dynamic recruitmentScheme;

@dynamic remarks;

@dynamic servicingAgent;

@dynamic sourceOfReferal;

@dynamic sscCode;

@dynamic status;

- (BOOL)statusValue {
	NSNumber *result = [self status];
	return [result boolValue];
}

- (void)setStatusValue:(BOOL)value_ {
	[self setStatus:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result boolValue];
}

- (void)setPrimitiveStatusValue:(BOOL)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithBool:value_]];
}

@dynamic token;

@end

