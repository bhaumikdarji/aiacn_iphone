// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblGroup.h instead.

#import <CoreData/CoreData.h>

extern const struct TblGroupAttributes {
	__unsafe_unretained NSString *agentId;
	__unsafe_unretained NSString *groupDescription;
	__unsafe_unretained NSString *groupName;
	__unsafe_unretained NSString *image;
	__unsafe_unretained NSString *iosAddressCode;
	__unsafe_unretained NSString *isDelete;
	__unsafe_unretained NSString *isSync;
} TblGroupAttributes;

extern const struct TblGroupRelationships {
	__unsafe_unretained NSString *contact;
} TblGroupRelationships;

@class TblContact;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblGroupID : NSManagedObjectID {}
@end

@interface _TblGroup : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblGroupID* objectID;

@property (nonatomic, strong) id agentId;

//- (BOOL)validateAgentId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id groupDescription;

//- (BOOL)validateGroupDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id groupName;

//- (BOOL)validateGroupName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id image;

//- (BOOL)validateImage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id iosAddressCode;

//- (BOOL)validateIosAddressCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isDelete;

@property (atomic) BOOL isDeleteValue;
- (BOOL)isDeleteValue;
- (void)setIsDeleteValue:(BOOL)value_;

//- (BOOL)validateIsDelete:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isSync;

@property (atomic) BOOL isSyncValue;
- (BOOL)isSyncValue;
- (void)setIsSyncValue:(BOOL)value_;

//- (BOOL)validateIsSync:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *contact;

- (NSMutableSet*)contactSet;

@end

@interface _TblGroup (ContactCoreDataGeneratedAccessors)
- (void)addContact:(NSSet*)value_;
- (void)removeContact:(NSSet*)value_;
- (void)addContactObject:(TblContact*)value_;
- (void)removeContactObject:(TblContact*)value_;

@end

@interface _TblGroup (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveAgentId;
- (void)setPrimitiveAgentId:(id)value;

- (id)primitiveGroupDescription;
- (void)setPrimitiveGroupDescription:(id)value;

- (id)primitiveGroupName;
- (void)setPrimitiveGroupName:(id)value;

- (id)primitiveImage;
- (void)setPrimitiveImage:(id)value;

- (id)primitiveIosAddressCode;
- (void)setPrimitiveIosAddressCode:(id)value;

- (NSNumber*)primitiveIsDelete;
- (void)setPrimitiveIsDelete:(NSNumber*)value;

- (BOOL)primitiveIsDeleteValue;
- (void)setPrimitiveIsDeleteValue:(BOOL)value_;

- (NSNumber*)primitiveIsSync;
- (void)setPrimitiveIsSync:(NSNumber*)value;

- (BOOL)primitiveIsSyncValue;
- (void)setPrimitiveIsSyncValue:(BOOL)value_;

- (NSMutableSet*)primitiveContact;
- (void)setPrimitiveContact:(NSMutableSet*)value;

@end
