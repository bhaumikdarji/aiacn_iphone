// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblFamily.m instead.

#import "_TblFamily.h"

const struct TblFamilyAttributes TblFamilyAttributes = {
	.iosAddressCode = @"iosAddressCode",
	.name = @"name",
	.occupation = @"occupation",
	.phoneNo = @"phoneNo",
	.position = @"position",
	.relationship = @"relationship",
	.unit = @"unit",
};

const struct TblFamilyRelationships TblFamilyRelationships = {
	.contact = @"contact",
};

@implementation TblFamilyID
@end

@implementation _TblFamily

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblFamily" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblFamily";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblFamily" inManagedObjectContext:moc_];
}

- (TblFamilyID*)objectID {
	return (TblFamilyID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic iosAddressCode;

@dynamic name;

@dynamic occupation;

@dynamic phoneNo;

@dynamic position;

@dynamic relationship;

@dynamic unit;

@dynamic contact;

@end

