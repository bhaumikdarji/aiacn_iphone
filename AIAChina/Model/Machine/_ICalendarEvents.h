// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ICalendarEvents.h instead.

#import <CoreData/CoreData.h>

extern const struct ICalendarEventsAttributes {
	__unsafe_unretained NSString *address;
	__unsafe_unretained NSString *agentCode;
	__unsafe_unretained NSString *co;
	__unsafe_unretained NSString *endTime;
	__unsafe_unretained NSString *eventDescription;
	__unsafe_unretained NSString *eventId;
	__unsafe_unretained NSString *openFlag;
	__unsafe_unretained NSString *origin;
	__unsafe_unretained NSString *referenceId;
	__unsafe_unretained NSString *st_dt;
	__unsafe_unretained NSString *startTime;
	__unsafe_unretained NSString *subject;
	__unsafe_unretained NSString *typeId;
	__unsafe_unretained NSString *typeName;
	__unsafe_unretained NSString *updateTime;
} ICalendarEventsAttributes;

@interface ICalendarEventsID : NSManagedObjectID {}
@end

@interface _ICalendarEvents : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ICalendarEventsID* objectID;

@property (nonatomic, strong) NSString* address;

//- (BOOL)validateAddress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* agentCode;

//- (BOOL)validateAgentCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* co;

//- (BOOL)validateCo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* endTime;

//- (BOOL)validateEndTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* eventDescription;

//- (BOOL)validateEventDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* eventId;

//- (BOOL)validateEventId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* openFlag;

//- (BOOL)validateOpenFlag:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* origin;

//- (BOOL)validateOrigin:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* referenceId;

//- (BOOL)validateReferenceId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* st_dt;

//- (BOOL)validateSt_dt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* startTime;

//- (BOOL)validateStartTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* subject;

//- (BOOL)validateSubject:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* typeId;

//- (BOOL)validateTypeId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* typeName;

//- (BOOL)validateTypeName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* updateTime;

//- (BOOL)validateUpdateTime:(id*)value_ error:(NSError**)error_;

@end

@interface _ICalendarEvents (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;

- (NSString*)primitiveAgentCode;
- (void)setPrimitiveAgentCode:(NSString*)value;

- (NSString*)primitiveCo;
- (void)setPrimitiveCo:(NSString*)value;

- (NSString*)primitiveEndTime;
- (void)setPrimitiveEndTime:(NSString*)value;

- (NSString*)primitiveEventDescription;
- (void)setPrimitiveEventDescription:(NSString*)value;

- (NSString*)primitiveEventId;
- (void)setPrimitiveEventId:(NSString*)value;

- (NSString*)primitiveOpenFlag;
- (void)setPrimitiveOpenFlag:(NSString*)value;

- (NSString*)primitiveOrigin;
- (void)setPrimitiveOrigin:(NSString*)value;

- (NSString*)primitiveReferenceId;
- (void)setPrimitiveReferenceId:(NSString*)value;

- (NSString*)primitiveSt_dt;
- (void)setPrimitiveSt_dt:(NSString*)value;

- (NSString*)primitiveStartTime;
- (void)setPrimitiveStartTime:(NSString*)value;

- (NSString*)primitiveSubject;
- (void)setPrimitiveSubject:(NSString*)value;

- (NSString*)primitiveTypeId;
- (void)setPrimitiveTypeId:(NSString*)value;

- (NSString*)primitiveTypeName;
- (void)setPrimitiveTypeName:(NSString*)value;

- (NSString*)primitiveUpdateTime;
- (void)setPrimitiveUpdateTime:(NSString*)value;

@end
