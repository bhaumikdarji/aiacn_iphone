// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblGreetings.m instead.

#import "_TblGreetings.h"

const struct TblGreetingsAttributes TblGreetingsAttributes = {
	.agentId = @"agentId",
	.catName = @"catName",
	.createdby = @"createdby",
	.createddate = @"createddate",
	.eGreetingCode = @"eGreetingCode",
	.eGreetingName = @"eGreetingName",
	.festiveCategory = @"festiveCategory",
	.fileLocation = @"fileLocation",
	.fileName = @"fileName",
	.modifiedby = @"modifiedby",
	.modifieddate = @"modifieddate",
	.status = @"status",
	.token = @"token",
};

@implementation TblGreetingsID
@end

@implementation _TblGreetings

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblGreetings" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblGreetings";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblGreetings" inManagedObjectContext:moc_];
}

- (TblGreetingsID*)objectID {
	return (TblGreetingsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"eGreetingCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"eGreetingCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"festiveCategoryValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"festiveCategory"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic agentId;

@dynamic catName;

@dynamic createdby;

@dynamic createddate;

@dynamic eGreetingCode;

- (int16_t)eGreetingCodeValue {
	NSNumber *result = [self eGreetingCode];
	return [result shortValue];
}

- (void)setEGreetingCodeValue:(int16_t)value_ {
	[self setEGreetingCode:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveEGreetingCodeValue {
	NSNumber *result = [self primitiveEGreetingCode];
	return [result shortValue];
}

- (void)setPrimitiveEGreetingCodeValue:(int16_t)value_ {
	[self setPrimitiveEGreetingCode:[NSNumber numberWithShort:value_]];
}

@dynamic eGreetingName;

@dynamic festiveCategory;

- (int16_t)festiveCategoryValue {
	NSNumber *result = [self festiveCategory];
	return [result shortValue];
}

- (void)setFestiveCategoryValue:(int16_t)value_ {
	[self setFestiveCategory:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveFestiveCategoryValue {
	NSNumber *result = [self primitiveFestiveCategory];
	return [result shortValue];
}

- (void)setPrimitiveFestiveCategoryValue:(int16_t)value_ {
	[self setPrimitiveFestiveCategory:[NSNumber numberWithShort:value_]];
}

@dynamic fileLocation;

@dynamic fileName;

@dynamic modifiedby;

@dynamic modifieddate;

@dynamic status;

- (BOOL)statusValue {
	NSNumber *result = [self status];
	return [result boolValue];
}

- (void)setStatusValue:(BOOL)value_ {
	[self setStatus:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result boolValue];
}

- (void)setPrimitiveStatusValue:(BOOL)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithBool:value_]];
}

@dynamic token;

@end

