// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblHoliday.m instead.

#import "_TblHoliday.h"

const struct TblHolidayAttributes TblHolidayAttributes = {
	.buCode = @"buCode",
	.buName = @"buName",
	.cityCode = @"cityCode",
	.createdBy = @"createdBy",
	.creationDate = @"creationDate",
	.district = @"district",
	.endDate = @"endDate",
	.holidayCode = @"holidayCode",
	.holidayName = @"holidayName",
	.iconSelection = @"iconSelection",
	.modificationDate = @"modificationDate",
	.modifiedBy = @"modifiedBy",
	.postedBy = @"postedBy",
	.sscCode = @"sscCode",
	.startDate = @"startDate",
	.status = @"status",
	.token = @"token",
};

const struct TblHolidayRelationships TblHolidayRelationships = {
	.calendarEvent = @"calendarEvent",
};

@implementation TblHolidayID
@end

@implementation _TblHoliday

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblHoliday" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblHoliday";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblHoliday" inManagedObjectContext:moc_];
}

- (TblHolidayID*)objectID {
	return (TblHolidayID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"buCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"buCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"createdByValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"createdBy"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"districtValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"district"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"holidayCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"holidayCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic buCode;

- (int64_t)buCodeValue {
	NSNumber *result = [self buCode];
	return [result longLongValue];
}

- (void)setBuCodeValue:(int64_t)value_ {
	[self setBuCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBuCodeValue {
	NSNumber *result = [self primitiveBuCode];
	return [result longLongValue];
}

- (void)setPrimitiveBuCodeValue:(int64_t)value_ {
	[self setPrimitiveBuCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic buName;

@dynamic cityCode;

@dynamic createdBy;

- (int64_t)createdByValue {
	NSNumber *result = [self createdBy];
	return [result longLongValue];
}

- (void)setCreatedByValue:(int64_t)value_ {
	[self setCreatedBy:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveCreatedByValue {
	NSNumber *result = [self primitiveCreatedBy];
	return [result longLongValue];
}

- (void)setPrimitiveCreatedByValue:(int64_t)value_ {
	[self setPrimitiveCreatedBy:[NSNumber numberWithLongLong:value_]];
}

@dynamic creationDate;

@dynamic district;

- (int64_t)districtValue {
	NSNumber *result = [self district];
	return [result longLongValue];
}

- (void)setDistrictValue:(int64_t)value_ {
	[self setDistrict:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveDistrictValue {
	NSNumber *result = [self primitiveDistrict];
	return [result longLongValue];
}

- (void)setPrimitiveDistrictValue:(int64_t)value_ {
	[self setPrimitiveDistrict:[NSNumber numberWithLongLong:value_]];
}

@dynamic endDate;

@dynamic holidayCode;

- (int64_t)holidayCodeValue {
	NSNumber *result = [self holidayCode];
	return [result longLongValue];
}

- (void)setHolidayCodeValue:(int64_t)value_ {
	[self setHolidayCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveHolidayCodeValue {
	NSNumber *result = [self primitiveHolidayCode];
	return [result longLongValue];
}

- (void)setPrimitiveHolidayCodeValue:(int64_t)value_ {
	[self setPrimitiveHolidayCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic holidayName;

@dynamic iconSelection;

@dynamic modificationDate;

@dynamic modifiedBy;

@dynamic postedBy;

@dynamic sscCode;

@dynamic startDate;

@dynamic status;

- (BOOL)statusValue {
	NSNumber *result = [self status];
	return [result boolValue];
}

- (void)setStatusValue:(BOOL)value_ {
	[self setStatus:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result boolValue];
}

- (void)setPrimitiveStatusValue:(BOOL)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithBool:value_]];
}

@dynamic token;

@dynamic calendarEvent;

@end

