// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblFamily.h instead.

#import <CoreData/CoreData.h>

extern const struct TblFamilyAttributes {
	__unsafe_unretained NSString *iosAddressCode;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *occupation;
	__unsafe_unretained NSString *phoneNo;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *relationship;
	__unsafe_unretained NSString *unit;
} TblFamilyAttributes;

extern const struct TblFamilyRelationships {
	__unsafe_unretained NSString *contact;
} TblFamilyRelationships;

@class TblContact;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblFamilyID : NSManagedObjectID {}
@end

@interface _TblFamily : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblFamilyID* objectID;

@property (nonatomic, strong) id iosAddressCode;

//- (BOOL)validateIosAddressCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id occupation;

//- (BOOL)validateOccupation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id phoneNo;

//- (BOOL)validatePhoneNo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id position;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id relationship;

//- (BOOL)validateRelationship:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id unit;

//- (BOOL)validateUnit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblContact *contact;

//- (BOOL)validateContact:(id*)value_ error:(NSError**)error_;

@end

@interface _TblFamily (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveIosAddressCode;
- (void)setPrimitiveIosAddressCode:(id)value;

- (id)primitiveName;
- (void)setPrimitiveName:(id)value;

- (id)primitiveOccupation;
- (void)setPrimitiveOccupation:(id)value;

- (id)primitivePhoneNo;
- (void)setPrimitivePhoneNo:(id)value;

- (id)primitivePosition;
- (void)setPrimitivePosition:(id)value;

- (id)primitiveRelationship;
- (void)setPrimitiveRelationship:(id)value;

- (id)primitiveUnit;
- (void)setPrimitiveUnit:(id)value;

- (TblContact*)primitiveContact;
- (void)setPrimitiveContact:(TblContact*)value;

@end
