// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblAgentDetails.m instead.

#import "_TblAgentDetails.h"

const struct TblAgentDetailsAttributes TblAgentDetailsAttributes = {
	.agentDetails = @"agentDetails",
	.agentGallayImages = @"agentGallayImages",
	.agentImage = @"agentImage",
	.agentType = @"agentType",
	.branch = @"branch",
	.certiID = @"certiID",
	.city = @"city",
	.contactNumber = @"contactNumber",
	.contractedDate = @"contractedDate",
	.dateofBirth = @"dateofBirth",
	.dtleader = @"dtleader",
	.educationBackground = @"educationBackground",
	.emailId = @"emailId",
	.freeText = @"freeText",
	.gender = @"gender",
	.graduationSchool = @"graduationSchool",
	.graduationTime = @"graduationTime",
	.isFirstTimeLogin = @"isFirstTimeLogin",
	.officeCode = @"officeCode",
	.officeName = @"officeName",
	.professionalInstitutions = @"professionalInstitutions",
	.rank = @"rank",
	.serviceDepartment = @"serviceDepartment",
	.ssc = @"ssc",
	.teamCode = @"teamCode",
	.teamName = @"teamName",
	.title = @"title",
	.titleDescribe = @"titleDescribe",
	.userID = @"userID",
	.userName = @"userName",
	.userStatus = @"userStatus",
	.userType = @"userType",
};

const struct TblAgentDetailsRelationships TblAgentDetailsRelationships = {
	.agentCareerProgress = @"agentCareerProgress",
};

@implementation TblAgentDetailsID
@end

@implementation _TblAgentDetails

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblAgentDetails" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblAgentDetails";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblAgentDetails" inManagedObjectContext:moc_];
}

- (TblAgentDetailsID*)objectID {
	return (TblAgentDetailsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isFirstTimeLoginValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isFirstTimeLogin"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic agentDetails;

@dynamic agentGallayImages;

@dynamic agentImage;

@dynamic agentType;

@dynamic branch;

@dynamic certiID;

@dynamic city;

@dynamic contactNumber;

@dynamic contractedDate;

@dynamic dateofBirth;

@dynamic dtleader;

@dynamic educationBackground;

@dynamic emailId;

@dynamic freeText;

@dynamic gender;

@dynamic graduationSchool;

@dynamic graduationTime;

@dynamic isFirstTimeLogin;

- (BOOL)isFirstTimeLoginValue {
	NSNumber *result = [self isFirstTimeLogin];
	return [result boolValue];
}

- (void)setIsFirstTimeLoginValue:(BOOL)value_ {
	[self setIsFirstTimeLogin:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsFirstTimeLoginValue {
	NSNumber *result = [self primitiveIsFirstTimeLogin];
	return [result boolValue];
}

- (void)setPrimitiveIsFirstTimeLoginValue:(BOOL)value_ {
	[self setPrimitiveIsFirstTimeLogin:[NSNumber numberWithBool:value_]];
}

@dynamic officeCode;

@dynamic officeName;

@dynamic professionalInstitutions;

@dynamic rank;

@dynamic serviceDepartment;

@dynamic ssc;

@dynamic teamCode;

@dynamic teamName;

@dynamic title;

@dynamic titleDescribe;

@dynamic userID;

@dynamic userName;

@dynamic userStatus;

@dynamic userType;

@dynamic agentCareerProgress;

- (NSMutableSet*)agentCareerProgressSet {
	[self willAccessValueForKey:@"agentCareerProgress"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"agentCareerProgress"];

	[self didAccessValueForKey:@"agentCareerProgress"];
	return result;
}

@end

