// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblCareerProgress.h instead.

#import <CoreData/CoreData.h>

extern const struct TblCareerProgressAttributes {
	__unsafe_unretained NSString *branch;
	__unsafe_unretained NSString *department;
	__unsafe_unretained NSString *endDate;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *startDate;
} TblCareerProgressAttributes;

extern const struct TblCareerProgressRelationships {
	__unsafe_unretained NSString *agentDetail;
} TblCareerProgressRelationships;

@class TblAgentDetails;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblCareerProgressID : NSManagedObjectID {}
@end

@interface _TblCareerProgress : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblCareerProgressID* objectID;

@property (nonatomic, strong) id branch;

//- (BOOL)validateBranch:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id department;

//- (BOOL)validateDepartment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* endDate;

//- (BOOL)validateEndDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id position;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* startDate;

//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblAgentDetails *agentDetail;

//- (BOOL)validateAgentDetail:(id*)value_ error:(NSError**)error_;

@end

@interface _TblCareerProgress (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveBranch;
- (void)setPrimitiveBranch:(id)value;

- (id)primitiveDepartment;
- (void)setPrimitiveDepartment:(id)value;

- (NSDate*)primitiveEndDate;
- (void)setPrimitiveEndDate:(NSDate*)value;

- (id)primitivePosition;
- (void)setPrimitivePosition:(id)value;

- (NSDate*)primitiveStartDate;
- (void)setPrimitiveStartDate:(NSDate*)value;

- (TblAgentDetails*)primitiveAgentDetail;
- (void)setPrimitiveAgentDetail:(TblAgentDetails*)value;

@end
