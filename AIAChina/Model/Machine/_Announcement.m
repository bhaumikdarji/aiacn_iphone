// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Announcement.m instead.

#import "_Announcement.h"

const struct AnnouncementAttributes AnnouncementAttributes = {
	.agentId = @"agentId",
	.annoucement_code = @"annoucement_code",
	.attachmentPath = @"attachmentPath",
	.bu = @"bu",
	.buCode = @"buCode",
	.buName = @"buName",
	.city = @"city",
	.cityCode = @"cityCode",
	.createdBy = @"createdBy",
	.creationDate = @"creationDate",
	.dist = @"dist",
	.district = @"district",
	.expDate = @"expDate",
	.isRead = @"isRead",
	.message = @"message",
	.modificationDate = @"modificationDate",
	.modifiedBy = @"modifiedBy",
	.msgType = @"msgType",
	.publishedDate = @"publishedDate",
	.ssc = @"ssc",
	.sscCode = @"sscCode",
	.status = @"status",
	.subject = @"subject",
	.token = @"token",
	.userName = @"userName",
};

const struct AnnouncementRelationships AnnouncementRelationships = {
	.pdfDetails = @"pdfDetails",
};

@implementation AnnouncementID
@end

@implementation _Announcement

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Announcement" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Announcement";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Announcement" inManagedObjectContext:moc_];
}

- (AnnouncementID*)objectID {
	return (AnnouncementID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"buCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"buCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"districtValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"district"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isReadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRead"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic agentId;

@dynamic annoucement_code;

@dynamic attachmentPath;

@dynamic bu;

@dynamic buCode;

- (int64_t)buCodeValue {
	NSNumber *result = [self buCode];
	return [result longLongValue];
}

- (void)setBuCodeValue:(int64_t)value_ {
	[self setBuCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBuCodeValue {
	NSNumber *result = [self primitiveBuCode];
	return [result longLongValue];
}

- (void)setPrimitiveBuCodeValue:(int64_t)value_ {
	[self setPrimitiveBuCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic buName;

@dynamic city;

@dynamic cityCode;

@dynamic createdBy;

@dynamic creationDate;

@dynamic dist;

@dynamic district;

- (int64_t)districtValue {
	NSNumber *result = [self district];
	return [result longLongValue];
}

- (void)setDistrictValue:(int64_t)value_ {
	[self setDistrict:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveDistrictValue {
	NSNumber *result = [self primitiveDistrict];
	return [result longLongValue];
}

- (void)setPrimitiveDistrictValue:(int64_t)value_ {
	[self setPrimitiveDistrict:[NSNumber numberWithLongLong:value_]];
}

@dynamic expDate;

@dynamic isRead;

- (BOOL)isReadValue {
	NSNumber *result = [self isRead];
	return [result boolValue];
}

- (void)setIsReadValue:(BOOL)value_ {
	[self setIsRead:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsReadValue {
	NSNumber *result = [self primitiveIsRead];
	return [result boolValue];
}

- (void)setPrimitiveIsReadValue:(BOOL)value_ {
	[self setPrimitiveIsRead:[NSNumber numberWithBool:value_]];
}

@dynamic message;

@dynamic modificationDate;

@dynamic modifiedBy;

@dynamic msgType;

@dynamic publishedDate;

@dynamic ssc;

@dynamic sscCode;

@dynamic status;

- (BOOL)statusValue {
	NSNumber *result = [self status];
	return [result boolValue];
}

- (void)setStatusValue:(BOOL)value_ {
	[self setStatus:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result boolValue];
}

- (void)setPrimitiveStatusValue:(BOOL)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithBool:value_]];
}

@dynamic subject;

@dynamic token;

@dynamic userName;

@dynamic pdfDetails;

@end

