// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Announcement.h instead.

#import <CoreData/CoreData.h>

extern const struct AnnouncementAttributes {
	__unsafe_unretained NSString *agentId;
	__unsafe_unretained NSString *annoucement_code;
	__unsafe_unretained NSString *attachmentPath;
	__unsafe_unretained NSString *bu;
	__unsafe_unretained NSString *buCode;
	__unsafe_unretained NSString *buName;
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *cityCode;
	__unsafe_unretained NSString *createdBy;
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *dist;
	__unsafe_unretained NSString *district;
	__unsafe_unretained NSString *expDate;
	__unsafe_unretained NSString *isRead;
	__unsafe_unretained NSString *message;
	__unsafe_unretained NSString *modificationDate;
	__unsafe_unretained NSString *modifiedBy;
	__unsafe_unretained NSString *msgType;
	__unsafe_unretained NSString *publishedDate;
	__unsafe_unretained NSString *ssc;
	__unsafe_unretained NSString *sscCode;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *subject;
	__unsafe_unretained NSString *token;
	__unsafe_unretained NSString *userName;
} AnnouncementAttributes;

extern const struct AnnouncementRelationships {
	__unsafe_unretained NSString *pdfDetails;
} AnnouncementRelationships;

@class PdfPool;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface AnnouncementID : NSManagedObjectID {}
@end

@interface _Announcement : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AnnouncementID* objectID;

@property (nonatomic, strong) NSString* agentId;

//- (BOOL)validateAgentId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* annoucement_code;

//- (BOOL)validateAnnoucement_code:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id attachmentPath;

//- (BOOL)validateAttachmentPath:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id bu;

//- (BOOL)validateBu:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* buCode;

@property (atomic) int64_t buCodeValue;
- (int64_t)buCodeValue;
- (void)setBuCodeValue:(int64_t)value_;

//- (BOOL)validateBuCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id buName;

//- (BOOL)validateBuName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id city;

//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id cityCode;

//- (BOOL)validateCityCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id createdBy;

//- (BOOL)validateCreatedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id dist;

//- (BOOL)validateDist:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* district;

@property (atomic) int64_t districtValue;
- (int64_t)districtValue;
- (void)setDistrictValue:(int64_t)value_;

//- (BOOL)validateDistrict:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id expDate;

//- (BOOL)validateExpDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRead;

@property (atomic) BOOL isReadValue;
- (BOOL)isReadValue;
- (void)setIsReadValue:(BOOL)value_;

//- (BOOL)validateIsRead:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id message;

//- (BOOL)validateMessage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id modificationDate;

//- (BOOL)validateModificationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id modifiedBy;

//- (BOOL)validateModifiedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* msgType;

//- (BOOL)validateMsgType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id publishedDate;

//- (BOOL)validatePublishedDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id ssc;

//- (BOOL)validateSsc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sscCode;

//- (BOOL)validateSscCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) BOOL statusValue;
- (BOOL)statusValue;
- (void)setStatusValue:(BOOL)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id subject;

//- (BOOL)validateSubject:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id token;

//- (BOOL)validateToken:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id userName;

//- (BOOL)validateUserName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) PdfPool *pdfDetails;

//- (BOOL)validatePdfDetails:(id*)value_ error:(NSError**)error_;

@end

@interface _Announcement (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAgentId;
- (void)setPrimitiveAgentId:(NSString*)value;

- (NSString*)primitiveAnnoucement_code;
- (void)setPrimitiveAnnoucement_code:(NSString*)value;

- (id)primitiveAttachmentPath;
- (void)setPrimitiveAttachmentPath:(id)value;

- (id)primitiveBu;
- (void)setPrimitiveBu:(id)value;

- (NSNumber*)primitiveBuCode;
- (void)setPrimitiveBuCode:(NSNumber*)value;

- (int64_t)primitiveBuCodeValue;
- (void)setPrimitiveBuCodeValue:(int64_t)value_;

- (id)primitiveBuName;
- (void)setPrimitiveBuName:(id)value;

- (id)primitiveCity;
- (void)setPrimitiveCity:(id)value;

- (id)primitiveCityCode;
- (void)setPrimitiveCityCode:(id)value;

- (id)primitiveCreatedBy;
- (void)setPrimitiveCreatedBy:(id)value;

- (id)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(id)value;

- (id)primitiveDist;
- (void)setPrimitiveDist:(id)value;

- (NSNumber*)primitiveDistrict;
- (void)setPrimitiveDistrict:(NSNumber*)value;

- (int64_t)primitiveDistrictValue;
- (void)setPrimitiveDistrictValue:(int64_t)value_;

- (id)primitiveExpDate;
- (void)setPrimitiveExpDate:(id)value;

- (NSNumber*)primitiveIsRead;
- (void)setPrimitiveIsRead:(NSNumber*)value;

- (BOOL)primitiveIsReadValue;
- (void)setPrimitiveIsReadValue:(BOOL)value_;

- (id)primitiveMessage;
- (void)setPrimitiveMessage:(id)value;

- (id)primitiveModificationDate;
- (void)setPrimitiveModificationDate:(id)value;

- (id)primitiveModifiedBy;
- (void)setPrimitiveModifiedBy:(id)value;

- (NSString*)primitiveMsgType;
- (void)setPrimitiveMsgType:(NSString*)value;

- (id)primitivePublishedDate;
- (void)setPrimitivePublishedDate:(id)value;

- (id)primitiveSsc;
- (void)setPrimitiveSsc:(id)value;

- (NSString*)primitiveSscCode;
- (void)setPrimitiveSscCode:(NSString*)value;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (BOOL)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(BOOL)value_;

- (id)primitiveSubject;
- (void)setPrimitiveSubject:(id)value;

- (id)primitiveToken;
- (void)setPrimitiveToken:(id)value;

- (id)primitiveUserName;
- (void)setPrimitiveUserName:(id)value;

- (PdfPool*)primitivePdfDetails;
- (void)setPrimitivePdfDetails:(PdfPool*)value;

@end
