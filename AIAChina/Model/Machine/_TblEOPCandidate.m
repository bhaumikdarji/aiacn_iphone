// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblEOPCandidate.m instead.

#import "_TblEOPCandidate.h"

const struct TblEOPCandidateAttributes TblEOPCandidateAttributes = {
	.age = @"age",
	.agencyLeaderCode = @"agencyLeaderCode",
	.agencyLeaderName = @"agencyLeaderName",
	.agentName = @"agentName",
	.attendedEventCount = @"attendedEventCount",
	.attendedStatus = @"attendedStatus",
	.buCode = @"buCode",
	.buName = @"buName",
	.candidateCode = @"candidateCode",
	.candidateName = @"candidateName",
	.cityCode = @"cityCode",
	.completeStatus = @"completeStatus",
	.contactNumber = @"contactNumber",
	.distName = @"distName",
	.districtCode = @"districtCode",
	.dob = @"dob",
	.dobStr = @"dobStr",
	.eventCandidateCode = @"eventCandidateCode",
	.eventCode = @"eventCode",
	.gender = @"gender",
	.servicingAgent = @"servicingAgent",
	.sourceOfReferal = @"sourceOfReferal",
	.sscCode = @"sscCode",
	.status = @"status",
	.token = @"token",
};

@implementation TblEOPCandidateID
@end

@implementation _TblEOPCandidate

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblEOPCandidate" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblEOPCandidate";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblEOPCandidate" inManagedObjectContext:moc_];
}

- (TblEOPCandidateID*)objectID {
	return (TblEOPCandidateID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"ageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"age"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"attendedEventCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"attendedEventCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"buCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"buCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"candidateCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"candidateCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"districtCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"districtCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"eventCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"eventCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic age;

- (int64_t)ageValue {
	NSNumber *result = [self age];
	return [result longLongValue];
}

- (void)setAgeValue:(int64_t)value_ {
	[self setAge:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveAgeValue {
	NSNumber *result = [self primitiveAge];
	return [result longLongValue];
}

- (void)setPrimitiveAgeValue:(int64_t)value_ {
	[self setPrimitiveAge:[NSNumber numberWithLongLong:value_]];
}

@dynamic agencyLeaderCode;

@dynamic agencyLeaderName;

@dynamic agentName;

@dynamic attendedEventCount;

- (int64_t)attendedEventCountValue {
	NSNumber *result = [self attendedEventCount];
	return [result longLongValue];
}

- (void)setAttendedEventCountValue:(int64_t)value_ {
	[self setAttendedEventCount:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveAttendedEventCountValue {
	NSNumber *result = [self primitiveAttendedEventCount];
	return [result longLongValue];
}

- (void)setPrimitiveAttendedEventCountValue:(int64_t)value_ {
	[self setPrimitiveAttendedEventCount:[NSNumber numberWithLongLong:value_]];
}

@dynamic attendedStatus;

@dynamic buCode;

- (int64_t)buCodeValue {
	NSNumber *result = [self buCode];
	return [result longLongValue];
}

- (void)setBuCodeValue:(int64_t)value_ {
	[self setBuCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBuCodeValue {
	NSNumber *result = [self primitiveBuCode];
	return [result longLongValue];
}

- (void)setPrimitiveBuCodeValue:(int64_t)value_ {
	[self setPrimitiveBuCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic buName;

@dynamic candidateCode;

- (int64_t)candidateCodeValue {
	NSNumber *result = [self candidateCode];
	return [result longLongValue];
}

- (void)setCandidateCodeValue:(int64_t)value_ {
	[self setCandidateCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveCandidateCodeValue {
	NSNumber *result = [self primitiveCandidateCode];
	return [result longLongValue];
}

- (void)setPrimitiveCandidateCodeValue:(int64_t)value_ {
	[self setPrimitiveCandidateCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic candidateName;

@dynamic cityCode;

@dynamic completeStatus;

@dynamic contactNumber;

@dynamic distName;

@dynamic districtCode;

- (int64_t)districtCodeValue {
	NSNumber *result = [self districtCode];
	return [result longLongValue];
}

- (void)setDistrictCodeValue:(int64_t)value_ {
	[self setDistrictCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveDistrictCodeValue {
	NSNumber *result = [self primitiveDistrictCode];
	return [result longLongValue];
}

- (void)setPrimitiveDistrictCodeValue:(int64_t)value_ {
	[self setPrimitiveDistrictCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic dob;

@dynamic dobStr;

@dynamic eventCandidateCode;

@dynamic eventCode;

- (int64_t)eventCodeValue {
	NSNumber *result = [self eventCode];
	return [result longLongValue];
}

- (void)setEventCodeValue:(int64_t)value_ {
	[self setEventCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveEventCodeValue {
	NSNumber *result = [self primitiveEventCode];
	return [result longLongValue];
}

- (void)setPrimitiveEventCodeValue:(int64_t)value_ {
	[self setPrimitiveEventCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic gender;

@dynamic servicingAgent;

@dynamic sourceOfReferal;

@dynamic sscCode;

@dynamic status;

- (BOOL)statusValue {
	NSNumber *result = [self status];
	return [result boolValue];
}

- (void)setStatusValue:(BOOL)value_ {
	[self setStatus:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result boolValue];
}

- (void)setPrimitiveStatusValue:(BOOL)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithBool:value_]];
}

@dynamic token;

@end

