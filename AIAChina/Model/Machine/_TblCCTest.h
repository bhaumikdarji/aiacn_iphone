// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblCCTest.h instead.

#import <CoreData/CoreData.h>

extern const struct TblCCTestAttributes {
	__unsafe_unretained NSString *assessmentResult;
	__unsafe_unretained NSString *cautionRecruit;
	__unsafe_unretained NSString *noramalRecruit;
	__unsafe_unretained NSString *urgentRecruit;
} TblCCTestAttributes;

@class NSObject;

@interface TblCCTestID : NSManagedObjectID {}
@end

@interface _TblCCTest : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblCCTestID* objectID;

@property (nonatomic, strong) id assessmentResult;

//- (BOOL)validateAssessmentResult:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cautionRecruit;

@property (atomic) BOOL cautionRecruitValue;
- (BOOL)cautionRecruitValue;
- (void)setCautionRecruitValue:(BOOL)value_;

//- (BOOL)validateCautionRecruit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* noramalRecruit;

@property (atomic) BOOL noramalRecruitValue;
- (BOOL)noramalRecruitValue;
- (void)setNoramalRecruitValue:(BOOL)value_;

//- (BOOL)validateNoramalRecruit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* urgentRecruit;

@property (atomic) BOOL urgentRecruitValue;
- (BOOL)urgentRecruitValue;
- (void)setUrgentRecruitValue:(BOOL)value_;

//- (BOOL)validateUrgentRecruit:(id*)value_ error:(NSError**)error_;

@end

@interface _TblCCTest (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveAssessmentResult;
- (void)setPrimitiveAssessmentResult:(id)value;

- (NSNumber*)primitiveCautionRecruit;
- (void)setPrimitiveCautionRecruit:(NSNumber*)value;

- (BOOL)primitiveCautionRecruitValue;
- (void)setPrimitiveCautionRecruitValue:(BOOL)value_;

- (NSNumber*)primitiveNoramalRecruit;
- (void)setPrimitiveNoramalRecruit:(NSNumber*)value;

- (BOOL)primitiveNoramalRecruitValue;
- (void)setPrimitiveNoramalRecruitValue:(BOOL)value_;

- (NSNumber*)primitiveUrgentRecruit;
- (void)setPrimitiveUrgentRecruit:(NSNumber*)value;

- (BOOL)primitiveUrgentRecruitValue;
- (void)setPrimitiveUrgentRecruitValue:(BOOL)value_;

@end
