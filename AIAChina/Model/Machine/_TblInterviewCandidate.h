// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblInterviewCandidate.h instead.

#import <CoreData/CoreData.h>

extern const struct TblInterviewCandidateAttributes {
	__unsafe_unretained NSString *age;
	__unsafe_unretained NSString *agencyLeaderCode;
	__unsafe_unretained NSString *agencyLeaderName;
	__unsafe_unretained NSString *agentName;
	__unsafe_unretained NSString *buCode;
	__unsafe_unretained NSString *buName;
	__unsafe_unretained NSString *candidateCode;
	__unsafe_unretained NSString *candidateName;
	__unsafe_unretained NSString *ccTestResult;
	__unsafe_unretained NSString *cityCode;
	__unsafe_unretained NSString *contactNumber;
	__unsafe_unretained NSString *distName;
	__unsafe_unretained NSString *districtCode;
	__unsafe_unretained NSString *dob;
	__unsafe_unretained NSString *dobStr;
	__unsafe_unretained NSString *fileNameList;
	__unsafe_unretained NSString *gender;
	__unsafe_unretained NSString *interviewCode;
	__unsafe_unretained NSString *interviewResult;
	__unsafe_unretained NSString *nric;
	__unsafe_unretained NSString *p100;
	__unsafe_unretained NSString *passedStatus;
	__unsafe_unretained NSString *recruitmentScheme;
	__unsafe_unretained NSString *remarks;
	__unsafe_unretained NSString *servicingAgent;
	__unsafe_unretained NSString *sourceOfReferal;
	__unsafe_unretained NSString *sscCode;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *token;
} TblInterviewCandidateAttributes;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblInterviewCandidateID : NSManagedObjectID {}
@end

@interface _TblInterviewCandidate : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblInterviewCandidateID* objectID;

@property (nonatomic, strong) NSNumber* age;

@property (atomic) int64_t ageValue;
- (int64_t)ageValue;
- (void)setAgeValue:(int64_t)value_;

//- (BOOL)validateAge:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agencyLeaderCode;

//- (BOOL)validateAgencyLeaderCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agencyLeaderName;

//- (BOOL)validateAgencyLeaderName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agentName;

//- (BOOL)validateAgentName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* buCode;

@property (atomic) int64_t buCodeValue;
- (int64_t)buCodeValue;
- (void)setBuCodeValue:(int64_t)value_;

//- (BOOL)validateBuCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id buName;

//- (BOOL)validateBuName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* candidateCode;

@property (atomic) int64_t candidateCodeValue;
- (int64_t)candidateCodeValue;
- (void)setCandidateCodeValue:(int64_t)value_;

//- (BOOL)validateCandidateCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id candidateName;

//- (BOOL)validateCandidateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id ccTestResult;

//- (BOOL)validateCcTestResult:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* cityCode;

//- (BOOL)validateCityCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id contactNumber;

//- (BOOL)validateContactNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id distName;

//- (BOOL)validateDistName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* districtCode;

@property (atomic) int64_t districtCodeValue;
- (int64_t)districtCodeValue;
- (void)setDistrictCodeValue:(int64_t)value_;

//- (BOOL)validateDistrictCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* dob;

//- (BOOL)validateDob:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id dobStr;

//- (BOOL)validateDobStr:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id fileNameList;

//- (BOOL)validateFileNameList:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id gender;

//- (BOOL)validateGender:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* interviewCode;

@property (atomic) int64_t interviewCodeValue;
- (int64_t)interviewCodeValue;
- (void)setInterviewCodeValue:(int64_t)value_;

//- (BOOL)validateInterviewCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id interviewResult;

//- (BOOL)validateInterviewResult:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id nric;

//- (BOOL)validateNric:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* p100;

@property (atomic) int64_t p100Value;
- (int64_t)p100Value;
- (void)setP100Value:(int64_t)value_;

//- (BOOL)validateP100:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id passedStatus;

//- (BOOL)validatePassedStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id recruitmentScheme;

//- (BOOL)validateRecruitmentScheme:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id remarks;

//- (BOOL)validateRemarks:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id servicingAgent;

//- (BOOL)validateServicingAgent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id sourceOfReferal;

//- (BOOL)validateSourceOfReferal:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sscCode;

//- (BOOL)validateSscCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) BOOL statusValue;
- (BOOL)statusValue;
- (void)setStatusValue:(BOOL)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id token;

//- (BOOL)validateToken:(id*)value_ error:(NSError**)error_;

@end

@interface _TblInterviewCandidate (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAge;
- (void)setPrimitiveAge:(NSNumber*)value;

- (int64_t)primitiveAgeValue;
- (void)setPrimitiveAgeValue:(int64_t)value_;

- (id)primitiveAgencyLeaderCode;
- (void)setPrimitiveAgencyLeaderCode:(id)value;

- (id)primitiveAgencyLeaderName;
- (void)setPrimitiveAgencyLeaderName:(id)value;

- (id)primitiveAgentName;
- (void)setPrimitiveAgentName:(id)value;

- (NSNumber*)primitiveBuCode;
- (void)setPrimitiveBuCode:(NSNumber*)value;

- (int64_t)primitiveBuCodeValue;
- (void)setPrimitiveBuCodeValue:(int64_t)value_;

- (id)primitiveBuName;
- (void)setPrimitiveBuName:(id)value;

- (NSNumber*)primitiveCandidateCode;
- (void)setPrimitiveCandidateCode:(NSNumber*)value;

- (int64_t)primitiveCandidateCodeValue;
- (void)setPrimitiveCandidateCodeValue:(int64_t)value_;

- (id)primitiveCandidateName;
- (void)setPrimitiveCandidateName:(id)value;

- (id)primitiveCcTestResult;
- (void)setPrimitiveCcTestResult:(id)value;

- (NSString*)primitiveCityCode;
- (void)setPrimitiveCityCode:(NSString*)value;

- (id)primitiveContactNumber;
- (void)setPrimitiveContactNumber:(id)value;

- (id)primitiveDistName;
- (void)setPrimitiveDistName:(id)value;

- (NSNumber*)primitiveDistrictCode;
- (void)setPrimitiveDistrictCode:(NSNumber*)value;

- (int64_t)primitiveDistrictCodeValue;
- (void)setPrimitiveDistrictCodeValue:(int64_t)value_;

- (NSDate*)primitiveDob;
- (void)setPrimitiveDob:(NSDate*)value;

- (id)primitiveDobStr;
- (void)setPrimitiveDobStr:(id)value;

- (id)primitiveFileNameList;
- (void)setPrimitiveFileNameList:(id)value;

- (id)primitiveGender;
- (void)setPrimitiveGender:(id)value;

- (NSNumber*)primitiveInterviewCode;
- (void)setPrimitiveInterviewCode:(NSNumber*)value;

- (int64_t)primitiveInterviewCodeValue;
- (void)setPrimitiveInterviewCodeValue:(int64_t)value_;

- (id)primitiveInterviewResult;
- (void)setPrimitiveInterviewResult:(id)value;

- (id)primitiveNric;
- (void)setPrimitiveNric:(id)value;

- (NSNumber*)primitiveP100;
- (void)setPrimitiveP100:(NSNumber*)value;

- (int64_t)primitiveP100Value;
- (void)setPrimitiveP100Value:(int64_t)value_;

- (id)primitivePassedStatus;
- (void)setPrimitivePassedStatus:(id)value;

- (id)primitiveRecruitmentScheme;
- (void)setPrimitiveRecruitmentScheme:(id)value;

- (id)primitiveRemarks;
- (void)setPrimitiveRemarks:(id)value;

- (id)primitiveServicingAgent;
- (void)setPrimitiveServicingAgent:(id)value;

- (id)primitiveSourceOfReferal;
- (void)setPrimitiveSourceOfReferal:(id)value;

- (NSString*)primitiveSscCode;
- (void)setPrimitiveSscCode:(NSString*)value;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (BOOL)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(BOOL)value_;

- (id)primitiveToken;
- (void)setPrimitiveToken:(id)value;

@end
