// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblAddress.m instead.

#import "_TblAddress.h"

const struct TblAddressAttributes TblAddressAttributes = {
	.addressId = @"addressId",
	.field = @"field",
	.rgCodeA = @"rgCodeA",
	.rgCodeB = @"rgCodeB",
	.rgCodeC = @"rgCodeC",
	.rgFiller1 = @"rgFiller1",
	.rgFiller2 = @"rgFiller2",
	.rgLevel = @"rgLevel",
	.rgName = @"rgName",
	.rgsName = @"rgsName",
};

@implementation TblAddressID
@end

@implementation _TblAddress

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblAddress" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblAddress";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblAddress" inManagedObjectContext:moc_];
}

- (TblAddressID*)objectID {
	return (TblAddressID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"addressIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"addressId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"fieldValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"field"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rgCodeAValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rgCodeA"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rgCodeBValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rgCodeB"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rgCodeCValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rgCodeC"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rgFiller1Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rgFiller1"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rgFiller2Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rgFiller2"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rgLevelValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rgLevel"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic addressId;

- (int64_t)addressIdValue {
	NSNumber *result = [self addressId];
	return [result longLongValue];
}

- (void)setAddressIdValue:(int64_t)value_ {
	[self setAddressId:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveAddressIdValue {
	NSNumber *result = [self primitiveAddressId];
	return [result longLongValue];
}

- (void)setPrimitiveAddressIdValue:(int64_t)value_ {
	[self setPrimitiveAddressId:[NSNumber numberWithLongLong:value_]];
}

@dynamic field;

- (int64_t)fieldValue {
	NSNumber *result = [self field];
	return [result longLongValue];
}

- (void)setFieldValue:(int64_t)value_ {
	[self setField:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveFieldValue {
	NSNumber *result = [self primitiveField];
	return [result longLongValue];
}

- (void)setPrimitiveFieldValue:(int64_t)value_ {
	[self setPrimitiveField:[NSNumber numberWithLongLong:value_]];
}

@dynamic rgCodeA;

- (int64_t)rgCodeAValue {
	NSNumber *result = [self rgCodeA];
	return [result longLongValue];
}

- (void)setRgCodeAValue:(int64_t)value_ {
	[self setRgCodeA:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRgCodeAValue {
	NSNumber *result = [self primitiveRgCodeA];
	return [result longLongValue];
}

- (void)setPrimitiveRgCodeAValue:(int64_t)value_ {
	[self setPrimitiveRgCodeA:[NSNumber numberWithLongLong:value_]];
}

@dynamic rgCodeB;

- (int64_t)rgCodeBValue {
	NSNumber *result = [self rgCodeB];
	return [result longLongValue];
}

- (void)setRgCodeBValue:(int64_t)value_ {
	[self setRgCodeB:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRgCodeBValue {
	NSNumber *result = [self primitiveRgCodeB];
	return [result longLongValue];
}

- (void)setPrimitiveRgCodeBValue:(int64_t)value_ {
	[self setPrimitiveRgCodeB:[NSNumber numberWithLongLong:value_]];
}

@dynamic rgCodeC;

- (int64_t)rgCodeCValue {
	NSNumber *result = [self rgCodeC];
	return [result longLongValue];
}

- (void)setRgCodeCValue:(int64_t)value_ {
	[self setRgCodeC:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRgCodeCValue {
	NSNumber *result = [self primitiveRgCodeC];
	return [result longLongValue];
}

- (void)setPrimitiveRgCodeCValue:(int64_t)value_ {
	[self setPrimitiveRgCodeC:[NSNumber numberWithLongLong:value_]];
}

@dynamic rgFiller1;

- (int64_t)rgFiller1Value {
	NSNumber *result = [self rgFiller1];
	return [result longLongValue];
}

- (void)setRgFiller1Value:(int64_t)value_ {
	[self setRgFiller1:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRgFiller1Value {
	NSNumber *result = [self primitiveRgFiller1];
	return [result longLongValue];
}

- (void)setPrimitiveRgFiller1Value:(int64_t)value_ {
	[self setPrimitiveRgFiller1:[NSNumber numberWithLongLong:value_]];
}

@dynamic rgFiller2;

- (int64_t)rgFiller2Value {
	NSNumber *result = [self rgFiller2];
	return [result longLongValue];
}

- (void)setRgFiller2Value:(int64_t)value_ {
	[self setRgFiller2:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRgFiller2Value {
	NSNumber *result = [self primitiveRgFiller2];
	return [result longLongValue];
}

- (void)setPrimitiveRgFiller2Value:(int64_t)value_ {
	[self setPrimitiveRgFiller2:[NSNumber numberWithLongLong:value_]];
}

@dynamic rgLevel;

- (int64_t)rgLevelValue {
	NSNumber *result = [self rgLevel];
	return [result longLongValue];
}

- (void)setRgLevelValue:(int64_t)value_ {
	[self setRgLevel:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRgLevelValue {
	NSNumber *result = [self primitiveRgLevel];
	return [result longLongValue];
}

- (void)setPrimitiveRgLevelValue:(int64_t)value_ {
	[self setPrimitiveRgLevel:[NSNumber numberWithLongLong:value_]];
}

@dynamic rgName;

@dynamic rgsName;

@end

