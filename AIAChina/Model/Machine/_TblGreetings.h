// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblGreetings.h instead.

#import <CoreData/CoreData.h>

extern const struct TblGreetingsAttributes {
	__unsafe_unretained NSString *agentId;
	__unsafe_unretained NSString *catName;
	__unsafe_unretained NSString *createdby;
	__unsafe_unretained NSString *createddate;
	__unsafe_unretained NSString *eGreetingCode;
	__unsafe_unretained NSString *eGreetingName;
	__unsafe_unretained NSString *festiveCategory;
	__unsafe_unretained NSString *fileLocation;
	__unsafe_unretained NSString *fileName;
	__unsafe_unretained NSString *modifiedby;
	__unsafe_unretained NSString *modifieddate;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *token;
} TblGreetingsAttributes;

@interface TblGreetingsID : NSManagedObjectID {}
@end

@interface _TblGreetings : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblGreetingsID* objectID;

@property (nonatomic, strong) NSString* agentId;

//- (BOOL)validateAgentId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* catName;

//- (BOOL)validateCatName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* createdby;

//- (BOOL)validateCreatedby:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* createddate;

//- (BOOL)validateCreateddate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* eGreetingCode;

@property (atomic) int16_t eGreetingCodeValue;
- (int16_t)eGreetingCodeValue;
- (void)setEGreetingCodeValue:(int16_t)value_;

//- (BOOL)validateEGreetingCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* eGreetingName;

//- (BOOL)validateEGreetingName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* festiveCategory;

@property (atomic) int16_t festiveCategoryValue;
- (int16_t)festiveCategoryValue;
- (void)setFestiveCategoryValue:(int16_t)value_;

//- (BOOL)validateFestiveCategory:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* fileLocation;

//- (BOOL)validateFileLocation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* fileName;

//- (BOOL)validateFileName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* modifiedby;

//- (BOOL)validateModifiedby:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* modifieddate;

//- (BOOL)validateModifieddate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) BOOL statusValue;
- (BOOL)statusValue;
- (void)setStatusValue:(BOOL)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* token;

//- (BOOL)validateToken:(id*)value_ error:(NSError**)error_;

@end

@interface _TblGreetings (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAgentId;
- (void)setPrimitiveAgentId:(NSString*)value;

- (NSString*)primitiveCatName;
- (void)setPrimitiveCatName:(NSString*)value;

- (NSString*)primitiveCreatedby;
- (void)setPrimitiveCreatedby:(NSString*)value;

- (NSString*)primitiveCreateddate;
- (void)setPrimitiveCreateddate:(NSString*)value;

- (NSNumber*)primitiveEGreetingCode;
- (void)setPrimitiveEGreetingCode:(NSNumber*)value;

- (int16_t)primitiveEGreetingCodeValue;
- (void)setPrimitiveEGreetingCodeValue:(int16_t)value_;

- (NSString*)primitiveEGreetingName;
- (void)setPrimitiveEGreetingName:(NSString*)value;

- (NSNumber*)primitiveFestiveCategory;
- (void)setPrimitiveFestiveCategory:(NSNumber*)value;

- (int16_t)primitiveFestiveCategoryValue;
- (void)setPrimitiveFestiveCategoryValue:(int16_t)value_;

- (NSString*)primitiveFileLocation;
- (void)setPrimitiveFileLocation:(NSString*)value;

- (NSString*)primitiveFileName;
- (void)setPrimitiveFileName:(NSString*)value;

- (NSString*)primitiveModifiedby;
- (void)setPrimitiveModifiedby:(NSString*)value;

- (NSString*)primitiveModifieddate;
- (void)setPrimitiveModifieddate:(NSString*)value;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (BOOL)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(BOOL)value_;

- (NSString*)primitiveToken;
- (void)setPrimitiveToken:(NSString*)value;

@end
