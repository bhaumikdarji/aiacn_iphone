// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblManualEntryCalendarEvents.m instead.

#import "_TblManualEntryCalendarEvents.h"

const struct TblManualEntryCalendarEventsAttributes TblManualEntryCalendarEventsAttributes = {
	.activityType = @"activityType",
	.agentId = @"agentId",
	.allDay = @"allDay",
	.candidateName = @"candidateName",
	.eDate = @"eDate",
	.endDate = @"endDate",
	.eventName = @"eventName",
	.latitude = @"latitude",
	.location = @"location",
	.longitude = @"longitude",
	.notes = @"notes",
	.reminder = @"reminder",
	.reminderMode = @"reminderMode",
	.sDate = @"sDate",
	.startDate = @"startDate",
};

@implementation TblManualEntryCalendarEventsID
@end

@implementation _TblManualEntryCalendarEvents

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblManualEntryCalendarEvents" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblManualEntryCalendarEvents";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblManualEntryCalendarEvents" inManagedObjectContext:moc_];
}

- (TblManualEntryCalendarEventsID*)objectID {
	return (TblManualEntryCalendarEventsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"allDayValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"allDay"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"latitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"latitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic activityType;

@dynamic agentId;

@dynamic allDay;

- (BOOL)allDayValue {
	NSNumber *result = [self allDay];
	return [result boolValue];
}

- (void)setAllDayValue:(BOOL)value_ {
	[self setAllDay:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAllDayValue {
	NSNumber *result = [self primitiveAllDay];
	return [result boolValue];
}

- (void)setPrimitiveAllDayValue:(BOOL)value_ {
	[self setPrimitiveAllDay:[NSNumber numberWithBool:value_]];
}

@dynamic candidateName;

@dynamic eDate;

@dynamic endDate;

@dynamic eventName;

@dynamic latitude;

- (double)latitudeValue {
	NSNumber *result = [self latitude];
	return [result doubleValue];
}

- (void)setLatitudeValue:(double)value_ {
	[self setLatitude:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLatitudeValue {
	NSNumber *result = [self primitiveLatitude];
	return [result doubleValue];
}

- (void)setPrimitiveLatitudeValue:(double)value_ {
	[self setPrimitiveLatitude:[NSNumber numberWithDouble:value_]];
}

@dynamic location;

@dynamic longitude;

- (double)longitudeValue {
	NSNumber *result = [self longitude];
	return [result doubleValue];
}

- (void)setLongitudeValue:(double)value_ {
	[self setLongitude:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLongitudeValue {
	NSNumber *result = [self primitiveLongitude];
	return [result doubleValue];
}

- (void)setPrimitiveLongitudeValue:(double)value_ {
	[self setPrimitiveLongitude:[NSNumber numberWithDouble:value_]];
}

@dynamic notes;

@dynamic reminder;

@dynamic reminderMode;

@dynamic sDate;

@dynamic startDate;

@end

