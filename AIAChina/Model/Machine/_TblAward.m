// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblAward.m instead.

#import "_TblAward.h"

const struct TblAwardAttributes TblAwardAttributes = {
	.awardDate = @"awardDate",
	.awardDesc = @"awardDesc",
	.awardImage = @"awardImage",
	.awardTitle = @"awardTitle",
	.isDelete = @"isDelete",
};

@implementation TblAwardID
@end

@implementation _TblAward

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblAward" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblAward";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblAward" inManagedObjectContext:moc_];
}

- (TblAwardID*)objectID {
	return (TblAwardID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isDeleteValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isDelete"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic awardDate;

@dynamic awardDesc;

@dynamic awardImage;

@dynamic awardTitle;

@dynamic isDelete;

- (BOOL)isDeleteValue {
	NSNumber *result = [self isDelete];
	return [result boolValue];
}

- (void)setIsDeleteValue:(BOOL)value_ {
	[self setIsDelete:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsDeleteValue {
	NSNumber *result = [self primitiveIsDelete];
	return [result boolValue];
}

- (void)setPrimitiveIsDeleteValue:(BOOL)value_ {
	[self setPrimitiveIsDelete:[NSNumber numberWithBool:value_]];
}

@end

