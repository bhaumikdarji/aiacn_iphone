// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblAddress.h instead.

#import <CoreData/CoreData.h>

extern const struct TblAddressAttributes {
	__unsafe_unretained NSString *addressId;
	__unsafe_unretained NSString *field;
	__unsafe_unretained NSString *rgCodeA;
	__unsafe_unretained NSString *rgCodeB;
	__unsafe_unretained NSString *rgCodeC;
	__unsafe_unretained NSString *rgFiller1;
	__unsafe_unretained NSString *rgFiller2;
	__unsafe_unretained NSString *rgLevel;
	__unsafe_unretained NSString *rgName;
	__unsafe_unretained NSString *rgsName;
} TblAddressAttributes;

@interface TblAddressID : NSManagedObjectID {}
@end

@interface _TblAddress : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblAddressID* objectID;

@property (nonatomic, strong) NSNumber* addressId;

@property (atomic) int64_t addressIdValue;
- (int64_t)addressIdValue;
- (void)setAddressIdValue:(int64_t)value_;

//- (BOOL)validateAddressId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* field;

@property (atomic) int64_t fieldValue;
- (int64_t)fieldValue;
- (void)setFieldValue:(int64_t)value_;

//- (BOOL)validateField:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rgCodeA;

@property (atomic) int64_t rgCodeAValue;
- (int64_t)rgCodeAValue;
- (void)setRgCodeAValue:(int64_t)value_;

//- (BOOL)validateRgCodeA:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rgCodeB;

@property (atomic) int64_t rgCodeBValue;
- (int64_t)rgCodeBValue;
- (void)setRgCodeBValue:(int64_t)value_;

//- (BOOL)validateRgCodeB:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rgCodeC;

@property (atomic) int64_t rgCodeCValue;
- (int64_t)rgCodeCValue;
- (void)setRgCodeCValue:(int64_t)value_;

//- (BOOL)validateRgCodeC:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rgFiller1;

@property (atomic) int64_t rgFiller1Value;
- (int64_t)rgFiller1Value;
- (void)setRgFiller1Value:(int64_t)value_;

//- (BOOL)validateRgFiller1:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rgFiller2;

@property (atomic) int64_t rgFiller2Value;
- (int64_t)rgFiller2Value;
- (void)setRgFiller2Value:(int64_t)value_;

//- (BOOL)validateRgFiller2:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rgLevel;

@property (atomic) int64_t rgLevelValue;
- (int64_t)rgLevelValue;
- (void)setRgLevelValue:(int64_t)value_;

//- (BOOL)validateRgLevel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* rgName;

//- (BOOL)validateRgName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* rgsName;

//- (BOOL)validateRgsName:(id*)value_ error:(NSError**)error_;

@end

@interface _TblAddress (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAddressId;
- (void)setPrimitiveAddressId:(NSNumber*)value;

- (int64_t)primitiveAddressIdValue;
- (void)setPrimitiveAddressIdValue:(int64_t)value_;

- (NSNumber*)primitiveField;
- (void)setPrimitiveField:(NSNumber*)value;

- (int64_t)primitiveFieldValue;
- (void)setPrimitiveFieldValue:(int64_t)value_;

- (NSNumber*)primitiveRgCodeA;
- (void)setPrimitiveRgCodeA:(NSNumber*)value;

- (int64_t)primitiveRgCodeAValue;
- (void)setPrimitiveRgCodeAValue:(int64_t)value_;

- (NSNumber*)primitiveRgCodeB;
- (void)setPrimitiveRgCodeB:(NSNumber*)value;

- (int64_t)primitiveRgCodeBValue;
- (void)setPrimitiveRgCodeBValue:(int64_t)value_;

- (NSNumber*)primitiveRgCodeC;
- (void)setPrimitiveRgCodeC:(NSNumber*)value;

- (int64_t)primitiveRgCodeCValue;
- (void)setPrimitiveRgCodeCValue:(int64_t)value_;

- (NSNumber*)primitiveRgFiller1;
- (void)setPrimitiveRgFiller1:(NSNumber*)value;

- (int64_t)primitiveRgFiller1Value;
- (void)setPrimitiveRgFiller1Value:(int64_t)value_;

- (NSNumber*)primitiveRgFiller2;
- (void)setPrimitiveRgFiller2:(NSNumber*)value;

- (int64_t)primitiveRgFiller2Value;
- (void)setPrimitiveRgFiller2Value:(int64_t)value_;

- (NSNumber*)primitiveRgLevel;
- (void)setPrimitiveRgLevel:(NSNumber*)value;

- (int64_t)primitiveRgLevelValue;
- (void)setPrimitiveRgLevelValue:(int64_t)value_;

- (NSString*)primitiveRgName;
- (void)setPrimitiveRgName:(NSString*)value;

- (NSString*)primitiveRgsName;
- (void)setPrimitiveRgsName:(NSString*)value;

@end
