// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblCandidateProcess.h instead.

#import <CoreData/CoreData.h>

extern const struct TblCandidateProcessAttributes {
	__unsafe_unretained NSString *abcTrainingDate;
	__unsafe_unretained NSString *abcTrainingStatus;
	__unsafe_unretained NSString *aleExamDate;
	__unsafe_unretained NSString *aleExamStatus;
	__unsafe_unretained NSString *attendEopDate;
	__unsafe_unretained NSString *attendEopStatus;
	__unsafe_unretained NSString *candidateAgentCode;
	__unsafe_unretained NSString *candidateProgress;
	__unsafe_unretained NSString *candidateRank;
	__unsafe_unretained NSString *ccTestDate;
	__unsafe_unretained NSString *ccTestRecruitment;
	__unsafe_unretained NSString *ccTestStatus;
	__unsafe_unretained NSString *companyInterviewDate;
	__unsafe_unretained NSString *companyInterviewStatus;
	__unsafe_unretained NSString *completeProgressDate;
	__unsafe_unretained NSString *contractDate;
	__unsafe_unretained NSString *contractStatus;
	__unsafe_unretained NSString *eopRegistrationDate;
	__unsafe_unretained NSString *eopRegistrationStatus;
	__unsafe_unretained NSString *firstInterviewDate;
	__unsafe_unretained NSString *firstInterviewRecruitment;
	__unsafe_unretained NSString *firstInterviewRemark;
	__unsafe_unretained NSString *firstInterviewResult;
	__unsafe_unretained NSString *firstInterviewStatus;
	__unsafe_unretained NSString *interactivePresentDate;
	__unsafe_unretained NSString *interactivePresentStatus;
	__unsafe_unretained NSString *recruitmentType;
} TblCandidateProcessAttributes;

extern const struct TblCandidateProcessRelationships {
	__unsafe_unretained NSString *contact;
} TblCandidateProcessRelationships;

@class TblContact;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblCandidateProcessID : NSManagedObjectID {}
@end

@interface _TblCandidateProcess : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblCandidateProcessID* objectID;

@property (nonatomic, strong) NSDate* abcTrainingDate;

//- (BOOL)validateAbcTrainingDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id abcTrainingStatus;

//- (BOOL)validateAbcTrainingStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* aleExamDate;

//- (BOOL)validateAleExamDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id aleExamStatus;

//- (BOOL)validateAleExamStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* attendEopDate;

//- (BOOL)validateAttendEopDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id attendEopStatus;

//- (BOOL)validateAttendEopStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id candidateAgentCode;

//- (BOOL)validateCandidateAgentCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* candidateProgress;

@property (atomic) int64_t candidateProgressValue;
- (int64_t)candidateProgressValue;
- (void)setCandidateProgressValue:(int64_t)value_;

//- (BOOL)validateCandidateProgress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* candidateRank;

@property (atomic) int64_t candidateRankValue;
- (int64_t)candidateRankValue;
- (void)setCandidateRankValue:(int64_t)value_;

//- (BOOL)validateCandidateRank:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* ccTestDate;

//- (BOOL)validateCcTestDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id ccTestRecruitment;

//- (BOOL)validateCcTestRecruitment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id ccTestStatus;

//- (BOOL)validateCcTestStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* companyInterviewDate;

//- (BOOL)validateCompanyInterviewDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id companyInterviewStatus;

//- (BOOL)validateCompanyInterviewStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* completeProgressDate;

//- (BOOL)validateCompleteProgressDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* contractDate;

//- (BOOL)validateContractDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id contractStatus;

//- (BOOL)validateContractStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* eopRegistrationDate;

//- (BOOL)validateEopRegistrationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id eopRegistrationStatus;

//- (BOOL)validateEopRegistrationStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* firstInterviewDate;

//- (BOOL)validateFirstInterviewDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id firstInterviewRecruitment;

//- (BOOL)validateFirstInterviewRecruitment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id firstInterviewRemark;

//- (BOOL)validateFirstInterviewRemark:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id firstInterviewResult;

//- (BOOL)validateFirstInterviewResult:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id firstInterviewStatus;

//- (BOOL)validateFirstInterviewStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* interactivePresentDate;

//- (BOOL)validateInteractivePresentDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id interactivePresentStatus;

//- (BOOL)validateInteractivePresentStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id recruitmentType;

//- (BOOL)validateRecruitmentType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblContact *contact;

//- (BOOL)validateContact:(id*)value_ error:(NSError**)error_;

@end

@interface _TblCandidateProcess (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveAbcTrainingDate;
- (void)setPrimitiveAbcTrainingDate:(NSDate*)value;

- (id)primitiveAbcTrainingStatus;
- (void)setPrimitiveAbcTrainingStatus:(id)value;

- (NSDate*)primitiveAleExamDate;
- (void)setPrimitiveAleExamDate:(NSDate*)value;

- (id)primitiveAleExamStatus;
- (void)setPrimitiveAleExamStatus:(id)value;

- (NSDate*)primitiveAttendEopDate;
- (void)setPrimitiveAttendEopDate:(NSDate*)value;

- (id)primitiveAttendEopStatus;
- (void)setPrimitiveAttendEopStatus:(id)value;

- (id)primitiveCandidateAgentCode;
- (void)setPrimitiveCandidateAgentCode:(id)value;

- (NSNumber*)primitiveCandidateProgress;
- (void)setPrimitiveCandidateProgress:(NSNumber*)value;

- (int64_t)primitiveCandidateProgressValue;
- (void)setPrimitiveCandidateProgressValue:(int64_t)value_;

- (NSNumber*)primitiveCandidateRank;
- (void)setPrimitiveCandidateRank:(NSNumber*)value;

- (int64_t)primitiveCandidateRankValue;
- (void)setPrimitiveCandidateRankValue:(int64_t)value_;

- (NSDate*)primitiveCcTestDate;
- (void)setPrimitiveCcTestDate:(NSDate*)value;

- (id)primitiveCcTestRecruitment;
- (void)setPrimitiveCcTestRecruitment:(id)value;

- (id)primitiveCcTestStatus;
- (void)setPrimitiveCcTestStatus:(id)value;

- (NSDate*)primitiveCompanyInterviewDate;
- (void)setPrimitiveCompanyInterviewDate:(NSDate*)value;

- (id)primitiveCompanyInterviewStatus;
- (void)setPrimitiveCompanyInterviewStatus:(id)value;

- (NSDate*)primitiveCompleteProgressDate;
- (void)setPrimitiveCompleteProgressDate:(NSDate*)value;

- (NSDate*)primitiveContractDate;
- (void)setPrimitiveContractDate:(NSDate*)value;

- (id)primitiveContractStatus;
- (void)setPrimitiveContractStatus:(id)value;

- (NSDate*)primitiveEopRegistrationDate;
- (void)setPrimitiveEopRegistrationDate:(NSDate*)value;

- (id)primitiveEopRegistrationStatus;
- (void)setPrimitiveEopRegistrationStatus:(id)value;

- (NSDate*)primitiveFirstInterviewDate;
- (void)setPrimitiveFirstInterviewDate:(NSDate*)value;

- (id)primitiveFirstInterviewRecruitment;
- (void)setPrimitiveFirstInterviewRecruitment:(id)value;

- (id)primitiveFirstInterviewRemark;
- (void)setPrimitiveFirstInterviewRemark:(id)value;

- (id)primitiveFirstInterviewResult;
- (void)setPrimitiveFirstInterviewResult:(id)value;

- (id)primitiveFirstInterviewStatus;
- (void)setPrimitiveFirstInterviewStatus:(id)value;

- (NSDate*)primitiveInteractivePresentDate;
- (void)setPrimitiveInteractivePresentDate:(NSDate*)value;

- (id)primitiveInteractivePresentStatus;
- (void)setPrimitiveInteractivePresentStatus:(id)value;

- (id)primitiveRecruitmentType;
- (void)setPrimitiveRecruitmentType:(id)value;

- (TblContact*)primitiveContact;
- (void)setPrimitiveContact:(TblContact*)value;

@end
