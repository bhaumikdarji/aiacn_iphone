// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblExperience.m instead.

#import "_TblExperience.h"

const struct TblExperienceAttributes TblExperienceAttributes = {
	.endDate = @"endDate",
	.income = @"income",
	.iosAddressCode = @"iosAddressCode",
	.occupation = @"occupation",
	.position = @"position",
	.startDate = @"startDate",
	.unit = @"unit",
	.witness = @"witness",
	.witnessContactNo = @"witnessContactNo",
};

const struct TblExperienceRelationships TblExperienceRelationships = {
	.contact = @"contact",
};

@implementation TblExperienceID
@end

@implementation _TblExperience

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblExperience" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblExperience";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblExperience" inManagedObjectContext:moc_];
}

- (TblExperienceID*)objectID {
	return (TblExperienceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic endDate;

@dynamic income;

@dynamic iosAddressCode;

@dynamic occupation;

@dynamic position;

@dynamic startDate;

@dynamic unit;

@dynamic witness;

@dynamic witnessContactNo;

@dynamic contact;

@end

