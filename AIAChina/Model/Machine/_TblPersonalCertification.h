// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblPersonalCertification.h instead.

#import <CoreData/CoreData.h>

extern const struct TblPersonalCertificationAttributes {
	__unsafe_unretained NSString *certificateName;
	__unsafe_unretained NSString *charterAgency;
	__unsafe_unretained NSString *charterDate;
	__unsafe_unretained NSString *iosAddressCode;
} TblPersonalCertificationAttributes;

extern const struct TblPersonalCertificationRelationships {
	__unsafe_unretained NSString *contact;
} TblPersonalCertificationRelationships;

@class TblContact;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblPersonalCertificationID : NSManagedObjectID {}
@end

@interface _TblPersonalCertification : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblPersonalCertificationID* objectID;

@property (nonatomic, strong) id certificateName;

//- (BOOL)validateCertificateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id charterAgency;

//- (BOOL)validateCharterAgency:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id charterDate;

//- (BOOL)validateCharterDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id iosAddressCode;

//- (BOOL)validateIosAddressCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblContact *contact;

//- (BOOL)validateContact:(id*)value_ error:(NSError**)error_;

@end

@interface _TblPersonalCertification (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveCertificateName;
- (void)setPrimitiveCertificateName:(id)value;

- (id)primitiveCharterAgency;
- (void)setPrimitiveCharterAgency:(id)value;

- (id)primitiveCharterDate;
- (void)setPrimitiveCharterDate:(id)value;

- (id)primitiveIosAddressCode;
- (void)setPrimitiveIosAddressCode:(id)value;

- (TblContact*)primitiveContact;
- (void)setPrimitiveContact:(TblContact*)value;

@end
