// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblEducation.h instead.

#import <CoreData/CoreData.h>

extern const struct TblEducationAttributes {
	__unsafe_unretained NSString *education;
	__unsafe_unretained NSString *educationLevel;
	__unsafe_unretained NSString *endDate;
	__unsafe_unretained NSString *iosAddressCode;
	__unsafe_unretained NSString *school;
	__unsafe_unretained NSString *startDate;
	__unsafe_unretained NSString *witness;
	__unsafe_unretained NSString *witnessContactNo;
} TblEducationAttributes;

extern const struct TblEducationRelationships {
	__unsafe_unretained NSString *contact;
} TblEducationRelationships;

@class TblContact;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblEducationID : NSManagedObjectID {}
@end

@interface _TblEducation : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblEducationID* objectID;

@property (nonatomic, strong) id education;

//- (BOOL)validateEducation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id educationLevel;

//- (BOOL)validateEducationLevel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id endDate;

//- (BOOL)validateEndDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id iosAddressCode;

//- (BOOL)validateIosAddressCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id school;

//- (BOOL)validateSchool:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id startDate;

//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id witness;

//- (BOOL)validateWitness:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id witnessContactNo;

//- (BOOL)validateWitnessContactNo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblContact *contact;

//- (BOOL)validateContact:(id*)value_ error:(NSError**)error_;

@end

@interface _TblEducation (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveEducation;
- (void)setPrimitiveEducation:(id)value;

- (id)primitiveEducationLevel;
- (void)setPrimitiveEducationLevel:(id)value;

- (id)primitiveEndDate;
- (void)setPrimitiveEndDate:(id)value;

- (id)primitiveIosAddressCode;
- (void)setPrimitiveIosAddressCode:(id)value;

- (id)primitiveSchool;
- (void)setPrimitiveSchool:(id)value;

- (id)primitiveStartDate;
- (void)setPrimitiveStartDate:(id)value;

- (id)primitiveWitness;
- (void)setPrimitiveWitness:(id)value;

- (id)primitiveWitnessContactNo;
- (void)setPrimitiveWitnessContactNo:(id)value;

- (TblContact*)primitiveContact;
- (void)setPrimitiveContact:(TblContact*)value;

@end
