// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblTraninigEvent.m instead.

#import "_TblTraninigEvent.h"

const struct TblTraninigEventAttributes TblTraninigEventAttributes = {
	.courseCode = @"courseCode",
	.courseName = @"courseName",
	.courseType = @"courseType",
	.creationDate = @"creationDate",
	.curriculamCode = @"curriculamCode",
	.detailCode = @"detailCode",
	.startDate = @"startDate",
};

const struct TblTraninigEventRelationships TblTraninigEventRelationships = {
	.calendarEvent = @"calendarEvent",
};

@implementation TblTraninigEventID
@end

@implementation _TblTraninigEvent

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblTraninigEvent" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblTraninigEvent";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblTraninigEvent" inManagedObjectContext:moc_];
}

- (TblTraninigEventID*)objectID {
	return (TblTraninigEventID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"detailCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"detailCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic courseCode;

@dynamic courseName;

@dynamic courseType;

@dynamic creationDate;

@dynamic curriculamCode;

@dynamic detailCode;

- (int16_t)detailCodeValue {
	NSNumber *result = [self detailCode];
	return [result shortValue];
}

- (void)setDetailCodeValue:(int16_t)value_ {
	[self setDetailCode:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveDetailCodeValue {
	NSNumber *result = [self primitiveDetailCode];
	return [result shortValue];
}

- (void)setPrimitiveDetailCodeValue:(int16_t)value_ {
	[self setPrimitiveDetailCode:[NSNumber numberWithShort:value_]];
}

@dynamic startDate;

@dynamic calendarEvent;

@end

