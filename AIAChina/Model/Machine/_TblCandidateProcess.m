// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblCandidateProcess.m instead.

#import "_TblCandidateProcess.h"

const struct TblCandidateProcessAttributes TblCandidateProcessAttributes = {
	.abcTrainingDate = @"abcTrainingDate",
	.abcTrainingStatus = @"abcTrainingStatus",
	.aleExamDate = @"aleExamDate",
	.aleExamStatus = @"aleExamStatus",
	.attendEopDate = @"attendEopDate",
	.attendEopStatus = @"attendEopStatus",
	.candidateAgentCode = @"candidateAgentCode",
	.candidateProgress = @"candidateProgress",
	.candidateRank = @"candidateRank",
	.ccTestDate = @"ccTestDate",
	.ccTestRecruitment = @"ccTestRecruitment",
	.ccTestStatus = @"ccTestStatus",
	.companyInterviewDate = @"companyInterviewDate",
	.companyInterviewStatus = @"companyInterviewStatus",
	.completeProgressDate = @"completeProgressDate",
	.contractDate = @"contractDate",
	.contractStatus = @"contractStatus",
	.eopRegistrationDate = @"eopRegistrationDate",
	.eopRegistrationStatus = @"eopRegistrationStatus",
	.firstInterviewDate = @"firstInterviewDate",
	.firstInterviewRecruitment = @"firstInterviewRecruitment",
	.firstInterviewRemark = @"firstInterviewRemark",
	.firstInterviewResult = @"firstInterviewResult",
	.firstInterviewStatus = @"firstInterviewStatus",
	.interactivePresentDate = @"interactivePresentDate",
	.interactivePresentStatus = @"interactivePresentStatus",
	.recruitmentType = @"recruitmentType",
};

const struct TblCandidateProcessRelationships TblCandidateProcessRelationships = {
	.contact = @"contact",
};

@implementation TblCandidateProcessID
@end

@implementation _TblCandidateProcess

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblCandidateProcess" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblCandidateProcess";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblCandidateProcess" inManagedObjectContext:moc_];
}

- (TblCandidateProcessID*)objectID {
	return (TblCandidateProcessID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"candidateProgressValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"candidateProgress"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"candidateRankValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"candidateRank"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic abcTrainingDate;

@dynamic abcTrainingStatus;

@dynamic aleExamDate;

@dynamic aleExamStatus;

@dynamic attendEopDate;

@dynamic attendEopStatus;

@dynamic candidateAgentCode;

@dynamic candidateProgress;

- (int64_t)candidateProgressValue {
	NSNumber *result = [self candidateProgress];
	return [result longLongValue];
}

- (void)setCandidateProgressValue:(int64_t)value_ {
	[self setCandidateProgress:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveCandidateProgressValue {
	NSNumber *result = [self primitiveCandidateProgress];
	return [result longLongValue];
}

- (void)setPrimitiveCandidateProgressValue:(int64_t)value_ {
	[self setPrimitiveCandidateProgress:[NSNumber numberWithLongLong:value_]];
}

@dynamic candidateRank;

- (int64_t)candidateRankValue {
	NSNumber *result = [self candidateRank];
	return [result longLongValue];
}

- (void)setCandidateRankValue:(int64_t)value_ {
	[self setCandidateRank:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveCandidateRankValue {
	NSNumber *result = [self primitiveCandidateRank];
	return [result longLongValue];
}

- (void)setPrimitiveCandidateRankValue:(int64_t)value_ {
	[self setPrimitiveCandidateRank:[NSNumber numberWithLongLong:value_]];
}

@dynamic ccTestDate;

@dynamic ccTestRecruitment;

@dynamic ccTestStatus;

@dynamic companyInterviewDate;

@dynamic companyInterviewStatus;

@dynamic completeProgressDate;

@dynamic contractDate;

@dynamic contractStatus;

@dynamic eopRegistrationDate;

@dynamic eopRegistrationStatus;

@dynamic firstInterviewDate;

@dynamic firstInterviewRecruitment;

@dynamic firstInterviewRemark;

@dynamic firstInterviewResult;

@dynamic firstInterviewStatus;

@dynamic interactivePresentDate;

@dynamic interactivePresentStatus;

@dynamic recruitmentType;

@dynamic contact;

@end

