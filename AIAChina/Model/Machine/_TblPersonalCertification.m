// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblPersonalCertification.m instead.

#import "_TblPersonalCertification.h"

const struct TblPersonalCertificationAttributes TblPersonalCertificationAttributes = {
	.certificateName = @"certificateName",
	.charterAgency = @"charterAgency",
	.charterDate = @"charterDate",
	.iosAddressCode = @"iosAddressCode",
};

const struct TblPersonalCertificationRelationships TblPersonalCertificationRelationships = {
	.contact = @"contact",
};

@implementation TblPersonalCertificationID
@end

@implementation _TblPersonalCertification

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblPersonalCertification" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblPersonalCertification";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblPersonalCertification" inManagedObjectContext:moc_];
}

- (TblPersonalCertificationID*)objectID {
	return (TblPersonalCertificationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic certificateName;

@dynamic charterAgency;

@dynamic charterDate;

@dynamic iosAddressCode;

@dynamic contact;

@end

