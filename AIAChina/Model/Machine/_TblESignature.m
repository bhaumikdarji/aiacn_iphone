// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblESignature.m instead.

#import "_TblESignature.h"

const struct TblESignatureAttributes TblESignatureAttributes = {
	.agentId = @"agentId",
	.applicationDate = @"applicationDate",
	.atachedWithInsuranceCo = @"atachedWithInsuranceCo",
	.branch = @"branch",
	.candidateName = @"candidateName",
	.city = @"city",
	.contactWithAia = @"contactWithAia",
	.eSignaturePhoto = @"eSignaturePhoto",
	.iosAddressCode = @"iosAddressCode",
	.serviceDepartment = @"serviceDepartment",
	.takenPspTest = @"takenPspTest",
};

const struct TblESignatureRelationships TblESignatureRelationships = {
	.contact = @"contact",
};

@implementation TblESignatureID
@end

@implementation _TblESignature

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblESignature" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblESignature";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblESignature" inManagedObjectContext:moc_];
}

- (TblESignatureID*)objectID {
	return (TblESignatureID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"atachedWithInsuranceCoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"atachedWithInsuranceCo"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"contactWithAiaValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"contactWithAia"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"takenPspTestValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"takenPspTest"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic agentId;

@dynamic applicationDate;

@dynamic atachedWithInsuranceCo;

- (BOOL)atachedWithInsuranceCoValue {
	NSNumber *result = [self atachedWithInsuranceCo];
	return [result boolValue];
}

- (void)setAtachedWithInsuranceCoValue:(BOOL)value_ {
	[self setAtachedWithInsuranceCo:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAtachedWithInsuranceCoValue {
	NSNumber *result = [self primitiveAtachedWithInsuranceCo];
	return [result boolValue];
}

- (void)setPrimitiveAtachedWithInsuranceCoValue:(BOOL)value_ {
	[self setPrimitiveAtachedWithInsuranceCo:[NSNumber numberWithBool:value_]];
}

@dynamic branch;

@dynamic candidateName;

@dynamic city;

@dynamic contactWithAia;

- (BOOL)contactWithAiaValue {
	NSNumber *result = [self contactWithAia];
	return [result boolValue];
}

- (void)setContactWithAiaValue:(BOOL)value_ {
	[self setContactWithAia:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveContactWithAiaValue {
	NSNumber *result = [self primitiveContactWithAia];
	return [result boolValue];
}

- (void)setPrimitiveContactWithAiaValue:(BOOL)value_ {
	[self setPrimitiveContactWithAia:[NSNumber numberWithBool:value_]];
}

@dynamic eSignaturePhoto;

@dynamic iosAddressCode;

@dynamic serviceDepartment;

@dynamic takenPspTest;

- (BOOL)takenPspTestValue {
	NSNumber *result = [self takenPspTest];
	return [result boolValue];
}

- (void)setTakenPspTestValue:(BOOL)value_ {
	[self setTakenPspTest:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveTakenPspTestValue {
	NSNumber *result = [self primitiveTakenPspTest];
	return [result boolValue];
}

- (void)setPrimitiveTakenPspTestValue:(BOOL)value_ {
	[self setPrimitiveTakenPspTest:[NSNumber numberWithBool:value_]];
}

@dynamic contact;

@end

