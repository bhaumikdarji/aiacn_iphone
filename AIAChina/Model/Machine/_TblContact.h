// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblContact.h instead.

#import <CoreData/CoreData.h>

extern const struct TblContactAttributes {
	__unsafe_unretained NSString *addressCode;
	__unsafe_unretained NSString *age;
	__unsafe_unretained NSString *agentId;
	__unsafe_unretained NSString *attenededEOPCount;
	__unsafe_unretained NSString *birthDate;
	__unsafe_unretained NSString *birthMonthDay;
	__unsafe_unretained NSString *birthPlace;
	__unsafe_unretained NSString *birthYear;
	__unsafe_unretained NSString *candidatePhoto;
	__unsafe_unretained NSString *createdBy;
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *eMailId;
	__unsafe_unretained NSString *education;
	__unsafe_unretained NSString *fixedLineNO;
	__unsafe_unretained NSString *gender;
	__unsafe_unretained NSString *groupName;
	__unsafe_unretained NSString *idType;
	__unsafe_unretained NSString *identityType;
	__unsafe_unretained NSString *iosAddressCode;
	__unsafe_unretained NSString *isDelete;
	__unsafe_unretained NSString *isSync;
	__unsafe_unretained NSString *lastContactedDate;
	__unsafe_unretained NSString *marritalStatus;
	__unsafe_unretained NSString *mobilePhoneNo;
	__unsafe_unretained NSString *modificationDate;
	__unsafe_unretained NSString *modifiedBy;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *natureOfBusiness;
	__unsafe_unretained NSString *newcomerSource;
	__unsafe_unretained NSString *nric;
	__unsafe_unretained NSString *officePhoneNo;
	__unsafe_unretained NSString *outlookApperance;
	__unsafe_unretained NSString *presentWorkCondition;
	__unsafe_unretained NSString *presenterReport;
	__unsafe_unretained NSString *presenterReportDate;
	__unsafe_unretained NSString *purchasedAnyInsurance;
	__unsafe_unretained NSString *qq;
	__unsafe_unretained NSString *race;
	__unsafe_unretained NSString *reJoin;
	__unsafe_unretained NSString *recommendedRecruitmentScheme;
	__unsafe_unretained NSString *recruitedBy;
	__unsafe_unretained NSString *recruitmentProgressStatus;
	__unsafe_unretained NSString *referalSource;
	__unsafe_unretained NSString *registeredAddress;
	__unsafe_unretained NSString *registeredAddress1;
	__unsafe_unretained NSString *registeredAddress2;
	__unsafe_unretained NSString *registeredPostalCode;
	__unsafe_unretained NSString *remarks;
	__unsafe_unretained NSString *residentialAddress;
	__unsafe_unretained NSString *residentialAddress1;
	__unsafe_unretained NSString *residentialAddress2;
	__unsafe_unretained NSString *residentialPostalCode;
	__unsafe_unretained NSString *salesExperience;
	__unsafe_unretained NSString *sourceComerNew;
	__unsafe_unretained NSString *talentProfile;
	__unsafe_unretained NSString *weChat;
	__unsafe_unretained NSString *weibo;
	__unsafe_unretained NSString *workingYearExperience;
	__unsafe_unretained NSString *yearlyIncome;
} TblContactAttributes;

extern const struct TblContactRelationships {
	__unsafe_unretained NSString *candidateESignatures;
	__unsafe_unretained NSString *candidateEducations;
	__unsafe_unretained NSString *candidateFamilyInfos;
	__unsafe_unretained NSString *candidateNotes;
	__unsafe_unretained NSString *candidateProcess;
	__unsafe_unretained NSString *candidateProfessionalCertifications;
	__unsafe_unretained NSString *candidateWorkExperiences;
	__unsafe_unretained NSString *group;
} TblContactRelationships;

@class TblESignature;
@class TblEducation;
@class TblFamily;
@class TblNotes;
@class TblCandidateProcess;
@class TblPersonalCertification;
@class TblExperience;
@class TblGroup;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblContactID : NSManagedObjectID {}
@end

@interface _TblContact : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblContactID* objectID;

@property (nonatomic, strong) NSNumber* addressCode;

@property (atomic) int64_t addressCodeValue;
- (int64_t)addressCodeValue;
- (void)setAddressCodeValue:(int64_t)value_;

//- (BOOL)validateAddressCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* age;

@property (atomic) int64_t ageValue;
- (int64_t)ageValue;
- (void)setAgeValue:(int64_t)value_;

//- (BOOL)validateAge:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agentId;

//- (BOOL)validateAgentId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* attenededEOPCount;

@property (atomic) int64_t attenededEOPCountValue;
- (int64_t)attenededEOPCountValue;
- (void)setAttenededEOPCountValue:(int64_t)value_;

//- (BOOL)validateAttenededEOPCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* birthDate;

//- (BOOL)validateBirthDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id birthMonthDay;

//- (BOOL)validateBirthMonthDay:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id birthPlace;

//- (BOOL)validateBirthPlace:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* birthYear;

@property (atomic) int64_t birthYearValue;
- (int64_t)birthYearValue;
- (void)setBirthYearValue:(int64_t)value_;

//- (BOOL)validateBirthYear:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id candidatePhoto;

//- (BOOL)validateCandidatePhoto:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id createdBy;

//- (BOOL)validateCreatedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id eMailId;

//- (BOOL)validateEMailId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id education;

//- (BOOL)validateEducation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id fixedLineNO;

//- (BOOL)validateFixedLineNO:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id gender;

//- (BOOL)validateGender:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id groupName;

//- (BOOL)validateGroupName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id idType;

//- (BOOL)validateIdType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id identityType;

//- (BOOL)validateIdentityType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id iosAddressCode;

//- (BOOL)validateIosAddressCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isDelete;

@property (atomic) BOOL isDeleteValue;
- (BOOL)isDeleteValue;
- (void)setIsDeleteValue:(BOOL)value_;

//- (BOOL)validateIsDelete:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isSync;

@property (atomic) BOOL isSyncValue;
- (BOOL)isSyncValue;
- (void)setIsSyncValue:(BOOL)value_;

//- (BOOL)validateIsSync:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* lastContactedDate;

//- (BOOL)validateLastContactedDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id marritalStatus;

//- (BOOL)validateMarritalStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id mobilePhoneNo;

//- (BOOL)validateMobilePhoneNo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* modificationDate;

//- (BOOL)validateModificationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id modifiedBy;

//- (BOOL)validateModifiedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id natureOfBusiness;

//- (BOOL)validateNatureOfBusiness:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id newcomerSource;

//- (BOOL)validateNewcomerSource:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id nric;

//- (BOOL)validateNric:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id officePhoneNo;

//- (BOOL)validateOfficePhoneNo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id outlookApperance;

//- (BOOL)validateOutlookApperance:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id presentWorkCondition;

//- (BOOL)validatePresentWorkCondition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id presenterReport;

//- (BOOL)validatePresenterReport:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* presenterReportDate;

//- (BOOL)validatePresenterReportDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id purchasedAnyInsurance;

//- (BOOL)validatePurchasedAnyInsurance:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id qq;

//- (BOOL)validateQq:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id race;

//- (BOOL)validateRace:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id reJoin;

//- (BOOL)validateReJoin:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id recommendedRecruitmentScheme;

//- (BOOL)validateRecommendedRecruitmentScheme:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id recruitedBy;

//- (BOOL)validateRecruitedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id recruitmentProgressStatus;

//- (BOOL)validateRecruitmentProgressStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id referalSource;

//- (BOOL)validateReferalSource:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id registeredAddress;

//- (BOOL)validateRegisteredAddress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id registeredAddress1;

//- (BOOL)validateRegisteredAddress1:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id registeredAddress2;

//- (BOOL)validateRegisteredAddress2:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id registeredPostalCode;

//- (BOOL)validateRegisteredPostalCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id remarks;

//- (BOOL)validateRemarks:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id residentialAddress;

//- (BOOL)validateResidentialAddress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id residentialAddress1;

//- (BOOL)validateResidentialAddress1:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id residentialAddress2;

//- (BOOL)validateResidentialAddress2:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id residentialPostalCode;

//- (BOOL)validateResidentialPostalCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id salesExperience;

//- (BOOL)validateSalesExperience:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id sourceComerNew;

//- (BOOL)validateSourceComerNew:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id talentProfile;

//- (BOOL)validateTalentProfile:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id weChat;

//- (BOOL)validateWeChat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id weibo;

//- (BOOL)validateWeibo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id workingYearExperience;

//- (BOOL)validateWorkingYearExperience:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id yearlyIncome;

//- (BOOL)validateYearlyIncome:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *candidateESignatures;

- (NSMutableSet*)candidateESignaturesSet;

@property (nonatomic, strong) NSSet *candidateEducations;

- (NSMutableSet*)candidateEducationsSet;

@property (nonatomic, strong) NSSet *candidateFamilyInfos;

- (NSMutableSet*)candidateFamilyInfosSet;

@property (nonatomic, strong) NSSet *candidateNotes;

- (NSMutableSet*)candidateNotesSet;

@property (nonatomic, strong) TblCandidateProcess *candidateProcess;

//- (BOOL)validateCandidateProcess:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *candidateProfessionalCertifications;

- (NSMutableSet*)candidateProfessionalCertificationsSet;

@property (nonatomic, strong) NSSet *candidateWorkExperiences;

- (NSMutableSet*)candidateWorkExperiencesSet;

@property (nonatomic, strong) NSSet *group;

- (NSMutableSet*)groupSet;

@end

@interface _TblContact (CandidateESignaturesCoreDataGeneratedAccessors)
- (void)addCandidateESignatures:(NSSet*)value_;
- (void)removeCandidateESignatures:(NSSet*)value_;
- (void)addCandidateESignaturesObject:(TblESignature*)value_;
- (void)removeCandidateESignaturesObject:(TblESignature*)value_;

@end

@interface _TblContact (CandidateEducationsCoreDataGeneratedAccessors)
- (void)addCandidateEducations:(NSSet*)value_;
- (void)removeCandidateEducations:(NSSet*)value_;
- (void)addCandidateEducationsObject:(TblEducation*)value_;
- (void)removeCandidateEducationsObject:(TblEducation*)value_;

@end

@interface _TblContact (CandidateFamilyInfosCoreDataGeneratedAccessors)
- (void)addCandidateFamilyInfos:(NSSet*)value_;
- (void)removeCandidateFamilyInfos:(NSSet*)value_;
- (void)addCandidateFamilyInfosObject:(TblFamily*)value_;
- (void)removeCandidateFamilyInfosObject:(TblFamily*)value_;

@end

@interface _TblContact (CandidateNotesCoreDataGeneratedAccessors)
- (void)addCandidateNotes:(NSSet*)value_;
- (void)removeCandidateNotes:(NSSet*)value_;
- (void)addCandidateNotesObject:(TblNotes*)value_;
- (void)removeCandidateNotesObject:(TblNotes*)value_;

@end

@interface _TblContact (CandidateProfessionalCertificationsCoreDataGeneratedAccessors)
- (void)addCandidateProfessionalCertifications:(NSSet*)value_;
- (void)removeCandidateProfessionalCertifications:(NSSet*)value_;
- (void)addCandidateProfessionalCertificationsObject:(TblPersonalCertification*)value_;
- (void)removeCandidateProfessionalCertificationsObject:(TblPersonalCertification*)value_;

@end

@interface _TblContact (CandidateWorkExperiencesCoreDataGeneratedAccessors)
- (void)addCandidateWorkExperiences:(NSSet*)value_;
- (void)removeCandidateWorkExperiences:(NSSet*)value_;
- (void)addCandidateWorkExperiencesObject:(TblExperience*)value_;
- (void)removeCandidateWorkExperiencesObject:(TblExperience*)value_;

@end

@interface _TblContact (GroupCoreDataGeneratedAccessors)
- (void)addGroup:(NSSet*)value_;
- (void)removeGroup:(NSSet*)value_;
- (void)addGroupObject:(TblGroup*)value_;
- (void)removeGroupObject:(TblGroup*)value_;

@end

@interface _TblContact (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAddressCode;
- (void)setPrimitiveAddressCode:(NSNumber*)value;

- (int64_t)primitiveAddressCodeValue;
- (void)setPrimitiveAddressCodeValue:(int64_t)value_;

- (NSNumber*)primitiveAge;
- (void)setPrimitiveAge:(NSNumber*)value;

- (int64_t)primitiveAgeValue;
- (void)setPrimitiveAgeValue:(int64_t)value_;

- (id)primitiveAgentId;
- (void)setPrimitiveAgentId:(id)value;

- (NSNumber*)primitiveAttenededEOPCount;
- (void)setPrimitiveAttenededEOPCount:(NSNumber*)value;

- (int64_t)primitiveAttenededEOPCountValue;
- (void)setPrimitiveAttenededEOPCountValue:(int64_t)value_;

- (NSDate*)primitiveBirthDate;
- (void)setPrimitiveBirthDate:(NSDate*)value;

- (id)primitiveBirthMonthDay;
- (void)setPrimitiveBirthMonthDay:(id)value;

- (id)primitiveBirthPlace;
- (void)setPrimitiveBirthPlace:(id)value;

- (NSNumber*)primitiveBirthYear;
- (void)setPrimitiveBirthYear:(NSNumber*)value;

- (int64_t)primitiveBirthYearValue;
- (void)setPrimitiveBirthYearValue:(int64_t)value_;

- (id)primitiveCandidatePhoto;
- (void)setPrimitiveCandidatePhoto:(id)value;

- (id)primitiveCreatedBy;
- (void)setPrimitiveCreatedBy:(id)value;

- (id)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(id)value;

- (id)primitiveEMailId;
- (void)setPrimitiveEMailId:(id)value;

- (id)primitiveEducation;
- (void)setPrimitiveEducation:(id)value;

- (id)primitiveFixedLineNO;
- (void)setPrimitiveFixedLineNO:(id)value;

- (id)primitiveGender;
- (void)setPrimitiveGender:(id)value;

- (id)primitiveGroupName;
- (void)setPrimitiveGroupName:(id)value;

- (id)primitiveIdType;
- (void)setPrimitiveIdType:(id)value;

- (id)primitiveIdentityType;
- (void)setPrimitiveIdentityType:(id)value;

- (id)primitiveIosAddressCode;
- (void)setPrimitiveIosAddressCode:(id)value;

- (NSNumber*)primitiveIsDelete;
- (void)setPrimitiveIsDelete:(NSNumber*)value;

- (BOOL)primitiveIsDeleteValue;
- (void)setPrimitiveIsDeleteValue:(BOOL)value_;

- (NSNumber*)primitiveIsSync;
- (void)setPrimitiveIsSync:(NSNumber*)value;

- (BOOL)primitiveIsSyncValue;
- (void)setPrimitiveIsSyncValue:(BOOL)value_;

- (NSDate*)primitiveLastContactedDate;
- (void)setPrimitiveLastContactedDate:(NSDate*)value;

- (id)primitiveMarritalStatus;
- (void)setPrimitiveMarritalStatus:(id)value;

- (id)primitiveMobilePhoneNo;
- (void)setPrimitiveMobilePhoneNo:(id)value;

- (NSDate*)primitiveModificationDate;
- (void)setPrimitiveModificationDate:(NSDate*)value;

- (id)primitiveModifiedBy;
- (void)setPrimitiveModifiedBy:(id)value;

- (id)primitiveName;
- (void)setPrimitiveName:(id)value;

- (id)primitiveNatureOfBusiness;
- (void)setPrimitiveNatureOfBusiness:(id)value;

- (id)primitiveNewcomerSource;
- (void)setPrimitiveNewcomerSource:(id)value;

- (id)primitiveNric;
- (void)setPrimitiveNric:(id)value;

- (id)primitiveOfficePhoneNo;
- (void)setPrimitiveOfficePhoneNo:(id)value;

- (id)primitiveOutlookApperance;
- (void)setPrimitiveOutlookApperance:(id)value;

- (id)primitivePresentWorkCondition;
- (void)setPrimitivePresentWorkCondition:(id)value;

- (id)primitivePresenterReport;
- (void)setPrimitivePresenterReport:(id)value;

- (NSDate*)primitivePresenterReportDate;
- (void)setPrimitivePresenterReportDate:(NSDate*)value;

- (id)primitivePurchasedAnyInsurance;
- (void)setPrimitivePurchasedAnyInsurance:(id)value;

- (id)primitiveQq;
- (void)setPrimitiveQq:(id)value;

- (id)primitiveRace;
- (void)setPrimitiveRace:(id)value;

- (id)primitiveReJoin;
- (void)setPrimitiveReJoin:(id)value;

- (id)primitiveRecommendedRecruitmentScheme;
- (void)setPrimitiveRecommendedRecruitmentScheme:(id)value;

- (id)primitiveRecruitedBy;
- (void)setPrimitiveRecruitedBy:(id)value;

- (id)primitiveRecruitmentProgressStatus;
- (void)setPrimitiveRecruitmentProgressStatus:(id)value;

- (id)primitiveReferalSource;
- (void)setPrimitiveReferalSource:(id)value;

- (id)primitiveRegisteredAddress;
- (void)setPrimitiveRegisteredAddress:(id)value;

- (id)primitiveRegisteredAddress1;
- (void)setPrimitiveRegisteredAddress1:(id)value;

- (id)primitiveRegisteredAddress2;
- (void)setPrimitiveRegisteredAddress2:(id)value;

- (id)primitiveRegisteredPostalCode;
- (void)setPrimitiveRegisteredPostalCode:(id)value;

- (id)primitiveRemarks;
- (void)setPrimitiveRemarks:(id)value;

- (id)primitiveResidentialAddress;
- (void)setPrimitiveResidentialAddress:(id)value;

- (id)primitiveResidentialAddress1;
- (void)setPrimitiveResidentialAddress1:(id)value;

- (id)primitiveResidentialAddress2;
- (void)setPrimitiveResidentialAddress2:(id)value;

- (id)primitiveResidentialPostalCode;
- (void)setPrimitiveResidentialPostalCode:(id)value;

- (id)primitiveSalesExperience;
- (void)setPrimitiveSalesExperience:(id)value;

- (id)primitiveSourceComerNew;
- (void)setPrimitiveSourceComerNew:(id)value;

- (id)primitiveTalentProfile;
- (void)setPrimitiveTalentProfile:(id)value;

- (id)primitiveWeChat;
- (void)setPrimitiveWeChat:(id)value;

- (id)primitiveWeibo;
- (void)setPrimitiveWeibo:(id)value;

- (id)primitiveWorkingYearExperience;
- (void)setPrimitiveWorkingYearExperience:(id)value;

- (id)primitiveYearlyIncome;
- (void)setPrimitiveYearlyIncome:(id)value;

- (NSMutableSet*)primitiveCandidateESignatures;
- (void)setPrimitiveCandidateESignatures:(NSMutableSet*)value;

- (NSMutableSet*)primitiveCandidateEducations;
- (void)setPrimitiveCandidateEducations:(NSMutableSet*)value;

- (NSMutableSet*)primitiveCandidateFamilyInfos;
- (void)setPrimitiveCandidateFamilyInfos:(NSMutableSet*)value;

- (NSMutableSet*)primitiveCandidateNotes;
- (void)setPrimitiveCandidateNotes:(NSMutableSet*)value;

- (TblCandidateProcess*)primitiveCandidateProcess;
- (void)setPrimitiveCandidateProcess:(TblCandidateProcess*)value;

- (NSMutableSet*)primitiveCandidateProfessionalCertifications;
- (void)setPrimitiveCandidateProfessionalCertifications:(NSMutableSet*)value;

- (NSMutableSet*)primitiveCandidateWorkExperiences;
- (void)setPrimitiveCandidateWorkExperiences:(NSMutableSet*)value;

- (NSMutableSet*)primitiveGroup;
- (void)setPrimitiveGroup:(NSMutableSet*)value;

@end
