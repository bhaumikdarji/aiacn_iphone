// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblESignature.h instead.

#import <CoreData/CoreData.h>

extern const struct TblESignatureAttributes {
	__unsafe_unretained NSString *agentId;
	__unsafe_unretained NSString *applicationDate;
	__unsafe_unretained NSString *atachedWithInsuranceCo;
	__unsafe_unretained NSString *branch;
	__unsafe_unretained NSString *candidateName;
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *contactWithAia;
	__unsafe_unretained NSString *eSignaturePhoto;
	__unsafe_unretained NSString *iosAddressCode;
	__unsafe_unretained NSString *serviceDepartment;
	__unsafe_unretained NSString *takenPspTest;
} TblESignatureAttributes;

extern const struct TblESignatureRelationships {
	__unsafe_unretained NSString *contact;
} TblESignatureRelationships;

@class TblContact;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblESignatureID : NSManagedObjectID {}
@end

@interface _TblESignature : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblESignatureID* objectID;

@property (nonatomic, strong) id agentId;

//- (BOOL)validateAgentId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id applicationDate;

//- (BOOL)validateApplicationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* atachedWithInsuranceCo;

@property (atomic) BOOL atachedWithInsuranceCoValue;
- (BOOL)atachedWithInsuranceCoValue;
- (void)setAtachedWithInsuranceCoValue:(BOOL)value_;

//- (BOOL)validateAtachedWithInsuranceCo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id branch;

//- (BOOL)validateBranch:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id candidateName;

//- (BOOL)validateCandidateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id city;

//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* contactWithAia;

@property (atomic) BOOL contactWithAiaValue;
- (BOOL)contactWithAiaValue;
- (void)setContactWithAiaValue:(BOOL)value_;

//- (BOOL)validateContactWithAia:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id eSignaturePhoto;

//- (BOOL)validateESignaturePhoto:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id iosAddressCode;

//- (BOOL)validateIosAddressCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id serviceDepartment;

//- (BOOL)validateServiceDepartment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* takenPspTest;

@property (atomic) BOOL takenPspTestValue;
- (BOOL)takenPspTestValue;
- (void)setTakenPspTestValue:(BOOL)value_;

//- (BOOL)validateTakenPspTest:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblContact *contact;

//- (BOOL)validateContact:(id*)value_ error:(NSError**)error_;

@end

@interface _TblESignature (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveAgentId;
- (void)setPrimitiveAgentId:(id)value;

- (id)primitiveApplicationDate;
- (void)setPrimitiveApplicationDate:(id)value;

- (NSNumber*)primitiveAtachedWithInsuranceCo;
- (void)setPrimitiveAtachedWithInsuranceCo:(NSNumber*)value;

- (BOOL)primitiveAtachedWithInsuranceCoValue;
- (void)setPrimitiveAtachedWithInsuranceCoValue:(BOOL)value_;

- (id)primitiveBranch;
- (void)setPrimitiveBranch:(id)value;

- (id)primitiveCandidateName;
- (void)setPrimitiveCandidateName:(id)value;

- (id)primitiveCity;
- (void)setPrimitiveCity:(id)value;

- (NSNumber*)primitiveContactWithAia;
- (void)setPrimitiveContactWithAia:(NSNumber*)value;

- (BOOL)primitiveContactWithAiaValue;
- (void)setPrimitiveContactWithAiaValue:(BOOL)value_;

- (id)primitiveESignaturePhoto;
- (void)setPrimitiveESignaturePhoto:(id)value;

- (id)primitiveIosAddressCode;
- (void)setPrimitiveIosAddressCode:(id)value;

- (id)primitiveServiceDepartment;
- (void)setPrimitiveServiceDepartment:(id)value;

- (NSNumber*)primitiveTakenPspTest;
- (void)setPrimitiveTakenPspTest:(NSNumber*)value;

- (BOOL)primitiveTakenPspTestValue;
- (void)setPrimitiveTakenPspTestValue:(BOOL)value_;

- (TblContact*)primitiveContact;
- (void)setPrimitiveContact:(TblContact*)value;

@end
