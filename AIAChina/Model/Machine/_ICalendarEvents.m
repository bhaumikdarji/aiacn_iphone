// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ICalendarEvents.m instead.

#import "_ICalendarEvents.h"

const struct ICalendarEventsAttributes ICalendarEventsAttributes = {
	.address = @"address",
	.agentCode = @"agentCode",
	.co = @"co",
	.endTime = @"endTime",
	.eventDescription = @"eventDescription",
	.eventId = @"eventId",
	.openFlag = @"openFlag",
	.origin = @"origin",
	.referenceId = @"referenceId",
	.st_dt = @"st_dt",
	.startTime = @"startTime",
	.subject = @"subject",
	.typeId = @"typeId",
	.typeName = @"typeName",
	.updateTime = @"updateTime",
};

@implementation ICalendarEventsID
@end

@implementation _ICalendarEvents

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ICalendarEvents" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ICalendarEvents";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ICalendarEvents" inManagedObjectContext:moc_];
}

- (ICalendarEventsID*)objectID {
	return (ICalendarEventsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic address;

@dynamic agentCode;

@dynamic co;

@dynamic endTime;

@dynamic eventDescription;

@dynamic eventId;

@dynamic openFlag;

@dynamic origin;

@dynamic referenceId;

@dynamic st_dt;

@dynamic startTime;

@dynamic subject;

@dynamic typeId;

@dynamic typeName;

@dynamic updateTime;

@end

