// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblAgentDetails.h instead.

#import <CoreData/CoreData.h>

extern const struct TblAgentDetailsAttributes {
	__unsafe_unretained NSString *agentDetails;
	__unsafe_unretained NSString *agentGallayImages;
	__unsafe_unretained NSString *agentImage;
	__unsafe_unretained NSString *agentType;
	__unsafe_unretained NSString *branch;
	__unsafe_unretained NSString *certiID;
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *contactNumber;
	__unsafe_unretained NSString *contractedDate;
	__unsafe_unretained NSString *dateofBirth;
	__unsafe_unretained NSString *dtleader;
	__unsafe_unretained NSString *educationBackground;
	__unsafe_unretained NSString *emailId;
	__unsafe_unretained NSString *freeText;
	__unsafe_unretained NSString *gender;
	__unsafe_unretained NSString *graduationSchool;
	__unsafe_unretained NSString *graduationTime;
	__unsafe_unretained NSString *isFirstTimeLogin;
	__unsafe_unretained NSString *officeCode;
	__unsafe_unretained NSString *officeName;
	__unsafe_unretained NSString *professionalInstitutions;
	__unsafe_unretained NSString *rank;
	__unsafe_unretained NSString *serviceDepartment;
	__unsafe_unretained NSString *ssc;
	__unsafe_unretained NSString *teamCode;
	__unsafe_unretained NSString *teamName;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *titleDescribe;
	__unsafe_unretained NSString *userID;
	__unsafe_unretained NSString *userName;
	__unsafe_unretained NSString *userStatus;
	__unsafe_unretained NSString *userType;
} TblAgentDetailsAttributes;

extern const struct TblAgentDetailsRelationships {
	__unsafe_unretained NSString *agentCareerProgress;
} TblAgentDetailsRelationships;

@class TblCareerProgress;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblAgentDetailsID : NSManagedObjectID {}
@end

@interface _TblAgentDetails : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblAgentDetailsID* objectID;

@property (nonatomic, strong) id agentDetails;

//- (BOOL)validateAgentDetails:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agentGallayImages;

//- (BOOL)validateAgentGallayImages:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agentImage;

//- (BOOL)validateAgentImage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id agentType;

//- (BOOL)validateAgentType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id branch;

//- (BOOL)validateBranch:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id certiID;

//- (BOOL)validateCertiID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id city;

//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id contactNumber;

//- (BOOL)validateContactNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id contractedDate;

//- (BOOL)validateContractedDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id dateofBirth;

//- (BOOL)validateDateofBirth:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id dtleader;

//- (BOOL)validateDtleader:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id educationBackground;

//- (BOOL)validateEducationBackground:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id emailId;

//- (BOOL)validateEmailId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id freeText;

//- (BOOL)validateFreeText:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id gender;

//- (BOOL)validateGender:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id graduationSchool;

//- (BOOL)validateGraduationSchool:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id graduationTime;

//- (BOOL)validateGraduationTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isFirstTimeLogin;

@property (atomic) BOOL isFirstTimeLoginValue;
- (BOOL)isFirstTimeLoginValue;
- (void)setIsFirstTimeLoginValue:(BOOL)value_;

//- (BOOL)validateIsFirstTimeLogin:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id officeCode;

//- (BOOL)validateOfficeCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id officeName;

//- (BOOL)validateOfficeName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id professionalInstitutions;

//- (BOOL)validateProfessionalInstitutions:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id rank;

//- (BOOL)validateRank:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id serviceDepartment;

//- (BOOL)validateServiceDepartment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id ssc;

//- (BOOL)validateSsc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id teamCode;

//- (BOOL)validateTeamCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id teamName;

//- (BOOL)validateTeamName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id titleDescribe;

//- (BOOL)validateTitleDescribe:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id userID;

//- (BOOL)validateUserID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id userName;

//- (BOOL)validateUserName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id userStatus;

//- (BOOL)validateUserStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* userType;

//- (BOOL)validateUserType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *agentCareerProgress;

- (NSMutableSet*)agentCareerProgressSet;

@end

@interface _TblAgentDetails (AgentCareerProgressCoreDataGeneratedAccessors)
- (void)addAgentCareerProgress:(NSSet*)value_;
- (void)removeAgentCareerProgress:(NSSet*)value_;
- (void)addAgentCareerProgressObject:(TblCareerProgress*)value_;
- (void)removeAgentCareerProgressObject:(TblCareerProgress*)value_;

@end

@interface _TblAgentDetails (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveAgentDetails;
- (void)setPrimitiveAgentDetails:(id)value;

- (id)primitiveAgentGallayImages;
- (void)setPrimitiveAgentGallayImages:(id)value;

- (id)primitiveAgentImage;
- (void)setPrimitiveAgentImage:(id)value;

- (id)primitiveAgentType;
- (void)setPrimitiveAgentType:(id)value;

- (id)primitiveBranch;
- (void)setPrimitiveBranch:(id)value;

- (id)primitiveCertiID;
- (void)setPrimitiveCertiID:(id)value;

- (id)primitiveCity;
- (void)setPrimitiveCity:(id)value;

- (id)primitiveContactNumber;
- (void)setPrimitiveContactNumber:(id)value;

- (id)primitiveContractedDate;
- (void)setPrimitiveContractedDate:(id)value;

- (id)primitiveDateofBirth;
- (void)setPrimitiveDateofBirth:(id)value;

- (id)primitiveDtleader;
- (void)setPrimitiveDtleader:(id)value;

- (id)primitiveEducationBackground;
- (void)setPrimitiveEducationBackground:(id)value;

- (id)primitiveEmailId;
- (void)setPrimitiveEmailId:(id)value;

- (id)primitiveFreeText;
- (void)setPrimitiveFreeText:(id)value;

- (id)primitiveGender;
- (void)setPrimitiveGender:(id)value;

- (id)primitiveGraduationSchool;
- (void)setPrimitiveGraduationSchool:(id)value;

- (id)primitiveGraduationTime;
- (void)setPrimitiveGraduationTime:(id)value;

- (NSNumber*)primitiveIsFirstTimeLogin;
- (void)setPrimitiveIsFirstTimeLogin:(NSNumber*)value;

- (BOOL)primitiveIsFirstTimeLoginValue;
- (void)setPrimitiveIsFirstTimeLoginValue:(BOOL)value_;

- (id)primitiveOfficeCode;
- (void)setPrimitiveOfficeCode:(id)value;

- (id)primitiveOfficeName;
- (void)setPrimitiveOfficeName:(id)value;

- (id)primitiveProfessionalInstitutions;
- (void)setPrimitiveProfessionalInstitutions:(id)value;

- (id)primitiveRank;
- (void)setPrimitiveRank:(id)value;

- (id)primitiveServiceDepartment;
- (void)setPrimitiveServiceDepartment:(id)value;

- (id)primitiveSsc;
- (void)setPrimitiveSsc:(id)value;

- (id)primitiveTeamCode;
- (void)setPrimitiveTeamCode:(id)value;

- (id)primitiveTeamName;
- (void)setPrimitiveTeamName:(id)value;

- (id)primitiveTitle;
- (void)setPrimitiveTitle:(id)value;

- (id)primitiveTitleDescribe;
- (void)setPrimitiveTitleDescribe:(id)value;

- (id)primitiveUserID;
- (void)setPrimitiveUserID:(id)value;

- (id)primitiveUserName;
- (void)setPrimitiveUserName:(id)value;

- (id)primitiveUserStatus;
- (void)setPrimitiveUserStatus:(id)value;

- (NSString*)primitiveUserType;
- (void)setPrimitiveUserType:(NSString*)value;

- (NSMutableSet*)primitiveAgentCareerProgress;
- (void)setPrimitiveAgentCareerProgress:(NSMutableSet*)value;

@end
