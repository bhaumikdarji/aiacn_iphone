// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblExperience.h instead.

#import <CoreData/CoreData.h>

extern const struct TblExperienceAttributes {
	__unsafe_unretained NSString *endDate;
	__unsafe_unretained NSString *income;
	__unsafe_unretained NSString *iosAddressCode;
	__unsafe_unretained NSString *occupation;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *startDate;
	__unsafe_unretained NSString *unit;
	__unsafe_unretained NSString *witness;
	__unsafe_unretained NSString *witnessContactNo;
} TblExperienceAttributes;

extern const struct TblExperienceRelationships {
	__unsafe_unretained NSString *contact;
} TblExperienceRelationships;

@class TblContact;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblExperienceID : NSManagedObjectID {}
@end

@interface _TblExperience : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblExperienceID* objectID;

@property (nonatomic, strong) id endDate;

//- (BOOL)validateEndDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id income;

//- (BOOL)validateIncome:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id iosAddressCode;

//- (BOOL)validateIosAddressCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id occupation;

//- (BOOL)validateOccupation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id position;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id startDate;

//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id unit;

//- (BOOL)validateUnit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id witness;

//- (BOOL)validateWitness:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id witnessContactNo;

//- (BOOL)validateWitnessContactNo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblContact *contact;

//- (BOOL)validateContact:(id*)value_ error:(NSError**)error_;

@end

@interface _TblExperience (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveEndDate;
- (void)setPrimitiveEndDate:(id)value;

- (id)primitiveIncome;
- (void)setPrimitiveIncome:(id)value;

- (id)primitiveIosAddressCode;
- (void)setPrimitiveIosAddressCode:(id)value;

- (id)primitiveOccupation;
- (void)setPrimitiveOccupation:(id)value;

- (id)primitivePosition;
- (void)setPrimitivePosition:(id)value;

- (id)primitiveStartDate;
- (void)setPrimitiveStartDate:(id)value;

- (id)primitiveUnit;
- (void)setPrimitiveUnit:(id)value;

- (id)primitiveWitness;
- (void)setPrimitiveWitness:(id)value;

- (id)primitiveWitnessContactNo;
- (void)setPrimitiveWitnessContactNo:(id)value;

- (TblContact*)primitiveContact;
- (void)setPrimitiveContact:(TblContact*)value;

@end
