// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblInterview.h instead.

#import <CoreData/CoreData.h>

extern const struct TblInterviewAttributes {
	__unsafe_unretained NSString *attachmentName;
	__unsafe_unretained NSString *attachmentPath;
	__unsafe_unretained NSString *attendeeCount;
	__unsafe_unretained NSString *buCode;
	__unsafe_unretained NSString *buName;
	__unsafe_unretained NSString *cityCode;
	__unsafe_unretained NSString *createdBy;
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *district;
	__unsafe_unretained NSString *endTime;
	__unsafe_unretained NSString *estimatedCondidates;
	__unsafe_unretained NSString *interviewCode;
	__unsafe_unretained NSString *interviewDate;
	__unsafe_unretained NSString *interviewMaterial;
	__unsafe_unretained NSString *interviewSessionName;
	__unsafe_unretained NSString *interviewType;
	__unsafe_unretained NSString *isRegistered;
	__unsafe_unretained NSString *location;
	__unsafe_unretained NSString *modificationDate;
	__unsafe_unretained NSString *modifiedBy;
	__unsafe_unretained NSString *organizer;
	__unsafe_unretained NSString *registeredCount;
	__unsafe_unretained NSString *sscCode;
	__unsafe_unretained NSString *startTime;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *token;
} TblInterviewAttributes;

extern const struct TblInterviewRelationships {
	__unsafe_unretained NSString *calendarEvent;
	__unsafe_unretained NSString *interviewCandidate;
} TblInterviewRelationships;

@class TblCalendarEvent;
@class TblInterviewCandidate;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface TblInterviewID : NSManagedObjectID {}
@end

@interface _TblInterview : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TblInterviewID* objectID;

@property (nonatomic, strong) id attachmentName;

//- (BOOL)validateAttachmentName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id attachmentPath;

//- (BOOL)validateAttachmentPath:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* attendeeCount;

@property (atomic) int64_t attendeeCountValue;
- (int64_t)attendeeCountValue;
- (void)setAttendeeCountValue:(int64_t)value_;

//- (BOOL)validateAttendeeCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* buCode;

@property (atomic) int64_t buCodeValue;
- (int64_t)buCodeValue;
- (void)setBuCodeValue:(int64_t)value_;

//- (BOOL)validateBuCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id buName;

//- (BOOL)validateBuName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* cityCode;

//- (BOOL)validateCityCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id createdBy;

//- (BOOL)validateCreatedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* district;

@property (atomic) int64_t districtValue;
- (int64_t)districtValue;
- (void)setDistrictValue:(int64_t)value_;

//- (BOOL)validateDistrict:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id endTime;

//- (BOOL)validateEndTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* estimatedCondidates;

@property (atomic) int64_t estimatedCondidatesValue;
- (int64_t)estimatedCondidatesValue;
- (void)setEstimatedCondidatesValue:(int64_t)value_;

//- (BOOL)validateEstimatedCondidates:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* interviewCode;

@property (atomic) int64_t interviewCodeValue;
- (int64_t)interviewCodeValue;
- (void)setInterviewCodeValue:(int64_t)value_;

//- (BOOL)validateInterviewCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* interviewDate;

//- (BOOL)validateInterviewDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* interviewMaterial;

//- (BOOL)validateInterviewMaterial:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id interviewSessionName;

//- (BOOL)validateInterviewSessionName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id interviewType;

//- (BOOL)validateInterviewType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRegistered;

@property (atomic) BOOL isRegisteredValue;
- (BOOL)isRegisteredValue;
- (void)setIsRegisteredValue:(BOOL)value_;

//- (BOOL)validateIsRegistered:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id location;

//- (BOOL)validateLocation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id modificationDate;

//- (BOOL)validateModificationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id modifiedBy;

//- (BOOL)validateModifiedBy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id organizer;

//- (BOOL)validateOrganizer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* registeredCount;

@property (atomic) int64_t registeredCountValue;
- (int64_t)registeredCountValue;
- (void)setRegisteredCountValue:(int64_t)value_;

//- (BOOL)validateRegisteredCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sscCode;

//- (BOOL)validateSscCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id startTime;

//- (BOOL)validateStartTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) BOOL statusValue;
- (BOOL)statusValue;
- (void)setStatusValue:(BOOL)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id token;

//- (BOOL)validateToken:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TblCalendarEvent *calendarEvent;

//- (BOOL)validateCalendarEvent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *interviewCandidate;

- (NSMutableSet*)interviewCandidateSet;

@end

@interface _TblInterview (InterviewCandidateCoreDataGeneratedAccessors)
- (void)addInterviewCandidate:(NSSet*)value_;
- (void)removeInterviewCandidate:(NSSet*)value_;
- (void)addInterviewCandidateObject:(TblInterviewCandidate*)value_;
- (void)removeInterviewCandidateObject:(TblInterviewCandidate*)value_;

@end

@interface _TblInterview (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveAttachmentName;
- (void)setPrimitiveAttachmentName:(id)value;

- (id)primitiveAttachmentPath;
- (void)setPrimitiveAttachmentPath:(id)value;

- (NSNumber*)primitiveAttendeeCount;
- (void)setPrimitiveAttendeeCount:(NSNumber*)value;

- (int64_t)primitiveAttendeeCountValue;
- (void)setPrimitiveAttendeeCountValue:(int64_t)value_;

- (NSNumber*)primitiveBuCode;
- (void)setPrimitiveBuCode:(NSNumber*)value;

- (int64_t)primitiveBuCodeValue;
- (void)setPrimitiveBuCodeValue:(int64_t)value_;

- (id)primitiveBuName;
- (void)setPrimitiveBuName:(id)value;

- (NSString*)primitiveCityCode;
- (void)setPrimitiveCityCode:(NSString*)value;

- (id)primitiveCreatedBy;
- (void)setPrimitiveCreatedBy:(id)value;

- (id)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(id)value;

- (NSNumber*)primitiveDistrict;
- (void)setPrimitiveDistrict:(NSNumber*)value;

- (int64_t)primitiveDistrictValue;
- (void)setPrimitiveDistrictValue:(int64_t)value_;

- (id)primitiveEndTime;
- (void)setPrimitiveEndTime:(id)value;

- (NSNumber*)primitiveEstimatedCondidates;
- (void)setPrimitiveEstimatedCondidates:(NSNumber*)value;

- (int64_t)primitiveEstimatedCondidatesValue;
- (void)setPrimitiveEstimatedCondidatesValue:(int64_t)value_;

- (NSNumber*)primitiveInterviewCode;
- (void)setPrimitiveInterviewCode:(NSNumber*)value;

- (int64_t)primitiveInterviewCodeValue;
- (void)setPrimitiveInterviewCodeValue:(int64_t)value_;

- (NSDate*)primitiveInterviewDate;
- (void)setPrimitiveInterviewDate:(NSDate*)value;

- (NSString*)primitiveInterviewMaterial;
- (void)setPrimitiveInterviewMaterial:(NSString*)value;

- (id)primitiveInterviewSessionName;
- (void)setPrimitiveInterviewSessionName:(id)value;

- (id)primitiveInterviewType;
- (void)setPrimitiveInterviewType:(id)value;

- (NSNumber*)primitiveIsRegistered;
- (void)setPrimitiveIsRegistered:(NSNumber*)value;

- (BOOL)primitiveIsRegisteredValue;
- (void)setPrimitiveIsRegisteredValue:(BOOL)value_;

- (id)primitiveLocation;
- (void)setPrimitiveLocation:(id)value;

- (id)primitiveModificationDate;
- (void)setPrimitiveModificationDate:(id)value;

- (id)primitiveModifiedBy;
- (void)setPrimitiveModifiedBy:(id)value;

- (id)primitiveOrganizer;
- (void)setPrimitiveOrganizer:(id)value;

- (NSNumber*)primitiveRegisteredCount;
- (void)setPrimitiveRegisteredCount:(NSNumber*)value;

- (int64_t)primitiveRegisteredCountValue;
- (void)setPrimitiveRegisteredCountValue:(int64_t)value_;

- (NSString*)primitiveSscCode;
- (void)setPrimitiveSscCode:(NSString*)value;

- (id)primitiveStartTime;
- (void)setPrimitiveStartTime:(id)value;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (BOOL)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(BOOL)value_;

- (id)primitiveToken;
- (void)setPrimitiveToken:(id)value;

- (TblCalendarEvent*)primitiveCalendarEvent;
- (void)setPrimitiveCalendarEvent:(TblCalendarEvent*)value;

- (NSMutableSet*)primitiveInterviewCandidate;
- (void)setPrimitiveInterviewCandidate:(NSMutableSet*)value;

@end
