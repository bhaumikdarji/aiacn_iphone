// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblInterview.m instead.

#import "_TblInterview.h"

const struct TblInterviewAttributes TblInterviewAttributes = {
	.attachmentName = @"attachmentName",
	.attachmentPath = @"attachmentPath",
	.attendeeCount = @"attendeeCount",
	.buCode = @"buCode",
	.buName = @"buName",
	.cityCode = @"cityCode",
	.createdBy = @"createdBy",
	.creationDate = @"creationDate",
	.district = @"district",
	.endTime = @"endTime",
	.estimatedCondidates = @"estimatedCondidates",
	.interviewCode = @"interviewCode",
	.interviewDate = @"interviewDate",
	.interviewMaterial = @"interviewMaterial",
	.interviewSessionName = @"interviewSessionName",
	.interviewType = @"interviewType",
	.isRegistered = @"isRegistered",
	.location = @"location",
	.modificationDate = @"modificationDate",
	.modifiedBy = @"modifiedBy",
	.organizer = @"organizer",
	.registeredCount = @"registeredCount",
	.sscCode = @"sscCode",
	.startTime = @"startTime",
	.status = @"status",
	.token = @"token",
};

const struct TblInterviewRelationships TblInterviewRelationships = {
	.calendarEvent = @"calendarEvent",
	.interviewCandidate = @"interviewCandidate",
};

@implementation TblInterviewID
@end

@implementation _TblInterview

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblInterview" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblInterview";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblInterview" inManagedObjectContext:moc_];
}

- (TblInterviewID*)objectID {
	return (TblInterviewID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"attendeeCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"attendeeCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"buCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"buCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"districtValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"district"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"estimatedCondidatesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"estimatedCondidates"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"interviewCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"interviewCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isRegisteredValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRegistered"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"registeredCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"registeredCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic attachmentName;

@dynamic attachmentPath;

@dynamic attendeeCount;

- (int64_t)attendeeCountValue {
	NSNumber *result = [self attendeeCount];
	return [result longLongValue];
}

- (void)setAttendeeCountValue:(int64_t)value_ {
	[self setAttendeeCount:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveAttendeeCountValue {
	NSNumber *result = [self primitiveAttendeeCount];
	return [result longLongValue];
}

- (void)setPrimitiveAttendeeCountValue:(int64_t)value_ {
	[self setPrimitiveAttendeeCount:[NSNumber numberWithLongLong:value_]];
}

@dynamic buCode;

- (int64_t)buCodeValue {
	NSNumber *result = [self buCode];
	return [result longLongValue];
}

- (void)setBuCodeValue:(int64_t)value_ {
	[self setBuCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBuCodeValue {
	NSNumber *result = [self primitiveBuCode];
	return [result longLongValue];
}

- (void)setPrimitiveBuCodeValue:(int64_t)value_ {
	[self setPrimitiveBuCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic buName;

@dynamic cityCode;

@dynamic createdBy;

@dynamic creationDate;

@dynamic district;

- (int64_t)districtValue {
	NSNumber *result = [self district];
	return [result longLongValue];
}

- (void)setDistrictValue:(int64_t)value_ {
	[self setDistrict:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveDistrictValue {
	NSNumber *result = [self primitiveDistrict];
	return [result longLongValue];
}

- (void)setPrimitiveDistrictValue:(int64_t)value_ {
	[self setPrimitiveDistrict:[NSNumber numberWithLongLong:value_]];
}

@dynamic endTime;

@dynamic estimatedCondidates;

- (int64_t)estimatedCondidatesValue {
	NSNumber *result = [self estimatedCondidates];
	return [result longLongValue];
}

- (void)setEstimatedCondidatesValue:(int64_t)value_ {
	[self setEstimatedCondidates:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveEstimatedCondidatesValue {
	NSNumber *result = [self primitiveEstimatedCondidates];
	return [result longLongValue];
}

- (void)setPrimitiveEstimatedCondidatesValue:(int64_t)value_ {
	[self setPrimitiveEstimatedCondidates:[NSNumber numberWithLongLong:value_]];
}

@dynamic interviewCode;

- (int64_t)interviewCodeValue {
	NSNumber *result = [self interviewCode];
	return [result longLongValue];
}

- (void)setInterviewCodeValue:(int64_t)value_ {
	[self setInterviewCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveInterviewCodeValue {
	NSNumber *result = [self primitiveInterviewCode];
	return [result longLongValue];
}

- (void)setPrimitiveInterviewCodeValue:(int64_t)value_ {
	[self setPrimitiveInterviewCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic interviewDate;

@dynamic interviewMaterial;

@dynamic interviewSessionName;

@dynamic interviewType;

@dynamic isRegistered;

- (BOOL)isRegisteredValue {
	NSNumber *result = [self isRegistered];
	return [result boolValue];
}

- (void)setIsRegisteredValue:(BOOL)value_ {
	[self setIsRegistered:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsRegisteredValue {
	NSNumber *result = [self primitiveIsRegistered];
	return [result boolValue];
}

- (void)setPrimitiveIsRegisteredValue:(BOOL)value_ {
	[self setPrimitiveIsRegistered:[NSNumber numberWithBool:value_]];
}

@dynamic location;

@dynamic modificationDate;

@dynamic modifiedBy;

@dynamic organizer;

@dynamic registeredCount;

- (int64_t)registeredCountValue {
	NSNumber *result = [self registeredCount];
	return [result longLongValue];
}

- (void)setRegisteredCountValue:(int64_t)value_ {
	[self setRegisteredCount:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRegisteredCountValue {
	NSNumber *result = [self primitiveRegisteredCount];
	return [result longLongValue];
}

- (void)setPrimitiveRegisteredCountValue:(int64_t)value_ {
	[self setPrimitiveRegisteredCount:[NSNumber numberWithLongLong:value_]];
}

@dynamic sscCode;

@dynamic startTime;

@dynamic status;

- (BOOL)statusValue {
	NSNumber *result = [self status];
	return [result boolValue];
}

- (void)setStatusValue:(BOOL)value_ {
	[self setStatus:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result boolValue];
}

- (void)setPrimitiveStatusValue:(BOOL)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithBool:value_]];
}

@dynamic token;

@dynamic calendarEvent;

@dynamic interviewCandidate;

- (NSMutableSet*)interviewCandidateSet {
	[self willAccessValueForKey:@"interviewCandidate"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"interviewCandidate"];

	[self didAccessValueForKey:@"interviewCandidate"];
	return result;
}

@end

