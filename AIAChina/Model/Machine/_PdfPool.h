// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PdfPool.h instead.

#import <CoreData/CoreData.h>

extern const struct PdfPoolAttributes {
	__unsafe_unretained NSString *pdfdata;
	__unsafe_unretained NSString *pdfpath;
} PdfPoolAttributes;

@class NSObject;

@class NSObject;

@interface PdfPoolID : NSManagedObjectID {}
@end

@interface _PdfPool : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PdfPoolID* objectID;

@property (nonatomic, strong) id pdfdata;

//- (BOOL)validatePdfdata:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id pdfpath;

//- (BOOL)validatePdfpath:(id*)value_ error:(NSError**)error_;

@end

@interface _PdfPool (CoreDataGeneratedPrimitiveAccessors)

- (id)primitivePdfdata;
- (void)setPrimitivePdfdata:(id)value;

- (id)primitivePdfpath;
- (void)setPrimitivePdfpath:(id)value;

@end
