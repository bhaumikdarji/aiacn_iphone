// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblNotes.m instead.

#import "_TblNotes.h"

const struct TblNotesAttributes TblNotesAttributes = {
	.activityDate = @"activityDate",
	.activityStatus = @"activityStatus",
	.activityType = @"activityType",
	.desc = @"desc",
	.eventCode = @"eventCode",
	.iosAddressCode = @"iosAddressCode",
	.isDelete = @"isDelete",
	.isSync = @"isSync",
	.isSystem = @"isSystem",
	.recruitmentStep = @"recruitmentStep",
};

const struct TblNotesRelationships TblNotesRelationships = {
	.contact = @"contact",
};

@implementation TblNotesID
@end

@implementation _TblNotes

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblNotes" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblNotes";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblNotes" inManagedObjectContext:moc_];
}

- (TblNotesID*)objectID {
	return (TblNotesID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"activityStatusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"activityStatus"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"eventCodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"eventCode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isDeleteValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isDelete"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isSyncValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isSync"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isSystemValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isSystem"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"recruitmentStepValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"recruitmentStep"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic activityDate;

@dynamic activityStatus;

- (BOOL)activityStatusValue {
	NSNumber *result = [self activityStatus];
	return [result boolValue];
}

- (void)setActivityStatusValue:(BOOL)value_ {
	[self setActivityStatus:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveActivityStatusValue {
	NSNumber *result = [self primitiveActivityStatus];
	return [result boolValue];
}

- (void)setPrimitiveActivityStatusValue:(BOOL)value_ {
	[self setPrimitiveActivityStatus:[NSNumber numberWithBool:value_]];
}

@dynamic activityType;

@dynamic desc;

@dynamic eventCode;

- (int64_t)eventCodeValue {
	NSNumber *result = [self eventCode];
	return [result longLongValue];
}

- (void)setEventCodeValue:(int64_t)value_ {
	[self setEventCode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveEventCodeValue {
	NSNumber *result = [self primitiveEventCode];
	return [result longLongValue];
}

- (void)setPrimitiveEventCodeValue:(int64_t)value_ {
	[self setPrimitiveEventCode:[NSNumber numberWithLongLong:value_]];
}

@dynamic iosAddressCode;

@dynamic isDelete;

- (BOOL)isDeleteValue {
	NSNumber *result = [self isDelete];
	return [result boolValue];
}

- (void)setIsDeleteValue:(BOOL)value_ {
	[self setIsDelete:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsDeleteValue {
	NSNumber *result = [self primitiveIsDelete];
	return [result boolValue];
}

- (void)setPrimitiveIsDeleteValue:(BOOL)value_ {
	[self setPrimitiveIsDelete:[NSNumber numberWithBool:value_]];
}

@dynamic isSync;

- (BOOL)isSyncValue {
	NSNumber *result = [self isSync];
	return [result boolValue];
}

- (void)setIsSyncValue:(BOOL)value_ {
	[self setIsSync:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsSyncValue {
	NSNumber *result = [self primitiveIsSync];
	return [result boolValue];
}

- (void)setPrimitiveIsSyncValue:(BOOL)value_ {
	[self setPrimitiveIsSync:[NSNumber numberWithBool:value_]];
}

@dynamic isSystem;

- (BOOL)isSystemValue {
	NSNumber *result = [self isSystem];
	return [result boolValue];
}

- (void)setIsSystemValue:(BOOL)value_ {
	[self setIsSystem:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsSystemValue {
	NSNumber *result = [self primitiveIsSystem];
	return [result boolValue];
}

- (void)setPrimitiveIsSystemValue:(BOOL)value_ {
	[self setPrimitiveIsSystem:[NSNumber numberWithBool:value_]];
}

@dynamic recruitmentStep;

- (int64_t)recruitmentStepValue {
	NSNumber *result = [self recruitmentStep];
	return [result longLongValue];
}

- (void)setRecruitmentStepValue:(int64_t)value_ {
	[self setRecruitmentStep:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRecruitmentStepValue {
	NSNumber *result = [self primitiveRecruitmentStep];
	return [result longLongValue];
}

- (void)setPrimitiveRecruitmentStepValue:(int64_t)value_ {
	[self setPrimitiveRecruitmentStep:[NSNumber numberWithLongLong:value_]];
}

@dynamic contact;

@end

