// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TblGroup.m instead.

#import "_TblGroup.h"

const struct TblGroupAttributes TblGroupAttributes = {
	.agentId = @"agentId",
	.groupDescription = @"groupDescription",
	.groupName = @"groupName",
	.image = @"image",
	.iosAddressCode = @"iosAddressCode",
	.isDelete = @"isDelete",
	.isSync = @"isSync",
};

const struct TblGroupRelationships TblGroupRelationships = {
	.contact = @"contact",
};

@implementation TblGroupID
@end

@implementation _TblGroup

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TblGroup" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TblGroup";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TblGroup" inManagedObjectContext:moc_];
}

- (TblGroupID*)objectID {
	return (TblGroupID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isDeleteValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isDelete"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isSyncValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isSync"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic agentId;

@dynamic groupDescription;

@dynamic groupName;

@dynamic image;

@dynamic iosAddressCode;

@dynamic isDelete;

- (BOOL)isDeleteValue {
	NSNumber *result = [self isDelete];
	return [result boolValue];
}

- (void)setIsDeleteValue:(BOOL)value_ {
	[self setIsDelete:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsDeleteValue {
	NSNumber *result = [self primitiveIsDelete];
	return [result boolValue];
}

- (void)setPrimitiveIsDeleteValue:(BOOL)value_ {
	[self setPrimitiveIsDelete:[NSNumber numberWithBool:value_]];
}

@dynamic isSync;

- (BOOL)isSyncValue {
	NSNumber *result = [self isSync];
	return [result boolValue];
}

- (void)setIsSyncValue:(BOOL)value_ {
	[self setIsSync:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsSyncValue {
	NSNumber *result = [self primitiveIsSync];
	return [result boolValue];
}

- (void)setPrimitiveIsSyncValue:(BOOL)value_ {
	[self setPrimitiveIsSync:[NSNumber numberWithBool:value_]];
}

@dynamic contact;

- (NSMutableSet*)contactSet {
	[self willAccessValueForKey:@"contact"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"contact"];

	[self didAccessValueForKey:@"contact"];
	return result;
}

@end

