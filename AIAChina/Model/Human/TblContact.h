#import "_TblContact.h"

@interface TblContact : _TblContact {}
// Custom logic goes here.
+ (TblContact *)getContactForName:(NSString *)name;
- (NSString *)getLocalizedQRCodeString;
- (NSString *)localizedBirthDate;
- (NSDate *)convertBirthDateToDate:(NSString *)localizedBirthDate;
@end
