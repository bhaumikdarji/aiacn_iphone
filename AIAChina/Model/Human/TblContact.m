#import "TblContact.h"
#import "DateUtils.h"
#import "Constants.h"
#import "NSString+isNumeric.h"
@interface TblContact ()

// Private interface goes here.

@end

@implementation TblContact

- (NSString *)getLocalizedQRCodeString
{
    
    NSMutableString *qrString = [[NSMutableString alloc] init];
    if (((NSString *)self.name).length > 0 && self.birthDate && ((NSString *)self.gender).length > 0 ) {
        [qrString appendFormat:@"%@,",self.name];
        [qrString appendFormat:@"%@,",[DateUtils dateToString:self.birthDate andFormate:kDisplayDateFormatte]];
        [qrString appendFormat:@"%@,",[self.gender getTrimmedValue]];
        [qrString appendFormat:@"%@,",[CachingService sharedInstance].currentUserId];
        [qrString appendFormat:@"%@",[CachingService sharedInstance].branchCode];
    }
    
    //NSString *sourceString = [[NSString alloc] initWithFormat:@"\357\273\277%@", qrString];
    
    const char *utfString = [qrString UTF8String];
    NSString* finalEncodedString = [NSString stringWithUTF8String:utfString];
    return finalEncodedString;
}

- (NSString *)localizedBirthDate
{
    return [DateUtils dateToString:self.birthDate andFormate:kDisplayDateFormatte];
}

- (NSDate *)convertBirthDateToDate:(NSString *)localizedBirthDate
{
    return  [DateUtils stringToDate:localizedBirthDate dateFormat:kDisplayDateFormatte];
}

+ (TblContact *)getContactForName:(NSString *)name
{
   TblContact *contact =  [[DbUtils fetchObject:@"TblContact"
               andPredict:[NSPredicate predicateWithFormat:@"name == %@ AND agentId == %@ AND isDelete == 0", [[CachingService sharedInstance] currentUserId]]
        andSortDescriptor:nil
     managedObjectContext:[CachingService sharedInstance].managedObjectContext] mutableCopy];
    return contact;
}
@end
