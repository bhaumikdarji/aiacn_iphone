#import "TblEOP.h"

@interface TblEOP ()

// Private interface goes here.

@end

@implementation TblEOP

// Custom logic goes here.
- (NSString *)getLocalizedQRCodeString{
    NSString *qrString = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@",self.eventName,
                          [[NSDate dateToString:self.eventDate
                                    andFormate:kDisplayDateFormatte] lowercaseString],
                          [[NSDate stringDateToString:self.startTime
                                   currentDateForamte:kServerDateFormatte
                                   displayDateFormate:@"HH:mma"] lowercaseString],
                          self.speaker,
                          self.location,
                          self.eopDescription,
                          self.publicUrl];
    return qrString;
}

- (NSString *)getShareMessage{
    NSString *msgString = [NSString stringWithFormat:@"%@,%@",self.eventName,[self.eventDate description]];
    return msgString;
}



@end
