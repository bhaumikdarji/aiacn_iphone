#import "_TblAgentDetails.h"

@interface TblAgentDetails : _TblAgentDetails {}
// Custom logic goes here.
+ (TblAgentDetails *)getCurrentAgentDetails;
+ (TblAgentDetails *)getAgentDetails:(NSString *)agentId;
@end
