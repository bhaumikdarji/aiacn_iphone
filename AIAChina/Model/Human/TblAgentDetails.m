#import "TblAgentDetails.h"
#import "DbUtils.h"
@interface TblAgentDetails ()

// Private interface goes here.

@end

@implementation TblAgentDetails

// Custom logic goes here.

+ (TblAgentDetails *)getCurrentAgentDetails
{
    return [TblAgentDetails getAgentDetails:[CachingService sharedInstance].currentUserId];
}

+ (TblAgentDetails *)getAgentDetails:(NSString *)agentId
{
    
    TblAgentDetails *agentDetails = (TblAgentDetails *)[DbUtils fetchObject:@"TblAgentDetails"
                                                        andPredict:[NSPredicate predicateWithFormat:@"userID = %@",agentId]
                                                 andSortDescriptor:nil
                                              managedObjectContext:[CachingService sharedInstance].managedObjectContext];
    return agentDetails;
    
}
@end
