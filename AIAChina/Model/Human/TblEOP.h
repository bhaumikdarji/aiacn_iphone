#import "_TblEOP.h"

@interface TblEOP : _TblEOP {}
// Custom logic goes here.
- (NSString *)getLocalizedQRCodeString;
- (NSString *)getShareMessage;
@end
